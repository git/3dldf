-- MySQL dump 10.13  Distrib 8.0.33, for Linux (x86_64)
--
-- Host: localhost    Database: 3dldf
-- ------------------------------------------------------
-- Server version	8.0.33-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `circle_arrays`
--

DROP TABLE IF EXISTS `circle_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circle_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circle_arrays`
--

LOCK TABLES `circle_arrays` WRITE;
/*!40000 ALTER TABLE `circle_arrays` DISABLE KEYS */;
INSERT INTO `circle_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `circle_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circle_vectors`
--

DROP TABLE IF EXISTS `circle_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circle_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circle_vectors`
--

LOCK TABLES `circle_vectors` WRITE;
/*!40000 ALTER TABLE `circle_vectors` DISABLE KEYS */;
INSERT INTO `circle_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `circle_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `circles`
--

DROP TABLE IF EXISTS `circles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `circles` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `radius` float NOT NULL DEFAULT '0',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `circles`
--

LOCK TABLES `circles` WRITE;
/*!40000 ALTER TABLE `circles` DISABLE KEYS */;
INSERT INTO `circles` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,'',0,0,'',1,1);
/*!40000 ALTER TABLE `circles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cone_arrays`
--

DROP TABLE IF EXISTS `cone_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cone_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cone_arrays`
--

LOCK TABLES `cone_arrays` WRITE;
/*!40000 ALTER TABLE `cone_arrays` DISABLE KEYS */;
INSERT INTO `cone_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `cone_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cone_vectors`
--

DROP TABLE IF EXISTS `cone_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cone_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cone_vectors`
--

LOCK TABLES `cone_vectors` WRITE;
/*!40000 ALTER TABLE `cone_vectors` DISABLE KEYS */;
INSERT INTO `cone_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `cone_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cones`
--

DROP TABLE IF EXISTS `cones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cones` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `direction_x_coord` float NOT NULL DEFAULT '0',
  `direction_y_coord` float NOT NULL DEFAULT '0',
  `direction_z_coord` float NOT NULL DEFAULT '0',
  `apex_x_coord` float NOT NULL DEFAULT '0',
  `apex_y_coord` float NOT NULL DEFAULT '0',
  `apex_z_coord` float NOT NULL DEFAULT '0',
  `angle` float NOT NULL DEFAULT '0',
  `type` int unsigned NOT NULL DEFAULT '0',
  `nap_type` int unsigned NOT NULL DEFAULT '0',
  `radius` float NOT NULL DEFAULT '0',
  `axis_x` float NOT NULL DEFAULT '0',
  `axis_y` float NOT NULL DEFAULT '0',
  `axis_z` float NOT NULL DEFAULT '0',
  `base_id` int unsigned NOT NULL DEFAULT '0',
  `cap_id` int unsigned NOT NULL DEFAULT '0',
  `base_point_count` int unsigned NOT NULL DEFAULT '0',
  `divisions` int unsigned NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cones`
--

LOCK TABLES `cones` WRITE;
/*!40000 ALTER TABLE `cones` DISABLE KEYS */;
INSERT INTO `cones` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0);
/*!40000 ALTER TABLE `cones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connectors`
--

DROP TABLE IF EXISTS `connectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `connectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_2_id` int unsigned NOT NULL DEFAULT '0',
  `up_2_type` int unsigned NOT NULL DEFAULT '0',
  `up_2_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_3_id` int unsigned NOT NULL DEFAULT '0',
  `up_3_type` int unsigned NOT NULL DEFAULT '0',
  `up_3_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `connector_string` varchar(2) DEFAULT '',
  `type0` int NOT NULL DEFAULT '0',
  `type1` int NOT NULL DEFAULT '0',
  `pt0_id` int unsigned NOT NULL DEFAULT '0',
  `pt1_id` int unsigned NOT NULL DEFAULT '0',
  `r0` float NOT NULL DEFAULT '0',
  `r1` float NOT NULL DEFAULT '0',
  `atleast_flag0` tinyint(1) DEFAULT NULL,
  `atleast_flag1` tinyint(1) DEFAULT NULL,
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connectors`
--

LOCK TABLES `connectors` WRITE;
/*!40000 ALTER TABLE `connectors` DISABLE KEYS */;
INSERT INTO `connectors` VALUES (0,0,0,0,0,'',0,0,'',0,0,'',0,0,'','','',0,0,0,0,0,0,NULL,NULL,1,1);
/*!40000 ALTER TABLE `connectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuboid_vectors`
--

DROP TABLE IF EXISTS `cuboid_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cuboid_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuboid_vectors`
--

LOCK TABLES `cuboid_vectors` WRITE;
/*!40000 ALTER TABLE `cuboid_vectors` DISABLE KEYS */;
INSERT INTO `cuboid_vectors` VALUES (0,'','dummy',0,0,'',0,0);
/*!40000 ALTER TABLE `cuboid_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuboids`
--

DROP TABLE IF EXISTS `cuboids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cuboids` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `width` float NOT NULL DEFAULT '0',
  `height` float NOT NULL DEFAULT '0',
  `depth` float NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuboids`
--

LOCK TABLES `cuboids` WRITE;
/*!40000 ALTER TABLE `cuboids` DISABLE KEYS */;
INSERT INTO `cuboids` VALUES (0,'','dummy',0,0,0,0,0,'',0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `cuboids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cylinder_arrays`
--

DROP TABLE IF EXISTS `cylinder_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cylinder_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cylinder_arrays`
--

LOCK TABLES `cylinder_arrays` WRITE;
/*!40000 ALTER TABLE `cylinder_arrays` DISABLE KEYS */;
INSERT INTO `cylinder_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `cylinder_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cylinder_vectors`
--

DROP TABLE IF EXISTS `cylinder_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cylinder_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cylinder_vectors`
--

LOCK TABLES `cylinder_vectors` WRITE;
/*!40000 ALTER TABLE `cylinder_vectors` DISABLE KEYS */;
INSERT INTO `cylinder_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `cylinder_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cylinders`
--

DROP TABLE IF EXISTS `cylinders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cylinders` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `direction_x_coord` float NOT NULL DEFAULT '0',
  `direction_y_coord` float NOT NULL DEFAULT '0',
  `direction_z_coord` float NOT NULL DEFAULT '0',
  `angle` float NOT NULL DEFAULT '0',
  `radius` float NOT NULL DEFAULT '0',
  `axis_x` float NOT NULL DEFAULT '0',
  `axis_y` float NOT NULL DEFAULT '0',
  `axis_z` float NOT NULL DEFAULT '0',
  `base_point_count` int unsigned NOT NULL DEFAULT '0',
  `divisions` int unsigned NOT NULL DEFAULT '0',
  `type` int unsigned NOT NULL DEFAULT '0',
  `base_id` int unsigned NOT NULL DEFAULT '0',
  `cap_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cylinders`
--

LOCK TABLES `cylinders` WRITE;
/*!40000 ALTER TABLE `cylinders` DISABLE KEYS */;
INSERT INTO `cylinders` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0);
/*!40000 ALTER TABLE `cylinders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ellipse_arrays`
--

DROP TABLE IF EXISTS `ellipse_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ellipse_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ellipse_arrays`
--

LOCK TABLES `ellipse_arrays` WRITE;
/*!40000 ALTER TABLE `ellipse_arrays` DISABLE KEYS */;
INSERT INTO `ellipse_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `ellipse_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ellipse_vectors`
--

DROP TABLE IF EXISTS `ellipse_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ellipse_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ellipse_vectors`
--

LOCK TABLES `ellipse_vectors` WRITE;
/*!40000 ALTER TABLE `ellipse_vectors` DISABLE KEYS */;
INSERT INTO `ellipse_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `ellipse_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ellipses`
--

DROP TABLE IF EXISTS `ellipses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ellipses` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `focus_0_x_coord` float NOT NULL DEFAULT '0',
  `focus_0_y_coord` float NOT NULL DEFAULT '0',
  `focus_0_z_coord` float NOT NULL DEFAULT '0',
  `focus_1_x_coord` float NOT NULL DEFAULT '0',
  `focus_1_y_coord` float NOT NULL DEFAULT '0',
  `focus_1_z_coord` float NOT NULL DEFAULT '0',
  `axis_h` float NOT NULL DEFAULT '0',
  `axis_v` float NOT NULL DEFAULT '0',
  `linear_eccentricity` float NOT NULL DEFAULT '0',
  `numerical_eccentricity` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ellipses`
--

LOCK TABLES `ellipses` WRITE;
/*!40000 ALTER TABLE `ellipses` DISABLE KEYS */;
INSERT INTO `ellipses` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0,'',1,1);
/*!40000 ALTER TABLE `ellipses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glyph_arrays`
--

DROP TABLE IF EXISTS `glyph_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `glyph_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glyph_arrays`
--

LOCK TABLES `glyph_arrays` WRITE;
/*!40000 ALTER TABLE `glyph_arrays` DISABLE KEYS */;
INSERT INTO `glyph_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `glyph_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glyph_vectors`
--

DROP TABLE IF EXISTS `glyph_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `glyph_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glyph_vectors`
--

LOCK TABLES `glyph_vectors` WRITE;
/*!40000 ALTER TABLE `glyph_vectors` DISABLE KEYS */;
INSERT INTO `glyph_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `glyph_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `glyphs`
--

DROP TABLE IF EXISTS `glyphs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `glyphs` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `glyphs`
--

LOCK TABLES `glyphs` WRITE;
/*!40000 ALTER TABLE `glyphs` DISABLE KEYS */;
INSERT INTO `glyphs` VALUES (0,'','dummy',0,0,0);
/*!40000 ALTER TABLE `glyphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hyperbola_arrays`
--

DROP TABLE IF EXISTS `hyperbola_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hyperbola_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hyperbola_arrays`
--

LOCK TABLES `hyperbola_arrays` WRITE;
/*!40000 ALTER TABLE `hyperbola_arrays` DISABLE KEYS */;
INSERT INTO `hyperbola_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `hyperbola_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hyperbola_vectors`
--

DROP TABLE IF EXISTS `hyperbola_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hyperbola_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hyperbola_vectors`
--

LOCK TABLES `hyperbola_vectors` WRITE;
/*!40000 ALTER TABLE `hyperbola_vectors` DISABLE KEYS */;
INSERT INTO `hyperbola_vectors` VALUES (0,'dummy','',0,0,'',0,0);
/*!40000 ALTER TABLE `hyperbola_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hyperbolae`
--

DROP TABLE IF EXISTS `hyperbolae`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hyperbolae` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `focus_0_x_coord` float NOT NULL DEFAULT '0',
  `focus_0_y_coord` float NOT NULL DEFAULT '0',
  `focus_0_z_coord` float NOT NULL DEFAULT '0',
  `focus_1_x_coord` float NOT NULL DEFAULT '0',
  `focus_1_y_coord` float NOT NULL DEFAULT '0',
  `focus_1_z_coord` float NOT NULL DEFAULT '0',
  `vertex_0_x_coord` float NOT NULL DEFAULT '0',
  `vertex_0_y_coord` float NOT NULL DEFAULT '0',
  `vertex_0_z_coord` float NOT NULL DEFAULT '0',
  `vertex_1_x_coord` float NOT NULL DEFAULT '0',
  `vertex_1_y_coord` float NOT NULL DEFAULT '0',
  `vertex_1_z_coord` float NOT NULL DEFAULT '0',
  `parameter` float NOT NULL DEFAULT '0',
  `linear_eccentricity` float NOT NULL DEFAULT '0',
  `numerical_eccentricity` float NOT NULL DEFAULT '0',
  `directrix_0_0_x_coord` float NOT NULL DEFAULT '0',
  `directrix_0_0_y_coord` float NOT NULL DEFAULT '0',
  `directrix_0_0_z_coord` float NOT NULL DEFAULT '0',
  `directrix_0_1_x_coord` float NOT NULL DEFAULT '0',
  `directrix_0_1_y_coord` float NOT NULL DEFAULT '0',
  `directrix_0_1_z_coord` float NOT NULL DEFAULT '0',
  `directrix_0_2_x_coord` float NOT NULL DEFAULT '0',
  `directrix_0_2_y_coord` float NOT NULL DEFAULT '0',
  `directrix_0_2_z_coord` float NOT NULL DEFAULT '0',
  `directrix_1_0_x_coord` float NOT NULL DEFAULT '0',
  `directrix_1_0_y_coord` float NOT NULL DEFAULT '0',
  `directrix_1_0_z_coord` float NOT NULL DEFAULT '0',
  `directrix_1_1_x_coord` float NOT NULL DEFAULT '0',
  `directrix_1_1_y_coord` float NOT NULL DEFAULT '0',
  `directrix_1_1_z_coord` float NOT NULL DEFAULT '0',
  `directrix_1_2_x_coord` float NOT NULL DEFAULT '0',
  `directrix_1_2_y_coord` float NOT NULL DEFAULT '0',
  `directrix_1_2_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_0_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_0_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_0_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_1_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_1_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_1_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_2_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_2_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_0_2_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_0_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_0_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_0_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_1_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_1_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_1_z_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_2_x_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_2_y_coord` float NOT NULL DEFAULT '0',
  `asymptote_1_2_z_coord` float NOT NULL DEFAULT '0',
  `max_extent` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hyperbolae`
--

LOCK TABLES `hyperbolae` WRITE;
/*!40000 ALTER TABLE `hyperbolae` DISABLE KEYS */;
INSERT INTO `hyperbolae` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0,'',1,1);
/*!40000 ALTER TABLE `hyperbolae` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `line_arrays`
--

DROP TABLE IF EXISTS `line_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `line_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `line_arrays`
--

LOCK TABLES `line_arrays` WRITE;
/*!40000 ALTER TABLE `line_arrays` DISABLE KEYS */;
INSERT INTO `line_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `line_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `line_vectors`
--

DROP TABLE IF EXISTS `line_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `line_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `line_vectors`
--

LOCK TABLES `line_vectors` WRITE;
/*!40000 ALTER TABLE `line_vectors` DISABLE KEYS */;
INSERT INTO `line_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `line_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lines_ldf`
--

DROP TABLE IF EXISTS `lines_ldf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lines_ldf` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `position_x_coord` float NOT NULL DEFAULT '0',
  `position_y_coord` float NOT NULL DEFAULT '0',
  `position_z_coord` float NOT NULL DEFAULT '0',
  `direction_x_coord` float NOT NULL DEFAULT '0',
  `direction_y_coord` float NOT NULL DEFAULT '0',
  `direction_z_coord` float NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lines_ldf`
--

LOCK TABLES `lines_ldf` WRITE;
/*!40000 ALTER TABLE `lines_ldf` DISABLE KEYS */;
INSERT INTO `lines_ldf` VALUES (0,'','dummy',0,0,0,0,0,0,'',0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `lines_ldf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numeric_arrays`
--

DROP TABLE IF EXISTS `numeric_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `numeric_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numeric_arrays`
--

LOCK TABLES `numeric_arrays` WRITE;
/*!40000 ALTER TABLE `numeric_arrays` DISABLE KEYS */;
INSERT INTO `numeric_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `numeric_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numeric_vectors`
--

DROP TABLE IF EXISTS `numeric_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `numeric_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numeric_vectors`
--

LOCK TABLES `numeric_vectors` WRITE;
/*!40000 ALTER TABLE `numeric_vectors` DISABLE KEYS */;
INSERT INTO `numeric_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `numeric_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numerics`
--

DROP TABLE IF EXISTS `numerics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `numerics` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `value` float NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numerics`
--

LOCK TABLES `numerics` WRITE;
/*!40000 ALTER TABLE `numerics` DISABLE KEYS */;
INSERT INTO `numerics` VALUES (0,'','dummy',0,0,0,0,0,'',0,0,0);
/*!40000 ALTER TABLE `numerics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parabola_arrays`
--

DROP TABLE IF EXISTS `parabola_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parabola_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parabola_arrays`
--

LOCK TABLES `parabola_arrays` WRITE;
/*!40000 ALTER TABLE `parabola_arrays` DISABLE KEYS */;
INSERT INTO `parabola_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `parabola_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parabola_vectors`
--

DROP TABLE IF EXISTS `parabola_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parabola_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parabola_vectors`
--

LOCK TABLES `parabola_vectors` WRITE;
/*!40000 ALTER TABLE `parabola_vectors` DISABLE KEYS */;
INSERT INTO `parabola_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `parabola_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parabolae`
--

DROP TABLE IF EXISTS `parabolae`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parabolae` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `focus_x_coord` float NOT NULL DEFAULT '0',
  `focus_y_coord` float NOT NULL DEFAULT '0',
  `focus_z_coord` float NOT NULL DEFAULT '0',
  `vertex_x_coord` float NOT NULL DEFAULT '0',
  `vertex_y_coord` float NOT NULL DEFAULT '0',
  `vertex_z_coord` float NOT NULL DEFAULT '0',
  `parameter` float NOT NULL DEFAULT '0',
  `directrix_0_x_coord` float NOT NULL DEFAULT '0',
  `directrix_0_y_coord` float NOT NULL DEFAULT '0',
  `directrix_0_z_coord` float NOT NULL DEFAULT '0',
  `directrix_1_x_coord` float NOT NULL DEFAULT '0',
  `directrix_1_y_coord` float NOT NULL DEFAULT '0',
  `directrix_1_z_coord` float NOT NULL DEFAULT '0',
  `directrix_2_x_coord` float NOT NULL DEFAULT '0',
  `directrix_2_y_coord` float NOT NULL DEFAULT '0',
  `directrix_2_z_coord` float NOT NULL DEFAULT '0',
  `axis_0_x_coord` float NOT NULL DEFAULT '0',
  `axis_0_y_coord` float NOT NULL DEFAULT '0',
  `axis_0_z_coord` float NOT NULL DEFAULT '0',
  `axis_1_x_coord` float NOT NULL DEFAULT '0',
  `axis_1_y_coord` float NOT NULL DEFAULT '0',
  `axis_1_z_coord` float NOT NULL DEFAULT '0',
  `axis_2_x_coord` float NOT NULL DEFAULT '0',
  `axis_2_y_coord` float NOT NULL DEFAULT '0',
  `axis_2_z_coord` float NOT NULL DEFAULT '0',
  `axis_3_x_coord` float NOT NULL DEFAULT '0',
  `axis_3_y_coord` float NOT NULL DEFAULT '0',
  `axis_3_z_coord` float NOT NULL DEFAULT '0',
  `axis_4_x_coord` float NOT NULL DEFAULT '0',
  `axis_4_y_coord` float NOT NULL DEFAULT '0',
  `axis_4_z_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_0_x_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_0_y_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_0_z_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_1_x_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_1_y_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_1_z_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_2_x_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_2_y_coord` float NOT NULL DEFAULT '0',
  `latus_rectum_2_z_coord` float NOT NULL DEFAULT '0',
  `max_extent` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parabolae`
--

LOCK TABLES `parabolae` WRITE;
/*!40000 ALTER TABLE `parabolae` DISABLE KEYS */;
INSERT INTO `parabolae` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'',0,0,'',1,1);
/*!40000 ALTER TABLE `parabolae` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `path_arrays`
--

DROP TABLE IF EXISTS `path_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `path_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `path_arrays`
--

LOCK TABLES `path_arrays` WRITE;
/*!40000 ALTER TABLE `path_arrays` DISABLE KEYS */;
INSERT INTO `path_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `path_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `path_vectors`
--

DROP TABLE IF EXISTS `path_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `path_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `path_vectors`
--

LOCK TABLES `path_vectors` WRITE;
/*!40000 ALTER TABLE `path_vectors` DISABLE KEYS */;
INSERT INTO `path_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `path_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paths`
--

DROP TABLE IF EXISTS `paths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `paths` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_2_id` int unsigned NOT NULL DEFAULT '0',
  `up_2_type` int unsigned NOT NULL DEFAULT '0',
  `up_2_type_name` varchar(64) NOT NULL DEFAULT '',
  `line_switch` tinyint(1) NOT NULL DEFAULT '0',
  `cycle_switch` tinyint(1) NOT NULL DEFAULT '0',
  `fill_draw_value` int NOT NULL DEFAULT '0',
  `arrow` int NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paths`
--

LOCK TABLES `paths` WRITE;
/*!40000 ALTER TABLE `paths` DISABLE KEYS */;
INSERT INTO `paths` VALUES (0,'','dummy',0,0,0,0,0,0,'',0,0,'',0,0,'',0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `paths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane_arrays`
--

DROP TABLE IF EXISTS `plane_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plane_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane_arrays`
--

LOCK TABLES `plane_arrays` WRITE;
/*!40000 ALTER TABLE `plane_arrays` DISABLE KEYS */;
INSERT INTO `plane_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `plane_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane_vectors`
--

DROP TABLE IF EXISTS `plane_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plane_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane_vectors`
--

LOCK TABLES `plane_vectors` WRITE;
/*!40000 ALTER TABLE `plane_vectors` DISABLE KEYS */;
INSERT INTO `plane_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `plane_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `planes`
--

DROP TABLE IF EXISTS `planes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planes` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `normal_x_coord` float NOT NULL DEFAULT '0',
  `normal_y_coord` float NOT NULL DEFAULT '0',
  `normal_z_coord` float NOT NULL DEFAULT '0',
  `point_x_coord` float NOT NULL DEFAULT '0',
  `point_y_coord` float NOT NULL DEFAULT '0',
  `point_z_coord` float NOT NULL DEFAULT '0',
  `distance` float NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planes`
--

LOCK TABLES `planes` WRITE;
/*!40000 ALTER TABLE `planes` DISABLE KEYS */;
INSERT INTO `planes` VALUES (0,'','dummy',0,0,0,0,0,0,'',0,0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `planes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_arrays`
--

DROP TABLE IF EXISTS `point_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `point_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_arrays`
--

LOCK TABLES `point_arrays` WRITE;
/*!40000 ALTER TABLE `point_arrays` DISABLE KEYS */;
INSERT INTO `point_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `point_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_vectors`
--

DROP TABLE IF EXISTS `point_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `point_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_vectors`
--

LOCK TABLES `point_vectors` WRITE;
/*!40000 ALTER TABLE `point_vectors` DISABLE KEYS */;
INSERT INTO `point_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `point_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `points` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_2_id` int unsigned NOT NULL DEFAULT '0',
  `up_2_type` int unsigned NOT NULL DEFAULT '0',
  `up_2_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_3_id` int unsigned NOT NULL DEFAULT '0',
  `up_3_type` int unsigned NOT NULL DEFAULT '0',
  `up_3_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_4_id` int unsigned NOT NULL DEFAULT '0',
  `up_4_type` int unsigned NOT NULL DEFAULT '0',
  `up_4_type_name` varchar(64) NOT NULL DEFAULT '',
  `world_coordinates_x` float NOT NULL DEFAULT '0',
  `world_coordinates_y` float NOT NULL DEFAULT '0',
  `world_coordinates_z` float NOT NULL DEFAULT '0',
  `drawdot_value` int NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points`
--

LOCK TABLES `points` WRITE;
/*!40000 ALTER TABLE `points` DISABLE KEYS */;
INSERT INTO `points` VALUES (0,'','dummy',0,0,0,0,0,0,0,'',0,0,'',0,0,'',0,0,'',0,0,'',0,0,0,0,1,1);
/*!40000 ALTER TABLE `points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polygon_arrays`
--

DROP TABLE IF EXISTS `polygon_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polygon_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polygon_arrays`
--

LOCK TABLES `polygon_arrays` WRITE;
/*!40000 ALTER TABLE `polygon_arrays` DISABLE KEYS */;
INSERT INTO `polygon_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `polygon_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polygon_vectors`
--

DROP TABLE IF EXISTS `polygon_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polygon_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polygon_vectors`
--

LOCK TABLES `polygon_vectors` WRITE;
/*!40000 ALTER TABLE `polygon_vectors` DISABLE KEYS */;
INSERT INTO `polygon_vectors` VALUES (0,'','dummy',0,0,'',0,0);
/*!40000 ALTER TABLE `polygon_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polygons`
--

DROP TABLE IF EXISTS `polygons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polygons` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_2_id` int unsigned NOT NULL DEFAULT '0',
  `up_2_type` int unsigned NOT NULL DEFAULT '0',
  `up_2_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polygons`
--

LOCK TABLES `polygons` WRITE;
/*!40000 ALTER TABLE `polygons` DISABLE KEYS */;
INSERT INTO `polygons` VALUES (0,'','dummy',0,0,0,0,0,0,0,'',0,0,'',0,0,'',1,1);
/*!40000 ALTER TABLE `polygons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polyhedra`
--

DROP TABLE IF EXISTS `polyhedra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polyhedra` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `faces` int unsigned NOT NULL DEFAULT '0',
  `vertices` int unsigned NOT NULL DEFAULT '0',
  `edges` int unsigned NOT NULL DEFAULT '0',
  `number_of_polygon_types` int unsigned NOT NULL DEFAULT '0',
  `face_radius` float NOT NULL DEFAULT '0',
  `edge_radius` float NOT NULL DEFAULT '0',
  `vertex_radius` float NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polyhedra`
--

LOCK TABLES `polyhedra` WRITE;
/*!40000 ALTER TABLE `polyhedra` DISABLE KEYS */;
INSERT INTO `polyhedra` VALUES (0,'','dummy',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `polyhedra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polyhedron_vectors`
--

DROP TABLE IF EXISTS `polyhedron_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `polyhedron_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polyhedron_vectors`
--

LOCK TABLES `polyhedron_vectors` WRITE;
/*!40000 ALTER TABLE `polyhedron_vectors` DISABLE KEYS */;
INSERT INTO `polyhedron_vectors` VALUES (0,'','dummy',0,0,'',0,0);
/*!40000 ALTER TABLE `polyhedron_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rectangle_vectors`
--

DROP TABLE IF EXISTS `rectangle_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rectangle_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rectangle_vectors`
--

LOCK TABLES `rectangle_vectors` WRITE;
/*!40000 ALTER TABLE `rectangle_vectors` DISABLE KEYS */;
INSERT INTO `rectangle_vectors` VALUES (0,0,0,'','','dummy',0,0);
/*!40000 ALTER TABLE `rectangle_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rectangles`
--

DROP TABLE IF EXISTS `rectangles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rectangles` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `axis_h` float NOT NULL DEFAULT '0',
  `axis_v` float NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rectangles`
--

LOCK TABLES `rectangles` WRITE;
/*!40000 ALTER TABLE `rectangles` DISABLE KEYS */;
INSERT INTO `rectangles` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,'',0,0,'',0,0,1,1);
/*!40000 ALTER TABLE `rectangles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_polygon_arrays`
--

DROP TABLE IF EXISTS `reg_polygon_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reg_polygon_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_polygon_arrays`
--

LOCK TABLES `reg_polygon_arrays` WRITE;
/*!40000 ALTER TABLE `reg_polygon_arrays` DISABLE KEYS */;
INSERT INTO `reg_polygon_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `reg_polygon_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_polygon_vectors`
--

DROP TABLE IF EXISTS `reg_polygon_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reg_polygon_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_polygon_vectors`
--

LOCK TABLES `reg_polygon_vectors` WRITE;
/*!40000 ALTER TABLE `reg_polygon_vectors` DISABLE KEYS */;
INSERT INTO `reg_polygon_vectors` VALUES (0,'','dummy',0,0,'',0,0);
/*!40000 ALTER TABLE `reg_polygon_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_polygons`
--

DROP TABLE IF EXISTS `reg_polygons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reg_polygons` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_2_id` int unsigned NOT NULL DEFAULT '0',
  `up_2_type` int unsigned NOT NULL DEFAULT '0',
  `up_2_type_name` varchar(64) NOT NULL DEFAULT '',
  `internal_angle` float NOT NULL DEFAULT '0',
  `radius` float NOT NULL DEFAULT '0',
  `sides` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_polygons`
--

LOCK TABLES `reg_polygons` WRITE;
/*!40000 ALTER TABLE `reg_polygons` DISABLE KEYS */;
INSERT INTO `reg_polygons` VALUES (0,'','dummy',0,0,0,0,0,0,0,0,0,0,'',0,0,'',0,0,'',0,0,0,1,1);
/*!40000 ALTER TABLE `reg_polygons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sphere_arrays`
--

DROP TABLE IF EXISTS `sphere_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sphere_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sphere_arrays`
--

LOCK TABLES `sphere_arrays` WRITE;
/*!40000 ALTER TABLE `sphere_arrays` DISABLE KEYS */;
INSERT INTO `sphere_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `sphere_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sphere_vectors`
--

DROP TABLE IF EXISTS `sphere_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sphere_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sphere_vectors`
--

LOCK TABLES `sphere_vectors` WRITE;
/*!40000 ALTER TABLE `sphere_vectors` DISABLE KEYS */;
INSERT INTO `sphere_vectors` VALUES (0,'','dummy',0,0,'',0,0);
/*!40000 ALTER TABLE `sphere_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spheres`
--

DROP TABLE IF EXISTS `spheres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spheres` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `radius` float NOT NULL DEFAULT '0',
  `center_x_coord` float NOT NULL DEFAULT '0',
  `center_y_coord` float NOT NULL DEFAULT '0',
  `center_z_coord` float NOT NULL DEFAULT '0',
  `sphere_type` int unsigned NOT NULL DEFAULT '0',
  `sphere_type_name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `divisions_vertical` int unsigned DEFAULT '0',
  `divisions_horizontal` int unsigned DEFAULT '0',
  `circle_point_count` int unsigned DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spheres`
--

LOCK TABLES `spheres` WRITE;
/*!40000 ALTER TABLE `spheres` DISABLE KEYS */;
INSERT INTO `spheres` VALUES (0,'','dummy',0,0,0,0,0,'',0,0,'',0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `spheres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `string_arrays`
--

DROP TABLE IF EXISTS `string_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `string_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `string_arrays`
--

LOCK TABLES `string_arrays` WRITE;
/*!40000 ALTER TABLE `string_arrays` DISABLE KEYS */;
INSERT INTO `string_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `string_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `string_vectors`
--

DROP TABLE IF EXISTS `string_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `string_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `string_vectors`
--

LOCK TABLES `string_vectors` WRITE;
/*!40000 ALTER TABLE `string_vectors` DISABLE KEYS */;
INSERT INTO `string_vectors` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `string_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `strings`
--

DROP TABLE IF EXISTS `strings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `strings` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `text` varchar(1024) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `strings`
--

LOCK TABLES `strings` WRITE;
/*!40000 ALTER TABLE `strings` DISABLE KEYS */;
INSERT INTO `strings` VALUES (0,'','dummy',0,0,0,0,0,0,0,'','',1,1);
/*!40000 ALTER TABLE `strings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transform_arrays`
--

DROP TABLE IF EXISTS `transform_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transform_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transform_arrays`
--

LOCK TABLES `transform_arrays` WRITE;
/*!40000 ALTER TABLE `transform_arrays` DISABLE KEYS */;
INSERT INTO `transform_arrays` VALUES (0,'','dummy',0,1,1);
/*!40000 ALTER TABLE `transform_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transform_vectors`
--

DROP TABLE IF EXISTS `transform_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transform_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transform_vectors`
--

LOCK TABLES `transform_vectors` WRITE;
/*!40000 ALTER TABLE `transform_vectors` DISABLE KEYS */;
INSERT INTO `transform_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `transform_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transforms`
--

DROP TABLE IF EXISTS `transforms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transforms` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `matrix_0_0` float NOT NULL DEFAULT '1',
  `matrix_0_1` float NOT NULL DEFAULT '0',
  `matrix_0_2` float NOT NULL DEFAULT '0',
  `matrix_0_3` float NOT NULL DEFAULT '0',
  `matrix_1_0` float NOT NULL DEFAULT '0',
  `matrix_1_1` float NOT NULL DEFAULT '1',
  `matrix_1_2` float NOT NULL DEFAULT '0',
  `matrix_1_3` float NOT NULL DEFAULT '0',
  `matrix_2_0` float NOT NULL DEFAULT '0',
  `matrix_2_1` float NOT NULL DEFAULT '0',
  `matrix_2_2` float NOT NULL DEFAULT '1',
  `matrix_2_3` float NOT NULL DEFAULT '0',
  `matrix_3_0` float NOT NULL DEFAULT '0',
  `matrix_3_1` float NOT NULL DEFAULT '0',
  `matrix_3_2` float NOT NULL DEFAULT '0',
  `matrix_3_3` float NOT NULL DEFAULT '1',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transforms`
--

LOCK TABLES `transforms` WRITE;
/*!40000 ALTER TABLE `transforms` DISABLE KEYS */;
INSERT INTO `transforms` VALUES (0,'','dummy',0,0,0,0,0,0,'',1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,0),(1,'','identity',0,0,0,0,0,0,'',1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1,1,0);
/*!40000 ALTER TABLE `transforms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `triangle_arrays`
--

DROP TABLE IF EXISTS `triangle_arrays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `triangle_arrays` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `triangle_arrays`
--

LOCK TABLES `triangle_arrays` WRITE;
/*!40000 ALTER TABLE `triangle_arrays` DISABLE KEYS */;
INSERT INTO `triangle_arrays` VALUES (0,'','dummy',1,1);
/*!40000 ALTER TABLE `triangle_arrays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `triangle_vectors`
--

DROP TABLE IF EXISTS `triangle_vectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `triangle_vectors` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `no_delete` tinyint(1) DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `triangle_vectors`
--

LOCK TABLES `triangle_vectors` WRITE;
/*!40000 ALTER TABLE `triangle_vectors` DISABLE KEYS */;
INSERT INTO `triangle_vectors` VALUES (0,'','dummy',0,0);
/*!40000 ALTER TABLE `triangle_vectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `triangles`
--

DROP TABLE IF EXISTS `triangles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `triangles` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `prefix` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `vector_type_id` int unsigned NOT NULL DEFAULT '0',
  `vector_type_type` int unsigned NOT NULL DEFAULT '0',
  `array_id` int unsigned NOT NULL DEFAULT '0',
  `array_flag` tinyint(1) NOT NULL DEFAULT '0',
  `up_id` int unsigned NOT NULL DEFAULT '0',
  `up_type` int unsigned NOT NULL DEFAULT '0',
  `up_type_name` varchar(64) NOT NULL DEFAULT '',
  `up_1_id` int unsigned NOT NULL DEFAULT '0',
  `up_1_type` int unsigned NOT NULL DEFAULT '0',
  `up_1_type_name` varchar(64) NOT NULL DEFAULT '',
  `A_x_coord` float NOT NULL DEFAULT '0',
  `A_y_coord` float NOT NULL DEFAULT '0',
  `A_z_coord` float NOT NULL DEFAULT '0',
  `B_x_coord` float NOT NULL DEFAULT '0',
  `B_y_coord` float NOT NULL DEFAULT '0',
  `B_z_coord` float NOT NULL DEFAULT '0',
  `C_x_coord` float NOT NULL DEFAULT '0',
  `C_y_coord` float NOT NULL DEFAULT '0',
  `C_z_coord` float NOT NULL DEFAULT '0',
  `a` float NOT NULL DEFAULT '0',
  `b` float NOT NULL DEFAULT '0',
  `c` float NOT NULL DEFAULT '0',
  `alpha` float NOT NULL DEFAULT '0',
  `beta` float NOT NULL DEFAULT '0',
  `gamma` float NOT NULL DEFAULT '0',
  `area` float NOT NULL DEFAULT '0',
  `vector_type_flag` tinyint(1) NOT NULL DEFAULT '0',
  `no_delete` tinyint(1) NOT NULL DEFAULT '0',
  `no_overwrite` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `triangles`
--

LOCK TABLES `triangles` WRITE;
/*!40000 ALTER TABLE `triangles` DISABLE KEYS */;
INSERT INTO `triangles` VALUES (0,'','dummy',0,0,0,0,0,0,'',0,0,'',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `triangles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_names`
--

DROP TABLE IF EXISTS `type_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_names` (
  `name_ctr` int unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_names`
--

LOCK TABLES `type_names` WRITE;
/*!40000 ALTER TABLE `type_names` DISABLE KEYS */;
INSERT INTO `type_names` VALUES (0,'Shape::NULL_SHAPE_TYPE'),(1,'Shape::POINT_TYPE'),(2,'Shape::POINT_POINTER_VECTOR_TYPE'),(3,'Shape::NURB_TYPE'),(4,'Shape::PATH_TYPE'),(5,'Shape::POLYGON_TYPE'),(6,'Shape::TRIANGLE_TYPE'),(7,'Shape::RECTANGLE_TYPE'),(8,'Shape::REG_POLYGON_TYPE'),(9,'Shape::CONIC_SECTION_TYPE'),(10,'Shape::ELLIPSE_TYPE'),(11,'Shape::SUPERELLIPSE_TYPE'),(12,'Shape::CIRCLE_TYPE'),(13,'Shape::PARABOLA_TYPE'),(14,'Shape::HYPERBOLA_TYPE'),(15,'Shape::ARC_TYPE'),(16,'Shape::HELIX_TYPE'),(17,'Shape::SOLID_TYPE'),(18,'Shape::SOLID_FACED_TYPE'),(19,'Shape::POLYHEDRON_TYPE'),(20,'Shape::CUBOID_TYPE'),(21,'Shape::TETRAHEDRON_TYPE'),(22,'Shape::OCTAHEDRON_TYPE'),(11,'Shape::SUPERELLIPSE_TYPE'),(12,'Shape::CIRCLE_TYPE'),(13,'Shape::PARABOLA_TYPE'),(14,'Shape::HYPERBOLA_TYPE'),(15,'Shape::ARC_TYPE'),(16,'Shape::HELIX_TYPE'),(17,'Shape::SOLID_TYPE'),(18,'Shape::SOLID_FACED_TYPE'),(19,'Shape::POLYHEDRON_TYPE'),(20,'Shape::CUBOID_TYPE'),(21,'Shape::TETRAHEDRON_TYPE'),(22,'Shape::OCTAHEDRON_TYPE'),(23,'Shape::DODECAHEDRON_TYPE'),(24,'Shape::ICOSAHEDRON_TYPE'),(25,'Shape::TRUNC_OCTAHEDRON_TYPE'),(26,'Shape::GREAT_RHOMBICOSIDODECAHEDRON_TYPE'),(27,'Shape::RHOMBIC_TRIACONTAHEDRON_TYPE'),(28,'Shape::CONE_TYPE'),(29,'Shape::ELLIPTICAL_CONE_TYPE'),(30,'Shape::CIRCULAR_CONE_TYPE'),(31,'Shape::PARABOLIC_CONE_TYPE'),(32,'Shape::HYPERBOLIC_CONE_TYPE'),(33,'Shape::CYLINDER_TYPE'),(23,'Shape::DODECAHEDRON_TYPE'),(24,'Shape::ICOSAHEDRON_TYPE'),(25,'Shape::TRUNC_OCTAHEDRON_TYPE'),(26,'Shape::GREAT_RHOMBICOSIDODECAHEDRON_TYPE'),(27,'Shape::RHOMBIC_TRIACONTAHEDRON_TYPE'),(28,'Shape::CONE_TYPE'),(29,'Shape::ELLIPTICAL_CONE_TYPE'),(30,'Shape::CIRCULAR_CONE_TYPE'),(31,'Shape::PARABOLIC_CONE_TYPE'),(32,'Shape::HYPERBOLIC_CONE_TYPE'),(33,'Shape::CYLINDER_TYPE'),(34,'Shape::ELLIPTICAL_CYLINDER_TYPE'),(35,'Shape::CIRCULAR_CYLINDER_TYPE'),(36,'Shape::PARABOLIC_CYLINDER_TYPE'),(37,'Shape::HYPERBOLIC_CYLINDER_TYPE'),(38,'Shape::ELLIPSOID_TYPE'),(39,'Shape::SUPERELLIPSOID_TYPE'),(40,'Shape::SPHERE_TYPE'),(41,'Shape::PARABOLOID_TYPE'),(42,'Shape::ELLIPTICAL_PARABOLOID_TYPE'),(43,'Shape::HYPERBOLIC_PARABOLOID_TYPE'),(44,'Shape::HYPERBOLOID_TYPE'),(34,'Shape::ELLIPTICAL_CYLINDER_TYPE'),(35,'Shape::CIRCULAR_CYLINDER_TYPE'),(36,'Shape::PARABOLIC_CYLINDER_TYPE'),(37,'Shape::HYPERBOLIC_CYLINDER_TYPE'),(38,'Shape::ELLIPSOID_TYPE'),(39,'Shape::SUPERELLIPSOID_TYPE'),(40,'Shape::SPHERE_TYPE'),(41,'Shape::PARABOLOID_TYPE'),(42,'Shape::ELLIPTICAL_PARABOLOID_TYPE'),(43,'Shape::HYPERBOLIC_PARABOLOID_TYPE'),(44,'Shape::HYPERBOLOID_TYPE'),(45,'Shape::ONE_SHEET_HYPERBOLOID_TYPE'),(46,'Shape::TWO_SHEET_HYPERBOLOID_TYPE'),(47,'Shape::CONNECTOR_TYPE_TYPE'),(48,'Shape::NUMERIC_TYPE'),(49,'Shape::TRANSFORM_TYPE'),(50,'Shape::NUMERIC_VECTOR_TYPE'),(51,'Shape::TRANSFORM_VECTOR_TYPE'),(45,'Shape::ONE_SHEET_HYPERBOLOID_TYPE'),(46,'Shape::TWO_SHEET_HYPERBOLOID_TYPE'),(47,'Shape::CONNECTOR_TYPE_TYPE'),(48,'Shape::NUMERIC_TYPE'),(49,'Shape::TRANSFORM_TYPE'),(50,'Shape::NUMERIC_VECTOR_TYPE'),(51,'Shape::TRANSFORM_VECTOR_TYPE'),(52,'Shape::POINT_VECTOR_TYPE'),(53,'Shape::PATH_VECTOR_TYPE'),(54,'Shape::ELLIPSE_VECTOR_TYPE'),(55,'Shape::CIRCLE_VECTOR_TYPE'),(56,'Shape::REG_POLYGON_VECTOR_TYPE'),(57,'Shape::ELLIPSOID_VECTOR_TYPE'),(58,'Shape::SPHERE_VECTOR_TYPE'),(59,'Shape::CUBOID_VECTOR_TYPE'),(52,'Shape::POINT_VECTOR_TYPE'),(53,'Shape::PATH_VECTOR_TYPE'),(54,'Shape::ELLIPSE_VECTOR_TYPE'),(55,'Shape::CIRCLE_VECTOR_TYPE'),(56,'Shape::REG_POLYGON_VECTOR_TYPE'),(57,'Shape::ELLIPSOID_VECTOR_TYPE'),(58,'Shape::SPHERE_VECTOR_TYPE'),(59,'Shape::CUBOID_VECTOR_TYPE'),(70,'Shape::STRING_TYPE'),(71,'Shape::STRING_ARRAY_TYPE'),(72,'Shape::STRING_VECTOR_TYPE'),(70,'Shape::STRING_TYPE'),(71,'Shape::STRING_ARRAY_TYPE'),(72,'Shape::STRING_VECTOR_TYPE');
/*!40000 ALTER TABLE `type_names` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-22  6:02:04
