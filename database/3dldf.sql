/* /home/laurence/3DLDF-2.0.4/database/3dldf.sql  */

/* Created Mon 22 Mar 2021 04:27:10 PM CET by Laurence D. Finston (LDF)  */

/* This file is part of GNU 3DLDF, a package for three-dimensional drawing.  */
/* Copyright (C) 2021 The Free Software Foundation, Inc. */

/* GNU 3DLDF is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or  */
/* (at your option) any later version. */

/* GNU 3DLDF is distributed in the hope that it will be useful,  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  */
/* GNU General Public License for more details.  */

/* You should have received a copy of the GNU General Public License  */
/* along with GNU 3DLDF; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  */

/* GNU 3DLDF is a GNU package.   */
/* It is part of the GNU Project of the   */
/* Free Software Foundation  */
/* and is published under the GNU General Public License.  */
/* See the website http://www.gnu.org  */
/* for more information.    */
/* GNU 3DLDF is available for downloading from  */
/* http://www.gnu.org/software/3dldf/LDF.html.  */

/* Please send bug reports to Laurence.Finston@gmx.de */
/* The mailing list help-3dldf@gnu.org is available for people to  */
/* ask other users for help.   */
/* The mailing list info-3dldf@gnu.org is for sending  */
/* announcements to users. To subscribe to these mailing lists, send an  */
/* email with "subscribe <email-address>" as the subject.   */

/* The author can be contacted at:  */

/* Laurence D. Finston                   */
/* c/o Free Software Foundation, Inc.   */
/* 51 Franklin St, Fifth Floor           */
/* Boston, MA  02110-1301               */
/* USA                                   */

/* Laurence.Finston@gmx.de */

/* * (1) */

create database 3dldf;

create user '3dldf'@'localhost';
grant all on 3dldf.* to '3dldf'@'localhost';

use 3dldf;

show tables;

show columns from connectors;


drop table points;

/* * points  */

show columns from points;

<!-- * (1) -->

drop table type_names;

create table type_names
(
   name_ctr int unsigned not null default 0,
   name varchar(64)	
);

select * from type_names;

delete from type_names;

replace into type_names (name_ctr, name) values
(0, "Shape::NULL_SHAPE_TYPE"),
(1, "Shape::POINT_TYPE"),
(2, "Shape::POINT_POINTER_VECTOR_TYPE"),
(3, "Shape::NURB_TYPE"),
(4, "Shape::PATH_TYPE"),
(5, "Shape::POLYGON_TYPE"),
(6, "Shape::TRIANGLE_TYPE"),
(7, "Shape::RECTANGLE_TYPE"),
(8, "Shape::REG_POLYGON_TYPE"),
(9, "Shape::CONIC_SECTION_TYPE"),
(10, "Shape::ELLIPSE_TYPE");



replace into type_names (name_ctr, name) values
(11, "Shape::SUPERELLIPSE_TYPE"),
(12, "Shape::CIRCLE_TYPE"),
(13, "Shape::PARABOLA_TYPE"),
(14, "Shape::HYPERBOLA_TYPE"),
(15, "Shape::ARC_TYPE"),
(16, "Shape::HELIX_TYPE"),
(17, "Shape::SOLID_TYPE"),
(18, "Shape::SOLID_FACED_TYPE"),
(19, "Shape::POLYHEDRON_TYPE"),
(20, "Shape::CUBOID_TYPE"),
(21, "Shape::TETRAHEDRON_TYPE"),
(22, "Shape::OCTAHEDRON_TYPE");


replace into type_names (name_ctr, name) values
(23, "Shape::DODECAHEDRON_TYPE"),
(24, "Shape::ICOSAHEDRON_TYPE"),
(25, "Shape::TRUNC_OCTAHEDRON_TYPE"),
(26, "Shape::GREAT_RHOMBICOSIDODECAHEDRON_TYPE"),
(27, "Shape::RHOMBIC_TRIACONTAHEDRON_TYPE"),
(28, "Shape::CONE_TYPE"),
(29, "Shape::ELLIPTICAL_CONE_TYPE"),
(30, "Shape::CIRCULAR_CONE_TYPE"),
(31, "Shape::PARABOLIC_CONE_TYPE"),
(32, "Shape::HYPERBOLIC_CONE_TYPE"),
(33, "Shape::CYLINDER_TYPE");

replace into type_names (name_ctr, name) values
(34, "Shape::ELLIPTICAL_CYLINDER_TYPE"),
(35, "Shape::CIRCULAR_CYLINDER_TYPE"),
(36, "Shape::PARABOLIC_CYLINDER_TYPE"),
(37, "Shape::HYPERBOLIC_CYLINDER_TYPE"),
(38, "Shape::ELLIPSOID_TYPE"),
(39, "Shape::SUPERELLIPSOID_TYPE"),
(40, "Shape::SPHERE_TYPE"),
(41, "Shape::PARABOLOID_TYPE"),
(42, "Shape::ELLIPTICAL_PARABOLOID_TYPE"),
(43, "Shape::HYPERBOLIC_PARABOLOID_TYPE"),
(44, "Shape::HYPERBOLOID_TYPE");

replace into type_names (name_ctr, name) values
(45, "Shape::ONE_SHEET_HYPERBOLOID_TYPE"),
(46, "Shape::TWO_SHEET_HYPERBOLOID_TYPE"),
(47, "Shape::CONNECTOR_TYPE_TYPE"),
(48, "Shape::NUMERIC_TYPE"),
(49, "Shape::TRANSFORM_TYPE"),
(50, "Shape::NUMERIC_VECTOR_TYPE"),
(51, "Shape::TRANSFORM_VECTOR_TYPE");


replace into type_names (name_ctr, name) values
(52, "Shape::POINT_VECTOR_TYPE"),
(53, "Shape::PATH_VECTOR_TYPE"),
(54, "Shape::ELLIPSE_VECTOR_TYPE"),
(55, "Shape::CIRCLE_VECTOR_TYPE"),
(56, "Shape::REG_POLYGON_VECTOR_TYPE"),
(57, "Shape::ELLIPSOID_VECTOR_TYPE"),
(58, "Shape::SPHERE_VECTOR_TYPE"),
(59, "Shape::CUBOID_VECTOR_TYPE");

replace into type_names (name_ctr, name) values
(70, "Shape::STRING_TYPE"),
(71,  "Shape::STRING_ARRAY_TYPE"),        
(72,  "Shape::STRING_VECTOR_TYPE");       


insert into points (id, prefix, up_id, up_type, up_type_name, up_1_id, up_1_type, up_1_type_name, up_2_id, up_2_type, up_2_type_name, world_coordinates_x, world_coordinates_y, world_coordinates_z) values (6, "", 3, 47, "CONNECTOR_TYPE_TYPE", 3, 4, "PATH_TYPE", 1, 56, "PATH_ARRAY_TYPE", 1, 2, 3, 1), (7, "", 3, 47, "CONNECTOR_TYPE_TYPE", 3, 4, "PATH_TYPE", 1, 56, "PATH_ARRAY_TYPE", 30, 40, 50, 1);

select * from type_names order by name_ctr;

update type_names set name = "xxx" where name_ctr = 4;

select * from points where id > 0 order by id asc limit 6type_names order by name_ctr;o;


select P.id, P.up_type, T.name_ctr, T.name
from points as P, type_names as T where P.up_type = T.name_ctr;

select P.up_type from paths as P;

select distinct P.*, T.name as "Type Name" from points as P, type_names as T where P.up_type <> 0 and P.up_type = T.name_ctr order by P.up_type\G


/* * (1) points table definition */

drop table points;

create table points
(
    id int unsigned not null default 0 unique,
    prefix varchar(32) not null default "",
    name varchar(32) not null default "",
    vector_type_id int unsigned not null default 0,
    vector_type_type int unsigned not null default 0,
    array_id int unsigned not null default 0,
    array_flag boolean not null default false,
    vector_type_flag boolean not null default false,
    up_id int unsigned not null default 0,              /* E.g., connector.  */
    up_type int unsigned not null default 0,
    up_type_name varchar(64) not null default "",
    up_1_id int unsigned not null default 0,            /* E.g., path.  */
    up_1_type int unsigned not null default 0,
    up_1_type_name varchar(64) not null default "",
    up_2_id int unsigned not null default 0,            /* E.g., circle.  */
    up_2_type int unsigned not null default 0,
    up_2_type_name varchar(64) not null default "",
    up_3_id int unsigned not null default 0,            /* E.g., sphere.  */
    up_3_type int unsigned not null default 0,
    up_3_type_name varchar(64) not null default "",
    up_4_id int unsigned not null default 0,            /* E.g., sphere vector.  */
    up_4_type int unsigned not null default 0,
    up_4_type_name varchar(64) not null default "",
    world_coordinates_x float not null default 0.0,
    world_coordinates_y float not null default 0.0,
    world_coordinates_z float not null default 0.0,
    drawdot_value int not null default 0,
    no_delete boolean default false,
    no_overwrite boolean default false
);
replace into points (name, id, no_delete, no_overwrite)
values
("dummy", 0, true, true);


select * from points order by id;

select * from points;

show columns from points;

insert into points (id, name, prefix, up_id, up_type, up_type_name, world_coordinates_x, world_coordinates_y, world_coordinates_z)
values             (1, "p[0]", "",    1,     54,      "POINT_ARRAY_TYPE", 1, 2, 3, 1);


replace into points (id, prefix, name, world_coordinates_x, world_coordinates_y, world_coordinates_z) values (1, "abc", "p", 1, 2, 3);

show columns from 3dldf.points;

/* Insert into `points'  */

delete from points where id > 0;


select * from points where id > 0 order by prefix, id;


select count(id) from points where up_id = 1 and up_type = 4 order by id\G

replace into points (name, id, no_delete, no_overwrite)
values
("dummy", 0, true, true);

delete from points where name = "origin";


delete from points;

select * from points\G



select id from points order by id desc limit 1;

/* * Select  */

select prefix, name, up, array_flag, vector_type_flag,
world_coordinates_x, world_coordinates_y, world_coordinates_z
from points order by prefix, name\G

select * from points order by id, prefix, name, world_coordinates_x, world_coordinates_y,
world_coordinates_z asc\G


/* Point_Vectors  */

show tables;

drop table point_vectors;

create table point_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into point_vectors (id, name) values (0, "dummy");

select * from point_vectors;

show columns from point_vectors;

delete from point_vectors where id = 1 limit 1;

select "nv[]";

/* Point_Arrays  */

drop table point_arrays;

create table point_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into point_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from point_arrays;

/* Numerics  */

drop table numerics;

create table numerics
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   value float not null default 0.0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into numerics (id, name) values (0, "dummy");
select * from numerics order by id;

insert into numerics (id, name, up_id, value) values (0, "nv[0]", 1, 1);


/* Numeric_Vectors  */

drop table numeric_vectors;

create table numeric_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into numeric_vectors (id, name) values (0, "dummy");
show columns from numeric_vectors;
select * from numeric_vectors;


delete from numeric_vectors where id = 1 limit 1;

select "nv[]";

/* Numeric_Arrays  */

drop table numeric_arrays;

create table numeric_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into numeric_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from numeric_arrays;

show tables;

/* Transforms  */

drop table transforms;

create table transforms
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_flag boolean not null default false,
   vector_type_id int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   matrix_0_0 float not null default 1.0,
   matrix_0_1 float not null default 0.0,
   matrix_0_2 float not null default 0.0,
   matrix_0_3 float not null default 0.0,       
   matrix_1_0 float not null default 0.0,
   matrix_1_1 float not null default 1.0,
   matrix_1_2 float not null default 0.0,
   matrix_1_3 float not null default 0.0,
   matrix_2_0 float not null default 0.0,
   matrix_2_1 float not null default 0.0,
   matrix_2_2 float not null default 1.0,
   matrix_2_3 float not null default 0.0,
   matrix_3_0 float not null default 0.0,
   matrix_3_1 float not null default 0.0,
   matrix_3_2 float not null default 0.0,
   matrix_3_3 float not null default 1.0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into transforms
(id, name, no_delete)
values
(0, "dummy", true);
insert into transforms
(id, name, no_delete)
values
(1, "identity", true);
select * from transforms order by id;

insert into transforms (id, name, prefix, up_id, up_type, up_type_name, matrix_0_0, matrix_0_1,
                        matrix_0_2, matrix_0_3, matrix_1_0, matrix_1_1, matrix_1_2, matrix_1_3,
			matrix_2_0, matrix_2_1, matrix_2_2, matrix_2_3, matrix_3_0, matrix_3_1,
			matrix_3_2, matrix_3_3) values
			(2, "t[0]", "", 2, 52, "TRANSFORM_ARRAY_TYPE", 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1);


alter table transforms add column id int unsigned not null default 0 after prefix;

alter table transforms drop column array_flag;
alter table transforms drop column vector_type_flag;

alter table transforms add column array_flag boolean not null default false after up_name;
alter table transforms add column vector_type_flag boolean not null default false after array_flag;



show columns from transforms;


insert into transforms () values ();


delete from transforms;

insert into transforms (name) values ("t0");

select * from transforms order by name, matrix_0_0 asc\G


/* Transform_Vectors  */

show tables;

drop table transform_vectors;

create table transform_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into transform_vectors (id, name) values (0, "dummy");
show columns from transform_vectors;
select * from transform_vectors;


delete from transform_vectors where id = 1 limit 1;

select "nv[]";

/* Transform_Arrays  */

drop table transform_arrays;

create table transform_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into transform_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from transform_arrays;

/* String_Vectors  */

show tables;

drop table string_vectors;

create table string_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into string_vectors (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from string_vectors order by id;

/* String_Arrays  */

show tables;

drop table string_arrays;

create table string_arrays
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into string_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from string_arrays order by id;

/* * paths  */

show columns from paths;

delete from transforms where id > 1;

replace into transforms (id, name) values (3, "TT");

select id, name from transforms where name like("%%") order by id;

select * from transforms order by id\G

/* PLEASE NOTE!  `name' can't be unique because you can have `paths' with the
   same `name' but different `prefixes'.  There might be a way of making the
   _combination_ unique, but it's probably difficult.
   LDF 2022.08.09.  */


drop table paths;

create table paths
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,
   up_2_type int unsigned not null default 0,
   up_2_type_name varchar(64) not null default "",
   line_switch boolean not null default false,
   cycle_switch boolean not null default false,
   fill_draw_value int not null default 0,
   arrow int not null default 0,
   vector_type_flag boolean not null default false,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into paths (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from paths;


insert into paths (id, name, no_delete, no_overwrite) values (1, "dummy", true, true);

select * from paths where id in (2, 3);

show columns from paths;


alter table paths add column name varchar(32) not null default "" after name;

alter table paths add column up_id int unsigned not null default 0 after name;

alter table paths add column up_type int unsigned not null default 0 after up_id;

alter table paths add column up_name varchar(32) not null default "" after up_type;

alter table paths drop column up;

/* Path_Arrays  */

drop table path_arrays;

create table path_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into path_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from path_arrays;

insert into path_arrays (id, name) values (1, "xxx");


/* Path_Vectors  */

show tables;

drop table path_vectors;

create table path_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into path_vectors (id, name) values (0, "dummy");
select * from path_vectors;

show tables;

/* Triangles  */

drop table triangles;

create table triangles
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,        /* E.g., triangle_array or polyhedron */
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,        /* E.g., polyhedron_array  */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   A_x_coord float not null default 0.0,
   A_y_coord float not null default 0.0,
   A_z_coord float not null default 0.0,
   B_x_coord float not null default 0.0,
   B_y_coord float not null default 0.0,
   B_z_coord float not null default 0.0,
   C_x_coord float not null default 0.0,
   C_y_coord float not null default 0.0,
   C_z_coord float not null default 0.0,
   a float not null default 0.0,
   b float not null default 0.0,
   c float not null default 0.0,
   alpha float not null default 0.0,
   beta float not null default 0.0,
   gamma float not null default 0.0,
   area float not null default 0.0,
   vector_type_flag boolean not null default false,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into triangles (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "Triangles:";
select * from triangles order by id;

/* Triangle_Arrays  */

drop table triangle_arrays;

create table triangle_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into triangle_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "triangle_arrays";
select * from triangle_arrays order by id;

/* Triangle_Vectors  */

show tables;

drop table triangle_vectors;

create table triangle_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into triangle_vectors (id, name) values (0, "dummy");
select "triangle_vectors";
select * from triangle_vectors order by id;

/* Circle_Vectors  */

show tables;
drop table circle_vectors;

create table circle_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into circle_vectors (id, name) values (0, "dummy");
select * from circle_vectors;

show tables;


/* * connectors  */

-- int type0;
-- int type1;
-- string connector_string;
-- Point *pt0;
-- Point *pt1;
-- real  r0;
-- real  r1;
-- bool atleast_flag0;
-- bool atleast_flag1;

drop table connectors;

create table connectors
(
   id int unsigned not null default 0 unique,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,            /* Probably only ever path.  */
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,            
   up_1_type int unsigned not null default 0,          /* E.g., circle.  */
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,            
   up_2_type int unsigned not null default 0,          /* E.g., sphere.  */
   up_2_type_name varchar(64) not null default "",
   up_3_id int unsigned not null default 0,            
   up_3_type int unsigned not null default 0,          /* Probably never used.  */
   up_3_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   connector_string varchar(2) default "",
   type0 int not null default 0,
   type1 int not null default 0,
   pt0_id int unsigned not null default 0,
   pt1_id int unsigned not null default 0,
   r0 float not null default 0.0,
   r1 float not null default 0.0,
   atleast_flag0 boolean,
   atleast_flag1 boolean,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into connectors (id, no_overwrite, no_delete) values (0, true, true);

select * from connectors order by id asc;

select id, pt0_id, pt1_id from connectors where pt0_id > 0 or pt1_id > 0 order by id;

alter table connectors add column up_type int unsigned not null default 0 after up_id;

delete from connectors where id > 0;

show columns from connectors;


select * from connectors\G


<!-- * (1) circles table definition  -->

show tables;

drop table circles;

create table circles
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   radius float not null default 0.0,
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,   
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., sphere  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., sphere vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into circles (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
select "circles";
select * from circles order by id;

alter table circles drop column path_id;
select count(id) from circles;


select id, name from circles where id > 0 and up_id = 0 order by id;

alter table circles drop column path_id;

/* circle_Arrays  */

drop table circle_arrays;

create table circle_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into circle_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from circle_arrays;

<!-- * (1) ellipses table definition  -->

drop table ellipses;

create table ellipses
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,
   focus_0_x_coord float not null default 0.0,
   focus_0_y_coord float not null default 0.0,
   focus_0_z_coord float not null default 0.0,
   focus_1_x_coord float not null default 0.0,
   focus_1_y_coord float not null default 0.0,
   focus_1_z_coord float not null default 0.0,
   axis_h float not null default 0.0,
   axis_v float not null default 0.0,
   linear_eccentricity float not null default 0.0,
   numerical_eccentricity float not null default 0.0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., ellipsoid  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., ellipsoid vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into ellipses (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
show columns from ellipses;
select "ellipses";
select * from ellipses order by id;

/* Ellipse_Arrays  */

drop table ellipse_arrays;

create table ellipse_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into ellipse_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from ellipse_arrays;

/* ellipse_vectors  */

show tables;

drop table ellipse_vectors;

create table ellipse_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into ellipse_vectors (id, name) values (0, "dummy");
select * from ellipse_vectors;

insert into ellipse_vectors (id, name) values (1, "ev");


<!-- * (1) parabolae table definition  -->

drop table parabolae;

create table parabolae
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   focus_x_coord float not null default 0.0,
   focus_y_coord float not null default 0.0,
   focus_z_coord float not null default 0.0,
   vertex_x_coord float not null default 0.0,
   vertex_y_coord float not null default 0.0,
   vertex_z_coord float not null default 0.0,
   parameter float not null default 0.0,
   directrix_0_x_coord float not null default 0.0,
   directrix_0_y_coord float not null default 0.0,
   directrix_0_z_coord float not null default 0.0,
   directrix_1_x_coord float not null default 0.0,
   directrix_1_y_coord float not null default 0.0,
   directrix_1_z_coord float not null default 0.0,
   directrix_2_x_coord float not null default 0.0,
   directrix_2_y_coord float not null default 0.0,
   directrix_2_z_coord float not null default 0.0,
   axis_0_x_coord float not null default 0.0,
   axis_0_y_coord float not null default 0.0,
   axis_0_z_coord float not null default 0.0,
   axis_1_x_coord float not null default 0.0,
   axis_1_y_coord float not null default 0.0,
   axis_1_z_coord float not null default 0.0,
   axis_2_x_coord float not null default 0.0,
   axis_2_y_coord float not null default 0.0,
   axis_2_z_coord float not null default 0.0,
   axis_3_x_coord float not null default 0.0,
   axis_3_y_coord float not null default 0.0,
   axis_3_z_coord float not null default 0.0,
   axis_4_x_coord float not null default 0.0,
   axis_4_y_coord float not null default 0.0,
   axis_4_z_coord float not null default 0.0,
   latus_rectum_0_x_coord float not null default 0.0,
   latus_rectum_0_y_coord float not null default 0.0,
   latus_rectum_0_z_coord float not null default 0.0,
   latus_rectum_1_x_coord float not null default 0.0,
   latus_rectum_1_y_coord float not null default 0.0,
   latus_rectum_1_z_coord float not null default 0.0,
   latus_rectum_2_x_coord float not null default 0.0,
   latus_rectum_2_y_coord float not null default 0.0,
   latus_rectum_2_z_coord float not null default 0.0,
   max_extent float not null default 0.0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., paraboloid  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., paraboloid vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into parabolae (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
show columns from parabolae;
select "parabolae";
select * from parabolae order by id;

/* Parabola_Arrays  */

drop table parabola_arrays;

create table parabola_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into parabola_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from parabola_arrays;

/* parabola_vectors  */

show tables;

drop table parabola_vectors;

create table parabola_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into parabola_vectors (id, name) values (0, "dummy");
select * from parabola_vectors;

insert into parabola_vectors (id, name) values (1, "ev");

/* * (1) spheres  */

drop table spheres;

create table spheres
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   radius float not null default 0.0,
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,   
   sphere_type int unsigned not null default 0,
   sphere_type_name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   divisions_vertical int unsigned default  0.0,
   divisions_horizontal int unsigned default  0.0,
   circle_point_count int unsigned default 0.0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into spheres (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
select "spheres";
select * from spheres order by id;

insert into spheres (id, name, prefix, sphere_type, sphere_type_name, up_id, up_type, up_type_name, array_id, array_flag, radius, divisions_vertical, divisions_horizontal, circle_point_count) values 
(1, "s[0]", "", 1, "SPHERE_NULL_TYPE", 1, 68, "SPHERE_ARRAY_TYPE", 1, true, 1, 4, 4, 4);

/* Sphere_Arrays  */

drop table sphere_arrays;

create table sphere_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into sphere_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from sphere_arrays;

/* Sphere_Vectors  */

show tables;

drop table sphere_vectors;

create table sphere_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into sphere_vectors (id, name) values (0, "dummy");
select * from sphere_vectors;

<!-- * (1) hyperbolae table definition  -->

drop table hyperbolae;

create table hyperbolae
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,
   focus_0_x_coord float not null default 0.0,
   focus_0_y_coord float not null default 0.0,
   focus_0_z_coord float not null default 0.0,
   focus_1_x_coord float not null default 0.0,
   focus_1_y_coord float not null default 0.0,
   focus_1_z_coord float not null default 0.0,
   vertex_0_x_coord float not null default 0.0,
   vertex_0_y_coord float not null default 0.0,
   vertex_0_z_coord float not null default 0.0,
   vertex_1_x_coord float not null default 0.0,
   vertex_1_y_coord float not null default 0.0,
   vertex_1_z_coord float not null default 0.0,
   parameter float not null default 0.0,
   linear_eccentricity float not null default 0.0,
   numerical_eccentricity float not null default 0.0,	
   directrix_0_0_x_coord float not null default 0.0,
   directrix_0_0_y_coord float not null default 0.0,
   directrix_0_0_z_coord float not null default 0.0,
   directrix_0_1_x_coord float not null default 0.0,
   directrix_0_1_y_coord float not null default 0.0,
   directrix_0_1_z_coord float not null default 0.0,
   directrix_0_2_x_coord float not null default 0.0,
   directrix_0_2_y_coord float not null default 0.0,
   directrix_0_2_z_coord float not null default 0.0,
   directrix_1_0_x_coord float not null default 0.0,
   directrix_1_0_y_coord float not null default 0.0,
   directrix_1_0_z_coord float not null default 0.0,
   directrix_1_1_x_coord float not null default 0.0,
   directrix_1_1_y_coord float not null default 0.0,
   directrix_1_1_z_coord float not null default 0.0,
   directrix_1_2_x_coord float not null default 0.0,
   directrix_1_2_y_coord float not null default 0.0,
   directrix_1_2_z_coord float not null default 0.0,
   asymptote_0_0_x_coord float not null default 0.0,
   asymptote_0_0_y_coord float not null default 0.0,
   asymptote_0_0_z_coord float not null default 0.0,
   asymptote_0_1_x_coord float not null default 0.0,
   asymptote_0_1_y_coord float not null default 0.0,
   asymptote_0_1_z_coord float not null default 0.0,
   asymptote_0_2_x_coord float not null default 0.0,
   asymptote_0_2_y_coord float not null default 0.0,
   asymptote_0_2_z_coord float not null default 0.0,
   asymptote_1_0_x_coord float not null default 0.0,
   asymptote_1_0_y_coord float not null default 0.0,
   asymptote_1_0_z_coord float not null default 0.0,
   asymptote_1_1_x_coord float not null default 0.0,
   asymptote_1_1_y_coord float not null default 0.0,
   asymptote_1_1_z_coord float not null default 0.0,
   asymptote_1_2_x_coord float not null default 0.0,
   asymptote_1_2_y_coord float not null default 0.0,
   asymptote_1_2_z_coord float not null default 0.0,
   max_extent float not null default 0.0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., hyperboloid  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., hyperboloid vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into hyperbolae (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
show columns from hyperbolae;
select "hyperbolae";
select * from hyperbolae order by id;

/* Hyperbola_Arrays  */

drop table hyperbola_arrays;

create table hyperbola_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into hyperbola_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from hyperbola_arrays;

/* hyperbola_vectors  */

show tables;

drop table hyperbola_vectors;

create table hyperbola_vectors
(
   id int unsigned not null default 0 unique,
   name varchar(32) not null default "",
   prefix varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into hyperbola_vectors (id, name) values (0, "dummy");
select * from hyperbola_vectors;

insert into hyperbola_vectors (id, name) values (1, "ev");


/* Cuboid_Vectors  */

show tables;

drop table cuboid_vectors;

create table cuboid_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into cuboid_vectors (id, name) values (0, "dummy");
select * from cuboid_vectors;

/* Polyhedron_Vectors  */

show tables;

drop table polyhedron_vectors;

create table polyhedron_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into polyhedron_vectors (id, name) values (0, "dummy");
select * from polyhedron_vectors;

<!-- * (1) rectangles table definition  -->

drop table rectangles;

create table rectangles
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., cuboid  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., cuboid vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   axis_h float not null default 0.0,
   axis_v float not null default 0.0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into rectangles (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
show columns from rectangles;
select * from rectangles order by id;

/* rectangle_vectors  */

show tables;

drop table rectangle_vectors;

create table rectangle_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into rectangle_vectors (id, name) values (0, "dummy");
select * from rectangle_vectors;

insert into rectangle_vectors (id, name) values (1, "ev");



/* * (1) cuboids  */

  /* !! TODO:  height, width and depth aren't updated when the cuboid is transformed.
     Change this.  LDF 2022.11.17.  */

-- height float not null default 0.0,
-- width float not null default 0.0,
-- depth float not null default 0.0,

drop table cuboids;

create table cuboids
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   width float not null default 0.0,
   height float not null default 0.0,
   depth float not null default 0.0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into cuboids (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
select "cuboids";
select * from cuboids order by id;

/* * (1) reg_polygons  */

drop table reg_polygons;

create table reg_polygons
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,   
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   vector_type_flag boolean not null default false,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,
   up_2_type int unsigned not null default 0,
   up_2_type_name varchar(64) not null default "",
   internal_angle float not null default 0.0,
   radius float not null default 0.0,
   sides int unsigned not null default 0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into reg_polygons (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "reg_polygons";
select * from reg_polygons;


select r.id as "Reg_Polygon ID", r.name as "Reg_Polygon name", r.sides, r.internal_angle, r.radius, p.id as "Path ID", pt.id as "Point ID", pt.up_id as "Point up ID", pt.up_type as "Point up type", pt.up_type_name as "Point up type name", pt.world_coordinates_x, pt.world_coordinates_y, pt.world_coordinates_z from reg_polygons as r, paths as p, points as pt where r.id > 0 and r.prefix = "" and r.up_id = 0 and p.up_id = r.id and p.up_type = 8 and (pt.up_id = r.id and pt.up_type = 8 or pt.up_id = p.id and pt.up_type = 4)  order by r.id asc, p.id asc, pt.id asc;

/* * (1) reg_polygon_vectors  */

show tables;

drop table reg_polygon_vectors;

create table reg_polygon_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into reg_polygon_vectors (id, name) values (0, "dummy");
select * from reg_polygon_vectors;

/* reg_polygon_arrays  */

drop table reg_polygon_arrays;

create table reg_polygon_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into reg_polygon_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from reg_polygon_arrays;

insert into reg_polygon_arrays (id, name) values (1, "xxx");


/* * (1) polygons  */

drop table polygons;

create table polygons
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   vector_type_flag boolean not null default false,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,
   up_2_type int unsigned not null default 0,
   up_2_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into polygons (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "polygons:";
select * from polygons order by id;


/* * (1) polygon_vectors  */

show tables;

drop table polygon_vectors;

create table polygon_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into polygon_vectors (id, name) values (0, "dummy");
select * from polygon_vectors;

/* polygon_arrays  */

drop table polygon_arrays;

create table polygon_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into polygon_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from polygon_arrays;

insert into polygon_arrays (id, name) values (1, "xxx");

/* * (1) polyhedra  */

  /* !! TODO:  height, width and depth aren't updated when the cuboid is transformed.
     Change this, or get rid of them entirely.  LDF 2022.11.17.  */

-- height float not null default 0.0,
-- width float not null default 0.0,
-- depth float not null default 0.0,

drop table polyhedra;

create table polyhedra
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,
   faces int unsigned not null default 0,
   vertices int unsigned not null default 0,
   edges int unsigned not null default 0,
   number_of_polygon_types int unsigned not null default 0,
   face_radius float not null default 0.0,
   edge_radius float not null default 0.0,
   vertex_radius float not null default 0.0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into polyhedra (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);
select * from polyhedra order by id;


/* * (1)  Cylinders */

<!-- * (1) cylinders table definition  -->

drop table cylinders;

create table cylinders
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   direction_x_coord float not null default 0.0,
   direction_y_coord float not null default 0.0,
   direction_z_coord float not null default 0.0,
   angle float not null default 0.0,
   radius float not null default 0.0,
   axis_x float not null default 0.0,
   axis_y float not null default 0.0,
   axis_z float not null default 0.0,	
   base_point_count int unsigned not null default 0,
   divisions int unsigned not null default 0,
   type int unsigned not null default 0,
   base_id int unsigned not null default 0,
   cap_id int unsigned not null default 0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., cylinder vector or array  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into cylinders (id, name) values (0, "dummy");
show columns from cylinders;
select "cylinders";
select * from cylinders order by id;

/* Cylinder_Arrays  */

drop table cylinder_arrays;

create table cylinder_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into cylinder_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from cylinder_arrays;

/* cylinder_vectors  */

show tables;

drop table cylinder_vectors;

create table cylinder_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into cylinder_vectors (id, name) values (0, "dummy");
select * from cylinder_vectors;

/* * (1)  */

<!-- * (1) cones table definition  -->

drop table cones;

create table cones
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   direction_x_coord float not null default 0.0,
   direction_y_coord float not null default 0.0,
   direction_z_coord float not null default 0.0,
   apex_x_coord float not null default 0.0,
   apex_y_coord float not null default 0.0,
   apex_z_coord float not null default 0.0,
   angle float not null default 0.0,
   type int unsigned not null default 0,
   nap_type int unsigned not null default 0,
   radius float not null default 0.0,
   axis_x float not null default 0.0,
   axis_y float not null default 0.0,
   axis_z float not null default 0.0,	
   base_id int unsigned not null default 0,
   cap_id int unsigned not null default 0,
   base_point_count int unsigned not null default 0,
   divisions int unsigned not null default 0,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., cone vector or array  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into cones (id, name) values (0, "dummy");
show columns from cones;
select "cones";
select * from cones order by id;

/* Cone_Arrays  */

drop table cone_arrays;

create table cone_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into cone_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select * from cone_arrays;

/* cone_vectors  */

show tables;

drop table cone_vectors;

create table cone_vectors
(
   id int unsigned not null default 0 unique,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into cone_vectors (id, name) values (0, "dummy");
select * from cone_vectors;

<!-- * (1) strings table definition -->

drop table strings;

create table strings
(
    id int unsigned not null default 0 unique,
    prefix varchar(32) not null default "",
    name varchar(32) not null default "",
    vector_type_id int unsigned not null default 0,
    vector_type_type int unsigned not null default 0,
    array_id int unsigned not null default 0,
    array_flag boolean not null default false,
    vector_type_flag boolean not null default false,
    up_id int unsigned not null default 0,              /* E.g., connector.  */
    up_type int unsigned not null default 0,
    up_type_name varchar(64) not null default "",
    text varchar(1024) not null default "",
    no_delete boolean default false,
    no_overwrite boolean default false
);
replace into strings (name, id, no_delete, no_overwrite)
values
("dummy", 0, true, true);
select * from strings order by id;

/* * (1)  lines_ldf */

/* The name "lines" cannot be used.  Apparently it's a keyword in SQL  */
/* LDF 2022.12.15.                                                     */

drop table lines_ldf;

create table lines_ldf
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   position_x_coord float not null default 0.0,
   position_y_coord float not null default 0.0,
   position_z_coord float not null default 0.0,
   direction_x_coord float not null default 0.0,
   direction_y_coord float not null default 0.0,
   direction_z_coord float not null default 0.0,
   vector_type_flag boolean not null default false,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into lines_ldf (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "lines_ldf:";
select * from lines_ldf order by id;

/* line_arrays  */

drop table line_arrays;

create table line_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into line_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "line_arrays:";
select * from line_arrays;

/* line_vectors  */

drop table line_vectors;

create table line_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into line_vectors (id, name) values (0, "dummy");
select "line_vectors:";
select * from line_vectors order by id;

/* * (1)  planes */

drop table planes;

create table planes
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   normal_x_coord float not null default 0.0,
   normal_y_coord float not null default 0.0,
   normal_z_coord float not null default 0.0,
   point_x_coord float not null default 0.0,
   point_y_coord float not null default 0.0,
   point_z_coord float not null default 0.0,
   distance float not null default 0.0,
   vector_type_flag boolean not null default false,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);
insert into planes (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "planes:";
select * from planes order by id;

/* plane_arrays  */

drop table plane_arrays;

create table plane_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into plane_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "plane_arrays:";
select * from plane_arrays;

/* plane_vectors  */

drop table plane_vectors;

create table plane_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into plane_vectors (id, name) values (0, "dummy");
select "plane_vectors:";
select * from plane_vectors order by id;


/* * (1) glyphs table definition */

drop table glyphs;

create table glyphs
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into glyphs (id, name) values (0, "dummy");
select "glyphs:";
select * from glyphs order by id;

/* glyph_arrays  */

drop table glyph_arrays;

create table glyph_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into glyph_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);
select "glyph_arrays:";
select * from glyph_arrays;

/* * (1) glyph_vectors table definition */

drop table glyph_vectors;

create table glyph_vectors
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);
insert into glyph_vectors (id, name) values (0, "dummy");
select "glyph_vectors:";
select * from glyph_vectors order by id;

/* * (1) pictures table definition */


/* * (1) Font tables */

/* * (1) font_points table definition */

drop table font_points;

create table font_points
(
    id int unsigned not null default 0 unique,
    prefix varchar(32) not null default "",
    name varchar(32) not null default "",
    vector_type_id int unsigned not null default 0,
    vector_type_type int unsigned not null default 0,
    array_id int unsigned not null default 0,
    array_flag boolean not null default false,
    vector_type_flag boolean not null default false,
    up_id int unsigned not null default 0,              /* E.g., connector.  */
    up_type int unsigned not null default 0,
    up_type_name varchar(64) not null default "",
    up_1_id int unsigned not null default 0,            /* E.g., path.  */
    up_1_type int unsigned not null default 0,
    up_1_type_name varchar(64) not null default "",
    up_2_id int unsigned not null default 0,            /* E.g., circle.  */
    up_2_type int unsigned not null default 0,
    up_2_type_name varchar(64) not null default "",
    up_3_id int unsigned not null default 0,            /* E.g., sphere.  */
    up_3_type int unsigned not null default 0,
    up_3_type_name varchar(64) not null default "",
    up_4_id int unsigned not null default 0,            /* E.g., sphere vector.  */
    up_4_type int unsigned not null default 0,
    up_4_type_name varchar(64) not null default "",
    world_coordinates_x float not null default 0.0,
    world_coordinates_y float not null default 0.0,
    world_coordinates_z float not null default 0.0,
    drawdot_value int not null default 0,
    no_delete boolean default false,
    no_overwrite boolean default false
);

replace into font_points (name, id, no_delete, no_overwrite)
values
("dummy", 0, true, true);


/* ** (2) font_point_Arrays  */

drop table font_point_arrays;

create table font_point_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   no_delete boolean default false,
   no_overwrite boolean not null default false
);

insert into font_point_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from font_point_arrays;

/* ** (2) font_paths  */

drop table font_paths;

create table font_paths
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,
   up_2_type int unsigned not null default 0,
   up_2_type_name varchar(64) not null default "",
   line_switch boolean not null default false,
   cycle_switch boolean not null default false,
   fill_draw_value int not null default 0,
   arrow int not null default 0,
   vector_type_flag boolean not null default false,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);

insert into font_paths (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from font_paths;

/* ** (2) font_path_arrays  */

drop table font_path_arrays;

create table font_path_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);

insert into font_path_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from font_path_arrays;

/* ** (2) font_connectors  */

drop table font_connectors;

create table font_connectors
(
   id int unsigned not null default 0 unique,
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,            /* Probably only ever path.  */
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,            
   up_1_type int unsigned not null default 0,          /* E.g., circle.  */
   up_1_type_name varchar(64) not null default "",
   up_2_id int unsigned not null default 0,            
   up_2_type int unsigned not null default 0,          /* E.g., sphere.  */
   up_2_type_name varchar(64) not null default "",
   up_3_id int unsigned not null default 0,            
   up_3_type int unsigned not null default 0,          /* Probably never used.  */
   up_3_type_name varchar(64) not null default "",
   prefix varchar(32) not null default "",
   connector_string varchar(2) default "",
   type0 int not null default 0,
   type1 int not null default 0,
   pt0_id int unsigned not null default 0,
   pt1_id int unsigned not null default 0,
   r0 float not null default 0.0,
   r1 float not null default 0.0,
   atleast_flag0 boolean,
   atleast_flag1 boolean,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);

insert into font_connectors (id, no_overwrite, no_delete) values (0, true, true);

select * from font_connectors order by id asc;

/* ** (2)  */

drop table font_circles;

create table font_circles
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   radius float not null default 0.0,
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,   
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   up_id int unsigned not null default 0,             /* E.g., sphere  */
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   up_1_id int unsigned not null default 0,           /* E.g., sphere vector or array */
   up_1_type int unsigned not null default 0,
   up_1_type_name varchar(64) not null default "",
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);

insert into font_circles (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);

/* ** (2) font_spheres  */

drop table font_spheres;

create table font_spheres
(
   id int unsigned not null default 0 unique,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   radius float not null default 0.0,
   center_x_coord float not null default 0.0,
   center_y_coord float not null default 0.0,
   center_z_coord float not null default 0.0,   
   sphere_type int unsigned not null default 0,
   sphere_type_name varchar(32) not null default "",
   up_id int unsigned not null default 0,
   up_type int unsigned not null default 0,
   up_type_name varchar(64) not null default "",
   vector_type_id int unsigned not null default 0,
   vector_type_type int unsigned not null default 0,
   array_id int unsigned not null default 0,
   array_flag boolean not null default false,
   divisions_vertical int unsigned default  0.0,
   divisions_horizontal int unsigned default  0.0,
   circle_point_count int unsigned default 0.0,
   no_delete boolean not null default false,
   no_overwrite boolean not null default false
);

insert into font_spheres (id, name, no_delete, no_overwrite) values (0, "dummy", 1, 1);

/* ** (2)  */

/* circle_arrays  */

drop table font_circle_arrays;

create table font_circle_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);

insert into font_circle_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

/* ** (2)  */

/* font_sphere_arrays  */

drop table font_sphere_arrays;

create table font_sphere_arrays
(
   id int unsigned not null default 0,
   prefix varchar(32) not null default "",
   name varchar(32) not null default "",
   no_delete boolean default false,
   no_overwrite boolean not null default false
);

insert into font_sphere_arrays (id, name, no_delete, no_overwrite) values (0, "dummy", true, true);

select * from font_sphere_arrays;

/* ** (2)  */

/* * (1)  */


select * from paths where id > 1 order by id;


select * from points where id > 4 order by id;

show tables;

select * from numerics;

select * from numeric_arrays;

select * from points;

select * from points;

update ellipses set focus_0_id = 1, focus_1_id = 2 where id = 1;

select * from ellipses where id > 0;


select hv.id as "Hyperbola vector ID", hv.name as "Hyperbola vector name", h.id as "Hyperbola ID",
p.id as "Path ID", pt.id as "Point ID"
from hyperbola_vectors as hv, hyperbolae as h, paths as p, points as pt
where hv.id > 0 and h.id > 0 and p.id > 0 and h.up_id = hv.id and p.up_id = h.id
and pt.up_id = p.id
order by hv.id, h.id, p.id, pt.id;

, points as pt
 pt.id;


, pt.id as "Point ID", pt.up_type as "Point up type", pt.up_type_name as "Point up type name",
pt.world_coordinates_x, pt.world_coordinates_y, pt.world_coordinates_z


where hv.prefix = "" and h.prefix = "" and p.prefix = "" and pt.prefix = "" and hv.id > 0 and hv.up_id = 0
and h.up_id = hv.id and p.up_id = h.id and pt.up_id = p.id and pt.up_type = 6 and p.up_type = 16


select hv.id as "Hyperbola vector ID", hv.name as "Hyperbola vector name"
-- h.id as "Hyperbola ID", h.center_x_coord, h.center_y_coord,
-- h.center_z_coord, h.focus_0_x_coord, h.focus_0_y_coord,
-- h.focus_0_z_coord, h.focus_1_x_coord, h.focus_1_y_coord,
-- h.focus_1_z_coord, h.vertex_0_x_coord, h.vertex_0_y_coord,
-- h.vertex_0_z_coord, h.vertex_1_x_coord, h.vertex_1_y_coord,
-- h.vertex_1_z_coord, h.parameter, h.linear_eccentricity,
-- h.numerical_eccentricity, h.directrix_0_0_x_coord,
-- h.directrix_0_0_y_coord, h.directrix_0_0_z_coord,
-- h.directrix_0_1_x_coord, h.directrix_0_1_y_coord,
-- h.directrix_0_1_z_coord, h.directrix_0_2_x_coord,
-- h.directrix_0_2_y_coord, h.directrix_0_2_z_coord,
-- h.directrix_1_0_x_coord, h.directrix_1_0_y_coord,
-- h.directrix_1_0_z_coord, h.directrix_1_1_x_coord,
-- h.directrix_1_1_y_coord, h.directrix_1_1_z_coord,
-- h.directrix_1_2_x_coord, h.directrix_1_2_y_coord,
-- h.directrix_1_2_z_coord, h.asymptote_0_0_x_coord,
-- h.asymptote_0_0_y_coord, h.asymptote_0_0_z_coord,
-- h.asymptote_0_1_x_coord, h.asymptote_0_1_y_coord,
-- h.asymptote_0_1_z_coord, h.asymptote_0_2_x_coord,
-- h.asymptote_0_2_y_coord, h.asymptote_0_2_z_coord,
-- h.asymptote_1_0_x_coord, h.asymptote_1_0_y_coord,
-- h.asymptote_1_0_z_coord, h.asymptote_1_1_x_coord,
-- h.asymptote_1_1_y_coord, h.asymptote_1_1_z_coord,
-- h.asymptote_1_2_x_coord, h.asymptote_1_2_y_coord,
-- h.asymptote_1_2_z_coord, h.max_extent, p.id as "Path ID", pt.id as
-- "Point ID", pt.up_type as "Point up type", pt.up_type_name as "Point
-- up type name", pt.world_coordinates_x, pt.world_coordinates_y,
-- pt.world_coordinates_z
from hyperbola_vectors as hv;

, hyperbolae as h, paths as p, points as pt;
-- where hv.prefix = "" and h.prefix = "" and p.prefix = "" and pt.prefix = "" and hv.id > 0 and hv.up_id = 0
-- and h.up_id = hv.id and p.up_id = h.id and pt.up_id = p.id and pt.up_type = 6 and p.up_type = 16
-- order by hv.id, h.id, p.id, pt.id;
    

select q.id, p.id, p.up_id, p.up_type, p.world_coordinates_x, p.world_coordinates_y, p.world_coordinates_z from paths as q,
points as p where q.id = 4 and q.id = p.up_id order by q.id, p.id;

select distinct up_id from points order by up_id;

where q.id = 4 and p.up_id = q.id and p.up_type = 6 order by q.id, p.id;

select count(id) from points;

select count(id) from paths where prefix = "cmr10";

select count(id) from paths where prefix = "cmsy10";

select count(id) from points where prefix = "cmsy10";

select count(id) from paths where prefix = "cmitt10";

select count(id) from points where prefix = "cmitt10";

select * from points where prefix = "cmitt10" and length(name) > 0 order by id limit 2;

select count(id) from points where prefix = "eurm10" and length(name) > 0;

select count(id) from paths where prefix = "eurm10";

select count(id) from connectors;

select * from paths where name = "q[0]";

select * from points where name = "p[10]";

select * from points where id > 0 and up_id > 0 and up_type_name = "CONNECTOR_TYPE_TYPE" order by id limit 1;

select * from point_arrays;


select distinct prefix from paths;

select id from paths where name = "r[2]" and prefix = "" and up_id = 6 and up_type = 64;


select * from connectors where id > 0;



/* Local Variables: */
/* mode:SQL */
/* outline-minor-mode:t */
/* End: */

