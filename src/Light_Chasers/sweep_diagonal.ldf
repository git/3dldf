%% sweep_diagonal.ldf
%% Created by Laurence D. Finston (LDF) So 31. Mär 12:50:47 CEST 2024

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Sweep masks, diagonal

%% ** (2)

v4 := null_picture;

j := 0;
k := 23;
for loop_ctr = -1 upto 16:
  beginfig(fig_ctr);

    message "Fig. " & decimal fig_ctr;
    
    if do_png:
      fill frame with_color white;
    fi;

    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    current_picture := v1;
    current_picture += v4;

    
    if loop_ctr >= 0:
      fill R_array[0][loop_ctr] with_color yellow on_picture v4;
      fill C_array[0][loop_ctr] with_color red on_picture v4;
      if loop_ctr <= 15:
	fill R_array[loop_ctr][0] with_color yellow on_picture v4;
	fill C_array[loop_ctr][0] with_color red on_picture v4;
      fi;
    fi;

    for i = 1 upto loop_ctr:
      if (i >= 0) and ((loop_ctr - i) >= 0):
	if (i <= 15):
	  fill R_array[i][loop_ctr-i] with_color yellow on_picture v4;
	  fill C_array[i][loop_ctr-i] with_color red on_picture v4;
	fi;
	if (loop_ctr - 1) <= 15:
  	  fill R_array[loop_ctr-i][i] with_color yellow on_picture v4;
	  fill C_array[loop_ctr-i][i] with_color red on_picture v4;
	fi;
      fi;
    endfor;
    
    scale current_picture (.4, .4, .4);

    
  endfig with_projection parallel_x_z;
  fig_ctr += 1;

endfor;

%% ** (2)

v5 := null_picture;

for loop_ctr := 17 upto 22:
  beginfig(fig_ctr);
    
    if do_png:
      fill frame with_color white;
    fi;
    
    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v1;
    current_picture += v4;
    current_picture += v5;

    j := 0;
    for i = 0 upto 15:
      fill R[loop_ctr+24i-j] with_color yellow on_picture v5;
      fill C[loop_ctr+24i-j] with_color red on_picture v5;
      j += 1;
    endfor;

        
    scale current_picture (.4, .4, .4);
    
  endfig with_projection parallel_x_z;
  fig_ctr += 1;
endfor;

%% ** (2)

v6 := null_picture;

t9 := identity rotated_around (mediate(a10, a11), mediate(a13, a14)) 180;
rotate_around t9 (mediate(a11, a14), mediate(a10, a13)) 180;

j := 0;
k := 23;
for loop_ctr = 0 upto 16:
  beginfig(fig_ctr);

    if do_png:
      fill frame with_color white;
    fi;

    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    current_picture := v1;
    current_picture += v4;
    current_picture += v5;
    current_picture += v6;

    
    if loop_ctr >= 0:
      fill (R_array[0][loop_ctr]) * t9 with_color pink on_picture v6;
      fill (C_array[0][loop_ctr]) * t9 with_color cyan on_picture v6;

      if loop_ctr <= 15:
	fill (R_array[loop_ctr][0]) * t9 with_color pink on_picture v6;
	fill (C_array[loop_ctr][0]) * t9 with_color cyan on_picture v6;
      fi;
    fi;

    for i = 1 upto loop_ctr:
      if (i >= 0) and ((loop_ctr - i) >= 0):
	if (i <= 15):
	  fill (R_array[i][loop_ctr-i]) * t9 with_color pink on_picture v6;
	  fill (C_array[i][loop_ctr-i]) * t9 with_color cyan on_picture v6;
	fi;
	if (loop_ctr - 1) <= 15:
	  fill (R_array[loop_ctr-i][i]) * t9 with_color pink on_picture v6;
	  fill (C_array[loop_ctr-i][i]) * t9 with_color cyan on_picture v6;
	fi;
      fi;
    endfor;
    
    scale current_picture (.4, .4, .4);

    
  endfig with_projection parallel_x_z;
  fig_ctr += 1;

endfor;

%% * (1) Perspective

v7 := null_picture;

j := 0;
k := 23;
for loop_ctr = 0 upto 16:
  beginfig(fig_ctr);

%% **** (4)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;

    draw frame with_pen big_square_pen with_color blue; % black; % 
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% **** (4)
    
    if do_labels:
      dotlabel.llft("{\smallrm p9}", p9);
      dotlabel.bot("{\smallrm p10}", p10);
      dotlabel.top("{\smallrm p11}", p11);
      dotlabel.top("{\smallrm p12}", p12);
    fi;

    fill r0 with_color orange;
    fill r1 with_color violet;
    draw r0 with_color line_color;
    draw r1 with_color line_color;

    if do_labels:
      dotlabel.bot("{\smallrm p13}", p13);
      dotlabel.top("{\smallrm p14}", p14);
    fi;

    j := 15;
    for i = 0 upto 23:

      if do_labels:
	s := "{\TINYrm " & decimal j & "}";
	dotlabel.bot(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.top(s, p[j+1]);
      fi;


      if (do_labels) and (not do_black):
	draw p[j] -- p[j+1] with_pen medium_pen with_color dark_gray;
      fi;

      if do_labels:
	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.bot(s, p[j+2]);

	s := "{\TINYrm " & decimal (j+3) & "}";
	dotlabel.top(s, p[j+3]);
      fi;

      if not do_black:
	draw p[j+2] -- p[j+3] with_pen medium_pen with_color dark_gray;
      fi;

      j += 4;
    endfor;

    for i = 0 upto 15:

      if do_labels:
	s := "{\TINYrm " & decimal (j) & "}";
	dotlabel.lft(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.lrt(s, p[j+1]);

	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.rt(s, p[j+2]);
      fi;

      if not do_black:
	draw p[j] -- p[j+1] -- p[j+2] with_pen medium_pen with_color gray;
      fi;
      
      j += 3;
    endfor;


%% **** (4)

    current_picture += v7;
    
    if loop_ctr >= 0:
      fill (R_array[0][loop_ctr] * t1) with_color teal_blue_rgb on_picture v7;
      fill (C_array[0][loop_ctr] * t1) with_color red on_picture v7;
      fill (M_array[0][loop_ctr] * t0) with_color Goldenrod_rgb on_picture v7;
      fill (D_array[0][loop_ctr] * t0) with_color green on_picture v7;
      if loop_ctr <= 15:
	fill (R_array[loop_ctr][0]) * t1 with_color teal_blue_rgb on_picture v7;
	fill (C_array[loop_ctr][0]) * t1 with_color red on_picture v7;
	fill (M_array[loop_ctr][0]) * t0 with_color Goldenrod_rgb on_picture v7;
	fill (D_array[loop_ctr][0]) * t0 with_color green on_picture v7;
      fi;
    fi;

    for i = 1 upto loop_ctr:
      if (i >= 0) and ((loop_ctr - i) >= 0):
	if (i <= 15):
	  fill (R_array[i][loop_ctr-i]) * t1 with_color teal_blue_rgb on_picture v7;
	  fill (C_array[i][loop_ctr-i]) * t1 with_color red on_picture v7;
	  fill (M_array[i][loop_ctr-i]) * t0 with_color Goldenrod_rgb on_picture v7;
	  fill (D_array[i][loop_ctr-i]) * t0 with_color green on_picture v7;
	fi;
	if (loop_ctr - 1) <= 15:
	  fill (R_array[loop_ctr-i][i]) * t1 with_color teal_blue_rgb on_picture v7;
	  fill (C_array[loop_ctr-i][i]) * t1 with_color red on_picture v7;
	  fill (M_array[loop_ctr-i][i]) * t0 with_color Goldenrod_rgb on_picture v7;
	  fill (D_array[loop_ctr-i][i]) * t0 with_color green on_picture v7;
	fi;
      fi;
    endfor;

    
%% **** (4)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    %clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue; % black; % 
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;

%% ** (2)

v8 := null_picture;

for loop_ctr := 17 upto 22:
  beginfig(fig_ctr);

%% **** (4)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;

    draw frame with_pen big_square_pen with_color blue; % black; % 
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% **** (4)
    
    if do_labels:
      dotlabel.llft("{\smallrm p9}", p9);
      dotlabel.bot("{\smallrm p10}", p10);
      dotlabel.top("{\smallrm p11}", p11);
      dotlabel.top("{\smallrm p12}", p12);
    fi;

    fill r0 with_color orange;
    fill r1 with_color violet;
    draw r0 with_color line_color;
    draw r1 with_color line_color;

    if do_labels:
      dotlabel.bot("{\smallrm p13}", p13);
      dotlabel.top("{\smallrm p14}", p14);
    fi;

    j := 15;
    for i = 0 upto 23:

      if do_labels:
	s := "{\TINYrm " & decimal j & "}";
	dotlabel.bot(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.top(s, p[j+1]);
      fi;


      if (do_labels) and (not do_black):
	draw p[j] -- p[j+1] with_pen medium_pen with_color dark_gray;
      fi;

      if do_labels:
	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.bot(s, p[j+2]);

	s := "{\TINYrm " & decimal (j+3) & "}";
	dotlabel.top(s, p[j+3]);
      fi;

      if not do_black:
	draw p[j+2] -- p[j+3] with_pen medium_pen with_color dark_gray;
      fi;

      j += 4;
    endfor;

    for i = 0 upto 15:

      if do_labels:
	s := "{\TINYrm " & decimal (j) & "}";
	dotlabel.lft(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.lrt(s, p[j+1]);

	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.rt(s, p[j+2]);
      fi;

      if not do_black:
	draw p[j] -- p[j+1] -- p[j+2] with_pen medium_pen with_color gray;
      fi;
      
      j += 3;
    endfor;


%% **** (4)

    current_picture += v7;
    current_picture += v8;

    j := 0;
    for i = 0 upto 15:
      fill (R[loop_ctr+24i-j]) * t1 with_color teal_blue_rgb on_picture v8;
      fill (C[loop_ctr+24i-j]) * t1 with_color red on_picture v8;
      fill (M[loop_ctr+24i-j]) * t0 with_color Goldenrod_rgb on_picture v8;
      fill (D[loop_ctr+24i-j]) * t0 with_color green on_picture v8;
      j += 1;
    endfor;
    
%% **** (4)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    %clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue; % black; % 
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;


%% ** (2)

v9 := null_picture;

j := 0;
k := 23;
for loop_ctr = 0 upto 16:
  beginfig(fig_ctr);

%% **** (4)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;

    draw frame with_pen big_square_pen with_color blue; % black; % 
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% **** (4)
    
    if do_labels:
      dotlabel.llft("{\smallrm p9}", p9);
      dotlabel.bot("{\smallrm p10}", p10);
      dotlabel.top("{\smallrm p11}", p11);
      dotlabel.top("{\smallrm p12}", p12);
    fi;

    fill r0 with_color orange;
    fill r1 with_color violet;
    draw r0 with_color line_color;
    draw r1 with_color line_color;

    if do_labels:
      dotlabel.bot("{\smallrm p13}", p13);
      dotlabel.top("{\smallrm p14}", p14);
    fi;

    j := 15;
    for i = 0 upto 23:

      if do_labels:
	s := "{\TINYrm " & decimal j & "}";
	dotlabel.bot(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.top(s, p[j+1]);
      fi;


      if (do_labels) and (not do_black):
	draw p[j] -- p[j+1] with_pen medium_pen with_color dark_gray;
      fi;

      if do_labels:
	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.bot(s, p[j+2]);

	s := "{\TINYrm " & decimal (j+3) & "}";
	dotlabel.top(s, p[j+3]);
      fi;

      if not do_black:
	draw p[j+2] -- p[j+3] with_pen medium_pen with_color dark_gray;
      fi;

      j += 4;
    endfor;

    for i = 0 upto 15:

      if do_labels:
	s := "{\TINYrm " & decimal (j) & "}";
	dotlabel.lft(s, p[j]);

	s := "{\TINYrm " & decimal (j+1) & "}";
	dotlabel.lrt(s, p[j+1]);

	s := "{\TINYrm " & decimal (j+2) & "}";
	dotlabel.rt(s, p[j+2]);
      fi;

      if not do_black:
	draw p[j] -- p[j+1] -- p[j+2] with_pen medium_pen with_color gray;
      fi;
      
      j += 3;%
    endfor;


%% **** (4)

    current_picture += v7;
    current_picture += v8;
    current_picture += v9;

    if loop_ctr >= 0:
      fill ((R_array[0][loop_ctr]) * t9) * t1 with_color pink on_picture v9;
      fill ((C_array[0][loop_ctr]) * t9) * t1 with_color cyan on_picture v9;

      fill ((M_array[0][loop_ctr]) * t9) * t0 with_color Periwinkle_rgb on_picture v9;
      fill ((D_array[0][loop_ctr]) * t9) * t0 with_color Emerald on_picture v9;
      if loop_ctr <= 15:
	fill ((R_array[loop_ctr][0]) * t9) * t1 with_color pink on_picture v9;
	fill ((C_array[loop_ctr][0]) * t9) * t1 with_color cyan on_picture v9;

	fill ((M_array[loop_ctr][0]) * t9) * t0 with_color Periwinkle_rgb on_picture v9;
	fill ((D_array[loop_ctr][0]) * t9) * t0 with_color Emerald on_picture v9;
      fi;
    fi;

    for i = 1 upto loop_ctr:
      if (i >= 0) and ((loop_ctr - i) >= 0):
	if (i <= 15):
	  fill ((R_array[i][loop_ctr-i]) * t9) * t1 with_color pink on_picture v9;
	  fill ((C_array[i][loop_ctr-i]) * t9) * t1 with_color cyan on_picture v9;

	  fill ((M_array[i][loop_ctr-i]) * t9) * t0 with_color Periwinkle_rgb on_picture v9;
	  fill ((D_array[i][loop_ctr-i]) * t9) * t0 with_color Emerald on_picture v9;
	fi;
	if (loop_ctr - 1) <= 15:
	  fill ((R_array[loop_ctr-i][i]) * t9) * t1 with_color pink on_picture v9;
	  fill ((C_array[loop_ctr-i][i]) * t9) * t1 with_color cyan on_picture v9;

	  fill ((M_array[loop_ctr-i][i]) * t9) * t0 with_color Periwinkle_rgb on_picture v9;
	  fill ((D_array[loop_ctr-i][i]) * t9) * t0 with_color Emerald on_picture v9;
	fi;
      fi;
    endfor;
    
%% **** (4)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    %clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue; % black; % 
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;



%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
