/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 22. Mär 12:59:42 CET 2024  */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) Color names that appear in this file  */

#if 0

apricot
bittersweet
black
blue
burnt_orange
cerulean_blue
cyan
dark_green
emerald
goldenrod
green

magenta
maroon
melon
orange
periwinkle
pink
plum
purple
raw_sienna
red
rose_madder
salmon
sky_blue
teal_blue
thistle
violet
white
yellow

#endif 

/* ** (2) */

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int j = 0;
int k = 0;

int 
main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   j = 0;

   bool background_switch;
   background_switch = true;

   for (int i = 0; i <= 91; ++i, ++k) // 89
   {


       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
          zeroes_1 = "000";
       else if (j < 100)
          zeroes_1 = "00";
       else if (j < 1000)
          zeroes_1 = "0";
       else
          zeroes_1 = "";

       if (k < 10)
          zeroes_2 = "000";
       else if (k < 100)
          zeroes_2 = "00";
       else if (k < 1000)
          zeroes_2 = "0";
       else
          zeroes_2 = "";

      s.str("");

      s << "convert -fill white -opaque orange R" << zeroes << i << ".png "
        << "S" << zeroes << i << ".png;\n";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }
  


  }  /* |for|  */

  return 0;

}  /* End of |main| definition  */

/* * (1) */

#if 0 

/* ** (2) */

      s << "mogrify -fill violet -opaque orange R" << zeroes << i << ".png;";


/* ** (2) */

      s << "convert -fill black -opaque magenta Q" << zeroes << i << ".png "
        << "R" << zeroes << i << ".png;\n";




/* ** (2) */

      s << "composite rectangles_yellow_frame_magenta_background_trans.png "
        << "P" << zeroes << i << ".png Q" << zeroes << i << ".png;\n";


/* ** (2) */

      s << "convert -fill black -opaque " << dark_green << " P0089.png P0090.png";


/* ** (2) */

      s << "convert -fill blue -opaque cyan "
        << "-opaque " << periwinkle << " "
        << "-fill red "
        << "-opaque " << purple << " "
        << "-opaque " << pink << " "
        << "-opaque " << melon << " "
        << "-fill black -opaque " << green << " "
        << "-fill " << maroon << " -opaque " << cerulean_blue << " "
        << "-fill orange "
        << "-opaque " << emerald << " "
        << "-opaque " << jungle_green << " "
        << "-fill violet "
        << "-opaque " << bittersweet << " "
        << "N" << zeroes << i << ".png A.png;\n"
        << "convert -fill " << dark_green << " -opaque " << maroon << " -opaque white "
        << "A.png P" << zeroes << i << ".png;\n";


/* ** (2) */


      s << "mogrify -fill " << thistle << " -opaque " << maroon << " L" << zeroes << i << ".png;";

      s << "convert -fill white -opaque " << emerald << " " 
        << "-opaque red "
        << "-fill red -opaque cyan " 
        << "-opaque " << green << " "
        << "-opaque " << bittersweet << " "
        << "-opaque " << pink << " "
        << "-fill white -opaque " << periwinkle << " "
        << "-opaque " << cerulean_blue << " "
        << "N" << zeroes << i << ".png P" << zeroes << i << ".png;";

      s << "mogrify -fill black -opaque white L" << zeroes << i << ".png;";

      s << "composite L" << zeroes << i << ".png M" << zeroes << i << ".png "
        << "N" << zeroes << i << ".png;";



      s << "mv Q" << zeroes << i << ".png L" << zeroes << i << ".png;";


      s << "convert -fill white -opaque orange " 
        << "-fill black -opaque " << rose_madder << " "
        << "J" << zeroes << i << ".png L" << zeroes << i << ".png;";

      s << "cp J0081.png J" << zeroes << i << ".png;";

      s << "convert -fill white -opaque cyan " 
        << "-fill black -opaque " << raw_sienna << " "
        << "K" << zeroes << i << ".png L" << zeroes << i << ".png;";


/* ** (2) */

      s << "mogrify -fill " << melon << " -opaque " << maroon << " " 
        << "K" << zeroes << i << ".png;";


      s << "mogrify -fill " << orange << " -opaque " << purple << " " 
        << "K" << zeroes << i << ".png;";

      s << "mogrify -fill " << raw_sienna << " -opaque " << apricot << " " 
        << "K" << zeroes << i << ".png;";



        << "-fill " << bittersweet << " -opaque " << green << " "

      s << "mogrify -fill yellow -opaque " << periwinkle << " " 
        << "-fill " << bittersweet << " -opaque " << green << " "
        << "K" << zeroes << i << ".png;";

      s << "mogrify -fill black -opaque orange -opaque " << rose_madder << " -opaque blue " 
        << "K" << zeroes << i << ".png;";

      s << "composite J0081.png F" << zeroes << i << ".png K" << zeroes << i << ".png;";

      s << "composite J" << zeroes << i << ".png F" << zeroes << i << ".png K" << zeroes << i << ".png;";

      s << "mogrify -fill " << cerulean_blue << " -opaque cyan J" << zeroes << i << ".png;\n";
      s << "convert -fill black -opaque blue "
        << "K" << zeroes << i << ".png A.png;\n" 
        << "convert -fill blue -opaque " << apricot << " " 
        << "-fill black -opaque orange -opaque " << rose_madder << " " 
        << "-fill " << bittersweet << " -opaque " << green << " "
        << "-fill " << yellow << " -opaque " << plum << " "
        << "-fill " << purple << " -opaque " << cyan << " "
        << "A.png L" << zeroes << i << ".png;\n";


/* ** (2) */

       if (i > 0)
          j %= 17;

       if (i > 14 && j == 0)
         background_switch = !background_switch;

      s << "composite vert_mask_trans_right_solid_left_0016.png I" << zeroes << i << ".png A.png;\n"
        << "convert -transparent " << rose_madder << " A.png B.png;\n";

      if (i <= 81)
        s << "composite B.png J" << zeroes << i << ".png K" << zeroes << i << ".png;\n";
      else
        s << "composite B.png J0081.png K" << zeroes << i << ".png;\n";

/* ** (2) */

      s << "mogrify -transparent violet J" << zeroes << i << ".png;\n";


/* ** (2) */

      s << "convert -transparent " << rose_madder << " vert_mask_trans_right_solid_left_" << zeroes_1 << j << ".png A.png;\n"
        << "composite A.png D" << zeroes << i << ".png H" << zeroes << i << ".png;\n"
        << "convert -transparent orange vert_mask_trans_right_solid_left_" << zeroes_1 << j << ".png B.png;\n"
        << "composite B.png D" << zeroes << i << ".png I" << zeroes << i << ".png;\n"
        << "mogrify -transparent orange H" << zeroes << i << ".png;\n"
        << "mogrify -transparent " << rose_madder << " I" << zeroes << i << ".png;\n";

      if (background_switch)
      {
         s << "mogrify -fill " << pink << " -opaque red H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << periwinkle << " -opaque red I" << zeroes << i << ".png;\n";

         s << "mogrify -fill " << bittersweet << " -opaque " << plum << " H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << jungle_green << " -opaque " << plum << " I" << zeroes << i << ".png;\n";

         s << "mogrify -fill " << maroon << " -opaque " << burnt_orange << " H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << melon << " -opaque " << burnt_orange << " I" << zeroes << i << ".png;\n";


      }
      else 
      {

         s << "mogrify -fill " << periwinkle << " -opaque red H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << pink << " -opaque red I" << zeroes << i << ".png;\n";


         s << "mogrify -fill " << jungle_green << " -opaque " << plum << " H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << bittersweet << " -opaque " << plum << " I" << zeroes << i << ".png;\n";

         s << "mogrify -fill " << melon << " -opaque " << burnt_orange << " H" << zeroes << i << ".png;\n"
           << "mogrify -fill " << maroon << " -opaque " << burnt_orange << " I" << zeroes << i << ".png;\n";
      }


      s << "composite H" << zeroes << i << ".png I" << zeroes << i << ".png J" << zeroes << i << ".png;\n";





   for (int i = 81; i <= 89; ++i, ++k)

      s << "composite vert_mask_trans_left_solid_right_0000.png C" << zeroes << i << ".png "
        << "D.png;\n" 
        << "convert -transparent violet D.png E" << zeroes << i << ".png;\n";

      s << "composite E0081.png F" << zeroes << i << ".png G" << zeroes << i << ".png;\n";

/* ** (2) */


      



       if (i >= 17 && j == 0)
          background_switch = !background_switch;


   for (int i = 0; i <= 81; ++i, ++k)

      if (i <= 76)
      {
         s << "convert -fill " << plum << " -opaque cyan "
           << "-fill " << burnt_orange << " -opaque magenta "
           << "Z" << zeroes << i << ".png A.png;\n"
           << "composite A.png vert_mask_" << zeroes_1 << j << ".png "
           << "B.png;\n";

         if (i == 76)
           s << "cp A.png A0076.png;\n";
      }
      else 
         s << "composite A0076.png vert_mask_" << zeroes_1 << j << ".png "
           << "B.png;\n";

      s << "cp B.png C" << zeroes << i << ".png;\n";



       if ((i < 22) || (i > 38 && i < 56))
          goto LOOP_END;


      s << "convert -fill " << salmon << " -opaque red F" << zeroes << i << ".png A.png;\n"
        << "convert -fill red -opaque " << sky_blue << " A.png B.png;\n"
        << "convert -fill " << sky_blue << " -opaque " << salmon << " B.png F" << zeroes << i << ".png;\n";

#endif 


#if 0

/* ** (2) */

      s << "mogrify -fill cyan -opaque red F" << zeroes << i << ".png;";

/* ** (2) */

      s << "convert -transparent " << rose_madder << " -transparent " << goldenrod << " "
        << "vert_mask_solid_all_" << zeroes << i << ".png "
        << "vert_mask_solid_bottom_trans_top_" << zeroes << i << ".png;\n";


/* ** (2) */

      s << "convert -transparent orange -transparent violet "
        << "vert_mask_solid_all_" << zeroes << i << ".png "
        << "vert_mask_solid_top_trans_bottom_" << zeroes << i << ".png;\n";

/* ** (2) */

   for (int i = 0; i <= 16; ++i, ++k)

      s << "convert -fill orange -opaque " << green << " "
        << "-fill violet -opaque " << teal_blue << " "
        << "-fill " << rose_madder << " -opaque cyan "
        << "-fill " << goldenrod << " -opaque magenta " 
        << "vert_mask_" << zeroes << i << ".png "
        << "vert_mask_solid_all_" << zeroes << i << ".png;\n";

/* ** (2) */

       if (i > 4)
          j %= 17;

       if (i >= 17 && j == 0)
          background_switch = !background_switch;

   for (int i = 0; i <= 89; ++i, ++k)

      s << "convert -fill ";

      if (background_switch)
         s << apricot << " ";
      else 
         s << dark_gray << " ";

      s << "-opaque cyan -opaque magenta " 
        << "-fill ";

      if (background_switch)
         s << dark_gray << " ";
      else 
         s << apricot << " ";

      s << "-opaque " << teal_blue << " -opaque " << green << " "
        << "-fill " << emerald << " -opaque " << dark_green << " "
        << "-fill black -opaque orange -opaque violet -opaque " << goldenrod << " "
        << "-opaque " << rose_madder << " B" << zeroes << i << ".png C" << zeroes << i << ".png;\n";
  

/* ** (2) */

   for (int i = 0; i <= 89; ++i, ++k)
      if (i <= 76)
      {
         s << "convert -fill " << dark_green << " -opaque cyan "
           << "-fill " << purple << " -opaque magenta "
           << "Z" << zeroes << i << ".png A.png;\n";

         if (i == 76)
            s << "cp A.png A0076.png;\n";
      }
      else 
         s << "cp A0076.png A.png;\n";

      s << "composite A.png vert_mask_" << zeroes_1 << j << ".png B" << zeroes << i << ".png;\n";
  


/* ** (2) */

      s << "convert -fill " << pink << " -opaque cyan "
        << "-fill " << dark_green << " -opaque magenta Z" << zeroes << i << ".png A.png;\n"
        << "composite A.png "
        << "vert_mask_0016.png X" << zeroes << i << ".png;\n";



/* ** (2) */

   for (int i = 0; k <= 45; ++i, ++j, ++k)
   {

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
          zeroes_1 = "000";
       else if (j < 100)
          zeroes_1 = "00";
       else if (j < 1000)
          zeroes_1 = "0";
       else
          zeroes_1 = "";

       if (k < 10)
          zeroes_2 = "000";
       else if (k < 100)
          zeroes_2 = "00";
       else if (k < 1000)
          zeroes_2 = "0";
       else
          zeroes_2 = "";

      s.str("");

      if (i <= 46)
      {
         s << "convert -transparent violet -transparent " << dark_green << " "
           << "Upper/Final_upper_" << zeroes << i << ".png A.png;\n";
         if (i == 46)
           s << "cp A.png A0046.png;\n";
      }
      else
         s << "cp A0046.png A.png;\n";     

      if (j < 0)
         s << "cp A.png B.png;\n";

      else if (j >= 0 && j <= 42)
      {
        s << "convert -transparent violet -transparent " << dark_green << " "
          << "Middle/Final_middle_" << zeroes_1 << j << ".png B.png;\n";
        if (j == 42)
           s << "cp B.png B0042.png;\n";
      }
      else
         s << "cp B0042.png B.png;\n";

      if (k >= 0 && k <= 45)
      {
        s << "convert -fill magenta -opaque red "
          << "-transparent black -transparent violet -transparent " << dark_green << " "
          << "Lower/Final_lower_" << zeroes_2 << k << ".png C.png;\n";
      }
      else
        s << "cp B.png C.png;\n";

      s << "composite A.png B.png M.png;\n"
        << "composite C.png M.png N.png;\n";
 
      s << "cp N.png Z" << zeroes << i << ".png;\n";




      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      

  }  /* |for|  */
#endif 

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        