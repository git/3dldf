%% L_small.ldf
%% Created by Laurence D. Finston (LDF) Mi 20. Mär 15:41:59 CET 2024

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Loop 1:  L travelling across r1, right side, from right to left.
%% * (1) Perspective projection
  
q100 := q0 shifted by (get_center R[95] - a21);;
q101 := q1 shifted by (get_center R[95] - a21);;

shift q100 (-10dist_horiz, 0);
shift q101 (-10dist_horiz, 0);

message "Loop 1:  L travelling across r1, right side, from right to left.";
message "First figure:";
message "fig_ctr == " & decimal fig_ctr;
  %pause;

for i = 0 upto 52:
  beginfig(fig_ctr);

    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;
    
    draw frame with_pen big_square_pen with_color black; % blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v0;

    draw r0 with_color line_color;
    draw r1 with_color line_color;

    %draw (q101 shifted (i*dist_horiz, 0)) * t1 with_color gray;
    % draw (q100 shifted (i*dist_horiz, 0)) * t1 with_color line_color;

    if i == 11:
      k := 0;
    fi;
    
    if (i >= 11) and (i <= 34):
      fill (C[47-k] * t1)  with_color bulb_color;
      fill (C[71-k] * t1)  with_color bulb_color;
      fill (C[95-k] * t1)  with_color bulb_color;
      fill (C[119-k] * t1) with_color bulb_color;
      fill (C[143-k] * t1) with_color bulb_color; 

      % draw (C[47-k] * t1)  with_pen medium_pen;
      % draw (C[71-k] * t1)  with_pen medium_pen;
      % draw (C[95-k] * t1)  with_pen medium_pen;
      % draw (C[119-k] * t1) with_pen medium_pen;
      % draw (C[143-k] * t1) with_pen medium_pen;
    fi;

    if i >= 11:
      k += 1;
    fi;
    
      % message "fig_ctr == " & decimal fig_ctr;
      % message "k       == " & decimal k;
    
    if (k >= 2) and (k < 26):
      fill ((C[143-k+2]) * t1) with_color bulb_color;
      % draw (C[143-k+2] * t1) with_pen medium_pen;
    fi;

    if (k >= 3) and (k < 27):
      fill ((C[143-k+3]) * t1) with_color bulb_color;
      % draw (C[143-k+3] * t1) with_pen medium_pen;
    fi;

    if (k >= 4) and (k < 28):
      fill ((C[143-k+4]) * t1) with_color bulb_color;
      % draw (C[143-k+4] * t1) with_pen medium_pen;
    fi;
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;
    
    draw frame with_color black; % blue; ;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;


%% * (1) Loop 2:  L on left panel, travelling across r0 from right to left.
%% * (1) Perspective projection

message "Loop 2:  L on left panel, travelling across r0 from right to left.";
message "First figure:";
message "fig_ctr == " & decimal fig_ctr;
  %pause;

for i = 0 upto 38:
  beginfig(fig_ctr);

    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;
    
    draw frame with_pen big_square_pen with_color black; % blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v0;

    draw r0 with_color line_color;
    draw r1 with_color line_color;
    
    %draw (q3 shifted (-i * dist_horiz, 0)) * t0 with_color gray;
    %draw (q2 shifted (-i * dist_horiz, 0)) * t0;

    message "fig_ctr == " & decimal fig_ctr;
    message "i       == " & decimal i;
    
    if (i >= 1) and (i < 25):
      fill (D[47-i+1] * t0) with_color bulb_color;
      fill (D[71-i+1] * t0) with_color bulb_color;
      fill (D[95-i+1] * t0) with_color bulb_color;
      fill (D[119-i+1] * t0) with_color bulb_color;
      fill (D[143-i+1] * t0) with_color bulb_color;

      % draw (D[47-i+1] * t0) with_pen medium_pen;
      % draw (D[71-i+1] * t0) with_pen medium_pen;
      % draw (D[95-i+1] * t0) with_pen medium_pen;
      % draw (D[119-i+1] * t0) with_pen medium_pen;
      % draw (D[143-i+1] * t0) with_pen medium_pen;
    fi;

    if (i >= 2) and (i < 26):
      fill (D[143-i+2] * t0) with_color bulb_color;
      % draw (D[143-i+2] * t0) with_pen medium_pen;
    fi;

    if (i >= 3) and (i < 27):
      fill (D[143-i+3] * t0) with_color bulb_color;
      % draw (D[143-i+3] * t0) with_pen medium_pen;
    fi;

    if (i >= 4) and (i < 28):
      fill (D[143-i+4] * t0) with_color bulb_color;
      % draw (D[143-i+4] * t0) with_pen medium_pen;
    fi;
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;
    
    draw frame with_color black; % blue; ;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;

%% * (1)  

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
