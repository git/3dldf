/* process_files_lower.cxx  */
/* Created by Laurence D. Finston (LDF) Do 28. Mär 22:02:11 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int j = 0;
int k = 0;

string P_zeroes;
string R_zeroes;
string E_zeroes;
string S_zeroes;
string E_zeroes_1;
string N_zeroes;
string T_zeroes;
string S_zeroes_1;

int P_min   = 97;
int P_max   = 149;

int R_min   = 1;
int R_max   = 52;

int E_min   = 1;
int E_max   = 52;

int S_min   = 1;
int S_max   = 51;

int N_min = 1;
int N_max = 52;

int T_min   = 1;
int T_max   = 52;

string P_str   = "P_lower/P_small_";
string R_str   = "R_lower/R_small_lower"; // No `_' after lower in some directories!  LDF 2024.03.29.
string E_str   = "E_lower/E_small_lower";
string S_str   = "S_lower/S_small_lower";
string N_str   = "N_lower/N_small_lower_";
string T_str   = "T_lower/T_small_lower";

int P_ctr   = 97;
int R_ctr   = 0;
int E_ctr   = 0;
int S_ctr   = 0;
int E_ctr_1 = 0;
int N_ctr   = 0;
int T_ctr   = 0;
int S_ctr_1 = 0;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   for (int i = 0;; ++i)
   {

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (P_ctr < 10)
          P_zeroes = "000";
       else if (P_ctr < 100)
          P_zeroes = "00";
       else if (i < 1000)
          P_zeroes = "0";
       else
          P_zeroes = "";

       if (R_ctr < 10)
          R_zeroes = "000";
       else if (R_ctr < 100)
          R_zeroes = "00";
       else if (i < 1000)
          R_zeroes = "0";
       else
          R_zeroes = "";

       if (E_ctr < 10)
          E_zeroes = "000";
       else if (E_ctr < 100)
          E_zeroes = "00";
       else if (i < 1000)
          E_zeroes = "0";
       else
          E_zeroes = "";

       if (S_ctr < 10)
          S_zeroes = "000";
       else if (S_ctr < 100)
          S_zeroes = "00";
       else if (i < 1000)
          S_zeroes = "0";
       else
          S_zeroes = "";

       if (E_ctr_1 < 10)
          E_zeroes_1 = "000";
       else if (E_ctr_1 < 100)
          E_zeroes_1 = "00";
       else if (i < 1000)
          E_zeroes_1 = "0";
       else
          E_zeroes_1 = "";

       if (N_ctr < 10)
          N_zeroes = "000";
       else if (N_ctr < 100)
          N_zeroes = "00";
       else if (i < 1000)
          N_zeroes = "0";
       else
          N_zeroes = "";

       if (T_ctr < 10)
          T_zeroes = "000";
       else if (T_ctr < 100)
          T_zeroes = "00";
       else if (i < 1000)
          T_zeroes = "0";
       else
          T_zeroes = "";

       if (S_ctr_1 < 10)
          S_zeroes_1 = "000";
       else if (S_ctr_1 < 100)
          S_zeroes_1 = "00";
       else if (i < 1000)
          S_zeroes_1 = "0";
       else
          S_zeroes_1 = "";

      if (i == 7)
         R_ctr = R_min;

      if (i == 13)
         E_ctr = E_min;

      if (i == 18)
         S_ctr = S_min;

      if (i == 23)
         E_ctr_1 = E_min;

      if (i == 28)
         N_ctr = N_min;

      if (i == 34)
         T_ctr = T_min;

      if (i == 40)
         S_ctr_1 = S_min;

      s.str("");

      if (P_ctr <= P_max)
         s << "convert -transparent black " << P_str << P_zeroes << P_ctr << ".png "
           << "A.png;\n";
      else
         s << "cp rectangles_yellow_frame_blue.png A.png;\n";

      if (R_ctr > 0 && R_ctr <= R_max)
        s << "convert -transparent black " << R_str << R_zeroes << R_ctr << ".png "
          << "B.png;\n"
          << "composite B.png A.png C.png;\n";
      else
         s << "cp A.png C.png;\n";

      if (E_ctr > 0 && E_ctr <= E_max)
        s << "convert -transparent black " << E_str << E_zeroes << E_ctr << ".png "
          << "D.png;\n"
          << "composite C.png D.png E.png;\n";
      else
         s << "cp C.png E.png;\n";

      if (S_ctr > 0 && S_ctr <= S_max)
        s << "convert -transparent black " << S_str << S_zeroes << S_ctr << ".png "
          << "F.png;\n"
          << "composite E.png F.png G.png;\n";
      else
         s << "cp E.png G.png;\n";

      if (E_ctr_1 > 0 && E_ctr_1 <= E_max)
        s << "convert -transparent black " << E_str << E_zeroes_1 << E_ctr_1 << ".png "
          << "H.png;\n"
          << "composite G.png H.png I.png;\n";
      else
         s << "cp G.png I.png;\n";

      if (N_ctr > 0 && N_ctr <= N_max)
        s << "convert -transparent black " << N_str << N_zeroes << N_ctr << ".png "
          << "J.png;\n"
          << "composite I.png J.png K.png;\n";
      else
         s << "cp I.png K.png;\n";

      if (T_ctr > 0 && T_ctr <= T_max)
        s << "convert -transparent black " << T_str << T_zeroes << T_ctr << ".png "
          << "L.png;\n"
          << "composite K.png L.png M.png;\n";
      else
         s << "cp K.png M.png;\n";

      if (S_ctr_1 > 0 && S_ctr_1 <= S_max)
        s << "convert -transparent black " << S_str << S_zeroes_1 << S_ctr_1 << ".png "
          << "N.png;\n"
          << "composite M.png N.png O.png;\n";
      else
         s << "cp M.png O.png;\n";

      s << "composite O.png presents_0000.png Final_lower_" << zeroes << i << ".png;\n";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      ++P_ctr;

      if (i >= 7)
        ++R_ctr;

      if (i >= 13)
         ++E_ctr;

      if (i >= 18)
         ++S_ctr;

      if (i >= 23)
         ++E_ctr_1;

      if (i >= 28)
         ++N_ctr;

      if (i >= 34)
         ++T_ctr;

      if (i >= 40)
         ++S_ctr_1;


     if (S_ctr_1 > S_max)
        break;


  }  /* |for|  */

  return 0;

}  /* End of |main| definition  */


#if 0 


      ++P_ctr;

      if (i >= 6)
         ++R_ctr;





      if (i >= 25)
         ++N_ctr;

      if (i >= 31)
         ++E_ctr_2;

      if (E_ctr_2 > E_max)
         break;


      if (i == 6)
         R_ctr = R_min;

      if (i == 8)
         E_ctr_1 = E_min;

      if (i == 14)
         S_ctr = S_min;

      if (i == 19)
         E_2_ctr = E_2_min;

      if (i == 25)
         N_ctr = N_min;

      if (i == 31)
         E_ctr_2 = E_min;

      if (P_ctr <= P_max)
         s << "convert -transparent black " << P_str << P_zeroes << P_ctr << ".png A.png;\n";
      else
        s << "cp rectangles_yellow_frame_blue.png A.png;\n";
   
      if (i >= 6 && R_ctr <= R_max)
         s << "convert -transparent black " << R_str << R_zeroes << R_ctr << ".png B.png;\n"
           << "composite A.png B.png C.png;\n";
      else 
        s << "cp A.png C.png;\n";

      if (i >= 8 && E_ctr_1 <= E_max)
         s << "convert -transparent black " << E_str << E_zeroes_1 << E_ctr_1 << ".png D.png;\n"
           << "composite C.png D.png E.png;\n";
      else 
        s << "cp C.png E.png;\n";


      if (i >= 14 && S_ctr <= S_max)
         s << "convert -transparent black " << S_str << S_zeroes << S_ctr << ".png F.png;\n"
           << "composite E.png F.png G.png;\n";
      else 
        s << "cp E.png G.png;\n";

      if (i >= 19 && E_2_ctr <= E_2_max)
         s << "convert -transparent black " << E_2_str << E_2_zeroes << E_2_ctr << ".png H.png;\n"
           << "composite G.png H.png I.png;\n";
      else 
        s << "cp G.png I.png;\n";

      if (i >= 25 && N_ctr <= N_max)
         s << "convert -transparent black " << N_str << N_zeroes << N_ctr << ".png J.png;\n"
           << "composite I.png J.png K.png;\n";
      else 
        s << "cp I.png K.png;\n";

      if (i >= 31 && E_ctr_2 <= E_max)
         s << "convert -transparent black " << E_str << E_zeroes_2 << E_ctr_2 << ".png L.png;\n"
           << "composite K.png L.png M.png;\n";
      else 
        s << "cp K.png M.png;\n";

      s << "composite M.png presents_0000.png Final_lower_" << zeroes << i << ".png;\n";


   for (int i = N_min; i <= N_max; ++i)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       s.str("");

       s << "composite rectangles_yellow_frame_blue.png " << N_str << zeroes << i << ".png "
         << "A.png; "
         << "convert -fill red -opaque white A.png B.png; "
         << "mv B.png " << N_str << zeroes << i << ".png; ";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

   }

   P_ctr   = 34;
   R_ctr   = 29;
   E_ctr   = 23;
   S_ctr   = 16;
   E_2_ctr = 11;
   N_ctr   = 6;
   E_ctr   = 0;





      s << "convert -fill red -opaque white I" << zeroes << i << ".png "
        << "J" << zeroes << i << ".png";

      s << "composite rectangles_yellow_frame_blue.png H" << zeroes << i << ".png "
        << "I" << zeroes_1 << j << ".png";

      s << "composite rectangles_yellow_frame_blue.png G" << zeroes << i << ".png "
        << "I" << zeroes << i << ".png";


      s << "mv H" << zeroes << i << ".png G" << zeroes_1 << j << ".png";
#endif 


#if 0

      s << "composite rectangles_yellow_frame_blue.png F" << zeroes << i << ".png "
        << "G" << zeroes << i << ".png; ";


      s << "cp D" << zeroes << i << ".png E" << zeroes << i << ".png";




      s << "cp R_small_" << zeroes << i << ".png D" << zeroes_1 << j << ".png";

/* ** (2) */

      s << "convert -transparent white presents_" << zeroes << i << ".png A.png; "
        << "convert -transparent white presents_" << zeroes_1 << j << ".png B.png; "
        << "composite A.png B.png C.png; "
        << "composite C.png presents_0000.png D" << zeroes_1 << j << ".png; ";
#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        