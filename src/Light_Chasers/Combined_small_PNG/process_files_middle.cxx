/* process_files_middle.cxx  */
/* Created by Laurence D. Finston (LDF) Do 28. Mär 22:02:11 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int j = 0;
int k = 0;

string F_zeroes;
string I_zeroes;
string N_zeroes_1;
string S_zeroes;
string T_zeroes;
string O_zeroes;
string N_zeroes_2;

int F_min   = 0;
int F_max   = 52;

int I_min   = 1;
int I_max   = 48;

int N_min   = 1;
int N_max   = 52;

int S_min   = 1;
int S_max   = 51;

int T_min = 1;
int T_max = 52;

int O_min   = 1;
int O_max   = 52;

string F_str   = "F_middle/F_small_";
string I_str   = "I_middle/I_small_";
string N_str   = "N_middle/N_small_middle_";
string S_str   = "S_middle/S_small_";
string T_str   = "T_middle/T_small_middle_";
string O_str   = "O_middle/O_small_middle_";

int F_ctr   = 0;
int I_ctr   = 0;
int N_ctr_1 = 0;
int S_ctr   = 0;
int T_ctr   = 0;
int O_ctr   = 0;
int N_ctr_2 = 0;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   for (int i = 0;; ++i)
   {

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (F_ctr < 10)
          F_zeroes = "000";
       else if (F_ctr < 100)
          F_zeroes = "00";
       else if (i < 1000)
          F_zeroes = "0";
       else
          F_zeroes = "";

       if (I_ctr < 10)
          I_zeroes = "000";
       else if (I_ctr < 100)
          I_zeroes = "00";
       else if (i < 1000)
          I_zeroes = "0";
       else
          I_zeroes = "";

       if (N_ctr_1 < 10)
          N_zeroes_1 = "000";
       else if (N_ctr_1 < 100)
          N_zeroes_1 = "00";
       else if (i < 1000)
          N_zeroes_1 = "0";
       else
          N_zeroes_1 = "";

       if (S_ctr < 10)
          S_zeroes = "000";
       else if (S_ctr < 100)
          S_zeroes = "00";
       else if (i < 1000)
          S_zeroes = "0";
       else
          S_zeroes = "";

       if (T_ctr < 10)
          T_zeroes = "000";
       else if (T_ctr < 100)
          T_zeroes = "00";
       else if (i < 1000)
          T_zeroes = "0";
       else
          T_zeroes = "";

       if (O_ctr < 10)
          O_zeroes = "000";
       else if (O_ctr < 100)
          O_zeroes = "00";
       else if (i < 1000)
          O_zeroes = "0";
       else
          O_zeroes = "";

       if (N_ctr_2 < 10)
          N_zeroes_2 = "000";
       else if (N_ctr_2 < 100)
          N_zeroes_2 = "00";
       else if (i < 1000)
          N_zeroes_2 = "0";
       else
          N_zeroes_2 = "";

      s.str("");

      if (i == 6)
         I_ctr = I_min;

      if (i == 8)
         N_ctr_1 = N_min;

      if (i == 14)
         S_ctr = S_min;

      if (i == 19)
         T_ctr = T_min;

      if (i == 25)
         O_ctr = O_min;

      if (i == 31)
         N_ctr_2 = N_min;

      if (F_ctr <= F_max)
         s << "convert -transparent black " << F_str << F_zeroes << F_ctr << ".png A.png;\n";
      else
        s << "cp rectangles_yellow_frame_blue.png A.png;\n";
   
      if (i >= 6 && I_ctr <= I_max)
         s << "convert -transparent black " << I_str << I_zeroes << I_ctr << ".png B.png;\n"
           << "composite A.png B.png C.png;\n";
      else 
        s << "cp A.png C.png;\n";

      if (i >= 8 && N_ctr_1 <= N_max)
         s << "convert -transparent black " << N_str << N_zeroes_1 << N_ctr_1 << ".png D.png;\n"
           << "composite C.png D.png E.png;\n";
      else 
        s << "cp C.png E.png;\n";


      if (i >= 14 && S_ctr <= S_max)
         s << "convert -transparent black " << S_str << S_zeroes << S_ctr << ".png F.png;\n"
           << "composite E.png F.png G.png;\n";
      else 
        s << "cp E.png G.png;\n";

      if (i >= 19 && T_ctr <= T_max)
         s << "convert -transparent black " << T_str << T_zeroes << T_ctr << ".png H.png;\n"
           << "composite G.png H.png I.png;\n";
      else 
        s << "cp G.png I.png;\n";

      if (i >= 25 && O_ctr <= O_max)
         s << "convert -transparent black " << O_str << O_zeroes << O_ctr << ".png J.png;\n"
           << "composite I.png J.png K.png;\n";
      else 
        s << "cp I.png K.png;\n";

      if (i >= 31 && N_ctr_2 <= N_max)
         s << "convert -transparent black " << N_str << N_zeroes_2 << N_ctr_2 << ".png L.png;\n"
           << "composite K.png L.png M.png;\n";
      else 
        s << "cp K.png M.png;\n";

      s << "composite M.png presents_0000.png Final_middle_" << zeroes << i << ".png;\n";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      ++F_ctr;

      if (i >= 6)
         ++I_ctr;

      if (i >= 8)
         ++N_ctr_1;

      if (i >= 14)
         ++S_ctr;

      if (i >= 19)
         ++T_ctr;

      if (i >= 25)
         ++O_ctr;

      if (i >= 31)
         ++N_ctr_2;

      if (N_ctr_2 > N_max)
         break;

  }  /* |for|  */



  return 0;

}  /* End of |main| definition  */


#if 0 

   for (int i = O_min; i <= O_max; ++i)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       s.str("");

       s << "composite rectangles_yellow_frame_blue.png " << O_str << zeroes << i << ".png "
         << "A.png; "
         << "convert -fill red -opaque white A.png B.png; "
         << "mv B.png " << O_str << zeroes << i << ".png; ";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

   }

   F_ctr   = 34;
   I_ctr   = 29;
   N_ctr   = 23;
   S_ctr   = 16;
   T_ctr = 11;
   O_ctr   = 6;
   N_ctr   = 0;





      s << "convert -fill red -opaque white I" << zeroes << i << ".png "
        << "J" << zeroes << i << ".png";

      s << "composite rectangles_yellow_frame_blue.png H" << zeroes << i << ".png "
        << "I" << zeroes_1 << j << ".png";

      s << "composite rectangles_yellow_frame_blue.png G" << zeroes << i << ".png "
        << "I" << zeroes << i << ".png";


      s << "mv H" << zeroes << i << ".png G" << zeroes_1 << j << ".png";
#endif 


#if 0

      s << "composite rectangles_yellow_frame_blue.png F" << zeroes << i << ".png "
        << "G" << zeroes << i << ".png; ";


      s << "cp D" << zeroes << i << ".png E" << zeroes << i << ".png";




      s << "cp I_small_" << zeroes << i << ".png D" << zeroes_1 << j << ".png";

/* ** (2) */

      s << "convert -transparent white presents_" << zeroes << i << ".png A.png; "
        << "convert -transparent white presents_" << zeroes_1 << j << ".png B.png; "
        << "composite A.png B.png C.png; "
        << "composite C.png presents_0000.png D" << zeroes_1 << j << ".png; ";
#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        