/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 29. Mär 07:49:03 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;




int main(int argc, char *argv[])
{
/* ** (2) */

   string white = "white";

   vector<string> color_vector;
   vector<string>::iterator cv_iter;
   vector<string>::iterator cv_iter_1;

   color_vector.push_back(white);
   color_vector.push_back(dandelion);
   color_vector.push_back(wild_strawberry);
   color_vector.push_back(periwinkle);
   color_vector.push_back(lime_green);
   color_vector.push_back(pink);
   color_vector.push_back(green_yellow);
   color_vector.push_back(mauve);

   cv_iter    = color_vector.begin();

   int status = 0;


   stringstream s;

   int j = 0;
   int k = 1;

   j = 177;

   string bg_color_0 = raw_sienna;
   string bg_color_1 = dark_gray;
   string temp_str;

   j = 187;

   for (int i = 1; i <= 186; ++i, ++j, ++k)
   {


       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
          zeroes_1 = "000";
       else if (j < 100)
          zeroes_1 = "00";
       else if (j < 1000)
          zeroes_1 = "0";
       else
          zeroes_1 = "";

      s.str("");

      s << "convert -fill " <<  sepia << " -opaque " << dark_gray << " "
        << "I" << zeroes << i << ".png A.png;\n"
        << "convert -fill " << dark_gray << " -opaque " << raw_sienna << " "
        << "A.png B.png;\n"
        << "convert -fill " << raw_sienna << " -opaque " << sepia << " "
        << "B.png I" << zeroes_1 << j << ".png\n";
 

#if 0 
      if (i > 0 && i % 17 == 0)
      {
          temp_str = bg_color_0;
          bg_color_0 = bg_color_1;
          bg_color_1 = temp_str;
      }

      s << "convert -fill " << bg_color_0 << " -opaque " << green << " " 
        " -opaque " << teal_blue << " " 
        << "-fill " << bg_color_1 << " -opaque magenta -opaque cyan " 
        << "H" << zeroes << i << ".png I" << zeroes << i << ".png;\n";

#endif 

#if 0 
      s << "mogrify -fill " << gray << " -opaque " << teal_blue << " " 
        << "-fill " << raw_sienna << " -opaque magenta " 
        << "I" << zeroes << i << ".png;\n";
#endif 

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

  }  /* |for|  */



  return 0;

}  /* End of |main| definition  */

/* * (1) SAVE!  This does the conversion.  LDF 2024.04.01.   */ 

/* !! TODO:  Put this in a function.  LDF 2024.04.01.  */

#if 0 
      s << "cp C" << zeroes << i << ".png D" << zeroes << i << ".png;\n"
        << "cp C" << zeroes << i << ".png E" << zeroes << i << ".png;\n"
        << "composite vert_mask_solid_bottom_trans_top_" << zeroes_1 << j << ".png "
        << "D" << zeroes << i << ".png "
        << "F" << zeroes << i << ".png;\n"
        << "composite vert_mask_solid_top_trans_bottom_" << zeroes_1 << j << ".png "
        << "E" << zeroes << i << ".png "
        << "G" << zeroes << i << ".png;\n";

      if (i <= 5)
        s << "mogrify -fill " << violet << " -opaque white G" << zeroes << i << ".png;\n";

      s << "mogrify -transparent orange -transparent " << violet << " "
        << "F" << zeroes << i << ".png;\n";

      if (i >= 17)
      {
          if (i % 17 == 0 )
          {
             cv_iter_1 = cv_iter;
             ++cv_iter;
             
             if (cv_iter == color_vector.end())
                cv_iter = color_vector.begin();

          }

         s << "mogrify -fill " << *cv_iter << " -opaque white F" << zeroes << i << ".png;"
           << "mogrify -fill " << *cv_iter_1 << " -opaque white G" << zeroes << i << ".png;";


      }

      s << "mogrify -transparent " << rose_madder << " -transparent " << goldenrod << " "
        << "G" << zeroes << i << ".png;\n"
        << "mogrify -transparent " << rose_madder << " -transparent " << goldenrod << " "
        << "G" << zeroes << i << ".png;\n"
        << "composite F" << zeroes << i << ".png G" << zeroes << i << ".png "
        << "H" << zeroes << i << ".png;\n"
        << "mogrify -fill black -opaque blue H" << zeroes << i << ".png;\n";

#endif 


/* * (1) */



#if 0 


      s << "convert -fill blue -opaque cyan -opaque magenta " 
        << "-fill " << dark_green << " -opaque " << teal_blue << " "
        << "-opaque " << green << " "
        << "C" << zeroes << i << ".png "
        << "D" << zeroes << i << ".png;\n";

      s << "convert -fill white -opaque " << emerald << " "
        << "-opaque " << plum << " "
        << "-opaque " << maroon << " "
        << "-fill black -opaque " << goldenrod << " -opaque orange "
        << "-opaque " << violet << " -opaque " << rose_madder << " "
        << "B" << zeroes << i << ".png "
        << "C" << zeroes << i << ".png;\n";

      s << "convert -fill " << maroon << " -opaque orange "
        << "-fill " << plum << " -opaque cyan "
        << "-fill " << emerald << " -opaque red "
        << "-transparent " << dark_green << " "
        << "-transparent " << violet << " "
        << "Final_all_" << zeroes << i << ".png A.png;\n"
        << "composite A.png "
        << "vert_mask_" << zeroes_1 << j << ".png "
        << "B" << zeroes << i << ".png;\n";

      if (i >= 1 && i <= 5)
        s << "composite vert_mask_trans_top_" << zeroes_1 << j << ".png "
          << "B" << zeroes << i << ".png "
          << "C.png;\n"
          << "mv C.png B" << zeroes << i << ".png;\n";


      s << "composite vert_mask_trans_top_" << zeroes_1 << j << ".png "
        << "B" << zeroes << i << ".png C" << zeroes << i << ".png;\n"
        << "cp B" << zeroes << i << ".png B_backup_" << zeroes << ".png;\n"
        << "mv C" << zeroes << i << ".png B" << zeroes << i << ".png;\n";



      s << "convert -fill " << cornflower_blue << " -opaque magenta "
        << " -fill " << dandelion << " -opaque cyan "
        << "-fill " << plum << " -opaque orange "
        << "-fill " << melon << " -opaque " << violet << " vert_mask_0016.png "
        << "vert_mask_contrasting_dots_all.png;\n";





      s << "convert -fill black -opaque " << violet << " -opaque " << rose_madder << " "
        << "C" << zeroes << i << ".png D" << zeroes << i << ".png;\n";


      s << "mogrify -fill " << rose_madder << " -opaque orange C" << zeroes << i << ".png;\n";

      s << "convert -transparent " << violet << " -transparent " << dark_green << " "
        << "Final_all_" << zeroes << i << ".png A.png;\n"
        << "convert -fill " << rose_madder << " -opaque orange "
        << "-fill " << teal_blue << " -opaque cyan vert_mask_" << zeroes_1 << j << ".png B.png;\n"
        << "composite A.png B.png C" << zeroes << i << ".png;\n";


      s << "mv vert_mask_" << zeroes << i << ".png vert_mask_" << zeroes_1 << j << ".png";


      s << "mv vert_mask_" << zeroes << i << ".png vert_mask_" << zeroes_1 << j << ".png;";
#endif 

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        