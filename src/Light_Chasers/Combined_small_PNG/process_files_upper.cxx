/* process_files_upper.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 22. Mär 12:59:42 CET 2024  */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

string L_zeroes;
string A_zeroes;
string U_zeroes;
string R_zeroes;
string E_zeroes_1;
string N_zeroes;
string C_zeroes;
string E_zeroes_2;


// !! START HERE.  LDF 2024.03.28.  Get max and min values for PNG files.
// Also, make sure rectangles are yellow in all PNG files.

int L_min   = 0;
int L_max   = 52;

int A_min   = 1;
int A_max   = 52;

int U_min   = 1;
int U_max   = 52;

int R_min   = 0;
int R_max   = 51;

int E_min = 1;
int E_max = 51;

int N_min   = 1;
int N_max   = 52;

int C_min   = 1;
int C_max   = 52;

int j = 29;
int k = 0;

string L_str   = "L_upper/L_small_";
string A_str   = "A_upper/A_small_";
string U_str   = "U_upper/U_small_";
string R_str   = "R_upper/R_small_";
string E_str   = "E_upper/E_small_";
string N_str   = "N_upper/N_small_";
string C_str   = "C_upper/C_small_";

int L_ctr   = 0;
int A_ctr   = 0;
int U_ctr   = 0;
int R_ctr   = 0;
int E_ctr_1 = 0;
int N_ctr   = 0;
int C_ctr   = 0;
int E_ctr_2 = 0;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   for (int i = 0;; ++i)
   {

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";


       if (L_ctr < 10)
          L_zeroes = "000";
       else if (L_ctr < 100)
          L_zeroes = "00";
       else if (i < 1000)
          L_zeroes = "0";
       else
          L_zeroes = "";

       if (A_ctr < 10)
          A_zeroes = "000";
       else if (A_ctr < 100)
          A_zeroes = "00";
       else if (i < 1000)
          A_zeroes = "0";
       else
          A_zeroes = "";

       if (U_ctr < 10)
          U_zeroes = "000";
       else if (U_ctr < 100)
          U_zeroes = "00";
       else if (i < 1000)
          U_zeroes = "0";
       else
          U_zeroes = "";

       if (R_ctr < 10)
          R_zeroes = "000";
       else if (R_ctr < 100)
          R_zeroes = "00";
       else if (i < 1000)
          R_zeroes = "0";
       else
          R_zeroes = "";

       if (E_ctr_1 < 10)
          E_zeroes_1 = "000";
       else if (E_ctr_1 < 100)
          E_zeroes_1 = "00";
       else if (i < 1000)
          E_zeroes_1 = "0";
       else
          E_zeroes_1 = "";

       if (N_ctr < 10)
          N_zeroes = "000";
       else if (N_ctr < 100)
          N_zeroes = "00";
       else if (i < 1000)
          N_zeroes = "0";
       else
          N_zeroes = "";

       if (C_ctr < 10)
          C_zeroes = "000";
       else if (C_ctr < 100)
          C_zeroes = "00";
       else if (i < 1000)
          C_zeroes = "0";
       else
          C_zeroes = "";

       if (E_ctr_2 < 10)
          E_zeroes_2 = "000";
       else if (E_ctr_2 < 100)
          E_zeroes_2 = "00";
       else if (i < 1000)
          E_zeroes_2 = "0";
       else
          E_zeroes_2 = "";

      s.str("");

      if (i == 6)
         A_ctr = A_min;

      if (i == 12)
         U_ctr = U_min;

      if (i == 18)
         R_ctr = R_min;

      if (i == 24)
         E_ctr_1 = E_min;

      if (i == 29)
         N_ctr = N_min;

      if (i == 35)
         C_ctr = C_min;

      if (i == 41)
         E_ctr_2 = E_min;

      if (L_ctr <= L_max)
         s << "convert -transparent black " << L_str << L_zeroes << L_ctr << ".png A.png;\n";
      else
        s << "cp rectangles_yellow_frame_blue.png A.png;\n";
   
      if (i >= 6 && A_ctr <= A_max)
         s << "convert -transparent black " << A_str << A_zeroes << A_ctr << ".png B.png;\n"
           << "composite A.png B.png C.png;\n";
      else 
        s << "cp A.png C.png;\n";

      if (i >= 12 && U_ctr <= U_max)
         s << "convert -transparent black " << U_str << U_zeroes << U_ctr << ".png D.png;\n"
           << "composite C.png D.png E.png;\n";
      else 
        s << "cp C.png E.png;\n";

      if (i >= 18 && R_ctr <= R_max)
         s << "convert -transparent black " << R_str << R_zeroes << R_ctr << ".png F.png;\n"
           << "composite E.png F.png G.png;\n";
      else 
        s << "cp E.png G.png;\n";

      if (i >= 24 && E_ctr_1 <= E_max)
         s << "convert -transparent black " << E_str << E_zeroes_1 << E_ctr_1 << ".png H.png;\n"
           << "composite G.png H.png I.png;\n";
      else 
        s << "cp G.png I.png;\n";

      if (i >= 29 && N_ctr <= N_max)
         s << "convert -transparent black " << N_str << N_zeroes << N_ctr << ".png J.png;\n"
           << "composite I.png J.png K.png;\n";
      else 
        s << "cp I.png K.png;\n";

      if (i >= 35 && C_ctr <= C_max)
         s << "convert -transparent black " << C_str << C_zeroes << C_ctr << ".png L.png;\n"
           << "composite K.png L.png M.png;\n";
      else 
        s << "cp K.png M.png;\n";

      if (i >= 41 && E_ctr_2 <= E_max)
         s << "convert -transparent black " << E_str << E_zeroes_2 << E_ctr_2 << ".png N.png;\n"
           << "composite M.png N.png O.png;\n";
      else 
        s << "cp M.png O.png;\n";

      s << "composite O.png presents_0000.png Final_" << zeroes << i << ".png;\n";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      ++L_ctr;

      if (i >= 6)
         ++A_ctr;

      if (i >= 12)
         ++U_ctr;

      if (i >= 18)
         ++R_ctr;

      if (i >= 24)
         ++E_ctr_1;

      if (i >= 29)
         ++N_ctr;

      if (i >= 35)
         ++C_ctr;

      if (i >= 41)
         ++E_ctr_2;

      if (E_ctr_2 > E_max)
         break;
#if 0 
      ++U_ctr;
      ++R_ctr;
      ++E_ctr_1;
      ++N_ctr;
      ++C_ctr;
      ++E_ctr_2;
#endif 

  }  /* |for|  */



  return 0;

}  /* End of |main| definition  */


#if 0 

   for (int i = N_min; i <= N_max; ++i)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       s.str("");

       s << "composite rectangles_yellow_frame_blue.png " << N_str << zeroes << i << ".png "
         << "A.png; "
         << "convert -fill red -opaque white A.png B.png; "
         << "mv B.png " << N_str << zeroes << i << ".png; ";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

   }

   L_ctr   = 34;
   A_ctr   = 29;
   U_ctr   = 23;
   R_ctr   = 16;
   E_ctr_1 = 11;
   N_ctr   = 6;
   C_ctr   = 0;
   E_ctr_2 = 0;





      s << "convert -fill red -opaque white I" << zeroes << i << ".png "
        << "J" << zeroes << i << ".png";

      s << "composite rectangles_yellow_frame_blue.png H" << zeroes << i << ".png "
        << "I" << zeroes_1 << j << ".png";

      s << "composite rectangles_yellow_frame_blue.png G" << zeroes << i << ".png "
        << "I" << zeroes << i << ".png";


      s << "mv H" << zeroes << i << ".png G" << zeroes_1 << j << ".png";
#endif 


#if 0

      s << "composite rectangles_yellow_frame_blue.png F" << zeroes << i << ".png "
        << "G" << zeroes << i << ".png; ";


      s << "cp D" << zeroes << i << ".png E" << zeroes << i << ".png";




      s << "cp A_small_" << zeroes << i << ".png D" << zeroes_1 << j << ".png";

/* ** (2) */

      s << "convert -transparent white presents_" << zeroes << i << ".png A.png; "
        << "convert -transparent white presents_" << zeroes_1 << j << ".png B.png; "
        << "composite A.png B.png C.png; "
        << "composite C.png presents_0000.png D" << zeroes_1 << j << ".png; ";
#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        