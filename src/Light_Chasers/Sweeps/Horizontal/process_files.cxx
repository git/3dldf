/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Mi 27. Mär 07:25:54 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;
   string zeroes_1;
   string zeroes_2;

   int j = 25;
   int k = 0;

   for (int i = 0; i <= 23; ++i, ++j)
   {

       j = i % 25;

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

      s.str("");

// !! START HERE:  LDF 2024.04.01.  Change the sweep so that the colors alternate
// instead of the cycle starting again from the beginning.

#if 0 

      s << "convert -transparent orange -transparent " << violet << " "
        << "-fill " <<  lime_green << " -opaque white "
        << "-fill " << yellow << " -opaque " << sky_blue << " "
        << "-fill " << cornflower_blue << " -opaque " << apricot << " "
        << "E" << zeroes << i << ".png G" << zeroes << i << ".png;\n"
        << "convert -transparent " << goldenrod << " -transparent " << pink << " " 
        << "F" << zeroes << i << ".png H" << zeroes << i << ".png;\n"
        << "composite G" << zeroes << i << ".png H" << zeroes << i << ".png "
        << "I" << zeroes << i << ".png;\n";

      s << "cp B" << zeroes << i << ".png C.png;\n"
        << "cp B" << zeroes << i << ".png D.png;\n"
        << "composite sweep_horiz_solid_trans_A_" << zeroes_1 << j << ".png C.png "
        << "E" << zeroes << i << ".png;\n"
        << "composite sweep_horiz_solid_trans_B_" << zeroes_1 << j << ".png D.png "
        << "F" << zeroes << i << ".png;\n";


      s << "convert -transparent " << pink << " -transparent " << goldenrod << " "
        << "sweep_horiz_solid_both_" << zeroes << i << ".png "
        << "sweep_horiz_solid_trans_A_" << zeroes << i << ".png;\n"
        << "convert -transparent orange -transparent " << violet << " " 
        << "sweep_horiz_solid_both_" << zeroes << i << ".png "
        << "sweep_horiz_solid_trans_B_" << zeroes << i << ".png;\n";





      s << "convert -transparent " << pink << " -transparent orange "
        << "sweep_horiz_solid_both_" << zeroes << i << ".png "
        << "sweep_horiz_solid_right_trans_left_" << zeroes << i << ".png;\n"
        << "convert -transparent " << goldenrod << " -transparent " << violet << " " 
        << "sweep_horiz_solid_both_" << zeroes << i << ".png "
        << "sweep_horiz_solid_left_trans_right_" << zeroes << i << ".png;\n";


#endif 



      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }
     


  }  /* |for|  */



  return 0;

}  /* End of |main| definition  */


#if 0

      s << "convert -fill black -opaque " << pink << " " 
        << "-opaque " << goldenrod << " -opaque " << purple << " "
        << "-opaque orange -opaque " << dandelion << " "
        << "-fill white -opaque red "
        << "-fill " << sky_blue << " -opaque " << periwinkle << " "
        << "-fill " << purple << " -opaque cyan "
        << "-fill " << dark_green << " -opaque " << green << " "
        << "A" << zeroes << i << ".png "
        << "B" << zeroes << i << ".png;\n";


      s << "composite letters_trans_" << zeroes << i << ".png "
        << "sweep_horiz_on_contrasting_colors_" << zeroes_1 << j << ".png "
        << "A" << zeroes << i << ".png;\n";


      s << "convert -fill black -opaque blue -transparent " << violet << " "
        << "-transparent " << dark_green << " "
        << "-fill " << periwinkle << " -opaque cyan "
        << "-fill " << apricot << " -opaque orange "
        << "Final_all_" << zeroes << i << ".png "
        << "letters_trans_" << zeroes << i << ".png;\n";

      s << "cp presents_" << zeroes << i << ".png sweep_horiz_" << zeroes_1 << j << ".png;\n"
        << "convert -fill " << goldenrod << " -opaque magenta " << " presents_" << zeroes << i << ".png "
        << "sweep_horiz_solid_right_" << zeroes_1 << j << ".png;\n"
        << "convert -fill " << pink << " -opaque cyan " << " presents_" << zeroes << i << ".png "
        << "sweep_horiz_solid_left_" << zeroes_1 << j << ".png;\n"
        << "convert -fill " << pink << " -opaque cyan -fill " << goldenrod << " -opaque magenta " 
        << " presents_" << zeroes << i << ".png "
        << "sweep_horiz_solid_both_" << zeroes_1 << j << ".png;\n";

#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        