/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) So 31. Mär 14:46:56 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;
   string zeroes_1;
   string zeroes_2;

   int k = 0;

#if 0 
   for (int i = 39; i >= 0; --i, ++j) 

   for (int i = 1; i <= 39; i+=2, ++j) 
#endif 

   int j = 21;

   for (int i = 19; i >= 0; --i, ++j) 
   {

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       s.str("");

       s << "cp sweep_diagonal_stripes_cyan_over_white_on_black_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_cyan_over_white_on_black_" << zeroes_1 << j << ".png;\n"
         << "cp sweep_diagonal_stripes_white_on_black_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_white_on_black_" << zeroes_1 << j << ".png;\n"
         << "cp sweep_diagonal_stripes_cyan_on_black_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_cyan_on_black_" << zeroes_1 << j << ".png;\n";

       cerr << "s.str() == " << s.str() << endl;

       status = system(s.str().c_str());

       cerr << "`status' == " << status << endl;

       if (status != 0)
       {
           exit(1);
       }

  }  /* |for|  */

  return 0;

}  /* End of |main| definition  */


/* * (1) */



#if 0 

/* ** (2) */

       s << "convert -transparent black "
         << "sweep_diagonal_stripes_cyan_on_black_" << zeroes << i << ".png A.png;\n"
         << "composite A.png sweep_diagonal_stripes_white_on_black_0020.png "
         << "sweep_diagonal_stripes_cyan_over_white_on_black_" << zeroes << i << ".png;\n";


/* ** (2) */

       s << "convert -transparent black "
         << "sweep_diagonal_stripes_cyan_on_black_" << zeroes << i << ".png A" << zeroes << i << ".png;\n"
         << "convert -transparent black "
         << "sweep_diagonal_stripes_white_on_black_" << zeroes << i << ".png "
         << "B" << zeroes << i << ".png;\n"
         << "composite A" << zeroes << i << ".png B" << zeroes << i << ".png C" << zeroes << i << ".png;\n"
         << "composite C" << zeroes << i << ".png frame_blue_rectangles_yellow_background_black.png "
         << "D" << zeroes << i << ".png;\n";



/* ** (2) */

       s << "convert -fill cyan -opaque white "
         << "sweep_diagonal_stripes_white_on_black_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_cyan_on_black_" << zeroes << i << ".png;\n";

/* ** (2) */

   for (int i = 19; i >= 0; --i, ++j) 

       s << "mv sweep_diagonal_stripes_white_on_black_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_white_on_black_" << zeroes_1 << j << ".png;\n";



/* ** (2) */

   for (int i = 1; i <= 39; i+=2, ++j) 

       s << "convert -fill white -opaque " << green << " -opaque red " 
         << "-fill black -opaque magenta -opaque cyan -opaque " << goldenrod << " " 
         << " -opaque " << dark_green << " -opaque " << pink << " " 
         << "-opaque " << teal_blue << " -opaque " << sky_blue << " " 
         << "-opaque " << plum << " -opaque " << rose_madder << " -opaque orange "
         << "sweep_diagonal_stripes_original_on_contrasting_colors_" << zeroes << i << ".png "
         << "sweep_diagonal_stripes_white_on_black_" << zeroes_1 << j << ".png;\n";

/* ** (2) */

       s << "convert -transparent orange -transparent violet " 
         << "sweep_diagonal_stripes_original_colors_" << zeroes << i << ".png "
         << "A.png;\n"
         << "composite A.png sweep_diagonal_stripes_contrasting_colors_all.png "
         << "sweep_diagonal_stripes_original_on_contrasting_colors_" << zeroes << i << ".png;\n";


/* ** (2) */

       s << "convert -transparent " << goldenrod << " "
         << "-transparent " << green << " "
         << "-transparent red " 
         << "-transparent " << teal_blue << " " 
         << "sweep_diagonal_original_colors_" << zeroes << i << ".png A.png;\n"
         << "composite A.png sweep_diagonal_stripes_original_colors_all.png "
         << "sweep_diagonal_stripes_original_colors_" << zeroes << i << ".png;\n";

/* ** (2) */

       s << "convert "
         // left panel
         << "-fill " << rose_madder << " -opaque " << goldenrod << " "
         << "-fill " << sky_blue << " -opaque magenta "
         << "-fill " << pink << " -opaque " << green << " "
         // right panel
         << "-fill " << dark_green << " -opaque red " 
         << "-fill " << plum << " -opaque cyan " 
         << "-fill orange " << " -opaque " << teal_blue << " " 
         << "sweep_diagonal_stripes_original_colors_all.png "
         << "sweep_diagonal_stripes_contrasting_colors_all.png;\n";


/* ** (2) */

// !! START HERE LDF 2024.04.02. 
convert -flip sweep_diagonal_original_on_contrasting_colors_reversed_0025.png A.png
convert -flop sweep_diagonal_original_on_contrasting_colors_reversed_0025.png B.png


/* ** (2) */

       s << "cp sweep_diagonal_original_on_contrasting_colors_" <<  zeroes << i << ".png "
         << "sweep_diagonal_original_on_contrasting_colors_reversed_" <<  zeroes_1 << j << ".png;\n";

       s << "mogrify -fill pink -opaque orange "
         << "sweep_diagonal_original_on_contrasting_colors_" <<  zeroes << i << ".png;\n";


       s << "convert -fill " << rose_madder << " -opaque " << green << " "
         << "-fill " << cerulean_blue << " -opaque " << goldenrod << " "
         << "-fill cyan -opaque red "
         << "-fill pink -opaque " << teal_blue << " "
         << "sweep_diagonal_original_colors_" << zeroes << i << ".png "
         << "sweep_diagonal_contrasting_colors_" << zeroes << i << ".png\n";

       s << "convert -transparent violet -transparent orange "
         << "sweep_diagonal_contrasting_colors_" << zeroes << i << ".png A.png;\n"
         << "composite A.png sweep_diagonal_original_colors_all.png "
         << "sweep_diagonal_contrasting_on_original_colors_" << zeroes << i << ".png;\n";


       s << "convert -fill " << rose_madder << " -opaque " << green << " "
         << "-fill " << cerulean_blue << " -opaque " << goldenrod << " "
         << "-fill cyan " << "-opaque red " 
         << "-fill orange " << "-opaque " << teal_blue << " "
         << "sweep_diagonal_original_colors_all.png sweep_diagonal_contrasting_colors_all.png;\n";


/* ** (2) */

       /* The "C" files needed to be processed in GIMP, because I'd 
          used cyan for the dots on both the left and right panels.
          Instead, I could have generated the files again with 3DLDF,
          but I didn't bother.
          LDF 2024.04.02.  */

       s << "convert -transparent orange -transparent violet " 
         << "presents_" << zeroes << i << ".png A.png;\n" 
         << "composite A.png sweep_diagonal_original_colors_all.png "
         << "B.png;\n" 
         << "convert -fill orange -opaque " << periwinkle << " "
         << "-fill violet -opaque " << pink << " "
         << "-fill violet -opaque " << cyan << " "
         << "B.png C" << zeroes << i << ".png;\n";

/* ** (2) */


       s << "cp presents_" << zeroes << i << ".png presents_" << zeroes_1 << j << ".png;\n";

       s << "convert -transparent orange -transparent violet "
         << "green_and_red_dots_" << zeroes << i << ".png A.png;\n"
         << "composite A.png dark_green_and_cyan_dots.png "
         << "green_and_red_dots_on_contrasting_dots_" << zeroes << i << ".png;\n";

      s << "cp green_and_red_dots_" << zeroes << i << ".png "
        << "green_and_red_dots_reversed_" << zeroes_1 << j << ".png;\n";


      s << "mogrify -fill orange -opaque " << dark_green << " -fill " << violet << " -opaque cyan "
        << "green_and_red_dots_" << zeroes << i << ".png;";

      s << "cp presents_" << zeroes << i << ".png green_and_red_dots_" << zeroes << i << ".png;";

       s << "convert -transparent orange -transparent violet presents_" << zeroes << i << ".png A.png;\n"
         << "composite A.png green_and_red_dots.png green_and_red_dots_" << zeroes_1 << j << ".png;\n";


      s << "convert -fill " << dark_green << " -opaque " << green << " "
        << "-fill cyan -opaque red presents_0085.png dark_green_and_cyan_dots.png;\n"
        << "convert -fill " << green << " -opaque " << dark_green << " "
        << "-fill red -opaque cyan presents_0085.png green_and_red_dots.png;\n";

#endif




/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        