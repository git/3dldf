/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 28. Mär 16:46:26 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

int convert(int start, int end, int left_val);
int rename(int start, int end, int skip_gt, int skip_lt, string filename);

// vector<string> color_vector;

int main(int argc, char *argv[])
{
/* ** (2) */

   string zeroes;
   string zeroes_1;

   stringstream s;
   int status = 0;

   int j = 0;

   for (int i = 0; i <= 53; ++i, ++j)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

      s.str("");

      s << "mv B" << zeroes << i << ".png U_small_" << zeroes << i << ".png";

#if 0 
      s << "composite rectangles_yellow_frame_blue.png U_small_" << zeroes << i << ".png A.png; "
        << "convert -fill red -opaque white A.png B" << zeroes << i << ".png";
#endif 
      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }
     
  }  /* |for|  */

  return 0;

#if 0 
#if 0 // 1
   convert(61, 64, 97);
#else
   rename(36, 125, 64, 101, string("U_small_"));
#endif 
#endif 

   return 0;

}  /* End of |main| definition  */

/* ** (2) convert */


int convert(int start, int end, int left_val)
{

   int status = 0;

   stringstream s;

   string zeroes;
   string zeroes_1;

   int j = left_val;

   for (int i = start; i <= end; ++i, ++j)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

      s.str("");

      s << "convert -transparent black U_presents_" << zeroes << i << ".png A.png; "
        << "convert -transparent black U_presents_" << zeroes_1 << j << ".png B.png; "
        << "composite A.png B.png C.png; "
        << "composite C.png U_presents_0000.png D" << zeroes << i << ".png";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }
     
  }  /* |for|  */

  return 0;

}  /* |convert|  */


/* ** (2) rename */

int rename(int start, int end, int skip_gt, int skip_lt, string filename)
{

   int status = 0;

   stringstream s;

   string zeroes;
   string zeroes_1;

   int j = 0;

   for (int i = start; i <= end; ++i)
   {
       if ((i > skip_gt) && (i < skip_lt))
          continue;

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

      s.str("");

      s << "cp U_presents_" << zeroes << i << ".png " << filename << zeroes_1 << j << ".png";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }
     

      ++j;

  }  /* |for|  */

  return 0;

} /* |rename|  */

/* ** (2) */

/* * (1) */

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        