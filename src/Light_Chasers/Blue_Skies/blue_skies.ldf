%% blue_skies.ldf
%% Created by Laurence D. Finston (LDF) Do 4. Apr 17:14:08 CEST 2024U

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


input "plainldf.lmc";

medium_pen := pencircle scaled (.5mm, .5mm, .5mm);

pen big_dot_pen;	

big_dot_pen := pencircle scaled (6mm, 6mm, 6mm);

verbatim_tex   "\font\TINYrm=cmr6 scaled 3500"
             & "\font\Tinyrm=cmr10 scaled 3000"
             & "\font\smallrm=cmr10 scaled 5000"
             & "\font\mediumrm=cmr10 scaled 8000"
             & "\font\largerm=cmr10 scaled 11000";


% & "\font\Largerm=cmr10 scaled 16000"
%            & "\font\Largeonerm=cmr10 scaled 22000"
%            & "\font\Largetworm=cmr10 scaled 24000";


boolean do_png;
boolean do_black;
boolean do_labels;
boolean do_fill;
boolean do_incremental;
boolean do_outlines;
boolean do_fill_letters;

do_fill_letters := true; % false; % 
do_outlines := true; % false; % 

do_incremental := true; % false; % 

do_black := false; % true; % 
do_png   := false; % true; % 

if false: % true: %
  do_black := true;
  do_png   := true;
fi;

if do_black:
  do_fill_letters := false;
fi;

do_labels := true; % false; % 

do_fill := false; % true; % 

if do_png:
  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%4c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";";
  do_labels := false;
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";dotlabeldiam:=4mm;";
fi;

%% * (1)

point p[];
point a[];

path q[];

rectangle r[];
rectangle R[];
rectangle L[];
rectangle M[];

transform t[];

circle c[];
circle C[];
circle D[];

picture v[];
picture letter_picture[];
picture dot_picture[];

string s;

numeric frame_wd;
numeric frame_ht;

dot_pen := pencircle scaled (4mm, 4mm, 4mm);

color line_color;
line_color := black;

color rectangle_color;

rectangle_color := black;

color bulb_color;

bulb_color := red;

if do_black:
  do_labels       := false;
  bulb_color      := white;
  line_color      := yellow;
  do_outlines     := false;
  rectangle_color := yellow;
fi;

%% * (1)

%% (* (/ 300 16.0) 9) 

frame_wd := 600mm;
frame_ht := 337.5mm;

frame_wd += 8mm;
frame_ht += 8mm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));

for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

for i = 0 upto 3:
  p[i+4] := mediate(get_point (i) frame, get_point ((i+1) mod 4) frame);
endfor;

p8 := get_center frame;

%% * (1)

pickup medium_pen;

big_pen := pencircle scaled (1.5mm, 1.5mm, 1.5mm);
big_square_pen := pensquare scaled (1.5mm, 1.5mm, 1.5mm);

Big_pen := pencircle scaled (2mm, 2mm, 2mm);

%% * (1)

numeric cv_size;

%% * (1) Fig. 0

fig_ctr    := 0;
circle_ctr := 0;

beginfig(fig_ctr);
  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  draw frame with_pen big_square_pen with_color blue;
endfig with_projection parallel_x_y;
fig_ctr += 1;  

%% * (1) Set up panels.

focus f;
set f with_position (0, 5, -10) with_direction (0, 5, 10) with_distance 20;

pickup big_pen;

message "Set up panels.";
message "fig_ctr == " & decimal fig_ctr;
%pause;

boolean save_do_labels;
save_do_labels := do_labels;
do_labels := false;

beginfig(fig_ctr);
  if do_black:
    fill frame with_color black;
  elseif do_png:
      fill frame with_color white;
  fi;
  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  picture_shift_val := 55;
  
  %dotlabel.bot("{\smallrm origin}", origin);
  shift current_picture (0, 0, picture_shift_val);
  output current_picture with_focus f;
  clear current_picture;

  t0 := identity rotated (90, -15);
  
  r0 := (unit_rectangle scaled (100, 0, 80)) transformed t0;

  for i = 0 upto 3:
    p[9+i] := get_point (i) r0;
  endfor;
  
  if do_labels:
    dotlabel.llft("{\smallrm p9}", p9);
    dotlabel.bot("{\smallrm p10}", p10);
    dotlabel.top("{\smallrm p11}", p11);
    dotlabel.top("{\smallrm p12}", p12);
  fi;

  if do_black:
    fill r0 with_color violet;
    draw r0 with_color yellow;
  else:
    draw r0;
  fi;

  t2 := identity rotated_around (p10, p11) -150;
  t1 := t0 * t2;

  r1 := (unit_rectangle scaled (100, 0, 80)) transformed t1;

  if do_black:
    fill r1 with_color dark_green;
    draw r1 with_color yellow;
  else:
    draw r1;
  fi;

  p13 := get_point 0 r1;
  p14 := get_point 3 r1;
  

  if do_labels:
    dotlabel.bot("{\smallrm p13}", p13);
    dotlabel.top("{\smallrm p14}", p14);
  fi;

  j := 15;
  for i = 0 upto 23:
    p[j] := mediate(p9, p10, i/24);

    if do_labels:
      s := "{\TINYrm " & decimal j & "}";
      dotlabel.bot(s, p[j]);
    fi;
    p[j+1] := mediate(p12, p11, i/24);

    if do_labels:
      s := "{\TINYrm " & decimal (j+1) & "}";
      dotlabel.top(s, p[j+1]);
    fi;

    %draw p[j] -- p[j+1] with_pen medium_pen with_color dark_gray;
    
    p[j+2] := mediate(p10, p13, i/24);

    if do_labels:
      s := "{\TINYrm " & decimal (j+2) & "}";
      dotlabel.bot(s, p[j+2]);
    fi;
    
    p[j+3] := mediate(p11, p14, i/24);

    if do_labels:
      s := "{\TINYrm " & decimal (j+3) & "}";
      dotlabel.top(s, p[j+3]);
    fi;

    %draw p[j+2] -- p[j+3] with_pen medium_pen with_color dark_gray;

    j += 4;
  endfor;

  for i = 0 upto 15:
    p[j] := mediate(p9, p12, i/16);

    if do_labels:
      s := "{\TINYrm " & decimal (j) & "}";
      dotlabel.lft(s, p[j]);
    fi;

    p[j+1] := mediate(p10, p11, i/16);

    if do_labels:
      s := "{\TINYrm " & decimal (j+1) & "}";
      dotlabel.lrt(s, p[j+1]);
    fi;

    p[j+2] := mediate(p13, p14, i/16);

    if do_labels:
      s := "{\TINYrm " & decimal (j+2) & "}";
      dotlabel.rt(s, p[j+2]);
    fi;

    %draw p[j] -- p[j+1] -- p[j+2] with_pen medium_pen with_color gray;
    
    j += 3;
  endfor;

  a := magnitude (p115 - p10);
  %message "a == " & decimal a;

  b := magnitude (p25 - p21);
  %message "b == " & decimal b;

  %message "j == " & decimal j;
  p159 := (p138 -- p139) intersection_point (p63 -- p64);

  if do_labels:
    dotlabel.lrt("{\Tinyrm p159}", p159);
  fi;
  
  c0 := (unit_circle scaled (5, 0, 5)) transformed t0;

  shift c0 by p159;

  %draw c0 with_color blue;

  p160 := p159 * t2;

  if do_labels:
    %dotlabel.lrt("{\Tinyrm p160}", p160);
  fi;

  c1 := unit_circle scaled (15, 0, 15);
  c1 *= t1;
  shift c1 by (p160 - get_center c1);

  %draw c1 with_color red;

  p161 := get_normal c1;

  %show unit_vector p161;

  p162 := get_normal r1;
  %show unit_vector p162;
  
  %pause;
  
  shift current_picture (-47.5, 5, picture_shift_val);
  %clip current_picture to frame;
  output current_picture with_focus f;
  clear current_picture;

endfig with_projection parallel_x_y;
fig_ctr += 1;  

do_labels := save_do_labels;

%% * (1) Parallel projection onto plane of r1 (right side)

beginfig(fig_ctr);

  message "Parallel projection onto plane of r1 (right side).";
  message "fig_ctr == " & decimal fig_ctr;
  %pause;

  
%% ** (2)
  
  if do_png:
    fill frame with_color white;
  fi;

  if do_labels:
    label.lrt("{\smallrm r1:  Right side}", p3 shifted (0, -2));
  fi;
  
  draw frame with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  t3 := inverse t1;

  r3 := r1 * t3;
  c3 := c1 * t3;
  
  draw r3;
  %draw c3 with_color red;

  a10 := p10 * t3;
  a11 := p11 * t3;
  a13 := p13 * t3;
  a14 := p14 * t3;

  for i = 0 upto 23:
    draw mediate(a10, a13, i/24) -- mediate(a11, a14, i/24) with_pen medium_pen with_color dark_gray;
  endfor;

  for i = 0 upto 15:
    draw mediate(a10, a11, i/16) -- mediate(a13, a14, i/16) with_pen medium_pen with_color dark_gray;
  endfor;

  if do_labels:
    dotlabel.lrt("{\smallrm a10}", a10);
    dotlabel.urt("{\smallrm a13}", a13);

    dotlabel.llft("{\smallrm a11}", a11);
    dotlabel.ulft("{\smallrm a14}", a14);
  fi;


  a15 := mediate(a11, a14, 1/24);
  a16 := mediate(a11, a10, 1/16);
  a17 := a16 shifted by (a15 - a11);

  % drawdot a15 with_color green with_pen dot_pen;
  % drawdot a16 with_color blue with_pen dot_pen;
  % drawdot a17 with_color red with_pen dot_pen;

  R0 := get_rectangle (a11 -- a15 -- a17 -- a16 -- cycle);
  %draw R0 with_color red;

  if do_labels:
    label("{\TINYrm 0}", get_center R0);
  fi;

  for i = 1 upto 23:
    R[i] := R[i-1] shifted by (a15 - a11);
    if do_labels:
      s := "{\TINYrm " & decimal i & "}";
      label(s, get_center R[i]);
    fi;
  endfor;

  j := 24;

  for k = 0 upto 14:
    for i = 0 upto 23:
      R[j] := R[i+24k] shifted by (a16 - a11);
      if do_labels:
	s := "{\TINYrm " & decimal j & "}";
	label(s, get_center R[j]);
      fi;
      j += 1;
    endfor;
  endfor;
  
%% ** (2)

  for i = 0 upto 383: 
    C[i] := (unit_circle scaled (1.7, 0, 1.75)) shifted by get_center R[i];
  endfor;

  % if not do_black:
  %   for i = 0 upto 143: % 383: %  
  %     draw C[i] with_pen medium_pen with_color blue;
  %   endfor;
  % fi;
  
  %fill C107 with_color red;

%% ** (2)

  rotate current_picture (0, 0, 180);

  v1 := current_picture;

  current_picture += letter_picture1;
  
  scale current_picture (.4, .4, .4);

endfig with_projection parallel_x_z;
fig_ctr += 1;

%% * (1) Parallel projection onto plane of r0 (left side)

beginfig(fig_ctr);

%% ** (2)

  message "Parallel projection onto plane of r0 (left side)";
  message "fig_ctr == " & decimal fig_ctr;
  %pause;
  
  if do_black:
    fill frame with_color black;
    line_color := yellow;
  else:
    line_color := black;
    if do_png:
      fill frame with_color white;
    fi;
  fi;

  if do_labels:
    label.lrt("{\smallrm r0:  Left side}", p3 shifted (0, -2));
  fi;
  
  draw frame with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2)  

  t4 := inverse t0;

  r2 := r0 * t4;
  c2 := c0 * t4;
  
  draw r2 with_color line_color;
  %draw c2 with_color red;

  a9  := p9 * t4;
  a12 := p12 * t4;

  if not do_black:  
    for i = 0 upto 23:
      draw mediate(a9, a10, i/24) -- mediate(a12, a11, i/24) with_pen medium_pen with_color dark_gray;
    endfor;
    
    for i = 0 upto 15:
      draw mediate(a9, a12, i/16) -- mediate(a10, a11, i/16) with_pen medium_pen with_color dark_gray;
    endfor;
  fi;
  
  if do_labels:
    dotlabel.bot("{\smallrm a9}", a9);
    dotlabel.bot("{\smallrm a10}", a10);
    dotlabel.top("{\smallrm a12}", a12);
    dotlabel.top("{\smallrm a11}", a11);
  fi;

  a18 := mediate(a12, a11, 1/24);
  a19 := mediate(a12, a9, 1/16);
  a20 := a19 shifted by (a18 - a12);

  % if not do_black:  
  %   drawdot a18 with_color green with_pen dot_pen;
  %   drawdot a19 with_color blue with_pen dot_pen;
  %   drawdot a20 with_color red with_pen dot_pen;
  % fi;

%% ** (2) M[]:  Rectangles
  
  M0 := get_rectangle (a12 -- a18 -- a20 -- a19 -- cycle);
  %draw M0 with_color blue;
  
  if do_labels:
    label("{\TINYrm 0}", get_center M0);
  fi;

  for i = 1 upto 23:
    M[i] := M[i-1] shifted by (a18 - a12);
    if do_labels:
      s := "{\TINYrm " & decimal i & "}";
      label(s, get_center M[i]);
    fi;
  endfor;

  j := 24;

  for k = 0 upto 14:
    for i = 0 upto 23:
      M[j] := M[i+24k] shifted by (a19 - a12);
      if do_labels:
  	s := "{\TINYrm " & decimal j & "}";
  	label(s, get_center M[j]);
      fi;
      j += 1;
    endfor;
  endfor;

%% ** (2) D[]:  Circles

  for i = 0 upto 383:
    D[i] := (unit_circle scaled (1.7, 0, 1.75)) shifted by get_center M[i];
    if not do_black:
      draw D[i] with_pen medium_pen with_color blue;
    fi;
  endfor;

%% ** (2)

  v2 := current_picture;
  
  scale current_picture (.4, .4, .4);

endfig with_projection parallel_x_z;
fig_ctr += 1;

%% * (1)

circle C_array[][];
circle D_array[][];

rectangle R_array[][];
rectangle M_array[][];

k := 0;
for i = 0 upto 15:
  for j = 0 upto 23:
    C_array[i][j] := C[k];
    D_array[i][j] := D[k];
    R_array[i][j] := R[k];
    M_array[i][j] := M[k];
    k += 1;
  endfor;
endfor;

numeric dist_horiz;
numeric dist_vert;

dist_horiz := magnitude(get_center R361 - get_center R360);
dist_vert  := magnitude(get_center R360 - get_center R336);

%% * (1) Perspective projection with both grids

beginfig(fig_ctr);

  message "Perspective projection with both grids";
  message "fig_ctr == " & decimal fig_ctr;
  %pause;
  
  if do_png:
    fill frame with_color white;
  fi;

  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  if do_labels:
    dotlabel.llft("{\smallrm p9}", p9);
    dotlabel.bot("{\smallrm p10}", p10);
    dotlabel.top("{\smallrm p11}", p11);
    dotlabel.top("{\smallrm p12}", p12);
  fi;
  
  draw r0;
  draw r1;

  if do_labels:
    dotlabel.bot("{\smallrm p13}", p13);
    dotlabel.top("{\smallrm p14}", p14);
  fi;

  % ang := (p9 - p10) angle (p13 - p10); % 150°
  % message "ang == " & decimal ang;
  % pause;
  
  j := 15;
  for i = 0 upto 23:

    if do_labels:
      s := "{\TINYrm " & decimal j & "}";
      dotlabel.bot(s, p[j]);

      s := "{\TINYrm " & decimal (j+1) & "}";
      dotlabel.top(s, p[j+1]);
    fi;


    if not do_black:
      draw p[j] -- p[j+1] with_pen medium_pen with_color dark_gray;
    fi;

    if do_labels:
      s := "{\TINYrm " & decimal (j+2) & "}";
      dotlabel.bot(s, p[j+2]);

      s := "{\TINYrm " & decimal (j+3) & "}";
      dotlabel.top(s, p[j+3]);
    fi;

    if not do_black:
      draw p[j+2] -- p[j+3] with_pen medium_pen with_color dark_gray;
    fi;

    j += 4;
  endfor;

  for i = 0 upto 15:

    if do_labels:
      s := "{\TINYrm " & decimal (j) & "}";
      dotlabel.lft(s, p[j]);

      s := "{\TINYrm " & decimal (j+1) & "}";
      dotlabel.lrt(s, p[j+1]);

      s := "{\TINYrm " & decimal (j+2) & "}";
      dotlabel.rt(s, p[j+2]);
    fi;

    if not do_black:
      draw p[j] -- p[j+1] -- p[j+2] with_pen medium_pen with_color gray;
    fi;
    
    j += 3;
  endfor;

  %draw c0 with_color blue;

  %draw c1 with_color red;

  if not do_black:
    for i = 0 upto 143: % 383: % 
      draw (C[i] * t1) with_pen medium_pen with_color blue;
      draw (D[i] * t0) with_pen medium_pen with_color blue;
    endfor;
  fi;

  v0 := current_picture;
  
  % fill (C107 * t1) with_color red;
  % draw (C107 * t1);

%% ** (2) L

    
%% ** (2)
    
  shift current_picture (-47.5, 5, picture_shift_val);
  %clip current_picture to frame;
  output current_picture with_focus f;
  clear current_picture;

  draw frame with_color blue; 
  
endfig with_projection parallel_x_y;
fig_ctr += 1;  


%% * (1)

if true: % false: % 
  input "sub_blue_skies.ldf";
fi;

%% * (1)

if not do_png:
  message "fig_ctr - 1 == " & decimal (fig_ctr - 1);
  pause;
fi;

bye;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:





