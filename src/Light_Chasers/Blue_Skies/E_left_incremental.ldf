%% E_left_incremental.ldf
%% Created by Laurence D. Finston (LDF) So 7. Apr 08:15:27 CEST 2024

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Empty left panel

beginfig(fig_ctr);

  message "Fig. " & decimal fig_ctr;
    %pause;
  
%% ** (2)

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
%% *** (3) Dots

  current_picture += v2;

  scale current_picture (.4, .4, .4);

endfig with_projection parallel_x_z;
fig_ctr += 1;  

%% * (1) Parallel projection

circle_vector E_left_cv;

for i = 43 step 24 until 355:
  E_left_cv += D[i];
endfor;

for i = 44 upto 46:
  E_left_cv += D[i];
endfor;

for i = 44 upto 46:
  E_left_cv += D[i+144];
endfor;

for i = 44 upto 46:
  E_left_cv += D[i+312];
endfor;

n := (size E_left_cv) - 1;

for i = 0 upto n:
  beginfig(fig_ctr);

    message "Fig. " & decimal fig_ctr;
  %pause;
    
%% ** (2)

    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;
    
    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
%% ** (2) L letter
    
    if do_black:
      fill q1 with_color black;
      fill q2 with_color black;
    else:
      unfill q1;
      unfill q2;
    fi;

%% *** (3) Dots

    for j = i downto 0:
      fill E_left_cv[j] with_color green;
    endfor;
    
    current_picture += v2;

    scale current_picture (.4, .4, .4);

  endfig with_projection parallel_x_z;
  fig_ctr += 1;  
endfor;

%% * (1) Empty panels, perspective projection

beginfig(fig_ctr);

%% ** (2)

  if do_black:
    fill frame with_color black;
  else:       
    if do_png:
      fill frame with_color white;
    fi;
  fi;

  if do_labels:
    s := "{\smallrm i {\char61} " & decimal i & "}";
    label.llft(s, p2 shifted (-.5, -.5));
  fi;
  
  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := v0;
  

  draw r0 with_color yellow;
  draw r1 with_color yellow;

%% ** (2)
  
  shift current_picture (-47.5, 5, picture_shift_val);
  clip current_picture to frame;

  output current_picture with_focus f;
  clear current_picture;

  draw frame with_color blue;

endfig with_projection parallel_x_y;
fig_ctr += 1;  

%% * (1) Perspective projection

for i = 0 upto n:
  beginfig(fig_ctr);

%% ** (2)

    if do_black:
      fill frame with_color black;
    else:       
      if do_png:
	fill frame with_color white;
      fi;
    fi;

    if do_labels:
      s := "{\smallrm i {\char61} " & decimal i & "}";
      label.llft(s, p2 shifted (-.5, -.5));
    fi;
    
    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    current_picture := v0;
    
    for j = i downto 0:
      fill E_left_cv[j] * t0 with_color green;
    endfor;
   
    draw r0 with_color yellow;
    draw r1 with_color yellow;

%% ** (2)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;

    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

