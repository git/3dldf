/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 5. Apr 08:07:37 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <sys/types.h>
#include <sys/wait.h>

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;   
   string zeroes_1; 
   string zeroes_2; 

   int i = 0;
   int j = 1;
   int k = -4;

   FILE *fp = 0;

   char str[8];

   for (int i = 4; i <= 93; ++i, ++j, ++k) // 21
   {    

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       j %= 17;

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       
       if (k > 0)
         k %= 17;


       if (k < 10)
         zeroes_2 = "000";
       else if (k < 100)
         zeroes_2 = "00";
       else if (k < 1000)
         zeroes_2 = "0";
       else
         zeroes_2 = "";

       s.str("");
      

       s << "convert -transparent black E" << zeroes << i << ".png A.png && "
         << "convert -transparent black I" << zeroes << i << ".png B.png && "
         << "composite A.png B.png C.png && "
         << "composite C.png frame_blue_background_black.png J" << zeroes << i << ".png;\n"
         << "echo $?"; 

       cerr << "s.str() == " << s.str() << endl;

       memset(str, '\0', 8);
       fp = popen(s.str().c_str(), "r");

       if (fp)
       {
          status = fread(str, 8, 1, fp);

          cerr << "status == " << status << endl;
          cerr << "errno == " << errno << endl;
          cerr << "str == " << str << endl;
          cerr << "atoi(str) == " << atoi(str) << endl;

          status = atoi(str);

          if (status != 0)
          {
              cerr << "Shell command failed.  Exiting `process_files' unsuccessfully with exit status 1."
                   << endl;

              pclose(fp);
              fp = 0;
              exit(1);
          }
       }
       else
       {
          cerr << "`popen' failed.  Exiting `process_files' unsuccessfully with exit status 1."
               << endl;

          pclose(fp);
          fp = 0;

          exit(1);
       }

       pclose(fp);
       fp = 0;

   } /* |for|  */

  return 0;

}  /* End of |main| definition  */


#if 0

/* * (1) */


/* ** (2) */

    for (int i = 0; i <= 93; ++i, ++j, ++k) // 21

       s << "convert -transparent black BLUE_SKIES_horiz_red_" << zeroes << i << ".png A.png && "
         << "composite A.png vert_mask_" << zeroes_1 << j << ".png B.png && "
         << "composite rectangles_right_solid_black.png B.png C.png && "
         << "convert -transparent black C.png D.png && " 
         << "composite D.png frame_blue_background_black.png E" << zeroes << i << ".png && \n";

       if (k >= 0)
         s << "composite A.png vert_mask_" << zeroes_2 << k << ".png F.png && "
           << "composite rectangles_left_solid_black.png F.png G.png && "
           << "convert -transparent black G.png H.png && "
           << "composite H.png frame_blue_background_black.png I" << zeroes << i << ".png; echo $?;\n";
        else
         s << " echo -n \"\"; echo $?;";


/* ** (2) */

       s << "cp BS_vert_sweep_" << zeroes << i << ".png Z" << zeroes << i << ".png;\n";

/* ** (2) */

   for (int i = 0; i <= 5; ++i, ++j) 
       s << "cp BLUE_SKIES_horiz_white_" << zeroes << i << ".png BS_vert_sweep_" << zeroes << i << ".png;\n";

#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        