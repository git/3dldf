/* process_files.cxx */
/* Created by Laurence D. Finston (LDF) Fr 5. Apr 08:07:37 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;   
   string zeroes_1; 

   int i = 0;
   int j = 0;
   int k = 0;

   j = 0;

   for (int i = 0; i <= 93; ++i, ++j) 
   {    

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       s.str("");

       s << "convert -fill cyan -opaque white "
         << "BLUE_SKIES_horiz_white_flopped_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_cyan_flopped_" << zeroes << i << ".png;\n"

         << "convert -fill red -opaque white "
         << "BLUE_SKIES_horiz_white_flopped_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_red_flopped_" << zeroes << i << ".png;\n";

       cerr << "s.str() == " << s.str() << endl;

   

       status = system(s.str().c_str());

       cerr << "`status' == " << status << endl;

       if (status != 0)
       {
           exit(1);
       }
     
   }

  return 0;

}  /* End of |main| definition  */


#if 0

/* * (1) */

/* ** (2) */

       s << "convert -flip Flopped/BLUE_SKIES_horiz_white_flopped_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_white_flopped_and_flipped_" << zeroes << i << ".png;\n";

       s << "convert -flip BLUE_SKIES_horiz_white_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_white_flipped_" << zeroes << i << ".png;\n";

       s << "convert -flop BLUE_SKIES_horiz_white_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_white_flopped_" << zeroes << i << ".png;\n";

       s << "convert -flop BLUE_SKIES_horiz_white_flipped_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_white_flipped_and_flopped_" << zeroes << i << ".png;\n";


/* ** (2) */

    s << "convert -fill white -opaque red -opaque " << green << " "
         << "-opaque cyan -opaque magenta -opaque orange -opaque violet " 
         << "-opaque " << purple << " " 
         << "-opaque " << teal_blue << " " 
         << "-opaque " << rose_madder << " " 
         << "Z" << zeroes << i << ".png BLUE_SKIES_horiz_white_" << zeroes << i << ".png;\n"
         << "convert -fill cyan -opaque white BLUE_SKIES_horiz_white_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_cyan_" << zeroes << i << ".png;\n"
         << "convert -fill red -opaque white BLUE_SKIES_horiz_white_" << zeroes << i << ".png "
         << "BLUE_SKIES_horiz_red_" << zeroes << i << ".png;\n";

/* ** (2) */

   string zeroes_2; // Final file (Z*.png)

   // Letters are ordered backwards, because "BLUE SKIES" scrolls from the left to the right.
   // LDF 2024.04.13.

   string zeroes;   // S 2
   string zeroes_1; // E right

   string zeroes_3; // I
   string zeroes_4; // K
   string zeroes_5; // S 1
   string zeroes_6; // E left
   string zeroes_7; // U 
   string zeroes_8; // L 
   string zeroes_9; // B

   //  i:     // S 2
   int j = 1; // E right 
   int k = 0; // Final file (Z*.png)
   int m = 1; // I
   int n = 1; // K     
   int o = 1; // S 1
   int p = 1; // E left
   int q = 1; // U
   int r = 1; // L
   int t = 1; // B




   for (int i = 0; k <= 95; ++i, ++k) 
   {


       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       if (k < 10)
         zeroes_2 = "000";
       else if (k < 100)
         zeroes_2 = "00";
       else if (k < 1000)
         zeroes_2 = "0";
       else
         zeroes_2 = "";

       if (m < 10)
         zeroes_3 = "000";
       else if (m < 100)
         zeroes_3 = "00";
       else if (m < 1000)
         zeroes_3 = "0";
       else
         zeroes_3 = "";

       if (n < 10) // K
         zeroes_4 = "000";
       else if (n < 100)
         zeroes_4 = "00";
       else if (n < 1000)
         zeroes_4 = "0";
       else
         zeroes_4 = "";

       if (o < 10) // S 1
         zeroes_5 = "000";
       else if (o < 100)
         zeroes_5 = "00";
       else if (o < 1000)
         zeroes_5 = "0";
       else
         zeroes_5 = "";

       if (p < 10) // E
         zeroes_6 = "000";
       else if (p < 100)
         zeroes_6 = "00";
       else if (p < 1000)
         zeroes_6 = "0";
       else
         zeroes_6 = "";

       if (q < 10) // U
         zeroes_7 = "000";
       else if (q < 100)
         zeroes_7 = "00";
       else if (q < 1000)
         zeroes_7 = "0";
       else
         zeroes_7 = "";

       if (r < 10) // L
         zeroes_8 = "000";
       else if (r < 100)
         zeroes_8 = "00";
       else if (r < 1000)
         zeroes_8 = "0";
       else
         zeroes_8 = "";

       if (t < 10) // B
         zeroes_9 = "000";
       else if (t < 100)
         zeroes_9 = "00";
       else if (t < 1000)
         zeroes_9 = "0";
       else
         zeroes_9 = "";



       s.str("");

       // S (-S)

       if (i <= 52)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill red -opaque cyan "
            << "../S_horiz/S_horiz_" << zeroes << i << ".png A.png;\n";
       else
          s << "cp frame_blue_rectangles_yellow_background_trans.png A.png;\n";

       // E (-ES)

       if (i > 5 && j <= 52)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill " << green << " -opaque magenta "
            << "../E_horiz/E_horiz_" << zeroes_1 << j++ << ".png B.png;\n";
       else 
          s << "cp A.png B.png;\n";

       // I (-IES)

       if (j > 6 && m <= 49)
          s << "convert -transparent violet -transparent " << pink << " "
            << "../I_horiz/I_horiz_" << zeroes_3 << m++ << ".png C.png;\n";
       else 
          s << "cp B.png C.png;\n";

       // K (-KIES)

       if (m > 3 && n <= 52)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill cyan -opaque magenta "
            << "../K_horiz/K_horiz_" << zeroes_4 << n++ << ".png D.png;\n";
       else 
          s << "cp C.png D.png;\n";


       // S (SKIES)

       if (n > 7 && o <= 51)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill orange -opaque red -opaque cyan "
            << "../S_horiz/S_horiz_" << zeroes_5 << o++ << ".png E.png;\n";
       else 
          s << "cp D.png E.png;\n";


       // E (E SKIES)

       if (o > 7 && p <= 51)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill " << green << " -opaque magenta "
            << "../E_horiz/E_horiz_" << zeroes_6 << p++ << ".png F.png;\n"
            << "mogrify -fill violet -opaque " << green << " F.png;\n";
       else 
          s << "cp E.png F.png;\n";


       // U (UE SKIES)

       if (p > 6 && q < 53)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill " << rose_madder << " -opaque " << teal_blue << " "
            << "../U_horiz/U_horiz_" << zeroes_7 << q++ << ".png G.png;\n";
       else 
          s << "cp F.png G.png;\n";

       // L (LUE SKIES)

       if (q > 7 & r < 52)
          s << "convert -transparent violet -transparent " << pink << " "
            << "../L_horiz/L_horiz_" << zeroes_8 << r++ << ".png H.png;\n";
       else 
          s << "cp G.png H.png;\n";

       // B (BLUE SKIES)

       if (r > 6 & t < 53)
          s << "convert -transparent violet -transparent " << pink << " "
            << "-fill " << purple << " -opaque red -opaque magenta "
            << "../B_horiz/B_horiz_" << zeroes_9 << t++ << ".png I.png;\n";
       else 
          s << "cp H.png I.png;\n";



       s << "composite A.png B.png Z.png;\n"
         << "composite Z.png C.png Y.png;\n"
         << "composite Y.png D.png Z.png;\n"
         << "composite Z.png E.png Y.png; cp Y.png W.png;\n"
         << "composite Y.png F.png Z.png; cp Z.png W.png;\n"
         << "composite Z.png G.png Y.png; cp Y.png W.png;\n"
         << "composite Y.png H.png Z.png; cp Z.png W.png;\n"
         << "composite Z.png I.png Y.png; cp Y.png W.png;\n"


         << "composite W.png ../frame_blue_background_black.png Y.png;\n"
         << "composite ../masks_both_panels_trans.png Y.png "
         << "Z" << zeroes_2 << k << ".png;\n";

       cerr << "s.str() == " << s.str() << endl;

       status = system(s.str().c_str());

       cerr << "`status' == " << status << endl;

       if (status != 0)
       {
           exit(1);
       }
     


  }  /* |for|  */



#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        