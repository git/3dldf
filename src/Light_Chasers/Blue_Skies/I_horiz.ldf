%% I_horiz.ldf
%% Created by Laurence D. Finston (LDF) Sa 13. Apr 07:21:53 CEST 2024

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) 

save_do_labels := do_labels;

do_labels := false;
do_outlines := false;

for loop_ctr = 0 upto 25:
  beginfig(fig_ctr);

%% ** (2)

    if do_png:
      fill frame with_color white;
    fi;
    
    if do_labels:
      s := "{\smallrm i {\char61} " & decimal i & "}";
      label.llft(s, p2 shifted (-.5, -.5));
    fi;
    
    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v0;

%% ** (2)
    
    fill r0 with_color violet;
    fill r1 with_color pink;
    draw r0 with_color green;
    draw r1 with_color green;

%% ** (2) I letter

    if do_outlines:
      draw q14 * t0;
    fi;

    dot_picture206 := dot_picture6;

    rotate dot_picture206 (180, 180);
    
    shift dot_picture206 (-13dist_horiz+(loop_ctr*dist_horiz), 0, 0);
    
    dot_picture206 *= t0;

    current_picture += dot_picture206;

%% ** (2)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;

    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;

%% * (1) Right panel

for loop_ctr = 0 upto 25:
  beginfig(fig_ctr);

%% ** (2)

    if do_png:
      fill frame with_color white;
    fi;
    
    if do_labels:
      s := "{\smallrm i {\char61} " & decimal i & "}";
      label.llft(s, p2 shifted (-.5, -.5));
    fi;
    
    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v0;

%% ** (2)
    
    fill r0 with_color violet;
    fill r1 with_color pink;
    draw r0 with_color green;
    draw r1 with_color green;

%% ** (2) I letter

    if do_outlines:
      draw q14 * t1;
    fi;

    dot_picture306 := dot_picture6;

    rotate dot_picture306 (0, 0);
    
    shift dot_picture306 (13dist_horiz-(loop_ctr*dist_horiz), 0, 0);
    
    dot_picture306 *= t1;

    current_picture += dot_picture306;

%% ** (2)
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;

    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;


%% * (1)

do_labels := save_do_labels;

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:



