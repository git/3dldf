#### Makefile
#### Created by Laurence D. Finston (LDF) Do 4. Apr 17:11:48 CEST 2024

.PHONY: mp eps pdf png

mp: blue_skies.mp
eps png: blue_skies_0000.eps
dvi: blue_skies.dvi
pdf: blue_skies.pdf

tmp: titles.mp
teps tpng: titles_00.eps
tdvi: titles.dvi
tpdf: titles.pdf


.PHONY: pf prog 

prog: process_files

pf: process_files
	./process_files

process_files: process_files.o
	g++ -o $@ $^

process_files.o: process_files.cxx colordefs.hxx 
	g++ -o $@ -c $< 

blue_skies.mp: blue_skies.ldf sub_blue_skies.ldf B_vert.ldf L_vert.ldf \
               U_vert.ldf E_left_vert.ldf S_2_vert.ldf K_vert.ldf I_vert.ldf \
               E_right_vert.ldf S_1_vert.ldf B_incremental.ldf \
               L_incremental.ldf U_incremental.ldf E_left_incremental.ldf \
               S_1_incremental.ldf K_incremental.ldf I_incremental.ldf \
               E_right_incremental.ldf S_2_incremental.ldf B_horiz.ldf L_horiz.ldf \
               U_horiz.ldf E_left_horiz.ldf S_1_horiz.ldf K_horiz.ldf I_horiz.ldf \
               E_right_horiz.ldf S_2_horiz.ldf plainldf.lmc
	./3dldf --no-database $<

blue_skies_0000.eps: blue_skies.mp
	mpost -numbersystem="double" $< >/dev/null

blue_skies.dvi: blue_skies.tex blue_skies_0000.eps
	tex $<

blue_skies.pdf: blue_skies.dvi
	dvipdfmx $<

titles.mp: titles.ldf plainldf.lmc
	./3dldf --no-database $<

titles_00.eps: titles.mp
	mpost -numbersystem="double" $< >/dev/null

titles.dvi: titles.tex titles_00.eps
	tex $<

titles.pdf: titles.dvi
	dvipdfmx $<

.PHONY: purge

purge:
	rm -f *.png *eps mp_input.* mp_output.* *.dvi *.log *.mf *.mp *.mpx mpxerr.*

