%% A_small_right.ldf
%% Created by Laurence D. Finston (LDF) Mi 20. Mär 15:48:15 CET 2024

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Loop 3:  A travelling across r1, right side, from right to left.
%%                Parallel projection

letter_picture100 := letter_picture1;

%% ** (2)


message "Loop 3:  A travelling across r1, right side, from right to left.";
message "Parallel projection";
message "First figure:";
message "fig_ctr == " & decimal fig_ctr;
%pause;

beginfig(fig_ctr);

  draw frame with_pen big_square_pen with_color black; % blue; ;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := v1;
  current_picture += letter_picture100;
  scale current_picture (.4, .4, .4);

endfig with_projection parallel_x_z;
fig_ctr += 1;  

%% ** (2)

shift letter_picture100 (-dist_horiz, 0);


for i = 0 upto 29:
  beginfig(fig_ctr);

%% *** (3)

    if do_black:
      fill frame with_color black;
    fi;

    if do_labels:
      label("{\smallrm Right side}", p3 shifted (5, -5));
    fi;

    
    draw frame with_pen big_square_pen with_color black; % blue; ;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    current_picture := v1;

%% *** (3)
    
    if do_labels:
      s := "{\smallrm i {\char61} " & decimal i & "}";
      label(s, a14 shifted (-16, 0));

      
    fi;

    shift letter_picture100 (dist_horiz, 0);

    % if (i == 0) or (i == 4):
    %   fill C143 with_color purple;
    % fi;

%% *** (3) Bottom of legs
    
    if (i >= 1) and (i <= 24):
      fill (C143 shifted ((i-1)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 5) and (i <= 28):
      fill (C143 shifted ((i-5)*dist_horiz, 0)) with_color bulb_color;
    fi;

%% *** (3) Crossbar

    if (i >= 1) and (i <= 24):
      fill (C119 shifted ((i-1)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 2) and (i <= 25):
      fill (C119 shifted ((i-2)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 3) and (i <= 26):
      fill (C119 shifted ((i-3)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 4) and (i <= 27):
      fill (C119 shifted ((i-4)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 5) and (i <= 28):
      fill (C119 shifted ((i-5)*dist_horiz, 0)) with_color bulb_color;
    fi;

%% *** (3) Left diagonal

    if (i >= 2) and (i <= 25):
      fill (C95 shifted ((i-2)*dist_horiz, 0)) with_color bulb_color;
      fill (C71 shifted ((i-2)*dist_horiz, 0)) with_color bulb_color;
    fi;

    if (i >= 3) and (i <= 26):
      fill (C47 shifted ((i-3)*dist_horiz, 0)) with_color bulb_color;
    fi;

%% *** (3) Right diagonal

    if (i >= 4) and (i <= 27):
      fill (C71 shifted ((i-4)*dist_horiz, 0)) with_color bulb_color;
      fill (C95 shifted ((i-4)*dist_horiz, 0)) with_color bulb_color;
    fi;
    

%% *** (3)
    

    if do_outlines:
      current_picture += letter_picture100;
    fi;
    
    scale current_picture (.4, .4, .4);

    
  endfig with_projection parallel_x_z;
  fig_ctr += 1;  
endfor;


%% * (1) Loop 4:  A travelling across r1, right side, from right to left.
%%       Perspective projection

message "Loop 4:  A travelling across r1, right side, from right to left.";
message "Perspective projection";
message "First figure:";
message "fig_ctr == " & decimal fig_ctr;
%pause;

q104 := q4 shifted by (get_center R[95] - a23);;
q105 := q5 shifted by (get_center R[95] - a23);;
q106 := q6 shifted by (get_center R[95] - a23);;

for i = 0 upto 29:
  beginfig(fig_ctr);

%% ** (2)

    if do_black:
      fill frame with_color black;
    else:       
      if do_png:
	fill frame with_color white;
      fi;
    fi;

    if do_labels:
      s := "{\smallrm i {\char61} " & decimal i & "}";
      label.llft(s, p2 shifted (-.5, -.5));
    fi;
    
    draw frame with_pen big_square_pen with_color black; % blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    current_picture := v0;

%% ** (2)

    if do_outlines:
      draw (q104 shifted (i*dist_horiz, 0)) * t1;
      draw (q105 shifted (i*dist_horiz, 0)) * t1;
    fi;

    if do_black:
      draw r0 with_color white;
      draw r1 with_color white;
    fi;
    

%% ** (2)
    
%% *** (3) Bottom of legs
    
    if (i >= 1) and (i <= 24):
      fill (C143 shifted ((i-1)*dist_horiz, 0)) * t1 with_color bulb_color;
    fi;

    if (i >= 5) and (i <= 28):
      fill (C143 shifted ((i-5)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

%% *** (3) Crossbar

    if (i >= 1) and (i <= 24):
      fill (C119 shifted ((i-1)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

    if (i >= 2) and (i <= 25):
      fill (C119 shifted ((i-2)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

    if (i >= 3) and (i <= 26):
      fill (C119 shifted ((i-3)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

    if (i >= 4) and (i <= 27):
      fill (C119 shifted ((i-4)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

    if (i >= 5) and (i <= 28):
      fill (C119 shifted ((i-5)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

%% *** (3) Left diagonal

    if (i >= 2) and (i <= 25):
      fill (C95 shifted ((i-2)*dist_horiz, 0))  * t1 with_color bulb_color;
      fill (C71 shifted ((i-2)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

    if (i >= 3) and (i <= 26):
      fill (C47 shifted ((i-3)*dist_horiz, 0))  * t1 with_color bulb_color;
    fi;

%% *** (3) Right diagonal

    if (i >= 4) and (i <= 27):
      fill (C71 shifted ((i-4)*dist_horiz, 0)) * t1 with_color bulb_color;
      fill (C95 shifted ((i-4)*dist_horiz, 0)) * t1 with_color bulb_color;
    fi;

    
    
%% ** (2)
    
    
    shift current_picture (-47.5, 5, picture_shift_val);
    clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;
    
    draw frame with_color black; % blue; ;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;  
endfor;

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:



