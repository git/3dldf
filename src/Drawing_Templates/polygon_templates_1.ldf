%% polygon_templates_1.ldf
%% Created by Laurence D. Finston (LDF) Tue 16 Aug 2022 07:35:20 AM CEST

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2022, 2023 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Top

verbatim_metapost "prologues := 3;\noutputtemplate := \"%j_%3c.eps\";";

verbatim_tex "\font\large=cmr12\font\largebf=cmbx12";

input "plainldf.lmc";

%% * (1)

%% From sphrmd00.lmc

macro crosshair;

def crosshair {point aa, numeric offset, point bb, point cc} =

  pen thin_pen;
  thin_pen := pencircle scaled (.25mm, .25mm, .25mm);

  transform tt;

  point A[];

  A0 := aa shifted (0, 0, 1);

  A1 := (offset * unit_vector(cc - bb)) shifted by aa;

  A2 := A1 rotated_around (aa, A0) 90;

  A3 := A1 rotated_around (aa, A0) 180;

  A4 := A1 rotated_around (aa, A0) 270;

  draw A1 -- A3 with_pen pencircle scaled (.25mm, .25mm, .25mm) with_pen thin_pen;
  draw A2 -- A4 with_pen pencircle scaled (.25mm, .25mm, .25mm) with_pen thin_pen;

enddef;



%% Example: 
%% crosshair {mediate(P10, P11, i/(crosshair_ctr + 1)), .1cm, P10, P11};

%% * (1) Figures

pickup pencircle scaled (.333mm, .333mm, .333mm);

bool do_labels;
do_labels := false; % true

%% ** (2) Fig. 0.  Dummy figure.  Must exist for make rules.  LDF 2022.08.14.

beginfig(0);
  drawdot origin;
endfig with_projection parallel_x_y;

%% ** (2)


numeric diam;
diam := 5;

numeric side_ctr;

side_ctr := 6;

macro template;

def template {numeric ffig_ctr, numeric ddiam, numeric sside_ctr} =

  beginfig(ffig_ctr);

    point p[];
    point r[];
    path q[];
    numeric n[];
    reg_polygon rp[];
    rectangle r[];
    transform t[];
    string s[];
    
    r0  := (unit_rectangle scaled (ddiam+3, 0, ddiam+3)) rotated (90, 0);

    set rp0 with_sides sside_ctr with_diameter ddiam with_normal (0, 0, 1);

    n0 := 360.0;
    n0 /= sside_ctr;

    t5 := identity rotated (0, 0, -.25n0);
    t6 := identity rotated (0, 0, .5n0);
    t7 := identity rotated (0, 0, .25n0);
    t8 := identity rotated (0, 0, n0);
    t9 := identity rotated (0, 0, -.25n0);
    t10 := identity rotated (0, 0, .5n0); 

    s5 	:= "Pentagon";
    s6 	:= "Hexagon";
    s7 	:= "Heptagon";
    s8 	:= "Octagon";
    s9 	:= "Nonagon";
    s10 := "Decagon";
    
    rp0 *= t[sside_ctr];

    draw r0;
    draw rp0;

    r1 := (unit_rectangle scaled (.5cm, 0, .5cm)) rotated (90, 0);
    draw r1;
    

    for i = 0 upto 3:
      p[i+1] := get_point (i) r0;
    endfor;
    
    p0 := get_center r0;
    crosshair {p0, .5cm, p1, p2};

    p5 := mediate(p3, p4);
    p6 := mediate(p4, p1);
    p7 := mediate(p1, p2);
    p8 := mediate(p2, p3);

    p9 := get_point 0 rp0;
    p10 := get_point 1 rp0;
    p11 := mediate(p9, p10);

    string temp_str;

    temp_str := "{\bf " & s[sside_ctr] & "}";

    label.lft(temp_str, p7 shifted (-.5, .5));

    temp_str := decimal ddiam & "cm diam.";

    label.rt(temp_str, p7 shifted (.5, .5));
    
    set rp1 with_sides sside_ctr with_diameter 2.5 with_normal (0, 0, 1);

    rp1 *= t[sside_ctr];
    
    draw rp1;
    
    p12 := get_point 0 rp1;
    p13 := get_point 1 rp1;
    p14 := mediate(p12, p13);

    p15 := (.25 * unit_vector(p13 - p12)) shifted by p14;
    p16 := (-.25 * unit_vector(p13 - p12)) shifted by p14;

    p17 := (.25 * unit_vector(p10 - p9)) shifted by p11;
    p18 := (-.25 * unit_vector(p10 - p9)) shifted by p11;

    q0 := p16 -- p18 -- p17 -- p15 -- cycle;

    for i = 0 upto (sside_ctr - 1):
      draw q0 rotated (0, 0, i*n0);
    endfor;

    if ddiam < 6:
      n1 := .333cm;
    else:
      n1 := .5cm;
    fi

    p19 := (n1 * unit_vector(p15 - p17)) shifted by p17;
    p20 := p18 rotated (0, 0, -n0);
    p21 := p16 rotated (0, 0, -n0);
    p22 := (n1 * unit_vector(p21 - p20)) shifted by p20;

    q1 := p10 --p13;

    p23 := (unit_vector(p10 - p17) * 3) shifted by p19;

    p24 := (p19 -- p23) intersection_point q1;

    q2 := p17 -- p19 -- p24 -- p22 -- p20 -- p10 -- cycle;

    for i = 0 upto (sside_ctr - 1):
      fill q2 rotated (0, 0, i*n0) with_color light_gray;
      draw q2 rotated (0, 0, i*n0);
    endfor;



    for i = 0 upto 3:
      p[25+i] := (p6 shifted (.75cm, 0)) rotated (0, 0, 90i);       
      crosshair {p[25+i], .5cm, p8, p6};
      draw (((unit_rectangle scaled (.5cm, 0, .5cm)) rotated (90, 0)) shifted by p25)
	rotated (0, 0, 90i);       
    endfor;


    for i = 0 upto 3:
      p[29+i] := (xpart p25, ypart p26) rotated (0, 0, 90i);       
      crosshair {p[29+i], .5cm, p8, p6};
      draw ((unit_rectangle scaled (.5cm, 0, .5cm)) rotated (90, 0)) shifted by p[29+i];
    endfor;

    

    if do_labels:
      dotlabel.llft("$p_1$", p1) rotate_text 90;
      dotlabel.lrt("$p_2$", p2);
      dotlabel.urt("$p_3$", p3);
      dotlabel.ulft("$p_4$", p4);
      dotlabel.top("$p_5$", p5);
      dotlabel.lft("$p_6$", p6);
      dotlabel.bot("$p_7$", p7);
      dotlabel.rt("$p_8$", p8);
      dotlabel.lft("$p_9$", p9);
      dotlabel.lft("$p_{10}$", p10);
      dotlabel.lft("$p_{11}$", p11);
      dotlabel.rt("$p_{12}$", p12);
      dotlabel.rt("$p_{13}$", p13);
      dotlabel.rt("$p_{14}$", p14);
      dotlabel.rt("$\scriptstyle p_{15}$", p15);
      dotlabel.rt("$\scriptstyle p_{16}$", p16);
      dotlabel.lft("$\scriptstyle p_{17}$", p17);
      dotlabel.lft("$\scriptstyle p_{18}$", p18);
      dotlabel.bot("$\scriptstyle p_{19}$", p19);
      dotlabel.ulft("$p_{20}$", p20);
      dotlabel.rt("$\scriptstyle p_{21}$", p21);
      dotlabel.urt("$\scriptstyle p_{22}$", p22);
      dotlabel.lrt("$\scriptstyle p_{24}$", p24);
    fi

  endfig with_projection parallel_x_y;
enddef;

%% ** (2)

%% `template' arguments:
%% ffig_ctr, ddiam, sside_ctr

j := 1;
for m = 4 step .25 until 12:    %% Diameter 
  for k = 5 upto 10: %% Sides
  template {j, m, k};
    j += 1;
  endfor;
endfor;

%% * (1)

end_mp;
end;

%% * (1)

%% * (1) End of 3DLDF code.

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%   	 GNU Emacs editor.  The local variable list is not evaluated when an 
%%   	 indirect buffer is visited, so it's necessary to evaluate the       
%%   	 following s-expression in order to use the facilities normally      
%%   	 accessed via the local variables list.                              
%%   	 \initials{LDF 2004.02.12}.                                          

%% (progn (metapost-mode) (outline-minor-mode t) (setq fill-column 80))    

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
