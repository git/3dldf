/* call_system.cxx  */
/* Created by Laurence D. Finston (LDF) Sa 20. Apr 07:52:19 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <sys/types.h>
#include <sys/wait.h>

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;


/* * (1) call_system  */

int
call_system(stringstream &s)
{
   int status = 0;

   char str[8];
   
   FILE *fp = 0;
   memset(str, '\0', 8);

   s << "echo $?";

   fp = popen(s.str().c_str(), "r");

   if (fp)
   {
      status = fread(str, 8, 1, fp);

#if 0 
      cerr << "status == " << status << endl;
      cerr << "errno == " << errno << endl;
      cerr << "str == " << str << endl;
      cerr << "atoi(str) == " << atoi(str) << endl;
#endif 

      status = atoi(str);

      if (status != 0)
      {
          cerr << "Shell command failed.  Exiting `process_files' unsuccessfully with exit status 1."
               << endl;

          pclose(fp);
          fp = 0;
          exit(1);
      }
   }
   else
   {
      cerr << "`popen' failed.  Exiting `process_files' unsuccessfully with exit status 1."
           << endl;

      pclose(fp);
      fp = 0;

      exit(1);
   }

   pclose(fp);
   fp = 0;

   return 0;

}  /* End of |call_system| definition  */

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */
