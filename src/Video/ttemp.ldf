%% ttemp.ldf
%% Created by Laurence D. Finston (LDF) Wed Apr 26 09:16:52 AM CEST 2023

input "plainldf.lmc";

verbatim_metapost "prologues := 3; outputtemplate := \"%j%3c.ps\";";
verbatim_tex "\font\Largebf=cmbx12 scaled \magstep1";

frame_wd := 400mm;
frame_ht := 225mm;

path frame;

frame :=    (-.5frame_wd, -.5frame_ht) -- (.5frame_wd, -.5frame_ht) -- (.5frame_wd, .5frame_ht)
         -- (-.5frame_wd, .5frame_ht) -- cycle;


outer_frame_wd := frame_wd + 7;
outer_frame_ht := frame_ht + 7;

path outer_frame;

outer_frame :=    ((-.5outer_frame_wd, -.5outer_frame_ht)
               -- (.5outer_frame_wd, -.5outer_frame_ht)
               -- (.5outer_frame_wd, .5outer_frame_ht)
               -- (-.5outer_frame_wd, .5outer_frame_ht) -- cycle) shifted (0, -1);


pen medium_pen;
medium_pen := pencircle scaled (.333mm, .333mm, .333mm);


pen big_pen;
big_pen := pencircle scaled (.5mm, .5mm, .5mm);


pen Big_pen;
Big_pen := pencircle scaled (.75mm, .75mm, .75mm);

pen BIG_pen;
BIG_pen := pencircle scaled (1mm, 1mm, 1mm);

pen huge_pen;
huge_pen := pencircle scaled (1.5mm, 1.5mm, 1.5mm);

pickup medium_pen;

picture label_picture;

point p[];
circle c[];

for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

p4 := mediate(p0, p1);
p5 := mediate(p1, p2);
p6 := mediate(p2, p3);
p7 := mediate(p3, p0);

beginfig(0);
  draw frame with_pen big_pen on_picture label_picture;
  dotlabel.urt("0", p0) on_picture label_picture;
  dotlabel.ulft("$p_1$", p1) on_picture label_picture;
  dotlabel.llft("$p_2$", p2) on_picture label_picture;
  dotlabel.lrt("$p_3$", p3) on_picture label_picture;
endfig with_projection parallel_x_y;

c0 := (unit_circle scaled (2, 0, 2)) rotated (90, 0);

string s;

j := 1;
k := 1;
m := .125;

for i = 1 upto 10:
  beginfig(i);
    draw frame with_pen BIG_pen;

    %message "k == " & decimal k;

    scale c0 (k, k);

    draw c0 with_pen Big_pen with_color red;
    
    s := "{\Largebf Frame ";

    if i < 10:
      s := s & "0000";
    elseif i < 100:
      s := s & "000";
    elseif i < 1000:
      s := s & "00";
    elseif i < 10000:
      s := s & "0";
    fi

    s := s & decimal i & "}";

    label(s, p4 shifted (0, -2));

    k += m;
    % message "k == " & decimal k & ", m == " & decimal m;
    % pause;
    
  endfig with_projection parallel_x_y;
endfor;

%pause;

%% ** (2)

%% * (1) End

bye;

%% * (1) Local variables for Emacs

%% Local Variables:
%% mode: MetaPost
%% eval:(outline-minor-mode t)
%% outline-regexp:"%% [*\f]+"
%% End:


