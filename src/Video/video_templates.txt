%% video_templates.txt
%% Created by Laurence D. Finston (LDF) Sat Apr 22 02:40:14 PM CEST 2023

%% * (1) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%% Copyright (C) 2023 The Free Software Foundation, Inc.

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version.  

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details.  

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html.

%% Please send bug reports to Laurence.Finston@gmx.de
%% The mailing list help-3dldf@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with ``subscribe <email-address>'' as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston 
%% c/o Free Software Foundation, Inc. 
%% 51 Franklin St, Fifth Floor 
%% Boston, MA  02110-1301  
%% USA

%% Laurence.Finston@gmx.de


\special{papersize=420mm, 297mm} %% DIN A3 Landscape
\vsize=297mm
\hsize=420mm

\input epsf
\input eplain
\input colordvi

\pageno=1

\parindent=0pt
\parskip=0pt
\baselineskip=0pt

\font\largebf=cmbx12 scaled \magstep1
\font\small=cmr9
\font\smalltt=cmtt9
\font\Largebf=cmbx17

\parindent=0pt
\parskip=0pt
\baselineskip=0pt

\advance\hoffset by -1in
\advance\voffset by -1in

\advance\vsize by -2cm

\footline={}

\begingroup
\advance\vsize by -2cm
\advance\hoffset by 1.5cm  %% This works.  LDF 2023.04.23.
\advance\voffset by 1.5cm
%% \hoffset=1.5cm %% This doesn't work.  LDF 2023.04.23.
%% \voffset=1.5cm
\baselineskip=12pt
\centerline{{\largebf Video Templates}}
\vskip1cm
\centerline{Author:  Laurence D.~Finston}
\vskip.5\baselineskip
\centerline{Created:  23.~April 2023}
\vskip.5\baselineskip
\centerline{Last updated:  24.~April 2023}

\small
\hsize=.75\hsize
%\hskip1cm
\vbox{\vskip2\baselineskip
This document is part of GNU 3DLDF, a package for three-dimensional
drawing.
\vskip\baselineskip
\advance\hsize by -19cm
Copyright (C) 2023 The Free Software Foundation, Inc.
\vskip\baselineskip

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version. 
\vskip\baselineskip

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 
\vskip\baselineskip

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc.,\hfil\break
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
\vskip\baselineskip

See the GNU Free Documentation License for the copying conditions 
that apply to this document.
\vskip\baselineskip

You should have received a copy of the GNU Free Documentation License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
\vskip\baselineskip

The mailing list {\smalltt info-3dldf@gnu.org} is for sending 
announcements to users. To subscribe to this mailing list, send an 
email with ``subscribe $\langle$email-address$\rangle$'' as the subject.  
\vskip\baselineskip

The webpages for GNU 3DLDF are here:
\hbox to 0pt{{\smalltt http://www.gnu.org/software/3dldf/LDF.html}\hss}
\vskip\baselineskip

The author can be contacted at:\vskip\baselineskip 

Laurence D. Finston\hfil\break
c/o Free Software Foundation, Inc.\hfil\break
51 Franklin St, Fifth Floor \hfil\break
Boston, MA  02110-1301 \hfil\break 
USA
\vskip\baselineskip

{\smalltt Laurence.Finston@gmx.de}}
\par
\vfil\eject
\endgroup

\footline={\hfil {\largebf \folio}\hfil}
\pageno=1

%% %% ** (2)  Empty page.  Prevents page from shifting in Document Viewer (evince).
%% \vbox to \vsize{\vss
%% \centerline{{\largebf (Blank page)}}\vss}
%% \vfil\eject

%% ** (2)

%% Frame only, 400mm by 225mm (w by h)

\vbox to \vsize{\vskip2cm\centerline{{\largebf Empty frame}}\vskip1cm
\centerline{\epsffile{video_templates000.eps}}
\vss}
\vfil\eject

%% Frame with crosshairs and with rectangles for 1/4, 1/2, 3/4 of the frame.
\vbox to \vsize{\vskip2cm\centerline{{\largebf Frame area divided into quarters}}\vskip1cm
\centerline{\epsffile{video_templates001.eps}}
\vss}
\vfil\eject

%% Frame with crosshairs and with rectangles for 1/3 and 2/3 of the frame.
\vbox to \vsize{\vskip2cm\centerline{{\largebf Frame area divided into thirds}}\vskip1cm
\centerline{\epsffile{video_templates002.eps}}
\vss}
\vfil\eject


%% %% ** (2)  Empty page.  Prevents page from shifting in Document Viewer (evince).
%% \vbox to \vsize{\vss
%% \centerline{{\largebf (Blank page)}}\vss}
%% \vfil\eject

%% *** (3) End here

\bye

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:plain-TeX
%% eval:(local-set-key "\"" 'self-insert-command)
%% eval:(outline-minor-mode t)
%% auto-fill-function:nil
%% outline-regexp:"%% [*\f]+"
%% End:


