%% diagonal_wipes.ldf
%% Created by Laurence D. Finston (LDF) Sat Apr 22 04:35:47 PM CEST 2023

%% * (1) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%% Copyright (C) 2023 The Free Software Foundation, Inc.

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version.  

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details.  

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html.

%% Please send bug reports to Laurence.Finston@gmx.de
%% The mailing list help-3dldf@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with ``subscribe <email-address>'' as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston 
%% c/o Free Software Foundation, Inc. 
%% 51 Franklin St, Fifth Floor 
%% Boston, MA  02110-1301  
%% USA

%% Laurence.Finston@gmx.de


input "plainldf.lmc";

verbatim_metapost "prologues := 3; outputtemplate := \"%j%3c.eps\";";
verbatim_tex "\font\hugebx=cmbx12 scaled 22000";

frame_wd := 400mm;
frame_ht := 225mm;

path frame;

frame :=    (-.5frame_wd, -.5frame_ht) -- (.5frame_wd, -.5frame_ht) -- (.5frame_wd, .5frame_ht)
         -- (-.5frame_wd, .5frame_ht) -- cycle;

pen medium_pen;
medium_pen := pencircle scaled (.333mm, .333mm, .333mm);


pen big_pen;
big_pen := pencircle scaled (.5mm, .5mm, .5mm);

pickup medium_pen;

beginfig(0);
  draw frame with_pen big_pen;
endfig with_projection parallel_x_y;

%% ** (2) Diagonal wipe from upper left hand to lower right hand corner.
%%        LDF 2023.04.22.

  
point p[];

p0 := get_point (0) frame;
p1 := get_point (1) frame;
p2 := get_point (2) frame;
p3 := get_point (3) frame;

beginfig(1);
  filldraw frame with_fill_color red;
endfig with_projection parallel_x_y;

j := 2;


increment_val := .025;

for i = increment_val step increment_val until (1 + .5increment_val):
  beginfig(j);
    draw frame with_pen big_pen;

    % dotlabel.bot("$p_0$", p0);
    % dotlabel.bot("$p_1$", p1);
    % dotlabel.top("$p_2$", p2);
    % dotlabel.top("$p_3$", p3);

    fill frame with_color red;
    filldraw p3 -- (mediate(p3, p2, i)) -- (mediate(p3, p0, i)) -- cycle with_fill_color blue;

  endfig with_projection parallel_x_y;
  j += 1;
endfor;

for i = increment_val step increment_val until (1 + .5increment_val):
  beginfig(j);
    
    draw frame with_pen big_pen;

    % dotlabel.bot("$p_0$", p0);
    % dotlabel.bot("$p_1$", p1);
    % dotlabel.top("$p_2$", p2);
    % dotlabel.top("$p_3$", p3);

    fill frame with_color blue;
    filldraw p1 -- (mediate(p0, p1, i)) -- (mediate(p2, p1, i)) -- cycle with_fill_color red;
  endfig with_projection parallel_x_y;
  j += 1;
endfor;

% message "j == " & decimal j;
% pause;

%% This might be needed, depending on when the previous loop exits.
%% This may differ, depending on rounding error.
%% LDF 2023.04.22.

% beginfig(j);
%   filldraw frame with_fill_color blue;
% endfig with_projection parallel_x_y;

%% ** (2)

%% * (1) End

bye;

%% * (1) Local variables for Emacs

%% Local Variables:
%% mode: MetaPost
%% eval:(outline-minor-mode t)
%% outline-regexp:"%% [*\f]+"
%% End:


