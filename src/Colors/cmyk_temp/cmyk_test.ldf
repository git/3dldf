%%%% cmyk_test.ldf
%%%% Created by Laurence D. Finston (LDF) Thu 11 Nov 2021 10:27:28 PM CET

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023 The Free Software Foundation, Inc.  

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


verbatim_metapost "verbatimtex \font\large=cmr12\font\largebx=cmbx12 etex;";
verbatim_metapost "verbatimtex \font\small=cmr7 etex;";

verbatim_metapost "prologues := 3;";

%% *** (3) Declarations

%% **** (4) Global variables

  point p[];
  path q[];

  q0 := origin -- (2cm, 0) -- (2cm, 2cm) -- (0, 2cm) -- cycle;
  p0 := (1cm, 0);

%% **** (4)

macro draw_sample[];

%% ***** (5)

def draw_sample[0] {color c, numeric xx_shift, numeric yy_shift} =
  string s;
  label.bot("\hbox to 3.5cm{\hss (" & decimal cyan_part c & ", " & decimal magenta_part c &
      ", " & decimal yellow_part c & ", " & decimal black_part c &
      ")\hss}", p0 shifted (xx_shift, -(yy_shift + .1cm)));
  s := get_name c;
  fill q0 shifted (xx_shift, -yy_shift) with_color c;
  if length s > 0:
    s := capitalize s;
    label.bot("\hbox to 3.5cm{\hss " & s & "\hss}",
	p0 shifted (xx_shift, -(yy_shift + .5cm)));
  fi;
enddef;

%% ***** (5)

def draw_sample[1] {numeric ccyan, numeric mmagenta, numeric yyellow,
                    numeric bblack, numeric xx_shift, numeric yy_shift} =
  string s;
  color c;
  c := (ccyan, mmagenta, yyellow, bblack);
  unset_name c;
  fill q0 shifted (xx_shift, -yy_shift) with_color c;
  label.bot("\hbox to 3.5cm{\hss(" & decimal cyan_part c & ", "  & decimal magenta_part c &
      ", " & decimal yellow_part c & ", " & decimal black_part c & ")\hss}",
      p0 shifted (xx_shift, -(yy_shift + .1)));
enddef;



%% *** (3)

color cyan;
cyan := (1, 0, 0, 0);

color magenta;
magenta := (0, 1, 0, 0);

color yellow;
yellow := (0, 0, 1, 0);

color black;
black := (0, 0, 0, 1);

color red;
red := (0, 1, 1, 0);

color green;
green := (1, 0, 1, 0);

color blue;
blue := (1, 1, 0, 0);

color cerulean_blue;
cerulean_blue := (1, .5, 0, 0);

set_name cerulean_blue to "cerulean blue";
show cerulean_blue;

color teal_blue;
teal_blue := (1, .5, .5, 0);

set_name teal_blue to "teal blue";
show teal_blue;

color dark_olive_green;
dark_olive_green := (0, 0, 1, .75);

set_name dark_olive_green to "dark olive green";
show dark_olive_green;


color mauve;
mauve := (.5, 1, .5, 0);

set_name mauve to "mauve";
show mauve;

color turquoise;
turquoise := (1, 0, .5, 0);

color violet;
violet := (.5, 1, 0, 0);

color rose_madder;
rose_madder := (0, 1, .5, 0);
set_name rose_madder to "rose madder";

color lime_green;
lime_green := (.5, 0, 1, 0);
set_name lime_green to "lime green";

color orange;
orange := (0, .5, 1, 0);


%% *** (3) Figures

%% **** (4) Figure 1

beginfig(0);
  draw_sample[0] {cyan,  0, 0};
  draw_sample[0] {magenta, 3, 0};
  draw_sample[0] {yellow,  6, 0};
  draw_sample[0] {black,   9, 0};
endfig with_projection parallel_x_y;

%% **** (4) Figure 2

beginfig(1);
  draw_sample[0] {red, 0, 3};
  draw_sample[0] {green, 3, 3};
  draw_sample[0] {blue, 6, 3};
endfig with_projection parallel_x_y;

%% **** (4) Combinations of 1 (Once) and 0.5 (Once) Without Black

beginfig(2);
  draw_sample[0] {cerulean_blue, 0, 0};
  draw_sample[0] {turquoise, 3, 0};
  draw_sample[0] {violet, 6, 0};
  draw_sample[0] {rose_madder, 9, 0};
  draw_sample[0] {lime_green, 12, 0};
  draw_sample[0] {orange, 15, 0};
endfig with_projection parallel_x_y;

%% **** (4) Combinations of 1 (Once) and 0.5 (Twice) Without Black

beginfig(3);
   draw_sample[0] {teal_blue, 0, 0};    %% Teal Blue
   draw_sample[0] {mauve, 3, 0};        %% Mauve
   draw_sample[1] {.5, .5, 1, 0, 6, 0}; %% Olive Green
endfig with_projection parallel_x_y;

%% **** (4) Combinations of 1 (Twice) and 0.5 (Once) Without Black

beginfig(4);
  draw_sample[1] {1, 1, .5, 0, 0, 0}; %% Prussian Blue
  draw_sample[1] {1, .5, 1, 0, 3, 0}; %% Forest Green
  draw_sample[1] {.5, 1, 1, 0, 6, 0}; %% Burnt Sienna
endfig with_projection parallel_x_y;

%% **** (4) Cyan to Blue:  Cyan 1 Plus Magenta in steps of .125 

beginfig(5);
  draw_sample[0] {cyan, 0, 0};
  draw_sample[1] {1, .125, 0, 0, 3, 0};
  draw_sample[1] {1, .25, 0, 0, 6, 0};
  draw_sample[1] {1, .375, 0, 0, 9, 0};
  draw_sample[1] {1, .5, 0, 0, 12, 0};
  draw_sample[1] {1, .625, 0, 0, 15, 0};
  draw_sample[1] {1, .75, 0, 0, 0, 3.5};
  draw_sample[1] {1, .875, 0, 0, 3, 3.5};
  draw_sample[0] {blue, 6, 3.5}; 
endfig with_projection parallel_x_y;


%% **** (4) Cyan to Green:  Cyan 1 Plus Yellow in steps of .125 

beginfig(6);
  draw_sample[0] {cyan, 0, 0};
  draw_sample[1] {1, 0, .125, 0, 3, 0};
  draw_sample[1] {1, 0, .25, 0, 6, 0};
  draw_sample[1] {1, 0, .375, 0, 9, 0};
  draw_sample[1] {1, 0, .5, 0, 12, 0};
  draw_sample[1] {1, 0, .625, 0, 15, 0};
  draw_sample[1] {1, 0, .75, 0, 0, 3.5};
  draw_sample[1] {1, 0, .875, 0, 3, 3.5};
  draw_sample[0] {green, 6, 3.5}; 
endfig with_projection parallel_x_y;

%% **** (4) Magenta to Red:  Magenta 1 Plus Yellow in steps of .125 

beginfig(7);
  draw_sample[0] {magenta, 0, 0};
  draw_sample[1] {0, 1, .125, 0, 3, 0};
  draw_sample[1] {0, 1, .25, 0, 6, 0};
  draw_sample[1] {0, 1, .375, 0, 9, 0};
  draw_sample[1] {0, 1, .5, 0, 12, 0};
  draw_sample[1] {0, 1, .625, 0, 15, 0};
  draw_sample[1] {0, 1, .75, 0, 0, 3.5};
  draw_sample[1] {0, 1, .875, 0, 3, 3.5};
  draw_sample[0] {red, 6, 3.5}; 
endfig with_projection parallel_x_y;

%% **** (4) Cyan to Black:  Cyan 1 Plus Black in steps of .125 

beginfig(8);
  draw_sample[0] {cyan, 0, 0};
  draw_sample[1] {1, 0, 0, .125, 3, 0};
  draw_sample[1] {1, 0, 0, .25, 6, 0};
  draw_sample[1] {1, 0, 0, .375, 9, 0};
  draw_sample[1] {1, 0, 0, .5, 12, 0};
  draw_sample[1] {1, 0, 0, .625, 15, 0};
  draw_sample[1] {1, 0, 0, .75, 0, 3.5};
  draw_sample[1] {1, 0, 0, .875, 3, 3.5};
  draw_sample[1] {1, 0, 0, 1, 6, 3.5}; 
endfig with_projection parallel_x_y;


%% **** (4) Magenta to Black:  Magenta 1 Plus Black in steps of .125 

beginfig(9);
  draw_sample[0] {magenta, 0, 0};
  draw_sample[1] {0, 1, 0, .125, 3, 0};
  draw_sample[1] {0, 1, 0, .25, 6, 0};
  draw_sample[1] {0, 1, 0, .375, 9, 0};
  draw_sample[1] {0, 1, 0, .5, 12, 0};
  draw_sample[1] {0, 1, 0, .625, 15, 0};
  draw_sample[1] {0, 1, 0, .75, 0, 3.5};
  draw_sample[1] {0, 1, 0, .875, 3, 3.5};
  draw_sample[1] {0, 1, 0, 1, 6, 3.5}; 
endfig with_projection parallel_x_y;

%% **** (4) Yellow to Black:  Yellow 1 Plus Black in steps of .125 

beginfig(10);
  draw_sample[0] {yellow, 0, 0};
  draw_sample[1] {0, 0, 1, .125, 3, 0};
  draw_sample[1] {0, 0, 1, .25, 6, 0};
  draw_sample[1] {0, 0, 1, .375, 9, 0};
  draw_sample[1] {0, 0, 1, .5, 12, 0};
  draw_sample[1] {0, 0, 1, .625, 15, 0};
  %draw_sample[1] {0, 0, 1, .75, 0, 3.5};
  draw_sample[0] {dark_olive_green, 0, 3.5};
  draw_sample[1] {0, 0, 1, .875, 3, 3.5};
  draw_sample[1] {0, 0, 1, 1, 6, 3.5}; 
endfig with_projection parallel_x_y;


%% **** (4) Light Lavender to Blue:  Cyan and Magenta in synchronized steps of .125 

beginfig(11);
  draw_sample[1] {.125, .125,  0, 0,  0, 0};
  draw_sample[1] {.25,  .25,   0, 0,  3, 0};
  draw_sample[1] {.375, .375,  0, 0,  6, 0};
  draw_sample[1] {.5,   .5,    0, 0,  9, 0};
  draw_sample[1] {.625, .625,  0, 0, 12, 0};
  draw_sample[1] {.75,  .75,   0, 0, 15, 0};
  draw_sample[1] {.875, .875,  0, 0,  0, 3.5};
  draw_sample[0] {blue,               3, 3.5}; 
endfig with_projection parallel_x_y;

%% **** (4) Pale Green to Green:  Cyan and Yellow in synchronized steps of .125 

beginfig(12);
  draw_sample[1] {.125, 0, .125,  0,  0, 0};
  draw_sample[1] {.25,  0, .25,   0,  3, 0};
  draw_sample[1] {.375, 0, .375,  0,  6, 0};
  draw_sample[1] {.5,   0, .5,    0,  9, 0};
  draw_sample[1] {.625, 0, .625,  0, 12, 0};
  draw_sample[1] {.75,  0, .75,   0, 15, 0};
  draw_sample[1] {.875, 0, .875,  0,  0, 3.5};
  draw_sample[0] {green,               3, 3.5};
endfig with_projection parallel_x_y;

%% **** (4) Pale Pink to Red:  Magenta and Yellow in synchronized steps of .125 

beginfig(13);
  draw_sample[1] {0, .125, .125,  0,  0, 0};
  draw_sample[1] {0, .25,  .25,   0,  3, 0};
  draw_sample[1] {0, .375, .375,  0,  6, 0};
  draw_sample[1] {0, .5,   .5,    0,  9, 0};
  draw_sample[1] {0, .625, .625,  0, 12, 0};
  draw_sample[1] {0, .75,  .75,   0, 15, 0};
  draw_sample[1] {0, .875, .875,  0,  0, 3.5};
  draw_sample[0] {red,               3, 3.5}; 
endfig with_projection parallel_x_y;

%% **** (4) Cyan and Magenta Plus Black 1

beginfig(14);
  draw_sample[1] {.25, .25, 0,  .125,   0, 0};
  draw_sample[1] {.25, .25, 0,  .25,    4, 0};
  draw_sample[1] {.25, .25, 0,  .375,   8, 0};
  draw_sample[1] {.25, .25, 0,  .5,    12, 0};
  draw_sample[1] {.25, .25, 0,  .625,   0, 3.5};
  draw_sample[1] {.25, .25, 0,  .75,    4, 3.5};

  draw_sample[1] {.5, .25, 0,  .125,   0, 7};
  draw_sample[1] {.5, .25, 0,  .25,    4, 7};
  draw_sample[1] {.5, .25, 0,  .375,   8, 7};
  draw_sample[1] {.5, .25, 0,  .5,    12, 7};
  draw_sample[1] {.5, .25, 0,  .625,   0, 10.5};
  draw_sample[1] {.5, .25, 0,  .75,    4, 10.5};

  draw_sample[1] {.75, .25, 0,  .125,   0, 14};
  draw_sample[1] {.75, .25, 0,  .25,    4, 14};
  draw_sample[1] {.75, .25, 0,  .375,   8, 14};
  draw_sample[1] {.75, .25, 0,  .5,    12, 14};
  draw_sample[1] {.75, .25, 0,  .625,   0, 17.5};
  draw_sample[1] {.75, .25, 0,  .75,    4, 17.5};
  
  
endfig with_projection parallel_x_y;

%% **** (4) Cyan and Magenta Plus Black 2

beginfig(15);

  draw_sample[1] {1, .25, 0,  .125,   0, 0};
  draw_sample[1] {1, .25, 0,  .25,    4, 0};
  draw_sample[1] {1, .25, 0,  .375,   8, 0};
  draw_sample[1] {1, .25, 0,  .5,    12, 0};
  draw_sample[1] {1, .25, 0,  .625,   0, 3.5};
  draw_sample[1] {1, .25, 0,  .75,    4, 3.5};

  draw_sample[1] {.25, .5, 0,  .125,   0, 7};
  draw_sample[1] {.25, .5, 0,  .25,    4, 7};
  draw_sample[1] {.25, .5, 0,  .375,   8, 7};
  draw_sample[1] {.25, .5, 0,  .5,    12, 7};
  draw_sample[1] {.25, .5, 0,  .625,   0, 10.5};
  draw_sample[1] {.25, .5, 0,  .75,    4, 10.5};

  draw_sample[1] {.5, .5, 0,  .125,   0, 14};
  draw_sample[1] {.5, .5, 0,  .25,    4, 14};
  draw_sample[1] {.5, .5, 0,  .375,   8, 14};
  draw_sample[1] {.5, .5, 0,  .5,    12, 14};
  draw_sample[1] {.5, .5, 0,  .625,   0, 17.5};
  draw_sample[1] {.5, .5, 0,  .75,    4, 17.5};
  
endfig with_projection parallel_x_y;

%% **** (4) Cyan and Magenta Plus Black 3

beginfig(16);

  draw_sample[1] {.75, .25, 0,  .125,   0, 0};
  draw_sample[1] {.75, .25, 0,  .25,    4, 0};
  draw_sample[1] {.75, .25, 0,  .375,   8, 0};
  draw_sample[1] {.75, .25, 0,  .5,    12, 0};
  draw_sample[1] {.75, .25, 0,  .625,   0, 3.5};
  draw_sample[1] {.75, .25, 0,  .75,    4, 3.5};

  draw_sample[1] {1, .5, 0,  .125,   0, 7};
  draw_sample[1] {1, .5, 0,  .125,   4, 7};
  draw_sample[1] {1, .5, 0,  .375,   8, 7};
  draw_sample[1] {1, .5, 0,  .5,    12, 7};
  draw_sample[1] {1, .5, 0,  .625,   0, 10.5};
  draw_sample[1] {1, .5, 0,  .75,    4, 10.5};

  draw_sample[1] {.25, .75, 0,  .125,   0, 14};
  draw_sample[1] {.25, .75, 0,  .25,    4, 14};
  draw_sample[1] {.25, .75, 0,  .375,   8, 14};
  draw_sample[1] {.25, .75, 0,  .5,    12, 14};
  draw_sample[1] {.25, .75, 0,  .625,   0, 17.5};
  draw_sample[1] {.25, .75, 0,  .75,    4, 17.5};
  
endfig with_projection parallel_x_y;




%% *** (3)


verbatim_metapost "end;";
end;

%% *** (3)

%% ** (2) End of 3DLDF code.

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not evaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       \initials{LDF 2004.02.12}.                                          

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80))    

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% abbrev-mode:t
%% outline-regexp:"%% [*\f]+"
%% End:
