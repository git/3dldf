/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 19. Dez 10:11:02 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/



#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */


int main(int argc, char *argv[])
{
/* ** (2) */
   
   int status = 0;

   stringstream s;
   string zeroes;
   string zeroes_1;

   for (int i = 601; i <= 802; i++) // 802
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      s << "convert -transparent " << cornflower_blue << " flying_saucer_" << zeroes << i << ".png A.png;"
        << "composite A.png ./Backgrounds/Kadmiumrot_dunkel_gouache.png B.png;" 
        << "convert -transparent " << orange << " B.png C.png;"
        << "composite C.png ./Backgrounds/Violett_plus_Zinkweiss.png D.png;";

      if (i >= 8)
      {
         s << " convert -transparent " << red << " D.png E.png;"
           << "composite E.png ./Backgrounds/Scharlach_gouache.png F.png;"
           << " convert -transparent " << blue << " F.png G.png;"
           << "composite G.png ./Backgrounds/Neapelgelb_hell_gouache.png H.png;"
           << " convert -transparent " << green << " H.png I.png;"
           << "composite I.png ./Backgrounds/Kadmiumgruen_gouache.png  J.png;"
           << " convert -transparent " << magenta << " J.png K.png;"
           << "composite K.png ./Backgrounds/Purpurmagenta.png L.png;"
           << " convert -transparent " << cyan << " L.png M.png;"
           << "composite M.png ./Backgrounds/Coelinblau_2_coats_gouache.png N.png;"
           << " convert -transparent " << purple << " N.png O.png;"
           << "composite O.png ./Backgrounds/Kadmiumgelb_dunkel_gouache.png P.png;"
           << " convert -transparent " << teal_blue << " P.png Q.png;"
           << "composite Q.png ./Backgrounds/Zinnobergruen_gouache.png R.png;"
           << " convert -transparent " << rose_madder << " R.png S.png;"
           << "composite S.png ./Backgrounds/Karminrot_gouache.png T.png;"
           << " convert -transparent " << emerald << " T.png U.png;"
           << "composite U.png ./Backgrounds/Lichter_ocker_gouache.png V.png;"
           << " convert -transparent " << periwinkle << " V.png W.png;"
           << "composite W.png ./Backgrounds/Kadmiumorange_gouache.png X.png;"
           << " convert -transparent " << goldenrod << " X.png Y.png;"
           << "composite Y.png ./Backgrounds/Indischgelb_gouache.png Z.png;"
           << " convert -transparent " << melon << " Z.png AA.png;"
           << "composite AA.png ./Backgrounds/Rotorange_gouache.png BB.png;";
        }
        else 
        {
           s << "mv D.png BB.png;";
        } 

        s << "convert -transparent black BB.png CC.png;"
          << "composite CC.png ./Backgrounds/Kobaltblau_hell_gouache.png DD.png;"
          << " convert -transparent white DD.png EE.png;"
          << "composite EE.png ./Backgrounds/Graphitgrau_2_coats_wc.png FF.png;"
          << "composite frame_black.png FF.png GG.png;"      
          << "mv GG.png A" << zeroes << i << ".png;";   

      cerr << "s.str() == " << s.str() << endl;

#if 1 /* 0  */
      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
      
#endif 

   } /* |for|  */


   
  return 0;

} /* |main|  */

/* * (1)  */

#if 0 

/* ** (2)  */

   /* This works.  All of these colors are converted properly.  LDF 2024.12.19.  */

   s << "convert -fill black "
     << " -opaque " << red << " "
     << " -opaque " << blue << " "
     << " -opaque " << green << " "
     << " -opaque " << magenta << " "
     << " -opaque " << cyan << " "
     << " -opaque " << purple << " "
     << " -opaque " << teal_blue << " "
     << " -opaque " << rose_madder << " "
     << " -opaque " << emerald << " "
     << " -opaque " << periwinkle << " "
     << " -opaque " << goldenrod << " "
     << " -opaque " << melon << " "
     << " -opaque " << cornflower_blue << " "
     << "color_test.png color_test_replaced.png";

/* ** (2)  */

#endif 
/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


