#! /bin/bash

#### ttemp.sh
#### Created by Laurence D. Finston (LDF) Sun Apr  9 04:26:03 PM CEST 2023

pdfseparate -f 	1 -l  1 cmbx12ldf.pdf A_cmbx12ldf.pdf
pdfseparate -f 	1 -l  1 cmr10ldf.pdf  A_cmr10ldf.pdf
pdfseparate -f 	1 -l  1 cmssbx10.pdf  A_cmssbx10.pdf
pdfseparate -f 37 -l 37 eufb10ldf.pdf A_eufb10ldf.pdf
pdfseparate -f 37 -l 37 eufm10ldf.pdf A_eufm10ldf.pdf
pdfseparate -f 28 -l 28 eurb10ldf.pdf A_eurb10ldf.pdf
pdfseparate -f 28 -l 28 eurm10.pdf    A_eurm10.pdf   
pdfseparate -f  3 -l  3 eusb10ldf.pdf A_eusb10ldf.pdf
pdfseparate -f  3 -l  3 eusm10ldf.pdf A_eusm10ldf.pdf

pdfunite A_cmbx12ldf.pdf A_cmr10ldf.pdf A_cmssbx10.pdf A_eufb10ldf.pdf \
A_eufm10ldf.pdf A_eurb10ldf.pdf A_eurm10.pdf A_eusb10ldf.pdf \
A_eusm10ldf.pdf A_all.pdf


exit 0

pdfunite testfont_cmbx12.pdf \
testfont_cmssbx10.pdf \
testfont_eufb10.pdf \
testfont_eufm10.pdf \
testfont_eurb10.pdf \
testfont_eurm10.pdf \
testfont_eusb10.pdf \
testfont_eusm10.pdf testfont_all.pdf

exit 0

dvipdfmx testfont_cmbx12.dvi
dvipdfmx testfont_cmssbx10.dvi
dvipdfmx testfont_euex10.dvi
dvipdfmx testfont_eufb10.dvi
dvipdfmx testfont_eufm10.dvi
dvipdfmx testfont_eurb10.dvi
dvipdfmx testfont_eurm10.dvi
dvipdfmx testfont_eusb10.dvi
dvipdfmx testfont_eusm10.dvi

exit 0

cp glyphs.lmc glyph_test_a4.txt glyph_test_cmssbx.ldf glyph_test_eufm.ldf glyph_test_eufm.txt glyph_test_eurm.ldf \
   glyph_test.ldf glyph_test_ssbx.txt glyph_test.txt ~/3DLDF-3.0_web/3dldf/SRC_CODE/

cp glyph_test_a4.pdf ~/3DLDF-3.0_web/3dldf/graphics/

exit 0
