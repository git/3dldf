/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License. */

/* This file is part of GNU 3DLDF, a package for three-dimensional drawing.  */
/* Copyright (C) 2024, 2025 The Free Software Foundation, Inc. */

/* GNU 3DLDF is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or  */
/* (at your option) any later version.   */

/* GNU 3DLDF is distributed in the hope that it will be useful,  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  */
/* GNU General Public License for more details.   */

/* You should have received a copy of the GNU General Public License  */
/* along with GNU 3DLDF; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA */

/* GNU 3DLDF is a GNU package.   */
/* It is part of the GNU Project of the   */
/* Free Software Foundation  */
/* and is published under the GNU General Public License.  */
/* See the website http://www.gnu.org  */
/* for more information.    */
/* GNU 3DLDF is available for downloading from  */
/* http://www.gnu.org/software/3dldf/LDF.html. */

/* Please send bug reports to Laurence.Finston@gmx.de */
/* The mailing list help-3dldf@gnu.org is available for people to  */
/* ask other users for help.   */
/* The mailing list info-3dldf@gnu.org is for sending  */
/* announcements to users. To subscribe to these mailing lists, send an  */
/* email with ``subscribe <email-address>'' as the subject.   */

/* The author can be contacted at:     */

/* Laurence D. Finston 		       */
/* c/o Free Software Foundation, Inc.  */
/* 51 Franklin St, Fifth Floor 	       */
/* Boston, MA  02110-1301  	       */
/* USA                                 */                             

/* Laurence.Finston@gmx.de  */




#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

// composite H000.png I020.png I021.png

/* 

color_vector.clear();

/*
color_vector.push_back(red);
green;
cyan;
magenta;
yellow;
purple;
orange;
dark_green;
rose_madder;
teal_blue;
peach;
cornflowerblue;
turquoise;
dandelion;
mulberry;
apricot;
lavender;
goldenrod;
plum;
greenyellow;
melon;
royalblue;
yelloworange;
processblue;
periwinkle;
bittersweet;
burntorange;
rhodamine;
mahogany;
carnationpink;
wildstrawberry;
pinegreen;
skyblue;
tan;
emerald;
salmon;
orchid;
orangered;
fuchsia;
aquamarine;
olivegreen;
brickred;
redorange;
royalpurple;  
junglegreen;
maroon;
cerulean;
rubinered;
*/


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   string zeroes;
   string zeroes_1;

   initialize_color_vector();

   int j = 350;

   for (int i = 28; i <= 348; i++, j++)
   {
      s.str("");

      if (i < 10)
         zeroes = "00";
      else if (i < 100)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite pretty_girl_" << zeroes << i << ".png pretty_girl_"
        << zeroes_1 << j << ".png A" << zeroes << i << ".png";


      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
  }

   
  return 0;

}


#if 0 

   s.str("");

   s << "convert -fill black ";

   for (vector<pair<string, string> >::iterator iter = color_vector.begin();
        iter != color_vector.end();
        ++iter)
   {
       s << "-opaque " << iter->second << " ";
   }




/* ** (2) */

  return 0;

}  /* End of |main| definition  */
#endif 


#if 0

        << "composite A.png purple.png B.png; "
        << "convert -transparent magenta B.png C.png; "
        << "composite C.png Kadmiumgelb_gouache.png D.png; " 
        << "composite frame.png D.png E" << zeroes << i << ".png";


      s << "convert -transparent black -transparent blue glyph_dots_" << zeroes << i << ".png A.png; "
        << "composite A.png letters_plain_nrc_262.png B.png; "
        << "composite B.png ./Backgrounds/Lampenschwarz.png C.png; "
        << "convert -transparent yellow C.png D.png; "
        << "composite D.png ./Backgrounds/Kadmiumgelb_gouache.png E.png; "
        << "convert E.png -transparent red F.png; "
        << "composite F.png ./Backgrounds/Kadmiumrot_dunkel_gouache.png "
        << "letters_plain_cr_" << zeroes << i << ".png; "
        << "rm A.png B.png C.png D.png E.png F.png";

s << "convert -transparent blue -transparent black glyph_dots_" << zeroes << i << ".png "
        << "A" << zeroes << i << ".png; "
        << "composite A" << zeroes << i << ".png ./Backgrounds/Lampenschwarz.png B" << zeroes
        << i << ".png; " 
        << "convert -transparent yellow B" << zeroes << i << ".png C" << zeroes << i << ".png; "
        << "composite C" << zeroes << i << ".png ./Backgrounds/Kadmiumgelb_gouache.png D" << zeroes 
        << i << ".png; "
        << "convert -transparent red D" << zeroes << i << ".png E" << zeroes << i << ".png; "
        << "composite E" << zeroes << i << ".png ./Backgrounds/Kadmiumrot_dunkel_gouache.png "
        << "letters_plain_" << zeroes << i << ".png";


      s.str("");

      if (i < 10)
         zeroes = "00";
      else if (i < 100)
         zeroes = "0";
      else
         zeroes = "";

      s << "convert -transparent black -transparent blue ./PNG/glyph_dots_" 
        << zeroes << i << ".png "
        << "./PNG/A" << zeroes << i << ".png";

      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite G" << zeroes << i << ".png frame_black_filled.png "
        << "G" << zeroes_1 << j << ".png";


   zeroes = "0";

   int j = -1;

   for (int i = 34; i <= 36; i++)
   {

      s.str("");

      if (j+1 < 10)
         zeroes_1 = "00";
      else if (j+1 < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      for (int k = 0; k <= 15; k+=3)
        s << "mogrify -transparent black borders_" << zeroes << (i+k) << ".png; "; 

      s << "composite borders_" << zeroes << i << ".png borders_" 
        << zeroes << i+3 << ".png A" << zeroes_1 << ++j << ".png; ";

      if (j+1 < 10)
         zeroes_1 = "00";
      else if (j+1 < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite A" << zeroes_1 << j << ".png borders_" << zeroes << i+6 << ".png "
        << "A" << zeroes_1 << ++j << ".png; ";

      if (j+1 < 10)
         zeroes_1 = "00";
      else if (j+1 < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite A" << zeroes_1 << j << ".png borders_" << zeroes << i+9 << ".png "
        << "A" << zeroes_1 << ++j << ".png; ";

      if (j+1 < 10)
         zeroes_1 = "00";
      else if (j+1 < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite A" << zeroes_1 << j << ".png borders_" << zeroes << i+12 << ".png "
        << "A" << zeroes_1 << ++j << ".png; ";

      if (j+1 < 10)
         zeroes_1 = "00";
      else if (j+1 < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite A" << zeroes_1 << j << ".png borders_" << zeroes << i+15 << ".png "
        << "A" << zeroes_1 << ++j << ".png; ";

      small_diamond_ctr = i % 3;
      circle_ctr        = i % 4;
      dart_ctr          = i % 5; 
      large_diamond_ctr = i % 6;

      cerr << "i                 == " << i << endl
           << "small_diamond_ctr == " << small_diamond_ctr << endl
           << "circle_ctr        == " << circle_ctr << endl 
           << "dart_ctr          == " << dart_ctr << endl        
           << "large_diamond_ctr == " << large_diamond_ctr << endl << endl;

      if (   i > 0 && small_diamond_ctr == 0 && circle_ctr == 0 && dart_ctr == 0 
          && large_diamond_ctr == 0)
      {
          cerr << "All counters are at 0.  Starting new cycle." << endl << endl;
      }

      if (i > 0 && circle_ctr == 0)
      {

      }

   int circle_ctr        = 0;     
   int large_diamond_ctr = 0;     
   int small_diamond_ctr = 0;
   int dart_ctr          = 0;

   string circle_filename_stem;
   string circle_filename;
   int circle_file_index;
   int circle_file_index_limit;


// (/ 355 60.0) 5.916666666666667
// (* 4 5 6) and 8:  cycle requires 120 steps.  (/ 120 8.0) 15.0
// (* 3 4 5 6) cycle requires 60 steps

       if (i < 10)
          zeroes = "00";
       else if (i < 100)
          zeroes = "0";
       else
          zeroes = "";

       s.str("");

       s << "convert -fill black -opaque white glyph_dots_" << zeroes << i << ".eps glyph_dots_" 
         << zeroes << i << ".png";

       cerr << "s.str() == " << s.str() << endl;

       status = system(s.str().c_str());

       cerr << "`status' == " << status << endl;

       if (status != 0)
       {
           exit(1);
       }   



   for (int i = 1001; i <= 1016; ++i)
   {
       s.str("");

       s << "convert -transparent magenta "
         << " -transparent " << turquoise << " -transparent " << dark_green
         << " -transparent " << rose_madder 
         << " -transparent " << teal_blue 
         << " -transparent black "
         << " -transparent " << green << " "
         << " -transparent cyan " 
         << " -transparent orange "
         << " -transparent yellow "
         << " glyph_dots_" << i << ".png A" << i << ".png";

       cerr << "s.str() == " << s.str() << endl;

       status = system(s.str().c_str());

       cerr << "`status' == " << status << endl;

       if (status != 0)
       {
           exit(1);
       }   

   }  /* |for|  */
#endif 

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


