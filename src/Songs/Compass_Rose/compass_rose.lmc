%% compass_rose.lmc
%% Created by Laurence D. Finston (LDF) So 27. Okt 09:19:14 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)


%% * (1) macro compass_rose

macro compass_rose;

def compass_rose (ppv, ccv, TTv_zero, TTv_one, TTv_two, PPv, llabel_picture)
                  {numeric rradius_zero, numeric rradius_one,
                  numeric rradius_two, numeric rradius_three} =

%% ** (2)

  message "Entering compass_rose.";
  point p[];
  circle c[];
  triangle T[];
  string s;

%% ** (2)

  clear ppv;
  clear ccv;
  clear TTv_zero;
  clear TTv_one;
  clear TTv_two;
  clear PPv;
  
%% ** (2)

  % message "rradius_zero: " & decimal rradius_zero;
  % message "rradius_one: " & decimal rradius_one;
  % message "rradius_two: " & decimal rradius_two;
  % message "rradius_three: " & decimal rradius_three;


  % pause;

  
  PPv += 2;
  
  c0 := unit_circle scaled (rradius_zero, 0, rradius_zero) rotated (90, 0);
  ccv += c0;
  
  draw c0 on_picture PPv0;
  
  c1 := unit_circle scaled (rradius_one, 0, rradius_one) rotated (90, 0);
  ccv += c1;

  draw c1 on_picture PPv0;

  c2 := (unit_circle scaled (rradius_two, 0, rradius_two)) rotated (90, 0);
  ccv += c2;
  draw c2 on_picture PPv0 with_color magenta;


  c3 := (unit_circle scaled (rradius_three, 0, rradius_three)) rotated (90, 0);
  ccv += c3;
  draw c3 on_picture PPv0 with_color blue;

%% *** (3) Points on circles.

  for i = 0 upto ((size c0) - 1):
    p[i] := get_point (i) c0;
    ppv += p[i];
  endfor;

  for i = 0 upto ((size c1) - 1):
    p[100+i] := get_point (i) c1;
    ppv += p[100+i];
  endfor;

%% *** (3) Labels for points on circles.  

  if do_labels:
    dotlabel.rt("$\scriptstyle p_{0}$",  p0) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{1}$",  p1) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{2}$",  p2) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{3}$",  p3) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{4}$",  p4) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{5}$",  p5) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{6}$",  p6) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{7}$",  p7) on_picture llabel_picture;
    dotlabel.top("$\scriptstyle p_{8}$",  p8) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{9}$",  p9) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{10}$", p10) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{11}$", p11) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{12}$", p12) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{13}$", p13) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{14}$", p14) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{15}$", p15) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{16}$", p16) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{17}$", p17) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{18}$", p18) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{19}$", p19) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{20}$", p20) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{21}$", p21) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{22}$", p22) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{23}$", p23) on_picture llabel_picture;
    dotlabel.bot("$\scriptstyle p_{24}$", p24) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{25}$", p25) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{26}$", p26) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{27}$", p27) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{28}$", p28) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{29}$", p29) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{30}$", p30) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{31}$", p31) on_picture llabel_picture;
  fi;

  if do_labels:
    dotlabel.rt("$\scriptstyle p_{100}$",   p100) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{101}$",   p101) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{102}$",   p102) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{103}$",   p103) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{104}$",   p104) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{105}$",   p105) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{106}$",   p106) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{107}$",   p107) on_picture llabel_picture;
    dotlabel.top("$\scriptstyle p_{108}$",  p108) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{109}$",  p109) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{110}$", p110) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{111}$", p111) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{112}$", p112) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{113}$", p113) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{114}$", p114) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{115}$", p115) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{116}$", p116) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{117}$", p117) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{118}$", p118) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{119}$", p119) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{120}$", p120) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{121}$", p121) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{122}$", p122) on_picture llabel_picture;
    dotlabel.lft("$\scriptstyle p_{123}$", p123) on_picture llabel_picture;
    dotlabel.bot("$\scriptstyle p_{124}$", p124) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{125}$",  p125) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{126}$",  p126) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{127}$",  p127) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{128}$",  p128) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{129}$",  p129) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{130}$",  p130) on_picture llabel_picture;
    dotlabel.rt("$\scriptstyle p_{131}$",  p131) on_picture llabel_picture;
  fi;

  

  
  
  % !! START HERE.  LDF 2024.10.26. 



%% ** (2)
    
  message "Exiting compass_rose.";

enddef;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.


%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
