/* colordefs_extern.hxx */
/* Created by Laurence D. Finston (LDF) Mi 13. Mär 16:25:34 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/

extern string white;
extern string black;

extern string red;                 
extern string blue;                

extern string cyan;                
extern string magenta;             
extern string yellow;              

extern string orange;              
extern string red_orange_neon;

extern string green;               

extern string orange_dvips;        
extern string brown;               
extern string pink;                
extern string purple;              
extern string violet;              

extern string apricot;             
extern string aquamarine;          
extern string bittersweet;         
extern string blue_violet;         
extern string blue_green;          
extern string brick_red;           
extern string burnt_orange;        
extern string cadet_blue;          
extern string carnation_pink;      
extern string cerulean_blue;       
extern string cerulean_dvips;      
extern string cornflower_blue;     
extern string dandelion;           
extern string dark_blue;           
extern string dark_green;          
extern string dark_olive_green;    
extern string dark_orchid;         
extern string emerald;             
extern string forest_green;        
extern string fuchsia;             
extern string goldenrod;           
extern string green_yellow;        
extern string jungle_green;        
extern string lavender;            
extern string lime_green;          
extern string mahogany;            
extern string maroon;              
extern string mauve;               
extern string melon;               
extern string midnight_blue;       
extern string mulberry;            
extern string navy_blue;           
extern string olive_green;         
extern string orange_red;          
extern string orange_red_dvips;    
extern string orchid;              
extern string peach;               
extern string periwinkle;          
extern string pine_green;          
extern string plum;                
extern string process_blue;        
extern string purple_dvips;        
extern string raw_sienna;          
extern string red_orange;          
extern string red_violet;          
extern string rhodamine;           
extern string rose_madder;         
extern string royal_blue;          
extern string royal_purple;        
extern string rubine_red;          
extern string salmon;              
extern string sea_green;           
extern string sepia;               
extern string sky_blue;            
extern string spring_green;        
extern string tan_dvips;           
extern string teal_blue;           
extern string teal_blue_dvips;     
extern string thistle;             
extern string turquoise;           
extern string turquoise_dvips;     
extern string violet_dvips;        
extern string violet_red;          
extern string violet_red_dvips;    
extern string wild_strawberry;     
extern string yellow_green;        
extern string yellow_green_dvips;  
extern string yellow_orange;       

extern string light_gray;
extern string gray;
extern string medium_dark_gray;
extern string dark_gray;
extern string Dark_gray;
extern string DARK_gray;

/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */
