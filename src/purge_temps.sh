#! /bin/bash

#### purge_temps.sh

a=`find . -name "[abcde][^.][^.][^.][^.][^.][^.]"`
#echo "a = $a"

if test "$a"
then
    #echo "Removing file(s): $a"
    echo "Removing temporary file(s)."
    rm $a
else
    echo "No temporary files found."
fi

exit 0
