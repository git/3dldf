@q scnmptpt.web@>
@q Created by Laurence D. Finston Thu 20 Jan 2022 05:14:09 AM CET  @>

@q * (1) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing.  @>
@q Copyright (C) 2022, 2023, 2024, 2025 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version.  @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details.  @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA@>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html.@>

@q ("@@" stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de@>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @>
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @>
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)@>


@q  * (1) Scanner for MetaPost logging output.  @>
@* Scanner for MetaPost logging output.
\initials{LDF 2022.01.20.}

@
@<Include files@>=

#include "loader.h++"

#include <string.h>

#ifdef __GNUC__
#include <getopt.h> 
#endif 

#include "pspglb.h++"
#include "io.h++"
#include "gsltmplt.h++"
#include "creatnew.h++"
#include "pntrvctr.h++"
#include "primes.h++"
#include "complex.h++"
#include "matrices.h++"
#include "colors.h++"
#include "transfor.h++"
#include "pens.h++"
#include "dashptrn.h++"
#include "shapes.h++" 
#include "pictures.h++"
#include "points.h++"
#include "lines.h++"
#include "planes.h++"
#include "nurbs.h++"
#include "cnnctrtp.h++"
#include "paths.h++"
#include "focsfncs.h++"
#include "curves.h++"
#include "polygons.h++"
#include "triangle.h++"
#include "rectangs.h++"
#include "conicsct.h++"
#include "ellipses.h++"
#include "catenary.h++"
#include "tractrix.h++"
#include "sinewaves.h++"
#include "sprellps.h++"
#include "circles.h++"
#include "parabola.h++"
#include "hyprbola.h++"
#include "cncsctlt.h++"
#include "helices.h++"
#include "origami.h++"
#if 0
   #include "patterns.h++"
#endif 

#include "solids.h++"
#include "solfaced.h++"
#include "cuboid.h++"
#include "prismatd.h++"
#include "polyhed.h++"
#include "tetrhdrn.h++"
#include "octahdrn.h++"
#include "icsahdrn.h++"
#include "ddchdrn.h++"
#include "rhtchdrn.h++"
#include "cones.h++"
#include "cylinder.h++" 
#include "ellpsoid.h++"
#include "sprelpsd.h++"
#include "spheres.h++" 
#include "sphrdevl.h++" 
#include "parabold.h++"
#include "hyprbold.h++"
#include "paraellp.h++"
#include "parahypr.h++" 
#include "glyphs.h++"
#include "pctfncs0.h++"
#include "utility.h++"
#include "pntrvcf0.h++"
#include "predctes.h++"
#include "scanprse.h++"   
#include "figures.h++"   
#include "parser.h++"   
#include "parser_1.h++"   
#include "scan.h++"
#include "scanprsf.h++"
#include "imetfncs.h++"
#include "deftfncs.h++"
#include "sctpcrt.h++"  
#include "sctpfncs.h++"  
#include "prrfnc0.h++"
#include "mpoutput.h++"

@q  ** (2) Start conditions.  @>
@ Start conditions.

@<Start conditions@>=

@q  ** (2) Options.  @>
@ Options.

@<Options@>=
@=%option header-file="scnmptpt.h++"@>
@=%option reentrant@>
@=%option prefix="zz"@>
@=%option noyywrap@>

@q ** (2) Name definitions.  @>
@ Name definitions.  
@<Name definitions@>=

NUMBER [-]?[0-9]+(\.[0-9]+)?([eE][-]?[0-9]+)?
POINT \({NUMBER},[[:space:]]*{NUMBER}\) 

@q * (1) Local variables for |zzlex|.  @>
@ Local variables for {\bf zzlex}.
\initials{LDF 2012.06.25.}

@<Local variables for |zzlex|@>=

bool DEBUG = false;  /* |true|  */

Scanner_Node scanner_node = static_cast<Scanner_Node>(yyextra); 
bool collecting_path          = false;
bool collecting_resolved      = false;
bool collecting_tangent       = false;
bool collecting_perpendicular = false;

@q ** (2) Code to be executed each time |zzlex| is entered.  @>

@ Code to be executed each time {\bf zzlex} is entered.  This code must be
indented or it causes an error when FLEX is run.  The start condition on entry
to {\bf zzlex} can be set here.
\initials{LDF 2012.06.27.}

@<Execute on entry to |zzlex|@>=

   if (DEBUG)
      cerr << "Entering `zzlex'." << endl;

@q ** (2) Rules.  @>
@ Rules.

@q *** (3) @> 
@ 
@<Rules@>= 
@="START GLYPH" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  START GLYPH" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="END GLYPH" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  END GLYPH" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="START PATH" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  START PATH" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   if (!(collecting_resolved || collecting_tangent || collecting_perpendicular))
      scanner_node->metapost_output_struct->path_vector.push_back(create_new<Path>(0));

   collecting_path = true;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="END PATH" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  END PATH" << endl;
  
       cerr << "scanner_node->metapost_output_struct->path_vector.size() == " 
            << scanner_node->metapost_output_struct->path_vector.size() << endl;

       if (scanner_node->metapost_output_struct->path_vector.size() > 0)
          scanner_node->metapost_output_struct->path_vector.back()->show("Path:");

       cerr << "scanner_node->metapost_output_struct->resolved_vector.size() == " 
            << scanner_node->metapost_output_struct->resolved_vector.size() << endl;

       if (scanner_node->metapost_output_struct->resolved_vector.size() > 0)
          scanner_node->metapost_output_struct->resolved_vector.back()->show("Path:");
  
       cerr << "scanner_node->metapost_output_struct->tangent_vector.size() == " 
            << scanner_node->metapost_output_struct->tangent_vector.size() << endl;

       if (scanner_node->metapost_output_struct->tangent_vector.size() > 0)
          scanner_node->metapost_output_struct->tangent_vector.back()->show("Path:");

       cerr << "scanner_node->metapost_output_struct->perpendicular_vector.size() == " 
            << scanner_node->metapost_output_struct->perpendicular_vector.size() << endl;

       if (scanner_node->metapost_output_struct->perpendicular_vector.size() > 0)
          scanner_node->metapost_output_struct->perpendicular_vector.back()->show("Path:");

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   collecting_path = false;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="START RESOLVED" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  START RESOLVED" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   scanner_node->metapost_output_struct->resolved_vector.push_back(create_new<Path>(0));

   collecting_resolved = true;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="END RESOLVED" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  END RESOLVED" << endl;
       scanner_node->metapost_output_struct->resolved_vector.back()->show("Resolved:");

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   collecting_resolved = false;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="START TANGENT" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  START TANGENT" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   scanner_node->metapost_output_struct->tangent_vector.push_back(create_new<Path>(0));

   collecting_tangent = true;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="END TANGENT" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  END TANGENT" << endl;
       scanner_node->metapost_output_struct->tangent_vector.back()->show("Tangent:");

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  


   collecting_tangent = false;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="START PERPENDICULAR" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  START PERPENDICULAR" << endl; 

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   scanner_node->metapost_output_struct->perpendicular_vector.push_back(create_new<Path>(0));

   collecting_perpendicular = true;

@=}@> 

@q *** (3) @> 
@ 
@<Rules@>= 
@="END PERPENDICULAR" {@> 

#if DEBUG_COMPILE 
   if (DEBUG) 
   { 
       cerr << "In `zzlex':  END PERPENDICULAR" << endl;
       scanner_node->metapost_output_struct->perpendicular_vector.back()->show("Perpendicular:");

   }  /* |if (DEBUG)|  */ 
#endif  /* |DEBUG_COMPILE|  */  

   collecting_perpendicular = false;

@=}@> 

@q *** (3) @>
@
@<Rules@>=
@=controls[[:space:]]*{POINT}[[:space:]]*and[[:space:]]*{POINT} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `controls .. and point':  " << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   Connector_Type *ct = create_new<Connector_Type>(0);
   ct->type1 = ct->type0 = Connector_Type::CONTROLS_TYPE;

   stringstream temp_strm;

   temp_strm << yytext;

   string s;
   char c;
   real x;
   real y;

   temp_strm >> s;

   if (DEBUG)
      cerr << "s == " << s << endl;

   temp_strm >> c;

   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> x;
   
   if (DEBUG)
      cerr << "x == " << x << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> y;
   
   if (DEBUG)
      cerr << "y == " << y << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   x *= 0.0352778;
   y *= 0.0352778;

   ct->pt0 = create_new<Point>(0);
   (ct->pt0)->set(x, y, 0);

   temp_strm >> s;
   
   if (DEBUG)
      cerr << "s == " << s << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> x;
   
   if (DEBUG)
      cerr << "x == " << x << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> y;
   
   if (DEBUG)
      cerr << "y == " << y << endl;

   ct->pt1 = create_new<Point>(0);
   
   x *= 0.0352778;
   y *= 0.0352778;

   (ct->pt1)->set(x, y, 0);

   if (DEBUG)
     ct->show("*ct:");

   if (collecting_resolved)
      *(scanner_node->metapost_output_struct->resolved_vector.back()) += ct;
   else if (collecting_tangent)
      *(scanner_node->metapost_output_struct->tangent_vector.back()) += ct; 
   else if (collecting_perpendicular)
      *(scanner_node->metapost_output_struct->perpendicular_vector.back()) += ct; 
   else
      *(scanner_node->metapost_output_struct->path_vector.back()) += ct;

@=}@>

@q *** (3) @>
@
@<Rules@>=
@=controls[[:space:]]*{POINT} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `controls point':  " << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   Connector_Type *ct = create_new<Connector_Type>(0);
   ct->type0 = Connector_Type::CONTROLS_TYPE;
   ct->type1 = Connector_Type::CT_NULL_TYPE;
   ct->pt1   = 0;

   stringstream temp_strm;
   temp_strm << yytext;

   string s;
   char c;
   real x;
   real y;

   temp_strm >> s;

   if (DEBUG)
      cerr << "s == " << s << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> x;
   
   if (DEBUG)
      cerr << "x == " << x << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> y;
   
   if (DEBUG)
      cerr << "y == " << y << endl;

   x *= 0.0352778;
   y *= 0.0352778;

   ct->pt0 = create_new<Point>(0);
   (ct->pt0)->set(x, y, 0);

   ct->pt1 = create_new<Point>(0);
   (ct->pt1)->set(x, y, 0);
   
   if (DEBUG)
      ct->show("*ct:");

   if (collecting_resolved)
      *(scanner_node->metapost_output_struct->resolved_vector.back()) += ct;
   else if (collecting_tangent)
      *(scanner_node->metapost_output_struct->tangent_vector.back()) += ct; 
   else if (collecting_perpendicular)
      *(scanner_node->metapost_output_struct->perpendicular_vector.back()) += ct; 
   else
      *(scanner_node->metapost_output_struct->path_vector.back()) += ct;

@=}@>

@q *** (3) @>
@
@<Rules@>=
@={POINT} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  A point:  " << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   stringstream temp_strm;
   temp_strm << yytext;

   string s;
   char c;
   real x;
   real y;

   temp_strm >> c;

   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> x;
   
   if (DEBUG)
      cerr << "x == " << x << endl;

   temp_strm >> c;
   
   if (DEBUG)
      cerr << "c == " << c << endl;

   temp_strm >> y;
   
   if (DEBUG)
      cerr << "y == " << y << endl;

   x *= 0.0352778;
   y *= 0.0352778;

   Point *p = create_new<Point>(0);
   p->set(x, y, 0);

   if (collecting_resolved)
      *(scanner_node->metapost_output_struct->resolved_vector.back()) += p;
   else if (collecting_tangent)
      *(scanner_node->metapost_output_struct->tangent_vector.back()) += p;
   else if (collecting_perpendicular)
      *(scanner_node->metapost_output_struct->perpendicular_vector.back()) += p;
   else if (collecting_path)
      *(scanner_node->metapost_output_struct->path_vector.back()) += p;
   else
      scanner_node->metapost_output_struct->point_vector.push_back(p);

@=}@>

@q *** (3) @>
@
@<Rules@>=
@={NUMBER} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  A number:  " << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

@=}@>


@q *** (3) @>
@
@<Rules@>=
@=cycle {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `cycle'." << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   scanner_node->metapost_output_struct->path_vector.back()->set_cycle(true, scanner_node);

@=}@>

@q *** (3) @>
@
@<Rules@>=
@=\.\. {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `connector':  " << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

@=}@>

@q *** (3) REAL.  @>
@ REAL.

\LOG
\initials{LDF 2022.05.17.}
Now reading number into a |double|, setting it to 0.0 if it's |< Point::epsilon()| and
casting it to |real|.  Of course, if |real| is a |typedef| for |double|, this is 
unnecessary.  I would hope that the compiler would optimize this cast out.  However,
at the present time, it doesn't work to have |real| be |double| and in addition, I don't
think there would ever be a reason to have |real| be |double|, so for the time being,
the cast will be needed in any case.
\ENDLOG 

@<Rules@>=
@=REAL[[:space:]]+{NUMBER} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `REAL[[:space:]]+{NUMBER}'." << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   stringstream temp_strm;
   string s;
   double d;
   real r;

   temp_strm << yytext;
   temp_strm >> s;
   temp_strm >> d;

   if (abs(d) < Point::epsilon())
   {
       d = 0.0;
   }

   r = static_cast<real>(d);

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `REAL[[:space:]]+{NUMBER}':" << endl
            << "`r' == " << r << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   scanner_node->metapost_output_struct->real_vector.push_back(r);

@=}@>

@q *** (3) BOX.  @>
@ BOX.

\LOG
\initials{LDF 2022.05.13.}
Added this rule.
\ENDLOG 

@<Rules@>=
@=BOX[[:space:]]+{NUMBER}pt,[[:space:]]+{NUMBER}pt,[[:space:]]+{NUMBER}pt {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  "
            << "`BOX[[:space:]]+{NUMBER}pt\\,[[:space:]]+{NUMBER}pt\\,"
            << "[[:space:]]+{NUMBER}pt'." 
            << endl
            << "`yytext' =="
            << endl 
            << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   stringstream temp_strm;
   string s;
   real wd;
   real ht;
   real dp;

   temp_strm << yytext;

   temp_strm >> s;

   temp_strm >> wd;
   temp_strm >> s;
   temp_strm >> ht;
   temp_strm >> s;
   temp_strm >> dp;

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  `BOX[[:space:]]+{NUMBER}':" << endl
            << "Before conversion:" << endl 
            << "`wd' == " << wd << endl
            << "`ht' == " << ht << endl
            << "`dp' == " << dp << endl;
   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   /* Convert pt to cm  */

   wd *= (2.54 / 72.27);
   ht *= (2.54 / 72.27);
   dp *= (2.54 / 72.27);

   scanner_node->metapost_output_struct->real_vector.push_back(wd);
   scanner_node->metapost_output_struct->real_vector.push_back(ht);
   scanner_node->metapost_output_struct->real_vector.push_back(dp);

@=}@>

@q *** (3) PAIR.  @>
@ PAIR.

\LOG
\initials{LDF 2022.05.13.}
Added this rule.
\ENDLOG 

@<Rules@>=
@=PAIR[[:space:]]*{POINT} {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  "
            << "`PAIR[[:space:]]*{POINT}':" << endl 
            << endl
            << "`yytext' =="
            << endl 
            << yytext << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   if (scanner_node->metapost_output_struct == 0)
      scanner_node->metapost_output_struct = new Metapost_Output_Struct;

   stringstream temp_strm;
   string s;
   char c;
   real x;
   real y;

   temp_strm << yytext;

   temp_strm >> s;
   temp_strm >> c;
   temp_strm >> x;

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "s == " << s << endl
            << "c == " << c << endl
            << "x == " << x << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   temp_strm >> s;
   temp_strm >> y;

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "s == " << s << endl
            << "y == " << y << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->metapost_output_struct->real_vector.push_back(x);
   scanner_node->metapost_output_struct->real_vector.push_back(y);

@=}@>

@q *** (3) Whitespace.  @>
@ Whitespace.
@<Rules@>=
@=[[:space:]] {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  whitespace, ignoring:  \"" << yytext << "\"" << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

@=}@>

@q *** (3) Semi-Colon.  @>
@*4 Semi-Colon

\LOG
\initials{LDF 2022.04.03.}
Added this rule.
\ENDLOG

@<Rules@>=
@=; {@>

#if 0 
   if (DEBUG_SCANNER)
   {
      
      cerr << endl << "In `zzlex':  Semi-colon." << endl;
      
   }
#endif 

@=}@>

@q *** (3) END.  @>
@ END.

@<Rules@>=
@=END {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  END:  Returning." << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   return 0;

@=}@>

@q *** (3) End-of-file (EOF).  @>
@ End-of-file (EOF).

@<Rules@>=
@=<<EOF>> {@>

#if DEBUG_COMPILE
   if (DEBUG)
   {
       cerr << "In `zzlex':  Read EOF.  Returning." << endl;

   }  /* |if (DEBUG)|  */
#endif  /* |DEBUG_COMPILE|  */ 

   return 0;

@=}@>


@ Putting scanner together.
@c
@=%{@>
@<Include files@>@;@/
using namespace std;
@=%}@>
@<Start conditions@>@;@/
@<Options@>@;@/
@<Name definitions@>@;@/
@=%%@>
@=%{@>
@<Local variables for |zzlex|@>@;@/
@=%}@>
@<Execute on entry to |zzlex|@>@;@/
@q Rules @>
@<Rules@>@;@/
@=%%@>
@q User code@>


 
@q * (1) Emacs-Lisp code for use in indirect buffers  @>

@q (progn (cweb-mode) (outline-minor-mode))           @>

@q * (1) Local variables for Emacs @>

@q * Local variables for Emacs.@>
@q Local Variables: @>
@q mode:CWEB @>
@q eval:(display-time) @>
@q abbrev-mode:t @>
@q eval:(read-abbrev-file) @>
@q indent-tabs-mode:nil @>
@q eval:(outline-minor-mode) @>
@q fill-column:80 @>
@q End: @>

