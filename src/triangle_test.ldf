%% triangle_test.ldf
%% Created by Laurence D. Finston (LDF) Mi 13. Nov 17:55:29 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

input "plainldf.lmc";

verbatim_metapost "prologues := 3;outputtemplate := \"%j_%2c.eps\";";
verbatim_metapost "dotlabeldiam:=2mm;labeloffset:=1.5mm;ahlength:=.5cm;";

verbatim_tex "\magnification=2000";

%% * (1)

frame_wd := 18cm;
frame_ht := 15cm;

path frame;

frame :=  (-.5frame_wd, -.5frame_ht) -- (.5frame_wd, -.5frame_ht) -- (.5frame_wd, .5frame_ht)
         -- (-.5frame_wd, .5frame_ht) -- cycle;

%% * (1)

triangle T[];
point p[];
path q[];
numeric ang[];
string s;

numeric_vector mv;
numeric_vector nv;

%% * (1) These parameters don't work.

% !! TODO:  Find out when values are such that a triangle can't be
% created.  Obviously, it's when the sin or cosine rules produce
% infinite values or when there's a division by 0, or possibly when
% a zero value is produced at some point.  However, I'd like to find out
% how one could test values to determine when this will happen rather
% than using trial-and-error.
%
% It might be possible by using circles, somehow.
% LDF 2024.11.13.

% This doesn't work.  LDF 2024.11.13.
% nv += 12;   
% nv += 3.7; 
% nv += 7;

%% * (1) These parameters work.  LDF 2024.11.13.

% This works.  LDF 2024.11.13.
% nv += 10;   
% nv += 3.7; 
% nv += 7;   

% This works.  LDF 2024.11.13.
% nv += 3.7; 
% nv += 8;   
% nv += 7;   

% This works.  LDF 2024.11.13.
% nv += 3.7; 
% nv += 4;   
% nv += 7;   


% This works.  LDF 2024.11.13.
% nv += 3;
% nv += 4;
% nv += 5;

%% * (1)

beginfig(0);
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  % This works.  LDF 2024.11.13.
  nv += 10;   
  nv += 3.7; 
  nv += 7;

  % This doesn't work.  LDF 2024.11.13.
  % nv += 12;   
  % nv += 3.7; 
  % nv += 7;
  
  set T0, nv, mv with_sss;

  if is_invalid T0:
    message "T0 is invalid.";
    endfig;
    end;
  else:
    message "T0 is valid.";
  fi;

  message "mv:";
  show mv;
  
  message "T0:";
  show T0;
  
  draw T0;

  % Both of these calls work:  `mv' is optional.  If present, the
  % lengths of the sides of the triangle and the angles are stored
  % in it.
  % LDF 2024.11.13.

  for i = 0 upto 2:
    p[i] := get_point (i) T0;
    q[i] := get_side (i), T0;
    ang[i] := get_angle (i), T0;
    message "ang" & decimal i & ": " & decimal ang[i];
  endfor;

  p3 := get_center T0;

  draw q0 with_color red with_pen big_pen;
  draw q1 with_color blue with_pen big_pen;
  draw q2 with_color dark_green with_pen big_pen;

  label.bot("$q_0$", mediate(get_point 0 q0, get_point 1 q0)) with_color red;
  label.rt("$q_1$", mediate(get_point 0 q1, get_point 1 q1, .333)) with_color blue;
  label.ulft("$q_2$", mediate(get_point 0 q2, get_point 1 q2)) with_color dark_green;
  
  
  dotlabel.bot("$p_0$", p0);
  dotlabel.bot("$p_1$", p1);
  dotlabel.top("$p_2$", p2);
  dotlabel.bot("$p_3$", p3);

  s := "$\scriptstyle\alpha=" & decimal ang0 & "^\circ$";
  label.ulft(s, p0 shifted (0, 0));

  s := "$\scriptstyle\beta=" & decimal ang1 & "^\circ$";
  label.urt(s, p1 shifted (0, 0));

  s := "$\scriptstyle\gamma=" & decimal ang2 & "^\circ$";
  label.lrt(s, p2 shifted (0, 0));


%% ** (2) A single bisector (path) and a single bisection point.

% This works.  LDF 2024.11.13.

  % q3 := get_bisector 0, T0;
  % message "q3:";
  % show q3;

  % p4 := get_bisection_point 0, T0;

  % dotlabel.rt("$p_4$", p4);

  % draw q3 with_color magenta;

%% ** (2) All bisectors (paths) and bisection points
  
  point_vector pv;
  path_vector  qv;

  pv := get_bisection_points T0;
  qv := get_bisectors T0;

  % message "pv:";
  % show pv;

  % message "qv:";
  % show qv;

  draw qv0 with_color dark_green;
  draw qv1 with_color magenta;
  draw qv2 with_color orange;
  
  for i = 0 upto 2:
    s := decimal i;
    dotlabel.top(s, pv[i]) with_text_color red;
  endfor;

  shift current_picture (-5, 0);
  
endfig with_projection parallel_x_y;
bye;


%% * (1)


%% ** (2) SSS.  This works.

% nv += 1; % 3;
% nv += 2; % 4;
% nv += sqrt 5; % 5;

% set T0, nv, pv with_sss;

% message "T0:";
% show T0;

%% ** (2) SAS.  This works.

% clear nv;
% clear pv;

% nv += 2;
% nv += 30;
% nv += 7;

% set T1, nv, pv with_sas;

% message "T1:";
% show T1;
% pause;


%% ** (2) SSA.  

clear nv;
clear pv;

nv += 2;
nv += 3;
nv += 10;

set T2, nv, pv with_ssa;

message "T2:";
show T2;
%pause;

%end;



%% ** (2)

%% * (1)

pickup medium_pen;

beginfig(0);

  draw frame;
  
  % SAS.  This works.  LDF 2024.11.04.
  % draw T0;

  % for i = 0 upto 2:
  %   p[i] := get_point (i) T0;
  % endfor;

  % dotlabel.bot("$p_0$", p0);
  % dotlabel.bot("$p_1$", p1);
  % dotlabel.top("$p_2$", p2);

  % p3 := get_center T0;

  % if is_valid p3:
  %   dotlabel.bot("$p_3$", p3);
  % fi;

  % SAS.  This works.  LDF 2024.11.04.

  % draw T1;

  % for i = 0 upto 2:
  %   p[4+i] := get_point (i) T1;
  % endfor;

  % dotlabel.bot("$p_4$", p4);
  % dotlabel.bot("$p_5$", p5);
  % dotlabel.top("$p_6$", p6);

  % p7 := get_center T1;

  % dotlabel.bot("$p_7$", p7);

  % SSA
  
  draw T2;

  for i = 0 upto 2:
    p[8+i] := get_point (i) T2;
  endfor;

  dotlabel.bot("$p_8$", p8);
  dotlabel.bot("$p_9$", p9);
  dotlabel.top("$p_{10}$", p10);

  p11 := get_center T2;

  dotlabel.bot("$p_{11}$", p11);
  
endfig with_projection parallel_x_y;


bye;


point p[];
triangle T[];

p0 := (0, 0);
p1 := (1, 1);
p2 := (2, 2);

set T0 with_points (p0, p1, p2);

message "is_valid T0:";
show (is_valid T0);

message "is_invalid T0:";
show (is_invalid T0);

% show T0;
end;




%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

