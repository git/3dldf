%% scale_layouts.lmc
%% Created by Laurence D. Finston (LDF) Sun 17 Jul 2022 08:06:52 AM CEST

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2022, 2023 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Top

%% ** (2) Macro werckmeister_three;

macro werckmeister_three;

def werckmeister_three {numeric sscale_length} =

%% *** (3)
  
  begingroup;

    numeric uunit_size;

    %% uunit_size is a value found by generating a scale and then dividing
    %% in order to get it to fit into the desired scale length.
    %% LDF 2022.07.20.
    
    uunit_size := sscale_length / (2 * 8.4430);

    boolean do_labels;

    do_labels := false; % true
    
    point p[];
    path  q[];
    
    message "Entering `werkmeister_three'.";

      
    message "sscale_length = " & decimal sscale_length;
    message "uunit_size    = " & decimal uunit_size;

    numeric a[];

    a0  := 1;
    a1  := 256/243;
    a2  := (64/81);
    a2  *= (sqrt 2);
    a3  := 32/27;
    a4  := 256/243;
    a4  *= 2 root 4;
    a5  := 4/3;
    a6  := 1024/729;

    a7  := 8 root 4; %% 8 == 2 pow 3;
    a7  *= 8/9;

    a8 := 128/81;

    a9 := 1024/729;
    a9 *= 2 root 4;

    a10 := 16/9;

    a11 := 2 root 4;
    a11 *= 128/81;

    a12 := 2;

    p0 := origin;

    if do_labels:
      dotlabel.lft("$p_0$", p0);
    fi

    label.top("Nut", p0) shifted (0, .25cm);
    draw (p0 shifted (-.25cm, 0)) -- (p0 shifted (.25cm, 0));
    label.lft("\setbox0=\hbox{\quad 0}Unison\hbox to \wd0{\hss}", p0) shifted (-.25cm, 0);
    
    
    q0 += --;
    q0 += p0;

    a_inverse0 := 1;

    numeric shift_val;
    shift_val := .25cm;
    
    for i = 1 upto 12:
      message "a" & decimal i & " = " & decimal a[i];
      a_inverse[i] := 1/a[i];
      message "a_inverse" & decimal i & " = " & decimal a_inverse[i];
      p[i] := p[i-1] shifted (0, -uunit_size * a_inverse[i]);
      q0 += p[i];
      if do_labels:
	dotlabel.lft("$p_{" & decimal i & "}$", p[i]);
      fi
    endfor;

    p13 := p12 shifted (0, -2cm);
    
    label.bot("Saddle, " & decimal sscale_length & "\Thinspace cm", p13) shifted (0, -.5cm);

    
    label.lft("Minor $2^{\rm{nd}}$\quad 1", 	 p1  shifted (-shift_val, 0));
    label.lft("Major $2^{\rm{nd}}$\quad 2", 	 p2  shifted (-shift_val, 0));
    label.lft("Minor $3^{\rm{rd}}$\quad 3", 	 p3  shifted (-shift_val, 0));
    label.lft("Major $3^{\rm{rd}}$\quad 4", 	 p4  shifted (-shift_val, 0));
    label.lft("$4^{\rm{th}}$\quad 5",       	 p5  shifted (-shift_val, 0));
    label.lft("Diminished $5^{\rm{th}}$\quad 6", p6  shifted (-shift_val, 0));
    label.lft("$5^{\rm{th}}$\quad 7",            p7  shifted (-shift_val, 0));
    label.lft("Minor $6^{\rm{th}}$\quad 8",      p8  shifted (-shift_val, 0));
    label.lft("Major $6^{\rm{th}}$\quad 9",      p9  shifted (-shift_val, 0));
    label.lft("Minor $7^{\rm{th}}$\quad 10",     p10 shifted (-shift_val, 0));
    label.lft("Major $7^{\rm{th}}$\quad 11",     p11 shifted (-shift_val, 0));
    label.lft("Octave\quad 12",                  p12 shifted (-shift_val, 0));


    label.rt("$1/1 = " & decimal a0 & " = " & decimal magnitude(ypart p0) 
	& "\Thinspace\rm{cm}$", p0) shifted (shift_val, 0);
    label.rt("$256/243 = " & decimal a1 & " = " & decimal magnitude(ypart p1) 
	& "\Thinspace\rm{cm}$", p1) shifted (shift_val, 0);
    label.rt("$64/81\sqrt 2 = " & decimal a2 & " = " & decimal magnitude(ypart p2) 
	& "\Thinspace\rm{cm}$", p2) shifted (shift_val, 0);
    label.rt("$32/27 = " & decimal a3 & " = " & decimal magnitude(ypart p3) 
	& "\Thinspace\rm{cm}$", p3) shifted (shift_val, 0);
    label.rt("$256/243\root 4 \of 2 = " & decimal a4 & " = " & decimal magnitude(ypart p4) 
	& "\Thinspace\rm{cm}$", p4) shifted (shift_val, 0);
    label.rt("$4/3 = " & decimal a5 & " = " & decimal magnitude(ypart p5) 
	& "\Thinspace\rm{cm}$", p5) shifted (shift_val, 0);
    label.rt("$1024/729 = " & decimal a6 & " = " & decimal magnitude(ypart p6) 
	& "\Thinspace\rm{cm}$", p6) shifted (shift_val, 0);
    label.rt("$8/9\root 4 \of {2^3} = " & decimal a7 & " = " & decimal magnitude(ypart p7) 
	& "\Thinspace\rm{cm}$", p7) shifted (shift_val, 0);
    label.rt("$128/81 = " & decimal a8 & " = " & decimal magnitude(ypart p8) 
	& "\Thinspace\rm{cm}$", p8) shifted (shift_val, 0);
    label.rt("$1024/729\root 4 \of 2 = " & decimal a9 & " = " & decimal magnitude(ypart p9)
	& "\Thinspace\rm{cm}$", p9) shifted (shift_val, 0);
    label.rt("$16/9 = " & decimal a10 & " = " & decimal magnitude(ypart p10) 
	& "\Thinspace\rm{cm}$", p10) shifted (shift_val, 0);
    label.rt("$128/81\root 4 \of 2 = " & decimal a11 & " = " & decimal magnitude(ypart p11) 
	& "\Thinspace\rm{cm}$", p11) shifted (shift_val, 0);
    label.rt("$2/1 = " & decimal a12 & " = " & decimal magnitude(ypart p12) 
	& "\Thinspace\rm{cm}$", p12) shifted (shift_val, 0);


%% *** (3) Scale (Ruler)

    
    p14 := p0 shifted (9cm, 0);
    p15 := p14 shifted (0, -ceil(scale_length / 2));

    draw p14 -- p15 with_pen small_pen;
    
    for i = 0 upto ceil(sscale_length / 2):
      draw (p14 shifted (-1cm, 0) -- p14) shifted (0, -i) with_pen small_pen;
      label.rt(decimal i & "cm", p14 shifted (0, -i)) shifted (.5shift_val, 0);
    endfor;

    for i = -.5 step -.5 until floor((-sscale_length / 2)):
      draw (p14 shifted (-.5cm, 0) -- p14) shifted (0, i) with_pen small_pen;
    endfor;

    for i = -.1 step -.1 until floor((-sscale_length / 2)):
      draw (p14 shifted (-.25cm, 0) -- p14) shifted (0, i) with_pen small_pen;
    endfor;

%% *** (3) Pure intervals (harmonics)

    p16 := p0 shifted (0, -sscale_length);
    
    p212 := mediate(p0, p16, 1/2); %% Octave
    p207 := mediate(p0, p16, 1/3);  %% Perfect 5th
    p204 := mediate(p0, p16, 1/5);  %% Maj. 3rd
    p205 := mediate(p0, p16, 1/4);  %% 4th
    p202 := mediate(p0, p16, 1/9);  %% 2nd
    p209 := mediate(p0, p16, 2/5);  %% 6th
    p211 := mediate(p0, p16, 7/15); %% Maj. 7th

    numeric u;

    %% 5th

    u := ((magnitude(magnitude(ypart p207) - magnitude(ypart p7))) * 100) / (magnitude(ypart p207));
    
    drawdot p207 with_color green with_pen dot_pen;
    label.lrt("$ 5^{\rm{th}}$\hskip-.25ex, $3/2$, $"
    	& decimal -ypart p207 & "\Thinspace\rm{cm}$, $"
	& "\Delta " & decimal magnitude(ypart p207 - ypart p7)	& "\Thinspace\rm{cm}"
	& "="
	& "\Delta" &  decimal u & "\%"
	& "$", p207) shifted (.25cm, -6pt)
      with_color dark_green;

    draw p207 shifted (-1.25mm, 0) -- p207 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;


    %% Maj. 3rd

    u := ((magnitude(magnitude(ypart p204) - magnitude(ypart p4))) * 100) / (magnitude(ypart p204));
    
    drawdot p204 with_color green with_pen dot_pen;
    label.urt("$ 3^{\rm{rd}}$\hskip-.25ex, $5/4$, $"
    	& decimal -ypart p204 & "\Thinspace\rm{cm}$, "
	& "$\Delta " & decimal magnitude(ypart p204 - ypart p4)
	& "\Thinspace\rm{cm} =\Thinspace \Delta" &  decimal u & "\%$"
	, p204) shifted (.25cm, 0pt)
      with_color dark_green;

    draw p204 shifted (-1.25mm, 0) -- p204 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;

    %% 4th

    u := ((magnitude(magnitude(ypart p205) - magnitude(ypart p5))) * 100) / (magnitude(ypart p205));
    
    drawdot p205 with_color green with_pen dot_pen;
    label.lrt("$ 4^{\rm{th}}$\hskip-.25ex, $3/2$, $"
    	& decimal -ypart p205 & "\Thinspace\rm{cm}$, $"
	& "\Delta " & decimal magnitude(ypart p205 - ypart p5)	& "\Thinspace\rm{cm}"
	& "="
	& "\Delta" &  decimal u & "\%"
	& "$", p205) shifted (.25cm, -6pt)
      with_color dark_green;

    draw p205 shifted (-1.25mm, 0) -- p205 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;

    %% 2nd

    u := ((magnitude(magnitude(ypart p202) - magnitude(ypart p2))) * 100) / (magnitude(ypart p202));
    
    drawdot p202 with_color green with_pen dot_pen;
    label.lrt("$ 2^{\rm{nd}}$\hskip-.25ex, $9/8$, $"
    	& decimal -ypart p202 & "\Thinspace\rm{cm}$, $"
	& "\Delta " & decimal magnitude(ypart p202 - ypart p2)	& "\Thinspace\rm{cm}"
	& "="
	& "\Delta" &  decimal u & "\%"
	& "$", p202) shifted (.25cm, -6pt)
      with_color dark_green;

    draw p202 shifted (-1.25mm, 0) -- p202 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;

    %% 6th

    u := ((magnitude(magnitude(ypart p209) - magnitude(ypart p9))) * 100) / (magnitude(ypart p209));
    
    drawdot p209 with_color green with_pen dot_pen;
    label.urt("$6^{\rm{th}}$\hskip-.25ex, $5/3$, $"
    	& decimal -ypart p209 & "\Thinspace\rm{cm}$, $"
	& "\Delta " & decimal magnitude(ypart p209 - ypart p9)	& "\Thinspace\rm{cm}"
	& "="
	& "\Delta" &  decimal u & "\%"
	& "$", p209) shifted (.25cm, 0pt)
      with_color dark_green;

    draw p209 shifted (-1.25mm, 0) -- p209 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;


    %% Maj. 7th

    u := ((magnitude(magnitude(ypart p211) - magnitude(ypart p11))) * 100) / (magnitude(ypart p211));
    
    drawdot p211 with_color green with_pen dot_pen;
    label.urt("Maj.~$7^{\rm{th}}$\hskip-.25ex, $5/3$, $"
    	& decimal -ypart p211 & "\Thinspace\rm{cm}$, $"
	& "\Delta " & decimal magnitude(ypart p211 - ypart p11)	& "\Thinspace\rm{cm}"
	& "="
	& "\Delta" &  decimal u & "\%"
	& "$", p211) shifted (.25cm, 1pt)
      with_color dark_green;

    draw p211 shifted (-1.25mm, 0) -- p211 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;


%% Octave    
    
    drawdot p212 with_color green with_pen dot_pen;
    label.lrt("Octave", p212) shifted (.25cm, -6pt) with_color dark_green;
 

%% *** (3)
    
    draw q0;
    drawarrow p12 -- p13;

    for i = 1 upto 12:
      if not do_labels:
	draw (p[i] shifted (-.25cm, 0)) -- (p[i] shifted (.25cm, 0))
	  with_pen small_pen;
      fi
    endfor;

%% *** (3)

    message "Exiting `werkmeister_three'.";

  endgroup;
  
enddef;

%% ** (2) Macro layout

def layout {point ttop,
            path Q,
            point_vector PV,
            numeric llength,
            numeric ffret_ctr,
            numeric ffret_thickness,
            numeric sstart,
            numeric eend,
            numeric oovershoot_top,
            numeric oovershoot_bot,
	    boolean ddo_scriptstyle
            } = 


%% *** (3)
    
    begingroup;

    message "Entering layout.";

    % message "ddo_scriptstyle = ";
    % show ddo_scriptstyle;


    numeric shift_val;
    
    
    if ddo_scriptstyle:
      verbatim_tex "\let\SSOPT=\scriptstyle";
      shift_val := 0;
    else:
      verbatim_tex "\let\SSOPT=\relax";
      shift_val := .25cm;
    fi
    
    % message "point ttop:";
    % show ttop;
    % message "llength           == " & decimal llength; 
    % message "ffret_ctr         == " & decimal ffret_ctr; 
    % message "ffret_thickness   == " & decimal ffret_thickness; 
    % message "sstart           == " & decimal sstart;

    %message "oovershoot_top   == " & decimal oovershoot_top;
    %message "oovershoot_bot   == " & decimal oovershoot_bot;
    %pause;

    point p[];
    numeric n[];

    clear pv;

    % message "sstart == " & decimal sstart;
    % message "eend == " & decimal eend;
    % pause;
    
    if eend == 0:
      eend := 1000;
    fi

    % message "sstart == " & decimal sstart;
    % message "eend == " & decimal eend;
    % pause;

    
    p1000   := ttop;
    p0 := ttop shifted (0, -llength);

    
    numeric a;
    a := 1.0/alpha;
    
    n1 := llength*a;
    p1 := p1000 shifted (0, -n1);

    
    numeric k;

    if ffret_ctr > ref_fret:
      k := ffret_ctr;
    else:
      k := ref_fret;
    fi

    for i = 2 upto k:
      n[i] := n[i-1] * a;
      %message "n" & decimal i;
      p[i] := p1000 shifted (0, -n[i]);
    endfor;

    % message "mags:";
    % show magnitude(p12 - p0);
    % show magnitude(p1000 - p12);    
    % pause;
    
    % n2 := n1*a;
    % p2 := p1000 shifted (0, -n2);

    % n3 := n2*a;
    % p3 := p1000 shifted (0, -n3);
    
    % for i = 99 downto 93:
    %   n[i] := a * n[i+1];
    %   p[i] := p0 shifted (0, n[i]);
    % endfor;

    transform t[];
    t0 := identity rotated (0, 0, 180);

    p0 *= p1000 *= t0;

    t1 := identity shifted (-xpart p0, -ypart p0);

    p0 *= p1000 *= t1;

    %t2 := identity scaled (0, m3);
   

    %p0 *= p1000 *= t2;
    
    for i = 1 upto k:
      p[i] *= t0;
      p[i] *= t1;
    endfor;

    
    % show p0;

    % message "p[ref_fret}:";
    % show p[ref_fret];
    %pause;
    
    boolean do_labels;
    do_labels := false; % true;

    pen small_pen;
    small_pen := pencircle scaled (.25mm, .25mm, .25mm);

    pen medium_pen;
    medium_pen := pencircle scaled (.333mm, .333mm, .333mm);

    pen dot_pen;
    dot_pen := pencircle scaled (2.5mm, 2.5mm, 2.5mm);

    % show ypart p1000;
    % show ypart p0;
    % show llength;
    %pause;

    pen tiny_pen;
    tiny_pen := pencircle scaled (.175mm, .175mm, .175mm);

    p212 := mediate(p0, p1000, 1/2);  %% Octave
    p207 := mediate(p0, p1000, 1/3);  %% Perfect 5th
    p204 := mediate(p0, p1000, 1/5);  %% Maj. 3rd
    p205 := mediate(p0, p1000, 1/4);  %% 4th
    p202 := mediate(p0, p1000, 1/9);  %% 2nd
    p209 := mediate(p0, p1000, 2/5);  %% 6th
    p211 := mediate(p0, p1000, 7/15); %% Maj. 7th
    
   
    % message "p0:";
    % show p0;
    % pause;

    numeric temp_val;


%% *** (3)
    
    numeric m[];
    numeric u;
    
    if (sstart > 0) and (sstart < ffret_ctr):
      p300 := p[sstart];
      message "p" & decimal sstart & ":";
      show p[sstart];
      m0 := floor(magnitude(ypart p300));
      m0 -= oovershoot_top;
      m0 := max(m0, 0);
      p302 := (xpart p0, -m0);      
    else:
      m0 := magnitude(ypart p0);
      p302 := p300 := p0;
      sstart := 0;
    fi

    if (eend > 1) and (eend > sstart) and (eend <= ffret_ctr):
      p301 := p[eend];
      message "p" & decimal eend & ":";
      show p[eend];
      m1 := ceil(magnitude(ypart p301));
      m1 += oovershoot_bot;
      p303 := (xpart p0, -m1);
    else:
      m1 := magnitude(ypart p1000);
      p303 := p301 := p1000;
      eend := 1000;
    fi
    
    message "m0 = " & decimal m0;
    message "m1 = " & decimal m1;

    if (magnitude(ypart p212) >= magnitude(ypart p302)) and (magnitude(ypart p212) <= magnitude(ypart p303)):
      drawdot p212 with_color green with_pen dot_pen;
      label.urt("Octave", p212) shifted (2pt, 6pt) with_color dark_green;
    fi

    %% 2nd

    if (magnitude(ypart p202) >= magnitude(ypart p302)) and (magnitude(ypart p202) <= magnitude(ypart p303)):
      drawdot p202 with_color green with_pen dot_pen;
      label.lrt("$\SSOPT 2^{\rm{nd}}$\hskip-.25ex, $\SSOPT 9/8$, $\SSOPT "
          & decimal -ypart p202 & "\Thinspace\rm{cm}$", p202) shifted (shift_val, -3pt)
        with_color dark_green;
      draw p202 shifted (-1.25mm, 0) -- p202 shifted (1.25mm, 0) with_pen tiny_pen;
      u := ((magnitude(magnitude(ypart p202) - magnitude(ypart p2))) * 100) / (magnitude(ypart p202));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p2)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT " & decimal (ypart p202 - ypart p2) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p2);
    fi

    %% 5th
    
    if (magnitude(ypart p207) >= magnitude(ypart p302)) and (magnitude(ypart p207) <= magnitude(ypart p303)):
      drawdot p207 with_color green with_pen dot_pen;
      label.lrt("$\SSOPT 5^{\rm{th}}$\hskip-.25ex, $\SSOPT 3/2$, $\SSOPT "
          & decimal -ypart p207 & "\Thinspace\rm{cm}$", p207) shifted (shift_val, -5pt)
        with_color dark_green;
      draw p207 shifted (-1.25mm, 0) -- p207 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;
      u := ((magnitude(magnitude(ypart p207) - magnitude(ypart p7))) * 100) / (magnitude(ypart p207));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p7)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT " & decimal (ypart p207 - ypart p7) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p7);
    fi

    %% Maj. 3rd
    
    if (magnitude(ypart p204) >= magnitude(ypart p302)) and (magnitude(ypart p204) <= magnitude(ypart p303)):
      drawdot p204 with_color green with_pen dot_pen;
      label.urt("$\SSOPT 3^{\rm{rd}}$\hskip-.25ex, $\SSOPT 5/4$, $\SSOPT "
          & decimal -ypart p204 & "\Thinspace\rm{cm}$", p204) shifted (shift_val, 0pt)
        with_color dark_green;
      draw p204 shifted (-1.25mm, 0) -- p204 shifted (1.25mm, 0) with_pen tiny_pen;
      u := ((magnitude(magnitude(ypart p204) - magnitude(ypart p4))) * 100) / (magnitude(ypart p204));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p4)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT +" & decimal (ypart p204 - ypart p4) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p4);
    fi

    %% 4th

    if (magnitude(ypart p205) >= magnitude(ypart p302)) and (magnitude(ypart p205) <= magnitude(ypart p303)):
      drawdot p205 with_color green with_pen dot_pen;
      label.urt("$\SSOPT 4^{\rm{th}}$\hskip-.25ex, $\SSOPT 4/3$, $\SSOPT "
          & decimal -ypart p205 & "\Thinspace\rm{cm}$", p205) shifted (shift_val, 3pt)
        with_color dark_green;
      draw p205 shifted (-1.25mm, 0) -- p205 shifted (1.25mm, 0) with_pen tiny_pen with_color magenta;
      u := ((magnitude(magnitude(ypart p205) - magnitude(ypart p5))) * 100) / (magnitude(ypart p205));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p5)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT +" & decimal (ypart p205 - ypart p5) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p5);
    fi

    %% 6th
    
    if (magnitude(ypart p209) >= magnitude(ypart p302)) and (magnitude(ypart p209) <= magnitude(ypart p303)):
      drawdot p209 with_color green with_pen dot_pen;
      label.urt("$\SSOPT 6^{\rm{th}}$\hskip-.25ex, $\SSOPT 5/3$, $\SSOPT "
          & decimal -ypart p209 & "\Thinspace\rm{cm}$", p209) shifted (shift_val, 0pt)
        with_color dark_green;
      draw p209 shifted (-1.25mm, 0) -- p209 shifted (1.25mm, 0) with_pen tiny_pen;
      u := ((magnitude(magnitude(ypart p209) - magnitude(ypart p9))) * 100) / (magnitude(ypart p209));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p9)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT +" & decimal (ypart p209 - ypart p9) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p9);
    fi

    %% Maj. 7th
    
    if (magnitude(ypart p211) >= magnitude(ypart p302)) and (magnitude(ypart p211) <= magnitude(ypart p303)):
      drawdot p211 with_color green with_pen dot_pen;
      label.urt("Maj.~$\SSOPT  7^{\rm{th}}$\hskip-.25ex, $\SSOPT  15/8$, $\SSOPT "
          & decimal -ypart p211 & "\Thinspace\rm{cm}$", p211) shifted (shift_val, -.25pt)
        with_color dark_green;
      draw p211 shifted (-1.25mm, 0) -- p211 shifted (1.25mm, 0) with_pen tiny_pen;
      u := ((magnitude(magnitude(ypart p211) - magnitude(ypart p11))) * 100) / (magnitude(ypart p211));
      label.rt("\setbox0=\hbox{" & decimal (-1 * ypart p11)
          & "\Thinspace cm}\lower10pt\hbox{\hskip\wd0\hskip.75em , "
          & "$\SSOPT +" & decimal (ypart p211 - ypart p11) & "\Thinspace =\Thinspace \Delta"
          & decimal u & "\%$}",
          p11);
    fi
    
    % pause;
    
    message "p0:";
    show p0;

    message "p300:";
    show p300;

    message "p301:";
    show p301;

    %pause;

    if (sstart == 0) and (eend == 1000):
      draw p0 -- p1000;
    else:
      draw p302 -- p303;
    fi

    draw (p302 -- p303) shifted (-2cm, 0) with_pen pencircle scaled (.25mm, .25mm, .25mm);
    
    %draw (p0 -- p1000) shifted (-2cm, 0);
    

    if sstart == 0:
      if do_labels:
        dotlabel.lft("$p_0$", p0);
      else:
        label.top("Nut", p0) shifted (0, 2pt);
        draw (p0 shifted (-.25cm, 0)) -- (p0 shifted (.25cm, 0)) with_pen medium_pen;
      fi
    %dotlabel.lft("$p_{100}$", p1000);
    fi

    

    if eend == 1000:
      draw (p1000 shifted (-.25cm, 0)) -- (p1000 shifted (.25cm, 0)) with_pen medium_pen;
      label.bot("Saddle", p1000) shifted (0, -2pt);
    fi
    
    % message "p[ref_fret]:";
    % show p[ref_fret];
    %pause;

    for i = m0 upto m1:
      draw (p0 shifted (-2cm, 0) -- p0 shifted (-1cm, 0)) shifted (0, -i);
      label.lft(decimal i & "cm", (p0 shifted (-2cm, 0)) shifted (0, -i));
    endfor;

    for i = (m0 + .5) step .5 until (m1 - .5):
      draw (p0 shifted (-2cm, 0) -- p0 shifted (-1.5cm, 0)) shifted (0, -i);
    endfor;

    
    for i = (m0 + .1) step .1 until m1:
      draw (p0 shifted (-2cm, 0) -- p0 shifted (-1.75cm, 0)) shifted (0, -i);
    endfor;

    if eend < 1000:
      k := eend;
    fi
      
    
    for i = sstart upto k:
      pv += p[i];
      if do_labels:%
        dotlabel.lft("$p_{" & decimal i & "}$", p[i]);
      else:
        if i <= ffret_ctr:
          label.lft(decimal i, p[i]) shifted (-.333cm, 0);
          draw (p[i] shifted (-.25cm, 0)) -- (p[i] shifted (.25cm, 0)) with_pen small_pen;
        % elseif i < 25:
        %   dotlabel.lft(decimal i, p[i]) shifted (-2pt, 0) with_color dark_gray;
        fi
        temp_val := -1 * ypart p[i];
        if (i > 0) and (i <= ffret_ctr):
          label.rt(decimal temp_val & "\Thinspace cm", p[i]) shifted (.25cm, 0);
        % elseif i < 25:
        %   label.rt(decimal temp_val & "cm", p[i]) shifted (.125cm, 0)
        %     with_text_color dark_gray;
          fi
        fi
    endfor;


    message "Exiting layout.";

    pickup pencircle scaled (.333mm, .333mm, .333mm);
    
  endgroup;


%% *** (3) End of layout definition
  
enddef;

%% ** (2)

endinput;

%% * (1) Local variables for Emacs

%% Local Variables:
%% mode: MetaPost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

