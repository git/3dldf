%% octahedra.tex
%% Created by Laurence D. Finston (LDF) Mi 13. Dez 22:13:52 CET 2023

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
%%%% Copyright (C) 2023 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version.  

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details.  

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html.

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for sending 
%%%% announcements to users. To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1) Top

%% * (1)

\input eplain
\input epsf 
\nopagenumbers
\input colordvi

\enablehyperlinks[dvipdfm]
\hlopts{bwidth=0}

%% * (1)

%% \special{papersize=210mm, 297mm} %% DIN A4 Portrait
%% \hsize=210mm
%% \vsize=297mm

\special{papersize=594mm, 420mm} %% DIN A2 Landscape
\hsize=594mm
\vsize=420mm

\advance\voffset by -1in
\advance\hoffset by -1in


\topskip=0pt
\parskip=0pt
\parindent=0pt

\font\large=cmr12 scaled 4000

%% *** (3) 

\def\epsfsize#1#2{.8#1}

%% *** (3) 

\def\A#1{\vbox to \vsize{\vskip2cm
\line{\hskip2cm\epsffile{octahedra_#1.eps}\hss}\vskip1cm
\line{\hskip2cm {\large Fig. #1}\hfil}
\vss}
\eject}

\newtoks\zerotoks
\zerotoks={00}
\newcount\figcnt

\zerotoks={00}
\figcnt=1
\loop
\A{\the\zerotoks\the\figcnt}
\advance\figcnt by 1
\ifnum\figcnt=100\zerotoks={}\else
\ifnum\figcnt=10\zerotoks={0}\fi
\fi
\ifnum\figcnt<250\repeat



\bye

  
%% ** (2) 

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not evaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (plain-tex-mode) (outline-minor-mode t) (setq outline-regexp "%% [*\f]+"))    

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:plain-TeX
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% abbrev-mode:t
%% outline-regexp:"%% [*\f]+"
%% auto-fill-function:nil
%% end:
