%% presents.ldf
%% Created by Laurence D. Finston (LDF) Do 2. Mai 15:44:13 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)


input "plainldf.lmc";


medium_pen := pencircle scaled (.333mm, .333mm, .333mm);

verbatim_tex   "\magnification=4500\font\smallrm=cmr10 scaled 800";

verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";dotlabeldiam:=4mm;";

verbatim_metapost "ahlength := .5cm;";

focus f;

set f with_position (0, 0, -10) with_direction (0, 0, 10) with_distance 20;

point f_pos;
point f_dir;

f_pos := get_position f;
f_dir := get_direction f;
point p[];
path q[];
picture v[];
transform t[];

polyhedron P[];

numeric frame_wd;
numeric frame_ht;

frame_wd := 600mm;
frame_ht := 337.5mm;

frame_wd += 8mm;
frame_ht += 8mm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));


for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

p4 := get_center frame;

pickup medium_pen;

point_vector face_centers;
point_vector face_normals;
point_vector face_perpendiculars;

string s;

boolean do_labels;
do_labels := true; % false;

boolean do_png;

do_png := false; % true; %

if do_png:
  do_labels := false;
fi;

color_vector cv;

cv += red;
cv += blue;
cv += dark_green;
cv += violet;

path_vector qv;

%% * (1) Fig. 0.

fig_ctr := 0;

pickup big_pen;

beginfig(fig_ctr);

%% ** (2)

  message "Fig. 0";
 
  draw frame with_pen big_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2) Top line
  
  p5 := p3 shifted (0, -7);

  if do_labels:
    dotlabel.ulft("$p_5$", p5);
  fi;
  
%% *** (3) a_letter_ss

  clear qv;
  
  glyph a_letter_ss;

  a_letter_ss := get_glyph 97 from "cmss10";

  qv := get_paths from a_letter_ss;

  scale qv (10, 10);

  q0 := qv0;
  q1 := qv1;
  q2 := qv2;

  n := (size q0 - 1);

  if false: % true: % 
    for i = 0 upto n:
      s := "{$\scriptstyle " &  decimal i & "$}";
      dotlabel.top(s, get_point (i) q0) with_color red;
    endfor;
  fi;

  p6 := p5 shifted (2, -.5);

  if do_labels:
    dotlabel.llft("$\scriptstyle p_6$", p6);
  fi;  

  p7 := (xpart get_point 7 q0, ypart get_point 8 q0);

  shift q0 by (p6 - p7);
  shift q1 by (p6 - p7);
  shift q2 by (p6 - p7);

  p7  := get_point 1 q2;
  p8  := get_point 2 q2;
  p9  := (xpart p6, ypart p8);
  p10 := mediate(p6, p8);

  if do_labels:
    dotlabel.bot("$\scriptstyle p_{7}$", p7);
    dotlabel.top("$\scriptstyle p_{8}$", p8);
    dotlabel.top("$\scriptstyle p_{9}$", p9);
    dotlabel.bot("$\scriptstyle p_{10}$", p10);
    label("$\scriptstyle q_{0}$", mediate(p9, p8) shifted (0, -1.25));
    label("$\scriptstyle q_{1}$", p10 shifted (0, -1.5));
    label.top("$\scriptstyle q_{2}$", mediate(p9, p8)) with_color dark_gray;
  fi;

  q2 := p6 -- p7 -- p8 -- p9 -- cycle;
  
  draw q2 with_color dark_gray;
  draw q0;
  draw q1;


%% *** (3) L_letter_ss

  clear qv;
  
  glyph L_letter_ss;

  L_letter_ss := get_glyph 76 from "cmss10";

  qv := get_paths from L_letter_ss;

  scale qv (10, 10);

  for i = 0 upto 1:
    q[i+3] := qv[i];
  endfor;

  n := (size q3 - 1);

  if false: % true: % 
    for i = 0 upto n:
      s := "{$\scriptstyle " &  decimal i & "$}";
      dotlabel.top(s, get_point (i) q3) with_color red;
    endfor;
  fi;

  p11 := p7 shifted (3, -2.5);

  if do_labels:
    dotlabel.bot("$\scriptstyle p_{11}$", p11);
  fi;  

  for i = 4 downto 3:
    shift q[i] by (p11 - get_point 2 q3);
  endfor;

  p12 := get_point 1 q4;
  p13 := get_point 2 q4;
  p14 := (xpart p11, ypart p13);
  p15 := mediate(p11, p13);

  if do_labels:
    dotlabel.bot("$\scriptstyle p_{12}$", p12);
    dotlabel.top("$\scriptstyle p_{13}$", p13);
    dotlabel.top("$\scriptstyle p_{14}$", p14);
    dotlabel.bot("$\scriptstyle p_{15}$", p15);
    label("$\scriptstyle q_{3}$", p11 shifted (2, 2));
    label.top("$\scriptstyle q_{4}$", mediate(p13, p14)) with_color dark_gray;
  fi;
  
  q4 := p11 -- p12 -- p13 -- p14 -- cycle;
  
  draw q4 with_color dark_gray;
  draw q3;


endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1)

P0 := unit_tetrahedron scaled (5, 5, 5);

point_vector cv;
point_vector pv;

point u[];

beginfig(fig_ctr);
  draw frame with_pen big_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  q103 := q3 shifted by (p0 - p11);
  shift q103 (1, 0, 10);

  q104 := q4 shifted by (p0 - p11);
  shift q104 (1, 0, 10);

  p111 := p0 shifted (1, 0, 10);

  p115 := p15 shifted by (p111 - p11);

  % if do_labels:
  %   dotlabel.bot("$\scriptstyle p_{111}$", p111);
  %   dotlabel.bot("$\scriptstyle p_{115}$", p115);
  % fi;

  u0 := p115 shifted (0, 0, 1);

  
  % draw q103;
  % draw q104;

  draw P0;

  % !! START HERE:  LDF 2024.05.02.  get_face_perpendiculars doesn't work right.
  

  cv := get_face_centers P0;

  pv := get_face_normals P0;

  for i = 0 upto 3:
    shift pv[i] by cv[i];
  endfor;
  
  % reg_polygon temp_polygon;
  
  % for i = 0 upto 3:
  %   temp_polygon := get_reg_polygon (i) P0;
  %   pv += (get_normal temp_polygon); %
  %   pv[i] *= -1;
  %   shift pv[i] by cv[i];
  % endfor;

  n := (size cv) - 1;

  for i = 0 upto n:
    drawarrow cv[i] -- pv[i];
    s := "$c_{" & decimal i & "}$";
    dotlabel.bot(s, cv[i]);
  endfor;

  t0 := align (cv1 -- pv1) with_axis z_axis;

  invert t0;

  q203 := q103 shifted by (origin - p115);
  scale q203 (.5, .5, .5);
  rotate q203 (0, 180, 90);
  
  q203 *= t0;

  rotate_around q203 (cv1, pv1) 10;
  
  fill q203 with_color red;
  draw q203;
  
  v0 := current_picture;
  
endfig with_focus f;
fig_ctr += 1;




%% * (1) Main loop

for i = 0 step 5 until 180:
  beginfig(fig_ctr);
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    current_picture := v0;

    rotate current_picture (0, -i);
    
  endfig with_focus f;

  fig_ctr += 1;
endfor;



%% * (1)
  
message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
pause;

bye;


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

