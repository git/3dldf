/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) So 21. Apr 15:45:15 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

#if 0 
dv += yellow;
dv += violet;
dv += pink;
dv += rose_madder_rgb;
dv += Melon_rgb;
dv += teal_blue_rgb;


cv += red;
cv += green;
cv += blue;
cv += cyan;
cv += magenta;
cv += orange;

#endif 

/* ** (2) */

/* * (1) */


int
call_system(stringstream &s);

/* * (1) */

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;   
   string zeroes_1; 
   string zeroes_2; 

   int i = 0;
   int j = 0;
   int k = 0;

   for (int i = 13; i <= 413; ++i, ++j, ++k)
   {    

       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       if (k < 10)
         zeroes_2 = "000";
       else if (k < 100)
         zeroes_2 = "00";
       else if (k < 1000)
         zeroes_2 = "0";
       else
         zeroes_2 = "";

       s.str("");

       cerr << "s.str() == " << s.str() << endl;

       status = call_system(s); 

       if (status == 0)
       {
          cerr << "`call_system' succeeded, returning 0." << endl;
       }

       /* If |call_system| fails, the program exits.  */

   } /* |for|  */

  return 0;

}  /* End of |main| definition  */


#if 0

/* * (1) */

/* ** (2) */

       s << "cp B" << zeroes << i << ".png C" << zeroes_1 << j++ << ".png;";

       if (i == 13 || i == 264 || i == 413)
       {
           for (int k = 0; k <= 19; ++k)
           {
              if (j < 10)
                zeroes_1 = "000";
              else if (j < 100)
                zeroes_1 = "00";
              else if (j < 1000)
                zeroes_1 = "0";
              else
                zeroes_1 = "";

              s << "cp B" << zeroes << i << ".png C" << zeroes_1 << j++ << ".png;";
           }
       }

       else if (i == 72 || i == 100 || i == 159 || i == 264)
       {
           for (int k = 0; k <= 9; ++k)
           {
              if (j < 10)
                zeroes_1 = "000";
              else if (j < 100)
                zeroes_1 = "00";
              else if (j < 1000)
                zeroes_1 = "0";
              else
                zeroes_1 = "";

              s << "cp B" << zeroes << i << ".png C" << zeroes_1 << j++ << ".png;";
           }
       }




/* ** (2) */

#if 0 
       s << "convert -fill " << dark_green << " -opaque white frame_blue_white_background.png "
         << "frame_blue_dark_green_background.png;";
#endif 


/* ** (2) */


       s << "composite A" << zeroes << i << ".png frame_blue_dark_green_background.png B.png && ";

/* *** (3) Red-orange on Yellow */

       // Background
        s << "convert -transparent yellow B.png C.png && " 
          << "composite C.png ./Backgrounds/Violett_plus_Zinkweiss.png D.png && ";

       // Letter
        s << "convert -transparent red D.png E.png && " 
          << "composite E.png ./Backgrounds/Rotorange_gouache.png F.png && ";

/* *** (3) Red on Green*/

       // Background
        s << "convert -transparent violet F.png G.png && " 
          << "composite G.png ./Backgrounds/Zinnobergruen_gouache.png H.png && ";

       // Letter
        s << "convert -transparent " << green << " H.png I.png && " 
          << "composite I.png ./Backgrounds/Karminrot_gouache.png J.png && ";

/* *** (3) Yellow on Blue */

       // Background
        s << "convert -transparent " << melon << " J.png K.png && " 
          << "composite K.png ./Backgrounds/Kobaltblau_hell_gouache.png L.png && ";

       // Letter
        s << "convert -transparent " << magenta << " L.png M.png && " 
          << "composite M.png ./Backgrounds/Kadmiumgelb_gouache.png N.png && ";

/* *** (3) Green on Yellow */

       // Background
        s << "convert -transparent " << rose_madder << " N.png O.png && " 
          << "composite O.png ./Backgrounds/Indischgelb_gouache.png P.png && ";

       // Letter
        s << "convert -transparent " << cyan << " P.png Q.png && " 
          << "composite Q.png ./Backgrounds/Kadmiumgruen_gouache.png R.png && ";


/* *** (3) Red-purple on ochre */

       // Background
        s << "convert -transparent " << teal_blue << " R.png S.png && " 
          << "composite S.png ./Backgrounds/Lichter_ocker_gouache.png T.png && ";

       // Letter
        s << "convert -transparent " << orange << " T.png U.png && " 
          << "composite U.png ./Backgrounds/Purpurmagenta.png V.png && ";

/* *** (3) Background only */         

       // Background
        s << "convert -transparent " << pink << " V.png W.png && " 
          << "composite W.png ./Backgrounds/Sienna_natur_1_coat_gouache.png X.png && ";

/* *** (3) */

/* ** (2) */


       s << "mogrify -fill white -opaque " << dark_green << " X.png && ";

       s << "cp X.png B" << zeroes << i << ".png;";



/* ** (2) */

       s << "composite frame_black_with_trans_background.png "
         << "rotations_" << zeroes << i << ".png A" << zeroes << i << ".png && "
         << "mogrify -fill black -opaque " << purple << " A" << zeroes << i << ".png;";

/* ** (2) */

#endif 

/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        