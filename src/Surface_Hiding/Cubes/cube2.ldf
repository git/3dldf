%% cube2.ldf
%% Created by Laurence D. Finston (LDF) Mo 8. Apr 14:02:15 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)


do_labels := true; % false; %

beginfig(fig_ctr);
  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  draw c0 with_color purple;

  path N_letter[];      % Changed 2024.04.10.
  path U_letter[];      % Changed 2024.04.10.
  path Seven[];         % Changed 2024.04.10.
  path Hash_symbol[];   % Changed 2024.04.10.
  path I_letter[];      % Changed 2024.04.10.
  path K_letter[];      % Changed 2024.04.10.


  if do_labels:
  
    for i = 0 upto 5:
      s := "{\smallrm " & decimal i & "}";
      label(s, P[i]) with_color cv[i];
    endfor;

    
    label.bot("{\smallrm v0}", vertices0);
    label.lrt("{\smallrm v1}", vertices1);
    label.rt("{\smallrm v2}", vertices2);
    label.urt("{\smallrm v3}", vertices3);
    label.ulft("{\smallrm v4}", vertices4);
    label.urt("{\smallrm v5}", vertices5);
    label.llft("{\smallrm v6}", vertices6);
    label.lft("{\smallrm v7}", vertices7);
  fi;
  

%% ** (2)  Seven, Rectangle 1
  
  V108 := Q108 shifted by (P1 - p582);

  rotate_around V108 (mediate(vertices4, vertices5), mediate(vertices7, vertices6)) 180;
  rotate_around V108 (P0, P1) 90;
  

  fill V108 with_color cv1;
  draw V108;

  Seven0 := V108;
  
  if do_labels:
    label("{\smallrm 1}", P1) with_color dark_green;
  fi;
  
%% ** (2) Hash_symbol, Rectangle 2

  V35 := Q35 shifted by (P2 - p403);
  V36 := Q36 shifted by (P2 - p403);

  rotate_around V35 (mediate(vertices4, vertices3), mediate(vertices0, vertices7)) 90;
  rotate_around V35 (P2, P3) 180;

  rotate_around V36 (mediate(vertices4, vertices3), mediate(vertices0, vertices7)) 90;
  rotate_around V36 (P2, P3) 180;
  
  
  fill V35 with_color cv2;
  draw V35;

  unfilldraw V36;

  Hash_symbol0 := V35;
  Hash_symbol1 := V36;

  label("{\smallrm 2}", P2) with_color yellow;

%% ** (2) I_letter, Rectangle 3

  V19 := q19 shifted by (P3 - p164);

  rotate_around V19 (mediate(vertices5, vertices2), mediate(vertices1, vertices6)) -90;
  rotate_around V19 (P3, P2) 90;

  I_letter0 := V19;
  
  fill V19 with_color cv[3];
  
  draw V19;
  
%% ** (2) K_letter, Rectangle 4

  V34 := q34 shifted by (P4 - p188);
  rotate_around V34 (mediate(vertices4, vertices3), mediate(vertices5, vertices2)) -90;

  K_letter0 := V34;
  
  fill V34 with_color cv4;
  draw V34;

%% ** (2) U letter, Rectangle 5

  V0 := Q0 shifted by (P5 - p231);

  rotate_around V0 (mediate(vertices0, vertices7), mediate(vertices1, vertices6)) 90;

  rotate_around V0 (P5, P4) 90;

  U_letter0 := V0;

  fill V0 with_color cv5;

  draw V0;

%% ** (2) N_letter, Rectangle 0

  V2 := Q2 shifted by (P0 - p239); 

  N_letter0 := V2;
  
  fill V2 with_color cv0;

  draw V2;
  
%% ** (2)
  
  
  draw c0 with_color purple;
  
  rotate current_picture (30, 30);

  shift current_picture (0, 0, 15);

  
endfig with_focus f;
fig_ctr += 1;


%% * (1) Main loop

shift_dir_x := 1;
shift_dir_y := 1;
shift_dir_z := 1; 

shift_val_x := 0;
shift_val_y := 0;
shift_val_z := 0;

do_fill   := true; % false; %
do_labels := false; % true; % 

%% ** (2)

k := 0;
for loop_ctr = 0 upto 0:
  beginfig(fig_ctr);

    draw frame with_pen big_square_pen with_color blue;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    t0 := identity rotated (-90, 0, 90);
    
    % if k < 360:
    %   t0 := identity rotated (0, k, 60);
    % else:
    %   t0 := identity rotated (60, k, 0);
    % fi;

    shift t0 (8, 23, 0);

    shift t0 (0, 0, 30);

    % message "shift_val_x == " & decimal shift_val_x;
    % message "shift_val_y == " & decimal shift_val_y;
    % message "shift_val_z == " & decimal shift_val_z;
    % pause;

    % This works, but needs adjustment.  LDF 2024.04.05.
    %shift t0 (2shift_val_x/10, 1.5shift_val_y/10, shift_val_z/100);
    
    
    c1 := c0 * t0;

    draw c1 with_color purple;
    
    point_vector   normals;
    point_vector   normals_minus_pos_f;
    point_vector   normals_projected;
    point_vector   centers;
    point centers_projected[];
    point centers_minus_pos_f[];
    point          f_position;
    point   pt_diffs[];
    numeric z_diffs[];


    f_position := get_position f; 
    
    

    % message "f_position:";
    % show f_position;

    centers := get_centers c1;
    normals := get_normals c1;

    for i  = 0 upto 5:
      shift normals[i] by centers[i];
    endfor;

    if do_labels:
      label("{\smallrm 0}", centers0);
      label("{\smallrm 1}", centers1);
      label("{\smallrm 2}", centers2);
      label("{\smallrm 3}", centers3);
      label("{\smallrm 4}", centers4);
      label("{\smallrm 5}", centers5);
    fi;

    numeric center_mag;
    numeric normal_mag;
    
    for j = 0 upto 5:

      message "Fig. " & decimal fig_ctr & "\n";
      
      
      centers_minus_pos_f[j] := centers[j] - f_position;

    % message "centers_minus_pos_f[" & decimal j  & "]:";
    % show centers_minus_pos_f[j];
      center_mag := magnitude centers_minus_pos_f[j];
      % message "center_mag:";
      % show center_mag;

      normals_minus_pos_f[j] := normals[j] - f_position;  
    % message "normals_minus_pos_f[" & decimal j & "]:";
    % show normals_minus_pos_f[j];
      normal_mag := magnitude normals_minus_pos_f[j];
      % message "normal_mag:";
      % show normal_mag;

      % message "Mags:";
      
      if normal_mag > center_mag:
	message "Face " & decimal j & " is hidden.";
      else:
	message "Face " & decimal j & " is visible.";

        %fill get_rectangle (j) c1 with_color cv[j];

	
	
	if (j == 0):
	  if do_fill:
	    filldraw (get_rectangle 0 c1) with_draw_color purple with_fill_color dv0;
	  fi;
	  filldraw N_letter0 * t0 with_fill_color cv0;
	fi;

	if (j == 1):
	  if do_fill:
	    filldraw (get_rectangle 1 c1) with_draw_color purple with_fill_color dv1;
	  fi;
	  filldraw Seven0 * t0 with_fill_color cv1;
	fi;

	if (j == 2):
	  if do_fill:
	    filldraw (get_rectangle 2 c1) with_draw_color purple with_fill_color dv2;
	  fi;
	  filldraw Hash_symbol0 * t0 with_fill_color cv2;
	  if do_fill:
	    filldraw Hash_symbol1 * t0 with_fill_color dv2;
	  else:
	    unfilldraw Hash_symbol1 * t0;
	  fi;
	fi;

	if (j == 3):
	  if do_fill:
	    filldraw (get_rectangle 3 c1) with_draw_color purple with_fill_color dv3;
	  fi;
	  filldraw I_letter0 * t0 with_fill_color cv3;
	fi;

	if (j == 4):
	  if do_fill:
	    filldraw (get_rectangle 4 c1) with_draw_color purple with_fill_color dv4;
	  fi;
	  filldraw K_letter0 * t0 with_fill_color cv4;
	fi;

	if (j == 5):
	  if do_fill:
	    filldraw (get_rectangle 5 c1) with_draw_color purple with_fill_color dv5;
	  fi;
	  filldraw U_letter0 * t0 with_fill_color cv5;
	fi;
      fi;
      %pause;
      
      centers_projected[j] := centers[j] projected f with_z;
      normals_projected[j] := normals[j] projected f with_z;
      
      pt_diffs[j] := normals_projected[j] - centers_projected[j];

      z_diffs[j] := zpart pt_diffs[j];

      % message "Diffs:";
      
      if z_diffs[j] > 0:
	% message "Face " & decimal j & " is hidden.";
      else:
	% message "Face " & decimal j & " is visible.";
      fi;
      %pause;

      % message "normal_mag:";
      % show normal_mag;

      % message "center_mag:";
      % show center_mag;

      % message "normal_mag > center_mag:";
      % show (normal_mag > center_mag);

      % message "z_diffs[j]:";
      % show z_diffs[j];

      % message "z_diffs[" & decimal j & "] > 0:";
      % show (z_diffs[j] > 0);
      
      % if ((normal_mag > center_mag) and (z_diffs[j] < 0))
      % 	   or ((normal_mag < center_mag) and (z_diffs[j] > 0)):
      % 	  message "WARNING!  Face " & decimal j & ": Contradiction:  Visibility value differs, "
      % 	        & "depending on which method is used.";
      % else:
      % 	if z_diffs[j] == 0:
      % 	  message "zdiffs[" & decimal j & "] == 0.  Haven't programmed this case yet.";
      % 	else:
      % 	  message "Visibility for Face " & decimal j & " is the same.";
      % 	fi;
      % fi;
      % %pause;	
      
    endfor;

    if (shift_dir_x > 0):
      shift_val_x += 2;
    else:
      shift_val_x -= 2;
    fi;

    if (shift_dir_y > 0):
      shift_val_y += 2;
    else:
      shift_val_y -= 2;
    fi;

    if (shift_dir_z > 0):
      shift_val_z += 2;
    else:
      shift_val_z -= 2;
    fi;

    if magnitude shift_val_x > 10:
      shift_dir_x *= -1;
    fi;

    if magnitude shift_val_y > 10:
      shift_dir_y *= -1;
    fi;

    if magnitude shift_val_z > 10:
      shift_dir_z *= -1;
    fi;
    
    if loop_ctr == 0:
      cube_picture2 := current_picture;
    fi;
    
    
    clip current_picture to frame;
    output current_picture with_focus f;
    clear current_picture;

    draw frame with_color blue;
    
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  k += 15;
endfor;

%% * (1)

endinput;


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

