%% radius_templates.txt
%% Created by Laurence D. Finston (LDF) Sat 02 Jul 2022 06:38:26 AM CEST

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%%%% Copyright (C) 2022, 2023 The Free Software Foundation, Inc.
   
%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version. 

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details. 

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html. 

%%%% Please send bug reports to Laurence.Finston@gmx.de.
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for the maintainer of 
%%%% GNU 3DLDF to send announcements to users. 
%%%% To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% c/o Free Software Foundation, Inc. 
%%%% 51 Franklin St, Fifth Floor 
%%%% Boston, MA  02110-1301  
%%%% USA

%%%% Laurence.Finston@gmx.de


%% * (1)

\input eplain
\input epsf 
\input colordvi
\enablehyperlinks[dvipdfm]

\nopagenumbers

\font\small=cmr8
\font\smalltt=cmtt8
\font\normalbsy=cmbsy10 %% normal bold math symbols (10pt)
\font\medium=cmr10 scaled \magstephalf
\font\mediumbx=cmbx10 scaled \magstephalf
\font\mediumit=cmti10 scaled \magstephalf
\font\mediumtt=cmtt10 scaled \magstephalf
\font\mediumbsy=cmbsy10 scaled \magstephalf %% medium bold math symbols
\font\large=cmr12
\font\largebx=cmbx12
\font\largebx=cmbx12
\font\largebsy=cmbsy12  %% large bold math symbols
\font\largeit=cmti12
\font\largett=cmtt12
\font\Largebx=cmbx12 scaled \magstephalf
\font\huge=cmr12 scaled \magstep2
\font\hugebx=cmbx12 scaled \magstep2
\font\mediumsy=cmsy10 scaled \magstephalf
\font\mediumcy=cmcyr10 scaled \magstephalf

%% * (1)


\footline={\hfil \folio\hfil}

%% Uncomment for A4 portrait
\iftrue % \iffalse
\special{papersize=210mm, 297mm}
\hsize=210mm
\vsize=297mm
\fi

\advance\vsize by -2.5cm
\advance\voffset by -1in
\advance\voffset by 1.25cm
\advance\hoffset by -1in
\advance\hoffset by .5cm

\parindent=0pt


\newdimen\twozerosdimen
\setbox0=\hbox{{\medium 00}}
\twozerosdimen=\wd0

\newcount\chapctr
\chapctr=0

\newcount\temppagecnt
\temppagecnt=0

\newcount\sectionctr
\sectionctr=0

\def\tocchapterentry#1#2#3{\line{\hbox to \twozerosdimen{\hss #2}. {\medium #1 \dotfill\ #3}}}
\def\tocsectionentry#1#2#3{\setbox0=\hbox{#2}\line{\hskip3em\ifdim\wd0>0pt{#2}. \fi{\medium #1 \dotfill\ #3}}}

\def\Chapter#1#2{\global\advance\chapctr by 1\sectionctr=0%
\writenumberedtocentry{chapter}{\hlstart{}{bwidth=0}{#1}\Blue{#2}\hlend}{\the\chapctr}}
\def\Section#1#2{\global\advance\sectionctr by 1\edef\A{\the\chapctr .\the\sectionctr}%
\writenumberedtocentry{section}{\hlstart{}{bwidth=0}{#1}\Blue{#2}\hlend}{\A}}


%% *** (3) 

\def\epsfsize#1#2{#1}

%% *** (3)

\headline={\hfil {\tt \timestamp}\hskip2cm}

\pageno=-1

\begingroup
\advance\hsize by -2.5cm
\parskip=.5\baselineskip
\centerline{{\largebx Radius Templates}}
\vskip\baselineskip

\small
This document is part of GNU 3DLDF, a package for three-dimensional drawing. 

!Copyright (C) 2022, 2023 The Free Software Foundation

\setbox0=\hbox{Last updated:\quad}

\leavevmode\hbox to \wd0{Created:\hfil}July 2, 2022

\leavevmode\box0 July 2, 2022

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version. 

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Publibfgfor more details. 

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

See the GNU Free Documentation License for the copying conditions 
that apply to this document.

You should have received a copy of the GNU Free Documentation License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

See the end of this document for the GNU General Public License and
the GNU Free Documentation License.

The mailing list {\smalltt info-3dldf@gnu.org} is for sending 
announcements to users. To subscribe to this mailing list, send an 
email with ``subscribe $\langle$email-address$\rangle$'' as the subject.  

The author can be contacted at:

Laurence D. Finston\hfil\break
c/o Free Software Foundation, Inc.\hfil\break
51 Franklin St, Fifth Floor \hfil\break
Boston, MA  02110-1301 USA\hfil\break
{\smalltt Laurence.Finston@gmx.de}
\vskip\baselineskip
\centerline{{\largebx Table of Contents}}
\advance\baselineskip by 2pt
%\advance\parskip by 2pt
\readtocfile
\vskip1.5\baselineskip
\endgroup
\vfil\eject

%% * (1)

\headline={\hfil{Radius Templates}\hfil\llap{{\tt \timestamp}\hskip2cm}}
\pageno=1

%% ** (2) Concave

%% *** (3) Section 1

\vbox to \vsize{\hldest{xyz}{}{concave}\vskip.333cm
\Chapter{concave}{Concave}
\centerline{{\largebx Concave}}
\vskip.25cm
\hldest{xyz}{}{sectionone}\vskip.333cm
\Section{sectionone}{Section 1}
\centerline{{\largebx Section 1}}
\vskip.75cm
\hbox{\epsffile{radius_templates001.eps}\hskip.333cm\epsffile{radius_templates002.eps}\hss}\vss}
\vfil\eject

\vbox to \vsize{\vskip.333cm
\vskip.75cm
\hbox{\epsffile{radius_templates003.eps}\hskip.333cm\epsffile{radius_templates004.eps}\hss}\vss}
\vfil\eject


\vbox to \vsize{\vskip.333cm
\vskip.75cm
\hbox{\epsffile{radius_templates005.eps}\hskip.333cm\epsffile{radius_templates006.eps}\hss}\vss}
\vfil\eject


%% *** (3) Section 2

\vbox to \vsize{\hldest{xyz}{}{sectiontwo}\vskip.333cm
\Section{sectiontwo}{Section 2}
\centerline{{\largebx Section 2}}
\vskip.75cm
\hbox{\epsffile{radius_templates101.eps}\hskip2cm\epsffile{radius_templates102.eps}\hss}\vss}
\vfil\eject


\vbox to \vsize{\vskip.333cm
\vskip.75cm
\hbox{\epsffile{radius_templates103.eps}\hskip2cm\epsffile{radius_templates104.eps}\hss}\vss}
\vfil\eject

\vbox to \vsize{\vskip.333cm
\vskip.75cm
\hbox{\epsffile{radius_templates105.eps}\hskip1.5cm\epsffile{radius_templates106.eps}\hss}\vss}
\vfil\eject


%% ** (2) Convex

%% *** (3) Section 1

%% *** (3) Section 2


%% *** (3) End here

\bye

%% ** (2)

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:TeX
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% abbrev-mode:t
%% outline-regexp:"%% [*\f]+"
%% auto-fill-function:nil
%% End:

