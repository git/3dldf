%% double_pendulum_1.ldf
%% Created by Laurence D. Finston (LDF) Sa 13. Jul 22:09:11 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

boolean do_png;

do_png := true; % false; %

boolean do_labels;
do_labels := false; % true; % 

picture v[];

if do_png:
  do_labels := false;

  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%4c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";dotlabeldiam:=4mm;";
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";dotlabeldiam:=4mm;";
fi;

verbatim_tex   "\magnification=5000";

input "plainldf.lmc";


point p[];
path q[];
transform t[];

path_vector qv[];

point temp_pt[];

frame_wd := 297mm;
frame_wd -= 2;

frame_ht := (frame_wd / 16) * 9;

frame_wd += 4mm;
frame_ht -= 4mm;

path frame;

frame :=    (-frame_wd, -frame_ht) -- (frame_wd, -frame_ht) -- (frame_wd, frame_ht)
         -- (-frame_wd, frame_ht) -- cycle;

pen big_dot_pen;

dot_pen := pencircle scaled (6mm, 6mm, 6mm);
big_dot_pen := pencircle scaled (10mm, 10mm, 10mm);

pen small_dot_pen;

small_dot_pen := pencircle scaled (2mm, 2mm, 2mm);

fig_ctr := 0;

medium_pen := pencircle scaled (1mm, 1mm, 1mm);
big_pen    := pencircle scaled (1.5mm, 1.5mm, 1.5mm);
Big_pen    := pencircle scaled (3mm, 3mm, 3mm);

focus f;

set f with_position (0, 0, -20) with_direction (0, 0, 20) with_distance 20;

pickup medium_pen;

string s;

%% * (1) Fig. 0.  


beginfig(fig_ctr);

%% ** (2)  

  fill frame with_color white;
  
  draw frame with_pen big_square_pen with_color black;

  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
  p0 := origin;

  p1 := (0, -20); % Length of first pendulum

  p2 := p1 shifted (0, -10); % Length of second pendulum
  
  q0 := p0 -- p1;

  q1 := p1 -- p2;

  p3 := p1 shifted (1, 0, 0);
  
  draw q0 with_color dark_green;
  draw q1 with_color orange;
 

  if do_labels:
    dotlabel.top("$p_0$", p0);
    dotlabel.lrt("$p_1$", p1);
    dotlabel.lrt("$p_2$", p2);
  fi;

  rotate current_picture (0, 30);
  shift current_picture (10, 10, 20);
  
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
    
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop

numeric rotation_val[];
numeric rotation_increment[];
numeric rotation_limit[];
numeric rotation_damping_val[];
  
rotation_val0          := 60;
rotation_increment0    := -2.12967;
rotation_limit0        := 60;
rotation_damping_val0  := 2;

rotation_val1          := 0;
rotation_increment1    := .7568;
rotation_limit1        := 80;
rotation_damping_val1 := 3;


rotation_val2          := 3; % For rotation of the second pendulum about the length
                             % of the first one, i.e., the line p0p1.

q2 += ..;

%% ** (2) Loop itself.

for loop_ctr = 0 upto 2500:

  clear v0;
  
  t0 := identity rotated (0, 0, rotation_val0);
  t2 := identity rotated_around (p1, p3) rotation_val1;

  % Using t3 didn't work.  q101 became detached from p1.
  % !! TODO:  Check why this happens.  LDF 2024.07.14.

  %t3 := identity rotated_around (p0, p3) rotation_val2*loop_ctr;
  t1 := t2; %  * t3;
  
  q101 := q1 * t1;
  q101 *= t0;
  p102 := p2 * t1;
  p102 *= t0;
  
  q100 := q0 * t0;
  p101 := p1 * t0;
  
  q2 += p102;

  %message "rotation_val0 == " & decimal rotation_val0;
  rotation_val0 += rotation_increment0;

  if (loop_ctr > 0) and (rotation_limit0 > 0) and (rotation_val0 >= rotation_limit0):
    rotation_limit0 -= rotation_damping_val0;
    rotation_limit0 := max(rotation_limit0, 0);
    rotation_val0 := rotation_limit0;
    %message "rotation_limit0: " & decimal rotation_limit0;
  fi;
  
  if magnitude(rotation_val0) >= rotation_limit0:
    rotation_increment0 *= -1;
  fi;

  %message "rotation_val1 == " & decimal rotation_val1;
  rotation_val1 += rotation_increment1;

  if (loop_ctr > 0) and (rotation_limit1 > 0) and (rotation_val1 >= rotation_limit1):
    rotation_limit1 -= rotation_damping_val1;
    rotation_limit1 := max(rotation_limit1, 0);
    rotation_val1 := rotation_limit1;
    %message "rotation_limit1: " & decimal rotation_limit1;
  fi;
  
  if magnitude(rotation_val1) >= rotation_limit1:
    rotation_increment1 *= -1;
  fi;

%% ** (2)
  
  n := loop_ctr mod 5;
  if n == 0:
    beginfig(fig_ctr);

%% *** (3)
      
      message "Fig. " & decimal fig_ctr;

      fill frame with_color white;
      
      draw frame with_pen big_square_pen with_color black;

      output current_picture with_projection parallel_x_y;
      clear current_picture;


      
      draw q2;

      % For testing whether the shape of the path is nearly correct.
      % Otherwise, it increases the run-time too much.
      % LDF 2024.07.14.
      
      % for i = 0 upto ((size q2) - 2):
      %   drawdot get_point (i) q2 with_pen small_dot_pen with_color green;
      % endfor;

      draw q100 with_color dark_green;
      draw q101 with_color orange;
      drawdot p0 with_color magenta with_pen dot_pen;
      drawdot p101 with_color red with_pen dot_pen;
      drawdot p102 with_color blue with_pen dot_pen;

      
      if do_labels:
	dotlabel.top("$p_{0}$", p0);

	s := "$p_{101}$, $" & decimal rotation_val0 & "^\circ$";
	dotlabel.lrt(s, p101);

	s := "$p_{102}$, $" & decimal rotation_val1 & "^\circ$";
	dotlabel.lrt(s, p102) with_color blue;
      fi;

      rotate current_picture (0, 30);
      shift current_picture (10, 10, 20);
      
      
      output current_picture with_focus f;
      clear current_picture;

      clip current_picture to frame;
      draw frame with_pen big_square_pen;
      
    endfig with_projection parallel_x_y;

%% ** (2)

    beginfig(2000+fig_ctr);

      fill frame with_color white;

      draw frame with_pen big_square_pen with_color black;

      output current_picture with_projection parallel_x_y;
      clear current_picture;

      draw q100 with_color dark_green;
      draw q101 with_color orange;
      
      drawdot p0 with_color magenta with_pen dot_pen;
      drawdot p101 with_color red with_pen dot_pen;

      draw q2;

      drawdot p102 with_color blue with_pen dot_pen;

      %scale current_picture (.75, .75, .75);
      
      output current_picture with_projection parallel_x_z;
      clear current_picture;

      clip current_picture to frame;
      draw frame with_pen big_square_pen;

    endfig with_projection parallel_x_y;
    fig_ctr += 1;
  fi;

%% ** (2)



endfor;


%% * (1)

if not do_png:
  message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
  pause;
fi;

bye;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

