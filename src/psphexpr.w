@q psphexpr.w @> 
@q Created by Laurence Finston Mo Jun  6 12:50:36 CEST 2005 @>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de  (@@ stands for a single ``at'' sign.)@>

@q * (0) sphere expressions.  @>
@** sphere expressions.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Created this file.
\ENDLOG 

@q * (1) sphere_primary.  @>
@* \�sphere primary>.
\initials{LDF 2005.06.06.}  

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sphere_primary@>@/

@q ** (2) sphere_primary --> sphere_variable.@>
@*1 \�sphere primary> $\longrightarrow$ \�sphere variable>.  

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=sphere_primary: sphere_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

    @=$$@> = static_cast<void*>(create_new<Sphere>(
                                  static_cast<Sphere*>(
                                     entry->object))); 

};

@q ** (2) sphere_primary --> LEFT_PARENTHESIS sphere_expression @> 
@q ** (2) RIGHT_PARENTHESIS                                   @>

@*1 \�sphere primary> $\longrightarrow$ \.{\LP} 
\�sphere expression> \.{\RP}.

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sphere_primary: LEFT_PARENTHESIS sphere_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q ***** (5) sphere_primary --> LAST @>
@q ***** (5) sphere_vector_expression.@>

@*4 \�sphere primary> $\longrightarrow$ 
\.{LAST} \�sphere vector expression>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=sphere_primary: LAST sphere_vector_expression@>@/
{ 

   Sphere* c;

         c = create_new<Sphere>(0);

   Pointer_Vector<Sphere>* pv 
      = static_cast<Pointer_Vector<Sphere>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2005.06.06.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<Sphere>*>(0))
      {

          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2005.06.06.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {
          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2005.06.06.}

@<Define rules@>=

   else 
      {
         *c = *(pv->v[pv->ctr - 1]);
         @=$$@> = static_cast<void*>(c); 
      }
@q ******* (7) @> 

};

@q ***** (5) sphere_primary --> GET_INNER_SPHERE cuboid_primary set_sphere_option_list @>
@q point_vector_optional circle_vector_optional.                                       @>

@*4 \�sphere primary> $\longrightarrow$ \.{GET\_INNER\_SPHERE} \�cuboid primary>
\�set sphere option list>  \�point vector optional> \�circle vector optional>.
\initials{LDF 2024.12.03.}

\LOG
\initials{LDF 2024.12.03.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=sphere_primary: GET_INNER_SPHERE cuboid_primary set_sphere_option_list @>@/
@=point_vector_optional circle_vector_optional@>@/
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `sphere_primary: GET_INNER_SPHERE cuboid_primary "
                << "set_sphere_option_list point_vector_optional circle_vector_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Cuboid *cuboid = static_cast<Cuboid*>(@=$2@>);

   Sphere *sphere = create_new<Sphere>(0);

   Id_Map_Entry_Node cv_entry = 0;

   Pointer_Vector<Point>  *pv = 0;
   Pointer_Vector<Circle>   *cv = 0;
  
   if (@=$4@>)
   {
      entry = static_cast<Id_Map_Entry_Node>(@=$4@>);
      if (entry->object)
      {
            Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                         entry); 
      }
      pv = new Pointer_Vector<Point>;
   }

   if (@=$5@>)
   {
      cv_entry = static_cast<Id_Map_Entry_Node>(@=$5@>);
      if (cv_entry->object)
      {
            Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                         cv_entry); 
      }
      cv = new Pointer_Vector<Circle>;
   }

   status = cuboid->get_sphere(sphere, 0, pv, cv, scanner_node);

   if (pv)
   {
     status = Scan_Parse::vector_type_assign<Point, Point>(
                                    static_cast<Scanner_Node>(parameter),
                                    entry,
                                    pv);          

      cerr << "pv->ctr == " << pv->ctr << endl;
   }

   if (cv)
   {
     status = Scan_Parse::vector_type_assign<Circle, Circle>(
                                    static_cast<Scanner_Node>(parameter),
                                    cv_entry,
                                    cv);          

      cerr << "cv->ctr == " << cv->ctr << endl;
   }

@q ******* (7) @>
 
   if (status != 0)
   {
      cerr << "ERROR! In parser, rule `sphere_primary: GET_INNER_SPHERE cuboid_primary "
           << "set_sphere_option_list point_vector_optional circle_vector_optional':"
           << endl 
           << "`Cuboid::get_sphere' failed, returning " << status << "." << endl 
           << "Failed to get sphere.  Will try to continue." << endl;
   }

@q ******* (7) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `sphere_primary: GET_INNER_SPHERE cuboid_primary "
           << "set_sphere_option_list point_vector_optional circle_vector_optional':"
           << endl 
           << "`Cuboid::get_sphere' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   @=$$@> = static_cast<void*>(sphere);

   delete cuboid;
   cuboid = 0;

@q ******* (7) @>

};

@q ***** (5) sphere_primary --> GET_OUTER_SPHERE cuboid_primary set_sphere_option_list @>
@q point_vector_optional circle_vector_optional.                                       @>

@*4 \�sphere primary> $\longrightarrow$ \.{GET\_OUTER\_SPHERE} \�cuboid primary>
\�set sphere option list>  \�point vector optional> \�circle vector optional>.
\initials{LDF 2024.12.07.}

\LOG
\initials{LDF 2024.12.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=sphere_primary: GET_OUTER_SPHERE cuboid_primary set_sphere_option_list @>@/
@=point_vector_optional circle_vector_optional@>@/
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `sphere_primary: GET_OUTER_SPHERE cuboid_primary "
                << "set_sphere_option_list point_vector_optional circle_vector_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Cuboid *cuboid = static_cast<Cuboid*>(@=$2@>);

   Sphere *sphere = create_new<Sphere>(0);

   Id_Map_Entry_Node cv_entry = 0;

   Pointer_Vector<Point>  *pv = 0;
   Pointer_Vector<Circle>   *cv = 0;
  
   if (@=$4@>)
   {
      entry = static_cast<Id_Map_Entry_Node>(@=$4@>);
      if (entry->object)
      {
            Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                         entry); 
      }
      pv = new Pointer_Vector<Point>;
   }

   if (@=$5@>)
   {
      cv_entry = static_cast<Id_Map_Entry_Node>(@=$5@>);
      if (cv_entry->object)
      {
            Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                         cv_entry); 
      }
      cv = new Pointer_Vector<Circle>;
   }

   status = cuboid->get_sphere(sphere, 1, pv, cv, scanner_node);

   if (pv)
   {
     status = Scan_Parse::vector_type_assign<Point, Point>(
                                    static_cast<Scanner_Node>(parameter),
                                    entry,
                                    pv);          

      cerr << "pv->ctr == " << pv->ctr << endl;
   }

   if (cv)
   {
     status = Scan_Parse::vector_type_assign<Circle, Circle>(
                                    static_cast<Scanner_Node>(parameter),
                                    cv_entry,
                                    cv);          

      cerr << "cv->ctr == " << cv->ctr << endl;
   }

@q ******* (7) @>
 
   if (status != 0)
   {
      cerr << "ERROR! In parser, rule `sphere_primary: GET_OUTER_SPHERE cuboid_primary "
           << "set_sphere_option_list point_vector_optional circle_vector_optional':"
           << endl 
           << "`Cuboid::get_sphere' failed, returning " << status << "." << endl 
           << "Failed to get sphere.  Will try to continue." << endl;
   }

@q ******* (7) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `sphere_primary: GET_OUTER_SPHERE cuboid_primary "
           << "set_sphere_option_list point_vector_optional circle_vector_optional':"
           << endl 
           << "`Cuboid::get_sphere' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   @=$$@> = static_cast<void*>(sphere);

   delete cuboid;
   cuboid = 0;

@q ******* (7) @>

};

@q **** (4) point_vector_optional.  @>
@ \�point vector optional>.
\initials{LDF 2024.12.05.}

\LOG
\initials{LDF 2024.12.05.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> point_vector_optional@>@/

@q ***** (5) @>
@
\LOG
\initials{LDF 2024.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_vector_optional: /* Empty  */@>@/
{ 
   @=$$@> = static_cast<void*>(0);

};

@q ***** (5) @>
@
\LOG
\initials{LDF 2024.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_vector_optional: COMMA point_vector_variable@>@/
{ 
   @=$$@> = static_cast<void*>(@=$2@>);
};

@q **** (4) circle_vector_optional.  @>
@ \�circle vector optional>.
\initials{LDF 2024.12.05.}

\LOG
\initials{LDF 2024.12.05.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_vector_optional@>@/

@q ***** (5) @>
@
\LOG
\initials{LDF 2024.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=circle_vector_optional: /* Empty  */@>@/
{ 
   @=$$@> = static_cast<void*>(0);

};

@q ***** (5) @>
@
\LOG
\initials{LDF 2024.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=circle_vector_optional: COMMA circle_vector_variable@>@/
{ 
   @=$$@> = static_cast<void*>(@=$2@>);
};

@q ***** (5) sphere_primary --> GENERATE FROM circle_expression set_sphere_option_list@>

@*4 \�sphere primary> $\longrightarrow$ \.{GENERATE} \.{FROM} \�circle expression>
\�set sphere option list>
\initials{LDF 2024.12.17.}

\LOG
\initials{LDF 2024.12.17.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=sphere_primary: GENERATE FROM circle_expression set_sphere_option_list@>@/
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `sphere_primary: GENERATE FROM circle_expression set_sphere_option_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   Circle *c = static_cast<Circle*>(@=$3@>);

   Sphere *sphere = create_new<Sphere>(0);

   status = sphere->generate(c, scanner_node);

   if (status != 0)
   {
      cerr << "ERROR!  In parser, rule `sphere_primary: GENERATE FROM circle_expression "
           << "set_sphere_option_list':"
           << endl
           << "`Sphere::generate' failed, returning " << status << "." << endl 
           << "Failed to generate sphere.  Will try to continue." << endl;
   }

@q ******* (7) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `sphere_primary: GENERATE FROM circle_expression set_sphere_option_list':"
           << endl
           << "`Sphere::generate' succeeded, returning 0." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;    

@q ******* (7) @>

   @=$$@> = static_cast<void*>(sphere);

   delete c;
   c = 0;

@q ******* (7) @>

};

@q ** (2) sphere_primary: sphere_primary SELF_ROTATED numeric_list with_transform_optional.  @> 
@
\LOG
\initials{LDF 2024.12.08.}
Added this rule.

\initials{LDF 2024.12.18.}
Changed return type of rule to \�sphere primary> from \�transform primary>.
\ENDLOG

@<Define rules@>=
@=sphere_primary: sphere_primary SELF_ROTATED numeric_list with_transform_optional@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `sphere_primary: sphere_primary SELF_ROTATED "
                << "numeric_list with_transform_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Transform t;

   Sphere *s = static_cast<Sphere*>(@=$1@>);

   real x = 0.0;
   real y = 0.0;
   real z = 0.0;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

   if (pv->v.size() > 0)
      x = *(pv->v[0]);

   if (pv->v.size() > 1)
      y = *(pv->v[1]);

   if (pv->v.size() > 2)
      z = *(pv->v[2]);

   s->self_rotate(&t, x, y, z, scanner_node);

   entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (entry)
   {
       if (!entry->object)
          entry->object = static_cast<void*>(create_new<Transform>(0));

       *static_cast<Transform*>(entry->object) = t;
   }

   delete pv;
   pv = 0;

   @=$$@> = static_cast<void*>(s);

};

@q * (1) sphere_secondary.  @>
@* \�sphere secondary>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sphere_secondary@>
  
@q ** (2) sphere secondary --> sphere_primary.@>
@*1 \�sphere secondary> $\longrightarrow$ \�sphere primary>.

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sphere_secondary: sphere_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: sphere_secondary --> sphere_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) sphere secondary --> sphere_secondary transformer.  @>
@*1 \�sphere secondary> $\longrightarrow$ \�sphere secondary> 
\�transformer>.

\LOG
\initials{LDF 2005.06.06.}
Added this rule.

\initials{LDF 2006.01.20.}
@:BUG FIX@> BUG FIX:  Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=sphere_secondary: sphere_secondary transformer@>@/
{
  Sphere* s = static_cast<Sphere*>(@=$1@>);
  Transform* t = static_cast<Transform*>(@=$2@>);

  *s *= *t;

#if 0 
  cerr << "In sphere_secondary: sphere_secondary transformer:" << endl;
  s->center.show("s->center after multiplication:");
#endif 

  delete t;

  @=$$@> = static_cast<void*>(s); 

};

@q * (1) sphere tertiary.@>
@* \�sphere tertiary>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sphere_tertiary@>

@q ** (2) sphere tertiary --> sphere_secondary.  @>
@*1 \�sphere tertiary> $\longrightarrow$ \�sphere secondary>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sphere_tertiary: sphere_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q * (1) sphere expression.@>
@* \�sphere expression>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sphere_expression@>

@q ** (2) sphere expression --> sphere_tertiary.  @>
@*1 \�sphere expression> $\longrightarrow$ \�sphere tertiary>.

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sphere_expression: sphere_tertiary@>@/
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

