/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Mo 11. Mär 14:25:30 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

/* ** (2) */

/* * (1) */

#if 0 /* Do not delete.  */
   color_vector.push_back(string("frame_black_border.png"));
   color_vector.push_back(string("Black/Lampenschwarz.png"));
#endif 

int
replace(int start, int end, int cube_ctr, vector<string> *ccolor_vector);


int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   vector<string> color_vector;

   stringstream s;

   string zeroes;
   string zeroes_1;

   int j = 171;

   for (int i = 214; i >= 202; --i, ++j)
   {
       if (i < 10)
          zeroes = "000";
       else if (i < 100)
          zeroes = "00";
       else if (i < 1000)
          zeroes = "0";
       else
          zeroes = "";

       if (j < 10)
         zeroes_1 = "000";
       else if (j < 100)
         zeroes_1 = "00";
       else if (j < 1000)
         zeroes_1 = "0";
       else
         zeroes_1 = "";

       s.str("");
  
/* * (1) c14 disappears  */

      s << "composite c14_" << zeroes << i << ".png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

  }  /* |for|  */



  return 0;

}  /* End of |main| definition  */

/* * (1) c6 disappears  */

#if 0 
     int j = 158;

     for (int i = 110; i >= 98; --i, ++j)

      s << "composite c14_0214.png c6_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";
#endif 

/* * (1) c8 disappears  */

#if 0

   int j = 145;

   for (int i = 136; i >= 124; --i, ++j)

       if (i == 136)
       {

          s << "cp c6_0110.png D.png; "

            << "composite D.png c14_0214.png J.png; ";


      }

      s << "composite J.png c8_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 



/* * (1) c15 disappears  */

#if 0 

   int j = 132;
   for (int i = 227; i >= 215; --i, ++j)

       if (i == 227)
       {

          s << "cp c6_0110.png D.png; "

            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c14_0214.png J.png; ";


      }

      s << "composite J.png c15_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 

/* * (1) c3 disappears  */

#if 0

   int j = 119;

   for (int i = 71; i >= 59; --i, ++j)

       if (i == 71)
       {

          s << "cp c6_0110.png D.png; "

            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; ";

      }

      s << "composite K.png c3_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";


#endif 

/* * (1) c16 disappears  */

#if 0

   int j = 106;
   for (int i = 240; i >= 228; --i, ++j)

       if (i == 240)
       {

          s << "cp c3_0071.png A.png; "


            << "composite A.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; ";

      }

      s << "composite K.png c16_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";


#endif 


/* * (1) c4 disappears  */

#if 0 

       if (i == 84)
       {

          s << "cp c3_0071.png A.png; "


            << "composite A.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png M.png; ";
      }

      s << "composite M.png c4_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 

/* * (1) c12 disappears  */

#if 0
   int j = 80;

   for (int i = 188; i >= 176; --i, ++j)

       if (i == 188)
       {

          s << "cp c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "

            << "composite B.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png M.png; ";
      }

      s << "composite M.png c12_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 

/* * (1) c9 disappears  */

#if 0

   int j = 67;

   for (int i = 149; i >= 137; --i, ++j)

       if (i == 149)
       {

          s << "cp c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "

            << "composite B.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "

            << "composite E.png c12_0188.png H.png; "

            << "composite H.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png M.png; ";
      }

      s << "composite M.png c9_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 

/* * (1) c5 disappears  */

#if 0 

   int j = 54;

   for (int i = 97; i >= 85; --i, ++j)

       if (i == 97)
       {

          s << "cp c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "

            << "composite B.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "
            << "composite E.png c9_0149.png  F.png; "

            << "composite F.png c12_0188.png H.png; "

            << "composite H.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png M.png; ";
      }

      s << "composite M.png c5_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 

/* * (1) c17 disappears  */

#if 0 

   for (int i = 253; i >= 241; --i, ++j)

       if (i == 253)
       {

          s << "cp c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "
            << "composite B.png c5_0097.png  C.png; "
            << "composite C.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "
            << "composite E.png c9_0149.png  F.png; "

            << "composite F.png c12_0188.png H.png; "

            << "composite H.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png M.png; ";
      }

      s << "composite M.png c17_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif

/* * (1) c2 disappears  */

#if 0 
       if (i == 58)
       {

#if 0 
<< "composite c2_0058.png c3_0071.png A.png; "
#endif 
          s << "cp c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "
            << "composite B.png c5_0097.png  C.png; "
            << "composite C.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "
            << "composite E.png c9_0149.png  F.png; "

            << "composite F.png c12_0188.png H.png; "

            << "composite H.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png L.png; "
            << "composite L.png c17_0253.png M.png; ";
      }

      s << "composite M.png c2_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";


#endif 


/* * (1) c13 disappears  */
#if 0 

       for (int i = 201; i >= 189; --i, ++j)

       if (i == 201)
       {
          s << "composite c2_0058.png c3_0071.png A.png; "
            << "composite A.png c4_0084.png  B.png; "
            << "composite B.png c5_0097.png  C.png; "
            << "composite C.png c6_0110.png  D.png; "
            << "composite D.png c8_0136.png  E.png; "
            << "composite E.png c9_0149.png  F.png; "

            << "composite F.png c12_0188.png H.png; "

            << "composite H.png c14_0214.png J.png; "
            << "composite J.png c15_0227.png K.png; "
            << "composite K.png c16_0240.png L.png; "
            << "composite L.png c17_0253.png M.png; ";
      }

      s << "composite M.png c13_" << zeroes << i << ".png I.png; "

        << "composite I.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_reversed_" << zeroes_1 << j << ".png; ";

#endif 


/* * (1) c10 disappears.  */
#if 0 
   for (int i = 162; i >= 150; --i, ++j)

       s << "composite c2_0058.png c3_0071.png A.png; "
         << "composite A.png c4_0084.png B.png; "
         << "composite B.png c5_0097.png  C.png; "
         << "composite C.png c6_0110.png  D.png; "
         << "composite D.png c8_0136.png  E.png; "
         << "composite E.png c9_0149.png  F.png; "

         << "composite F.png c10_" << zeroes << i << ".png G.png; "

         << "composite G.png c12_0188.png H.png; "
         << "composite H.png c13_0201.png I.png; "
         << "composite I.png c14_0214.png J.png; "
         << "composite J.png c15_0227.png K.png; "
         << "composite K.png c16_0240.png L.png; "
         << "composite L.png c17_0253.png M.png; "
         << "composite M.png ./IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
         << "combined_reversed_" << zeroes_1 << j << ".png; ";
#endif 

/* * (1) Created reversed files */

#if 0





      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      int j = 254;

      for (int i = 252; i >= 45; --i, ++j)
      {

      if ((i >= 163 && i <= 175) || (i >= 110 && i <= 123))
         continue;

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s.str("");

      s << "cp combined_" << zeroes << i << ".png combined_" << zeroes_1 << j << ".png"; 



     }
#endif 

/* * (1) */

#if 0

   /* c17  */

   // color_vector.clear();

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));

   color_vector.push_back(string("Red/Rubinrot_1_coat_wc.png"));
   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Blue/Preussischblau_1_coat_wc.png"));
   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));

   color_vector.push_back(string("Tan/Siena_natur_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Preussischblau_2_coats_wc.png"));

   color_vector.push_back(string("Brown/Englisch_Venez_Rot_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));

   status = replace(241, 241, 17, 0);

   status = replace(242, 253, 17, &color_vector);


#endif 


/* * (1) */

#if 0

   /* c16  */

   // color_vector.clear();

   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));
   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_2_coats_wc.png"));

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Tan/Umbra_natur_2_coats_wc.png"));

   color_vector.push_back(string("Brown/Umbra_gebrannt_1_coat_wc.png"));
   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));

   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Scheveningen_blue_deep_2_coats_wc.png"));

   status = replace(228, 228, 16, 0);

   status = replace(229, 240, 16, &color_vector);


#endif 


/* * (1) */

#if 0 

   /* c15  */

   // color_vector.clear();

   color_vector.push_back(string("Red/Lasurdunkelrot_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Neapelgelb_1_coat_wc.png"));

   color_vector.push_back(string("Green/Chromoxidgruen_stumpf_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Scheveningen_orange_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Umbra_gruenlich_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));
   color_vector.push_back(string("Red/Rubinrot_1_coat_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));

   status = replace(215, 215, 15, 0);

   status = replace(216, 227, 15, &color_vector);

#endif 


/* * (1) */

#if 0

   /* c14  */

   // color_vector.clear();


   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Red/Krapplack_dunkel_wc.png"));
   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));

   color_vector.push_back(string("Tan/Umbra_natur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));

   color_vector.push_back(string("Blue/Preussischblau_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));

   color_vector.push_back(string("Purple/Cobalt_violet_dark_2_coats_wc.png"));
   color_vector.push_back(string("Green/Chromoxidgruen_stumpf_1_coat_wc.png"));

   color_vector.push_back(string("Brown/Umbra_gebrannt_1_coat_wc.png"));
   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));

   status = replace(202, 202, 14, 0);

   status = replace(203, 214, 14, &color_vector);

#endif 

/* * (1) c13 */

#if 0 
   /* c13  */

   color_vector.clear();


   color_vector.push_back(string("Tan/Siena_natur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Krapplack_dunkel_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));

   color_vector.push_back(string("Yellow/Neapelgelb_1_coat_wc.png"));
   color_vector.push_back(string("Red/Lasurdunkelrot_3_coats_wc.png"));

   color_vector.push_back(string("Brown/Umbra_gebrannt_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Pariserblau_2_coats_wc.png"));

   color_vector.push_back(string("Red/Rubinrot_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Scheveningen_orange_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_2_coats_wc.png"));

   status = replace(189, 189, 13, 0);

   status = replace(190, 201, 13, &color_vector);
#endif 

/* * (1) */

#if 0

   /* c12  */

   color_vector.clear();

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));

   color_vector.push_back(string("Blue/Preussischblau_1_coat_wc.png"));
   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Neapelgelb_roetlich_2_coats_wc.png"));
   color_vector.push_back(string("Blue/Scheveningen_blue_deep_2_coats_wc.png"));

   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Lasurdunkelrot_2_coats_wc.png"));

   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));
   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));

   status = replace(176, 176, 12, 0);

   status = replace(177, 188, 12, &color_vector);


#endif 

/* * (1) c10 */

#if 0

   /* c10  */

   color_vector.clear();

   color_vector.push_back(string("Blue/Scheveningen_blue_deep_2_coats_wc.png"));
   color_vector.push_back(string("Brown/Umbra_gebrannt_1_coat_wc.png"));

   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));
   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));

   color_vector.push_back(string("Orange/Chromorangeton_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Siena_natur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));

   status = replace(150, 150, 10, 0);

   status = replace(151, 162, 10, &color_vector);


#endif 

/* * (1) c9 */

#if 0 

   /* c9  */

   color_vector.clear();

   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Dioxazine_mauve_2_coats_wc.png"));

   color_vector.push_back(string("Blue/Preussischblau_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Neapelgelb_1_coat_wc.png"));

   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));
   color_vector.push_back(string("Red/Rubinrot_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Siena_natur_1_coat_wc.png"));
   color_vector.push_back(string("Green/Chromoxidgruen_stumpf_1_coat_wc.png"));

   color_vector.push_back(string("Blue/Preussischblau_1_coat_wc.png"));
   color_vector.push_back(string("Red/Lasurdunkelrot_3_coats_wc.png"));

   status = replace(137, 137, 9, 0);

   status = replace(138, 149, 9, &color_vector);

#endif 


/* * (1) c8 */

#if 0 

   /* c8  */

   color_vector.push_back(string("Tan/Umbra_natur_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Blue/Pariserblau_2_coats_wc.png"));
   color_vector.push_back(string("Red/Krapplack_dunkel_wc.png"));

   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_2_coats_wc.png"));

   color_vector.push_back(string("Tan/Umbra_gruenlich_wc.png"));
   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));

   color_vector.push_back(string("Yellow/Indischgelb_2_coat_wc.png"));
   color_vector.push_back(string("Blue/Scheveningen_blue_deep_2_coats_wc_flipped_horiz.png"));

   status = replace(124, 124, 8, 0);

   status = replace(125, 136, 8, &color_vector);

#endif 

/* * (1) */

#if 0

   /* c7  */

   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));

   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Umbra_natur_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Preussischblau_1_coat_wc.png"));

   color_vector.push_back(string("Gray/Graphitgrau_2_coats_wc.png"));
   color_vector.push_back(string("Gray/Graphitgrau_2_coats_wc.png"));


/* ** (2) */

   status = replace(111, 111, 7, 0);

   status = replace(112, 123, 7, &color_vector);

#endif 

/* * (1) */

#if 0 

  /* c6  */

   color_vector.push_back(string("Brown/Umbra_gebrannt_2_coats_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Red/Lasurdunkelrot_3_coats_wc.png"));
   color_vector.push_back(string("Blue/Preussischblau_1_coat_wc.png"));

   color_vector.push_back(string("Purple/Chinacridon_Purpur_1_coat_wc.png"));
   color_vector.push_back(string("Yellow/Neapelgelb_1_coat_wc.png"));

   color_vector.push_back(string("Tan/Umbra_natur_1_coat_wc.png"));
   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));

   color_vector.push_back(string("Orange/Chromorangeton_1_coat_wc.png"));
   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));

   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));
   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));

/* ** (2) */

   status = replace(98, 98, 6, 0);

   status = replace(99, 110, 6, &color_vector);

 #endif 


/* * (1) */

#if 0 

   c5

   color_vector.push_back(string("Red/Lasurdunkelrot_2_coats_wc.png"));
   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));

   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));
   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));

   color_vector.push_back(string("Brown/Caput_mortuum_2_coats_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Yellow/Lasurgoldgruen_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));
   color_vector.push_back(string("Blue/Preussischblau_2_coats_wc.png"));

   color_vector.push_back(string("Tan/Neapelgelb_roetlich_2_coats_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_2_coats_wc.png"));


/* ** (2) */

   status = replace(85, 85, 5, 0);

   status = replace(86, 97, 5, &color_vector);
#endif 

/* * (1) */

#if 0

   /* c4  */

   color_vector.push_back(string("Green/Hookersgruen_1_2_coats_wc.png"));
   color_vector.push_back(string("Blue/Delftblau_1_coat_wc.png"));

   color_vector.push_back(string("Brown/Umbra_gebrannt_2_coats_wc.png"));
   color_vector.push_back(string("Red/Rubinrot_1_coat_wc.png"));

   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));
   color_vector.push_back(string("Blue/Pariserblau_2_coats_wc.png"));

   color_vector.push_back(string("Tan/Umbra_natur_1_coat_wc.png"));
   color_vector.push_back(string("Red/Lasurdunkelrot_3_coats_wc.png"));

   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));
   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));

   color_vector.push_back(string("Red/Krapplack_dunkel_wc.png"));
   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));

/* ** (2) */

   status = replace(72, 72, 4, 0);

   status = replace(73, 84, 4, &color_vector);
#endif 



/* * (1) */ 



#if 0 

   /* c3 using |replace| function. */

   color_vector.push_back(string("Green/Chromoxidgruen_stumpf_1_coat_wc.png"));
   color_vector.push_back(string("Red/Kadmiumrot_mittel_1_coat_wc.png"));

   color_vector.push_back(string("Orange/Gelborange_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Pariserblau_2_coats_wc.png"));

   color_vector.push_back(string("Yellow/Neapelgelb_1_coat_wc.png"));
   color_vector.push_back(string("Purple/Cobalt_violet_dark_1_coat_wc.png"));

   color_vector.push_back(string("Green/Scheveningen_green_wc.png"));
   color_vector.push_back(string("Yellow/Turners_Gelb_2_coats_wc.png"));

   color_vector.push_back(string("Yellow/Zitronengelb_1_coat_wc.png"));
   color_vector.push_back(string("Blue/Pariserblau_2_coats_wc.png"));

   color_vector.push_back(string("Purple/Cobalt_violet_dark_2_coats_wc.png"));
   color_vector.push_back(string("Orange/Chromorangeton_2_coats_wc.png"));


/* ** (2) */

   status = replace(59, 59, 3, 0);

   status = replace(60, 71, 3, &color_vector);

#endif 


#if 0 
   c2     

   % front, A 
   Periwinkle_rgb panel
   red letter

   % F, back, 
   turquoise  panel
   goldenrod letter

   Maroon_rgb; % Left, panel      
   cyan; % P, left

   orange; % W, right, panel
   blue; % W, right

   yellow; % L, top, panel
   magenta; % L, top

   pink; % C, bottom, panel
   green; % C, bottom

#endif 

/* ** (2) */


#if 0 
c3

Back
red;
green;

Left
blue;
cyan;

Top
magenta;
orange;

Bottom
purple;
yellow;

Right
violet;
teal_blue;

Front
rose_madder;
dark_green;
#endif 

/* ** (2) */

#if 0 
/* *** (3) c3 */

   for (int i = 60; i <= 71; i++) //

/* **** (4) Front */

      s << "convert -transparent " <<  rose_madder << " aint_she_sweet_" << zeroes << i << ".png A.png; "
        << "composite A.png IPNG_Backgrounds/Green/Chromoxidgruen_stumpf_1_coat_wc.png B.png; "
        << "convert -transparent " << dark_green << " B.png -define png:color-type=6 C.png; "
        << "composite C.png IPNG_Backgrounds/Red/Kadmiumrot_mittel_1_coat_wc.png -define png:color-type=6 "
        << "D.png; ";

/* **** (4) Right */

/* violet; Gelborange_1_coat_wc.png      */
/* teal_blue; Pariserblau_2_coats_wc.png */

      s << "convert -transparent " <<  violet << " D.png E.png; "
        << "composite E.png IPNG_Backgrounds/Orange/Gelborange_1_coat_wc.png -define png:color-type=6 F.png; "
        << "convert -transparent " << teal_blue << " F.png G.png; "
        << "composite G.png IPNG_Backgrounds/Blue/Pariserblau_2_coats_wc.png "
        << "-define png:color-type=6 H.png; ";


/* **** (4) Bottom */

/* purple; Neapelgelb_1_coat_wc.png         */
/* yellow; Cobalt_violet_dark_1_coat_wc.png */

      s << "convert -transparent " <<  purple << " H.png I.png; "
        << "composite I.png IPNG_Backgrounds/Yellow/Neapelgelb_1_coat_wc.png -define png:color-type=6 J.png; "
        << "convert -transparent " << yellow << " J.png K.png; "
        << "composite K.png IPNG_Backgrounds/Purple/Cobalt_violet_dark_1_coat_wc.png "
        << "-define png:color-type=6 L.png; ";




/* **** (4) Back */

/* red;   Scheveningen_green_wc.png */
/* green;  Turners_Gelb_2_coats_wc.png */

      s << "convert -transparent " << red << " L.png M.png; "
        << "composite M.png  IPNG_Backgrounds/Green/Scheveningen_green_wc.png "
        << "-define png:color-type=6 N.png; "
        << "convert -transparent " << green << " N.png O.png; "
        << "composite O.png IPNG_Backgrounds/Yellow/Turners_Gelb_2_coats_wc.png "
        << "-define png:color-type=6 P.png; ";



/* **** (4) Left */

/* blue; Zitronengelb_1_coat_wc.png  */
/* cyan; Pariserblau_2_coats_wc.png  */

      s << "convert -transparent " << blue << " P.png Q.png; "
        << "composite Q.png IPNG_Backgrounds/Yellow/Zitronengelb_1_coat_wc.png "
        << "-define png:color-type=6 R.png; "
        << "convert -transparent " << cyan << " R.png S.png; "
        << "composite S.png IPNG_Backgrounds/Blue/Pariserblau_2_coats_wc.png "
        << "-define png:color-type=6 T.png; ";

/* **** (4) Top */

/* magenta; Cobalt_violet_dark_2_coats_wc.png */
/* orange;  Chromorangeton_2_coats_wc.png */

      s << "convert -transparent " << magenta << " T.png U.png; "
        << "composite U.png IPNG_Backgrounds/Purple/Cobalt_violet_dark_2_coats_wc.png "
        << "-define png:color-type=6 V.png; "
        << "convert -transparent " << orange << " V.png W.png; "
        << "composite W.png IPNG_Backgrounds/Orange/Chromorangeton_2_coats_wc.png "
        << "-define png:color-type=6 X.png; ";

/* *** (3) Combine */        
    
#if 0 
      s << "convert -transparent white X.png c3_" << zeroes << i << ".png; "
        << "composite c3_" << zeroes << i << ".png "
        << "c2_0058.png A.png; "
        << "composite A.png IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_" << zeroes << i << ".png; ";
#endif 


/* *** (3) c2 */

      (int i = 47; i <= 58; i++, j++)

      /* Front  */

      s << "convert -transparent " <<  periwinkle << " aint_she_sweet_" << zeroes << i << ".png A.png; "
        << "composite A.png IPNG_Backgrounds/Umbra_natur_1_coat_wc.png B.png; "
        << "convert -transparent " << red << " B.png C.png; "
        << "composite C.png IPNG_Backgrounds/Pariserblau_2_coats_wc.png D.png; ";

      /* Bottom */

      s << "convert -transparent " <<  pink << " D.png E.png; "
        << "composite E.png IPNG_Backgrounds/Caput_mortuum_2_coats_wc.png F.png; "
        << "convert -transparent " << green << " F.png G.png; "
        << "composite G.png IPNG_Backgrounds/Rubinrot_1_coat_wc.png H.png; ";

      /* Right */

      s << "convert -transparent " << orange << " H.png I.png; "
        << "composite I.png IPNG_Backgrounds/Chinacridon_Purpur_1_coat_wc.png J.png; "
        << "convert -transparent " << blue << " J.png K.png; "
        << "composite K.png IPNG_Backgrounds/Chromoxidgruen_stumpf_1_coat_wc.png "
        << "L.png; ";

      /* Back */

      s << "convert -transparent " << turquoise << " L.png M.png; "
        << "composite M.png IPNG_Backgrounds/Neapelgelb_1_coat_wc.png N.png; "
        << "convert -transparent " << goldenrod << " N.png O.png; "
        << "composite O.png IPNG_Backgrounds/Scheveningen_green_wc.png "
        << "P.png; ";
      

      /* Left, P */

      s << "convert -transparent " << maroon << " P.png Q.png; "
        << "composite Q.png IPNG_Backgrounds/Delftblau_1_coat_wc.png R.png; "
        << "convert -transparent " << cyan << " R.png S.png; "
        << "composite S.png IPNG_Backgrounds/Gelborange_1_coat_wc.png "
        << "T.png; ";


      /* Top, "L" */

      s << "convert -transparent " << yellow << " T.png U.png; "
        << "composite U.png IPNG_Backgrounds/Neapelgelb_roetlich_2_coats_wc.png V.png; "
        << "convert -transparent " << magenta << " V.png W.png; "
        << "composite W.png IPNG_Backgrounds/Lasurdunkelrot_3_coats_wc.png "
        << "c2_" << zeroes << i << ".png; ";

#endif 



/* * (1) */

#if 0 
      s << "convert -transparent white aint_she_sweet_" << zeroes << i << ".png "
        << "c3_"<< zeroes << i << ".png; "
        << "composite c3_" << zeroes << i << ".png c2_0058.png A.png; "
        << "composite A.png IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "combined_" << zeroes << i << ".png; ";
#endif 

/* * (1) */



#if 0 


      s << "convert -transparent white L.png M.png; "
        << "composite M.png Arches_140lb_wc_paper.png N.png; ";

      s << "convert -transparent " << green << " J.png K.png; "
        << "composite K.png Chromoxidgruen_stumpf_1_coat_wc.png L.png; ";

      s << "convert -transparent " << pink << " H.png I.png; "
        << "composite I.png Caput_mortuum_2_coats_wc.png J.png; ";

      s << "convert -transparent blue F.png G.png; "
        << "composite G.png Chromorangeton_2_coats_wc.png H.png; ";

      s << "convert -transparent red D.png E.png; "
        << "composite E.png Pariserblau_2_coats_wc.png F.png; ";

      s << "convert -transparent " << periwinkle << " aint_she_sweet_0254.png A.png; "
        << "composite A.png Neapelgelb_1_coat_wc.png B.png; "
        << "convert -transparent orange B.png C.png; "
        << "composite C.png Chinacridon_Purpur_1_coat_wc.png D.png; ";



      s << "convert -fill white -opaque red -opaque blue -opaque " << green << " "
        << "-opaque " << periwinkle << " -opaque " << pink << " -opaque orange "
        << "aint_she_sweet_0254.png A0254.png; ";

#endif 

#if 0
      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";
#endif 




/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


        