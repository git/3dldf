/* replace.cxx  */
/* Created by Laurence D. Finston (LDF) Mi 13. Mär 16:19:36 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs_extern.hxx"


// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

int
replace(int start, int end, int cube_ctr, vector<string> *ccolor_vector = 0)
{
   stringstream s;

   int status = 0;

   string zeroes;
   string zeroes_1;

   for (int i = start; i <= end; i++)
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (start < 10)
         zeroes_1 = "000";
      else if (start < 100)
         zeroes_1 = "00";
      else if (start < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      if (ccolor_vector == 0)
         goto PAPER;

/* **** (4) */

/* red */
/* green */

      s << "convert -transparent " <<  red << " aint_she_sweet_" << zeroes << i << ".png "
        << "-define png:color-type=6 A.png; "
        << "composite A.png IPNG_Backgrounds/" << (*ccolor_vector)[0] << " -define png:color-type=6 B.png; "
        << "convert -transparent " << green << " B.png -define png:color-type=6 C.png; "
        << "composite C.png IPNG_Backgrounds/" << (*ccolor_vector)[1] << " "
        << "-define png:color-type=6 D.png; ";

/* **** (4) */

/* blue   */ 
/* cyan */ 

      s << "convert -transparent " <<  blue << " D.png -define png:color-type=6 E.png; "
        << "composite E.png IPNG_Backgrounds/" << (*ccolor_vector)[2] << " -define png:color-type=6 F.png; "
        << "convert -transparent " << cyan << " F.png -define png:color-type=6 G.png; "
        << "composite G.png IPNG_Backgrounds/" << (*ccolor_vector)[3] 
        << " -define png:color-type=6 H.png; ";



/* **** (4) */

/* magenta  */ 
/* yellow */ 


      s << "convert -transparent " <<  magenta << " H.png -define png:color-type=6 I.png; "
        << "composite I.png IPNG_Backgrounds/" << (*ccolor_vector)[4] << " -define png:color-type=6 J.png; "
        << "convert -transparent " << yellow << " J.png -define png:color-type=6 K.png; "
        << "composite K.png IPNG_Backgrounds/" << (*ccolor_vector)[5]
        << " -define png:color-type=6 L.png; ";

/* **** (4) */

/* purple   */ 
/* orange */ 




      s << "convert -transparent " << purple << " L.png -define png:color-type=6 M.png; "
        << "composite M.png  IPNG_Backgrounds/" << (*ccolor_vector)[6] << " -define png:color-type=6 N.png; "
        << "convert -transparent " << orange << " N.png -define png:color-type=6 O.png; "
        << "composite O.png IPNG_Backgrounds/" << (*ccolor_vector)[7]
        << " -define png:color-type=6 P.png; ";



/* **** (4) */

/* violet  */
/* teal_blue  */


      s << "convert -transparent " << violet << " P.png -define png:color-type=6 Q.png; "
        << "composite Q.png IPNG_Backgrounds/" << (*ccolor_vector)[8] << " -define png:color-type=6 R.png; "
        << "convert -transparent " << teal_blue << " R.png -define png:color-type=6 S.png; "
        << "composite S.png IPNG_Backgrounds/" << (*ccolor_vector)[9]
        << " -define png:color-type=6 T.png; ";

/* **** (4) */

/* rose_madder */
/* dark_green */

      s << "convert -transparent " << rose_madder << " T.png -define png:color-type=6 U.png; "
        << "composite U.png IPNG_Backgrounds/" << (*ccolor_vector)[10] << " -define png:color-type=6 V.png; "
        << "convert -transparent " << dark_green << " V.png -define png:color-type=6 W.png; "
        << "composite W.png IPNG_Backgrounds/" << (*ccolor_vector)[11]
        << " -define png:color-type=6 X.png; ";
	

/* *** (3) Combine */        

PAPER:

      if (ccolor_vector == 0)
         s << "cp aint_she_sweet_" << zeroes_1 << start << ".png X.png; ";



      s << "convert -transparent white X.png -define png:color-type=6 c"
        << cube_ctr << "_" << zeroes << i << ".png; "
        << "composite c" << cube_ctr << "_" << zeroes << i << ".png "
        << "c2_0058.png -define png:color-type=6 A.png; ";

      if (cube_ctr > 3)
         s << "composite A.png c3_0071.png B.png; mv B.png A.png; ";

      if (cube_ctr > 4)
         s << "composite A.png c4_0084.png B.png; mv B.png A.png; ";

      if (cube_ctr > 5)
         s << "composite A.png c5_0097.png B.png; mv B.png A.png; ";

      if (cube_ctr > 6)
         s << "composite A.png c6_0110.png B.png; mv B.png A.png; ";

#if 0 
      /* c7 not included.  */

      if (cube_ctr > 7)
         s << "composite A.png c7_0123.png B.png; mv B.png A.png; ";
#endif 

      if (cube_ctr > 8)
         s << "composite A.png c8_0136.png B.png; mv B.png A.png; ";

      if (cube_ctr > 9)
         s << "composite A.png c9_0149.png B.png; mv B.png A.png; ";

      if (cube_ctr > 10)
         s << "composite A.png c10_0162.png B.png; mv B.png A.png; ";

#if 0 
      /* c11 not included. */

      if (cube_ctr > 11)
         s << "composite A.png c11_0.png B.png; mv B.png A.png; ";
#endif 

      if (cube_ctr > 12)
         s << "composite A.png c12_0188.png B.png; mv B.png A.png; ";


      if (cube_ctr > 13)
         s << "composite A.png c13_0201.png B.png; mv B.png A.png; ";

      if (cube_ctr > 14)
         s << "composite A.png c14_0214.png B.png; mv B.png A.png; ";

      if (cube_ctr > 15)
         s << "composite A.png c15_0227.png B.png; mv B.png A.png; ";

      /* Order of compositing is reversed for `cube_ctr' == 17, because
         cube 17 is partially obscured by cube 16.  LDF 2024.03.15.  */

      if (cube_ctr > 16) 
         s << "composite c16_0240.png A.png  B.png; mv B.png A.png; ";
  
      s << "composite A.png IPNG_Backgrounds/Paper/Arches_140lb_wc_paper.png "
        << "-define png:color-type=6 combined_" << zeroes << i << ".png; ";


       if (ccolor_vector)
          s << "mv I.png HH.png; "; /* The name `I.png' interferes with name completion
                                       in Emacs and in the shell when accessing
                                       the subdirectory `IPNG_Backgrounds'.
                                       LDF 2024.03.14.  */

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }

      if (ccolor_vector == 0)
         break;

   }  /* |for|  */
   

/* ** (2) */

   return 0;

} /* End of |replace| definition  */

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */
