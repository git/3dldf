%% presents_setup.ldf
%% Created by Laurence D. Finston (LDF) Di 5. Mär 13:18:33 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)



%% * (1)

beginfig(fig_ctr);

%% ** (2)

  fill frame with_color light_gray;
  draw frame with_pen big_square_pen with_color blue;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2)

  c1 := (c0 scaled (1, 1, 10)) shifted (-17.5, 14, 18);

  for i = 0 upto 5:
    R[1][i] := get_rectangle (i) c1;
  endfor;

  %draw c1 on_picture boxes1;
  r1 := get_rectangle 0 c1;
  fill r1 with_color white on_picture boxes1;
  draw r1 on_picture boxes1;
  a1 := get_center r1;


  if do_labels:
    dotlabel.bot("{\Tinyrm a1}", a1);
  fi;

  j := 2;
  for i = 5 step 5 until 35:
    c[j] := c1 shifted (i, 0);
    %draw c[j] on_picture boxes[j];
    r[j] := get_rectangle 0 c[j];
    a[j] := get_center r[j];
    fill r[j] with_color white on_picture boxes[j];
    draw r[j] on_picture boxes[j];
    if do_labels:
      s := "{\Tinyrm a" & decimal j & "}";
      dotlabel.bot(s, a[j]);
    fi;
    for k = 0 upto 5:
      R[j][k] := get_rectangle (k) c[j];
    endfor;
    j += 1;
  endfor;

  c9 := c1 shifted (1, -9);


  
  %draw c9 on_picture boxes9;
  r9 := get_rectangle 0 c9;
  fill r9 with_color white on_picture boxes9;
  draw r9 on_picture boxes9;
  a9 := get_center r9;
  if do_labels:
    dotlabel.bot("{\Tinyrm a9}", a9);
  fi;

  for k = 0 upto 5:
    R[9][k] := get_rectangle (k) c[9];
  endfor;

  j := 10;

  for i = 1 upto 6:
    c[j] := c9 shifted (5.5i, 0);
    %draw c[j] on_picture boxes[j];
    r[j] := get_rectangle 0 c[j];
    a[j] := get_center r[j];
    fill r[j] with_color white on_picture boxes[j];
    draw r[j] on_picture boxes[j];
    if do_labels:
      s := "{\Tinyrm a" & decimal j & "}";
      dotlabel.bot(s, a[j]);
    fi;

    for k = 0 upto 5:
      R[j][k] := get_rectangle (k) c[j];
    endfor;
    
    j += 1;
  endfor;

  c16 := c1 shifted (0, -18);

  %draw c16 on_picture boxes16;
  r16 := get_rectangle 0 c16;
  fill r16 with_color white on_picture boxes16;
  draw r16 on_picture boxes16;
  a16 := get_center r16;
  if do_labels:
    dotlabel.bot("{\Tinyrm a16}", a16);
  fi;

  for k = 0 upto 5:
    R[16][k] := get_rectangle (k) c[16];
  endfor;

  
  j := 17;
  for i = 5 step 5 until 35:
    c[j] := c16 shifted (i, 0);
    %draw c[j] on_picture boxes[j];
    r[j] := get_rectangle 0 c[j];
    a[j] := get_center r[j];
    fill r[j] with_color white on_picture boxes[j];
    draw r[j] on_picture boxes[j];
    if do_labels:
      s := "{\Tinyrm a" & decimal j & "}";
      dotlabel.bot(s, a[j]);
    fi;

    for k = 0 upto 5:
      R[j][k] := get_rectangle (k) c[j];
    endfor;
  
    j += 1;
  endfor;

%% ** (2) Sides of boxes

%% *** (3) Top row
  
%% **** (4)

  fill R[1][3] with_color dark_gray on_picture boxes1; % L
  fill R[1][5] with_color gray on_picture boxes1;
  draw R[1][3] on_picture boxes1;
  draw R[1][5] on_picture boxes1;

%% **** (4)
  
  fill R[2][3] with_color dark_gray on_picture boxes2; % A (LA)
  fill R[2][5] with_color gray on_picture boxes2;
  draw R[2][3] on_picture boxes2;
  draw R[2][5] on_picture boxes2;

%% **** (4)
  
  fill R[3][3] with_color dark_gray on_picture boxes3; % U (LAU)
  draw R[3][3] on_picture boxes3;
  fill R[3][5] with_color gray on_picture boxes3;
  draw R[3][5] on_picture boxes3;

%% **** (4)
  
  %draw c4 on_picture boxes4; % R (LAUR)
  fill R[4][3] with_color dark_gray on_picture boxes4;
  draw R[4][3] on_picture boxes4;
  fill R[4][5] with_color gray on_picture boxes4;
  draw R[4][5] on_picture boxes4;

%% **** (4)
  
  %draw c5 on_picture boxes5; % E (LAURE)
  fill R[5][2] with_color dark_gray on_picture boxes5;
  draw R[5][2] on_picture boxes5;
  fill R[5][5] with_color gray on_picture boxes5;
  draw R[5][5] on_picture boxes5;

%% **** (4)
  
  %draw c6 on_picture boxes6; % N (LAUREN)
  fill R[6][2] with_color dark_gray on_picture boxes6;
  draw R[6][2] on_picture boxes6;
  fill R[6][5] with_color gray on_picture boxes6;
  draw R[6][5] on_picture boxes6;

%% **** (4)
  
  %draw c7 on_picture boxes7;  % C (LAURENC)
  fill R[7][2] with_color dark_gray on_picture boxes7;
  draw R[7][2] on_picture boxes7;
  fill R[7][5] with_color gray on_picture boxes7;
  draw R[7][5] on_picture boxes7;

%% **** (4)
  
  %draw c8 on_picture boxes8;  % N (LAURE)
  fill R[8][2] with_color dark_gray on_picture boxes8;
  draw R[8][2] on_picture boxes8;
  fill R[8][5] with_color gray on_picture boxes8;
  draw R[8][5] on_picture boxes8;

%% **** (4)
  
%% *** (3) Middle row

%% **** (4) F
  
  %draw c9 on_picture boxes9;  % N (LAURE)
  fill R[9][3] with_color dark_gray on_picture boxes9;
  draw R[9][3] on_picture boxes9;


%% **** (4) I (FI)

  %draw c10 on_picture boxes10;  % N (LAURE)
  fill R[10][3] with_color dark_gray on_picture boxes10;
  draw R[10][3] on_picture boxes10;

  
%% **** (4) N (FIN)

  %draw c11 on_picture boxes11;  % N (LAURE)
  fill R[11][3] with_color dark_gray on_picture boxes11;
  draw R[11][3] on_picture boxes11;
  
%% **** (4) S (FINS)
  
  %draw c12 on_picture boxes12;  % N (LAURE)
  
%% **** (4) T (FINST)

  %draw c13 on_picture boxes13;  % N (LAURE)
  fill R[13][2] with_color dark_gray on_picture boxes13;
  draw R[13][2] on_picture boxes13;
  
%% **** (4) O (FINSTO)

  %draw c14 on_picture boxes14;  % N (LAURE)
  fill R[14][2] with_color dark_gray on_picture boxes14;
  draw R[14][2] on_picture boxes14;

%% **** (4) N (FINSTON)
    
  %draw c15 on_picture boxes15;  % N (LAURE)
  fill R[15][2] with_color dark_gray on_picture boxes15;
  draw R[15][2] on_picture boxes15;

%% **** (4)
  
%% *** (3) Bottom row

%% **** (4) P 
    
  %draw c16 on_picture boxes16;  
  fill R[16][3] with_color dark_gray on_picture boxes16;
  draw R[16][3] on_picture boxes16;
  fill R[16][4] with_color gray on_picture boxes16;
  draw R[16][4] on_picture boxes16;
  

%% **** (4) R (PR) 
    
  %draw c17 on_picture boxes17;  
  fill R[17][3] with_color dark_gray on_picture boxes17;
  draw R[17][3] on_picture boxes17;
  fill R[17][4] with_color gray on_picture boxes17;
  draw R[17][4] on_picture boxes17;
  
%% **** (4) E (PRE)
    
  %draw c18 on_picture boxes18;  
  fill R[18][3] with_color dark_gray on_picture boxes18;
  draw R[18][3] on_picture boxes18;
  fill R[18][4] with_color gray on_picture boxes18;
  draw R[18][4] on_picture boxes18;
  
%% **** (4) S (PRES)
    
  %draw c19 on_picture boxes19;  
  fill R[19][3] with_color dark_gray on_picture boxes19;
  draw R[19][3] on_picture boxes19;
  fill R[19][4] with_color gray on_picture boxes19;
  draw R[19][4] on_picture boxes19;
  
%% **** (4) E (PRESE)
    
  %draw c20 on_picture boxes20;  
  fill R[20][2] with_color dark_gray on_picture boxes20;
  draw R[20][2] on_picture boxes20;
  fill R[20][4] with_color gray on_picture boxes20;
  draw R[20][4] on_picture boxes20;

%% **** (4) N (PRESEN)
    
  %draw c21 on_picture boxes21;  
  fill R[21][2] with_color dark_gray on_picture boxes21;
  draw R[21][2] on_picture boxes21;
  fill R[21][4] with_color gray on_picture boxes21;
  draw R[21][4] on_picture boxes21;


%% **** (4) T (PRESENT)
    
  %draw c22 on_picture boxes22;  
  fill R[22][2] with_color dark_gray on_picture boxes22;
  draw R[22][2] on_picture boxes22;
  fill R[22][4] with_color gray on_picture boxes22;
  draw R[22][4] on_picture boxes22;

%% **** (4) S (PRESENTS)
    
  %draw c23 on_picture boxes23;  
  fill R[23][2] with_color dark_gray on_picture boxes23;
  draw R[23][2] on_picture boxes23;
  fill R[23][4] with_color gray on_picture boxes23;
  draw R[23][4] on_picture boxes23;

%% **** (4)

%% *** (3)  
  
%% ** (2) Letters

%% *** (3) Top row  
  
%% **** (4) L

  A0 := q24 scaled (.25, .25) shifted by (a1 - p173 scaled (.25, .25));

  %t0 := c1 self_rotated (0, 0, 180); % This works.  LDF 2024.03.05.
  %A0 *= t0;
  
  fill A0 with_color cv1 on_picture letters1;
  draw A0 on_picture letters1;

%% **** (4) A (LA), c1, v1.

  A1 := q0 scaled (.25, .25) shifted by (a2 - p109 scaled (.25, .25));
  A2 := q1 scaled (.25, .25) shifted by (a2 - p109 scaled (.25, .25));
  fill A1 with_color cv1 on_picture letters2;
  fill A2 with_color white on_picture letters2;
  draw A1 on_picture letters2;
  draw A2 on_picture letters2;

%% **** (4) U (LAU)

  A3 := Q0 scaled (.25, .25) shifted by (a3 - p231 scaled (.25, .25));
  fill A3 with_color cv1 on_picture letters3;
  draw A3 on_picture letters3;

%% **** (4) R (LAUR)

  A4 := q6 scaled (.25, .25) shifted by (a4 - p125 scaled (.25, .25));
  A5 := q7 scaled (.25, .25) shifted by (a4 - p125 scaled (.25, .25));
  fill A4 with_color cv1 on_picture letters4;
  draw A4 on_picture letters4;
  unfill A5 on_picture letters4;
  draw A5 on_picture letters4;

%% **** (4) E (LAURE)

  A6 := q9 scaled (.25, .25) shifted by (a5 - p132 scaled (.25, .25));
  fill A6 with_color cv1 on_picture letters5;
  draw A6 on_picture letters5;

%% **** (4) N (LAUREN)

  A7 := Q2 scaled (.25, .25) shifted by (a6 - p239 scaled (.25, .25));
  fill A7 with_color cv1 on_picture letters6;
  draw A7 on_picture letters6;


%% **** (4) C (LAURENC)

  A8 := Q4 scaled (.25, .25) shifted by (a7 - p250 scaled (.25, .25));
  fill A8 with_color cv1 on_picture letters7;
  draw A8 on_picture letters7;

%% **** (4) E (LAURENCE)

  A9 := q9 scaled (.25, .25) shifted by (a8 - p132 scaled (.25, .25));
  fill A9 with_color cv1 on_picture letters8;
  draw A9 on_picture letters8;
  
%% *** (3) Middle row

%% **** (4) F 

  A10 := Q6 scaled (.25, .25) shifted by (a9 - p261 scaled (.25, .25));
  fill A10 with_color cv2 on_picture letters9;
  draw A10 on_picture letters9;
  
%% **** (4) I (FI)

  A11 := q19 scaled (.25, .25) shifted by (a10 - p164 scaled (.25, .25));
  fill A11 with_color cv2 on_picture letters10;
  draw A11 on_picture letters10;


%% **** (4) N (FIN)

  A12 := Q2 scaled (.25, .25) shifted by (a11 - p239 scaled (.25, .25));
  fill A12 with_color cv2 on_picture letters11;
  draw A12 on_picture letters11;

  
%% **** (4) S (FINS)

  A13 := q28 scaled (.25, .25) shifted by (a12 - p176 scaled (.25, .25));
  fill A13 with_color cv2 on_picture letters12;
  draw A13 on_picture letters12;


%% **** (4) T (FINST)

  A14 := q11 scaled (.25, .25) shifted by (a13 - p147 scaled (.25, .25));
  fill A14 with_color cv2 on_picture letters13;
  draw A14 on_picture letters13;

  
%% **** (4) O (FINSTO)

  A15 := q47 scaled (.25, .25) shifted by (a14 - p197 scaled (.25, .25));
  fill A15 with_color cv2 on_picture letters14;
  draw A15 on_picture letters14;
  A16 := q48 scaled (.25, .25) shifted by (a14 - p197 scaled (.25, .25));
  unfill A16 on_picture letters14;
  draw A16 on_picture letters14;
  
%% **** (4) N (FINSTON)

  A17 := Q2 scaled (.25, .25) shifted by (a15 - p239 scaled (.25, .25));
  fill A17 with_color cv2 on_picture letters15;
  draw A17 on_picture letters15;
  
%% *** (3) Bottom row

%% **** (4) P 

  % Change suffixes when I've done the middle row.  LDF 2024.03.05.
  
  A18 := q3 scaled (.25, .25) shifted by (a16 - p123 scaled (.25, .25));
  A19 := q4 scaled (.25, .25) shifted by (a16 - p123 scaled (.25, .25));
  fill A18 with_color cv3 on_picture letters16;
  draw A18 on_picture letters16;
  unfill A19 on_picture letters16;
  draw A19 on_picture letters16;
  

%% **** (4) R

  A20 := q6 scaled (.25, .25) shifted by (a17 - p125 scaled (.25, .25));
  A21 := q7 scaled (.25, .25) shifted by (a17 - p125 scaled (.25, .25));
  fill A20 with_color cv3 on_picture letters17;
  draw A20 on_picture letters17;
  unfill A21 on_picture letters17;
  draw A21 on_picture letters17;

%% **** (4) E

  A22 := q9 scaled (.25, .25) shifted by (a18 - p132 scaled (.25, .25));
  fill A22 with_color cv3 on_picture letters18;
  draw A22 on_picture letters18;

%% **** (4) S

  A23 := q28 scaled (.25, .25) shifted by (a19 - p176 scaled (.25, .25));
  fill A23 with_color cv3 on_picture letters19;
  draw A23 on_picture letters19;

  

%% **** (4) E

  A24 := q9 scaled (.25, .25) shifted by (a20 - p132 scaled (.25, .25));
  fill A24 with_color cv3 on_picture letters20;
  draw A24 on_picture letters20;


%% **** (4) N

  A25 := Q2 scaled (.25, .25) shifted by (a21 - p239 scaled (.25, .25));
  fill A25 with_color cv3 on_picture letters21;
  draw A25 on_picture letters21;

%% **** (4) T

  A26 := q11 scaled (.25, .25) shifted by (a22 - p147 scaled (.25, .25));
  fill A26 with_color cv3 on_picture letters22;
  draw A26 on_picture letters22;
  
%% **** (4) S

  A27 := q28 scaled (.25, .25) shifted by (a23 - p176 scaled (.25, .25));
  fill A27 with_color cv3 on_picture letters23;
  draw A27 on_picture letters23;

%% **** (4)  

%% ** (2)  Combine pictures

%% *** (3) Top row


%% **** (4) L
  
  % This works.  LDF 2024.03.05.
  % t0 := c1 self_rotated (0, 0, 10);
  % temp_picture := boxes1;
  % temp_picture *= t0;
  % shift temp_picture (0, 0, 3);
  % current_picture += temp_picture;

  % temp_picture := letters1;
  % temp_picture *= t0;
  % shift temp_picture (0, 0, 3);
  % current_picture += temp_picture;

  current_picture += boxes1;   
  current_picture += letters1;

%% **** (4) A (LA)
  
  current_picture += boxes2;   
  current_picture += letters2;

%% **** (4) U (LAU)
  
  current_picture += boxes3;   
  current_picture += letters3;

%% **** (4) R (LAUR) 
  
  current_picture += boxes4;   
  current_picture += letters4;

%% **** (4) E (LAURE)
  
  current_picture += boxes5;   
  current_picture += letters5;

%% **** (4) E (LAURENCE)
  
  current_picture += boxes8;   
  current_picture += letters8;

%% **** (4)
  
  current_picture += boxes7;   % C (LAURENC)
  current_picture += letters7;

%% **** (4)
  
  current_picture += boxes6;   % N (LAUREN)
  current_picture += letters6;

%% **** (4)
  
%% *** (3) Middle row

%% **** (4)  

  current_picture += boxes9;       % F
  current_picture += letters9; 	   

%% **** (4)
  
  current_picture += boxes10;  	   % I (FI)       
  current_picture += letters10;	                  

%% **** (4)
  
  current_picture += boxes11;  	   % N (FIN)      
  current_picture += letters11;	                  

%% **** (4)
  
  current_picture += boxes15;      % N (FINSTON)  
  current_picture += letters15;

%% **** (4)
  
  current_picture += boxes14;  	   % O (FINSTO)   
  current_picture += letters14;	                  

%% **** (4)
  
  current_picture += boxes13;  	   % T (FINST)    
  current_picture += letters13;	                  

%% **** (4)
  
  current_picture += boxes12;  	   % S (FINS)     
  current_picture += letters12;	                  
			       	                  
%% **** (4)

%% *** (3) Bottom row

%% **** (4)
  
  current_picture += boxes16;   % P
  current_picture += letters16;

%% **** (4)
  
  current_picture += boxes17;   % R (PR)
  current_picture += letters17;

%% **** (4)

  current_picture += boxes18;   % E (PRE)
  current_picture += letters18;

%% **** (4)  

  current_picture += boxes19;   % S (PRES)
  current_picture += letters19;

%% **** (4)  

  current_picture += boxes23;   % S (PRESENTS)
  current_picture += letters23;

%% **** (4)
  
  current_picture += boxes22;   % T (PRESENT)
  current_picture += letters22;

%% **** (4)
  
  current_picture += boxes21;   % N (PRESEN)
  current_picture += letters21;

%% **** (4)
  
  current_picture += boxes20;   % E (PRESE)
  current_picture += letters20;

%% **** (4)
  
%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
