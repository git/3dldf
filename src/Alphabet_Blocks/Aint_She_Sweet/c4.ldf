%% c4.ldf
%% Created by Laurence D. Finston (LDF) Do 7. Mär 07:39:32 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) First row, third cube. c4/c104

do_fill := true; % false; % 

if do_fill:
  step_val := 1;
else:
  step_val := 2;
fi;

for i = 0 step step_val until 12:
  beginfig(fig_ctr);

    if do_black:
      fill frame shifted (0, 0) with_color black;
    else:
      if do_png:
	fill frame shifted (0, 0) with_color white;
      fi;
    fi;
    
    draw frame shifted (0, 0) with_pen big_square_pen; %  with_color blue; %  shifted (-40, 20) 
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    % !! BUG:  This causes a segmentation fault error.  LDF 2024.03.03.
    % Find out why.  It works to copy v2 to current_picture and output
    % the latter.
    
    % output v3 with_focus f;

    if not do_incremental:
      current_picture := v2;
      current_picture += v3;
      output current_picture with_focus f;
      clear current_picture;
    fi;
    
    if do_black:
      draw c104 with_color white;
    else:
      draw c104;
    fi;

%% ** (2)

  if do_black:
    draw c104 with_color white;
  else:
    draw c104;
  fi;

%% ** (2) Back
  
  if i > 0:
    if do_fill:
      fill r113 with_color cv0; % Salmon_rgb;
    fi;
  fi;

  if i > 1:
    fill u137 with_color cv1; % blue; % G (back)
    draw u137;
  fi;

    draw r113;
  
%% ** (2) Left

  if i > 2:
    if do_fill:
      fill r114 with_color cv2; % Plum_rgb;
    fi;
  fi;

  if i > 3:
    fill u138 with_color cv3; % cyan; % Q, left
    
    if do_fill:
      fill u139 with_color cv2; % Plum_rgb;
    else:
      unfill u139;
    fi;
    draw u138;
    draw u139;
  fi;
  
  draw r114;

  
%% ** (2) Top

  if i > 4:
    if do_fill:
      fill r116 with_color cv4; % SkyBlue_rgb;
    fi;
  fi;
  
  if i > 5:
  fill u143 with_color cv5; % magenta; % Y, top
    draw u143;
  fi;
  
  draw r116;

%% ** (2) Right

  if i > 6:
    if do_fill:
      fill r115 with_color cv6; % RawSienna_rgb;
    fi;
  fi;

  if i > 7:
    fill u140 with_color cv7; % green; % B, right

    if do_fill:
      fill u141 with_color cv6; % RawSienna_rgb;
      fill u142 with_color cv6; % RawSienna_rgb;
    else:
      unfill u141;
      unfill u142;
    fi;

    draw u140;
    draw u141;
    draw u142;
  fi;


    draw r115;
  

%% ** (2) Bottom

  if i > 8:
    if do_fill:
      fill r117 with_color cv8; % purple;
    fi;
  fi;

  if i > 9:
    fill u144 with_color cv9; % orange; % U, bot
    draw u144;
  fi;
  
  draw r117;

%% ** (2) Front

  if i > 10:
    if do_fill:
      fill r112 with_color cv10; % Periwinkle_rgb;
    fi;
  fi;

  if i > 11:
    fill u105 with_color cv11; % red; % N (front)
    draw u105;
  fi;
    
  draw r112;
  
%% ** (2)

  
  % if do_labels:
  %   dotlabel.bot("{\smallrm fc112}", fc112);
  %   dotlabel.bot("{\smallrm fc113}", fc113);
  %   dotlabel.bot("{\smallrm fc114}", fc114);
  %   dotlabel.bot("{\smallrm fc115}", fc115);
  %   dotlabel.bot("{\smallrm fc116}", fc116);
  %   dotlabel.bot("{\smallrm fc117}", fc117);
  % fi;

  if do_labels:
    label.bot("{\smallrm c4/c104}", get_center c104) shifted (0, -15);
  fi;

%% ** (2)


    if i == 12:
      v4 := current_picture;
    fi;

  endfig with_focus f;
  fig_ctr += 1;
endfor;


%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
