/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 1. Mär 14:51:31 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */


// composite folding_0011.png folding_0018.png A.png

/* composite A.png folding_0025.png B.png */
/* composite B.png folding_0032.png C.png */
/* composite C.png folding_0039.png D.png */
/* composite D.png folding_0052.png E.png */






int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   string zeroes;
   string zeroes_1;
   string zeroes_2;


   int j = 31;

   int k = 24;

   for (int i = 0; i <= 60; i++, j++) // 52
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "000";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "000";
      else
         zeroes_1 = "";

      if (k < 10)
         zeroes_2 = "000";
      else if (k < 100)
         zeroes_2 = "00";
      else if (k < 1000)
         zeroes_2 = "000";
      else
         zeroes_2 = "";

      s << "composite M" << zeroes << i << ".png frame_white_black_border.png N" << zeroes << i << ".png";  

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
  }

   
  return 0;

}

#if 0

      s << "mv N" << zeroes << i << ".png M" << zeroes << i << ".png";  

      s << "composite folding_0012.png "
        << "folding_" << zeroes << i << ".png G.png; "        // Back/Top
        << "composite G.png folding_0011.png H.png; "         // Bot, B
        << "composite H.png folding_0019.png I.png; "         // Right, O
        << "composite folding_0026.png I.png J.png; "        // Left, K
        << "composite folding_0033.png J.png M" << zeroes_2 << k << ".png; " // Front
        << "rm G.png H.png I.png J.png; ";

      s << "composite folding_0012.png "
        << "folding_0046.png G.png; "                         // Back/Top
        << "composite G.png folding_0011.png H.png; "         // Bot, B
        << "composite H.png folding_0019.png I.png; "         // Right, O
        << "composite folding_0026.png I.png J.png; "        // Left, K
        << "composite folding_" << zeroes << i << ".png J.png M" << zeroes_2 << k << ".png; " // Front
        << "rm G.png H.png I.png J.png; ";

      s << "composite folding_0012.png "
        << "folding_0046.png G.png; "                         // Back/Top
        << "composite G.png folding_0011.png H.png; "         // Bot, B
        << "composite H.png folding_0019.png I.png; "         // Right, O
        << "composite folding_" << zeroes << i << ".png I.png J.png; "       // Left, K
        << "composite J.png folding_0039.png M" << zeroes_2 << k << ".png; " // Front
        << "rm G.png H.png I.png J.png; ";

      s << "mogrify -transparent white folding_" << zeroes << i << ".png";

      s << "composite folding_0012.png "
        << "folding_0046.png G.png; "                         // Back/Top
        << "composite G.png folding_0011.png H.png; "         // Bot
        << "composite H.png folding_0032.png I.png; "         // Left
        << "composite I.png folding_" << zeroes << i << ".png J.png; "          // Right
        << "composite J.png folding_0039.png M" << zeroes_2 << k << ".png; " // Front
        << "rm G.png H.png I.png J.png; ";

      s << "composite folding_0011.png folding_0012.png G.png; " // Bot/Back
        << "composite folding_0040.png G.png H.png; "            // Top
        << "composite folding_0026.png H.png I.png; "            // Left
        << "composite folding_0019.png I.png J.png; "            // Right
        << "composite folding_0033.png J.png K.png; "           // Front
        << "rm G.png H.png I.png J.png; ";

      s << "convert -transparent red -transparent " << green << " -transparent blue "
        << "-transparent cyan -transparent magenta -transparent orange "
        << "-transparent " << turquoise << " -transparent " << apricot 
        << " -transparent " << rose_madder << " -transparent " 
        << violet << " -transparent yellow -transparent " << sky_blue
        << " E.png F.png";

   s << "mogrify -transparent white folding_" << zeroes << i << ".png";

#endif 

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


