%% c12.ldf
%% Created by Laurence D. Finston (LDF) Sa 9. Mär 07:39:48 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Third row, first cube. c12/c112

do_fill := true; % false; % 

if do_fill:
  step_val := 1;
else:
  step_val := 2;
fi;

for i = 0 step step_val until 12:
  beginfig(fig_ctr);

    if do_black:
      fill frame shifted (0, 0) with_color black;
    else:
      if do_png:
        fill frame shifted (0, 0) with_color white;
      fi;
    fi;

    draw frame shifted (0, 0) with_pen big_square_pen; %  with_color blue; %  shifted (-40, 20) 
    output current_picture with_projection parallel_x_y;
    clear current_picture;

    % !! BUG:  This causes a segmentation fault error.  LDF 2024.03.03.
    % Find out why.  It works to copy v2 to current_picture and output
    % the latter.
    
    % output v3 with_focus f;

    if not do_incremental:
      current_picture := v2;
      current_picture += v3;
      current_picture += v4;
      current_picture += v5;
      current_picture += v6;
      current_picture += v7;
      current_picture += v8;
      current_picture += v9;
      current_picture += v10;
      current_picture += v11;
    fi;
    
    output current_picture with_focus f;
    clear current_picture;
    
    if do_black:
      draw c112 with_color white;
    else:
      draw c112;
    fi;


    
%% ** (2)

  if do_black:
    draw c112 with_color white;
  else:
    draw c112;
  fi;

%% ** (2) Back

  if i > 0:
    if do_fill:
      fill r161 with_color cv0; % RoyalBlue_rgb;
    fi;
  fi;

  draw r161;

  if i > 1:
    fill u175 with_color cv1; % Apricot_rgb; % 5 (numeral), back
    draw u175;
  fi;

%% ** (2) Left

  if i > 2:
    if do_fill:
      fill r162 with_color cv2; % RoyalPurple_rgb;
    fi;
  fi;

  draw r162;

  if i > 3:
    fill u176 with_color cv3; % Bittersweet_rgb; % Sigma, Greek letter, uppercase, left
    draw u176;
  fi;
    
  
%% ** (2) Bottom

  if i > 4:
    if do_fill:
      fill r165 with_color cv4; % Peach_rgb;
    fi;
  fi;

  draw r165;

  if i > 5:
    fill u180 with_color cv5; % NavyBlue_rgb; % Phi (Greek letter), bottom

    if do_fill:
      fill u181 with_color cv4; % Peach_rgb;
      fill u182 with_color cv4; % Peach_rgb;
    else:
      unfill u181;
      unfill u182;
    fi;

    draw u180;
    draw u181;
    draw u182;
  fi;
  

%% ** (2) Top
  
  if i > 6:
    if do_fill:
      fill r164 with_color cv6; % violet;
    fi;
  fi;

  draw r164;

  if i > 7:
    fill u178 with_color cv7; % Dandelion_rgb; % D, top
    if do_fill:
      fill u179 with_color cv6; % violet;
    else:
      unfill u179;
    fi;

    draw u178; 
    draw u179;
  fi;
  
%% ** (2) Right

  if i > 8:
    if do_fill:
      fill r163 with_color cv8; % pink;
    fi;
  fi;

  draw r163;

  if i > 9:
    fill u177 with_color cv9; % ForestGreen_rgb; % E, right
    draw u177;
  fi;

%% ** (2) Front

  if i > 10:
    if do_fill:
      fill r160 with_color cv10; % Tan_rgb;
    fi;
  fi;
  
  draw r160;

  if i > 11:
    fill u115 with_color cv11; % SkyBlue_rgb; % S, front 
    draw u115;
  fi;
  
%% ** (2)

  % if do_labels:
  %   dotlabel.bot("{\smallrm fc160}", fc160);
  %   dotlabel.bot("{\smallrm fc161}", fc161);
  %   dotlabel.bot("{\smallrm fc162}", fc162);
  %   dotlabel.bot("{\smallrm fc163}", fc163);
  %   dotlabel.bot("{\smallrm fc164}", fc164);
  %   dotlabel.bot("{\smallrm fc165}", fc165);
  % fi;

  if do_labels:
    label.bot("{\smallrm c12/c112}", get_center c112) shifted (0, -15);
  fi;

%% ** (2)


    if i == 12:
      v12 := current_picture;
    fi;

  endfig with_focus f;
  fig_ctr += 1;
endfor;


%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
