/* ttemp.cxx  */
/* Created by Laurence D. Finston (LDF) Do 15. Feb 16:10:51 CET 2024 */

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

stringstream s;

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

struct A
{
  string letter;
  string name;
  float max_z;
  float min_z;
  float mean_z;	

};

bool
compare_max_z(const A &a, const A &b)
{

  return a.max_z < b.max_z;
}

bool
compare_min_z(const A &a, const A &b)
{

  return a.min_z < b.min_z;
}

bool
compare_mean_z(const A &a, const A &b)
{

  return a.mean_z < b.min_z;
}

vector<A> paths;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   A curr_path;

   curr_path.letter = "Y";
   curr_path.name   = "q215";
   curr_path.max_z  = 0.08072459;
   curr_path.min_z  = -0.51014829;
   curr_path.mean_z = -0.16662410;
   paths.push_back(curr_path);

   curr_path.letter = "A";
   curr_path.name   = "q200";
   curr_path.max_z  =  0.08334640;
   curr_path.min_z  = -0.51727712;
   curr_path.mean_z = -0.32543191;
   paths.push_back(curr_path);

   curr_path.letter = "M";
   curr_path.name   = "q241";
   curr_path.max_z = 0.20000000;
   curr_path.min_z = 0.20000000;
   curr_path.mean_z = 0.19999996;
   paths.push_back(curr_path);

   curr_path.letter = "W";
   curr_path.name   = "q158";
   curr_path.max_z = 0.08334640;
   curr_path.min_z = -0.51727718;
   curr_path.mean_z = -0.15687205;
   paths.push_back(curr_path);

   curr_path.letter = "D";
   curr_path.name   = "q250";
   curr_path.max_z = -1.00000000;
   curr_path.min_z = -1.00000000;
   curr_path.mean_z = -1.00000000;
   paths.push_back(curr_path);

   curr_path.letter = "S";
   curr_path.name   = "q228";
   curr_path.max_z  = 0.02965878;
   curr_path.min_z  = -0.38998017;
   curr_path.mean_z = -0.15423194;
   paths.push_back(curr_path);

   cerr << "Before sorts:" << endl;

   for (vector<A>::iterator iter = paths.begin();
        iter != paths.end();
        ++iter)
   {
      cerr << "iter->letter == " << iter->letter << endl
           << "iter->name   == " << iter->name << endl
           << "iter->max_z  == " << iter->max_z << endl
           << "iter->min_z  == " << iter->min_z << endl
           << "iter->mean_z == " << iter->mean_z << endl
           << endl;

   }

   sort(paths.begin(), paths.end(), compare_max_z);

   cerr << "After sort max_z:" << endl;

   for (vector<A>::iterator iter = paths.begin();
        iter != paths.end();
        ++iter)
   {
      cerr << "iter->letter == " << iter->letter << endl
           << "iter->name   == " << iter->name << endl
           << "iter->max_z  == " << iter->max_z << endl
           << endl;

   }

   sort(paths.begin(), paths.end(), compare_min_z);

   cerr << "After sort min_z:" << endl;

   for (vector<A>::iterator iter = paths.begin();
        iter != paths.end();
        ++iter)
   {
      cerr << "iter->letter == " << iter->letter << endl
           << "iter->name   == " << iter->name << endl
           << "iter->min_z  == " << iter->min_z << endl
           << endl;

   }


   sort(paths.begin(), paths.end(), compare_mean_z);

   cerr << "After sort mean_z:" << endl;

   for (vector<A>::iterator iter = paths.begin();
        iter != paths.end();
        ++iter)
   {
      cerr << "iter->letter == " << iter->letter << endl
           << "iter->name   == " << iter->name << endl
           << "iter->mean_z  == " << iter->min_z << endl
           << endl;

   }







/* 




   Panels:
   q155:
   max_z = 

   0.20000000
   min_z = 

   -1.00000000
   mean_z = 

   -0.39999998
   q152:
   max_z = 

   0.20000000
   min_z = 

   -1.00000000
   mean_z = 

   -0.39999998
   q156:
   max_z = 

   0.20000000
   min_z = 

   -1.00000000
   mean_z = 

   -0.39999998
   q153:
   max_z = 

   0.20000000
   min_z = 

   0.20000000
   mean_z = 

   0.20000000
   q151:
   max_z = 

   -1.00000000
   min_z = 

   -1.00000000
   mean_z = 

   -1.00000000
   q154:
   max_z = 

   0.20000000
   min_z = 

   -1.00000000
   mean_z = 

   -0.40000001

*/


  return 0;

}


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */




