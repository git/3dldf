%% cone_test_1.ldf
%% Created by Laurence D. Finston (LDF) Mo 2. Dez 14:07:24 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

input "plainldf.lmc";

big_pen := pencircle scaled (.75mm, .75mm, .75mm);
big_dot_pen := pencircle scaled (2mm, 2mm, 2mm);

pen Big_dot_pen;
Big_dot_pen := pencircle scaled (3mm, 3mm, 3mm);

verbatim_metapost "prologues := 3;outputtemplate := \"%j_%2c.eps\";";
verbatim_metapost "dotlabeldiam:=1.5mm;labeloffset:=1.5mm;ahlength:=.25cm;";
verbatim_tex "\magnification=1750";

%% * (1)

frame_wd := 297mm;
frame_ht := 210mm;

frame_wd -= 20mm;
frame_ht -= 20mm;


path frame;

frame :=  (-.5frame_wd, -.5frame_ht) -- (.5frame_wd, -.5frame_ht) -- (.5frame_wd, .5frame_ht)
         -- (-.5frame_wd, .5frame_ht) -- cycle;

path q[];
point p[];
point M[];
point temp_pt;
string s;
picture v[];
transform t[];
circle c[];
rectangle r[];
bool_point_vector bpv;


focus f;
set f with_position (0, 5, -10) with_direction (0, 5, 10) with_distance 20;

fig_ctr := 0;

%% * (1) Fig.

beginfig(fig_ctr);
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  cone C;

  set C with_type circular with_type single_napped with_radius 5 with_axis_y 8
    with_point_count 32 with_divisions 4;

  % message "C:";
  % pause;
  % show C;
  % pause;
  
  p0 := get_base_center C;

  c0 := get_base_circle C;

  q0 := get_path 0 C;

  p1 := get_point 0 q0;
  p2 := get_apex C;

  p3 := p2 shifted (-5, 0);  % Line through the apex, one intersection point
  p4 := p2 shifted (5, 0);

  p7 := p1 shifted (0, 0, -6); % Line through p1, tangent to cone at base
  p8 := p1 shifted (0, 0, 6);

  p11 := p7 shifted (2, 0); % Line doesn't pass through the cone
  p12 := p8 shifted (2, 0);

  p13 := mediate(p1, p2);
  p14 := p7 shifted by (p13 - p1);
  p15 := p8 shifted by (p13 - p1);
  
  
  t0 := identity;

  if true: % false
    rotate t0 (5, 5, 5);
    shift t0 (1, 2, 3);
  fi; 

  C *= c0 *= q0 *= t0;

  forsuffixes i = 0, 1, 2, 3, 4, 7, 8, 11, 12, 13, 14, 15:
    p[i] *= t0;
  endfor; 
  
%% ** (2)
  

  draw C;
  draw c0 with_color Salmon_rgb with_pen big_pen;
  draw q0 with_color blue with_pen big_pen;
  draw p0 -- p1;

  dotlabel.lft("$p_0$", p0);
  dotlabel.rt("$p_1$", p1);
  dotlabel.top("$p_2$", p2);

%% ** (2) Line through the apex, one intersection point

  if false: % true
    
    draw p3 -- p4; % Line through the apex, one intersection point

    dotlabel.lft("$p_3$", p3);
    dotlabel.rt("$p_4$", p4);
    
    clear bpv;
    bpv := (p3 -- p4) intersection_points C;

    message "size bpv:" & decimal (size bpv);

    message "bpv:";
    show bpv;

    if (size bpv) > 0:
      p5 := bpv0;
      dotlabel.llft("$p_5$", p5);
    fi; 

    if (size bpv) > 1:
      p6 := bpv1;
      dotlabel.lrt("$p_6$", p6);
    fi; 

  fi; 
  
%% ** (2) Tangent line through point on base

  if false: % true

    draw p7 -- p8; % Line through the apex, one intersection point

    dotlabel.bot("$p_7$", p7);
    dotlabel.bot("$p_8$", p8);
    
    
    clear bpv;
    bpv := (p7 -- p8) intersection_points C with_tolerance .001;

    message "size bpv:" & decimal (size bpv);

    message "bpv:";
    show bpv;

    if (size bpv) > 0:
      p9 := bpv0;
      dotlabel.llft("$p_9$", p9);
    fi; 

    if (size bpv) > 1:
      p10 := bpv1;
      dotlabel.lrt("$p_{10}$", p10);
    fi; 

  fi;

%% ** (2) Line doesn't pass through cone

  if false: % true

    draw p11 -- p12;

    dotlabel.bot("$p_{11}$", p11);
    dotlabel.bot("$p_{12}$", p12);

    
    clear bpv;
    bpv := (p11 -- p12) intersection_points C with_tolerance .001;
    message "size bpv:" & decimal (size bpv); % 0
    
    message "bpv:";
    show bpv;
  fi; 
  
%% ** (2) Tangent line through point on mantle

% This fails.  Points are found that don't lie on the line.
% !! TODO:  Try to debug this.
% LDF 2024.12.02.
  
  draw p14 -- p15;

  dotlabel.lrt("$p_{13}$", p13);
  dotlabel.bot("$p_{14}$", p14);
  dotlabel.bot("$p_{15}$", p15);

  clear bpv;
  bpv := (p14 -- p15) intersection_points C with_all_points with_tolerance .001;
  message "size bpv:" & decimal (size bpv); % 0
  
  message "bpv:";
  show bpv;
  pause;

  if (size bpv) > 0:
    for i = 0 upto ((size bpv) - 1):
      M[i] := bpv[i];
      dotlabel.top(decimal i, M[i]) with_color red;
    endfor;
  fi;
  

%% ** (2)

  v0 := current_picture;

  shift current_picture (0, 0, 10);
  
endfig with_focus f;
fig_ctr += 1;

%% * (1) Fig.  Parallel projection, x-z plane

beginfig(fig_ctr);
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := v0;

endfig with_projection parallel_x_z;
fig_ctr += 1;

%% * (1)

message "fig_ctr - 1: " & decimal (fig_ctr - 1);
%pause;

bye;


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

