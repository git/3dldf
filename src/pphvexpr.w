@q pphvexpr.w @> 
@q Created by Laurence Finston Fri Dec 10 16:27:40 CET 2004 @>
     
@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) |path_vector| expressions.@>
@** {\bf path\_vector} expressions.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Created this file and wrote quite a few rules.  
\ENDLOG 

@q * (1) |path_vector| primary.  @>
@* \�path vector primary>.
\initials{LDF 2004.12.10.}
  
\LOG
\initials{LDF 2004.12.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> path_vector_primary@>@/

@q ** (2) path_vector_primary --> path_vector_variable.@>
@*1 \�path vector primary> $\longrightarrow$ 
\�path vector variable>.  
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=path_vector_primary: path_vector_variable@>@/ 
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `path_vector_primary --> "
                << "path_vector_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q **** (4) Error handling:  |entry == 0 || entry->object == 0|.@> 

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.12.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In `yyparse()', rule " 
                << endl 
                << "Rule `path_vector_primary --> "
                << "path_vector_variable':"
                << endl << "`entry' == 0 or `entry->object' == 0."
                << endl 
                << "Setting `path_vector_primary' "
                << "to 0 and will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

@q **** (4) |!(entry == 0 || entry->object == 0)|.@> 

@ |!(entry == 0 || entry->object == 0)|.
\initials{LDF 2004.12.10.}

@<Define rules@>=
  else /* |!(entry == 0 || entry->object == 0)|  */

     {

        typedef Pointer_Vector<Path> PV;

        PV* pv = new PV;

        *pv = *static_cast<PV*>(entry->object);

        @=$$@> = static_cast<void*>(pv);                    

     }  /* |else| (|!(entry == 0 || entry->object == 0)|)  */

};

@q ** (2) path_vector_primary --> LEFT_PARENTHESIS  @>
@q ** (2) path_vector_expression  RIGHT_PARENTHESIS.@>

@*1 \�path vector primary> $\longrightarrow$ \.{LEFT\_PARENTHESIS}
\�path vector expression> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: LEFT_PARENTHESIS path_vector_expression RIGHT_PARENTHESIS@>@/ 
{
   
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `path_vector_primary "
                << "--> LEFT_PARENTHESIS path_vector_expression "
                << "RIGHT_PARENTHESIS'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) path_vector_primary --> DEVELOP numeric_expression COMMA @>
@q        numeric_expression COMMA numeric_expression              @>

@*2 \�path vector primary> $\longrightarrow$ 
\.{DEVELOP} $\ldots$.

\�path vector primary> $\longrightarrow$ \.{DEVELOP}
 \�numeric expression> \.{COMMA} 
\�numeric expression> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2009.11.03.}

\LOG
\initials{LDF 2009.11.03.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=path_vector_primary: DEVELOP numeric_expression COMMA @>
@=numeric_expression COMMA numeric_expression@>
{ 

@q develop(real rad = 1, @>
@q         unsigned int segments = 8, @>
@q         real increment = 5.0; @>
@q         Transform* t = 0, @>
@q         Scanner_Node scanner_node = 0); @>

     @=$$@> = Sphere::develop(@=$2@>, @=$4@>, @=$6@>, 0,  
                              static_cast<Scanner_Node>(parameter));

};

@q ** (2) path_vector_primary --> GET_PATHS FROM glyph_expression @>

@*2 \�path vector primary> $\longrightarrow$ \.{GET\_PATHS} \.{FROM} \�glyph expression>.
\initials{LDF 2022.01.18.}

\LOG
\initials{LDF 2022.01.18.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=path_vector_primary: GET_PATHS FROM glyph_expression@>
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

   DEBUG = false; /* |true|  */

   if (DEBUG)
   {
      cerr_strm << thread_name 
                << "*** Parser: `path_vector_primary: GET_PATHS FROM glyph_expression'."
                << endl;
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

@q ******* (7) @>

   Glyph *g = static_cast<Glyph*>(@=$3@>);

#if 0 
   g->show("*g:");
#endif 

   typedef Pointer_Vector<Path> PV;

   PV* pv = new PV;

   for (vector<Path*>::iterator iter = g->path_vector.v.begin();
        iter != g->path_vector.v.end();
        ++iter)
   {
      *pv += create_new<Path>(*iter, scanner_node);
   }

@q ******* (7) @>

   delete g;
   g = 0;

   @=$$@> = static_cast<void*>(pv);

};

@q *** (3) path_vector_primary: RESOLVE path_expression LEFT_PARENTHESIS numeric_expression COMMA   @>
@q         numeric_expression RIGHT_PARENTHESIS TO numeric_expression                               @>
@q         resolve_option_list                                                                      @>

@ \�path vector primary> $\longrightarrow$ \.{RESOLVE} \�path expression> \.{LEFT\_PARENTHESIS} 
\�numeric expression> \.{COMMA} \�numeric expression> \.{RIGHT\_PARENTHESIS} 
\.{TO} \�numeric expression> \�resolve option list>.
\initials{LDF 2022.01.18.}

\LOG
\initials{LDF 2022.01.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: RESOLVE path_expression LEFT_PARENTHESIS numeric_expression COMMA @>
@=numeric_expression RIGHT_PARENTHESIS TO numeric_expression resolve_option_list         @>@/
{
@q **** (4) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
    cerr_strm << "*** Parser: `path_vector_primary --> RESOLVE path_expression"
              << endl 
              << "LEFT_PARENTHESIS numeric_expression COMMA numeric_expression"
              << endl 
              << "RIGHT_PARENTHESIS TO numeric_expression resolve_option_list'.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

  Pointer_Vector<Path> *pv = create_new<Pointer_Vector<Path> >(0);

  Path *q = static_cast<Path*>(@=$2@>);

  bool save             = @=$10@> &  1U; 
  bool with_transform   = @=$10@> &  2U; 
  bool with_ampersand   = @=$10@> &  4U; 
  bool test_planar      = @=$10@> &  8U; 
  bool make_planar      = @=$10@> & 16U; 

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "In parser (resolve rule):  save == " << save << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  if (q == 0)
  {
     cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> RESOLVE path_expression"
               << endl 
               << "TO numeric_expression resolve_option_list:"
               << endl 
               << "`path_expression' is NULL.  Can't resolve."
               << endl 
               << "Returning empty `path_vector' and continuing."
               << endl;

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     delete pv;
     pv = 0;

     goto END_RESOLVE_RULE_0;

  }

@q **** (4) @>

#if DEBUG_COMPILE
  if (DEBUG)
  { 
     cerr << "resolve_option_list == " << @=$10@> << " == 0x" << hex << @=$10@> 
          << dec << endl
          << "save == " << save << endl
          << "with_transform == " << with_transform << endl
          << "with_ampersand == " << with_ampersand << endl
          << "test_planar == " << test_planar << endl
          << "make_planar == " << make_planar << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

  status = static_cast<Path*>(@=$2@>)->resolve(
                              pv,
                              scanner_node,
                              static_cast<int>(@=$9@>), 
                              static_cast<int>(@=$4@>), 
                              static_cast<int>(@=$6@>), 
                              save,
                              with_transform,
                              test_planar,
                              make_planar,
                              with_ampersand);
  if (status != 0)
  {
     cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> RESOLVE path_expression "
               << "LEFT_PARENTHESIS numeric_expression COMMA"
               << endl 
               << "numeric_expression RIGHT_PARENTHESIS "
               << "TO numeric_expression resolve_option_list:"
               << endl 
               << "`Path::resolve' failed, returning " << status << "."
               << endl 
               << "Failed to resolve path.  Returning empty `path_vector'."
               << endl;

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  } 

@q **** (4) @>

#if DEBUG_COMPILE
  else if (DEBUG)
  {
    cerr_strm << "In parser, rule `path_vector_primary --> RESOLVE path_expression "
              << "LEFT_PARENTHESIS numeric_expression COMMA"
              << endl 
              << "numeric_expression RIGHT_PARENTHESIS "
              << "TO numeric_expression resolve_option_list:"
              << endl 
              << "`Path::resolve' succeeded, returning 0.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");
    
  }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

END_RESOLVE_RULE_0:

  @=$$@> =  static_cast<void*>(pv);

};

@q *** (3) path_vector_primary: RESOLVE path_expression TO numeric_expression resolve_option_list @>

@ \�path vector primary> $\longrightarrow$ \.{RESOLVE} \�path expression> \.{TO} 
\�numeric expression> \�resolve option list>.
\initials{LDF 2022.04.22.}

\LOG
\initials{LDF 2022.04.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: RESOLVE path_expression TO numeric_expression resolve_option_list @>@/
{
@q **** (4) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
    cerr_strm << "*** Parser: `path_vector_primary --> RESOLVE path_expression "
              << "TO numeric_expression resolve_option_list'.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

  Pointer_Vector<Path> *pv = create_new<Pointer_Vector<Path> >(0);

  Path *q = static_cast<Path*>(@=$2@>);

  bool save             = @=$5@> &  1U; 
  bool with_transform   = @=$5@> &  2U; 
  bool with_ampersand   = @=$5@> &  4U; 
  bool test_planar      = @=$5@> &  8U; 
  bool make_planar      = @=$5@> & 16U; 

@q **** (4) @>

  if (q == 0)
  {
     cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> RESOLVE path_expression"
               << endl 
               << "TO numeric_expression resolve_option_list:"
               << endl 
               << "`path_expression' is NULL.  Can't resolve."
               << endl 
               << "Returning empty `path_vector' and continuing."
               << endl;

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     delete pv;
     pv = 0;

     goto END_RESOLVE_RULE_1;
  }

@q **** (4) @>

#if DEBUG_COMPILE
  if (DEBUG)
  { 
     cerr << "resolve_option_list == " << @=$5@> << " == 0x" << hex << @=$5@> 
          << dec << endl
          << "save == " << save << endl
          << "with_transform == " << with_transform << endl
          << "with_ampersand == " << with_ampersand << endl
          << "test_planar == " << test_planar << endl
          << "make_planar == " << make_planar << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

  status = q->resolve(pv,
                      scanner_node,
                      static_cast<int>(@=$4@>), 
                      0, 
                      -1,
                      save,
                      with_transform,
                      test_planar,
                      make_planar,
                      with_ampersand);
  if (status != 0)
  {
     cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> RESOLVE path_expression"
               << endl 
               << "TO numeric_expression resolve_option_list:"
               << endl 
               << "`Path::resolve' failed, returning " << status << "."
               << endl 
               << "Failed to resolve path.  Returning empty `path_vector'."
               << endl;

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  } 

@q **** (4) @>

#if DEBUG_COMPILE
  else if (DEBUG)
  {
    cerr_strm << "In parser, rule `path_vector_primary --> RESOLVE path_expression"
              << endl 
              << "TO numeric_expression resolve_option_list:"
              << endl 
              << "`Path::resolve' succeeded, returning 0.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");
    
  }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

END_RESOLVE_RULE_1:

  @=$$@> =  static_cast<void*>(pv);

};

@q *** (3) resolve_option_list @>
@
\LOG
\initials{LDF 2022.04.19.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <uint_value> resolve_option_list@>

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: /* Empty  */@>
{
   @=$$@> = (2U | 8U | 16U);
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list SAVE@>
{

   @=$$@> = @=$1@> |= 1U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list NO_SAVE@>
{

   @=$$@> = @=$1@> &= ~1U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list WITH_TRANSFORM@>
{

   @=$$@> = @=$1@> |= 2U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list WITH_NO_TRANSFORM@>
{

   @=$$@> = @=$1@> &= ~2U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list WITH_AMPERSAND@>
{

   @=$$@> = @=$1@> |= 4U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list WITH_NO_AMPERSAND@>
{

   @=$$@> = @=$1@> &= ~4U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list TEST_PLANAR@>
{

   @=$$@> = @=$1@> |= 8U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list NO_TEST_PLANAR@>
{

   @=$$@> = @=$1@> &= ~8U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list MAKE_PLANAR@>
{

   @=$$@> = @=$1@> |= 16U;
};

@q **** (4) @>
@
@<Define rules@>= 
@=resolve_option_list: resolve_option_list NO_MAKE_PLANAR@>
{

   @=$$@> = @=$1@> &= ~16U;
};

@q ** (3) path_vector_primary: GET_TANGENTS ... @>

@ \�path vector primary> $\longrightarrow$ \.{GET\_TANGENTS} \�path expression>
\�numeric list optional> \�with angles optional> \�with perpendiculars optional>
\�save optional> \�with test optional>.
\initials{LDF 2023.04.18.}

\LOG
\initials{LDF 2023.04.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_TANGENTS path_expression numeric_list_optional with_angles_optional@>@/
@=with_perpendiculars_optional with_scale_value_optional save_optional with_test_optional@>@/
{
@q *** (3) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
    cerr_strm << "*** Parser: `path_vector_primary --> GET_TANGENTS path_expression"
              << endl 
              << "numeric_list_optional with_angles_optional with_perpendiculars_optional"
              << endl 
              << "with_scale_value_optional save_optional with_test_optional'.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");

    cerr << "with_scale_value_optional == $6 == " << @=$6@> << endl
         << "save_optional             == $7 == " << @=$7@> << endl
         << "with_test_optional        == $8 == " << @=$8@> << endl;

    if (@=$8@> == NULL_VALUE)
       cerr << "with_test_optional == NULL_VALUE." << endl;
    else if (@=$8@> == WITH_TEST)
       cerr << "with_test_optional == WITH_TEST." << endl;
    else if (@=$8@> == WITH_NO_TEST)
       cerr << "with_test_optional == WITH_NO_TEST." << endl;
    else 
       cerr << "with_test_optional has invalid value." << endl;
  }
#endif /* |DEBUG_COMPILE|  */@;

  bool save = @=$7@>;

  bool do_test = (@=$8@> == WITH_TEST) ? true : false;

  real scale_val = @=$6@>; 

@q *** (3) @>

  Pointer_Vector<Path> *pv = create_new<Pointer_Vector<Path> >(0);

  Path *q = static_cast<Path*>(@=$2@>);

  Id_Map_Entry_Node angles_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);  
  Id_Map_Entry_Node perp_entry   = static_cast<Id_Map_Entry_Node>(@=$5@>);  

  status = q->get_tangents_and_perpendiculars_func(scanner_node,
                                                   pv,                
                                                   false,
                                                   static_cast<Pointer_Vector<real>*>(@=$3@>), 
                                                   scale_val,
                                                   angles_entry,
                                                   perp_entry,  
                                                   save,
                                                   do_test);          

@q *** (3) @>

  if (status != 0)
  {
      cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> GET_TANGENTS path_expression"
                << endl 
                << "numeric_list_optional with_angles_optional with_perpendiculars_optional"
                << endl 
                << "save_optional with_test_optional':"
                << endl 
                << "`Path::get_tangents_and_perpendiculars_func' failed, returning " << status << "." << endl
                << "Failed to find tangents for path.  Will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

  }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, rule `path_vector_primary --> GET_TANGENTS path_expression"
                << endl 
                << "numeric_list_optional with_angles_optional with_perpendiculars_optional"
                << endl 
                << "save_optional with_test_optional':"
                << endl 
                << "`Path::get_tangents_and_perpendiculars_func' succeeded, returning 0.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (perp_entry)
   {
#if 0
      cerr << "perp_entry is non-NULL." << endl;
      perp_entry->show("*perp_entry:");
#endif 

   }
   else
      cerr << "perp_entry is NULL." << endl;

   @=$$@> = static_cast<void*>(pv);

@q *** (3) @>

};

@q ** (3) path_vector_primary: GET_PERPENDICULARS ... @>

@ \�path vector primary> $\longrightarrow$ \.{GET\_PERPENDICULARS} \�path expression>
\�numeric list optional> \�with angles optional> \�with tangents optional>
\�save optional> \�with test optional>.
\initials{LDF 2023.04.18.}

\LOG
\initials{LDF 2023.04.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_PERPENDICULARS path_expression numeric_list_optional with_angles_optional@>@/
@=with_tangents_optional with_scale_value_optional save_optional with_test_optional@>@/
{
@q *** (3) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
    cerr_strm << "*** Parser: `path_vector_primary --> GET_PERPENDICULARS path_expression"
              << endl 
              << "numeric_list_optional with_angles_optional with_tangents_optional"
              << endl 
              << "with_scale_value_optional save_optional with_test_optional'.";

    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");

    cerr << "with_scale_value_optional == $6 == " << @=$6@> << endl
         << "save_optional             == $7 == " << @=$7@> << endl
         << "with_test_optional        == $8 == " << @=$8@> << endl;

    if (@=$8@> == NULL_VALUE)
       cerr << "with_test_optional == NULL_VALUE." << endl;
    else if (@=$8@> == WITH_TEST)
       cerr << "with_test_optional == WITH_TEST." << endl;
    else if (@=$8@> == WITH_NO_TEST)
       cerr << "with_test_optional == WITH_NO_TEST." << endl;
    else 
       cerr << "with_test_optional has invalid value." << endl;
  }
#endif /* |DEBUG_COMPILE|  */@;

  bool save = @=$7@>;

  bool do_test = (@=$8@> == WITH_TEST) ? true : false;

  real scale_val = @=$6@>; 

@q *** (3) @>

  Pointer_Vector<Path> *pv = create_new<Pointer_Vector<Path> >(0);

  Path *q = static_cast<Path*>(@=$2@>);

  Id_Map_Entry_Node angles_entry   = static_cast<Id_Map_Entry_Node>(@=$4@>);  
  Id_Map_Entry_Node tangents_entry = static_cast<Id_Map_Entry_Node>(@=$5@>);  

  status = q->get_tangents_and_perpendiculars_func(scanner_node,
                                                   pv,                
                                                   true,
                                                   static_cast<Pointer_Vector<real>*>(@=$3@>), 
                                                   scale_val,
                                                   angles_entry,
                                                   tangents_entry,  
                                                   save,
                                                   do_test);          

@q *** (3) @>

  if (status != 0)
  {
      cerr_strm << "ERROR!  In parser, rule `path_vector_primary --> GET_PERPENDICULARS path_expression"
                << endl 
                << "numeric_list_optional with_angles_optional with_tangents_optional"
                << endl 
                << "save_optional with_test_optional':"
                << endl 
                << "`Path::get_tangents_and_perpendiculars_func' failed, returning " << status << "." << endl
                << "Failed to find perpendiculars for path.  Will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

  }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, rule `path_vector_primary --> GET_PERPENDICULARS path_expression"
                << endl 
                << "numeric_list_optional with_angles_optional with_tangents_optional"
                << endl 
                << "save_optional with_test_optional':"
                << endl 
                << "`Path::get_tangents_and_perpendiculars_func' succeeded, returning 0.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (tangents_entry)
   {
      cerr << "tangents_entry is non-NULL." << endl;

#if 0
      tangents_entry->show("*tangents_entry:");
#endif 

   }
   else
      cerr << "tangents_entry is NULL." << endl;

   @=$$@> = static_cast<void*>(pv);

@q *** (3) @>

   Pointer_Vector<Path> *tv = static_cast<Pointer_Vector<Path>*>(tangents_entry->object);

   /* for (int i = 0; i < pv->v.size(); ++i) */
   /* { */
   /*     pv->v[i]->rotate(0, 0, -90); */
   /*     tv->v[i]->rotate(0, 0, 90); */
   /* } */

@q *** (3) @>

};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_angles_optional@>@/
@=%type <pointer_value> with_perpendiculars_optional@>@/
@=%type <pointer_value> with_tangents_optional@>@/
@=%type <real_value> with_scale_value_optional@>@/

@q *** (3) @>
@
@<Define rules@>=

@q *** (3) @>
@
@<Define rules@>=
@=with_angles_optional: /* Empty */@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_angles_optional: /* Empty */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) @>
@
@<Define rules@>=
@=with_angles_optional: WITH_ANGLES numeric_vector_variable clear_option@>@/
{
@q **** (4) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_angles_optional: WITH_ANGLES numeric_vector_variable clear_option'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

    unsigned int clear_val = @=$3@>;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    if (entry)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "entry->name == " << entry->name << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@; 

    }

@q **** (4) @>

    else
    {
        cerr << "WARNING:  `numeric_vector_variable' is NULL." << endl;

        @=$$@> = static_cast<void*>(0); 

        goto END_WITH_ANGLES_OPTIONAL_RULE;
    }

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "clear_val == " << clear_val << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (clear_val)
   {
      status = Scan_Parse::clear_vector_func(scanner_node, entry); 

      cerr << "Scan_Parse::clear_vector_func returned status == " << status << endl;
   }

@q **** (4) @>

   @=$$@> = @=$2@>;

@q **** (4) @>

END_WITH_ANGLES_OPTIONAL_RULE:
;

};

@q *** (3) @>
@
@<Define rules@>=

@q *** (3) @>
@
@<Define rules@>=
@=with_perpendiculars_optional: /* Empty */@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_perpendiculars_optional: /* Empty */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) @>
@
@<Define rules@>=
@=with_perpendiculars_optional: WITH_PERPENDICULARS path_vector_variable clear_option@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_perpendiculars_optional: WITH_PERPENDICULARS "
                << "path_vector_variable clear_option'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

    unsigned int clear_val = @=$3@>;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    if (entry)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "entry->name == " << entry->name << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@; 

    }

@q **** (4) @>

    else
    {
        cerr << "WARNING:  `path_vector_variable' is NULL." << endl;

        @=$$@> = static_cast<void*>(0); 

        goto END_WITH_PERPENDICULARS_OPTIONAL_RULE;
    }

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "clear_val == " << clear_val << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (clear_val)
   {
      status = Scan_Parse::clear_vector_func(scanner_node, entry); 

      cerr << "Scan_Parse::clear_vector_func returned status == " << status << endl;
   }

@q **** (4) @>

   @=$$@> = @=$2@>;

@q **** (4) @>

END_WITH_PERPENDICULARS_OPTIONAL_RULE:
;

};

@q *** (3) @>
@
@<Define rules@>=

@q *** (3) @>
@
@<Define rules@>=
@=with_tangents_optional: /* Empty */@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_tangents_optional: /* Empty */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) @>
@
@<Define rules@>=
@=with_tangents_optional: WITH_TANGENTS path_vector_variable clear_option@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_tangents_optional: WITH_TANGENTS "
                << "path_vector_variable clear_option'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

    unsigned int clear_val = @=$3@>;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    if (entry)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "entry->name == " << entry->name << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@; 

    }

@q **** (4) @>

    else
    {
        cerr << "WARNING:  `path_vector_variable' is NULL." << endl;

        @=$$@> = static_cast<void*>(0); 

        goto END_WITH_TANGENTS_OPTIONAL_RULE;
    }

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "clear_val == " << clear_val << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (clear_val)
   {
      status = Scan_Parse::clear_vector_func(scanner_node, entry); 

      cerr << "Scan_Parse::clear_vector_func returned status == " << status << endl;
   }

@q **** (4) @>

   @=$$@> = @=$2@>;

@q **** (4) @>

END_WITH_TANGENTS_OPTIONAL_RULE:
;

};

@q *** (3) @>
@
@<Define rules@>=

@q *** (3) @>
@
@<Define rules@>=
@=with_scale_value_optional: /* Empty */@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_scale_value_optional: /* Empty */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 1.0;

};

@q *** (3) @>
@
@<Define rules@>=
@=with_scale_value_optional: WITH_SCALE_VALUE numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `with_scale_value_optional: WITH_SCALE_VALUE numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = @=$2@>;

};

@q ** (2) path_vector_primary --> GET_TABS DODECAHEDRON.@>

@*1 \�path vector primary> $\longrightarrow$ \.{GET\_TABS}
\.{DODECAHEDRON}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=path_vector_primary: GET_TABS DODECAHEDRON with_tabs_option_list@>@/
{
 
    @=$$@> = Dodecahedron::get_tabs(parameter);

};

@*1 \�path vector primary> $\longrightarrow$ \.{GET\_TABS} \.{RHOMBIC\_TRIACONTAHEDRON}.
\initials{LDF 2007.10.24.}

\LOG
\initials{LDF 2007.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=path_vector_primary: GET_TABS RHOMBIC_TRIACONTAHEDRON with_tabs_option_list@>@/
{
 
    @=$$@> = Rhombic_Triacontahedron::get_tabs(parameter);

};

@q ** (2) with_tabs_option_list.@>
@*1 \�with tabs option list>.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this type declaration.  It is currently commented-out, since
the symbol \�with tabs option list> doesn't require a semantic value.
\ENDLOG

@q <Type declarations for non-terminal symbols@>
@q =%type <pointer_value> with_tabs_option_list@>

@q *** (3) with_tabs_option_list --> /* Empty  */@>
@*2 \�with tabs option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: /* Empty  */@>@/
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->polyhedron_options)
      scanner_node->polyhedron_options->clear();
   else
      scanner_node->polyhedron_options = new Polyhedron_Options;
     
};

@q *** (3) with_tabs_option_list: with_tabs_option_list @>
@q *** (3) WITH_DIAMETER numeric_expression.            @>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_DIAMETER} \�numeric expression>.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list @>
@=WITH_DIAMETER numeric_expression@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->polygon_0_diameter 
    = @=$3@>;

};

@q *** (3) with_tabs_option_list: with_tabs_option_list @>
@q *** (3) WITH_RHOMBUS_SIDE_LENGTH numeric_expression. @>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_RHOMBUS\_SIDE\_LENGTH} 
\�numeric expression>.
\initials{LDF 2007.10.24.}

\LOG
\initials{LDF 2007.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list @>
@=WITH_RHOMBUS_SIDE_LENGTH numeric_expression@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->rhombus_0_side_length 
    = @=$3@>;

};

@q *** (3) with_tabs_option_list: with_tabs_option_list WITH_HALF.@>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_HALF}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list WITH_HALF@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->do_half = true;

};

@q *** (3) with_tabs_option_list: with_tabs_option_list WITH_STITCH_LINES.@>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_STITCH\_LINES}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list WITH_STITCH_LINES@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->do_stitch_lines = true;

};

@q *** (3) with_tabs_option_list: with_tabs_option_list WITH_NO_STITCH_LINES.@>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_NO\_STITCH\_LINES}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list WITH_NO_STITCH_LINES@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->do_stitch_lines = false;

};

@q *** (3) with_tabs_option_list: with_tabs_option_list @>
@q *** (3) WITH_STITCH_DIVISIONS numeric_expression.    @>
@*2 \�with tabs option list> $\longrightarrow$ 
\�with tabs option list> \.{WITH\_STITCH\_DIVISIONS}.
\initials{LDF 2007.10.12.}

\LOG
\initials{LDF 2007.10.12.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_tabs_option_list: with_tabs_option_list WITH_STITCH_DIVISIONS @>
@=numeric_expression@>@/
{
    static_cast<Scanner_Node>(
       parameter)->polyhedron_options->stitch_divisions 
    = (@=$3@> <= 0) ? 0 
                    : static_cast<unsigned short>(
                         floor(fabs(@=$3@>))); 
};

@q *** (3) path_vector_primary: point_primary NEUSIS point_secondary COMMA point_secondary @>
@q *** (3) WITH_DISTANCE numeric_expression                                                    @>

@ \�path vector primary> $\longrightarrow$ \�point primary> \.{NEUSIS} 
\�point secondary> \.{COMMA} \�point secondary> \.{WITH\_DISTANCE} 
\�numeric expression> \.{WITH\_PATHS}.
\initials{LDF 2022.12.20.}

\LOG
\initials{LDF 2022.12.20.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=path_vector_primary: point_primary NEUSIS point_secondary @>
@=COMMA point_secondary WITH_DISTANCE numeric_expression WITH_PATHS@>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @;
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `path_vector_primary: NEUSIS point_secondary COMMA point_secondary"
                << endl 
                << "WITH_DISTANCE numeric_expression WITH_PATHS'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Point *p0 = static_cast<Point*>(@=$1@>);
    Point *p1 = static_cast<Point*>(@=$3@>);
    Point *p2 = static_cast<Point*>(@=$5@>);

    Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

    status = p0->neusis(p1, p2, @=$7@>, 0, pv);
 
    if (status != 0)
    {
       cerr_strm << "ERROR!  In parser, rule `path_vector_primary: NEUSIS point_secondary "
                 << endl 
                 << "COMMA point_secondary WITH_DISTANCE numeric_expression WITH_PATHS':" << endl
                 << "`Point::neusis' failed, returning " << status << "." << endl 
                 << "Failed to find `points' using the neusis construction." << endl 
                 << "Setting value of the `point_primary' (on the left-hand side) "
                 << "to `INVALID_POINT' and continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }

    delete p0;
    p0 = 0;     

    delete p1;
    p1 = 0;     

    delete p2;
    p2 = 0;     

    @=$$@> = static_cast<void*>(pv);
};

@q * (1) @>
@
@<Define rules@>=
@=path_vector_primary: GET_ENCLOSING_PATHS sinewave_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `path_vector_primary:  "
                 << "GET_ENCLOSING_PATHS sinewave_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Sinewave *s = static_cast<Sinewave*>(@=$2@>);

    Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

    status = s->get_enclosing_paths(qv, 0, scanner_node);

    cerr << "status == " << status << endl;

    cerr << "qv->v.size() == " << qv->v.size() << endl;
    cerr << "qv->ctr == " << qv->ctr << endl;

    delete s;
    s = 0;
 
    @=$$@> = static_cast<void*>(qv);

};

@q * (1) @>
@
@<Define rules@>=
@=path_vector_primary: GET_EDGES polyhedron_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `path_vector_primary:  GET_EDGES "
                 << "polyhedron_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

    Polyhedron *p = static_cast<Polyhedron*>(@=$2@>);

    status = p->get_edges(pv, scanner_node);

#if DEBUG_COMPILE
    if (DEBUG)
    { 
       cerr << "status == " << status << endl;
    }  
#endif /* |DEBUG_COMPILE|  */@; 

    delete p;

    p = 0;     

    @=$$@> = static_cast<void*>(pv); 

};

@q * (1) @>
@
@<Define rules@>=
@=path_vector_primary: GET_EDGES cuboid_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `path_vector_primary:  GET_EDGES "
                 << "cuboid_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

    Cuboid *c = static_cast<Cuboid*>(@=$2@>);

    status = c->get_edges(pv, scanner_node);

#if DEBUG_COMPILE
    if (DEBUG)
    { 
       cerr << "status == " << status << endl;
    }  
#endif /* |DEBUG_COMPILE|  */@; 

    delete c;

    c = 0;     

    @=$$@> = static_cast<void*>(pv); 

};

@q * (1) path_vector_primary: GET_FACE_AXES numeric_primary polyhedron_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_FACE\_AXES} \�numeric primary>
\�polyhedron expression>.
\initials{LDF 2023.12.31.}

\LOG
\initials{LDF 2023.12.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_FACE_AXES numeric_primary polyhedron_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_FACE_AXES numeric_primary polyhedron_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Polyhedron *p = static_cast<Polyhedron*>(@=$3@>);

   if (p)
   {
#if 0 
      cerr << "p is non-NULL." << endl;

      cerr << "p->shape_type == " << p->shape_type << " == " 
           << Shape::type_name_map[p->shape_type] 
           << endl;
#endif 

      if (p->shape_type = Shape::PENTAGONAL_HEXECONTAHEDRON_TYPE)
      {
#if 0  
         r = Pentagonal_Hexecontahedron::face_axes;
#else

         Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

         /* !! START HERE  2023.12.31. */

#endif 

         @=$$@> = static_cast<void*>(qv);

      }
      else
      {
     
         cerr << "In parser, rule `path_vector_primary: "
              << "GET_FACE_AXES numeric_primary polyhedron_expression':"
              << endl 
              << "This case hasn't been programmed.  Setting $$ to NULL and continuing." 
              << endl;

         @=$$@> = static_cast<void*>(0);
      }
   }

   else
   {
      cerr << "In parser, rule `path_vector_primary:"
           << "GET_FACE_AXES numeric_primary polyhedron_expression':"
            << endl 
           << "`Polyhedron *p' is NULL.  Setting $$ to NULL and continuing." 
           << endl;

      @=$$@> = static_cast<void*>(0);
   }

   delete p;
   p = 0;

};

@q * (1) path_vector_primary: GET_AXES cuboid_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�cuboid expression>.
\initials{LDF 2024.02.20.}

\LOG
\initials{LDF 2024.02.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES cuboid_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES cuboid_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Cuboid *c = static_cast<Cuboid*>(@=$2@>);

   Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

   status = c->get_axes(qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES cuboid_expression':"
               << endl 
               << "`Cuboid::get_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of cuboid.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES cuboid_expression':"
               << endl 
               << "`Cuboid::get_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   delete c;
   c = 0;

   @=$$@> = static_cast<void*>(qv);

};

@q * (1) path_vector_primary: GET_AXES sphere_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�sphere expression>.
\initials{LDF 2024.12.08.}

\LOG
\initials{LDF 2024.12.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES sphere_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES sphere_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Sphere *s = static_cast<Sphere*>(@=$2@>);

   vector<Path> qv;

   status = s->Ellipsoid::get_main_axes(&qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES sphere_expression':"
               << endl 
               << "`Sphere::get_main_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of sphere.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

@q ** (2) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES sphere_expression':"
               << endl 
               << "`Sphere::get_main_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ** (2) @>

   Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

   for (vector<Path>::iterator iter = qv.begin();
        iter != qv.end();
        ++iter)
   {
       *pv += *iter;
   }

@q ** (2) @>

   delete s;
   s = 0;

   @=$$@> = static_cast<void*>(pv);

};

@q * (1) path_vector_primary: GET_AXES ellipsoid_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�ellipsoid expression>.
\initials{LDF 2024.12.08.}

\LOG
\initials{LDF 2024.12.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES ellipsoid_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES ellipsoid_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Ellipsoid *e = static_cast<Ellipsoid*>(@=$2@>);

   vector<Path> qv;

   status = e->get_main_axes(&qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES ellipsoid_expression':"
               << endl 
               << "`Ellipsoid::get_main_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of ellipsoid.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

@q ** (2) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES ellipsoid_expression':"
               << endl 
               << "`Ellipsoid::get_main_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ** (2) @>

   Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

   for (vector<Path>::iterator iter = qv.begin();
        iter != qv.end();
        ++iter)
   {
       *pv += *iter;
   }

@q ** (2) @>

   delete e;
   e = 0;

   @=$$@> = static_cast<void*>(pv);

};

@q * (1) path_vector_primary: GET_AXES polyhedron_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�polyhedron expression>.
\initials{LDF 2024.02.20.}

\LOG
\initials{LDF 2024.02.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES polyhedron_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES polyhedron_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Polyhedron *p = static_cast<Polyhedron*>(@=$2@>);

   Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

#if 0 

   /* get_axes no longer exists.  use get_main_axes.  LDF 2024.12.16.  */
   status = p->get_axes(qv, scanner_node);

#endif 


   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES polyhedron_expression':"
               << endl 
               << "`Polyhedron::get_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of polyhedron.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES polyhedron_expression':"
               << endl 
               << "`Polyhedron::get_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   delete p;
   p = 0;

   @=$$@> = static_cast<void*>(qv);

};

@q * (1) path_vector_primary: GET_AXES ellipse_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�ellipse expression>.
\initials{LDF 2024.12.13.}

\LOG
\initials{LDF 2024.12.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES ellipse_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES ellipse_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Ellipse *e = static_cast<Ellipse*>(@=$2@>);

   Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

   status = e->get_axes(qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES ellipse_expression':"
               << endl 
               << "`Ellipse::get_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of ellipse.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES ellipse_expression':"
               << endl 
               << "`Ellipse::get_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   delete e;
   e = 0;

   @=$$@> = static_cast<void*>(qv);

};

@q * (1) path_vector_primary: GET_AXES circle_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�circle expression>.
\initials{LDF 2024.12.13.}

\LOG
\initials{LDF 2024.12.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES circle_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES circle_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Circle *c = static_cast<Circle*>(@=$2@>);

   Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

   status = c->Ellipse::get_axes(qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES circle_expression':"
               << endl 
               << "`Ellipse::get_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of circle.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES circle_expression':"
               << endl 
               << "`Ellipse::get_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   delete c;
   c = 0;

   @=$$@> = static_cast<void*>(qv);

};

@q * (1) path_vector_primary: GET_AXES rectangle_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_AXES} \�rectangle expression>.
\initials{LDF 2024.12.29.}

\LOG
\initials{LDF 2024.12.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_AXES rectangle_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_AXES rectangle_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Rectangle *r = static_cast<Rectangle*>(@=$2@>);

   Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

   status = r->get_axes(qv, scanner_node);

   if (status != 0)
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `path_vector_primary: "
               << "GET_AXES rectangle_expression':"
               << endl 
               << "`Rectangle::get_axes' failed, returning " << status << "." 
               << endl 
               << "Failed to get axes of rectangle.  Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << thread_name << "In parser, rule `path_vector_primary: "
               << "GET_AXES rectangle_expression':"
               << endl 
               << "`Rectangle::get_axes' succeeded, returning 0.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   delete r;
   r = 0;

   @=$$@> = static_cast<void*>(qv);

};




@q * (1) path_vector_primary: GET_SPHERICAL_TRIANGLE triangle_expression with_radius_optional @>
@q       with_sphere_variable_optional with_center_optional COMMA triangle_vector_variable @>
@q       with_test_optional@>

@ \�numeric primary> $\longrightarrow$ \.{GET\_SPHERICAL\_TRIANGLE} \�triangle expression>
\�with radius optional> \�with sphere variable optional> \�with center optional> \.{COMMA} 
\�triangle vector variable>
\�with test optional>.
\initials{LDF 2024.10.28.}

If a \�sphere variable> and a radius are both specified, the radius of the \�sphere variable>
takes precedence.
\initials{LDF 2024.10.28.}

\LOG
\initials{LDF 2024.10.28.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_SPHERICAL_TRIANGLE triangle_expression with_radius_optional @>
@=with_sphere_variable_optional with_center_optional COMMA triangle_vector_variable with_test_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_SPHERICAL_TRIANGLE triangle_expression with_radius_optional"
               << endl
               << "with_sphere_variable_optional COMMA with_center_optional "
               << "triangle_vector_variable with_test_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Triangle *t = static_cast<Triangle*>(@=$2@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$7@>);

   Point *center = static_cast<Point*>(@=$5@>);

   Pointer_Vector<Triangle> *tv = 0;

   real radius = @=$3@>;

   Id_Map_Entry_Node sphere_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (sphere_entry && sphere_entry->object)
   {
      radius = static_cast<Sphere*>(sphere_entry->object)->get_radius();
   }

   if (radius <= 0)
   {
     cerr << "ERROR!  In parser, rule `path_vector_primary: "
          << "GET_SPHERICAL_TRIANGLE triangle_expression" << endl 
          << "with_radius_optional with_sphere_variable_optional with_center_optional COMMA "
          << "triangle_vector_variable with_test_optional':" << endl
          << "`real radius' == " << radius << " (<= 0)"
          << endl            
          << "Can't get spherical triangle.  Will try to continue." << endl;

      @=$$@> = static_cast<void*>(0);
   }   

@q ** (2) @>

   else
   {

      Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

      if (entry && entry->object)
         tv = static_cast<Pointer_Vector<Triangle>*>(entry->object);

      bool do_test = (@=$8@> == WITH_TEST);

      status = Sphere::get_spherical_triangle(radius, t, qv, center, tv, do_test, scanner_node);

@q *** (3) @>

      if (status != 0)
      {
        cerr << "ERROR!  In parser, rule `path_vector_primary: "
             << "GET_SPHERICAL_TRIANGLE triangle_expression" << endl 
             << "with_radius_optional with_sphere_variable_optional with_center_optional COMMA "
             << "triangle_vector_variable with_test_optional':" << endl
             << "`Sphere::get_spherical_triangle' failed, returning " << status << "." 
             << endl            
             << "Failed to get spherical triangle.  Will try to continue." << endl;
      }

@q *** (3) @>

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
        cerr << "In parser, rule `path_vector_primary: "
             << "GET_SPHERICAL_TRIANGLE triangle_expression" << endl 
             << "with_radius_optional with_sphere_variable_optional "
             << "with_center_optional COMMA triangle_vector_variable with_test_optional':" 
             << endl
             << "`Sphere::get_spherical_triangle' succeeded, returning 0."
             << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

      @=$$@> = static_cast<void*>(qv);

   }  /* |else|  */

@q ** (2) @>

   delete t;
   t = 0;

   delete center;  /* |center| should never be NULL.  LDF 2024.10.28.  */
   center = 0;

};

@q * (1) path_vector_primary: GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list @>
@q RIGHT_PARENTHESIS with_radius_optional with_sphere_variable_optional @>
@q with_center_optional COMMA triangle_vector_variable @>

@ 
\initials{LDF 2024.10.28.}

\LOG
\initials{LDF 2024.10.28.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_primary: GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list @>
@= RIGHT_PARENTHESIS with_radius_optional @>
@=with_sphere_variable_optional with_center_optional COMMA @>
@=triangle_vector_variable with_test_optional @>
{
@q ** (2) @>

@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `path_vector_primary: "
               << "GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list RIGHT_PARENTHESIS"
               << "with_radius_optional"
               << endl
               << "with_sphere_variable_optional with_center_optional COMMA triangle_vector_"
               << "variable with_test_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$9@>);

   Point *center = static_cast<Point*>(@=$7@>);

   Pointer_Vector<Triangle> *tv = 0;

   real radius = @=$5@>;

   Id_Map_Entry_Node sphere_entry = static_cast<Id_Map_Entry_Node>(@=$6@>);

   Triangle *t = create_new<Triangle>(0);

   Pointer_Vector<Point> *pv = static_cast<Pointer_Vector<Point>*>(@=$3@>);

   if (pv && pv->v.size() < 3)
   {
      cerr << "ERROR!  In parser, rule `path_vector_primary: "
           << "GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list" << endl 
           << "RIGHT_PARENTHESIS with_radius_optional with_sphere_variable_optional with_center_optional "
           << "COMMA triangle_vector_variable with_test_optional':" << endl;

      if (pv) 
      {
         cerr  << "`point_expression_list' contains fewer than 3 points:"
           << "`pv->v.size()' == " << pv->v.size() << endl;

         delete pv; 
         pv = 0;
      }

      else
         cerr  << "`point_expression_list' is NULL." << endl;
   
      cerr << "Can't get spherical triangle.  Will try to continue." << endl;

       @=$$@> = static_cast<void*>(0);

       goto END_GET_SPHERICAL_TRIANGLE_RULE;

   }
   else 
   {
      Point *p0 = create_new<Point>(0);
      Point *p1 = create_new<Point>(0);
      Point *p2 = create_new<Point>(0);;

      *p0 = *(pv->v[0]);
      *p1 = *(pv->v[1]);        
      *p2 = *(pv->v[2]);

      t->set(p0, p1, p2);

      delete pv; 
      pv = 0;

   }

@q ** (2) @>

   if (sphere_entry && sphere_entry->object)
   {
      radius = static_cast<Sphere*>(sphere_entry->object)->get_radius();
   }

   if (radius <= 0)
   {
     cerr << "ERROR!  In parser, rule `path_vector_primary: "
          << "GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list" << endl 
          << "RIGHT_PARENTHESIS with_radius_optional with_sphere_variable_optional with_center_optional "
          << "COMMA "
          << "triangle_vector_variable with_test_optional':" << endl
          << "`real radius' == " << radius << " (<= 0)"
          << endl            
          << "Can't get spherical triangle.  Will try to continue." << endl;

      @=$$@> = static_cast<void*>(0);
   }   

@q ** (2) @>

   else
   {

      Pointer_Vector<Path> *qv = new Pointer_Vector<Path>;

      if (entry && entry->object)
         tv = static_cast<Pointer_Vector<Triangle>*>(entry->object);

      bool do_test = (@=$10@> == WITH_TEST);

      status = Sphere::get_spherical_triangle(radius, t, qv, center, tv, do_test, scanner_node);

@q *** (3) @>

      if (status != 0)
      {
        cerr << "ERROR!  In parser, rule `path_vector_primary: "
             << "GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list" << endl 
             << "RIGHT_PARENTHESIS with_radius_optional with_sphere_variable_optional "
             << "with_center_optional COMMA "
             << "triangle_vector_variable with_test_optional':" << endl
             << "`Sphere::get_spherical_triangle' failed, returning " << status << "." 
             << endl            
             << "Failed to get spherical triangle.  Will try to continue." << endl;
      }

@q *** (3) @>

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
        cerr << "In parser, rule `path_vector_primary: "
             << "GET_SPHERICAL_TRIANGLE LEFT_PARENTHESIS point_expression_list" << endl 
             << "RIGHT_PARENTHESIS with_radius_optional with_sphere_variable_optional "
             << "with_center_optional COMMA triangle_vector_variable with_test_optional':" 
             << endl
             << "`Sphere::get_spherical_triangle' succeeded, returning 0."
             << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

      @=$$@> = static_cast<void*>(qv);

   }  /* |else|  */

@q ** (2) @>

END_GET_SPHERICAL_TRIANGLE_RULE:

   delete t;
   t = 0;
 
   delete center;  /* |center| should never be NULL.  LDF 2024.10.28.  */
   center = 0;

@q *** (3) @>

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_sphere_variable_optional@>@/

@q ** (2) @>
@
@<Define rules@>=
@=with_sphere_variable_optional: /* Empty */@>
{
   @=$$@> = static_cast<void*>(0);
};

@q ** (2) @>
@
@<Define rules@>=
@=with_sphere_variable_optional: sphere_variable@>
{
   @=$$@> = @=$1@>;
};

@q ** (2) @>

@ \�path vector primary> $\longrightarrow$ \.{GET\_BISECTORS} \�triangle expression> \�with test optional>.
\initials{LDF 2024.11.13.}

\LOG
\initials{LDF 2024.11.13.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=path_vector_primary: GET_BISECTORS triangle_expression with_test_optional@>@/
{
@q *** (3) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: path_vector_primary: GET_BISECTORS "
                << "triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    Pointer_Vector<Path> *pv = new Pointer_Vector<Path>;

    status = t->get_bisectors(0, pv, do_test, scanner_node);

@q *** (3) @>

    if (status != 0)
    {
      cerr << "ERROR!  In parser, rule `path_vector_primary: GET_BISECTORS "
           << "triangle_expression with_test_optional':"
           << endl 
           << "`Triangle::get_bisectors' failed, returning " << status << "." 
           << endl 
           << "Failed to get bisectors.  Will try to continue." << endl;
    }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `path_vector_primary: GET_BISECTORS "
           << "triangle_expression with_test_optional':"
           << endl 
           << "`Triangle::get_bisectors' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

    delete t;
    t = 0;

    @=$$@> = static_cast<void*>(pv);

};



@q * (1) path_vector secondary.@>

@* \�path vector secondary>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> path_vector_secondary@>
  
@q ** (2) path_vector secondary --> path_vector_primary.@>
@*1 \�path vector secondary> $\longrightarrow$ 
\�path vector primary>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.

\initials{LDF 2007.06.19.}
Moved the |@<Common declarations for rules@>| section inside the conditional 
debugging code.
\ENDLOG

@<Define rules@>=
@=path_vector_secondary: path_vector_primary@>@/ 
{

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {

       cerr_strm << thread_name 
                 << "*** Parser:  `path_vector_secondary "
                 << "--> path_vector_primary'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;
  
  @=$$@> = @=$1@>;

};

@q *** (3) path_vector_secondary --> path_secondary REFLECTED_OFF @>
@q *** (3) path_expression WITH_DIRECTION point_expression.       @>

@*2 \�path vector secondary> $\longrightarrow$ \�path secondary> 
\.{REFLECTED\_OFF} \�path expression>
\.{WITH\_DIRECTION} \�point expression>.

\LOG
\initials{LDF 2004.12.10.}
Added this rule.  It is not yet functional, because I haven't written 
|Path::reflect_off| yet.

\initials{LDF 2004.12.10.}
Added ``\.{WITH\_DIRECTION} \�point expression>''.

\initials{LDF 2004.12.10.}
Working on |Path::reflect_off|.

\initials{LDF 2004.12.10.}
Changed \�path secondary> on the left-hand side to 
\�path vector secondary>.  Moved this rule from 
\filename{ppthexpr.w} to this file (\filename{pphvexpr.w}).

\initials{LDF 2005.10.24.}
Changed |path_like_expression| to |path_like_expression|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=path_vector_secondary: path_secondary REFLECTED_OFF @>
@=path_expression WITH_DIRECTION point_expression@>@/ 
{

    Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

    typedef Pointer_Vector<Path> PV;

    PV* pv = new PV;

    *pv += create_new<Path>(0, scanner_node);
    *pv += create_new<Path>(0, scanner_node);
    *pv += create_new<Path>(0, scanner_node);
    *pv += create_new<Path>(0, scanner_node);

@q ******* (7) Call |Scan_Parse::reflect_off_func<Path>|.@> 

@ Call |Scan_Parse::reflect_off_func<Path>|.
\initials{LDF 2004.12.10.}

@<Define rules@>=

    int status = reflect_off_func<Path>(scanner_node,
                                        static_cast<Path*>(@=$1@>), 
                                        static_cast<Path*>(@=$3@>), 
                                        static_cast<Point*>(@=$5@>), 
                                        pv);          

@q ******** (8) Error handling:  |status != 0|.         @> 
@q ******** (8) |Scan_Parse::reflect_off_func| failed.@> 

@ Error handling:  |status != 0|.  
|Scan_Parse::reflect_off_func| failed.
\initials{LDF 2004.12.10.}

@<Define rules@>=                        
  
  if (status != 0)
     {
       @=$$@> = static_cast<void*>(0);

     } /* |if (status != 0)|  */
      
@q ******** (8) |Scan_Parse::reflect_off_func| succeeded.@> 

@ |Scan_Parse::reflect_off_func| succeeded.
\initials{LDF 2004.10.05.}

\LOG
\initials{LDF 2004.12.13.}
@:BUG FIX@> BUG FIX:  
Now setting |@=$$@>| to |static_cast<void*>(pv)| rather than
|static_cast<void*>(pv->v[0])|.
\ENDLOG 

@<Define rules@>=                        

  else /* |status == 0|  */
     {

          @=$$@> = static_cast<void*>(pv);

     }  /* |else| (|status == 0|)  */

@q ******* (7).@> 

};

@q ** (2) path_vector_secondary --> path_secondary @> 
@q ** (2) DECOMPOSE point_primary                  @>  

@*1 \�path vector secondary> $\longrightarrow$ 
\�path secondary> \.{DECOMPOSE} 
\�point primary>.
\initials{LDF 2007.06.19.}

\LOG
\initials{LDF 2007.06.19.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=path_vector_secondary: path_secondary DECOMPOSE@>@/
@=point_primary@>@/ 
{

       @<Decompose |Path| using a |Point|@>@;

};

@q ** (2) path_vector_secondary --> path_secondary @> 
@q ** (2) OVER point_primary                       @>  

@*1 \�path vector secondary> $\longrightarrow$ 
\�path secondary> \.{OVER} 
\�point primary>.
\initials{LDF 2007.06.19.}

\LOG
\initials{LDF 2007.06.19.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=path_vector_secondary: path_secondary OVER@>@/
@=point_primary@>@/ 
{

       @<Decompose |Path| using a |Point|@>@;

};

@q ** (2) Decompose |Path| using a |Point|.@> 
@ Decompose {\bf Path} using a {\bf Point}.
\initials{LDF 2007.06.19.}

This section is used in the rules
\�path vector secondary> $\longrightarrow$ 
\�path secondary> \.{DECOMPOSE} 
\�point primary>
and 
\�path vector secondary> $\longrightarrow$ 
\�path secondary> \.{OVER} 
\�point primary>

\LOG
\initials{LDF 2007.06.19.}
Added this section.
\ENDLOG

@<Decompose |Path| using a |Point|@>=

    Pointer_Vector<Path>* pv = new Pointer_Vector<Path>;

    int status = decompose_func(static_cast<Scanner_Node>(parameter),
                                @=$1@>,
                                PATH,
                                @=$3@>,
                                POINT,
                                0,
                                NULL_VALUE,
                                static_cast<void*>(pv),
                                PATH_VECTOR);

   if (status != 0)
   {
      
       @=$$@> = static_cast<void*>(0);

   }  /* |if (status != 0)|  */

else /* |status != 0|  */
   {
 
      @=$$@> = static_cast<void*>(pv);

   }  /* |else| (|status != 0|)  */

@q * (1) path_vector tertiary.  @>

@* \�path vector tertiary>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> path_vector_tertiary@>

@q ***** (5) path_vector tertiary --> path_vector_secondary.@>
@ \�path vector tertiary> $\longrightarrow$ 
\�path vector secondary>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_tertiary: path_vector_secondary@>@/ 
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser: `path_vector_tertiary "
                 << "--> path_vector_secondary'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q * (1) path_vector expression.@>
@* \�path vector expression>.

\LOG
\initials{LDF 2004.12.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> path_vector_expression@>

@q ** (2) path_vector expression --> path_vector_tertiary.  @>
@*1 \�path vector expression> $\longrightarrow$ 
\�path vector tertiary>.

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=path_vector_expression: path_vector_tertiary@>@/ 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser: `path_vector_expression "
                 << "--> path_vector_tertiary'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:70                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

