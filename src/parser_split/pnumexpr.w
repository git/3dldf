@q pnumexpr.w @> 
@q Created by Laurence Finston Thu Jan 29 19:09:21 MET 2004  @>

@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) Numeric expressions.  @>
@** Numeric expressions.
\initials{LDF 2004.10.01.}

\LOG
\initials{LDF 2004.04.29.}  
Created this file.  It contains code formerly contained in
\filename{parser.w} and is included in that file by means of CWEB's
``\.{@@i}'' command. 
\ENDLOG 

@q **** (4) numeric_atom.  @>
@*3 \�numeric atom>.
\initials{LDF 2004.10.01.}

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_atom@>@/

@q ***** (5) numeric_atom --> numeric_variable.@>
@*4 \�numeric atom> $\longrightarrow$ \�numeric variable>. 
\initials{LDF 2004.10.01.}

\LOG
\initials{LDF 2004.05.03.}  
Changed |variable| to |numeric_variable|.
The way it was before caused reduce/reduce conflicts when I tried to 
use |variable| in the rule for |point_primary|.

\initials{LDF 2004.05.03.}
@:BUG FIX@> BUG FIX: 
Changed this rule to account for the fact that
|@=$1@>| is now an |Id_Map_Entry_Node| rather that a |real*| (after
being cast from |void*|).

\initials{LDF 2004.05.03.}
Added error handling code for the case that |entry == 0|.

\initials{LDF 2004.10.13.}
@:BUG FIX@> BUG FIX:  Now testing whether |entry->object == 0| as well
as whether |entry == 0|.

\initials{LDF 2004.11.21.}
Commented-out the error message formerly issued in the case that
|entry == 0 || entry->object == 0|.  This condition occurs legitimately 
when one tries to show an ``unknown |numeric|''.
\ENDLOG 

@q ****** (6) Definition.  @>

@<Define rules@>= 

@=numeric_atom: numeric_variable@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_token_atom --> numeric_variable'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (scanner_node->variable_entry != 0)
          cerr << "scanner_node->variable_entry is non-NULL." << endl;
       else
          cerr << "scanner_node->variable_entry is NULL." << endl;

  }
#endif /* |DEBUG_COMPILE|  */

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

#if DEBUG_COMPILE
  if (DEBUG)
  { 
     if (entry)
        entry->show("*entry:");
     else
        cerr << "`entry' is NULL." << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = ZERO_REAL;

    }  /* |if (entry == 0 || entry->object == 0)| */

  else /* |entry != 0| */
 
      @=$$@> = *static_cast<real*>(entry->object);
 
};

@q ***** (5) numeric_argument.  @>
@*4 \�numeric argument>.
\initials{LDF 2004.10.01.}

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
Add this case.  \initials{LDF 2004.04.28.}
\ENDTODO 

@q ***** (5) numeric_token_atom.  @>
@*4 \�numeric token atom>.
\initials{LDF 2004.10.01.}

@<Define rules@>= 
@=numeric_atom: numeric_token_atom@>
{

  @=$$@> = @=$1@>;

};

@q ***** (5) NORMALDEVIATE.  @>
@*4 \.{NORMALDEVIATE}.
\initials{LDF 2004.10.01.}

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this case.  
\ENDTODO 

@q ***** (5) numeric_single.  @>

@*4 \�numeric single>.
\initials{LDF 2004.10.01.}

\LOG
\initials{LDF 2004.04.30.}
Changed this rule.  It now uses |numeric_single| rather than
`\.{\LP}' \�numeric expression> `\.{\RP}' explicitly.
\ENDLOG 

@<Define rules@>= 
@=numeric_atom: numeric_single@>
{

  @=$$@> = @=$1@>;

};

@q **** (4) Numeric token atom.  @>
@*3 \�numeric token atom>.

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_token_atom@>@/

@q ***** (5) numeric_token> / numeric_token.  @>

@*4 \�numeric token> / \�numeric token>.  @>

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
\initials{LDF 2004.04.24.}
!! TO DO:  Add error handling code for division by 0.  
Print out context. 
\ENDTODO  

@<Define rules@>= 
@=numeric_token_atom: numeric_token OVER numeric_token@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      {
        cerr_strm << thread_name 
                  << "*** Parser: `numeric_token_atom --> "
                  << "numeric_token OVER numeric_token'.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */

  if (@=$3@> == ZERO_REAL)
    {
#if 0 
@q       unsigned int first_line   = @@=@@1@@>.first_line;   @>
@q       unsigned int first_column = @@=@@1@@>.first_column; @>
@q       unsigned int last_line    = @@=@@3@@>.last_line;    @>
@q       unsigned int last_column  = @@=@@3@@>.last_column;  @>
#endif 

      cerr_strm << "ERROR! ";

#if 0 
      if (scanner_node->in->type == Io_Struct::FILE_TYPE)
        cerr_strm << "In input file `" << scanner_node->in->filename << "' "
                  << first_line << "." << first_column << "--" << last_line
                  << "." << last_column << "." << endl;
#endif 

      cerr_strm << "Division by 0.  Setting <numeric token atom> to 0."
                << "Will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      @=$$@> = ZERO_REAL;

    } /* |if (@=$3@> == 0)|  */
  
  else
    @=$$@> = @=$1@> / @=$3@>;

#if DEBUG_COMPILE
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "In `yyparse()', rule "
                << "`numeric_token_atom --> "
                << "numeric_token OVER numeric_token':"
                << endl 
                << "`$$' == " << @=$$@> << ".";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */ 

};

@q ***** (5) numeric_token not followed by "/ numeric_token".@>
@*4 \�numeric token> not followed by ``$/$ \�<numeric token>''. 
\initials{LDF 2004.10.01.}

@<Define rules@>= 
@=numeric_token_atom: numeric_token@>
{
  @=$$@> = @=$1@>;

};

@q **** (4) Numeric primary.  @>
@*3 \�numeric primary>.
\initials{LDF 2004.10.01.}

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_primary@>@/

@q ***** (5) numeric_primary --> numeric_atom.  @>
@*4 \�numeric primary> $\longrightarrow$ \�numeric atom>.

@<Define rules@>= 
@=numeric_primary: numeric_atom@>
{
  @=$$@> = @=$1@>;

};

@q ***** (5) numeric_atom [ numeric_expression, numeric_expression ].  @>

@*4 \�numeric atom> \.{LEFT\_BRACKET} 
\�numeric expression> \.{COMMA} \.{RIGHT\_BRACKET}.
\initials{LDF 2004.04.27.}  

\LOG
\initials{LDF 2004.04.27.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_primary: numeric_atom LEFT_BRACKET numeric_expression COMMA@>@/
@=numeric_expression RIGHT_BRACKET@>
{

  @=$$@> = @=$3@> + @=$1@> * (@=$5@> - @=$3@>);

};

@*4 \�numeric primary> $\longrightarrow$ \.{MAGNITUDE} \�numeric primary>.
\initials{LDF 2007.06.20.}

\LOG
\initials{LDF 2007.06.20.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_primary: MAGNITUDE numeric_primary@>
{

  @=$$@> = fabs(@=$2@>);

};

@q ***** (5) numeric_primary LENGTH numeric_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} 
\�numeric primary>.

This rule is trivial.  The length of a \�numeric primary> is simply it's 
value.  This doesn't seem to be documented in {\it The METAFONTbook}.
\initials{LDF 2004.04.27.}

\LOG
\initials{LDF 2004.04.27.}  
Added this rule.

\initials{LDF 2004.10.06.}
No longer including 
|@<Common code for |LENGTH| AND |MAGNITUDE|@>| 
in the action for this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_primary: LENGTH numeric_primary@>
{

  @=$$@> = @=$2@>;

};

@q ***** (5) numeric_primary --> LENGTH point_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} 
\�point primary>.

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_primary: LENGTH point_primary@>
{

  Point* p = static_cast<Point*>(@=$2@>);

  if (p == static_cast<Point*>(0))
    {

      @=$$@> = ZERO_REAL;
      
    } /* |if (p == 0)|  */

  else /* |p != 0|  */
    {
      @=$$@> = p->magnitude();
      delete p;

    } /* |else| (|p != 0|)  */

};

@q ***** (5) numeric_primary --> MAGNITUDE point_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{MAGNITUDE} 
\�point primary>.

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.

\initials{LDF 2004.10.06.}
Added \�optional of> to this rule.

\initials{LDF 2004.10.07.}
Removed \�optional of> from this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_primary: MAGNITUDE point_primary@>
{

  Point* p = static_cast<Point*>(@=$2@>);

  if (p == static_cast<Point*>(0))
    {
      @=$$@> = ZERO_REAL;
      
    } /* |if (p == 0)|  */

  else /* |p != 0|  */
    {
      @=$$@> = p->magnitude();
      delete p;

    } /* |else| (|p != 0|)  */
};

@q ***** (5) numeric_primary --> LENGTH path_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�path primary>.

@<Define rules@>=
@=numeric_primary: LENGTH path_primary@>@/
{
   Path* p = static_cast<Path*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE path_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�path primary>.

@<Define rules@>=
@=numeric_primary: SIZE path_primary@>@/
{
   Path* p = static_cast<Path*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> LENGTH ellipse_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�ellipse primary>.

@<Define rules@>=
@=numeric_primary: LENGTH ellipse_primary@>@/
{
   Ellipse* e = static_cast<Ellipse*>(@=$2@>);

   if (e)
     @=$$@> = e->size();
   else
     @=$$@> = ZERO_REAL;

   delete e;

};

@q ***** (5) numeric_primary --> SIZE ellipse_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�ellipse primary>.

@<Define rules@>=
@=numeric_primary: SIZE ellipse_primary@>@/
{
   Ellipse* e = static_cast<Ellipse*>(@=$2@>);

   if (e)
     @=$$@> = e->size();
   else
     @=$$@> = ZERO_REAL;

   delete e;

};

@q ***** (5) numeric_primary --> LENGTH superellipse_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�superellipse primary>.
\initials{LDF 2022.04.27.}

\LOG
Added this rule.
\ENDLOG 

@<Define rules@>=
@=numeric_primary: LENGTH superellipse_primary@>@/
{
   Superellipse* e = static_cast<Superellipse*>(@=$2@>);

   if (e)
     @=$$@> = e->size();
   else
     @=$$@> = ZERO_REAL;

   delete e;

};

@q ***** (5) numeric_primary --> SIZE superellipse_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�superellipse primary>.
\initials{LDF 2022.04.27.}

\LOG
Added this rule.
\ENDLOG 

@<Define rules@>=
@=numeric_primary: SIZE superellipse_primary@>@/
{
   Superellipse* s = static_cast<Superellipse*>(@=$2@>);

   if (s)
     @=$$@> = s->size();
   else
     @=$$@> = ZERO_REAL;

   delete s;

};

@q ***** (5) numeric_primary --> LENGTH circle_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�circle primary>.
\initials{LDF 2007.11.08.}

\LOG
\initials{LDF 2007.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH circle_primary@>@/
{
   Circle* c = static_cast<Circle*>(@=$2@>);

   if (c)
     @=$$@> = c->size();
   else
     @=$$@> = ZERO_REAL;

   delete c;

};

@q ***** (5) numeric_primary --> ARC_LENGTH numeric_primary circle_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{ARC\_LENGTH} \�numeric primary> 
\�circle primary>.
\initials{LDF 2021.6.28.}

\LOG
\initials{LDF 2021.6.28.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: ARC_LENGTH numeric_primary circle_primary@>@/
{
   Circle* c  = static_cast<Circle*>(@=$3@>);

#if LDF_REAL_FLOAT
   @=$$@> = c->get_arc_length(fabsf(@=$2@>));
#elif LDF_REAL_DOUBLE
   @=$$@> = c->get_arc_length(fabs(@=$2@>));
#else /* Default.  */
   @=$$@> = c->get_arc_length(fabsf(@=$2@>));   
#endif

   delete c;
   c = 0;

};

@q ***** (5) numeric_primary --> ARC_LENGTH numeric_primary WITH_RADIUS numeric_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{ARC\_LENGTH} \�numeric primary> 
\.{WITH\_RADIUS} \�numeric primary>.
\initials{LDF 2021.07.03.}

\LOG
\initials{LDF 2021.07.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: ARC_LENGTH numeric_primary WITH_RADIUS numeric_primary@>@/
{
   Point p;
   p.set(0, 0, 0);

   Circle c;
   c.set(p, 2 * @=$4@>);

#if LDF_REAL_FLOAT
   @=$$@> = c.get_arc_length(fabsf(@=$2@>));
#elif LDF_REAL_DOUBLE
   @=$$@> = c.get_arc_length(fabs(@=$2@>));
#else /* Default.  */
   @=$$@> = c.get_arc_length(fabsf(@=$2@>));   
#endif

};

@q ***** (5) numeric_primary --> RIGHT_ASCENSION_DECIMAL_DEGREES star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{RIGHT\_ASCENSION\_DECIMAL\_DEGREES} \�star primary>.
\initials{LDF 2021.6.29.}

\LOG
\initials{LDF 2021.6.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: RIGHT_ASCENSION_DECIMAL_DEGREES star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->right_ascension_decimal_degrees;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> RIGHT_ASCENSION_HOURS star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{RIGHT\_ASCENSION\_HOURS} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: RIGHT_ASCENSION_HOURS star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->right_ascension_hours;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> RIGHT_ASCENSION_MINUTES star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{RIGHT\_ASCENSION\_MINUTES} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: RIGHT_ASCENSION_MINUTES star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->right_ascension_minutes;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> RIGHT_ASCENSION_SECONDS star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{RIGHT\_ASCENSION\_SECONDS} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: RIGHT_ASCENSION_SECONDS star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->right_ascension_seconds;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> DECLINATION_DECIMAL_DEGREES star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{DECLINATION\_DECIMAL\_DEGREES} \�star primary>.
\initials{LDF 2021.6.29.}

\LOG
\initials{LDF 2021.6.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: DECLINATION_DECIMAL_DEGREES star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->declination_decimal_degrees;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> DECLINATION_DEGREES star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{DECLINATION\_DEGREES} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: DECLINATION_DEGREES star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->declination_degrees;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> DECLINATION_MINUTES star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{DECLINATION\_MINUTES} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: DECLINATION_MINUTES star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->declination_minutes;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> DECLINATION_SECONDS star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{DECLINATION\_SECONDS} \�star primary>.
\initials{LDF 2021.09.05.}

\LOG
\initials{LDF 2021.09.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: DECLINATION_SECONDS star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->declination_seconds;

   delete s;
   s = 0; 

};

@q ***** (5) numeric_primary --> APPARENT_MAGNITUDE star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{APPARENT\_MAGNITUDE} \�star primary>.
\initials{LDF 2021.07.10.}

\LOG
\initials{LDF 2021.07.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: APPARENT_MAGNITUDE star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

   @=$$@> = s->apparent_magnitude;

   delete s;
   s = 0; 
};

@q ***** (5) numeric_primary --> ABSOLUTE_MAGNITUDE star_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{ABSOLUTE\_MAGNITUDE} \�star primary>.
\initials{LDF 2021.09.06.}

\LOG
\initials{LDF 2021.09.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: ABSOLUTE_MAGNITUDE star_primary@>@/
{
   Star* s = static_cast<Star*>(@=$2@>);

#if 0
   s->show("s:");
#endif 

   @=$$@> = s->absolute_magnitude;

   delete s;
   s = 0; 
};

@q ***** (5) numeric_primary --> SIZE circle_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�circle primary>.
\initials{LDF 2007.11.08.}

\LOG
\initials{LDF 2007.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE circle_primary@>@/
{
   Circle* c = static_cast<Circle*>(@=$2@>);

   if (c)
     @=$$@> = c->size();
   else
     @=$$@> = ZERO_REAL;

   delete c;
   c = 0;

};

@q ***** (5) numeric_primary --> LENGTH parabola_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�parabola primary>.
\initials{LDF 2005.11.09.}

\LOG
\initials{LDF 2005.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH parabola_primary@>@/
{
   Parabola* p = static_cast<Parabola*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE parabola_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�parabola primary>.
\initials{LDF 2005.11.09.}

\LOG
\initials{LDF 2005.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE parabola_primary@>@/
{
   Parabola* p = static_cast<Parabola*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE hyperbola_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�hyperbola primary>.
\initials{LDF 2007.07.03.}

\LOG
\initials{LDF 2007.07.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE hyperbola_primary@>@/
{
   Hyperbola* p = static_cast<Hyperbola*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> LENGTH polygon_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�polygon primary>.
\initials{LDF 2005.12.11.}

\LOG
\initials{LDF 2005.12.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH polygon_primary@>@/
{
   Polygon* p = static_cast<Polygon*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE polygon_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�polygon primary>.
\initials{LDF 2005.12.11.}

\LOG
\initials{LDF 2005.12.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE polygon_primary@>@/
{
   Polygon* p = static_cast<Polygon*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> LENGTH reg_polygon_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�regular polygon primary>.
\initials{2009.01.15.}

\LOG
\initials{2009.01.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH reg_polygon_primary@>@/
{
   Reg_Polygon* p = static_cast<Reg_Polygon*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE reg_polygon_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�regular polygon primary>.
\initials{2009.01.15.}

\LOG
\initials{2009.01.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE reg_polygon_primary@>@/
{
   Reg_Polygon* p = static_cast<Reg_Polygon*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> SIZE ellipsoid_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�ellipsoid primary>.

@<Define rules@>=
@=numeric_primary: SIZE ellipsoid_primary@>@/
{
   Ellipsoid* e = static_cast<Ellipsoid*>(@=$2@>);

   if (e)
     @=$$@> = e->size();
   else
     @=$$@> = ZERO_REAL;

   delete e;

};

@q ***** (5) numeric_primary --> SIZE sphere_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�sphere primary>.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.  It currently contains a dummy action.

\initials{LDF 2009.09.09.}
Now calling |Scan_Parse::sphere_size_func|.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE sphere_primary@>@/
{

   @=$$@> = sphere_size_func(@=$2@>, parameter);

};

@q ***** (5) numeric_primary --> SIZE paraboloid_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�paraboloid primary>.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.  It currently contains a dummy action.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE paraboloid_primary@>@/
{
   Paraboloid* p = static_cast<Paraboloid*>(@=$2@>);

   delete p;

   @=$$@> = ZERO_REAL;

};

@q ***** (5) numeric_primary --> SIZE polyhedron_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�polyhedron primary>.
\initials{LDF 2005.12.20.}

\LOG
\initials{LDF 2005.12.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE polyhedron_primary@>@/
{
   Polyhedron* p = static_cast<Polyhedron*>(@=$2@>);

   if (p)
     @=$$@> = p->size();
   else
     @=$$@> = ZERO_REAL;

   delete p;

};

@q ***** (5) numeric_primary --> LENGTH string_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�string primary>.
\initials{LDF 2021.07.009.}

@<Define rules@>=
@=numeric_primary: LENGTH string_primary@>@/
{
   string* s = static_cast<string*>(@=$2@>);

   if (s)
     @=$$@> = s->length();
   else
     @=$$@> = ZERO_REAL;

   delete s;

};

@q ***** (5) numeric_primary --> SIZE string_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�string primary>.
\initials{LDF 2021.07.009.}

@<Define rules@>=
@=numeric_primary: SIZE string_primary@>@/
{
   string* s = static_cast<string*>(@=$2@>);

   if (s)
     @=$$@> = s->size();
   else
     @=$$@> = ZERO_REAL;

   delete s;

};

@q ***** (5) numeric_primary --> ASCII string_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{ASCII} \�string primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this rule.  
\ENDTODO 

@q ***** (5) numeric_primary --> OCT string_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{OCT} \�string primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this rule.  
\ENDTODO 

@q ***** (5) numeric_primary --> HEX string_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{HEX} \�string primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this rule.  
\ENDTODO 

@q ***** (5) numeric_primary --> SIZE boolean_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�numeric vector primary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE boolean_vector_primary@>@/
{

   Pointer_Vector<bool>* bv 
         = static_cast<Pointer_Vector<bool>*>(@=$2@>); 

   @=$$@> = bv->ctr;

   delete bv;

};

@q ***** (5) numeric_primary --> SIZE numeric_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�numeric vector primary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE numeric_vector_primary@>@/
{

   Pointer_Vector<real>* nv 
         = static_cast<Pointer_Vector<real>*>(@=$2@>); 

   @=$$@> = nv->ctr;

   delete nv;

};

@q ***** (5) numeric_primary --> SIZE ulong_long_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�ulong long vector primary>.
\initials{LDF 2005.12.07.}

\LOG
\initials{LDF 2005.12.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE ulong_long_vector_primary@>@/
{

   Pointer_Vector<ulong_long>* nv 
         = static_cast<Pointer_Vector<ulong_long>*>(@=$2@>); 

   @=$$@> = nv->ctr;

   delete nv;

};

@q ***** (5) numeric_primary --> SIZE string_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�string vector primary>.
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE string_vector_primary@>@/
{

   Pointer_Vector<string>* sv 
         = static_cast<Pointer_Vector<string>*>(@=$2@>); 

   @=$$@> = sv->ctr;

   delete sv;

};

@q ***** (5) numeric_primary --> SIZE pen_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�pen vector primary>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE pen_vector_primary@>@/
{

   Pointer_Vector<Pen>* pv 
         = static_cast<Pointer_Vector<Pen>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE dash_pattern_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�dash pattern vector primary>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE dash_pattern_vector_primary@>@/
{

   Pointer_Vector<Dash_Pattern>* pv 
         = static_cast<Pointer_Vector<Dash_Pattern>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE color_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�color vector primary>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE color_vector_primary@>@/
{

   Pointer_Vector<Color>* cv 
         = static_cast<Pointer_Vector<Color>*>(@=$2@>); 

   @=$$@> = cv->ctr;

   delete cv;

};

@q ***** (5) numeric_primary --> SIZE transform_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�transform vector primary>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE transform_vector_primary@>@/
{

   Pointer_Vector<Transform>* pv 
         = static_cast<Pointer_Vector<Transform>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE focus_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�focus vector primary>.
\initials{LDF 2005.01.18.}

\LOG
\initials{LDF 2005.01.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE focus_vector_primary@>@/
{

   Pointer_Vector<Focus>* pv 
         = static_cast<Pointer_Vector<Focus>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE point_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�point vector primary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE point_vector_primary@>@/
{

   Pointer_Vector<Point>* pv 
         = static_cast<Pointer_Vector<Point>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE bool_point_vector_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�bool-point vector primary>.
\initials{LDF 2004.11.09.}

\LOG
\initials{LDF 2004.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE bool_point_vector_primary@>@/
{

   Pointer_Vector<Bool_Point>* bpv 
         = static_cast<Pointer_Vector<Bool_Point>*>(@=$2@>); 

   @=$$@> = bpv->ctr;

   delete bpv;

};

@q ***** (5) numeric_primary --> SIZE path_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�path vector primary>.
\initials{LDF 2004.12.12.}

\LOG
\initials{LDF 2004.12.12.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE path_vector_primary@>@/
{

   Pointer_Vector<Path>* pv 
         = static_cast<Pointer_Vector<Path>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> LENGTH path_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�path vector primary>.
\initials{LDF 2023.04.08.}

\LOG
\initials{LDF 2023.04.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH path_vector_primary@>@/
{

   Pointer_Vector<Path>* pv = static_cast<Pointer_Vector<Path>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};


@q ***** (5) numeric_primary --> SIZE ellipse_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�ellipse vector primary>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE ellipse_vector_primary@>@/
{

   Pointer_Vector<Ellipse>* pv 
         = static_cast<Pointer_Vector<Ellipse>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE circle_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�circle vector primary>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE circle_vector_primary@>@/
{

   Pointer_Vector<Circle>* pv 
         = static_cast<Pointer_Vector<Circle>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE parabola_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�parabola vector primary>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE parabola_vector_primary@>@/
{

   Pointer_Vector<Parabola>* pv 
         = static_cast<Pointer_Vector<Parabola>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE hyperbola_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�hyperbola vector primary>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE hyperbola_vector_primary@>@/
{

   Pointer_Vector<Hyperbola>* pv 
         = static_cast<Pointer_Vector<Hyperbola>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE nurb_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�nurb vector primary>.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE nurb_vector_primary@>@/
{

   Pointer_Vector<Nurb>* pv 
         = static_cast<Pointer_Vector<Nurb>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE polygon_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�polygon vector primary>.
\initials{LDF 2005.02.25.}

\LOG
\initials{LDF 2005.02.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE polygon_vector_primary@>@/
{

   Pointer_Vector<Polygon>* pv 
         = static_cast<Pointer_Vector<Polygon>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE triangle_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�triangle vector primary>.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE triangle_vector_primary@>@/
{

   Pointer_Vector<Triangle>* pv 
         = static_cast<Pointer_Vector<Triangle>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE rectangle_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�rectangle vector primary>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE rectangle_vector_primary@>@/
{

   Pointer_Vector<Rectangle>* pv 
         = static_cast<Pointer_Vector<Rectangle>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE reg_polygon_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�regular polygon vector primary>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE reg_polygon_vector_primary@>@/
{

   Pointer_Vector<Reg_Polygon>* pv 
         = static_cast<Pointer_Vector<Reg_Polygon>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE ellipsoid_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�ellipsoid vector primary>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE ellipsoid_vector_primary@>@/
{

   Pointer_Vector<Ellipsoid>* pv 
         = static_cast<Pointer_Vector<Ellipsoid>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE sphere_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�sphere vector primary>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE sphere_vector_primary@>@/
{

   Pointer_Vector<Sphere>* pv 
         = static_cast<Pointer_Vector<Sphere>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE cuboid_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�cuboid vector primary>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE cuboid_vector_primary@>@/
{

   Pointer_Vector<Cuboid>* pv 
         = static_cast<Pointer_Vector<Cuboid>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE polyhedron_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�polyhedron vector primary>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE polyhedron_vector_primary@>@/
{

   Pointer_Vector<Polyhedron>* pv 
         = static_cast<Pointer_Vector<Polyhedron>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> SIZE picture_vector_variable.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} 
\�picture vector primary>.
\initials{LDF 2005.01.17.}

\LOG
\initials{LDF 2005.01.17.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=numeric_primary: SIZE picture_vector_variable@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

@q ******* (7) Error handling:  |entry == 0|.@> 

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.17.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

         @=$$@> = INVALID_REAL;

      }  /* |if (entry == 0)|  */

@q ******* (7) |entry != 0|.@>     

@ |entry != 0|.
\initials{LDF 2005.01.17.}

@<Define rules@>=

   else  /* |entry != 0|  */
      {
          if (entry->object == static_cast<void*>(0))
             @=$$@> = ZERO_REAL;

          else  /* |entry->object != 0|  */
             {
                 Pointer_Vector<Picture>* pv 
                    = static_cast<Pointer_Vector<Picture>*>(
                                                        entry->object); 
                 @=$$@> = pv->ctr;

             }  /* |else| (|entry->object != 0|)  */

      }  /* |else| (|entry != 0|)  */

@q ******* (7) @> 

};

@q ***** (5) numeric_primary --> SIZE macro_vector_variable.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�macro vector primary>.
\initials{LDF 2005.01.06.}

\LOG
\initials{LDF 2005.01.06.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=numeric_primary: SIZE macro_vector_variable@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

@q ******* (7) Error handling:  |entry == 0|.@> 

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.06.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         @=$$@> = INVALID_REAL;

      }  /* |if (entry == 0)|  */

@q ******* (7) |entry != 0|.@>     

@ |entry != 0|.
\initials{LDF 2005.01.06.}

@<Define rules@>=

   else  /* |entry != 0|  */
      {
          if (entry->object == static_cast<void*>(0))
             @=$$@> = ZERO_REAL;

          else  /* |entry->object != 0|  */
             {
                 Pointer_Vector<Definition_Info_Type>* pv 
                    = static_cast<Pointer_Vector<Definition_Info_Type>*>(
                                                               entry->object); 
                 @=$$@> = pv->ctr;

             }  /* |else| (|entry->object != 0|)  */

      }  /* |else| (|entry != 0|)  */

@q ******* (7) @> 

};


@q ***** (5) numeric_primary --> SIZE glyph_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�glyph vector primary>.
\initials{LDF 2023.04.08.}

\LOG
\initials{LDF 2023.04.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE glyph_vector_primary@>@/
{

   Pointer_Vector<Glyph>* pv = static_cast<Pointer_Vector<Glyph>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> LENGTH glyph_vector_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�glyph vector primary>.
\initials{LDF 2023.04.08.}

\LOG
\initials{LDF 2023.04.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH glyph_vector_primary@>@/
{

   Pointer_Vector<Glyph>* pv = static_cast<Pointer_Vector<Glyph>*>(@=$2@>); 

   @=$$@> = pv->ctr;

   delete pv;

};

@q ***** (5) numeric_primary --> LENGTH sinewave_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{LENGTH} \�sinewave primary>.
\initials{LDF 2023.08.15.}

\LOG
\initials{LDF 2023.08.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: LENGTH sinewave_primary@>@/
{
   Sinewave* s = static_cast<Sinewave*>(@=$2@>);

   if (s)
     @=$$@> = s->size();
   else
     @=$$@> = ZERO_REAL;

   delete s;

};

@q ***** (5) numeric_primary --> SIZE sinewave_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \.{SIZE} \�sinewave primary>.
\initials{LDF 2023.08.15.}

\LOG
\initials{LDF 2023.08.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: SIZE sinewave_primary@>@/
{
   Sinewave* s = static_cast<Sinewave*>(@=$2@>);

   if (s)
     @=$$@> = s->size();
   else
     @=$$@> = ZERO_REAL;

   delete s;

};

@q ***** (5) numeric_primary --> point_part point_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \�point part> \�point primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.05.05.}  
This rule currently only extracts coordinates from |Point::world_coordinates|.
I want to make it possible to get coordinates from the other coordinate
vectors, either by modifying this rule, or by adding one or more others.
\ENDTODO 

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.

\initials{LDF 2007.11.04.}
Removed code from this rule.  Now calling 
|Scan_Parse::numeric_primary_rule_func_1|, which contains the old code.
\ENDLOG 

@q ****** (6) Definition.  @>

@<Define rules@>=
@=numeric_primary: point_part point_primary@>@/
{

    @=$$@> = numeric_primary_rule_func_1(@=$1@>, @=$2@>, parameter);

};

@q ***** (5) numeric_primary --> point_part transform_primary.@>

@*4 \�numeric primary> $\longrightarrow$ 
\�transform part> \�transform primary>.
\initials{LDF 2004.10.06.}

@<Define rules@>=
@=numeric_primary: point_part transform_primary@>@/
{

  Transform* t = static_cast<Transform*>(@=$2@>);

  int part = @=$1@>;

  if (part == XPART)
    @=$$@> = t->get_element(3, 0);
  
  else if (part == YPART)
    @=$$@> = t->get_element(3, 1);

  else if (part == ZPART)
    @=$$@> = t->get_element(3, 2);

  else if (part == WPART)
    @=$$@> = t->get_element(3, 3);

else
    {
#if 0 
      cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                << "`numeric_primary --> "
                << endl 
                << "point_part transform_primary': "
                << "Invalid `point_part': " << name_map[part]
                << "." << endl << "Setting `numeric_primary' "
                << "to `INVALID_REAL' "
                << "and will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");
#endif 
      @=$$@> = INVALID_REAL;

    } /* |else|  */

  delete t;

};

@q ***** (5) numeric_primary --> transform_part transform_primary.@>

@*4 \�numeric primary> $\longrightarrow$ 
\�transform part> \�transform primary>.
\initials{LDF 2004.10.06.}

@<Define rules@>=
@=numeric_primary: transform_part transform_primary@>@/
{

  Transform* t = static_cast<Transform*>(@=$2@>);

  int part = @=$1@>;

  if (part == XXPART)
    @=$$@> = t->get_element(0, 0);

  else if (part == XYPART)
    @=$$@> = t->get_element(1, 0);

  else if (part == XZPART)
    @=$$@> = t->get_element(2, 0);

  else if (part == YXPART)
    @=$$@> = t->get_element(0, 0);

  else if (part == YYPART)
    @=$$@> = t->get_element(1, 0);

  else if (part == YZPART)
    @=$$@> = t->get_element(2, 0);

  else if (part == ZXPART)
    @=$$@> = t->get_element(0, 0);

  else if (part == ZYPART)
    @=$$@> = t->get_element(1, 0);

  else if (part == ZZPART)
    @=$$@> = t->get_element(2, 0);

  else if (part == WXPART)
    @=$$@> = t->get_element(0, 0);

  else if (part == WYPART)
    @=$$@> = t->get_element(1, 0);

  else if (part == WZPART)
    @=$$@> = t->get_element(2, 0);

  else
    {
#if 0 
      cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                << "`numeric_primary --> "
                << endl 
                << "transform_part transform_primary': "
                << "Invalid `transform_part': " << name_map[part]
                << "." << endl << "Setting `numeric_primary' "
                << "to `INVALID_REAL' "
                << "and will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");
#endif

      @=$$@> = INVALID_REAL;

    } /* |else|  */

  delete t;

};

@q ***** (5) numeric_primary --> color_part color_primary.@>

@*4 \�numeric primary> $\longrightarrow$ 
\�color part> \�color primary>.
\initials{LDF 2004.12.16.}

\LOG
\initials{LDF 2004.12.16.}
Added this rule.

\initials{LDF 2004.12.20.}
Now setting \�numeric primary> to 0 if \�color part> is
\.{YELLOW\_PART}, 
\.{CYAN\_PART}, 
\.{MAGENTA\_PART}, 
\.{BLACK\_PART}, 
\.{WHITE\_PART}, 
\.{RED\_ORANGE\_PART}, or
\.{BLUE\_VIOLET\_PART}.  No warning is issued.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=numeric_primary: color_part color_primary@>@/
{

  Color* c = static_cast<Color*>(@=$2@>);

  int part = @=$1@>;

#if 0 
  cerr << "part == " << part << " == " << name_map[part] << endl;
#endif 

  if (part == RED_PART)
    @=$$@> = c->get_red_part();

  else if (part == GREEN_PART)
    @=$$@> = c->get_green_part();

  else if (part == BLUE_PART)
    @=$$@> = c->get_blue_part();

  else if (part == CYAN_PART)
    @=$$@> = c->get_cyan_part();

  else if (part == YELLOW_PART)
    @=$$@> = c->get_yellow_part();

  else if (part == MAGENTA_PART)
    @=$$@> = c->get_magenta_part();

  else if (part == BLACK_PART)
    @=$$@> = c->get_black_part();

  else if (part == GREY_PART)
    @=$$@> = c->get_grey_part();

  else if (part == WHITE_PART)
    @=$$@> = ZERO_REAL;

  else if (part == RED_ORANGE_PART)
    @=$$@> = ZERO_REAL;

  else if (part == BLUE_VIOLET_PART)
    @=$$@> = ZERO_REAL;

  else
    {

#if 0
      cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                << "`numeric_primary --> "
                << endl 
                << "color_part color_primary': "
                << "Invalid `color_part': " << name_map[part]
                << "." << endl << "Setting `numeric_primary' "
                << "to `INVALID_REAL' "
                << "and will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");
#endif

      @=$$@> = INVALID_REAL;

    } /* |else|  */

  delete c;
  c = 0;

};

@q ***** (5) numeric_primary --> GET_TYPE color_primary.@>

@*4 \�numeric primary> $\longrightarrow$ \.{GET\_TYPE} \�color primary>.
\initials{LDF 2024.01.25.}

\LOG
\initials{LDF 2024.01.25.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_TYPE color_primary@>@/
{

  Color* c = static_cast<Color*>(@=$2@>);

  if (c)
     @=$$@> = c->get_type();
  else
     @=$$@> = INVALID_REAL;

  delete c;
  c = 0;

};

@q **** (4) point_part.@>
@*3 \�point part>.

\LOG
\initials{LDF 2004.05.05.}  
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> point_part@>@/

@q ***** (5) point_part --> XPART.  @>
@ \�point part> $\longrightarrow$ \.{XPART}.

\LOG
\initials{LDF 2004.05.05.}  Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_part: XPART@>@/
{

  @=$$@> = XPART;

};

@q ***** (5) point_part --> YPART.  @>
@ \�point part> $\longrightarrow$ \.{YPART}.

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_part: YPART@>@/
{

  @=$$@> = YPART;

};

@q ***** (5) point_part --> ZPART.  @>
@ \�point part> $\longrightarrow$ \.{ZPART}.

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_part: ZPART@>@/
{

  @=$$@> = ZPART;

};

@q ***** (5) point_part --> WPART.  @>
@ \�point part> $\longrightarrow$ \.{WPART}.

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.
\ENDLOG 

@<Define rules@>=
@=point_part: WPART@>@/
{

  @=$$@> = WPART;

};

@q **** (4) transform_part.@> 
@*3 \�transform part>.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> transform_part@>@/

@q ***** (5) transform_part --> XXPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{XXPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: XXPART@>@/
{
  @=$$@> = XXPART;

};

@q ***** (5) transform_part --> XYPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{XYPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: XYPART@>@/
{

  @=$$@> = XYPART;

};

@q ***** (5) transform_part --> XZPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{XZPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: XZPART@>@/
{

  @=$$@> = XZPART;

};

@q ***** (5) transform_part --> YXPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{YXPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: YXPART@>@/
{

  @=$$@> = YXPART;

};

@q ***** (5) transform_part --> YYPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{YYPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: YYPART@>@/
{

  @=$$@> = YYPART;

};

@q ***** (5) transform_part --> YZPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{YZPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: YZPART@>@/
{

  @=$$@> = YZPART;

};

@q ***** (5) transform_part --> ZXPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{ZXPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: ZXPART@>@/
{

  @=$$@> = ZXPART;

};

@q ***** (5) transform_part --> ZYPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{ZYPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: ZYPART@>@/
{

  @=$$@> = ZYPART;

};

@q ***** (5) transform_part --> ZZPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{ZZPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: ZZPART@>@/
{

  @=$$@> = ZZPART;

};

@q ***** (5) transform_part --> WXPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{WXPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: WXPART@>@/
{

  @=$$@> = WXPART;

};

@q ***** (5) transform_part --> WYPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{WYPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: WYPART@>@/
{

  @=$$@> = WYPART;

};

@q ***** (5) transform_part --> WZPART.@> 
@*4 \�transform part> $\longrightarrow$ \.{WZPART}.
\initials{LDF 2004.10.07.}

\LOG
\initials{LDF 2004.10.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=transform_part: WZPART@>@/
{

  @=$$@> = WZPART;

};

@q **** (4) color_part.@>
@*3 \�color part>.

\LOG
\initials{LDF 2004.12.16.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> color_part@>@/

@q ***** (5) color_part --> RED_PART.  @>
@ \�color part> $\longrightarrow$ \.{RED\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: RED_PART@>@/
{

  @=$$@> = RED_PART;

};

@q ***** (5) color_part --> GREEN_PART.  @>
@ \�color part> $\longrightarrow$ \.{GREEN\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: GREEN_PART@>@/
{

  @=$$@> = GREEN_PART;

};

@q ***** (5) color_part --> BLUE_PART.  @>
@ \�color part> $\longrightarrow$ \.{BLUE\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: BLUE_PART@>@/
{

  @=$$@> = BLUE_PART;

};

@q ***** (5) color_part --> YELLOW_PART.  @>
@ \�color part> $\longrightarrow$ \.{YELLOW\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: YELLOW_PART@>@/
{

  @=$$@> = YELLOW_PART;

};

@q ***** (5) color_part --> CYAN_PART.  @>
@ \�color part> $\longrightarrow$ \.{CYAN\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: CYAN_PART@>@/
{

  @=$$@> = CYAN_PART;

};

@q ***** (5) color_part --> MAGENTA_PART.  @>
@ \�color part> $\longrightarrow$ \.{MAGENTA\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: MAGENTA_PART@>@/
{

  @=$$@> = MAGENTA_PART;

};

@q ***** (5) color_part --> BLACK_PART.  @>
@ \�color part> $\longrightarrow$ \.{BLACK\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: BLACK_PART@>@/
{

  @=$$@> = BLACK_PART;

};

@q ***** (5) color_part --> WHITE_PART.  @>
@ \�color part> $\longrightarrow$ \.{WHITE\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: WHITE_PART@>@/
{

  @=$$@> = WHITE_PART;

};


@q ***** (5) color_part --> GREY_PART.  @>
@ \�color part> $\longrightarrow$ \.{GREY\_PART}.

\LOG
\initials{LDF 2023.09.26.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: GREY_PART@>@/
{

  @=$$@> = GREY_PART;

};

@q ***** (5) color_part --> RED_ORANGE_PART.  @>
@ \�color part> $\longrightarrow$ \.{RED\_ORANGE\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: RED_ORANGE_PART@>@/
{

  @=$$@> = RED_ORANGE_PART;

};

@q ***** (5) color_part --> BLUE_VIOLET_PART.  @>
@ \�color part> $\longrightarrow$ \.{BLUE\_VIOLET\_PART}.

\LOG
\initials{LDF 2004.12.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=color_part: BLUE_VIOLET_PART@>@/
{

  @=$$@> = BLUE_VIOLET_PART;

};

@q ***** (5) TURNING_NUMBER path_primary.  @>

@*4 \.{TURNING\_NUMBER} \�path primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this rule.  
\ENDTODO 

@q ***** (5) TOTALWEIGHT picture_primary.  @>

@*4 \.{TOTALWEIGHT} \�picture primary>.

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.04.28.}
Add this rule.  
\ENDTODO 

@q ***** (5) numeric_primary --> numeric_operator numeric_primary.  @>

@*4 \�numeric primary> $\longrightarrow$ \�numeric operator> \�numeric primary>.

\LOG
\initials{LDF 2004.04.28.}  
Added code for all cases.  Some contain dummy code, however. 

\initials{LDF 2007.02.06.}
Now calling |Scan_Parse::numeric_primary_rule_func_0|.
\ENDLOG 

@<Define rules@>=
@=numeric_primary: numeric_operator numeric_primary@>
{ 
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_primary "
           << "--> numeric_operator numeric_primary." << endl;
#endif /* |DEBUG_COMPILE|  */

       real r;

       int i = numeric_primary_rule_func_0(@=$1@>,  
                                           @=$2@>, 
                                           &r,                               
                                           static_cast<Scanner_Node>(parameter));   

       @=$$@> = (i == 0) ? r : INVALID_REAL;

}; /* End of rule |@=numeric_operator numeric_primary@>|.  */

@q ***** (5) numeric_primary --> LAST numeric_vector_expression.@>

@*4 \�numeric primary> $\longrightarrow$ 
\.{LAST} \�numeric vector expression>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=numeric_primary: LAST numeric_vector_expression@>@/
{ 

   Pointer_Vector<real>* pv 
      = static_cast<Pointer_Vector<real>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<real>*>(0))
      {
          @=$$@> = INVALID_REAL;

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {

          @=$$@> = INVALID_REAL;

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

   else 
      @=$$@> = *(pv->v[pv->ctr - 1]);

@q ******* (7) @> 

};

@q ***** (5) numeric_primary --> GET_DISTANCE focus_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_DISTANCE}
\�focus expression>.
\initials{LDF 2007.09.24.}

\LOG
\initials{LDF 2007.09.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_DISTANCE focus_expression@>@/
{

  Focus* f = static_cast<Focus*>(@=$2@>); 

  if (f)
  {
     @=$$@> = f->get_distance();
     delete f;
  }
  else
     @=$$@> = INVALID_REAL;

};

@q ***** (5) numeric_primary --> GET_DIAMETER circle_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_DIAMETER}
\�circle expression>.
\initials{LDF 2007.11.04.}

\LOG
\initials{LDF 2007.11.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_DIAMETER circle_expression@>@/
{

  Circle* c = static_cast<Circle*>(@=$2@>); 

  if (c)
  {
     @=$$@> = c->get_diameter();
     delete c;
  }
  else
     @=$$@> = INVALID_REAL;

};

@q ***** (5) numeric_primary --> GET_RADIUS circle_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_RADIUS}
\�circle expression>.
\initials{LDF 2009.09.15.}

\LOG
\initials{LDF 2009.09.15.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_RADIUS circle_expression@>@/
{

  Circle* c = static_cast<Circle*>(@=$2@>); 

  if (c)
  {
     @=$$@> = c->get_radius();
     delete c;
     c = 0;
  }
  else
  {
     cerr << "ERROR!  In parser, rule `numeric_primary: GET_RADIUS circle_expression':"
          << endl 
          << "`circle_expression' is NULL.  Can't get radius." << endl 
          << "Returning `INVALID_REAL'." << endl;

     @=$$@> = INVALID_REAL;
  }

};

@q ***** (5) numeric_primary --> GET_RADIUS sphere_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_RADIUS}
\�sphere expression>.
\initials{LDF 2022.11.29.}

!! PLEASE NOTE:  It is much better to use the command corresponding to the rule 
\�numeric primary> $\longrightarrow$ \.{GET\_SPHERE\_RADIUS} \�sphere variable> because
it doesn't require copying a |Sphere| object, whereas this rule does.
|Sphere| objects are almost always very large, so copying them to make a temporary is
wasteful of time and memory.
\par
This rule is included to avoid errors, if a user doesn't know or forgets this.
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_RADIUS sphere_expression@>@/
{
@q ****** (6) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `numeric_primary:  GET_RADIUS sphere_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>
@
@<Define rules@>= 

  cerr_strm << thread_name 
            << "WARNING!  In parser, rule `numeric_primary:  GET_RADIUS sphere_expression':"
            << endl
            << "This rule requires copying a (probably large) sphere object in order to create a temporary object."
            << endl 
            << "This is wasteful of time and memory." << endl 
            << "It is better to use the `get_sphere_radius <sphere variable>' instead."
            << endl 
            << "However, this command works, too, in order to avoid unnecessary syntax errors.";

  log_message(cerr_strm);
  cerr_message(cerr_strm);
  cerr_strm.str("");

@q ****** (6) @>
@
@<Define rules@>= 

  Sphere* s = static_cast<Sphere*>(@=$2@>); 

  if (s)
  {
     @=$$@> = s->get_radius();
     delete s;
     s = 0;
  }
  else
  {
     cerr << "ERROR!  In parser, rule `numeric_primary: GET_RADIUS sphere_expression':"
          << endl 
          << "`sphere_expression' is NULL.  Can't get radius." << endl 
          << "Returning `INVALID_REAL'." << endl;

     @=$$@> = INVALID_REAL;
  }

};



@q ***** (5) numeric_primary --> CIRCUMFERENCE circle_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{CIRCUMFERENCE}
\�circle expression>.
\initials{LDF 2009.09.16.}

\LOG
\initials{LDF 2009.09.16.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: CIRCUMFERENCE circle_expression@>@/
{

  Circle* c = static_cast<Circle*>(@=$2@>); 

  if (c)
  {
     @=$$@> = c->get_circumference();
     delete c;
  }
  else
     @=$$@> = INVALID_REAL;

};

@q ***** (5) numeric_primary --> GET_CIRCUMFERENCE WITH_RADIUS numeric_expression.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET_CIRCUMFERENCE} \.{WITH\_RADIUS} \�numeric expression>.@>
\initials{LDF 2021.07.03.}

\LOG
\initials{LDF 2021.07.03.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_CIRCUMFERENCE WITH_RADIUS numeric_expression@>@/
{

  Circle c;
  Point p(0, 0, 0);
  c.set(p, 2 * @=$3@>);

  @=$$@> = c.get_circumference();
  
};

@q ***** (5) numeric_primary --> GET_SPHERE_DIAMETER sphere_variable.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET_SPHERE\_DIAMETER}
\�sphere variable>.
\initials{LDF 2022.11.01.}

\LOG
\initials{LDF 2022.11.01.}
Added this rule.  \.{GET\_DIAMETER} can't be used here because it causes 
reduce/reduce conflicts.  \�sphere variable> is used instead
of \�sphere expression> in order to avoid copying large objects.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_SPHERE_DIAMETER sphere_variable@>@/
{

  Sphere* s = 0;

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
     s = static_cast<Sphere*>(entry->object); 

  if (s)
  {
     @=$$@> = s->get_diameter();
  }
  else
     @=$$@> = INVALID_REAL;

};

@q ***** (5) numeric_primary --> GET_SPHERE_RADIUS sphere_variable.  @>

@ \�numeric primary> $\longrightarrow$ \.{GET_SPHERE\_RADIUS}
\�sphere variable>.
\initials{LDF 2022.11.01.}

\LOG
\initials{LDF 2022.11.01.}
Added this rule.  \.{GET\_RADIUS} can't be used here because it causes 
reduce/reduce conflicts.  \�sphere variable> is used instead
of \�sphere expression> in order to avoid copying large objects.
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_SPHERE_RADIUS sphere_variable@>@/
{

  Sphere* s = 0;

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
     s = static_cast<Sphere*>(entry->object); 

  if (s)
  {
     @=$$@> = s->get_radius();
  }
  else
     @=$$@> = INVALID_REAL;

};

@q ***** (5) numeric_primary --> ANGLE_DIRECTION_METAPOST numeric_expression @>
@q ***** (5) OF path_expression call_metapost_option_list                    @>

@ \�numeric primary> $\longrightarrow$ \.{ANGLE\_DIRECTION\_METAPOST} 
\�numeric expression> \.{Of} \�path expression> \�call metapost option list>.
\initials{LDF 2021.11.24.}

\LOG
\initials{LDF 2021.11.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: ANGLE_DIRECTION_METAPOST numeric_expression OF path_expression @>@/
@=call_metapost_option_list@>@/
{

@q ****** (6) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `numeric_primary:  ANGLE_DIRECTION_METAPOST numeric_expression"
                << endl 
                << "OF path_expression call_metapost_option_list.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

  bool save               = @=$5@> & 1U;
  bool clear              = @=$5@> & 2U;
  bool suppress_mp_stdout = @=$5@> & 4U;
  bool do_transform       = @=$5@> & 8U;

  Point *origin_pt = 0;
  Point *x_axis_pt = 0;

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

  real r = 0.0;
  Path *q = static_cast<Path*>(@=$4@>); 

  status = q->get_metapost_path_info(@=$2@>, 
                                     0,
                                     &r,
                                     0,
                                     0, 
                                     0, 
                                     origin_pt, 
                                     x_axis_pt, 
                                     do_transform,
                                     save,
                                     suppress_mp_stdout,
                                     scanner_node);


@q ****** (6) @>

  if (status != 0)
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `numeric_primary: ANGLE_DIRECTION_METAPOST "
                << "numeric_expression OF path_expression call_metapost_option_list':"
                << endl 
                << "`Path::get_metapost_path_info' failed, returning " << status << "." 
                << endl
                << "Failed to obtain angle direction of path."
                << "Setting `numeric_primary' to `INVALID_REAL' and continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = INVALID_REAL;

  }  /* |if (status != 0)| */

@q ****** (6) @>

  else
  {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr_strm << thread_name 
                    << "In parser, rule `numeric_primary: ANGLE_DIRECTION_METAPOST "
                    << "numeric_expression OF path_expression call_metapost_option_list':"
                    << endl 
                    << "`Path::get_metapost_path_info' succeeded, returning 0." << endl
                    << "'r' == " << r << endl;

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
      }  
#endif /* |DEBUG_COMPILE|  */@; 

     @=$$@> = r;

  }  /* |else| */

@q ****** (6) @>

  delete q;
  q = 0;

  if (x_axis_pt)
  {
     delete x_axis_pt;
     x_axis_pt = 0;
  }

  if (origin_pt)
  {
     delete origin_pt;
     origin_pt = 0;
  }

  if (scanner_node->metapost_output_struct)
  {
     delete scanner_node->metapost_output_struct;
     scanner_node->metapost_output_struct = 0;
  }
 
  if (scanner_node->tolerance)
  {
     delete scanner_node->tolerance;
     scanner_node->tolerance = 0;
  } 
 
};

@q ***** (5) numeric_primary --> TURNINGNUMBER path_expression call_metapost_option_list@>@/

@ \�numeric primary> $\longrightarrow$ \.{TURNINGNUMBER} \�path expression>
\�call metapost option list>.
\initials{LDF 2022.01.18.}

\LOG
\initials{LDF 2022.01.18.}
Added this rule.

\initials{LDF 2022.05.17.}
Rewrote this rule.  It now calls |Path::get_metapost_path_info|.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: TURNINGNUMBER path_expression call_metapost_option_list@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: TURNINGNUMBER path_expression call_metapost_option_list.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Path *q = static_cast<Path*>(@=$2@>);
    real r = 0.0;
    Point *x_axis_pt = 0;
    Point *origin_pt = 0;

    bool save               = @=$3@> & 1U;
    bool clear              = @=$3@> & 2U;
    bool suppress_mp_stdout = @=$3@> & 4U;
    bool do_transform       = @=$3@> & 8U;
    

@q *** (3) @>

   if (@=$3@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$3@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 




    status = q->get_metapost_path_info(0,
                                       0,
                                       0,
                                       &r,
                                       0, 
                                       0, 
                                       origin_pt, 
                                       x_axis_pt, 
                                       do_transform,
                                       save,
                                       suppress_mp_stdout,
                                       scanner_node);


@q ****** (6) @>

  if (status != 0)
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `numeric_primary: TURNINGNUMBER path_expression "
                << "call_metapost_option_list':"
                << endl 
                << "`Path::get_metapost_path_info' failed, returning " << status << "." 
                << endl
                << "Failed to obtain turningnumber of path."
                << "Setting `numeric_primary' to `INVALID_REAL' and continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      r = INVALID_REAL;

  }  /* |if (status != 0)| */

@q ****** (6) @>

  else
  {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr_strm << thread_name 
                    << "In parser, rule `numeric_primary: TURNINGNUMBER path_expression "
                    << "call_metapost_option_list':"
                    << endl 
                    << "`Path::get_metapost_path_info' succeeded, returning 0." << endl
                    << "'r' == " << r << endl;

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
      }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |else| */

@q ****** (6) @>

  @=$$@> = r;

  delete q;
  q = 0;

  if (x_axis_pt)
  {
     delete x_axis_pt;
     x_axis_pt = 0;
  }

  if (origin_pt)
  {
     delete origin_pt;
     origin_pt = 0;
  }

  if (scanner_node->metapost_output_struct)
  {
     delete scanner_node->metapost_output_struct;
     scanner_node->metapost_output_struct = 0;
  }

  if (scanner_node->tolerance)
  {
     delete scanner_node->tolerance;
     scanner_node->tolerance = 0;
  } 

};

@q * (1) @>

@ \�numeric primary> $\longrightarrow$ \.{GET_ANGLE} \�numeric expression> \.{COMMA}
\�triangle expression> \�with test optional>.
\initials{LDF 2022.12.17.}

The values 0, 1 and 2 should be used.  However, values $<1$ are accepted for 0,
$<2$ for 1 and $<3$ for 2.  This is to account for possible loss of precision due to
the use of real numbers.
\initials{LDF 2024.11.12.}

\LOG
\initials{LDF 2024.11.12.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_ANGLE numeric_expression COMMA triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_ANGLE numeric_expression COMMA "
                << "triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$4@>);

    real r = @=$2@>;

    bool do_test;

    if (@=$5@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    if (r < 1.0)
       @=$$@> = t->get_alpha(scanner_node, do_test);
    else if (r < 2.0)
       @=$$@> = t->get_beta(scanner_node, do_test);
    else if (r < 3.0)
       @=$$@> = t->get_gamma(scanner_node, do_test);
    else 
    {
      cerr << "ERROR!  In parser, rule `numeric_primary: GET_ANGLE numeric_expression COMMA"
           << endl 
           << "triangle_expression with_test_optional':"
           << endl 
           << "`numeric_expression' out of range:  " << @=$2@> << endl 
           << "Valid range:  0 ... 2"
           << endl 
           << "Setting `numeric_primary' to `INVALID_REAL' and will try to continue."
           << endl;

       @=$$@> = INVALID_REAL;
    }



    delete t;
    t = 0;

};




@ \�numeric primary> $\longrightarrow$ \.{GET_ALPHA} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_ALPHA triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_ALPHA triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_alpha(scanner_node, do_test);

    delete t;
    t = 0;

};

@ \�numeric primary> $\longrightarrow$ \.{GET_BETA} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_BETA triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_BETA triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_beta(scanner_node, do_test);

    delete t;
    t = 0;

};

@ \�numeric primary> $\longrightarrow$ \.{GET_GAMMA} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_GAMMA triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_GAMMA triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_gamma(scanner_node, do_test);

    delete t;
    t = 0;

};

@q * (1) @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_SIDE\_LENGTH\_A} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_SIDE_LENGTH_A triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_SIDE_LENGTH_A triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_a(scanner_node, do_test);

    delete t;
    t = 0;

};

@ \�numeric primary> $\longrightarrow$ \.{GET\_SIDE\_LENGTH\_B} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_SIDE_LENGTH_B triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_SIDE_LENGTH_B triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_b(scanner_node, do_test);

    delete t;
    t = 0;

};


@ \�numeric primary> $\longrightarrow$ \.{GET\_SIDE\_LENGTH\_C} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_SIDE_LENGTH_C triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_SIDE_LENGTH_C triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_c(scanner_node, do_test);

    delete t;
    t = 0;

};

@ \�numeric primary> $\longrightarrow$ \.{GET_AREA} \�triangle expression>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: GET_AREA triangle_expression with_test_optional@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: numeric_primary: GET_AREA triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    @=$$@> = t->get_area(scanner_node, do_test);

    delete t;
    t = 0;

};

@q ***** (5) numeric_secondary --> DIRECTIONTIME numeric_list OF path_primary @>
@q ***** (5) call_metapost_option_list.                                       @>

@*4 \�numeric secondary> $\longrightarrow$ \.{DIRECTIONTIME} \�numeric list> \.{OF} 
\�path primary> \�call metapost option list>.
\initials{LDF 2022.05.05.}

@<Define rules@>=
@=numeric_secondary: DIRECTIONTIME numeric_list OF path_primary call_metapost_option_list @>@;
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   { 
       if (DEBUG)
       {
         cerr_strm << thread_name 
                   << "*** Parser: numeric_secondary: DIRECTIONTIME numeric_list OF path_primary"
                   << endl 
                   << "call_metapost_option_list."
                   << endl 
                   << "`call_metapost_option_list' ($5) == " << @=$5@>;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
       }    
       
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$2@>);
   Path *q = static_cast<Path*>(@=$4@>);
   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 


   real r = 0.0;

   if (pv && q && pv->v.size() >= 2)
   {
        cerr << "*(pv->v[0]) == " << *(pv->v[0]) << endl
             << "*(pv->v[1]) == " << *(pv->v[1]) << endl;

#if DEBUG_COMPILE
        if (DEBUG)
        { 
           q->show("q:");
        }    
#endif /* |DEBUG_COMPILE|  */@; 


        status = q->get_directiontime(*(pv->v[0]), *(pv->v[1]), &r, origin_pt, x_axis_pt, save, 
                                      suppress_mp_stdout, do_transform, 
                                      scanner_node); 

        if (status != 0)
        {
           @=$$@> = INVALID_REAL;
        }
        else
        {
           @=$$@> = r;
        }
   }

@q ****** (6) @>
   else
   {

      if (pv && pv->v.size() < 2)
         cerr << "`pv->v.size()' == " << pv->v.size() << " (< 2)" << endl;
      else
         cerr << "`pv' is NULL." << endl;

      if (!q)
         cerr << "`Path *q' is NULL." << endl;

      @=$$@> = INVALID_REAL;

   }  /* |else| */

@q ****** (6) @>

   if (pv)
   {         
      delete pv;
      pv = 0;
   }

   if (q)
   {
      delete q;
      q = 0;
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

@q ****** (6) @>

};

@ \�numeric primary> $\longrightarrow$ \.{RANDOM} random_option_list
\initials{LDF 2023.08.17.}

\LOG
\initials{LDF 2023.08.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_primary: RANDOM random_option_list@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: RANDOM random_option_list.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    real r;

    status = get_random_number(scanner_node, &r);

    if (status != 0)
    {
        cerr << "ERROR!  In parser, rule `numeric_primary: RANDOM random_option_list':"
             << endl 
             << "`get_random_number' (version 2) failed, returning " << status << "."
             << endl 
             << "Will try to continue." << endl;
    }
#if DEBUG_COMPILE
    else if (DEBUG)
    { 
        cerr << "In parser, rule `numeric_primary: RANDOM random_option_list':"
             << endl 
             << "`get_random_number' (version 2) succeeded, returning 0."
             << endl;
    }  
#endif /* |DEBUG_COMPILE|  */@; 

#if 0 
    cerr << "In parser:  r == " << r << endl;
#endif 

    scanner_node->random_range_low_value            = 0;   
    scanner_node->random_range_high_value           = 0;
    scanner_node->random_count_value                = 1;
    scanner_node->random_modulus_value              = 0;
    scanner_node->random_unique_flag                = 0;
    scanner_node->random_unique_factor              = 5;

    @=$$@> = r;

};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> random_option_list@>@/

@q *** (3) @>
@
@<Define rules@>=
@=random_option_list: /* Empty */@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: random_option_list: /* Empty */";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->random_range_low_value            = 0;   
    scanner_node->random_range_high_value           = 0;
    scanner_node->random_range_high_value           = 0;
    scanner_node->random_count_value                = 1;
    scanner_node->random_modulus_value              = 0;
    scanner_node->random_unique_flag                = 0;
    scanner_node->random_unique_factor              = 5;

    @=$$@> = 0;

};

@q *** (3) @>
@
@<Define rules@>=
@=random_option_list: random_option_list WITH_RANGE numeric_list@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: random_option_list: random_option_list WITH_RANGE numeric_list";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

    /* !! START HERE:  LDF 2023.08.17.  Use the value of L for a constant
       in Scanner_Type.  */

    long L = std::min(static_cast<long>(numeric_limits<int>::max()), 
                      static_cast<long>(numeric_limits<float>::max()));

    if (pv)
    {
#if 0 
        cerr << "pv->v.size() == " << pv->v.size() << endl;
#endif 

        if (pv->v.size() > 0)
        {
#if 0
           cerr << "*(pv->v[0]) == " << *(pv->v[0]) << endl;
#endif 

           scanner_node->random_range_low_value  = *(pv->v[0]);
        }

        if (pv->v.size() > 1)
        {
#if 0 
           cerr << "*(pv->v[1]) == " << *(pv->v[1]) << endl;
#endif 

           scanner_node->random_range_high_value = (*(pv->v[1]) > L) ? L : *(pv->v[1]);
            
#if 0
           cerr << "scanner_node->random_range_high_value == " << scanner_node->random_range_high_value 
                << endl;
#endif 
        }
        else
           scanner_node->random_range_high_value = L;
    }
    else
    {
#if 0 
        cerr << "pv == 0" << endl;
#endif 

        scanner_node->random_range_low_value  = 0;
        scanner_node->random_range_high_value = L;
    }

    delete pv;
    pv = 0;

    @=$$@> = 0;

};

@q *** (3) @>
@
@<Define rules@>=
@=random_option_list: random_option_list WITH_COUNT numeric_primary@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: random_option_list: random_option_list WITH_COUNT "
                << "numeric_primary";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "$3 == " << @=$3@> << endl;

    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->random_count_value = static_cast<int>(roundf(fabs(@=$3@>)));

#if 0 /* 1  */
    cerr << "scanner_node->random_count_value == " 
         << scanner_node->random_count_value 
         << endl;
#endif 

    @=$$@> = 0;

};


@q *** (3) @>
@
@<Define rules@>=
@=random_option_list: random_option_list WITH_MODULUS numeric_primary@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: random_option_list: random_option_list WITH_MODULUS "
                << "numeric_primary";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "$3 == " << @=$3@> << endl;

    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->random_modulus_value = static_cast<int>(roundf(@=$3@>));

#if 0 /* 1  */
    cerr << "scanner_node->random_modulus_value == " 
         << scanner_node->random_modulus_value 
         << endl;
#endif 

    @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>=
@=random_option_list: random_option_list UNIQUE @>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser:  `random_option_list: "
                << "random_option_list UNIQUE'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->random_unique_flag = 1;
   
   @=$$@> = 0;  

};


@q ** (2) @>
@
@<Define rules@>=
@=random_option_list: random_option_list WITH_FACTOR numeric_primary @>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser:  `random_option_list: "
                << "random_option_list WITH_FACTOR numeric_primary'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->random_unique_factor = static_cast<int>(fabs(roundf(@=$3@>)));
   
   @=$$@> = 0;  

};

@q ***** (5) numeric_secondary --> DIRECTIONTIME point_secondary OF path_primary @>
@q ***** (5) call_metapost_option_list.  @>

@*4 \�numeric secondary> $\longrightarrow$ \.{DIRECTIONTIME} \�point secondary> \.{OF} 
\�path primary> \�call metapost option list>.
\initials{LDF 2022.05.05.}

@<Define rules@>=
@=numeric_secondary: DIRECTIONTIME point_secondary OF path_primary call_metapost_option_list@>@;
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   { 
       if (DEBUG)
       {
         cerr_strm << thread_name 
                   << "*** Parser: numeric_secondary: DIRECTIONTIME point_secondary"
                   << endl 
                   << "OF path_primary call_metapost_option_list.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

       }    
       
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   Point *p = static_cast<Point*>(@=$2@>);
   Path *q  = static_cast<Path*>(@=$4@>);
   Point *x_axis_pt = 0;
   Point *origin_pt = 0;


   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;

   real r = 0.0;

   if (p && q)
   {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          p->show("p:");
          q->show("q:");
      }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

@ If |*p| lies in the x-z plane, exchange the z- and y-coordinates so that it
lies in the x-y plane.  This assumes that |*p| just indicates a direction with respect
to the main Cartesian axes.  There are other ways that it could be interpreted.  For example,
it would possible to pass it to |Path::directiontime| and have it transformed (if necessary) 
along with |*q|.
\initials{LDF 2022.05.12.}

@<Define rules@>=

      if (p->get_y() == 0.0 && p->get_z() != 0.0)
      {
         p->set_y(p->get_z());
         p->set_z(0.0);
      }

@q ******* (7) @>
@
@<Define rules@>=
 
      status = q->get_directiontime(p->get_x(), p->get_y(), &r, origin_pt, x_axis_pt, save, 
                                      suppress_mp_stdout, do_transform, 
                                      scanner_node); 

      if (status != 0)
      {
         cerr << "ERROR!  In parser, rule `numeric_secondary: DIRECTIONTIME point_secondary"
              << endl 
              << "OF path_primary call_metapost_option_list':" << endl
              << "`Path::get_directiontime' failed, returning " << status << "." << endl 
              << "Failed to obtain `directiontime' for `path_primary'." << endl 
              << "Setting `numeric_secondary' to `INVALID_REAL' and continuing."
              << endl;

         r = INVALID_REAL;

      }  /* |if (status != 0)| */

#if DEBUG_COMPILE
     else if (DEBUG)
     { 
         cerr << "In parser, rule `numeric_secondary: DIRECTIONTIME point_secondary"
              << endl 
              << "OF path_primary call_metapost_option_list':" << endl
              << "`Path::get_directiontime' succeeded, returning 0." << endl 
              << "`r' == " << r << "." << endl; 
     }   
#endif /* |DEBUG_COMPILE|  */@; 

   }  /* |if (p && q)| */

@q ****** (6) @>
@
@<Define rules@>=

   else
   {
      if (!p)
         cerr << "ERROR!  In parser, rule `numeric_secondary: DIRECTIONTIME point_secondary"
              << endl 
              << "OF path_primary call_metapost_option_list':" 
              << endl 
              << "`point_secondary' (`Point *p') is NULL." << endl
              << "Can't find directiontime." << endl 
              << "Setting `numeric_secondary' (`$$', `real r') to `INVALID_REAL' "
              << "and continuing." << endl;
               

      if (!q)
         cerr << "ERROR!  In parser, rule `numeric_secondary: DIRECTIONTIME point_secondary"
              << endl 
              << "OF path_primary call_metapost_option_list':" 
              << endl 
              << "`path_primary' (`Path *q') is NULL." << endl
              << "Can't find directiontime." << endl 
              << "Setting `numeric_secondary' (`$$', `real r') to `INVALID_REAL' "
              << "and continuing." << endl;

      r = INVALID_REAL;

   }  /* |else| */

@q ****** (6) @>

   if (p)
   {         
      delete p;
      p = 0;
   }

   if (q)
   {
      delete q;
      q = 0;
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

@q ****** (6) @>

   @=$$@> = r;

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

};

@q **** (4) numeric_operator.  @>
@ \�numeric operator>.

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> numeric_operator@>@/

@q ***** (5) numeric_operator --> SQRT.@>
@*4 \�numeric operator> $\longrightarrow$ \.{SQRT}.
\initials{LDF 2004.04.28.}  

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: SQRT@>@;  
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_operator "
           << "(SQRT)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = SQRT;

};

@q ***** (5) Trigonometric functions.@>
@*4 Trigonometric functions.
\initials{LDF 2007.02.06.}

\LOG
\initials{LDF 2007.02.06.}
Added this section.
\ENDLOG

@q ****** (6) numeric_operator --> SIND.@>

@*5 \�numeric operator> $\longrightarrow$ \.{SIND}.
\initials{LDF 2004.04.28.}  

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: SIND@>@;  
{
    @=$$@> = SIND;
};

@q ****** (6) numeric_operator --> COSD.  @>

@*5 \�numeric operator> $\longrightarrow$ \.{COSD}.
\initials{LDF 2004.04.28.}  

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: COSD@>@;  
{
  @=$$@> = COSD;
};

@q ****** (6) numeric_operator --> TAND.@>

@*5 \�numeric operator> $\longrightarrow$ \.{TAND}.
\initials{LDF 2007.02.06.}

\LOG
\initials{LDF 2007.02.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: TAND@>@;  
{

  @=$$@> = TAND;

};

@q ****** (6) numeric_operator --> ARCSIND.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCSIND}.
\initials{LDF 2007.02.06.}

\LOG
\initials{LDF 2007.02.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCSIND@>@;  
{

  @=$$@> = ARCSIND;

};

@q ****** (6) numeric_operator --> ARCCOSD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCCOSD}.
\initials{LDF 2007.02.06.}

\LOG
\initials{LDF 2007.02.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCCOSD@>@;  
{

  @=$$@> = ARCCOSD;

};

@q ****** (6) numeric_operator --> ARCTAND.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCTAND}.
\initials{LDF 2007.02.06.}

\LOG
\initials{LDF 2007.02.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCTAND@>@;  
{

  @=$$@> = ARCTAND;

};

@q ****** (6) numeric_operator --> SIN.@>

@*5 \�numeric operator> $\longrightarrow$ \.{SIN}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: SIN@>@;  
{

  @=$$@> = SIN;

};

@q ****** (6) numeric_operator --> COS.@>

@*5 \�numeric operator> $\longrightarrow$ \.{COS}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: COS@>@;  
{

  @=$$@> = COS;

};

@q ****** (6) numeric_operator --> TAN.@>

@*5 \�numeric operator> $\longrightarrow$ \.{TAN}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: TAN@>@;  
{

  @=$$@> = TAN;

};


@q ****** (6) numeric_operator --> SINH.@>

@*5 \�numeric operator> $\longrightarrow$ \.{SINH}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: SINH@>@;  
{

  @=$$@> = SINH;

};

@q ****** (6) numeric_operator --> COSH.@>

@*5 \�numeric operator> $\longrightarrow$ \.{COSH}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: COSH@>@;  
{

  @=$$@> = COSH;

};

@q ****** (6) numeric_operator --> TANH.@>

@*5 \�numeric operator> $\longrightarrow$ \.{TANH}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: TANH@>@;  
{

  @=$$@> = TANH;

};


@q ****** (6) numeric_operator --> SINHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{SINHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: SINHD@>@;  
{

  @=$$@> = SINHD;

};

@q ****** (6) numeric_operator --> COSHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{COSHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: COSHD@>@;  
{

  @=$$@> = COSHD;

};

@q ****** (6) numeric_operator --> TANHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{TANHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: TANHD@>@;  
{

  @=$$@> = TANHD;

};

@q ****** (6) numeric_operator --> ARCSINHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCSINHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCSINHD@>@;  
{

  @=$$@> = ARCSINHD;

};

@q ****** (6) numeric_operator --> ARCCOSHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCCOSHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCCOSHD@>@;  
{

  @=$$@> = ARCCOSHD;

};

@q ****** (6) numeric_operator --> ARCTANHD.@>

@*5 \�numeric operator> $\longrightarrow$ \.{ARCTANHD}.
\initials{LDF 2023.04.25.}

\LOG
\initials{LDF 2023.04.25.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ARCTANHD@>@;  
{

  @=$$@> = ARCTANHD;

};

@q ***** (5) numeric_operator --> MLOG.  @>
@q           numeric_operator 4. @>                       

@*4 \�numeric operator> $\longrightarrow$ \.{MLOG}.

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: MLOG@>@;  
{

  @=$$@> = MLOG;

};

@q ***** (5) numeric_operator --> MEXP.  @>

@*4 \�numeric operator> $\longrightarrow$ \.{MEXP}.
\initials{LDF 2004.04.28.}  

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: MEXP@>@;  
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_operator "
           << "(MEXP)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = MEXP;

};

@q ***** (5) numeric_operator --> FLOOR.  @>
@q           numeric_operator 6. @>                       

@ \�numeric operator> $\longrightarrow$ \.{FLOOR}.
\�numeric operator> 6.

\LOG
\initials{LDF 2004.04.28.}  Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: FLOOR@>@;  
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_operator "
           << "(FLOOR)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = FLOOR;

};

@q ***** (5) numeric_operator --> CEILING.  @>

@ \�numeric operator> $\longrightarrow$ \.{CEILING}.

\LOG
\initials{LDF 2021.10.22.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: CEILING@>@;  
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_operator "
           << "(CEILING)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = CEILING;

};

@q ***** (5) numeric_operator --> ROUND.  @>

@ \�numeric operator> $\longrightarrow$ \.{ROUND}.

\LOG
\initials{LDF 2021.10.22.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: ROUND@>@;  
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_operator "
           << "(ROUND)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = ROUND;

};

@q ***** (5) numeric_operator --> UNIFORMDEVIATE.  @>

@ \�numeric operator> $\longrightarrow$ \.{UNIFORMDEVIATE}.

\LOG
\initials{LDF 2004.04.28.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: UNIFORMDEVIATE@>@;  
{

  @=$$@> = UNIFORMDEVIATE;

};


@q ***** (5) numeric_operator --> MAX.  @>

@ \�numeric operator> $\longrightarrow$ \.{MAX}.

\LOG
\initials{LDF 2021.10.23.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: MAX@>@;  
{

  @=$$@> = MAX;

};

@q ***** (5) numeric_operator --> MIN.  @>

@ \�numeric operator> $\longrightarrow$ \.{MIN}.

\LOG
\initials{LDF 2021.10.23.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: MIN@>@;  
{

  @=$$@> = MIN;

};


@q **** (4) Numeric secondary.  @>
@*3 Numeric secondary.

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_secondary@>@/

@q ***** (5) numeric_secondary --> numeric_primary.@>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric primary>.  

@<Define rules@>= 
@=numeric_secondary: numeric_primary@>
{

  @=$$@> = @=$1@>;
  
};

@q ***** (5) numeric_secondary --> numeric_secondary @>
@q ***** (5) TIMES numeric_variable.                 @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\.{TIMES} \�numeric variable>. 

\LOG
\initials{LDF 2004.12.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: numeric_secondary TIMES numeric_variable@>
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

@q ******* (7) Error handling:  |entry == 0 || entry->object == 0|.@>    

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
      {

         @=$$@> = INVALID_REAL;
            
      } /* |if (entry == 0 || entry->object == 0)|  */

@q ******* (7) |entry != 0 && entry->object != 0|.@> 

@ |entry != 0 && entry->object != 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   else  /* |entry != 0 && entry->object != 0|  */
      {
         real* r = static_cast<real*>(entry->object);
 
         @=$$@> = @=$1@> * *r;  

      }  /* |else| (|entry != 0 && entry->object != 0|)  */

};

@q ***** (5) numeric_secondary --> numeric_secondary @>
@q ***** (5) numeric_variable.                       @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\�numeric variable>. 
\initials{LDF 2004.12.10.}

Multiplication without \.{TIMES}.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: numeric_secondary numeric_variable@>
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

@q ******* (7) Error handling:  |entry == 0 || entry->object == 0|.@>    

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
      {

         @=$$@> = INVALID_REAL;
            
      } /* |if (entry == 0 || entry->object == 0)|  */

@q ******* (7) |entry != 0 && entry->object != 0|.@> 

@ |entry != 0 && entry->object != 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   else  /* |entry != 0 && entry->object != 0|  */
      {
         real* r = static_cast<real*>(entry->object);
 
         @=$$@> = @=$1@> * *r;  

      }  /* |else| (|entry != 0 && entry->object != 0|)  */

};

@q ***** (5) numeric_secondary --> numeric_token    @>
@q ***** (5) OVER numeric_variable.                 @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric token> 
\.{OVER} \�numeric variable>. 

\LOG
\initials{LDF 2004.12.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: numeric_token OVER numeric_variable@>
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

@q ******* (7) Error handling:  |entry == 0 || entry->object == 0|.@>    

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
      {
         @=$$@> = INVALID_REAL;
            
      } /* |if (entry == 0 || entry->object == 0)|  */

@q ******* (7) |entry != 0 && entry->object != 0|.@> 

@ |entry != 0 && entry->object != 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

   else  /* |entry != 0 && entry->object != 0|  */
      {
         real* r = static_cast<real*>(entry->object);

@q ******** (8) Error handling:  |*r == 0|.@> 

@ Error handling:  |*r == 0|.
\initials{LDF 2004.12.07.}

@<Define rules@>=

      if (*r == ZERO_REAL)
         {
             @=$$@> = INVALID_REAL;

         } /* |if (*r == 0)|  */

@q ******** (8) @> 

      else /* |*r != 0|  */
         {

            @=$$@> = @=$1@> / *r;  

         } /* |else| (|*r != 0|)  */

      }   /* |else| (|entry != 0 && entry->object != 0|)  */

};

@q ***** (5) numeric_secondary --> numeric_secondary @>
@q ***** (5) times_or_over numeric_primary.          @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\�times or over> 
\�numeric primary>. 

@<Define rules@>= 
@=numeric_secondary: numeric_secondary times_or_over numeric_primary@>
{

  @<Common declarations for rules@>@; 

  if (@=$2@> == TIMES)
    @=$$@> = @=$1@> * @=$3@>;
  else if (@=$2@> == OVER)
    {
      if (@=$3@> == ZERO_REAL)
        {

#if 0 
@q           unsigned int first_line   = @@=@@1.first_line;@@>@@;   @>
@q           unsigned int first_column = @@=@@1.first_column;@@>@@; @>
@q           unsigned int last_line    = @@=@@3.last_line;@@>@@;    @>
@q           unsigned int last_column  = @@=@@3.last_column;@@>@@;  @>
#endif 

          cerr_strm << "ERROR! ";

#if 0 
          if (scanner_node->in->type == Io_Struct::FILE_TYPE)
            cerr_strm << "In input file `" << scanner_node->in->filename << "' "
                      << first_line << "." << first_column << "--" << last_line
                      << "." << last_column << ".\n";
#endif 

          cerr_strm << "Division by 0.  Setting <numeric secondary> to 0.\n"
                    << "Will try to continue.\n";

          @=$$@> = ZERO_REAL;
            
          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");

        } /* |if (@=$3@> == 0)|  */

      else
        @=$$@> = @=$1@> / @=$3@>;
    }
  else
    {
      int i = @=$2@>;

      cerr_strm << thread_name 
                << "ERROR! In `yyparse()': Invalid value for `times_or_over': "
                << i << ".\nSetting <numeric_secondary> to 0. "
                << "Will try to continue.\n";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |else|  */  
  
#if DEBUG_COMPILE
  if (DEBUG)
     {
        cerr_strm << thread_name << "`$$' == " 
                  << @=$$@> << ".";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm, error_stop_value);
        cerr_strm.str("");

     }
#endif /* |DEBUG_COMPILE|  */ 

};

@q ***** (5) numeric_secondary --> numeric_secondary @>
@q ***** (5) POW numeric_primary.                    @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\.{POW} \�numeric primary>. 
\initials{LDF 2022.07.10.}

\LOG
\initials{LDF 2022.07.10.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: numeric_secondary POW numeric_primary@>
{
     @=$$@> = powf(@=$1@>, @=$3@>);  
};

@q ***** (5) numeric_secondary --> numeric_secondary @>
@q ***** (5) ROOT numeric_primary.                   @>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\.{ROOT} \�numeric primary>. 
\initials{LDF 2022.07.10.}

\LOG
\initials{LDF 2022.07.10.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: numeric_secondary ROOT numeric_primary@>
{
     @=$$@> = powf(@=$1@>, 1.0F/@=$3@>);  
};

@q ***** (5) numeric_secondary --> point_secondary @>
@q ***** (5) DOT_PRODUCT point_primary.            @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{DOT\_PRODUCT} \�point primary>. 
\initials{LDF 2004.10.05.}

\LOG
\initials{LDF 2004.10.05.}  
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary DOT_PRODUCT point_primary@>
{

  Point* p = static_cast<Point*>(@=$1@>);
  
  Point* q = static_cast<Point*>(@=$3@>);  

  real r;

        r = p->dot_product(*q);  

@q ******* (7) |Point::dot_product| succeeded.@> 
@ |Point::dot_product| succeeded.
Set value of rule to |r|, delete |p| and |q|, and exit rule.
\initials{LDF 2004.10.05.}

@<Define rules@>=

  delete p;
  delete q;

  @=$$@> = r;

};

@q ***** (5) numeric_secondary --> point_secondary ANGLE point_primary.  @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{ANGLE} \�point>.
\initials{LDF 2004.05.05.}  

This rule doesn't correspond to a rule in Metafont.  It replaces the rule
``\�numeric primary> $\longrightarrow$ \.{ANGLE} \�point primary>''.  
The latter wouldn't make sense in 3DLDF, because 
we can't simply use the x-axis as our reference for angles.
The function |Point::angle| requires two operands, so this rule is at the
secondary rather than the primary level.
\initials{LDF 2004.05.05.}

\LOG
\initials{LDF 2004.05.05.}  
Added this rule.

\initials{LDF 2004.10.01.}
Revised this rule.  Removed the |goto| statements and made the
debugging and error output thread-safe.
\ENDLOG 

@q ****** (6) Definition.@> 

@<Define rules@>= 
@=numeric_secondary: point_secondary ANGLE point_primary@>
{
    Point* p = static_cast<Point*>(@=$1@>); 
    Point* q = static_cast<Point*>(@=$3@>); 

@q ****** (6)  Error handling:  |p == 0 || q == 0|.  @>

@ Error handling.  |p == 0 || q == 0|.
\initials{LDF 2004.10.01.}

@<Define rules@>=

    if (p == static_cast<Point*>(0) || q == static_cast<Point*>(0))
      {

        @=$$@> = INVALID_REAL;

      }  /* |if (p == 0 || q == 0)|  */@;

@q ******* (7)  Error handling.  |*p == *q|.  @>

@ Error handling.  |*p == *q|.
\initials{LDF 2004.10.01.}

@<Define rules@>=

    else if (*p == *q)
      {

        @=$$@> = INVALID_REAL;

      }  /* |if (*p == *q)|  */@;

@q ******* (7) Success! @>

@ Success!
\initials{LDF 2004.10.01.}

@<Define rules@>=
  
  else /* |p| and |q| point to valid and non-congruent |Points|.  */
                            
    @=$$@> = p->angle(*q);

@q ******* (7) Delete |p| and |q| and exit rule.@>

@ Delete |p| and |q| and exit rule.
\initials{LDF 2004.10.01.}

@<Define rules@>=

  delete p;
  
  delete q;

};

@q ***** (5) numeric_secondary --> point_secondary LOCATION              @>
@q ***** (5) ellipse_primary with_test_optional with_tolerance_optional. @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�ellipse primary> \�with test optional>. 
\initials{LDF 2004.10.23.}

\LOG
\initials{LDF 2004.10.23.}  
Added this rule.

\initials{LDF 2005.10.24.}
Changed |ellipse_like_primary| to |ellipse_primary|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION ellipse_primary with_test_optional with_tolerance_optional@>
{

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_secondary: point_secondary "
                 << "LOCATION ellipse_primary with_test_optional with_tolerance_optional'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (scanner_node->variable_entry != 0)
          cerr << "scanner_node->variable_entry is non-NULL." << endl;
       else
          cerr << "scanner_node->variable_entry is NULL." << endl;

  }
#endif /* |DEBUG_COMPILE|  */

   Point* p   = static_cast<Point*>(@=$1@>);
   Ellipse* e = static_cast<Ellipse*>(@=$3@>);  

   real do_test = true;
   real tolerance = @=$5@>;  /* Default value for \�with tolerance optional> 
                                is $-1$, which is the same as the default value 
                                of the |tolerance| parameter of |Ellipse::location|.
                                \initials{LDF 2022.12.24.}
                             */

   if (@=$4@> == WITH_NO_TEST)
   {
      do_test = false;
   }

   @=$$@> = e->location(*p, tolerance, scanner_node, do_test);

   delete p;
   delete e;   

};

@q ***** (5) numeric_secondary --> point_secondary LOCATION  @>
@q ***** (5) superellipse_primary call_metapost_option_list. @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�superellipse primary> \�call metapost option list>. 
\initials{LDF 2022.05.28.}

\LOG
\initials{LDF 2022.05.28.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION superellipse_primary call_metapost_option_list@>
{
@q ******* (7) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      {
        cerr_strm << thread_name 
                  << "*** Parser: `numeric_secondary: point_secondary LOCATION "
                  << "superellipse_primary call_metapost_option_list'.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @>

   bool save               = @=$4@> & 1U;
   bool clear              = @=$4@> & 2U;
   bool suppress_mp_stdout = @=$4@> & 4U;
   bool do_transform       = @=$4@> & 8U;

   Point* p   = static_cast<Point*>(@=$1@>);
   Superellipse* s = static_cast<Superellipse*>(@=$3@>);  

   real offset;
   Point closest_pt;

   status = static_cast<real>(s->location(*p, 
                              true,  /* |do_test|  */
                              do_transform, 
                              save, 
                              &offset, 
                              &closest_pt, 
                              0,    /* |Transform*|     */
                              0,    /* |Superellipse*|  */
                              scanner_node));
 
   if (status < -2)
   {

      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `numeric_secondary: point_secondary LOCATION "
                << "superellipse_primary call_metapost_option_list':"
                << endl 
                << "`Superellipse::location' failed, returning " << status << "."
                << endl 
                << "Failed to find location of point on superellipse." << endl 
                << "Setting `numeric_secondary' to `INVALID_REAL' and continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = INVALID_REAL;
   }
   else
   {
      @=$$@> = status;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr_strm << thread_name 
                   << "In parser, rule `numeric_secondary: point_secondary LOCATION "
                   << "superellipse_primary call_metapost_option_list':"
                   << endl 
                   << "`Superellipse::location' succeeded, returning " << status << "."
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         if (status == -2) /* Point isn't coplanar with superellipse  */ 
         {

         }  
         else if (status == 0)  /* Point lies on perimeter of the superellipse or within a given tolerance  */ 
         {

         }
         else if (status == 1) /* Point lies within the perimeter of the superellipse  */ 
         {
   
         }
         else if (status == -1) /* Point lies outside the perimeter of the superellipse  */ 
         {

         }  

      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }  /* |else| */

   delete p;
   delete s;   

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 
};

@q ***** (5) numeric_secondary --> point_secondary @>
@q ***** (5) LOCATION circle_primary.              @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�circle primary>. 
\initials{LDF 2004.10.23.}

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION circle_primary@>
{

   Point* p   = static_cast<Point*>(@=$1@>);
   Circle* e = static_cast<Circle*>(@=$3@>);  

   @=$$@> = e->location(*p);

   delete p;
   delete e;   

};

@q ***** (5) numeric_secondary --> point_secondary        @>
@q ***** (5) LOCATION parabola_primary.                    @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�parabola primary>. 
\initials{LDF 2005.11.21.}

\LOG
\initials{LDF 2005.11.21.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION parabola_primary@>
{

   Point* p   = static_cast<Point*>(@=$1@>);
   Parabola* e = static_cast<Parabola*>(@=$3@>);  
  
   if (!(p && e))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = e->location(*p);

   delete p;
   delete e;   

};

@q ***** (5) numeric_secondary --> point_secondary        @>
@q ***** (5) LOCATION hyperbola_primary.                  @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�hyperbola primary>. 
\initials{LDF 2005.11.28.}

\LOG
\initials{LDF 2005.11.28.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION hyperbola_primary@>
{

   Point* p     = static_cast<Point*>(@=$1@>);
   Hyperbola* e = static_cast<Hyperbola*>(@=$3@>);  
  
   if (!(p && e))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = e->location(*p);

   delete p;
   delete e;   

};

@q ***** (5) numeric_secondary --> point_secondary  @>
@q ***** (5) LOCATION ellipsoid_primary.            @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�ellipsoid primary>. 
\initials{LDF 2005.12.07.}

\LOG
\initials{LDF 2005.12.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION ellipsoid_primary@>
{

   Point* p     = static_cast<Point*>(@=$1@>);
   Ellipsoid* e = static_cast<Ellipsoid*>(@=$3@>);  
  
   if (!(p && e))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = e->location(*p, static_cast<Scanner_Node>(parameter));

   delete p;
   delete e;   

};

@q ***** (5) numeric_secondary --> point_secondary  @>
@q ***** (5) LOCATION sphere_primary.               @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�sphere primary>. 
\initials{LDF 2005.12.07.}

\LOG
\initials{LDF 2005.12.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION sphere_primary@>
{

   Point* p     = static_cast<Point*>(@=$1@>);
   Sphere* s = static_cast<Sphere*>(@=$3@>);  
  
   if (!(p && s))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = s->location(*p);

   delete p;
   delete s;   

};

@q ***** (5) numeric_secondary --> point_secondary  @>
@q ***** (5) LOCATION cuboid_primary.               @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�cuboid primary>. 
\initials{LDF 2005.12.09.}

\LOG
\initials{LDF 2005.12.09.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION cuboid_primary@>
{

   Point*  p = static_cast<Point*>(@=$1@>);
   Cuboid* c = static_cast<Cuboid*>(@=$3@>);  
  
   if (!(p && c))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = c->location(*p);

   delete p;
   delete c;   

};

@q ***** (5) numeric_secondary --> point_secondary LOCATION cone_primary.@>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�cone primary>. 
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION cone_primary@>
{

   Point* p     = static_cast<Point*>(@=$1@>);
   Cone* c  = static_cast<Cone*>(@=$3@>);  
  
#if 0 
   if (!(p && c))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = c->location(*p, static_cast<Scanner_Node>(parameter));

   delete p;
   delete c;   
#else
   @=$$@> = INVALID_REAL;
#endif 

};

@q ***** (5) numeric_secondary --> point_secondary  @>
@q ***** (5) LOCATION cylinder_primary.            @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�cylinder primary>. 
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION cylinder_primary@>
{

   Point* p     = static_cast<Point*>(@=$1@>);
   Cylinder* c  = static_cast<Cylinder*>(@=$3@>);  
  
   if (!(p && c))
      @=$$@> = INVALID_REAL;
   else
      @=$$@> = c->location(*p, static_cast<Scanner_Node>(parameter));

   delete p;
   delete c;   

};

@q ***** (5) numeric_secondary --> point_secondary @>
@q ***** (5) LOCATION reg_polygon_primary.              @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{LOCATION} \�reg polygon primary>. 
\initials{LDF 2024.04.10.}

\LOG
\initials{LDF 2024.04.10.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary LOCATION reg_polygon_primary@>
{
@q ******* (7) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_secondary: point_secondary "
                 << "LOCATION reg_polygon_primary'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (scanner_node->variable_entry != 0)
          cerr << "scanner_node->variable_entry is non-NULL." << endl;
       else
          cerr << "scanner_node->variable_entry is NULL." << endl;

  }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @>

   Point* p       = static_cast<Point*>(@=$1@>);
   Reg_Polygon* r = static_cast<Reg_Polygon*>(@=$3@>);  

   status = r->location(p, r, scanner_node);

#if DEBUG_COMPILE
  if (DEBUG) 
  {
       cerr_strm << thread_name << "In parser, rule `numeric_secondary: point_secondary "
                 << "LOCATION reg_polygon_primary':"
                 << endl 
                 << "`Reg_Polygon::location' returned `status' == "
                 << status << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @>

   @=$$@> = status;

   delete p;
   delete r;   

   p = 0;
   r = 0;

};


@q ***** (5) numeric_secondary --> point_secondary @>
@q ***** (5) DISTANCE_TO_PLANE path_primary.       @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{DISTANCE\_TO\_PLANE} \�path primary>. 
\initials{LDF 2004.10.23.}

\LOG
\initials{LDF 2004.10.23.}  
Added this rule.

\initials{LDF 2005.10.24.}
Changed |path_like_primary| to |path_primary|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary DISTANCE_TO_PLANE path_primary@>@/
{

   Point* p   = static_cast<Point*>(@=$1@>);
   Path*  q = static_cast<Path*>(@=$3@>);  

@q ******* (7) Error handling:  |*q| is non-planar.@>    

@ Error handling:  |*q| is non-planar.
\initials{LDF 2004.10.23.}  

@<Define rules@>=

   if (!q->is_planar())
      {
  
          @=$$@> = INVALID_REAL;
  
      } /* |if (!q->is_planar())|  */

@q ******* (7) |*q| is planar.@> 

@ |*q| is planar.
\initials{LDF 2004.10.23.}

@<Define rules@>=

   else /* |q->is_planar() == true|  */
      {
         Plane pplane = q->get_plane();

         Real_Short rs = pplane.get_distance(*p, static_cast<Scanner_Node>(parameter));

         @=$$@> = rs.first * rs.second;

      } /* |else| (|q->is_planar() == true|)  */

@q ******* (7) Delete |p| and |q| and exit rule.@> 

@ Delete |p| and |q| and exit rule.
\initials{LDF 2004.10.23.}

@<Define rules@>=

   delete p;
   delete q;   

};

@q ***** (5) numeric_secondary --> point_secondary @>
@q ***** (5) DISTANCE_ALONG_LINE path_primary.     @>

@*4 \�numeric secondary> $\longrightarrow$ \�point secondary> 
\.{DISTANCE\_ALONG\_LINE} \�path primary>. 
\initials{LDF 2004.10.25.}

\LOG
\initials{LDF 2004.10.25.}  
Added this rule.

\initials{LDF 2005.10.24.}
Changed |path_like_primary| to |path_primary|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: point_secondary DISTANCE_ALONG_LINE path_primary@>
{

   Point* p   = static_cast<Point*>(@=$1@>);
   Path*  q   = static_cast<Path*>(@=$3@>);  

   if (q && p && q->is_linear())
     {

         Bool_Real br = p->is_on_segment(q->get_point(0), q->get_last_point());

         @=$$@> = br.second;

     }  /* |if (q && p && q->is_linear())|  */

   else /* |!(q && p && q->is_linear())|  */
      {

          @=$$@> = INVALID_REAL;

      } /*  |else| (|!(q && p && q->is_linear())|)  */

@q ******* (7) Delete |p| and |q| and exit rule.@> 

@ Delete |p| and |q| and exit rule.
\initials{LDF 2004.10.25.}

@<Define rules@>=

   delete p;
   delete q;   

};

@q ***** (5) numeric_secondary --> numeric_secondary MODULUS numeric_primary.@>

@*4 \�numeric secondary> $\longrightarrow$ \�numeric secondary> 
\.{MODULUS} \�numeric primary>. 
\initials{LDF 2021.10.22.}

\LOG
\initials{LDF 2021.10.22.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_secondary: numeric_secondary MODULUS numeric_primary@>
{
   @=$$@> = fmod(@=$1@>, @=$3@>);

};

@q **** (4) times_or_over.  @>
@ \�times or over>. 

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> times_or_over@>@/

@q ***** (5) TIMES.  @>

@ \.{TIMES}.
@<Define rules@>= 
@=times_or_over: TIMES@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: times_or_over (TIMES)." << endl;
#endif /* |DEBUG_COMPILE|  */
  
  @=$$@> = TIMES;

}
;

@q ***** (5) OVER.  @>

@ \.{OVER}.
@<Define rules@>= 
@=times_or_over: OVER@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: times_or_over (OVER)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = OVER;

}
;

@q **** (4) Numeric tertiary.  @>
@ \�numeric tertiary>.

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_tertiary@>@/

@q ***** (5) numeric_secondary@>

@ \�numeric secondary>.
@<Define rules@>= 
@=numeric_tertiary: numeric_secondary@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_tertiary (numeric_secondary)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = @=$1@>;
  
#if DEBUG_COMPILE
  if (DEBUG)
    cerr << "$$ == " << @=$$@> << endl;
#endif /* |DEBUG_COMPILE|  */ 

};

@q ***** (5) numeric_tertiary --> numeric_tertiary @>
@q ***** (5) PLUS numeric_secondary.               @>

@ \�numeric tertiary> $\longrightarrow$ \�numeric tertiary> 
\.{PLUS} \�numeric secondary>.

\LOG
\initials{LDF 2004.04.27.}  
Added this rule.

\initials{LDF 2005.01.06.}
Changed ``\�plus or minus>'' to \.{PLUS}.
Made debugging output thread-safe.
\ENDLOG 

@<Define rules@>= 
@=numeric_tertiary: numeric_tertiary PLUS numeric_secondary@>
{

   @=$$@> = @=$1@> + @=$3@>;

};

@q ***** (5) numeric_tertiary --> numeric_tertiary @>
@q ***** (5) MINUS numeric_secondary.              @>

@ \�numeric tertiary> $\longrightarrow$ \�numeric tertiary> 
\.{MINUS} \�numeric secondary>.
\initials{LDF 2005.01.06.}

\LOG
\initials{LDF 2005.01.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_tertiary: numeric_tertiary MINUS numeric_secondary@>
{

   @=$$@> = @=$1@> - @=$3@>;

};

@q ***** (5) numeric_tertiary pythagorean_plus_or_minus numeric_secondary@>

@ \�numeric tertiary> \�Pythagorean plus or minus> \�numeric secondary>.

\LOG
\initials{LDF 2004.04.27.}  Added this rule.

\initials{LDF 2004.04.28.}  Added error handling and got Pythagorean 
subtraction to work.  
  
\ENDLOG 
@q ****** (6) Definition.  @>
@<Define rules@>= 
@=numeric_tertiary: numeric_tertiary pythagorean_plus_or_minus numeric_secondary@>
{

    @<Common declarations for rules@>@; 

  real r0;
  real r1;

  if (@=$1@> > MAX_REAL_SQRT)
    {

      cerr << "ERROR! In yyparse().\n"
           << "Rule `numeric_tertiary: numeric_tertiary "
           << "pythagorean_plus_or_minus" << endl 
           << "numeric_secondary'." << endl
           << "($1 == " << @=$1@> << ") > MAX_REAL_SQRT" 
           << "Can't square it."
           << endl << "Setting `numeric_tertiary' to 0 "
           << "and will try to continue.\n";

      @=$$@> = ZERO_REAL;
      goto end_numeric_tertiary_2;
    }

  else if (@=$2@> > MAX_REAL_SQRT)
    {
      cerr << "ERROR! In yyparse().\n"
           << "Rule `numeric_tertiary: numeric_tertiary "
           << "pythagorean_plus_or_minus" << endl 
           << "numeric_secondary'." << endl
           << "($2 == " << @=$2@> << ") > MAX_REAL_SQRT" 
           << "Can't square it."
           << endl << "Setting `numeric_tertiary' to 0 "
           << "and will try to continue.\n";

      @=$$@> = ZERO_REAL;
      goto end_numeric_tertiary_2;
    }

  r0 = @=$1@> * @=$1@>;
  r1 = @=$3@> * @=$3@>;

@q ****** (6) Pythagorean plus.  @>
@ Pythagorean plus.
\initials{LDF 2004.05.02.}
@<Define rules@>= 
  if (@=$2@> ==  1) 
    {
      if (MAX_REAL - r0 < r1)
        {
          cerr << "ERROR! In yyparse().\n"
               << "Rule `numeric_tertiary: numeric_tertiary "
               << "pythagorean_plus_or_minus" << endl 
               << "numeric_secondary'." << endl
               << r0 << " + " << r1 << " > MAX_REAL" 
               << "Can't perform Pythagorean addition."
               << endl << "Setting `numeric_tertiary' to 0 "
               << "and will try to continue.\n";

          @=$$@> = ZERO_REAL;

          if (scanner_node->run_state.error_stop_mode == Run_State::STOPPING)
            {
              cerr << "Type <RETURN> to continue.\n";

              getchar();  /* Don't delete this!  */@;
            } 

          goto end_numeric_tertiary_2;
        }
      else  
        @=$$@> = sqrt(r0 + r1);
    }
@q ****** (6) Pythagorean minus.  @>
@ Pythagorean minus.
\initials{LDF 2004.05.02.}

\LOG
\initials{LDF 2004.05.02.}  Added error handling for the case that $r_0 < r_1$.  
\ENDLOG 
@<Define rules@>= 
  else if (@=$2@> ==  -1)  
    {

      if (r0 < r1)
        {
          cerr << "ERROR! In yyparse().\n"
               << "Rule `numeric_tertiary: numeric_tertiary "
               << "pythagorean_plus_or_minus" << endl 
               << "numeric_secondary'." << endl
               << r0 << " < " << r1 << ". "
               << "Can't perform Pythagorean subtraction."
               << endl << "Setting `numeric_tertiary' to 0 "
               << "and will try to continue.\n";

          @=$$@> = ZERO_REAL;

          if (scanner_node->run_state.error_stop_mode == Run_State::STOPPING)
            {
              cerr << "Type <RETURN> to continue.\n";
              getchar();  /* Don't delete this!  */@;
            } 

          goto end_numeric_tertiary_2;
        }

      @=$$@> = sqrt(r0 - r1);

    } /* |else if (@=$2@> ==  -1)|   */

@q ****** (6) Error handling.  @>
@ Error handling.
\initials{LDF 2004.05.02.}
@<Define rules@>=
  else
    {
      cerr << "ERROR! In yyparse().\n"
           << "Rule `numeric_tertiary: numeric_tertiary "
           << "pythagorean_plus_or_minus" << endl 
           << "numeric_secondary'." << endl
           << "$2 has invalid value: " << @=$2@>
           << endl << "Setting `numeric_tertiary' to 0 "
           << "and will try to continue.\n";

      @=$$@> = ZERO_REAL;

      goto end_numeric_tertiary_2;
    }

@ End.
\initials{LDF 2004.05.02.}
@<Define rules@>=
#if DEBUG_COMPILE
  if (DEBUG)
    {
      cerr << "$$ == " << @=$$@> << endl;
    }
#endif /* |DEBUG_COMPILE|  */ 
 end_numeric_tertiary_2:
  ; /* Do nothing.  */
}
;

@q **** (4) pythagorean_plus_or_minus.  @>
@ \�Pythagorean plus or minus>.

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> pythagorean_plus_or_minus@>@/

@q ***** (5) PYTHAGOREAN_PLUS.  @>

@
@<Define rules@>= 
@=pythagorean_plus_or_minus: PYTHAGOREAN_PLUS@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: pythagorean_plus_or_minus (PYTHAGOREAN_PLUS)." << endl;
#endif /* |DEBUG_COMPILE|  */
  @=$$@> = 1;
}  
;

@q ***** (5) PYTHAGOREAN_MINUS.  @>

@
@<Define rules@>= 
@=pythagorean_plus_or_minus: PYTHAGOREAN_MINUS@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: pythagorean_plus_or_minus (PYTHAGOREAN_MINUS)." << endl;
#endif /* |DEBUG_COMPILE|  */
  @=$$@> = -1;

};

@q **** (4) numeric_expression.  @>
@ \�numeric expression>.

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_expression@>@/

@
@<Define rules@>= 
@=numeric_expression: numeric_tertiary@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_expression (numeric_tertiary)." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = @=$1@>;
  
#if DEBUG_COMPILE
  if (DEBUG)
    cerr << "$$ == " << @=$$@> << endl;
#endif /* |DEBUG_COMPILE|  */ 

};

@q **** (4) MAX.  @>
@
@<Define rules@>= 
@=numeric_expression: MAX LEFT_PARENTHESIS numeric_primary COMMA numeric_secondary RIGHT_PARENTHESIS@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_expression: MAX LEFT_PARENTHESIS numeric_primary" << endl 
           << "COMMA numeric_secondary RIGHT_PARENTHESIS." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = max(@=$3@>, @=$5@>);
  
};

@q **** (4) MIN.  @>
@
@<Define rules@>= 
@=numeric_expression: MIN LEFT_PARENTHESIS numeric_primary COMMA numeric_secondary RIGHT_PARENTHESIS@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_expression: MIN LEFT_PARENTHESIS numeric_primary" << endl 
           << "COMMA numeric_secondary RIGHT_PARENTHESIS." << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = min(@=$3@>, @=$5@>);
  
};

@q ** (2) numeric_primary: MEDIATE LEFT_PARENTHESIS numeric_expression @>
@q ** (2) COMMA numeric_expression RIGHT_PARENTHESIS.                  @>

@ \�numeric primary> $\longrightarrow$ \.{MEDIATE} 
\.{LEFT\_PARENTHESIS} \�numeric expression> \.{COMMA} 
\�numeric expression> \.{RIGHT\_PARENTHESIS}.                                       

\LOG
\initials{LDF 2022.04.12.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: MEDIATE LEFT_PARENTHESIS numeric_expression @>
@=COMMA numeric_expression RIGHT_PARENTHESIS@>
{
@q **** (4) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      {
        cerr_strm << thread_name << "*** Parser: "
                  << "`numeric_primary: MEDIATE LEFT_PARENTHESIS"
                  << endl 
                  << "numeric_expression COMMA numeric_expression RIGHT_PARENTHESIS'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */

@q **** (4) @>

   real r0 = @=$3@>;
   real r1 = @=$5@>; 

   real result = 0.0;

@q **** (4) @>

   if ((r0 >= 0 && r1 >= 0) || (r0 < 0 && r1 <= 0))
   {
#if LDF_REAL_FLOAT
      if (MAX_REAL - fabsf(r0) <= fabsf(r1))
      {
          result = INVALID_REAL;
      }  
#else
      if (MAX_REAL - fabs(r0) <= fabs(r1))
      {
          result = INVALID_REAL;
      }  
#endif     
   }
  
@q **** (4) @>

   if (result == INVALID_REAL)
   {
       cerr << "ERROR!  In parser, rule `numeric_primary: MEDIATE "
            << "LEFT_PARENTHESIS numeric_expression"
            << endl 
            << "COMMA numeric_expression RIGHT_PARENTHESIS':"
            << endl
            << "The sum of the first two `numeric_expressions' >= `MAX_REAL'."
            << endl
            << "Can't perform the mediation operation."
            << endl 
            << "Set result to `INVALID_REAL' and will continue."
            << endl;
   }

@q **** (4) @>

   else
   {
      result = r0 + ((r1 - r0) * .5);
   
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "In parser, rule `numeric_primary: MEDIATE "
               << "LEFT_PARENTHESIS numeric_expression"
               << endl 
               << "COMMA numeric_expression RIGHT_PARENTHESIS':"
               << endl
               << "`result' == " << result
               << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 
  
   } /* |else| */

@q **** (4) @>

   @=$$@> = result;

@q **** (4) @>   

};

@q ** (2) numeric_primary: MEDIATE LEFT_PARENTHESIS numeric_expression @>
@q ** (2) COMMA numeric_expression COMMA numeric_expression            @>
@q ** (2) RIGHT_PARENTHESIS.                                           @>

@ \�numeric primary> $\longrightarrow$ \.{MEDIATE} 
\.{LEFT\_PARENTHESIS} \�numeric expression> \.{COMMA} 
\�numeric expression> \.{COMMA} \�numeric expression> 
\.{RIGHT\_PARENTHESIS}.                                       

\LOG
\initials{LDF 2022.04.12.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: MEDIATE LEFT_PARENTHESIS numeric_expression @>
@=COMMA numeric_expression COMMA numeric_expression RIGHT_PARENTHESIS@>
{
@q **** (4) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      {
        cerr_strm << thread_name << "*** Parser: "
                  << "`numeric_primary: MEDIATE LEFT_PARENTHESIS"
                  << endl 
                  << "numeric_expression COMMA numeric_expression COMMA "
                  << "numeric_expression RIGHT_PARENTHESIS'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */

@q **** (4) @>

   real r0 = @=$3@>;
   real r1 = @=$5@>; 
   real r2 = @=$7@>;

   real result = 0.0;

@q **** (4) @>

   if ((r0 >= 0 && r1 >= 0) || (r0 < 0 && r1 <= 0))
   {
#if LDF_REAL_FLOAT
      if (MAX_REAL - fabsf(r0) <= fabsf(r1))
      {
          result = INVALID_REAL;
      }  
#else
      if (MAX_REAL - fabs(r0) <= fabs(r1))
      {
          result = INVALID_REAL;
      }  
#endif     
   }
  
@q **** (4) @>

   if (result == INVALID_REAL)
   {
       cerr << "ERROR!  In parser, rule `numeric_primary: MEDIATE "
            << "LEFT_PARENTHESIS numeric_expression"
            << endl 
            << "COMMA numeric_expression COMMA numeric_expression "
            << "RIGHT_PARENTHESIS':"
            << endl
            << "The sum of the first two `numeric_expressions' >= `MAX_REAL'."
            << endl
            << "Can't perform the mediation operation."
            << endl 
            << "Set result to `INVALID_REAL' and will continue."
            << endl;
   }

@q **** (4) @>

   else
   {
      result = r0 + ((r1 - r0) * r2);
   
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "In parser, rule `numeric_primary: MEDIATE "
               << "LEFT_PARENTHESIS numeric_expression"
               << endl 
               << "COMMA numeric_expression COMMA numeric_expression "
               << "RIGHT_PARENTHESIS':"
               << endl
               << "`result' == " << result
               << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 
  
   } /* |else| */

@q **** (4) @>

   @=$$@> = result;

@q **** (4) @>   

};

@q ** (2) numeric_primary: GET_ELEMENT transform_expression numeric_list. @>


@ \�numeric primary> $\longrightarrow$ \.{GET\_ELEMENT} \�transform expression> 
\�numeric list>.
\initials{LDF 2022.06.06.}

\LOG
\initials{LDF 2022.06.06.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_ELEMENT transform_expression numeric_list@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: "
               << "`numeric_primary: GET_ELEMENT transform_expression numeric_list'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Transform *t = static_cast<Transform*>(@=$2@>);
   Pointer_Vector<real> *rv = static_cast<Pointer_Vector<real>*>(@=$3@>); 

   int row;
   int col;
   real r = 0.0;

   if (t == 0)
   {
       cerr << "ERROR!  In parser, rule `numeric_primary: GET_ELEMENT transform_expression numeric_list':"
            << endl 
            << "`transform_expression' is NULL.  Can't get element.  Continuing." << endl;

       r = INVALID_REAL;
       goto END_GET_ELEMENT_RULE;
 
   }
#if 0 
   else
   {
      cerr << "`t' is non-NULL." << endl;
   }
#endif 
   
@q *** (3) @>

   if (rv != 0)
   {
#if 0 
       cerr << "`rv' is non-NULL." << endl;
#endif 

       if (rv->v.size() < 2 || rv->v.size() % 2 != 0)
       {
              cerr << "ERROR!  In parser, rule `numeric_primary: GET_ELEMENT transform_expression numeric_list':"
               << endl 
               << "The size of `numeric_list' is < 2 or not divisible by 2."  << endl 
               << "Invalid `numeric_list'.  Can't get element.  Continuing." << endl;
       } 
       else
       {

           vector<real*>::iterator iter = rv->v.begin();

#if LDF_REAL_DOUBLE
           row = round(abs(**iter++));
           col = round(abs(**iter));
#else
           row = roundf(fabs(**iter++));
           col = roundf(fabs(**iter));
#endif 

#if 0 
           cerr << "row == " << row << endl
                << "col == " << col << endl;
#endif 

           r = t->get_element(row, col);

#if 0
           cerr << "r == " << r << endl;
#endif 

       }  /* |else|  */

   }  /* |if (rv != 0)| */

   else
   {
       cerr << "ERROR!  In parser, rule `numeric_primary: GET_ELEMENT transform_expression numeric_list':"
            << endl 
            << "`numeric_list' is NULL.  Can't get element.  Continuing." << endl;
   }

@q *** (3) @>

END_GET_ELEMENT_RULE:

  if (t)
  {
     delete t;
     t = 0;
  }

  if (rv != 0)
  {
      delete rv;
      rv = 0;
  }

   @=$$@> = r;

};

@q **** (4) numeric_token.  @>
@ \�numeric token>.  

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_token@>@/

@
@<Define rules@>= 

@q ***** (5) INTEGER.  @>

@=numeric_token: INTEGER@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
    cerr << "\n*** Parser: numeric_token (INTEGER)."
         << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = static_cast<real>(@=$1@>);

#if DEBUG_COMPILE
  if (DEBUG)
    cerr << "$$ == " << @=$$@> << endl;
#endif /* |DEBUG_COMPILE|  */ 
}
;

@q ***** (5) REAL.  @>

@=numeric_token: REAL@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
    cerr << "\n*** Parser: numeric_token (REAL)."
         << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = @=$1@>;

#if DEBUG_COMPILE
  if (DEBUG)
    cerr << "$$ == " << @=$$@> << endl;
#endif /* |DEBUG_COMPILE|  */ 

};

@q *** (3) numeric_single.  @>
@*2 \�numeric single>.
`\.{\LP}' \�numeric expression> `\.{\RP}'.

\LOG
\initials{LDF 2004.04.30.}  
Added this rule.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_single@>@;

@
@<Define rules@>= 
@=numeric_single: LEFT_PARENTHESIS numeric_expression RIGHT_PARENTHESIS@>
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      cerr << "\n*** Parser: numeric_single "
           << "(LEFT_PARENTHESIS numeric_expression RIGHT_PARENTHESIS)."
           << endl;
#endif /* |DEBUG_COMPILE|  */

  @=$$@> = @=$2@>;

#if DEBUG_COMPILE
  if (DEBUG)
    cerr << "$$ == " << @=$$@> << endl;
#endif /* |DEBUG_COMPILE|  */ 

};

@q *** (3) numeric_list  @>
@*2 \�numeric list>.
\initials{LDF 2004.05.12.}  

It's not possible to define a rule for \�numeric list> without parentheses
because this causes reduce/reduce conflicts.
\initials{LDF 2004.05.12.}

\LOG
\initials{LDF 2004.05.12.}  
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_list@>
  
@q **** (4) @>   

@*5 \�numeric list> $\longrightarrow$ \.{LEFT\_PARENTHESIS} \�numeric sublist> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.

\initials{LDF 2004.09.24.}
Changed |w| from |Real_Pointer_Vector*| to |Pointer_Vector<real>*|.

\initials{LDF 2007.09.19.}
@:BUG FIX@> BUG FIX:  Now using |Pointer_Vector::operator+=| to put
pointers to |real| onto the |Pointer_Vector<real>| pointed to by |w|.
The way it was before caused |w->ctr| to be 0.

\initials{LDF 2007.11.04.}
Removed code from this rule.  Now calling 
|Scan_Parse::numeric_list_rule_func_0|, which contains the old code.
\ENDLOG

@<Define rules@>=
@=numeric_list: LEFT_PARENTHESIS numeric_sublist RIGHT_PARENTHESIS@>@/ 
{

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_list: LEFT_PARENTHESIS numeric_sublist RIGHT_PARENTHESIS'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */


   @=$$@> = numeric_list_rule_func_0(@=$2@>, parameter);

};

@q *** (3) numeric_sublist. @>

@*2 \�numeric sublist>.
\�numeric sublist>.

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_sublist@>

@q **** (4) numeric_sublist --> numeric_expression COMMA numeric_expression.@>

@ \�numeric subvector> $\longrightarrow$ \�numeric expression> 
`\.{,}' \�numeric expression>. 

\LOG
\initials{LDF 2004.05.12.}  
Added this rule.

\initials{LDF 2007.11.25.}
Changed |scanner_node->token_real_vector| to |scanner_node->numeric_list_real_vector|.
\ENDLOG

@<Define rules@>=
@=numeric_sublist: numeric_expression COMMA numeric_expression@>@/ 
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_sublist: numeric_expression COMMA numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

     scanner_node->numeric_list_real_vector.push_back(@=$1@>);
     scanner_node->numeric_list_real_vector.push_back(@=$3@>);

#if 0
   cerr << "`numeric_sublist: numeric_expression COMMA numeric_expression':"
        << endl 
        << "static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() == " 
        << static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() << endl;
#endif 

     @=$$@> = static_cast<void*>(0);

};

@q **** (4) numeric_sublist --> numeric_range.@>

@ \�numeric subvector> $\longrightarrow$ \�numeric range>. 

\LOG
\initials{LDF 2023.04.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_sublist: numeric_range@>@/ 
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_sublist: numeric_range'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

#if 0

   cerr << "`numeric_sublist: numeric_expression COMMA numeric_expression':"
        << endl 
        << "static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() == " 
        << static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() << endl;
#endif 

     @=$$@> = static_cast<void*>(0);

};

@q *** (3) numeric_sublist --> numeric_sublist COMMA numeric_expression. @>
@*2 \�numeric sublist> $\longrightarrow$ 
\�numeric sublist> \�numeric sublist> `\.{,}' \�numeric expression>. 

\LOG
\initials{LDF 2004.05.12.}  
Added this rule.

\initials{LDF 2007.11.25.}
Changed |scanner_node->token_real_vector| to |scanner_node->numeric_list_real_vector|.
\ENDLOG

@<Define rules@>=
@=numeric_sublist: numeric_sublist COMMA numeric_expression@>@/ 
{

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_sublist: numeric_sublist COMMA numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

   static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.push_back(@=$3@>);

#if 0
   cerr << "`numeric_sublist: numeric_sublist COMMA numeric_expression':"
        << endl 
        << "'static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() == " 
        << static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() << endl;
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) numeric_sublist --> numeric_sublist COMMA numeric_range. @>
@*2 \�numeric sublist> $\longrightarrow$ 
\�numeric sublist> \�numeric sublist> `\.{,}' \�numeric range>. 
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_sublist: numeric_sublist COMMA numeric_range@>@/ 
{

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_sublist: numeric_sublist COMMA numeric_range'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

#if 0
   cerr << "`numeric_sublist: numeric_sublist COMMA numeric_range':"
        << endl 
        << "'static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() == " 
        << static_cast<Scanner_Node>(parameter)->numeric_list_real_vector.size() << endl;
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) numeric_range. @>

@*2 \�numeric range>.
\�numeric range>.
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this type declaration.
\ENDLOG 
 
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_range@>

@q **** (4) numeric_range --> numeric_expression TO numeric_expression.@>

@ \�numeric range> $\longrightarrow$ \�numeric expression> 
\.{TO} \�numeric expression>. 
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_range: numeric_expression TO numeric_expression@>@/ 
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG) 
  {
       cerr_strm << thread_name << "*** Parser: `numeric_range: numeric_expression TO numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  real r0 = @=$1@>;
  real r1 = @=$3@>;

  if (r0 == r1)
     scanner_node->numeric_list_real_vector.push_back(@=$1@>);
  else if (r0 < r1)  
  {
      for (real r = r0; r <= r1; r += 1)
        scanner_node->numeric_list_real_vector.push_back(r);
  }
  else if (r0 > r1)  
  {
      for (real r = r1; r <= r0; r += 1)
        scanner_node->numeric_list_real_vector.push_back(r);
  }

  @=$$@> = static_cast<void*>(0);

};

@q ***** (5) numeric_operator --> PLUS.  @>
@*4 \�numeric operator> $\longrightarrow$ 
\.{PLUS}.
\initials{LDF Undated.}

\LOG
\initials{LDF 2007.02.06.}
Now setting the semantic value of the rule to |PLUS|.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: PLUS %prec UNARY_PLUS_OR_MINUS@>@;
{
    @=$$@> = PLUS;
};

@q ***** (5) numeric_operator --> MINUS.  @>
@*4 \�numeric operator> $\longrightarrow$ \.{MINUS}.
\initials{LDF Undated.}

\LOG
\initials{LDF 2007.02.06.}
Now setting the semantic value of the rule to |MINUS|.
\ENDLOG 

@<Define rules@>= 

@=numeric_operator: MINUS %prec UNARY_PLUS_OR_MINUS@>@;
{

  @=$$@> = MINUS;

};

@q ** (2) numeric_primary: GET_TYPE sphere_variable. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_TYPE} \�sphere variable>. 
\initials{LDF 2022.11.28.}

|Spheres| are very large objects so I prefer to avoid copying them.  Therefore
\�sphere variable> is used here instead of \�sphere primary>, \�sphere expression>,
etc.
\initials{LDF 2022.11.28.}

\LOG
\initials{LDF 2022.11.28.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_TYPE sphere_variable@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_TYPE sphere_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      Sphere *s = static_cast<Sphere*>(entry->object);
      @=$$@> = s->get_type();     
   }   
   else 
   {
      cerr_strm << thread_name << "WARNING!  In parser, rule "
                << "`numeric_primary: GET_TYPE sphere_variable':"
                << endl 
                << "`Id_Map_Entry_Node entry' or `void *entry->object' is NULL:"
                << endl 
                << "`sphere_variable' is NULL or empty.  Can't get type."
                << endl 
                << "Setting `numeric_primary' to -1 and continuing." << endl;
     
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = -1.0;
   }
};

@q ** (2) numeric_primary: GET_DIVISIONS_HORIZONTAL sphere_variable. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_DIVISIONS\_HORIZONTAL} \�sphere variable>. 
\initials{LDF 2022.11.28.}

|Spheres| are very large objects so I prefer to avoid copying them.  Therefore
\�sphere variable> is used here instead of \�sphere primary>, \�sphere expression>,
etc.
\initials{LDF 2022.11.28.}

\LOG
\initials{LDF 2022.11.28.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_DIVISIONS_HORIZONTAL sphere_variable@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_DIVISIONS_HORIZONTAL sphere_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      Sphere *s = static_cast<Sphere*>(entry->object);
      @=$$@> = s->get_divisions_horizontal();     
   }   
   else 
   {
      cerr_strm << thread_name << "WARNING!  In parser, rule "
                << "`numeric_primary: GET_DIVISIONS_HORIZONTAL sphere_variable':"
                << endl 
                << "`Id_Map_Entry_Node entry' or `void *entry->object' is NULL:"
                << endl 
                << "`sphere_variable' is NULL or empty.  Can't get divisions_horizontal."
                << endl 
                << "Setting `numeric_primary' to -1 and continuing." << endl;
     
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = -1.0;
   }
};

@q ** (2) numeric_primary: GET_DIVISIONS_VERTICAL sphere_variable. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_DIVISIONS\_VERTICAL} \�sphere variable>. 
\initials{LDF 2022.11.28.}

|Spheres| are very large objects so I prefer to avoid copying them.  Therefore
\�sphere variable> is used here instead of \�sphere primary>, \�sphere expression>,
etc.
\initials{LDF 2022.11.28.}

\LOG
\initials{LDF 2022.11.28.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_DIVISIONS_VERTICAL sphere_variable@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_DIVISIONS_VERTICAL sphere_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      Sphere *s = static_cast<Sphere*>(entry->object);
      @=$$@> = s->get_divisions_vertical();     
   }   
   else 
   {
      cerr_strm << thread_name << "WARNING!  In parser, rule "
                << "`numeric_primary: GET_DIVISIONS_VERTICAL sphere_variable':"
                << endl 
                << "`Id_Map_Entry_Node entry' or `void *entry->object' is NULL:"
                << endl 
                << "`sphere_variable' is NULL or empty.  Can't get divisions_vertical."
                << endl 
                << "Setting `numeric_primary' to -1 and continuing." << endl;
     
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = -1.0;
   }
};

@q ** (2) numeric_primary: GET_CIRCLE_POINT_COUNT sphere_variable. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_CIRCLE\_POINT\_COUNT} \�sphere variable>. 
\initials{LDF 2022.11.28.}

|Spheres| are very large objects so I prefer to avoid copying them.  Therefore
\�sphere variable> is used here instead of \�sphere primary>, \�sphere expression>,
etc.
\initials{LDF 2022.11.28.}

\LOG
\initials{LDF 2022.11.28.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_CIRCLE_POINT_COUNT sphere_variable@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_CIRCLE_POINT_COUNT sphere_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      Sphere *s = static_cast<Sphere*>(entry->object);
      @=$$@> = s->get_circle_point_count();     
   }   
   else 
   {
      cerr_strm << thread_name << "WARNING!  In parser, rule "
                << "`numeric_primary: GET_CIRCLE_POINT_COUNT sphere_variable':"
                << endl 
                << "`Id_Map_Entry_Node entry' or `void *entry->object' is NULL:"
                << endl 
                << "`sphere_variable' is NULL or empty.  Can't get circle_point_count."
                << endl 
                << "Setting `numeric_primary' to -1 and continuing." << endl;
     
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      @=$$@> = -1.0;
   }
};


@q ** (2) numeric_primary: GET_HEIGHT cuboid_primary. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_HEIGHT} \�cuboid primary>. 
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_HEIGHT cuboid_primary@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_HEIGHT cuboid_primary'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Cuboid *c = static_cast<Cuboid*>(@=$2@>);

   if (c && c->rectangles.size() > 0)
      @=$$@> = c->get_height();
   else
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_HEIGHT cuboid_primary':"
               << endl
               << "Cuboid is NULL or empty.  Can't return `Cuboid::height'." << endl
               << "Setting `numeric_primary' to `INVALID_REAL' and continuing." 
               << endl; 
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     @=$$@> = INVALID_REAL;
   }

};

@q ** (2) numeric_primary: GET_WIDTH cuboid_primary. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_WIDTH} \�cuboid primary>. 
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_WIDTH cuboid_primary@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_WIDTH cuboid_primary'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Cuboid *c = static_cast<Cuboid*>(@=$2@>);

   if (c && c->rectangles.size() > 0)
      @=$$@> = c->get_width();
   else
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_WIDTH cuboid_primary':"
               << endl
               << "Cuboid is NULL or empty.  Can't return `Cuboid::width'." << endl
               << "Setting `numeric_primary' to `INVALID_REAL' and continuing." 
               << endl; 
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     @=$$@> = INVALID_REAL;
   }

};

@q ** (2) numeric_primary: GET_DEPTH cuboid_primary. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_DEPTH} \�cuboid primary>. 
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_DEPTH cuboid_primary@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_DEPTH cuboid_primary'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Cuboid *c = static_cast<Cuboid*>(@=$2@>);

   if (c && c->rectangles.size() > 0)
      @=$$@> = c->get_depth();
   else
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_DEPTH cuboid_primary':"
               << endl
               << "Cuboid is NULL or empty.  Can't return `Cuboid::depth'." << endl
               << "Setting `numeric_primary' to `INVALID_REAL' and continuing." 
               << endl; 
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     @=$$@> = INVALID_REAL;
   }

   if (c)
   {
      delete c;
      c = 0;
   }

};

@q ** (2) numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_BRANCH\_LENGTH} numeric_expression COMMA \�hyperbola primary>. 
\initials{LDF 2023.01.30.}

\LOG
\initials{LDF 2023.01.30.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Hyperbola *h = static_cast<Hyperbola*>(@=$4@>);

   int i = static_cast<int>(@=$2@>);

   if (h)
   {
      if (i == 0)
         @=$$@> = h->Path::size();
      else if (i == 1)
         @=$$@> = h->branch_1.size();
      else
      {
           cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_BRANCH_LENGTH "
                     << "numeric_expression COMMA hyperbola_primary':"
                     << endl
                     << "`numeric_expression' is neither 0 nor 1.  Invalid value:"
                     << endl
                     << "`numeric_expression' == " << @=$2@>
                     << endl 
                     << "Setting value of rule (`numeric_primary') to `INVALID_REAL' and continuing." 
                     << endl; 
     
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           @=$$@> = INVALID_REAL;
      }
   }
   else
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_BRANCH_LENGTH "
               << "numeric_expression COMMA hyperbola_primary':"
               << endl
               << "Hyperbola is NULL or empty.  Can't return `Hyperbola::branch_length'." << endl
               << "Setting value of rule (`numeric_primary') to `INVALID_REAL' and continuing." 
               << endl; 
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     @=$$@> = INVALID_REAL;
   }

};

@q * (1) @>
@
@<Define rules@>=
@=numeric_primary: SHIFT_DECIMAL_POINT numeric_expression BY numeric_expression with_direction_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: SHIFT_DECIMAL_POINT "
               << "numeric_expression BY numeric_expression with_direction_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   real s;      

   status = shift_decimal_point(&s, @=$2@>, @=$4@>, @=$5@>, scanner_node);

   if (status != 0)
   {
     cerr_strm << "ERROR!  In parser, rule `numeric_primary: SHIFT_DECIMAL_POINT"
               << endl 
               << "numeric_expression with_direction_optional':"
               << endl   
               << "`shift_decimal_point' failed, returning " << status << "."
               << endl 
               << "Failed to shift decimal point." << endl 
               << "Will try to continue.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }

@q ** (2) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << "In parser, rule `numeric_primary: SHIFT_DECIMAL_POINT"
               << endl 
               << "numeric_expression with_direction_optional':"
               << endl  
               << "`shift_decimal_point' succeeded, returning 0."
               << endl;
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = s;

};

@q * (1) numeric_primary: GET_ANGLE numeric_primary polyhedron_expression  @>
@ \�numeric primary> $\longrightarrow$ \.{GET\_ANGLE} \�numeric primary> 
\�polyhedron expression>.
\initials{LDF 2023.12.29.}

\LOG
\initials{LDF 2023.12.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_ANGLE numeric_primary polyhedron_expression @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: "
               << "GET_ANGLE numeric_primary polyhedron_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Polyhedron *p = static_cast<Polyhedron*>(@=$3@>);
   real r = 0.0;


   if (p)
   {
#if 0 
      cerr << "p is non-NULL." << endl;

      cerr << "p->shape_type == " << p->shape_type << " == " 
           << Shape::type_name_map[p->shape_type] 
           << endl;
#endif 

      if (p->shape_type = Shape::PENTAGONAL_HEXECONTAHEDRON_TYPE)
      {

#if 0 
         cerr << "static_cast<Pentagonal_Hexecontahedron*>(p)->angle_0 == " 
              << static_cast<Pentagonal_Hexecontahedron*>(p)->angle_0 << endl;

         cerr << "Pentagonal_Hexecontahedron::angle_0 == " 
              << Pentagonal_Hexecontahedron::angle_0 << endl;
#endif 
         if (@=$2@> == 0.0)
            r = Pentagonal_Hexecontahedron::angle_0;
         else if (@=$2@> == 1.0)
            r = Pentagonal_Hexecontahedron::angle_1;
         else
         {
             cerr_strm << thread_name << "WARNING!  In parser, rule `numeric_primary:"
                       << endl 
                       << "GET_ANGLE numeric_primary polyhedron_expression':"
                       << endl
                       << "Invalid value for $2 (`numeric_primary'):  " << @=$2@>
                       << endl 
                       << "Value must be 0 or 1.  Setting $$ (`numeric_primary' to `INVALID_REAL'."
                       << endl
                       << "Will try to continue.";
     
             log_message(cerr_strm);
             cerr_message(cerr_strm);
             cerr_strm.str("");

             r = INVALID_REAL;  
         }

#if 0 
         cerr << "r == " << r << endl;
#endif 

         @=$$@> = r;

      }
      else
      {
     
         cerr << "In parser, rule `numeric_primary: GET_ANGLE numeric_primary "
              << "polyhedron_expression':"
              << endl 
              << "This case hasn't been programmed.  Setting $$ to 0.0 and continuing." 
              << endl;

         @=$$@> = 0.0;
      }
   }

   else
   {
      cerr << "In parser, rule `numeric_primary:"
             << endl 
             << "`Polyhedron *p' is NULL.  Setting $$ to `INVALID_REAL' and continuing." 
             << endl;

      @=$$@> = INVALID_REAL;
   }

   delete p;
   p = 0;

};


@q * (1) numeric_primary: GET_TRIANGLE_RADIUS polyhedron_variable  @>
@ \�numeric primary> $\longrightarrow$ \.{GET\_TRIANGLE\_RADIUS} 
\�polyhedron variable>.
\initials{LDF 2024.05.04.}

\LOG
\initials{LDF 2024.05.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_TRIANGLE_RADIUS polyhedron_variable @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: "
               << "GET_TRIANGLE_RADIUS polyhedron_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Polyhedron *p = 0;
   real r = 0.0;

   if (entry && entry->object)
   {
      p = static_cast<Polyhedron*>(entry->object);

      if (p->shape_type == Shape::TETRAHEDRON_TYPE)
      {
         r = static_cast<Tetrahedron*>(p)->Tetrahedron::get_triangle_radius();

         @=$$@> = r;
      }

      else
      {
     
         cerr << "In parser, rule `numeric_primary: GET_TRIANGLE_RADIUS polyhedron_variable':"
              << endl 
              << "p->shape_type == " << p->shape_type << " == " << type_name_map[p->shape_type]
              << endl 
              << "p->shape_type != Shape::TETRAHEDRON_TYPE" << endl 
              << "This case hasn't been programmed.  Setting `$$' to 0.0 and will try to continue." 
              << endl;

         @=$$@> = 0.0;
      }
   }

   else
   {
      cerr << "In parser, rule `numeric_primary:  GET_TRIANGLE_RADIUS polyhedron_variable':"
             << endl 
             << "`polyhedron_variable' is NULL.  Setting $$ to `INVALID_REAL' and continuing." 
             << endl;

      @=$$@> = INVALID_REAL;
   }

};

@q * (1) numeric_primary: GET_DIHEDRAL_ANGLE polyhedron_variable  @>
@ \�numeric primary> $\longrightarrow$ \.{GET\_DIHEDRAL\_ANGLE} 
\�polyhedron variable>.
\initials{LDF 2023.12.30.}

\LOG
\initials{LDF 2023.12.30.}
Added this rule.

\initials{LDF 2024.05.04.}
Changed \�polyhedron expression> to \�polyhedron variable>.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_DIHEDRAL_ANGLE polyhedron_variable @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: "
               << "GET_DIHEDRAL_ANGLE polyhedron_variable'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Polyhedron *p = 0;
   real r = 0.0;

   if (entry && entry->object)
   {
      p = static_cast<Polyhedron*>(entry->object);

      if (p->shape_type == Shape::TETRAHEDRON_TYPE)
      {

         r = Tetrahedron::get_dihedral_angle();

         @=$$@> = r;

      }

      else
      {
     
         cerr << "In parser, rule `numeric_primary: GET_DIHEDRAL_ANGLE polyhedron_variable':"
              << endl 
              << "p->shape_type == " << p->shape_type << " == " << type_name_map[p->shape_type]
              << endl 
              << "p->shape_type != Shape::TETRAHEDRON_TYPE" << endl 
              << "This case hasn't been programmed.  Setting `$$' to 0.0 and will try to continue." 
              << endl;

         @=$$@> = 0.0;
      }
   }

   else
   {
      cerr << "In parser, rule `numeric_primary:  GET_DIHEDRAL_ANGLE polyhedron_variable':"
             << endl 
             << "`polyhedron_variable' is NULL.  Setting $$ to `INVALID_REAL' and continuing." 
             << endl;

      @=$$@> = INVALID_REAL;
   }

};



@q * (1) numeric_primary: GET_FACE_AXIS_LENGTH polyhedron_expression  @>
@ \�numeric primary> $\longrightarrow$ \.{GET\_FACE_AXIS\_LENGTH} 
\�polyhedron expression>.
\initials{LDF 2023.12.31.}

\LOG
\initials{LDF 2023.12.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_FACE_AXIS_LENGTH numeric_primary COMMA numeric_primary @>
@=polyhedron_expression @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: "
               << "GET_FACE_AXIS_LENGTH numeric_primary COMMA numeric_primary"
               << endl 
               << "polyhedron_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Polyhedron *p = static_cast<Polyhedron*>(@=$5@>);
   real r = 0.0;


   if (p)
   {
#if 0 
      cerr << "p is non-NULL." << endl;

      cerr << "p->shape_type == " << p->shape_type << " == " 
           << Shape::type_name_map[p->shape_type] 
           << endl;
#endif 

      if (p->shape_type = Shape::PENTAGONAL_HEXECONTAHEDRON_TYPE)
      {
#if 0  
         r = Pentagonal_Hexecontahedron::face_axis_length;
#else
         r = 0;  /* !! START HERE  2023.12.31. */
#endif 

         @=$$@> = r;

      }
      else
      {
     
         cerr << "In parser, rule `numeric_primary: "
              << "GET_FACE_AXIS_LENGTH numeric_primary COMMA numeric_primary"
              << endl 
              << "polyhedron_expression':"
              << endl 
              << "This case hasn't been programmed.  Setting $$ to 0.0 and continuing." 
              << endl;

         @=$$@> = 0.0;
      }
   }

   else
   {
      cerr << "In parser, rule `numeric_primary:"
           << "GET_FACE_AXIS_LENGTH numeric_primary COMMA numeric_primary"
           << endl 
           << "polyhedron_expression':"
           << endl 
           << "`Polyhedron *p' is NULL.  Setting $$ to `INVALID_REAL' and continuing." 
           << endl;

      @=$$@> = INVALID_REAL;
   }

   delete p;
   p = 0;

};

@q * (1) numeric_primary: GET_DISTANCE point_primary TO focus_expression.@>
@ \�numeric primary> $\longrightarrow$ \.{GET\_DISTANCE} \�point primary> 
\.{TO} \�focus expression>.
\initials{LDF 2024.02.18.}

\LOG
\initials{LDF 2024.02.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_DISTANCE point_primary TO focus_expression @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: "
               << "GET_DISTANCE point_primary TO focus_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Focus *f = static_cast<Focus*>(@=$4@>);
   Point *p = static_cast<Point*>(@=$2@>);

   *p -= f->get_position();

   real r;

   r = p->magnitude();

   delete p;
   p = 0;

#if 0 
  /* As in other places, trying to delete a |Focus*| causes a segmentation error.
     Are foci meant to be persistent?  !! TODO:  Find out.
  */

   delete f;
   f = 0;
#endif 

   @=$$@> = r;

};

@q ** (2) numeric_primary --> rectangle_primary GET_ORIENTATION focus_variable with_normal_optional_A@>

@*1 \�numeric primary> $\longrightarrow$ \�rectangle primary> 
\.{GET\_ORIENTATION} \�focus variable> \�with normal optional A>.
\initials{LDF 2024.05.25.}

\LOG
\initials{LDF 2024.05.25.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: rectangle_primary GET_ORIENTATION focus_variable with_normal_optional_A@>@/
{
  @<Common declarations for rules@>@;  

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr << "*** Parser: `numeric_primary: rectangle_primary GET_ORIENTATION focus_variable "
           << "with_normal_optional_A'." 
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

   Rectangle *r = static_cast<Rectangle*>(@=$1@>);

   Focus *f = 0;

   entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

   f = static_cast<Focus*>(entry->object);

   Point *nnormal = 0;

   if (@=$4@>)
      nnormal = static_cast<Point*>(@=$4@>);

   status = r->get_orientation(f, nnormal, scanner_node);

   real ret_val = 0.0;

   if (status == 0)
     ret_val = 0.0;
   else if (status == 1)
     ret_val = 1.0;
   else
     ret_val = -1.0;

   delete r;
   r = 0;

   @=$$@> = ret_val;

};

@q ** (2) numeric_primary --> reg_polygon_primary GET_ORIENTATION focus_variable with_normal_optional_A@>

@*1 \�numeric primary> $\longrightarrow$ \�regular polygon primary> 
\.{GET\_ORIENTATION} \�focus variable> \�with normal optional A>.
\initials{LDF 2024.05.25.}

\LOG
\initials{LDF 2024.05.25.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: reg_polygon_primary GET_ORIENTATION focus_variable with_normal_optional_A@>@/
{
  @<Common declarations for rules@>@;  

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr << "*** Parser: `numeric_primary: reg_polygon_primary GET_ORIENTATION focus_variable "
           << "with_normal_optional_A'." 
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

   Reg_Polygon *rp = static_cast<Reg_Polygon*>(@=$1@>);

   Focus *f = 0;

   entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

   f = static_cast<Focus*>(entry->object);

   Point *nnormal = 0;

   if (@=$4@>)
      nnormal = static_cast<Point*>(@=$4@>);

   status = rp->get_orientation(f, nnormal, scanner_node);

   real ret_val = 0.0;

   if (status == 0)
     ret_val = 0.0;
   else if (status == 1)
     ret_val = 1.0;
   else
     ret_val = -1.0;

   delete rp;
   rp = 0;

   @=$$@> = ret_val;

};

@q * (1) with_normal_optional_A.@> 
@* \�with normal optional A>.
\initials{LDF 2024.05.26.}

\LOG
\initials{LDF 2024.05.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> with_normal_optional_A@>

@q ** (2) with_normal_optional_A --> EMPTY.@> 
@*1 \�with normal optional A> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_normal_optional_A: /* Empty.  */@>@/
{
     @=$$@> = static_cast<void*>(0); 
};

@q ** (2) with_normal_optional_A --> WITH_NORMAL point_expression.@> 
@*1 \�with normal optional A> $\longrightarrow$ 
\.{WITH\_NORMAL} \�point expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_normal_optional_A: WITH_NORMAL point_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_direction_optional@>@/

@q * (1) @>
@
@<Define rules@>=
@=with_direction_optional:  /* Empty  */@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `with_direction_optional:  /* Empty  */'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   @=$$@> = 1;  /* ``shift right'' is the default.  \initials{LDF 2023.08.30.}  */

};

@q * (1) @>
@
@<Define rules@>=
@=with_direction_optional:  WITH_RIGHT_OFFSET@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `with_direction_optional:  WITH_RIGHT_OFFSET'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   @=$$@> = 1;

};

@q * (1) @>
@
@<Define rules@>=
@=with_direction_optional:  WITH_LEFT_OFFSET@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `with_direction_optional:  WITH_LEFT_OFFSET'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   @=$$@> = 2;

};

@q ** (2) numeric_primary --> FIND string_secondary COMMA string_primary@>

@*1 \�numeric primary> $\longrightarrow$ \.{FIND}
\�string secondary> \.{COMMA} \�string primary>. 
\initials{LDF 2024.09.24.}

\LOG
\initials{LDF 2024.09.24.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: FIND string_secondary COMMA string_primary@>@/
{
  @<Common declarations for rules@>@;  

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
    cerr << "*** Parser: `FIND string_secondary COMMA string_primary'." 
         << endl;
  }
#endif /* |DEBUG_COMPILE|  */@;

  real r = 0.0;

  string *s0 = static_cast<string*>(@=$2@>);
  string *s1 = static_cast<string*>(@=$4@>);

  size_t pos = s1->find(*s0);

  if (pos == string::npos)
     r = INVALID_REAL;
  else
    r = pos;

  delete s0;
  s0 = 0;

  delete s1;
  s1 = 0;


  @=$$@> = r;

};


@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q abbrev-mode:t                      @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>
