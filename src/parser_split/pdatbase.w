@q pdatbase.w @> 
@q Created by Laurence Finston Wed 01 Sep 2021 04:36:51 PM CEST @>
     
@q * (1) Top @>

@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (1) Commands for the database.  @>
@ Commands for the database.

@q ** (2) command --> SAVE any_variable_list with_prefix_option database_option_list.@> 
@*1 \�command> $\longrightarrow$ \.{SAVE} \�any variable list> \�with prefix option> 
\�database option list>.
\initials{LDF 2021.03.23.}

\LOG
\initials{LDF 2021.03.23.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SAVE any_variable_list with_prefix_option database_option_list@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command --> SAVE any_variable_list "
                 << "with_prefix_option database_option_list'."
                 << endl;  

       cerr_strm << "`scanner_node->database_variable_vector.size()' == "
                 << scanner_node->database_variable_vector.size()
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   


   bool query     = @=$4@> & QUERY_VALUE;

   /* \.{NO\_OVERWRITE} always takes precedence over \.{OVERWRITE}, no matter
      what order they're in.
      \initials{LDF 2022.06.26.}  */

   bool overwrite;

   if (@=$4@> & NO_OVERWRITE_VALUE)
      overwrite = false;
   else if (@=$4@> & OVERWRITE_VALUE)
      overwrite = true;
   else
      overwrite = false;
 
@q *** (3) @>

@ The |no_delete| option always takes precedence over |delete| option,
if both are present.  |no_delete| is also the default.  That is,
if |no_delete| is specified or neither option is specified,
points present in the database, but not in the entry during
the current run, aren't deleted from the database.
\initials{LDF 2024.07.30.}

@<Define rules@>=

   bool delete_on_overwrite = false;

   if (@=$4@> & NO_DELETE_VALUE)
      delete_on_overwrite = false;
   else if (@=$4@> & DELETE_VALUE)
      delete_on_overwrite = true;
   else 
      delete_on_overwrite = false;

@q *** (3) @>
@
@<Define rules@>=

   bool add = @=$4@> & ADD_VALUE;

@q *** (3) @>
@
@<Define rules@>=

   bool save = @=$4@> & SAVE_VALUE;

#if 0 
   cerr << "save == " << save << endl;
#endif

   bool use_font_tables = @=$4@> & USE_FONT_TABLES_VALUE;

#if 0
   cerr << "use_font_tables == " << use_font_tables << endl;
#endif

@q *** (3) @>
@
@<Define rules@>=

   if (!database_enabled)
   {
       cerr_strm << "WARNING!  In `yyparse', Rule `command --> SAVE any_variable_list "
                 << "with_prefix_option database_option_list':"
                 << endl  
                 << "`database_enabled' is `false':  Database not enabled." 
                 << endl 
                 << "Can't save objects to database.  Continuing."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm, warning_stop_value);
       cerr_strm.str("");
   }

@q *** (3) @>

   else
   {
@q **** (4) @>

       string prefix;
       string *prefix_ptr = static_cast<string*>(@=$3@>);

       if (prefix_ptr)
       {
          prefix = *prefix_ptr;
          delete prefix_ptr;
          prefix_ptr = 0;
       }

       status = save_to_database_func(static_cast<Scanner_Node>(parameter), 
                                      prefix, query, overwrite, add, delete_on_overwrite, 
                                      save, use_font_tables);

@q **** (4) @>

@ The case that |database_enabled == false| is caught above.
\initials{LDF 2022.06.09.}

@<Define rules@>=

       if (status == 2)
       {
           cerr_strm << "ERROR!  In `yyparse', Rule `command --> SAVE any_variable_list "
                     << "with_prefix_option database_option_list':"
                     << endl  
                     << "`Scan_Parse::save_to_database_func' failed, returning 2:  "
                     << "Database not enabled." 
                     << endl 
                     << "This code should not have been reached!" << endl 
                     << endl
                     << "Can't save objects to database.  Continuing."
                     << endl;
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
       }

@q **** (4) @>

       else if (status != 0)
       {
           cerr_strm << "ERROR!  In `yyparse', Rule `command --> SAVE any_variable_list "
                     << "with_prefix_option database_option_list':"
                     << endl  
                     << "`Scan_Parse::save_to_database_func' failed, returning " 
                     << status << "."
                     << endl 
                     << "Failed to save objects to database."
                     << endl 
                     << "Continuing."
                     << endl;
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
       }
@q **** (4) @>
       else
       {
@q ***** (5) @>

#if DEBUG_COMPILE
           if (DEBUG)
            {
                 cerr_strm << "*** Parser: Rule `command --> SAVE any_variable_list "
                           << "with_prefix_option database_option_list':"
                           << endl  
                           << "`Scan_Parse::save_to_database_func' succeeded, returning 0."
                           << endl;
                 log_message(cerr_strm);
                 cerr_message(cerr_strm);
                 cerr_strm.str("");

             }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

       } /* |else|  */

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) any_variable_list @>
@ \�any variable list>.

\LOG
\initials{LDF 2021.03.24.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> any_variable_list@>

@q ** (2) any_variable_list> --> any_variable @>
@ \�any variable list> $\longrightarrow$ \�any variable>.
\initials{LDF 2021.03.24.}

\LOG
\initials{LDF 2021.03.24.}
Added this rule.

\initials{LDF 2022.11.19.}
Changed right-hand side of rule from |/* Empty */| to |any_variable|.
\ENDLOG

@<Define rules@>= 
@=any_variable_list: any_variable@>
{

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `any_variable_list: /* Empty  */'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_variable_vector.clear();

    scanner_node->database_variable_vector.push_back(static_cast<Id_Map_Entry_Node>(@=$1@>));

    @=$$@> = 0;

};

@q ** (2) any_variable_list --> any_variable_list any_variable @>
@ \�any variable list> $\longrightarrow$ \�any variable list> \�any variable>. 

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=any_variable_list: any_variable_list any_variable @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `any_variable_list: any_variable_list any_variable'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_variable_vector.push_back(static_cast<Id_Map_Entry_Node>(@=$2@>));

    @=$$@> = 0;

};

@q ** (2) any_variable_list --> any_variable_list COMMA any_variable. @>

@ \�any variable list> $\longrightarrow$ \�any variable list> \.{COMMA} \�any variable>.

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=any_variable_list: any_variable_list COMMA any_variable@>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `any_variable_list: any_variable_list COMMA any_variable'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_variable_vector.push_back(static_cast<Id_Map_Entry_Node>(@=$3@>));

};

@q ** (2) with_prefix_option @>
@ \�with prefix option>.

\LOG
\initials{LDF 2021.03.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_prefix_option@>

@q ** (2) with_prefix_option> --> EMPTY @>
@ \�with prefix option> $\longrightarrow$ /* Empty */

\LOG
\initials{LDF 2021.03.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=with_prefix_option: /* Empty  */@>
{

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `with_prefix_option: /* Empty  */'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = 0;

};

@q ** (2) with_prefix_option -->  @>
@ \�with prefix option> $\longrightarrow$ \.{WITH\_PREFIX} \�string expression>. 

\LOG
\initials{LDF 2021.03.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=with_prefix_option: WITH_PREFIX string_expression @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `with_prefix_option: WITH_PREFIX string_expression'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = @=$2@>;

};

@q ** (2) command: SAVE GROUP with_prefix_option database_option_list @>

@ \�command> $\longrightarrow$ \.{SAVE} \.{GROUP} \�with prefix option> 
\�database option list>.
\initials{LDF 2022.07.04.}

\LOG
\initials{LDF 2022.07.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SAVE GROUP with_prefix_option database_option_list@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `command: SAVE GROUP with_prefix_option "
                << "database_option_list'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   bool query     = @=$4@> & QUERY_VALUE;

   /* \.{NO\_OVERWRITE} always takes precedence over \.{OVERWRITE}, no matter
      what order they're in.
      \initials{LDF 2022.06.26.}  */

   bool overwrite;

   if (@=$4@> & NO_OVERWRITE_VALUE)
      overwrite = false;
   else if (@=$4@> & OVERWRITE_VALUE)
      overwrite = true;
   else
      overwrite = false;

   string prefix;
   string *prefix_ptr = static_cast<string*>(@=$3@>);

   if (prefix_ptr)
   {
      prefix = *prefix_ptr;
      delete prefix_ptr;
      prefix_ptr = 0;
   }

@q *** (3) @>

   if (!database_enabled)
   {
       cerr_strm << "WARNING!  In `yyparse', Rule `command --> SAVE GROUP "
                 << endl 
                 << "with_prefix_option database_option_list':"
                 << endl  
                 << "`database_enabled' is `false':  Database not enabled." 
                 << endl 
                 << "Can't save objects to database.  Continuing."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm, warning_stop_value);
       cerr_strm.str("");
   }

   else if (scanner_node->id_map_node->up == 0)
   {
       cerr_strm << "WARNING!  In `yyparse', Rule `command --> SAVE GROUP"
                 << endl 
                 << "with_prefix_option database_option_list':"
                 << endl  
                 << "`scanner_node->id_map_node->up' is NULL." << endl 
                 << "Saving the top-level `id_map_node' to the database is not permitted."
                 << endl 
                 << "Continuing." << endl; 

       log_message(cerr_strm);
       cerr_message(cerr_strm, warning_stop_value);
       cerr_strm.str("");
   }

@q *** (3) @>

   else
   {
@q **** (4) @>

       status = save_group_func(scanner_node, prefix, query, overwrite);

       if (status != 0)
       {
           cerr_strm << "ERROR!  In `yyparse', Rule `command --> SAVE GROUP"
                     << endl 
                     << "with_prefix_option database_option_list':"
                     << endl  
                     << "`Scan_Parse::save_group_func' failed, returning " 
                     << status << "." << endl
                     << "Failed to save current group to database."
                     << endl 
                     << "Continuing." << endl; 

           log_message(cerr_strm);
           cerr_message(cerr_strm, warning_stop_value);
           cerr_strm.str("");

       }

@q **** (4) @>

   }

@q *** (3) @>

END_SAVE_GROUP_RULE:

   @=$$@> = 0;

@q *** (3) @>

};

@q ** (2) command --> RESTORE type_optional_list with_prefix_option database_option_list.@> 
@*1 \�command> $\longrightarrow$ \.{RESTORE} \�type optional list> \�with prefix option> 
\�database option list>.
\initials{LDF 2021.03.24.}

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: RESTORE type_optional_list with_prefix_option database_option_list@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command --> RESTORE type_optional_list with_prefix_option "
                 << "database_option_list'."
                 << endl;  
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   if (!database_enabled)
   {
       cerr_strm << "WARNING!  In `yyparse', Rule `command --> RESTORE type_optional_list with_prefix_option"
                 << endl 
                 << "database_option_list':"
                 << endl  
                 << "`database_enabled' is `false':  Database not enabled." 
                 << endl 
                 << "Can't save objects to database.  Continuing."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm, warning_stop_value);
       cerr_strm.str("");

       goto END_RESTORE_RULE;
   }

@q *** (3) @>

   else
   {
@q **** (4) @>


      string prefix;
      string *prefix_ptr = static_cast<string*>(@=$3@>);

      if (prefix_ptr)
      {
         prefix = *prefix_ptr;
         delete prefix_ptr;
         prefix_ptr = 0;
      }

#if DEBUG_COMPILE
      if (DEBUG)
      {
          cerr_strm << "*** Parser: Rule `command --> RESTORE type_optional_list with_prefix_option"
                    << endl 
                    << "database_option_list':"
                    << endl
                    << "`prefix' == " << prefix << endl;
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");

      }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

      status = restore_from_database_func(static_cast<Scanner_Node>(parameter), 
                                          prefix,
                                          @=$4@>);


      if (status != 0)
      {
          cerr_strm << "ERROR!  In `yyparse', Rule `command --> "
                    << "RESTORE type_optional_list with_prefix_option database_option_list'."
                    << endl  
                    << "`Scan_Parse::restore_from_database_func' failed, returning " << status << "."
                    << endl 
                    << "Failed to restore objects from database."
                    << endl 
                    << "Continuing."
                    << endl;
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
      }
      else
      {
#if DEBUG_COMPILE
          if (DEBUG)
           {
                cerr_strm << "*** Parser: Rule `command --> RESTORE type_optional_list with_prefix_option "
                          << "database_option_list':"
                          << endl  
                          << "`Scan_Parse::restore_from_database_func' succeeded, returning 0."
                          << endl;
                log_message(cerr_strm);
                cerr_message(cerr_strm);
                cerr_strm.str("");

            }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

      }

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

END_RESTORE_RULE:

   scanner_node->database_type_options.reset();

   @=$$@> = 0;

};

@q ** (2) database_option_list ... @>
@ \�database option list> $\ldots$
\initials{LDF 2022.06.25.}

\LOG
\initials{LDF 2022.06.25.}
Added these type declarations.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> type_optional_list@>
@=%type <ulong_value> database_option_list@>
@=%type <ulong_value> database_option@>
@=%type <int_value> comma_optional@>

@q ** (2) comma_optional --> EMPTY @>
@ \�comma optional> $\longrightarrow$ /* Empty */

\LOG
\initials{LDF 2022.06.26.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=comma_optional: /* Empty  */@>
{
   @=$$@> = 0;
};

@q ** (2) comma_optional --> COMMA @>
@ \�comma optional> $\longrightarrow$ \.{COMMA}.

\LOG
\initials{LDF 2022.06.26.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=comma_optional: COMMA @>
{
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> EMPTY @>
@ \�type optional list> $\longrightarrow$ /* Empty */

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: /* Empty  */@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.reset();
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional NUMERICS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{NUMERICS}.

\LOG
\initials{LDF 2022.07.31.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional NUMERICS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_NUMERICS_VALUE);   
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional TRANSFORMS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{TRANSFORMS}.
\initials{LDF 2022.06.25.}

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional TRANSFORMS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_TRANSFORMS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional STRINGS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{STRINGS}.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional STRINGS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_STRINGS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional LINES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{LINES}.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional LINES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_LINES_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional PLANES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{PLANES}.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PLANES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PLANES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POINTS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{POINTS}.

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POINTS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POINTS_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional PATHS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PATHS}.

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PATHS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PATHS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CIRCLES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CIRCLES}.

\LOG
\initials{LDF 2022.09.03.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CIRCLES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CIRCLES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional ELLIPSES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{ELLIPSES}.
\initials{LDF 2022.11.05.}

\LOG
\initials{LDF 2022.11.05.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional ELLIPSES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_ELLIPSES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional SUPERELLIPSES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{SUPERELLIPSES}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional SUPERELLIPSES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_SUPERELLIPSES_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional PARABOLAE @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PARABOLAE}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PARABOLAE@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PARABOLAE_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional HYPERBOLAE @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{HYPERBOLAE}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional HYPERBOLAE@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_HYPERBOLAE_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional REG_POLYGONS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{REG\_POLYGONS}.
\initials{LDF 2022.11.15.}

\LOG
\initials{LDF 2022.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional REG_POLYGONS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_REG_POLYGONS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POLYGONS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{REG\_POLYGONS}.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POLYGONS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POLYGONS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional RECTANGLES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{REG\_POLYGONS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional RECTANGLES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_RECTANGLES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional TRIANGLES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{REG\_POLYGONS}.
\initials{LDF 2022.12.19.}

\LOG
\initials{LDF 2022.12.19.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional TRIANGLES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_TRIANGLES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional SPHERES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{SPHERES}.
\initials{LDF 2022.09.03.}

\LOG
\initials{LDF 2022.09.03.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional SPHERES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_SPHERES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional ELLIPSOIDS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{ELLIPSOIDS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional ELLIPSOIDS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_ELLIPSOIDS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CONES @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CONES}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CONES@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CONES_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CYLINDERS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CYLINDERS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CYLINDERS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CYLINDERS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CUBOIDS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CUBOIDS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CUBOIDS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CUBOIDS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POLYHEDRA @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{POLYHEDRA}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POLYHEDRA@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POLYHEDRA_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional NUMERIC_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{NUMERIC_VECTORS}.

\LOG
\initials{LDF 2022.08.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional NUMERIC_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_NUMERIC_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional STRING_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{STRING_VECTORS}.
\initials{LDF 2023.01.21.}

\LOG
\initials{LDF 2023.01.21.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional STRING_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_STRING_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional TRANSFORM_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{TRANSFORM_VECTORS}.

\LOG
\initials{LDF 2022.08.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional TRANSFORM_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_TRANSFORM_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POINT_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{POINT_VECTORS}.

\LOG
\initials{LDF 2022.08.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POINT_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POINT_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PATH_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PATH_VECTORS}.

\LOG
\initials{LDF 2022.08.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PATH_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PATH_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CIRCLE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CIRCLE_VECTORS}.

\LOG
\initials{LDF 2022.10.19.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CIRCLE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CIRCLE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PARABOLA_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PARABOLA_VECTORS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PARABOLA_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PARABOLA_VECTORS_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional HYPERBOLA_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{HYPERBOLA_VECTORS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional HYPERBOLA_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_HYPERBOLA_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional RECTANGLE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{RECTANGLE_VECTORS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional RECTANGLE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_RECTANGLE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional SPHERE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{SPHERE_VECTORS}.
\initials{LDF 2022.11.01.}

\LOG
\initials{LDF 2022.11.01.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional SPHERE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_SPHERE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional ELLIPSOID_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{ELLIPSOID_VECTORS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional ELLIPSOID_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_ELLIPSOID_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CONE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CONE_VECTORS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CONE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CONE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CYLINDER_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CYLINDER_VECTORS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CYLINDER_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CYLINDER_VECTORS_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional CUBOID_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CUBOID_VECTORS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CUBOID_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CUBOID_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POLYHEDRON_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{POLYHEDRON_VECTORS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POLYHEDRON_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POLYHEDRON_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional NUMERIC_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{NUMERIC_ARRAYS}.

\LOG
\initials{LDF 2022.09.11.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional NUMERIC_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_NUMERIC_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional STRING_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{STRING_ARRAYS}.
\initials{LDF 2023.01.17.}

\LOG
\initials{LDF 2023.01.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional STRING_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_STRING_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional TRANSFORM_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{TRANSFORM_ARRAYS}.

\LOG
\initials{LDF 2022.09.11.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional TRANSFORM_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_TRANSFORM_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POINT_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{POINT_ARRAYS}.

\LOG
\initials{LDF 2022.09.11.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POINT_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POINT_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional LINE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{LINE_ARRAYS}.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional LINE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_LINE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PLANE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{PLANE_ARRAYS}.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PLANE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PLANE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional LINE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{LINE_VECTORS}.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional LINE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_LINE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PLANE_VECTORS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> \.{PLANE_VECTORS}.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PLANE_VECTORS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PLANE_VECTORS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PATH_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PATH_ARRAYS}.

\LOG
\initials{LDF 2022.09.11.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PATH_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PATH_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CIRCLE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CIRCLE_ARRAYS}.

\LOG
\initials{LDF 2022.09.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CIRCLE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CIRCLE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional ELLIPSE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{ELLIPSE_ARRAYS}.

\LOG
\initials{LDF 2022.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional ELLIPSE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_ELLIPSE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional SUPERELLIPSE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{SUPERELLIPSE_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional SUPERELLIPSE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_SUPERELLIPSE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional PARABOLA_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{PARABOLA_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional PARABOLA_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_PARABOLA_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional HYPERBOLA_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{HYPERBOLA_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional HYPERBOLA_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_HYPERBOLA_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional RECTANGLE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{RECTANGLE_ARRAYS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional RECTANGLE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_RECTANGLE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional SPHERE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{SPHERE_ARRAYS}.
\initials{LDF 2022.10.27.}

\LOG
\initials{LDF 2022.10.27.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional SPHERE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_SPHERE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional ELLIPSOID_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{ELLIPSOID_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional ELLIPSOID_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_ELLIPSOID_ARRAYS_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional CONE_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CONE_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CONE_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CONE_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional CYLINDER_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CYLINDER_ARRAYS}.
\initials{LDF 2022.12.02.}

\LOG
\initials{LDF 2022.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CYLINDER_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CYLINDER_ARRAYS_VALUE);
   @=$$@> = 0;
};


@q ** (2) type_optional_list> --> type_optional_list comma_optional CUBOID_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{CUBOID_ARRAYS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional CUBOID_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_CUBOID_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) type_optional_list> --> type_optional_list comma_optional POLYHEDRON_ARRAYS @>
@ \�type optional list> $\longrightarrow$ \�type optional list> \�comma optional> 
\.{POLYHEDRON_ARRAYS}.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=type_optional_list: type_optional_list comma_optional POLYHEDRON_ARRAYS@>
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
   scanner_node->database_type_options.set(WITH_POLYHEDRON_ARRAYS_VALUE);
   @=$$@> = 0;
};

@q ** (2) command --> LOAD DATABASE.@> 
@*1 \�command> $\longrightarrow$ \.{LOAD} \.{DATABASE}.
\initials{LDF 2021.06.07.}

\LOG
\initials{LDF 2021.06.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: LOAD DATABASE@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command --> LOAD DATABASE'."
                 << endl;  
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   if (database_enabled)
   {
       cerr_strm << "In `yyparse', rule `command --> LOAD DATABASE':"
                 << endl
                 << "`database_enabled' is already `true'."
                 << endl 
                 << "Not calling `Scanner_Type::load_database'.  Continuing."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_DATABASE;
   }

@q *** (3) @>

   status = scanner_node->load_database();
  
@q *** (3) @>

@ This code should never be reached.
\initials{LDF 2021.06.09.}

@<Define rules@>=

   if (status == 2)
   {
       cerr_strm << "In `yyparse', rule `command --> LOAD DATABASE':"
                 << endl
                 << "`Scanner_Type::load_database' returned 2:"
                 << endl 
                 << "Database already loaded.  Not reloaded."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_DATABASE;

   } 

@q *** (3) @>
@
@<Define rules@>=

   else if (status != 0)
   {
       cerr_strm << "ERROR!  In `yyparse', rule `command --> LOAD DATABASE':"
                 << endl
                 << "`Scanner_Type::load_database' failed, returning " << status << "."
                 << endl 
                 << "Failed to load database.  Will try to continue."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_DATABASE;
   } 
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << "In `yyparse', rule `command --> LOAD DATABASE':"
                 << endl
                 << "`Scanner_Type::load_database' succeeded, returning 0."
                 << endl
                 << "Loaded database successfully."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

END_LOAD_DATABASE:

   @=$$@> = 0;

};

@q ** (2) command --> LOAD ASTRONOMY.@> 
@*1 \�command> $\longrightarrow$ \.{LOAD} \.{ASTRONOMY}.
\initials{LDF 2021.06.07.}

\LOG
\initials{LDF 2021.06.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: LOAD ASTRONOMY@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command --> LOAD ASTRONOMY'."
                 << endl;  
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   if (astronomy_enabled)
   {
       cerr_strm << "In `yyparse', rule `command --> LOAD ASTRONOMY':"
                 << endl
                 << "`astronomy_enabled' is already `true'."
                 << endl 
                 << "Not calling `Scanner_Type::get_astronomy'.  Continuing."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_ASTRONOMY;
   }

@q *** (3) @>

   if (!database_enabled)
   {
@q **** (4) @>

      status = scanner_node->load_database();

      if (status != 0)
      {
           cerr_strm << "ERROR!  In `yyparse', rule `command --> LOAD ASTRONOMY':"
                     << endl
                     << "`Scanner_Type::load_database' failed, returning " << status << "."
                     << endl 
                     << "Not calling `Scanner_Type::get_astronomy'."
                     << endl
                     << "Will try to continue."
                     << endl;
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           goto END_LOAD_ASTRONOMY;

      }
@q **** (4) @>
@
@<Define rules@>=

      else if (!database_enabled)
      {
           cerr_strm << "ERROR!  In `yyparse', rule `command --> LOAD ASTRONOMY':"
                     << endl
                     << "`database_enabled' is `false' following successful call to `scanner_node::load_database'."
                     << endl 
                     << "This shouldn't be possible."
                     << endl 
                     << "Not calling `Scanner_Type::get_astronomy'."
                     << endl
                     << "Will try to continue."
                     << endl;
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           goto END_LOAD_ASTRONOMY;

      }

@q **** (4) @>
@
@<Define rules@>=

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
           cerr_strm << "In `yyparse', rule `command --> LOAD ASTRONOMY':"
                     << endl
                     << "`Scanner_Type::load_database' succeeded, returning 0."
                     << endl 
                     << "Will call `Scanner_Type::get_astronomy'."
                     << endl;
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
      }   
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

   }  /* |if (!database_enabled)|  */

@q *** (3) @>
@
@<Define rules@>=

   status = scanner_node->get_astronomy();

@q *** (3) @>

@ This code should never be reached.
\initials{LDF 2021.06.09.}

@<Define rules@>=

   if (status == 2)
   {
       cerr_strm << "In `yyparse', rule `command --> LOAD ASTRONOMY':"
                 << endl
                 << "`Scanner_Type::get_astronomy' returned 2:"
                 << endl 
                 << "Astronomy already loaded.  Not reloaded."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_ASTRONOMY;

   } 

@q *** (3) @>
@
@<Define rules@>=

   else if (status != 0)
   {
       cerr_strm << "ERROR!  In `yyparse', rule `command --> LOAD ASTRONOMY':"
                 << endl
                 << "`Scanner_Type::get_astronomy' failed, returning " << status << "."
                 << endl 
                 << "Failed to load astronomy.  Will try to continue."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       goto END_LOAD_ASTRONOMY;
   } 
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << "In `yyparse', rule `command --> LOAD ASTRONOMY':"
                 << endl
                 << "`Scanner_Type::get_astronomy' succeeded, returning 0."
                 << endl
                 << "Loaded astronomy successfully."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

END_LOAD_ASTRONOMY:

   @=$$@> = 0;

};

@q ** (2) command: CLEAR DATABASE database_option_list @>

@ \�command> \.{CLEAR} \.{DATABASE \�clear database option list>.
\initials{LDF 2022.06.10.}

\LOG
\initials{LDF 2022.06.10.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: CLEAR DATABASE database_option_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command --> CLEAR DATABASE database_option_list'."
                 << endl;  
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   status = clear_database_func(scanner_node, @=$3@>);

   if (status != 0)
   {
       cerr_strm << "ERROR!  In parser, rule `command --> CLEAR DATABASE database_option_list':"
                 << endl
                 << "`Scan_Parse::clear_database_func' failed, returning " << status << "."
                 << endl
                 << "Failed to clear database.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (status != 0)| */

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << "In parser, rule `command --> CLEAR DATABASE database_option_list':"
                 << endl
                 << "`Scan_Parse::clear_database_func' succeeded, returning 0."
                 << endl
                 << "Cleared database successfully.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   @=$$@> = 0;

};

@q ****** (6) command --> SHOW DATABASE string_optional database_option_list@>

@*5 \�command> $\longrightarrow$ \.{SHOW} \.{DATABASE} \�string optional>
\�database option list>.
\initials{LDF 2021.03.23.}

\LOG
\initials{LDF 2021.03.23.}
Added this rule.
\ENDLOG

@q ******* (7) Definition.@> 

@<Define rules@>=
  
@=command: SHOW DATABASE string_optional database_option_list@>@/
{
@q ******** (8) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `command --> SHOW DATABASE string_optional database_option_list'.";
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

@q ******** (8) @>

#ifndef MYSQL_AVAILABLE
    cerr_strm << "MySQL not available.  No database to show." << endl
              << "Continuing." << endl;
    log_message(cerr_strm);
    cerr_message(cerr_strm);
    cerr_strm.str("");

    goto END_SHOW_DATABASE_0;
#endif 

@q ******** (8) @>
@
@<Define rules@>=

    string *s = static_cast<string*>(@=$3@>);

#if 0 
    if (s)
       cerr << "`s' is non-NULL." << endl;
    else
       cerr << "`s' is NULL." << endl;
#endif 


@q ******** (8) @>

    if (!database_enabled)
    {
       cerr_strm << "*** Parser:  In rule `command --> SHOW DATABASE string_optional database_option_list':"
                 << "`database_enabled' == `false'." << endl 
                 << endl
                 << "Database is not available.  Continuing." << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   
       goto END_SHOW_DATABASE_0;
    }

@q ******** (8) @>
@
@<Define rules@>= 

    if (scanner_node->database_options.empty())
       scanner_node->database_options.push_back(ALL);

    status = Scan_Parse::show_database_func(scanner_node, s);
 
    if (status == 2)
    {
       cerr_strm << "*** Parser:  In rule `command --> SHOW DATABASE string_optional database_option_list':"
                 << "`Scan_Parse::show_database_func' returned 2." << endl 
                 << endl
                 << "No rows returned from database.  Continuing." << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
    else if (status != 0)
    {
       cerr_strm << "*** Parser:  ERROR! In rule `command --> SHOW DATABASE string_optional "
                 << "database_option_list':"
                 << "`Scan_Parse::show_database_func' failed, returning " << status 
                 << endl
                 << "Failed to show database.  Continuing." << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
    else if (DEBUG)
    {
       cerr_strm << "*** Parser:  In rule `command --> SHOW DATABASE string_optional "
                 << "database_option_list':" << endl 
                 << "`Scan_Parse::show_database_func' succeeded, returning 0."
                 << endl;
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }

@q ******** (8) @>

END_SHOW_DATABASE_0:

    scanner_node->database_options.clear();

    if (s)
       delete s;

    @=$$@> = static_cast<void*>(0);

};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> string_optional@>

@q *** (3) @>
@
@<Define rules@>= 
@=string_optional: /* Empty  */@>@/
{
   @=$$@> = static_cast<void*>(0);
};

@q *** (3) @>
@
@<Define rules@>= 
@=string_optional: string_expression@>@/
{
   @=$$@> = static_cast<void*>(@=$1@>);
};

@q *** (3) @>
@
@<Define rules@>= 
@=command: SET_PREFIX string_expression@>@/
{
   string *s = static_cast<string*>(@=$2@>);

   Scan_Parse::curr_prefix = *s;

   delete s;
};

@q *** (3) @>
@
@<Define rules@>= 
@=command: UNSET_PREFIX@>@/
{

   Scan_Parse::curr_prefix = "";

};

@q *** (3) @>
@
@<Define rules@>= 
@=command: SHOW_PREFIX@>@/
{
 
   if (Scan_Parse::curr_prefix.empty())
      cerr << "curr_prefix is empty." << endl;
   else
      cerr << "curr_prefix == " << Scan_Parse::curr_prefix << endl;
};

@q ** (2) database_option_list> --> EMPTY @>
@ \�database option list> $\longrightarrow$ /* Empty */

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option_list: /* Empty  */@>
{
   @<Common declarations for rules@>@; 

   scanner_node->database_options.clear();

   @=$$@> = 0U;
};

@q ** (2) database_option_list> --> database_option_list database_option @>
@ \�database option list> $\longrightarrow$ \�database option list>  \�database option>.
\initials{LDF 2022.06.25.}

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option_list: database_option_list database_option @>
{
   @=$$@> = @=$1@> |= @=$2@>;
};

@q ** (2) database_option --> QUERY @>
@ \�database option> $\longrightarrow$ \.{QUERY}.

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: QUERY @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: QUERY'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = QUERY_VALUE;

};

@q ** (2) database_option --> OVERWRITE @>
@ \�database option> $\longrightarrow$ \.{OVERWRITE}.

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: OVERWRITE @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: OVERWRITE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = OVERWRITE_VALUE;

};

@q ** (2) database_option --> NO_OVERWRITE @>
@ \�database option> $\longrightarrow$ \.{NO\_OVERWRITE}.

\LOG
\initials{LDF 2021.03.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: NO_OVERWRITE @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: NO_OVERWRITE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = NO_OVERWRITE_VALUE;

};

@q ** (2) database_option --> ALL @>
@ \�database option> $\longrightarrow$ \.{ALL}.

\LOG
\initials{LDF 2022.06.25.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: ALL @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: ALL'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = ALL_TYPES_VALUE;

};

@q ** (2) database_option --> SAVE @>
@ \�database option> $\longrightarrow$ \.{SAVE}.
\initials{LDF 2022.08.18.}

\LOG
\initials{LDF 2022.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: SAVE @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: SAVE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(SAVE_VALUE);
    @=$$@> = SAVE_VALUE;

};

@q ** (2) database_option --> WITH_NO_PREFIX @>
@ \�database option> $\longrightarrow$ \.{WITH\_NO\_PREFIX}.
\initials{LDF 2024.07.28.}

\LOG
\initials{LDF 2024.07.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: WITH_NO_PREFIX @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: WITH_NO_PREFIX'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(WITH_NO_PREFIX_VALUE);
    @=$$@> = WITH_NO_PREFIX_VALUE;

};

@q ** (2) database_option --> ADD @>
@ \�database option> $\longrightarrow$ \.{ADD}.
\initials{LDF 2024.07.28.}

\LOG
\initials{LDF 2024.07.28.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: ADD @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: ADD'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(ADD_VALUE);
    @=$$@> = ADD_VALUE;

};

@q ** (2) database_option --> DELETE @>
@ \�database option> $\longrightarrow$ \.{DELETE}.
\initials{LDF 2024.07.30.}

\LOG
\initials{LDF 2024.07.30.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: DELETE @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: DELETE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(DELETE_VALUE);
    @=$$@> = DELETE_VALUE;

};

@q ** (2) database_option --> NO_DELETE @>
@ \�database option> $\longrightarrow$ \.{NO\_DELETE}.
\initials{LDF 2024.07.30.}

\LOG
\initials{LDF 2024.07.30.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: NO_DELETE @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: NO_DELETE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(NO_DELETE_VALUE);
    @=$$@> = NO_DELETE_VALUE;

};

@q ** (2) database_option --> USE_FONT_TABLES @>
@ \�database option> $\longrightarrow$ \.{USE\_FONT\_TABLES}.
\initials{LDF 2024.08.04.}

\LOG
\initials{LDF 2024.08.04.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=database_option: USE_FONT_TABLES @>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `database_option: USE_FONT_TABLES'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->database_options.push_back(USE_FONT_TABLES_VALUE);
    @=$$@> = USE_FONT_TABLES_VALUE;

};


@q * (1)@>
@
@<Define rules@>= 
@=command: OPEN SQL_FILE string_expression@>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `command: OPEN SQL_FILE string_expression'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    string *s = static_cast<string*>(@=$3@>);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "*s == " << *s << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->sql_file.open(s->c_str());


   if (s)
   {
      delete s;
      s = 0;
   }
  
   @=$$@> = 0;

};

@q * (1)@>
@
@<Define rules@>= 
@=command: CLOSE SQL_FILE@>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `command: CLOSE SQL_FILE'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    scanner_node->sql_file.close();


    @=$$@> = 0;
  
};

@q * (1)@>
@
@<Define rules@>= 
@=command: LOAD SQL_FILE string_expression@>
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: Rule `command: LOAD SQL_FILE string_expression'."
                << endl;  

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;


    string *s = static_cast<string*>(@=$3@>);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "*s == " << *s << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

    if (s)
    {
       delete s;
       s = 0;
    }
  
    @=$$@> = 0;
  
};





@q ** (2) @>

@q * (1)@>

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables: @>
@q mode:CWEB  @>
@q eval:(outline-minor-mode t)  @>
@q abbrev-file-name:"~/.abbrev_defs" @>
@q eval:(read-abbrev-file)  @>
@q fill-column:70 @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End: @>
