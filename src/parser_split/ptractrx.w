@q ptractrx.w.w @> 
@q Created by Laurence Finston Di 20. Jun 22:06:53 CEST 2023@>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2023 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) tractrix expressions.  @>
@** tractrix expressions.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Created this file.
\ENDLOG 

@q * (1) tractrix_primary.  @>
@* \�tractrix primary>.
\initials{LDF 2023.06.20.}  

\LOG
\initials{LDF 2023.06.20.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> tractrix_primary@>@/

@q ** (2) tractrix_primary --> tractrix_variable.@>
@*1 \�tractrix primary> $\longrightarrow$ \�tractrix variable>.  

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=tractrix_primary: tractrix_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

#if 0 
    @=$$@> = static_cast<void*>(create_new<Tractrix>(
                                  static_cast<Tractrix*>(
                                  entry->object))); 

#else
/* !! START HERE LDF 2023.06.20. */

   @=$$@> = static_cast<void*>(new Tractrix);

#endif 



};

@q ** (2) tractrix_primary --> LEFT_PARENTHESIS tractrix_expression @> 
@q ** (2) RIGHT_PARENTHESIS                                         @>

@*1 \�tractrix primary> $\longrightarrow$ \.{\LP} 
\�tractrix expression> \.{\RP}.

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=tractrix_primary: LEFT_PARENTHESIS tractrix_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q * (1) tractrix_secondary.  @>
@* \�tractrix secondary>.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> tractrix_secondary@>
  
@q ** (2) tractrix secondary --> tractrix_primary.@>
@*1 \�tractrix secondary> $\longrightarrow$ \�tractrix primary>.

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=tractrix_secondary: tractrix_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: tractrix_secondary --> tractrix_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) tractrix secondary --> tractrix_secondary transformer.  @>
@*1 \�tractrix secondary> $\longrightarrow$ \�tractrix secondary> 
\�transformer>.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Added this rule.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX: Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=tractrix_secondary: tractrix_secondary transformer@>@/
{



#if 0 
/* !! START HERE LDF 2023.06.20. */

  Tractrix* p = static_cast<Tractrix*>(@=$1@>);
  Transform* t = static_cast<Transform*>(@=$2@>);

  *p *= *t;

  @=$$@> = static_cast<void*>(p); 

  delete t;
#endif 

  @=$$@> = 0;

};

@q * (1) tractrix tertiary.@>
@* \�tractrix tertiary>.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> tractrix_tertiary@>

@q ** (2) tractrix tertiary --> tractrix_secondary.  @>
@*1 \�tractrix tertiary> $\longrightarrow$ \�tractrix secondary>.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=tractrix_tertiary: tractrix_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q * (1) tractrix expression.@>
@* \�tractrix expression>.
\initials{LDF 2023.06.20.}

\LOG
\initials{LDF 2023.06.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> tractrix_expression@>

@q ** (2) tractrix expression --> tractrix_tertiary.  @>
@*1 \�tractrix expression> $\longrightarrow$ \�tractrix tertiary>.

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=tractrix_expression: tractrix_tertiary@>@/
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:70                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

