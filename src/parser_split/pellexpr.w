@q pellexpr.w @> 
@q Created by Laurence Finston Tue Jun 29 17:06:20 CEST 2004  @>
       
@q * (1) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing.  @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version.  @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details.  @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA@>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html.@>

@q ("@@" stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de@>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)@>

@q * (0) ellipse expressions.  @>
@** Ellipse expressions.
\initials{LDF 2004.06.29.}

\LOG
\initials{LDF 2004.06.29.}
Created this file.
\ENDLOG 

@q ** (2) Ellipse primary.  @>
@*1 \�ellipse primary>.
\initials{LDF 2004.06.29.}    

\LOG
\initials{LDF 2004.06.29.}  
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipse_primary@>@/

@q *** (3) ellipse_primary --> ellipse_variable.@>
@*2 \�ellipse primary> $\longrightarrow$ \�ellipse variable>.  

\LOG
\initials{LDF 2004.06.29.}
Added this rule.

\initials{LDF 2004.11.23.}
No longer issuing an error message if |entry->object == 0|.
This condition occurs legitimately when one tries to show
an ``unknown |ellipse|''.
\ENDLOG 

@<Define rules@>=
@=ellipse_primary: ellipse_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

    @=$$@> = static_cast<void*>(create_new<Ellipse>(
                                  static_cast<Ellipse*>(
                                     entry->object))); 
};

@q *** ellipse_primary --> ellipse_argument.@>
@ \�ellipse primary> $\longrightarrow$ \�ellipse argument>.  

@q *** ellipse_primary --> ( ellipse_expression )  @>
@ \�ellipse primary> $\longrightarrow$ `\.{\LP}' 
\�ellipse expression> `\.{\RP}'.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=ellipse_primary: LEFT_PARENTHESIS ellipse_expression RIGHT_PARENTHESIS@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr << "\n*** Parser: ellipse_primary --> `(' ellipse_expression `)' "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$2@>;

};

@q ** (2) ellipse_primary --> GET_ELLIPSE INTEGER cone_primary@>
@*1 \�ellipse primary> $\longrightarrow$ \.{GET\_ELLIPSE}
INTEGER \�cone primary>. 
\initials{LDF 2006.11.09.}

\LOG
\initials{LDF 2006.11.09.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=ellipse_primary: GET_ELLIPSE INTEGER cone_primary@>@/
{

   @=$$@> = quad_surf_get_element_func(parameter, 
                                       GET_ELLIPSE, 
                                       Shape::CONE_TYPE, 
                                       @=$3@>,
                                       @=$2@>);

};

@q ** (2) ellipse_primary --> GET_ELLIPSE numeric_primary ellipsoid_primary@>
@*1 \�ellipse primary> $\longrightarrow$ \.{GET\_ELLIPSE}
\�numeric primary> \�ellipsoid primary>. 

\LOG
\initials{LDF 2004.10.13.}
Added this rule.

\initials{LDF 2005.11.02.}
Commented-in this rule.  
Removed debugging code.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=ellipse_primary: GET_ELLIPSE numeric_primary ellipsoid_primary@>@/ 
{

  Ellipsoid* s = static_cast<Ellipsoid*>(@=$3@>);

  Ellipse* e;

  const Ellipse* t 
     = s->get_ellipse_ptr(static_cast<unsigned short>( floor(fabs(@=$2@>) + .5))); 

  if (t == static_cast<const Ellipse*>(0))
   {
      delete s;

      @=$$@> = static_cast<void*>(0);

   } /* |if (t == 0)|  */

  else /* |t != 0|  */
     {
            e = create_new<Ellipse>(t);
            delete s;
            @=$$@> = static_cast<void*>(e);

  }  /* |else| (|t != 0|)  */
};

@q ** (2) ellipse_primary --> GET_ELLIPSE point_vector_primary@>
@*1 \�ellipse primary> $\longrightarrow$ \.{GET\_ELLIPSE}
\�point vector primary>.
\initials{LDF 2005.11.24.}

\LOG
\initials{LDF 2005.11.24.}
Added this rule.

\initials{LDF 2007.06.27.}
Added \�numeric primary> symbol.

\initials{LDF 2007.07.03.}
Replaced |Ellipse::create_new_ellipse| with |Conic_Section::generate|.

\initials{LDF 2007.07.03.}
Now passing |Shape::ELLIPSE_TYPE| to |Scan_Parse::get_conic_section_func|.

\initials{LDF 2007.07.20.}
Added \�with test optional>.  It's  tested and the appropriate |bool| value
is passed to |Scan_Parse::get_conic_section_func|.

\initials{LDF 2007.07.20.}
Added \�with rectify optional>.  It's tested and the appropriate |bool| value
is passed to |Scan_Parse::get_conic_section_func|.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=ellipse_primary: GET_ELLIPSE point_vector_primary numeric_primary@>@/ 
@=with_test_optional with_rectify_optional@>@/
{

   bool test_value    = (@=$4@> == WITH_NO_TEST)    ? false : true;
   bool rectify_value = (@=$4@> == WITH_NO_RECTIFY) ? false : true;

   @=$$@> = get_conic_section_func<Ellipse>(static_cast<Pointer_Vector<Point>*>(@=$2@>), 
                                    static_cast<int>(@=$3@>),
                                    Shape::ELLIPSE_TYPE,
                                    static_cast<Ellipse*>(0),
                                    parameter,
                                    test_value,
                                    rectify_value);

};

@q ** (2) ellipse_primary --> IN_ELLIPSE rectangle_primary@>
@*1 \�ellipse primary> $\longrightarrow$ \.{IN\_ELLIPSE}
\�ellipse primary>. 

\LOG
\initials{LDF 2004.11.20.}
Added this rule.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX: Now deleting |Rectangle* r|.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=ellipse_primary: IN_ELLIPSE rectangle_primary@>@/
{

   Rectangle* r = static_cast<Rectangle*>(@=$2@>);

   Ellipse* e;

   e = create_new<Ellipse>(0);

   *e = r->in_ellipse();
   
   delete r;

   @=$$@> = static_cast<void*>(e); 

};

@q ** (2) ellipse_primary --> OUT_ELLIPSE rectangle_primary@>
@*1 \�ellipse primary> $\longrightarrow$ \.{OUT\_ELLIPSE}
\�ellipse primary>. 

\LOG
\initials{LDF 2004.11.20.}
Added this rule.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX: Now deleting |Rectangle* r|.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=ellipse_primary: OUT_ELLIPSE rectangle_primary@>@/
{

   Rectangle* r = static_cast<Rectangle*>(@=$2@>);

   Ellipse* e;

         e = create_new<Ellipse>(0);

   *e = r->out_ellipse();
   
   delete r;

   @=$$@> = static_cast<void*>(e); 

};

@q ***** (5) ellipse_primary --> LAST @>
@q ***** (5) ellipse_vector_expression.@>

@*4 \�ellipse primary> $\longrightarrow$ 
\.{LAST} \�ellipse vector expression>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=ellipse_primary: LAST ellipse_vector_expression@>@/
{ 
   Ellipse* p;

         p = create_new<Ellipse>(0);

   Pointer_Vector<Ellipse>* pv 
      = static_cast<Pointer_Vector<Ellipse>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<Ellipse>*>(0))
      {

          delete p;

          @=$$@> = static_cast<void*>(0);

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {

          delete p;

          @=$$@> = static_cast<void*>(0);

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   else 
      {
         *p = *(pv->v[pv->ctr - 1]);
         @=$$@> = static_cast<void*>(p); 
      }
@q ******* (7) @> 

};

@q *** (3) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_transform_optional@>@/
@=%type <pointer_value> with_circle_optional@>@/

@q *** (3) @>
@
@<Define rules@>=
@=with_transform_optional: /* Empty  */@>@/
{

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) @>
@
@<Define rules@>=
@=with_transform_optional: WITH_TRANSFORM transform_variable@>
{ 
   @=$$@> = @=$2@>;
}; 


@q *** (3) @>
@
@<Define rules@>=
@=with_circle_optional: /* Empty  */@>@/
{

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) @>
@
@<Define rules@>=
@=with_circle_optional: WITH_CIRCLE circle_variable@>@/
{
   @=$$@> = @=$2@>;
};


@q *** (3) ellipse_primary: ellipse_primary NORMALIZED with_transform_optional call_metapost_option_list.  @>

@ \�ellipse primary> $\longrightarrow$ \�ellipse primary> \.{NORMALIZED} with_transform_optional
\�call metapost option list>.
\initials{LDF 2022.05.19.}

\LOG
\initials{LDF 2022.05.19.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=ellipse_primary: ellipse_primary NORMALIZED with_transform_optional call_metapost_option_list@>@/
{
@q ***** (5) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `ellipse_primary: ellipse_primary NORMALIZED "
	       << "with_transform_optional with_ellipse_optional call_metapost_option_list'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

   bool save               = @=$4@> & 1U;
   bool clear              = @=$4@> & 2U;
   bool suppress_mp_stdout = @=$4@> & 4U;
   bool do_transform       = @=$4@> & 8U;    

   Point *x_axis_pt = 0;

   Id_Map_Entry_Node transform_entry = 0;
   Transform *curr_transform = 0;

   if (@=$3@> != 0)
   {
       transform_entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

       if (transform_entry->object != 0)
          curr_transform = static_cast<Transform*>(transform_entry->object);
       else
       {
           curr_transform = create_new<Transform>(0);
           transform_entry->object = curr_transform;
       }
   }

@q ***** (5) @>

   if (@=$4@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q ***** (5) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>
@
@<Define rules@>=

   Ellipse *c = static_cast<Ellipse*>(@=$1@>);
   Ellipse q;
   Transform t;
   Point *origin_pt = 0;

   if (c)
   {
@q ****** (6) @>

     if (@=$4@> & 32U)
     {
         origin_pt = scanner_node->origin_pt;
         scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "`with_origin_pt' option present." << endl;
             if (origin_pt)
                cerr << "`origin_pt' is non-NULL." << endl;
             else
                cerr << "`origin_pt' is NULL." << endl;
         }  
#endif /* |DEBUG_COMPILE|  */@; 

     }

@q ****** (6) @>

     else 
     {
#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "`with_origin_pt' option not present.  Using `c->center'." << endl
                  << "This is the normal case." << endl;
         }  
#endif /* |DEBUG_COMPILE|  */@; 

         origin_pt = create_new<Point>(0);

         *origin_pt = c->get_center();

     }

@q ****** (6) @>

     status = c->normalize_ellipse(&q, &t, 0, origin_pt, x_axis_pt, save, 
                                   suppress_mp_stdout, scanner_node);

     if (status != 0)
     {
        cerr_strm << thread_name 
                  << "ERROR!  In parser, rule `ellipse_primary: ellipse_primary NORMALIZED "
		  << "with_transform_optional call_metapost_option_list':"
                  << endl 
                  << "`Ellipse::normalize_ellipse' failed, returning " << status << "."
                  << endl 
                  << "Couldn't normalize ellipse."
                  << endl 
                  << "Returning unchanged `ellipse_primary' from right-hand side as result "
		  << "and continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str(""); 

     }

@q ****** (6) @>

     else
     { 
#if DEBUG_COMPILE
        if (DEBUG)
     	{ 
     	   cerr_strm << thread_name 
     	             << "In parser, rule `ellipse_primary: ellipse_primary NORMALIZED "
                     << "with_transform_optional call_metapost_option_list':"
     	             << endl 
     	             << "`Ellipse::normalize_ellipse' succeeded, returning 0.";

     	   log_message(cerr_strm);
     	   cerr_message(cerr_strm);
     	   cerr_strm.str(""); 



#if 0
     	   q.show("q:");
#endif 
 
     	}  
#endif /* |DEBUG_COMPILE|  */@; 

        *c = q;

#if 0 
        c->show("*c after assignment:");
#endif 


     }

   }  /* |if (c)| */

@q ***** (5) @>

   else
   {
       cerr_strm << thread_name 
                 << "WARNING!  In parser, rule `ellipse_primary: ellipse_primary NORMALIZED "
         	<< "with_transform_optional call_metapost_option_list':"
                 << endl 
                 << "`ellipse_primary' on right-hand side is NULL.  Can't normalize ellipse."
                 << endl 
                 << "Returning `ellipse_primary' from right-hand side as result unchanged."
                 << endl; 

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str(""); 
 
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   if (curr_transform != 0)
      *curr_transform = t;

   @=$$@> = static_cast<void*>(c);

@q ***** (5) @>

};

@q *** (3) ellipse_primary --> GET_BASE_ELLIPSE cone_primary.@>
@*1 \�ellipse primary> $\longrightarrow$ \.{GET\_BASE\_ELLIPSE}
\�cone primary>. 
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=ellipse_primary: GET_BASE_ELLIPSE cone_primary@>@/
{
@q ***** (5) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
     {
         cerr_strm << thread_name << "*** Parser:  `ellipse_primary: "
                   << "GET_BASE_ELLIPSE cone_primary'. ";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */

@q ***** (5) @>

   Cone *c = static_cast<Cone*>(@=$2@>);

   if (c->type == Shape::ELLIPTICAL_CONE_TYPE)
   {
       Ellipse *e = create_new<Ellipse>(0);

       *e = *(c->ellipses[0]);

       @=$$@> = static_cast<void*>(e);

   }

@q ***** (5) @>

   else
   {
      cerr << "WARNING!  In parser, rule `ellipse_primary: GET_BASE_ELLIPSE cone_primary':" 
           << endl 
           << "`cone_primary' isn't a elliptical cone:" << endl 
           << "c->type == " << c->type << " (" << type_name_map[c->type] << ")" 
           << endl
           << "Can't return base.  Will try to continue." << endl;

      @=$$@> = static_cast<void*>(0);

   }



@q ***** (5) @>

   delete c;
   c = 0;

};



@q ** (2) ellipse secondary.  @>
@*1 \�ellipse secondary>.

\LOG
\initials{LDF 2004.06.29.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipse_secondary@>
  
@q *** ellipse secondary --> ellipse_primary.  @>
@ \�ellipse secondary> $\longrightarrow$ \�ellipse primary>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=ellipse_secondary: ellipse_primary@>@/
{

  @=$$@> = @=$1@>;

};

@q *** (3) ellipse secondary --> ellipse_secondary transformer.  @>
@*2 \�ellipse secondary> $\longrightarrow$ \�ellipse secondary> 
\�transformer>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX:  Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=ellipse_secondary: ellipse_secondary transformer@>@/
{

  Ellipse* p = static_cast<Ellipse*>(@=$1@>);
  Transform* t = static_cast<Transform*>(@=$2@>);

  *p *= *t;

  delete t;

  @=$$@> = static_cast<void*>(p); 

};

@q *** (3) ellipse secondary --> ellipse_secondary REFLECTED_IN @>
@q *** (3) path_expression.@> 

@*2 \�ellipse secondary> $\longrightarrow$ 
\�ellipse secondary> 
\.{REFLECTED\_IN} \�path expression>.

\LOG
\initials{LDF 2004.10.05.}
Added this rule.

\initials{LDF 2004.12.03.}
Changed |REFLECTED IN| to |REFLECTED_IN|.

\initials{LDF 2005.10.24.}
Changed |path_like_expression| to |path_expression|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=ellipse_secondary: ellipse_secondary REFLECTED_IN @>
@=path_expression@>@/ 
{

     Ellipse* p = reflect_in_func<Ellipse>(static_cast<Scanner_Node>(parameter),
                                           static_cast<Ellipse*>(@=$1@>),
                                           static_cast<Path*>(@=$3@>));

     @=$$@> = static_cast<void*>(p);

};

@q ** (2) ellipse tertiary.  @>
@*1 \�ellipse tertiary>.

\LOG
\initials{LDF 2004.06.29.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipse_tertiary@>

@q *** ellipse tertiary --> ellipse_secondary.  @>
@ \�ellipse tertiary> $\longrightarrow$ \�ellipse secondary>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=ellipse_tertiary: ellipse_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q **** (4) ellipse_tertiary: ellipsoid_tertiary @> 
@q **** (4) INTERSECTION plane_secondary.        @> 

@*3 \�ellipse tertiary> $\longrightarrow$ 
\�ellipsoid tertiary>
\.{INTERSECTION} \�plane secondary>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.

\initials{LDF 2005.12.15.}
Changed \�path vector tertiary> to \�ellipse tertiary>.
I was using \�path vector tertiary> for testing.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>=
@=ellipse_tertiary: ellipsoid_tertiary INTERSECTION plane_secondary@>@/
{
#if 0 
   @=$$@> = Scan_Parse::ellipsoid_like_plane_intersection_func<Ellipsoid>(
                     static_cast<Ellipsoid*>(@=$1@>),
                     static_cast<Plane*>(@=$3@>),
                     parameter);
#else
   @=$$@> = static_cast<void*>(0); 
#endif 
};

@q ** (2) ellipse expression.  @>
@*1 \�ellipse expression>.
\initials{LDF 2004.06.29.}  

\LOG
\initials{LDF 2004.06.29.}  
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipse_expression@>

@q *** (3) ellipse expression --> ellipse_tertiary.  @>
@*2 \�ellipse expression> $\longrightarrow$ \�ellipse tertiary>.
\initials{LDF 2004.06.29.}

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=ellipse_expression: ellipse_tertiary@>@/
{
  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

