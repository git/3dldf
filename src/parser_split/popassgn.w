@q popassgn.w @> 
@q Created by Laurence Finston Sa Dez  4 18:48:15 CET 2004 @>

@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) Operation Assignments.  @>
@** Operation Assignments.  

Operation assignments cannot be used in 
assignment chains.  
\initials{LDF 2004.12.04.}

Multiplication by a |transform| with assignment is 
not an \�operation assignment>, but rather than a
\�transformation assignment command>,   
which can be chained.
Rules for \�transformation assignment commands> 
are defined in \filename{ptrfcmnd.w}.
\initials{LDF 2004.12.04.} 

@q * (1) operation_assignment.  @>
@* \�operation assignment>.
\initials{LDF 2004.12.04.}  

\LOG
\initials{LDF 2004.12.04.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> operation_assignment@>

@q ** (2) Non-vector types.@>  
@*1 Non-vector types.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q *** (3) |numerics|.@>  

@*2 {\bf numerics}.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> numeric_variable @> 
@q **** (4) PLUS_ASSIGN numeric_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�numeric variable>\.{PLUS\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=operation_assignment: numeric_variable PLUS_ASSIGN numeric_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
 
      } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               real* r = static_cast<real*>(entry->object);              

               *r += @=$3@>;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.04.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> numeric_variable @> 
@q **** (4) MINUS_ASSIGN numeric_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�numeric variable>\.{MINUS\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: numeric_variable MINUS_ASSIGN numeric_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
 
      } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               real* r = static_cast<real*>(entry->object);              

               *r -= @=$3@>;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.04.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> numeric_variable @> 
@q **** (4) TIMES_ASSIGN numeric_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�numeric variable>\.{TIMES\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: numeric_variable TIMES_ASSIGN numeric_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
 
      } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               real* r = static_cast<real*>(entry->object);              

               *r *= @=$3@>;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.04.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> numeric_variable @> 
@q **** (4) DIVIDE_ASSIGN numeric_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�numeric variable>\.{DIVIDE\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: numeric_variable DIVIDE_ASSIGN numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |numeric_expression == 0|.@>   

@ Error handling:  |numeric_expression == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (@=$3@> == ZERO_REAL)
   ;   /* Do nothing  */

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
   ;  /* Do nothing  */ 

@q ***** (5) |$3 != 0 && entry != 0|.@>   

@ |@=$3@> != 0 && entry != 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

    else /* |@=$3@> != 0 && entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               real* r = static_cast<real*>(entry->object);              

               *r /= @=$3@>;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|@=$3@> != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.04.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |colors|.@>  

@*2 {\bf colors}.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> color_variable            @> 
@q **** (4) PLUS_ASSIGN color_expression with_weight_optional. @>

@*3 \�operation assignment> $\longrightarrow$ 
\�color variable>\.{PLUS\_ASSIGN} \�color expression> \�with weight optional>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: color_variable PLUS_ASSIGN color_expression with_weight_optional@>@/
{

  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = true; /* |false| */
   if (DEBUG)
     {
          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "color_variable PLUS_ASSIGN color_expression with_weight_optional'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Color* c = static_cast<Color*>(@=$3@>);

@q ****** (6) Error handling:  |c == 0|.@>   

@ Error handling:  |c == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (c == static_cast<Color*>(0))
   ;

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
   ;

@q ***** (5) |c != 0 && entry != 0|.@>   

@ |c != 0 && entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |c != 0 && entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              delete c;

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
         {
            Color* q = static_cast<Color*>(entry->object);              

            if (@=$4@> == 0)
            {
               *q += *c;
            }
            else
            {
                status = q->add_color_with_weight(*c, @=$4@>, scanner_node);

#if 0 
                cerr << "`Color::add_color_with_weight' returned `status' == " << status << endl;
#endif 

            }

            delete c;

         }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|c != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=

@=%type <real_value> with_weight_optional@>

@q ** (2) @>
@
@<Define rules@>=
@=with_weight_optional: /* Empty  */ @>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = true; /* |false| */
   if (DEBUG)
   {
        cerr_strm << thread_name << "*** Parser: `with_weight_optional: /* Empty  */'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>=
@=with_weight_optional: WITH_WEIGHT numeric_expression @>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = true; /* |false| */
   if (DEBUG)
   {
        cerr_strm << thread_name << "*** Parser: `WITH_WEIGHT numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$2@>;
     

};


@q **** (4) operation_assignment --> color_variable @> 
@q **** (4) MINUS_ASSIGN color_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�color variable>\.{MINUS\_ASSIGN} \�color  expression>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: color_variable MINUS_ASSIGN color_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Color* c = static_cast<Color*>(@=$3@>);

@q ****** (6) Error handling:  |c == 0|.@>   

@ Error handling:  |c == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (c == static_cast<Color*>(0))
      ;  /* Do nothing  */ 

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
   ; /* Do nothing  */

@q ***** (5) |c != 0 && entry != 0|.@>   

@ |c != 0 && entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |c != 0 && entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              delete c;

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Color* q = static_cast<Color*>(entry->object);              

               *q -= *c;

               delete c;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|c != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> color_variable @> 
@q **** (4) TIMES_ASSIGN numeric_expression.        @>

@*3 \�operation assignment> $\longrightarrow$ 
\�color variable>\.{TIMES\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: color_variable TIMES_ASSIGN numeric_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   real r = @=$3@>;

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
         ; /* Do nothing  */ 

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Color* q = static_cast<Color*>(entry->object);              

               *q *= r;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|c != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> color_variable @> 
@q **** (4) DIVIDE_ASSIGN numeric_expression.        @>

@*3 \�operation assignment> $\longrightarrow$ 
\�color variable>\.{DIVIDE\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: color_variable DIVIDE_ASSIGN numeric_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "color_variable DIVIDE_ASSIGN numeric_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   real r = @=$3@>;

@q ****** (6) Error handling:  |r == 0|.@>   

@ Error handling:  |r == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (r == ZERO_REAL)
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "color_variable DIVIDE_ASSIGN numeric_expression':"
                   << endl << "`numeric_expression' == 0.  " 
                   << "Can't divide."
                   << endl << "Leaving value of `color_variable', "
                   << "if any, unchanged and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str(""); 

      } /* |if (c == 0)|  */   

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "color_variable DIVIDE_ASSIGN numeric_expression':"
                   << endl << "`color_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Can't multiply.  Will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                        << "`operation_assignment --> "
                        << endl 
                        << "color_variable DIVIDE_ASSIGN numeric_expression':"
                        << endl << "`color_variable' is unknown.  "
                        << "Can't subtract from it."
                        << endl 
                        << "Will try to continue.";

              log_message(cerr_strm);
              cerr_message(cerr_strm, error_stop_value);
              cerr_strm.str("");

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Color* q = static_cast<Color*>(entry->object);              

               *q /= r;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|c != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |strings|.@>  

@*2 {\bf strings}.
\initials{LDF 2005.01.21.}

\LOG
\initials{LDF 2005.01.21.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> string_variable PLUS_ASSIGN @>
@q **** (4) numeric_expression.                                  @>

@*3 \�operation assignment> $\longrightarrow$ \�string variable> 
\.{PLUS\_ASSIGN} \�numeric expression>.
\initials{LDF 2005.01.21.}

\LOG
\initials{LDF 2005.01.21.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: string_variable PLUS_ASSIGN numeric_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "string_variable PLUS_ASSIGN numeric_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.21.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "string_variable PLUS_ASSIGN numeric_expression':"
                   << endl << "`string_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Not assigning, but will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         string* s = static_cast<string*>(entry->object);

@q ****** (6).  |s == 0|.  Try to allocate memory for a new |string|.@> 

@ |s == 0|.  Try to allocate memory for a new |string|.
\initials{LDF 2005.01.21.}
  
@<Define rules@>=

         if (s == static_cast<string*>(0))
           {

              try 
                {
                   s = new string;     
                }

              catch (bad_alloc)
                {
                   
                   cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                             << "`operation_assignment --> "
                             << endl 
                             << "string_variable PLUS_ASSIGN numeric_expression':"
                             << endl << "Failed to create new `string'. " 
                             << "Rethrowing `bad_alloc'.";

                   log_message(cerr_strm);
                   cerr_message(cerr_strm, error_stop_value);
                   cerr_strm.str("");
 
                   throw;

                } /* |catch (bad_alloc)|  */
              
               entry->object = static_cast<void*>(s); 

           } /* |if (s == 0)|  */

@q ****** (6).@>           
 
         stringstream t;

         t << @=$3@>;

         *s += t.str();

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.21.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |points|.@>  

@*2 {\bf points}.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> point_variable @> 
@q **** (4) PLUS_ASSIGN point_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�point variable>\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: point_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "point_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Point* p = static_cast<Point*>(@=$3@>);

@q ****** (6) Error handling:  |p == 0|.@>   

@ Error handling:  |p == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (p == static_cast<Point*>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable PLUS_ASSIGN point_expression':"
                   << endl << "`point_expression' == 0.  " 
                   << "Leaving value of `point_variable', if any, "
                   << "unchanged and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str(""); 

      } /* |if (p == 0)|  */   

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable PLUS_ASSIGN point_expression':"
                   << endl << "`point_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Deleting `point_expression' and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |p != 0 && entry != 0|.@>   

@ |p != 0 && entry != 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

    else /* |p != 0 && entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                        << "`operation_assignment --> "
                        << endl 
                        << "point_variable PLUS_ASSIGN point_expression':"
                        << endl << "`point_variable' is unknown.  "
                        << "Can't add to it."
                        << endl 
                        << "Deleting `point_expression' and will "
                        << "try to continue.";

              log_message(cerr_strm);
              cerr_message(cerr_strm, error_stop_value);
              cerr_strm.str("");

              delete p;

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.04.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Point* q = static_cast<Point*>(entry->object);              

               *q += *p;

               delete p;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|p != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.04.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> point_variable @> 
@q **** (4) MINUS_ASSIGN point_expression.           @>

@*3 \�operation assignment> $\longrightarrow$ 
\�point variable>\.{MINUS\_ASSIGN} \�point expression>.
\initials{LDF 2004.12.06.}

\LOG
\initials{LDF 2004.12.06.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: point_variable MINUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "point_variable MINUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Point* p = static_cast<Point*>(@=$3@>);

@q ****** (6) Error handling:  |p == 0|.@>   

@ Error handling:  |p == 0|.
\initials{LDF 2004.12.06.}

@<Define rules@>=

   if (p == 0)
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable MINUS_ASSIGN point_expression':"
                   << endl << "`point_expression' == 0.  " 
                   << "Leaving value of `point_variable', if any, "
                   << "unchanged and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str(""); 

      } /* |if (p == 0)|  */   

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.06.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable MINUS_ASSIGN point_expression':"
                   << endl << "`point_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Deleting `point_expression' and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |p != 0 && entry != 0|.@>   

@ |p != 0 && entry != 0|.
\initials{LDF 2004.12.06.}

@<Define rules@>=

    else /* |p != 0 && entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.06.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                        << "`operation_assignment --> "
                        << endl 
                        << "point_variable MINUS_ASSIGN point_expression':"
                        << endl << "`point_variable' is unknown.  "
                        << "Can't subtract from it."
                        << endl 
                        << "Deleting `point_expression' and will "
                        << "try to continue.";

              log_message(cerr_strm);
              cerr_message(cerr_strm, error_stop_value);
              cerr_strm.str("");

              delete p;

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.06.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Point* q = static_cast<Point*>(entry->object);              

               *q -= *p;

               delete p;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|p != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.06.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> point_variable @> 
@q **** (4) TIMES_ASSIGN numeric_expression.        @>

@*3 \�operation assignment> $\longrightarrow$ 
\�point variable>\.{TIMES\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: point_variable TIMES_ASSIGN numeric_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "point_variable TIMES_ASSIGN numeric_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   real r = @=$3@>;

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable TIMES_ASSIGN numeric_expression':"
                   << endl << "`point_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Can't multiply.  Will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                        << "`operation_assignment --> "
                        << endl 
                        << "point_variable TIMES_ASSIGN numeric_expression':"
                        << endl << "`point_variable' is unknown.  "
                        << "Can't subtract from it."
                        << endl 
                        << "Will try to continue.";

              log_message(cerr_strm);
              cerr_message(cerr_strm, error_stop_value);
              cerr_strm.str("");

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Point* q = static_cast<Point*>(entry->object);              

               *q *= r;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|p != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> point_variable @> 
@q **** (4) DIVIDE_ASSIGN numeric_expression.        @>

@*3 \�operation assignment> $\longrightarrow$ 
\�point variable>\.{DIVIDE\_ASSIGN} \�numeric expression>.
\initials{LDF 2004.12.18.}

\LOG
\initials{LDF 2004.12.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: point_variable DIVIDE_ASSIGN numeric_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "point_variable DIVIDE_ASSIGN numeric_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   real r = @=$3@>;

@q ****** (6) Error handling:  |r == 0|.@>   

@ Error handling:  |r == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   if (r == ZERO_REAL)
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable DIVIDE_ASSIGN numeric_expression':"
                   << endl << "`numeric_expression' == 0.  " 
                   << "Can't divide."
                   << endl << "Leaving value of `point_variable', "
                   << "if any, unchanged and will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str(""); 

      } /* |if (p == 0)|  */   

@q ****** (6) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

   else if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "point_variable DIVIDE_ASSIGN numeric_expression':"
                   << endl << "`point_variable' is invalid, i.e., " 
                   << "`entry == 0'.  "
                   << "Can't multiply.  Will try to continue.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |else if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.18.}

@<Define rules@>=

    else /* |entry != 0|  */
      {

@q ****** (6).  Error handling:  |entry->object == 0|.@> 

@ Error handling:  |entry->object == 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         if (entry->object == static_cast<void*>(0))
           {

              cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                        << "`operation_assignment --> "
                        << endl 
                        << "point_variable DIVIDE_ASSIGN numeric_expression':"
                        << endl << "`point_variable' is unknown.  "
                        << "Can't subtract from it."
                        << endl 
                        << "Will try to continue.";

              log_message(cerr_strm);
              cerr_message(cerr_strm, error_stop_value);
              cerr_strm.str("");

           } /* |if (entry->object == 0)|  */

@q ****** (6).  |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.18.}
  
@<Define rules@>=

         else /* |entry->object != 0|  */
            {
               Point* q = static_cast<Point*>(entry->object);              

               *q /= r;

            }   /* |else| (|entry->object != 0|)  */                

@q ****** (6).@> 

      }  /* |else| (|p != 0 && entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |paths|.@>  

@*2 {\bf paths}.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> path_variable PLUS_ASSIGN @>
@q **** (4) point_expression.                                  @>

@*3 \�operation assignment> $\longrightarrow$ \�path variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2004.10.27.}

\LOG
\initials{LDF 2004.10.27.}
Added this rule.

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|path_assignment| to |operation_assignment|.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 

\initials{LDF 2005.02.11.}
Changed \�path variable> to \�path-like variable>.

\initials{LDF 2005.04.30.}
Changed \�path-like variable> back to \�path variable>.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: path_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "path_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2004.10.27.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "path_variable PLUS_ASSIGN point_expression':"
                   << endl << "`path_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ***** (5) Error handling:  |entry| has invalid type.@>   
@ Error handling:  |entry| has invalid type.
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this section.
\ENDLOG

@<Define rules@>=

   else if (!(   entry->type == PATH
              || entry->type == POLYGON
              || entry->type == TRIANGLE
              || entry->type == RECTANGLE
              || entry->type == REG_POLYGON
              || entry->type == ELLIPSE
              || entry->type == SUPERELLIPSE
              || entry->type == CIRCLE))

      {
          cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                    << "`operation_assignment --> "
                    << endl 
                    << "path_variable PLUS_ASSIGN point_expression':"
                    << endl << "`entry' has an invalid `type':  " 
                    << name_map[entry->type] << "."
                    << endl << "Deleting `point_expression'.";
         
          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");
 
          delete p;

      }  /* |else|  (|entry->type| is invalid.)  */

@q ***** (5) |entry != 0| and |entry->type| is valid.@>   

@ |entry != 0| and |entry->type| is valid.
\initials{LDF Undated.}

@<Define rules@>=

    else /* |entry != 0| and |entry->type| is valid.  */
      {

         Path* q = static_cast<Path*>(entry->object);

@q ****** (6).  |q == 0|.  Try to allocate memory for a object.@> 

@ |q == 0|.  Try to allocate memory for a new object.
\initials{LDF 2004.10.27.}

\LOG
\initials{LDF 2005.02.11.}
Now creating a new |Path|, if |entry->type == PATH|,
a new |Polygon|, if |entry->type == POLYGON|, etc.
If |entry->type != PATH|, the pointer to the new object 
is cast to |Path*|.
\ENDLOG 
  
@<Define rules@>=

         if (q == static_cast<Path*>(0))
           {

              try 
                {
                   if (entry->type == PATH)
                      q = create_new<Path>(0);     
                   else if (entry->type == POLYGON)
                      q = static_cast<Path*>(create_new<Polygon>(0)); 
                   else if (entry->type == TRIANGLE)
                      q = static_cast<Path*>(create_new<Triangle>(0)); 
                   else if (entry->type == RECTANGLE)
                      q = static_cast<Path*>(create_new<Rectangle>(0)); 
                   else if (entry->type == REG_POLYGON)
                      q = static_cast<Path*>(create_new<Reg_Polygon>(0)); 
                   else if (entry->type == ELLIPSE)
                      q = static_cast<Path*>(create_new<Ellipse>(0)); 
                   else if (entry->type == SUPERELLIPSE)
                      q = static_cast<Path*>(create_new<Superellipse>(0)); 
                   else if (entry->type == CIRCLE)
                      q = static_cast<Path*>(create_new<Circle>(0)); 
                      
                }  /* |try|  */

              catch (bad_alloc)
                {
                   
                   cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                             << "`operation_assignment --> "
                             << endl 
                             << "path_variable PLUS_ASSIGN point_expression':"
                             << endl << "Failed to create new object. " 
                             << "Deleting `point_expression' and rethrowing "
                             << "`bad_alloc'.";

                   log_message(cerr_strm);
                   cerr_message(cerr_strm, error_stop_value);
                   cerr_strm.str("");
 
                   delete p;
 
                   throw;

                } /* |catch (bad_alloc)|  */

           } /* |if (q == 0)|  */

@q ****** (6).@>           

         *q += *p;

         entry->object = static_cast<void*>(q);

         delete p;

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |entry->object|
upon success and 0 upon failure.
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> path_variable PLUS_ASSIGN path_join.@>

@*3 \�operation assignment> $\longrightarrow$ \�path variable>
\.{PLUS\_ASSIGN} \�path join>.
\initials{LDF 2004.10.27.}

\LOG
\initials{LDF 2004.10.27.}
Added this rule.

\initials{LDF 2004.11.01.}
Changed |path_variable| to |path_like_variable|.  This change made it
impossible to allocate memory for |entry->object| if it's null,
because it's not known what type it should be.  It would be possible
to find out by examining |entry->type| and proceding accordingly, 
but I don't think it would be worthwhile.

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|path_assignment| to |operation_assignment|.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 

\initials{LDF 2005.10.24.}
Changed |path_like_variable| to |path_variable|.
Removed debugging code.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: path_variable PLUS_ASSIGN path_join@>@/
{

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE

  DEBUG = false; /* |true| */

  if (DEBUG)
  { 
     cerr << "*** Parser:  Rule `operation_assignment: path_variable PLUS_ASSIGN path_join'."
          << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);
  Path *p = 0;

#if 0 
  c->show("*c:");
#endif 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry->object == 0)
  {
    p = create_new<Path>(0);
    entry->object = static_cast<void*>(p);
  }
  else 
    p = static_cast<Path*>(entry->object); 

  *p += c;

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |entry->object|
upon success and 0 upon failure. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> path_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�path variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2004.11.01.}

\LOG
\initials{LDF 2004.11.01.}
Added this rule.

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|path_assignment| to |operation_assignment|.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 

\initials{LDF 2005.02.11.}
Changed \�path variable> to \�path-like variable>.

\initials{LDF 2005.04.30.}
Changed \�path-like variable> back to \�path variable>.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: path_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2004.11.01.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Path* p = static_cast<Path*>(entry->object);

@q ****** (6).  |p == 0|.  Try to allocate memory for a new |Path|.@> 

@ |p == 0|.  Try to allocate memory for a new |Path|.
\initials{LDF 2004.11.01.}

\LOG
\initials{LDF 2005.02.21.}
Added code for allocating memory for an object of the type 
indicated by |entry->type|.
\ENDLOG 
  
@<Define rules@>=

         if (p == static_cast<Path*>(0))
           {

@q ******* (7) @>

if (entry->type == PATH)
                      p = create_new<Path>(0);     
                   else if (entry->type == POLYGON)
                      p = create_new<Polygon>(0);      
                   else if (entry->type == TRIANGLE)
                      p = create_new<Triangle>(0);      
                   else if (entry->type == REG_POLYGON)
                      p = create_new<Reg_Polygon>(0);      
                   else if (entry->type == RECTANGLE)
                      p = create_new<Rectangle>(0);      
                   else if (entry->type == ELLIPSE)
                      p = create_new<Ellipse>(0);      
                   else if (entry->type == SUPERELLIPSE)
                      p = create_new<Superellipse>(0);      
                   else if (entry->type == CIRCLE)
                      p = create_new<Circle>(0);      
                   else 
                      {

                      }  /* |else| (|entry->type| is invalid).  */

@q ******* (7) @> 

              entry->object = static_cast<void*>(p); 

           } /* |if (p == 0)|  */

@q ****** (6).@>           

         if (p != static_cast<Path*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |entry->object|
upon success and 0 upon failure. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> path_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�path variable>
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2004.11.01.}

\LOG
\initials{LDF 2004.11.01.}
Added this rule.

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|path_assignment| to |operation_assignment|.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 

\initials{LDF 2005.02.21.}
Changed \�path variable> to \�path-like variable>.

\initials{LDF 2005.10.24.}
Changed |path_like_variable| to |path_variable|.
Removed debugging code.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: path_variable MINUS_ASSIGN CYCLE@>@/ 
{

    Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         /* Do nothing.  */          
      }

@q ***** (5) |entry != 0|.@>   
@
\LOG
\initials{LDF 2005.02.21.}
Removed code for allocating memory for a new |Path|, 
if |entry->object == 0|.  There's no need to make an unknown 
|Path| non-cyclical.
\ENDLOG

@<Define rules@>=

    else /* |entry != 0|  */
      {

         Path* p = static_cast<Path*>(entry->object);

         if (p != static_cast<Path*>(0))
            {
                p->set_cycle(false);

            }   /* |if (p != 0)|  */

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |entry->object|
upon success and 0 upon failure. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> circle_variable PLUS_ASSIGN @>
@q **** (4) point_expression.                                    @>

@*3 \�operation assignment> $\longrightarrow$ \�circle variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2022.11.03.}

\LOG
\initials{LDF 2022.11.03.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: circle_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "circle_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.03.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "circle_variable PLUS_ASSIGN point_expression':"
                   << endl << "`circle_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ***** (5) Error handling:  |entry| has invalid type.@>   
@ Error handling:  |entry| has invalid type.
\initials{LDF 2022.11.03.}

@<Define rules@>=

   else if (!CIRCLE)
   {
       cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                 << "`operation_assignment --> "
                 << endl 
                 << "circle_variable PLUS_ASSIGN point_expression':"
                 << endl << "`entry' has an invalid `type':  " 
                 << name_map[entry->type] << "."
                 << endl << "Deleting `point_expression'.";
      
       log_message(cerr_strm);
       cerr_message(cerr_strm, error_stop_value);
       cerr_strm.str("");
 
       delete p;

   }  /* |else|  (|entry->type| is invalid.)  */

@q ***** (5) |entry != 0| and |entry->type| is valid.@>   

@ |entry != 0| and |entry->type| is valid.
\initials{LDF 2022.11.03.}

@<Define rules@>=

    else /* |entry != 0| and |entry->type| is valid.  */
    {

       Circle* q = static_cast<Circle*>(entry->object);

@q ****** (6).  |q == 0|.  Try to allocate memory for a object.@> 

@ |q == 0|.  Try to allocate memory for a new object.
\initials{LDF 2022.11.03.}

@<Define rules@>=

       if (q == 0)
       {
          q = create_new<Circle>(0); 
          entry->object = static_cast<void*>(q);
                    
       } /* |if (q == 0)|  */

@q ****** (6).@>           

       *q += *p;

       delete p;

    }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2022.11.03.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> circle_variable PLUS_ASSIGN path_join.@>

@*3 \�operation assignment> $\longrightarrow$ \�circle variable>
\.{PLUS\_ASSIGN} \�path join>.
\initials{LDF 2022.11.03.}

\LOG
\initials{LDF 2022.11.03.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: circle_variable PLUS_ASSIGN path_join@>@/
{

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE

  DEBUG = false; /* |true| */

  if (DEBUG)
  { 
     cerr << "*** Parser:  Rule `operation_assignment: circle_variable PLUS_ASSIGN path_join'."
          << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  Circle *q = 0;

  Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);

#if 0 
  c->show("*c:");
#endif 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
   {
      q = create_new<Circle>(0);

      entry->object = static_cast<void*>(q);

   } /* |if (entry == 0 || entry->object == 0)|  */
   else
      q = static_cast<Circle*>(entry->object);           

   *q += c;

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.03.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> circle_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�circle variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.03.}

\LOG
\initials{LDF 2022.11.03.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: circle_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.03.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Circle* p = static_cast<Circle*>(entry->object);

@q ****** (6).  |p == 0|.  Try to allocate memory for a new |Circle|.@> 

@ |p == 0|.  Try to allocate memory for a new |Circle|.
\initials{LDF 2022.11.03.}

@<Define rules@>=

         if (p == 0)
         {
            p = create_new<Circle>(0);      

            entry->object = static_cast<void*>(p); 

         } /* |if (p == 0)|  */

@q ****** (6).@>           

         if (p != static_cast<Circle*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.03.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> circle_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�circle variable>
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.03.}

\LOG
\initials{LDF 2022.11.03.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: circle_variable MINUS_ASSIGN CYCLE@>@/ 
{

    Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         /* Do nothing.  */          
      }

@q ***** (5) |entry != 0|.@>   
@
@<Define rules@>=

    else /* |entry != 0|  */
      {

         Circle* p = static_cast<Circle*>(entry->object);

         if (p != static_cast<Circle*>(0))
            {
                p->set_cycle(false);

            }   /* |if (p != 0)|  */

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.03.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> ellipse_variable PLUS_ASSIGN @>
@q **** (4) point_expression.                                     @>

@*3 \�operation assignment> $\longrightarrow$ \�ellipse variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: ellipse_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "ellipse_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.06.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "ellipse_variable PLUS_ASSIGN point_expression':"
                   << endl << "`ellipse_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ***** (5) Error handling:  |entry| has invalid type.@>   
@ Error handling:  |entry| has invalid type.
\initials{LDF 2022.11.06.}

@<Define rules@>=

   else if (!entry->type == ELLIPSE)
   {
       cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                 << "`operation_assignment --> "
                 << endl 
                 << "ellipse_variable PLUS_ASSIGN point_expression':"
                 << endl << "`entry' has an invalid `type':  " 
                 << name_map[entry->type] << "."
                 << endl << "Deleting `point_expression'.";
      
       log_message(cerr_strm);
       cerr_message(cerr_strm, error_stop_value);
       cerr_strm.str("");
 
       delete p;

   }  /* |else|  (|entry->type| is invalid.)  */

@q ***** (5) |entry != 0| and |entry->type| is valid.@>   

@ |entry != 0| and |entry->type| is valid.
\initials{LDF 2022.11.06.}

@<Define rules@>=

   else /* |entry != 0| and |entry->type| is valid.  */
   {

       Ellipse* q = static_cast<Ellipse*>(entry->object);

@q ****** (6).  |q == 0|.  Try to allocate memory for a object.@> 

@ |q == 0|.  Try to allocate memory for a new object.
\initials{LDF 2022.11.06.}

@<Define rules@>=

       if (q == 0)
       {
          q = create_new<Ellipse>(0); 
          entry->object = static_cast<void*>(q);
                    
       } /* |if (q == 0)|  */

@q ****** (6).@>           

       *q += *p;

       delete p;

   }  /* |else| (|entry != 0|)  */

   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2022.11.06.}

@<Define rules@>=

   @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> ellipse_variable PLUS_ASSIGN path_join.@>

@*3 \�operation assignment> $\longrightarrow$ \�ellipse variable>
\.{PLUS\_ASSIGN} \�path join>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: ellipse_variable PLUS_ASSIGN path_join@>@/
{

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE

  DEBUG = false; /* |true| */

  if (DEBUG)
  { 
     cerr << "*** Parser:  Rule `operation_assignment: ellipse_variable PLUS_ASSIGN path_join'."
          << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);
  Ellipse *e = 0;

#if 0 
  c->show("*c:");
#endif 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0 || entry->object == 0|.@>   
@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2022.11.06.}

@<Define rules@>=


   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
   {
      e = create_new<Ellipse>(0);

      entry->object = static_cast<void*>(e);

   } /* |if (entry == 0 || entry->object == 0)|  */

   else
      e = static_cast<Ellipse*>(entry->object);           

   *e += c;

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.06.}

@<Define rules@>=


  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> ellipse_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�ellipse variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: ellipse_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.06.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Ellipse* p = static_cast<Ellipse*>(entry->object);

@q ****** (6).  |p == 0|.  Try to allocate memory for a new |Ellipse|.@> 

@ |p == 0|.  Try to allocate memory for a new |Ellipse|.
\initials{LDF 2022.11.06.}

@<Define rules@>=

         if (p == 0)
         {
            p = create_new<Ellipse>(0);      

            entry->object = static_cast<void*>(p); 

         } /* |if (p == 0)|  */

@q ****** (6).@>           

         if (p != static_cast<Ellipse*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.06.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> ellipse_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�ellipse variable>
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: ellipse_variable MINUS_ASSIGN CYCLE@>@/ 
{

    Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         /* Do nothing.  */          
      }

@q ***** (5) |entry != 0|.@>   
@
@<Define rules@>=

    else /* |entry != 0|  */
      {

         Ellipse* p = static_cast<Ellipse*>(entry->object);

         if (p != static_cast<Ellipse*>(0))
            {
                p->set_cycle(false);

            }   /* |if (p != 0)|  */

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.06.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> reg_polygon_variable PLUS_ASSIGN @>
@q **** (4) point_expression.                                         @>

@*3 \�operation assignment> $\longrightarrow$ \�reg_polygon variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: reg_polygon_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "reg_polygon_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.16.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "reg_polygon_variable PLUS_ASSIGN point_expression':"
                   << endl << "`reg_polygon_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ***** (5) Error handling:  |entry| has invalid type.@>   
@ Error handling:  |entry| has invalid type.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   else if (!entry->type == REG_POLYGON)
   {
       cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                 << "`operation_assignment --> "
                 << endl 
                 << "reg_polygon_variable PLUS_ASSIGN point_expression':"
                 << endl << "`entry' has an invalid `type':  " 
                 << name_map[entry->type] << "."
                 << endl << "Deleting `point_expression'.";
      
       log_message(cerr_strm);
       cerr_message(cerr_strm, error_stop_value);
       cerr_strm.str("");
 
       delete p;

   }  /* |else|  (|entry->type| is invalid.)  */

@q ***** (5) |entry != 0| and |entry->type| is valid.@>   

@ |entry != 0| and |entry->type| is valid.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   else /* |entry != 0| and |entry->type| is valid.  */
   {

       Reg_Polygon* r = static_cast<Reg_Polygon*>(entry->object);

@q ****** (6).  |r == 0|.  Try to allocate memory for a object.@> 

@ |r == 0|.  Try to allocate memory for a new object.
\initials{LDF 2022.11.16.}

@<Define rules@>=

       if (r == 0)
       {
          r = create_new<Reg_Polygon>(0); 
          entry->object = static_cast<void*>(r);
                    
       } /* |if (r == 0)|  */

@q ****** (6).@>           

       *r += *p;

       delete p;

   }  /* |else| (|entry != 0|)  */

   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> reg_polygon_variable PLUS_ASSIGN path_join.@>

@*3 \�operation assignment> $\longrightarrow$ \�reg_polygon variable>
\.{PLUS\_ASSIGN} \�path join>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: reg_polygon_variable PLUS_ASSIGN path_join@>@/
{

  @<Common declarations for rules@>@; 

  Reg_Polygon* r = 0;

#if DEBUG_COMPILE

  DEBUG = false; /* |true| */

  if (DEBUG)
  { 
     cerr << "*** Parser:  Rule `operation_assignment: reg_polygon_variable PLUS_ASSIGN path_join'."
          << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);

#if 0 /* 1  */
  c->show("*c:");
#endif 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0 || entry->object == 0|.@>   
@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
   {
      r = create_new<Reg_Polygon>(0);

      entry->object = static_cast<void*>(r);

   } /* |if (entry == 0 || entry->object == 0)|  */

@q ****** (6).  |entry != 0 && entry->object != 0|.@> 

@ |entry != 0 && entry->object != 0|.
\initials{LDF 2022.11.16.}
  
@<Define rules@>=

   else
      r = static_cast<Reg_Polygon*>(entry->object);           

   *r += c;

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=


  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> reg_polygon_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�reg_polygon variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: reg_polygon_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Reg_Polygon* p = static_cast<Reg_Polygon*>(entry->object);

@q ****** (6).  |p == 0|.  Try to allocate memory for a new |Reg_Polygon|.@> 

@ |p == 0|.  Try to allocate memory for a new |Reg_Polygon|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

         if (p == 0)
         {
            p = create_new<Reg_Polygon>(0);      

            entry->object = static_cast<void*>(p); 

         } /* |if (p == 0)|  */

@q ****** (6).@>           

         if (p != static_cast<Reg_Polygon*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> reg_polygon_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�reg_polygon variable>
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: reg_polygon_variable MINUS_ASSIGN CYCLE@>@/ 
{

    Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         /* Do nothing.  */          
      }

@q ***** (5) |entry != 0|.@>   
@
@<Define rules@>=

    else /* |entry != 0|  */
      {

         Reg_Polygon* p = static_cast<Reg_Polygon*>(entry->object);

         if (p != static_cast<Reg_Polygon*>(0))
            {
                p->set_cycle(false);

            }   /* |if (p != 0)|  */

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};


@q *** (3) |nurbs|.@>  

@*2 {\bf nurbs}.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> nurb_variable PLUS_ASSIGN point_expression.@>

@*3 \�operation assignment> $\longrightarrow$ \�nurb variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: nurb_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser:  "
                    << "`operation_assignment --> "
                    << "nurb_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.26.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "nurb_variable PLUS_ASSIGN point_expression':"
                   << endl << "`nurb_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Nurb* q = static_cast<Nurb*>(entry->object);

@q ******* (7).  |q == 0|.  Try to allocate memory for a new |Nurb|.@> 

@ |q == 0|.  Try to allocate memory for a new |Nurb|.
\initials{LDF 2005.01.26.}
  
@<Define rules@>=

         if (q == static_cast<Nurb*>(0))
           {

              try 
                {
                   q = new Nurb;     
                }

              catch (bad_alloc)
                {
                   
                   cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                             << "`operation_assignment --> "
                             << endl 
                             << "nurb_variable PLUS_ASSIGN point_expression':"
                             << endl << "Failed to create new `nurb'. " 
                             << "Deleting `point_expression' and rethrowing "
                             << "`bad_alloc'.";

                   log_message(cerr_strm);
                   cerr_message(cerr_strm, error_stop_value);
                   cerr_strm.str("");
 
                   delete p;
 
                   throw;

                } /* |catch (bad_alloc)|  */

           } /* |if (q == 0)|  */

@q ******* (7).@>           

         *q += p;

         entry->object = static_cast<void*>(q);

      }  /* |else| (|entry != 0|)  */
   
@q ****** (6)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.26.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> nurb_variable PLUS_ASSIGN @>
@q **** (4) knot_or_weight numeric_expression.                 @>

@*3 \�operation assignment> $\longrightarrow$ \�nurb variable> 
\.{PLUS\_ASSIGN} \�knot or weight> \�numeric expression>.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=
@=operation_assignment: nurb_variable PLUS_ASSIGN knot_or_weight@>@/
@=numeric_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser:  "
                    << "`operation_assignment --> "
                    << "nurb_variable PLUS_ASSIGN knot_or_weight "
                    << "numeric_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  real r = @=$4@>; 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.26.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "nurb_variable PLUS_ASSIGN knot_or_weight numeric_expression':"
                   << endl << "`nurb_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `numeric_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
      } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Nurb* q = static_cast<Nurb*>(entry->object);

@q ******* (7).  |q == 0|.  Try to allocate memory for a new |Nurb|.@> 

@ |q == 0|.  Try to allocate memory for a new |Nurb|.
\initials{LDF 2005.01.26.}
  
@<Define rules@>=

         if (q == static_cast<Nurb*>(0))
           {

              try 
                {
                   q = new Nurb;     
                }

              catch (bad_alloc)
                {
                   
                   cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                             << "`operation_assignment --> "
                             << endl 
                             << "nurb_variable PLUS_ASSIGN knot_or_weight "
                             << "numeric_expression':"
                             << endl << "Failed to create new `nurb'. " 
                             << "Deleting `numeric_expression' and rethrowing "
                             << "`bad_alloc'.";

                   log_message(cerr_strm);
                   cerr_message(cerr_strm, error_stop_value);
                   cerr_strm.str("");
 
                   throw;

                } /* |catch (bad_alloc)|  */

           } /* |if (q == 0)|  */

@q ******* (7).@>           

         q->append_knot_or_weight(r, @=$3@>, scanner_node);

         entry->object = static_cast<void*>(q);

      }  /* |else| (|entry != 0|)  */
   
@q ****** (6)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.26.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) knot_or_weight.@>   
@*3 \�knot or weight>.
\initials{LDF 2005.01.27.}

\LOG
\initials{LDF 2005.01.27.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> knot_or_weight@>

@q ***** (5) knot_or_weight --> KNOT.@> 

@*4 \�knot or weight> $\longrightarrow$ \.{KNOT}.
\initials{LDF 2005.01.27.}

\LOG
\initials{LDF 2005.01.27.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=knot_or_weight: KNOT@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser:  `knot_or_weight --> "
                    << "KNOT'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0;
   
};

@*4 \�knot or weight> $\longrightarrow$ \.{WEIGHT}.
\initials{LDF 2005.01.27.}

\LOG
\initials{LDF 2005.01.27.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=knot_or_weight: WEIGHT@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser:  `knot_or_weight --> "
                    << "WEIGHT'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 1;
   
};

@q *** (3) |cuboids|.  @>

@ |cuboids|.  
\initials{LDF 2022.11.17.}

\LOG
Added this section.
\ENDLOG

@q **** (4) operation_assignment: cuboid_variable PLUS_ASSIGN path_expression @>

@ \�operation assignment> $\longrightarrow$  \$cuboid variable> \.{PLUS\_ASSIGN} \�path expression>.
\initials{LDF 2022.11.17.}

\LOG
\initials{LDF 2022.11.17.}
Added this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: cuboid_variable PLUS_ASSIGN path_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN path_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   status = cuboid_plus_assign_rule_func(scanner_node, @=$1@>, @=$3@>, Shape::PATH_TYPE);

   if (status != 0)
   {
       cerr_strm << thread_name << "ERROR!  In parser, rule `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::cuboid_plus_assign_rule_func' failed, returning " << status << "."
                 << endl
                 << "Failed to append `path' (`rectangle') to `cuboid'."
                 << endl 
                 << "Continuing."
                 << endl;
                   

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << thread_name << "In parser, rule `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::cuboid_plus_assign_rule_func' succeeded, returning 0."
                 << endl
                 << "Appended `path' (`rectangle') to `cuboid' successfully."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment: cuboid_variable PLUS_ASSIGN rectangle_expression @>

@ \�operation assignment> $\longrightarrow$  \$cuboid variable> \.{PLUS\_ASSIGN} \�rectangle expression>.
\initials{LDF 2022.11.21.}

\LOG
\initials{LDF 2022.11.21.}
Added this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: cuboid_variable PLUS_ASSIGN rectangle_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN rectangle_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   status = cuboid_plus_assign_rule_func(scanner_node, @=$1@>, @=$3@>, Shape::RECTANGLE_TYPE);

   if (status != 0)
   {
       cerr_strm << thread_name << "ERROR!  In parser, rule `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN rectangle_expression':"
                 << endl
                 << "`Scan_Parse::cuboid_plus_assign_rule_func' failed, returning " << status << "."
                 << endl
                 << "Failed to append `rectangle' to `cuboid'."
                 << endl 
                 << "Continuing."
                 << endl;
                   

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << thread_name << "In parser, rule `operation_assignment --> "
                 << "cuboid_variable PLUS_ASSIGN rectangle_expression':"
                 << endl
                 << "`Scan_Parse::cuboid_plus_assign_rule_func' succeeded, returning 0."
                 << endl
                 << "Appended `rectangle' to `cuboid' successfully."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) |Polyhedra|.  @>

@ |Polyhedrons|.  
\initials{LDF 2022.11.18.}

\LOG
Added this section.
\ENDLOG

@q **** (4) operation_assignment: polyhedron_variable PLUS_ASSIGN path_expression @>

@ \�operation assignment> $\longrightarrow$  \$polyhedron variable> \.{PLUS\_ASSIGN} \�path expression>.
\initials{LDF 2022.11.18.}

\LOG
\initials{LDF 2022.11.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: polyhedron_variable PLUS_ASSIGN path_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "polyhedron_variable PLUS_ASSIGN path_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   status = polyhedron_plus_assign_rule_func(scanner_node, @=$1@>, @=$3@>);

   if (status != 0)
   {
       cerr_strm << thread_name << "ERROR!  In parser, rule `operation_assignment --> "
                 << "polyhedron_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::polyhedron_plus_assign_rule_func' failed, returning " << status << "."
                 << endl
                 << "Failed to append `path' (`reg_polygon') to `polyhedron'."
                 << endl 
                 << "Continuing."
                 << endl;
                   

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << thread_name << "In parser, rule `operation_assignment --> "
                 << "polyhedron_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::polyhedron_plus_assign_rule_func' succeeded, returning 0."
                 << endl
                 << "Appended `path' (`reg_polygon') to `polyhedron' successfully."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) |polyhedrons|.  @>

@ |polyhedrons|.  
\initials{LDF 2022.11.30.}

\LOG
\initials{LDF 2022.11.30.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment: polyhedron_variable PLUS_ASSIGN reg_polygon_expression @>

@ \�operation assignment> $\longrightarrow$  \$polyhedron variable> \.{PLUS\_ASSIGN} \�reg polygon expression>.
\initials{LDF 2022.11.30.}

\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: polyhedron_variable PLUS_ASSIGN reg_polygon_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "polyhedron_variable PLUS_ASSIGN reg_polygon_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry && entry->object)
   {
      Polyhedron *p = static_cast<Polyhedron*>(entry->object);

      p->reg_polygons.push_back(static_cast<Reg_Polygon*>(@=$3@>));
   }
   else
   {
       cerr_strm << thread_name << "WARNING!  In parser, rule `operation_assignment: "
                 << "polyhedron_variable PLUS_ASSIGN reg_polygon_expression':"
                 << endl
                 << "`polyhedron_variable' is NULL.  Can't add `reg_polygon' to `polyhedron'."
                 << endl 
                 << "Continuing.";
                 
         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }

   @=$$@> = static_cast<void*>(0);

};


@q *** (3) |spheres|.  @>

@ |spheres|.  
\initials{LDF 2022.10.21.}

\LOG
\initials{LDF 2022.10.21.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment: sphere_variable PLUS_ASSIGN path_expression @>

@ \�operation assignment> $\longrightarrow$  \$sphere variable> \.{PLUS\_ASSIGN} \�path expression>.
\initials{LDF 2022.10.21.}

\LOG
\initials{LDF 2022.10.21.}
Added this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: sphere_variable PLUS_ASSIGN path_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "sphere_variable PLUS_ASSIGN path_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   status = sphere_plus_assign_rule_func(scanner_node, @=$1@>, @=$3@>);

   if (status != 0)
   {
       cerr_strm << thread_name << "ERROR!  In parser, rule `operation_assignment --> "
                 << "sphere_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::sphere_plus_assign_rule_func' failed, returning " << status << "."
                 << endl
                 << "Failed to append `path' (`circle') to `sphere'."
                 << endl 
                 << "Continuing."
                 << endl;
                   

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << thread_name << "In parser, rule `operation_assignment --> "
                 << "sphere_variable PLUS_ASSIGN path_expression':"
                 << endl
                 << "`Scan_Parse::sphere_plus_assign_rule_func' succeeded, returning 0."
                 << endl
                 << "Appended `path' (`circle') to `sphere' successfully."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment: sphere_variable PLUS_ASSIGN circle_expression @>

@ \�operation assignment> $\longrightarrow$  \$sphere variable> \.{PLUS\_ASSIGN} \�circle expression>.
\initials{LDF 2022.10.21.}

\LOG
\initials{LDF 2022.10.21.}
Added this rule.

\initials{LDF 2022.11.29.}
Rewrote this rule.
\ENDLOG

@q ***** (5) Definition @>

@<Define rules@>=
@=operation_assignment: sphere_variable PLUS_ASSIGN circle_expression @>
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                 << "sphere_variable PLUS_ASSIGN circle_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   Sphere *s = 0;
   Circle *c = static_cast<Circle*>(@=$3@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry && entry->object)
   {
       s = static_cast<Sphere*>(entry->object);
       s->circles.push_back(c);
   }
    
   @=$$@> = static_cast<void*>(0);

};


@q *** (3) parabolae.@> 
@*2 {\bf parabolae}.
\initials{LDF 2005.11.18.}

\LOG
\initials{LDF 2005.11.18.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> parabola_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�parabola variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2005.11.18.}

\LOG
\initials{LDF 2005.11.18.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: parabola_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2005.11.18.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Parabola* p = static_cast<Parabola*>(entry->object);

         if (p == static_cast<Parabola*>(0))
           {

                p = create_new<Parabola>(0);     

                entry->object = static_cast<void*>(p); 

           } /* |if (p == 0)|  */

         if (p != static_cast<Parabola*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2005.11.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> parabola_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�parabola variable> 
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2005.11.18.}

\LOG
\initials{LDF 2005.11.18.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: parabola_variable MINUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2005.11.18.}
@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Parabola* p = static_cast<Parabola*>(entry->object);

         if (p == static_cast<Parabola*>(0))
           {

                p = create_new<Parabola>(0);     

                entry->object = static_cast<void*>(p); 

           } /* |if (p == 0)|  */

         if (p != static_cast<Parabola*>(0))
            p->set_cycle(false);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2005.11.18.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |rectangles|.@>  

@*2 {\bf rectangles}.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> rectangle_variable @>
@q **** (4) PLUS_ASSIGN point_expression.               @>

@*3 \�operation assignment> $\longrightarrow$ \�rectangle variable> 
\.{PLUS\_ASSIGN} \�point expression>.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: rectangle_variable PLUS_ASSIGN point_expression@>@/
{
  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `operation_assignment --> "
                    << "rectangle_variable PLUS_ASSIGN point_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Point* p = static_cast<Point*>(@=$3@>); 
  
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.25.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                   << "`operation_assignment --> "
                   << endl 
                   << "rectangle_variable PLUS_ASSIGN point_expression':"
                   << endl << "`rectangle_variable' is invalid, i.e., " 
                   << "`entry == 0'."
                   << endl << "Deleting `point_expression'.";
         
         log_message(cerr_strm);
         cerr_message(cerr_strm, error_stop_value);
         cerr_strm.str("");
 
         delete p;

      } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Rectangle* q = static_cast<Rectangle*>(entry->object);

@q ****** (6).  |q == 0|.  Try to allocate memory for a new |Rectangle|.@> 

@ |q == 0|.  Try to allocate memory for a new |Rectangle|.
\initials{LDF 2005.01.25.}
  
@<Define rules@>=

         if (q == static_cast<Rectangle*>(0))
           {

              try 
                {
                   q = create_new<Rectangle>(0);     
                }

              catch (bad_alloc)
                {
                   
                   cerr_strm << thread_name << "ERROR! In `yyparse()', rule "
                             << "`operation_assignment --> "
                             << endl 
                             << "rectangle_variable PLUS_ASSIGN point_expression':"
                             << endl << "Failed to create new `rectangle'. " 
                             << "Deleting `point_expression' and rethrowing "
                             << "`bad_alloc'.";

                   log_message(cerr_strm);
                   cerr_message(cerr_strm, error_stop_value);
                   cerr_strm.str("");
 
                   delete p;
 
                   throw;

                } /* |catch (bad_alloc)|  */

           } /* |if (q == 0)|  */

@q ****** (6).@>           

         *q += *p;

         entry->object = static_cast<void*>(q);

         delete p;

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5)  Set |$$| to 0 and exit rule.@> 

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |entry->object|
upon success and 0 upon failure.
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> rectangle_variable PLUS_ASSIGN path_join.@>

@*3 \�operation assignment> $\longrightarrow$ \�rectangle variable>
\.{PLUS\_ASSIGN} \�path join>.
\initials{LDF 2022.11.19.}

\LOG
\initials{LDF 2022.11.19.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: rectangle_variable PLUS_ASSIGN path_join@>@/
{

  @<Common declarations for rules@>@; 

  Rectangle* r = 0;

#if DEBUG_COMPILE

  DEBUG = false; /* |true| */

  if (DEBUG)
  { 
     cerr << "*** Parser:  Rule `operation_assignment: rectangle_variable PLUS_ASSIGN path_join'."
          << endl;
  }  
#endif /* |DEBUG_COMPILE|  */@; 

  Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);

#if 0 /* 1  */
  c->show("*c:");
#endif 

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0 || entry->object == 0|.@>   
@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
   {
      r = create_new<Rectangle>(0);

      entry->object = static_cast<void*>(r);

   } /* |if (entry == 0 || entry->object == 0)|  */

@q ****** (6).  |entry != 0 && entry->object != 0|.@> 

@ |entry != 0 && entry->object != 0|.
\initials{LDF 2022.11.16.}
  
@<Define rules@>=

   else
      r = static_cast<Rectangle*>(entry->object);           

   *r += c;

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=


  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> rectangle_variable PLUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�rectangle variable> 
\.{PLUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.19.}

\LOG
\initials{LDF 2022.11.19.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: rectangle_variable PLUS_ASSIGN CYCLE@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) Error handling:  |entry == 0|.@>   
@ Error handling:  |entry == 0|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

    else /* |entry != 0|  */
      {

         Rectangle* p = static_cast<Rectangle*>(entry->object);

@q ****** (6).  |p == 0|.  Try to allocate memory for a new |Rectangle|.@> 

@ |p == 0|.  Try to allocate memory for a new |Rectangle|.
\initials{LDF 2022.11.16.}

@<Define rules@>=

         if (p == 0)
         {
            p = create_new<Rectangle>(0);      

            entry->object = static_cast<void*>(p); 

         } /* |if (p == 0)|  */

@q ****** (6).@>           

         if (p != static_cast<Rectangle*>(0))
            p->set_cycle(true);

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q **** (4) operation_assignment --> rectangle_variable MINUS_ASSIGN CYCLE.@>

@*3 \�operation assignment> $\longrightarrow$ \�rectangle variable>
\.{MINUS\_ASSIGN} \.{CYCLE}.
\initials{LDF 2022.11.19.}

\LOG
\initials{LDF 2022.11.19.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: rectangle_variable MINUS_ASSIGN CYCLE@>@/ 
{

    Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
         /* Do nothing.  */          
      }

@q ***** (5) |entry != 0|.@>   
@
@<Define rules@>=

    else /* |entry != 0|  */
      {

         Rectangle* p = static_cast<Rectangle*>(entry->object);

         if (p != static_cast<Rectangle*>(0))
            {
                p->set_cycle(false);

            }   /* |if (p != 0)|  */

@q ****** (6).@> 

      }  /* |else| (|entry != 0|)  */
   
@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |$$| to 0 and exit rule.
\initials{LDF 2022.11.16.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};


@q *** (3) |pictures|.@>  

@*2 {\bf pictures}.
\initials{LDF 2005.05.12.}

\LOG
\initials{LDF 2005.05.12.}
Added this section.
\ENDLOG

@q **** (4) operation_assignment --> picture_variable   @>
@q **** (4) PLUS_ASSIGN picture_expression.             @>

@*3 \�operation assignment> $\longrightarrow$ \�picture variable> 
\.{PLUS\_ASSIGN} \�picture expression>.
\initials{LDF 2005.05.12.}

\LOG
\initials{LDF 2005.05.12.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=operation_assignment: picture_variable PLUS_ASSIGN picture_expression@>@/
{
   
   operation_assignment_picture_rule_func_0(
                                         @=$1@>,
                                         @=$3@>,
                                         static_cast<Scanner_Node>(parameter));

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) Vector-types.@>  

@*1 Vector-types.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q *** (3) |boolean_vectors|.@>  

@*2 {\bf boolean\_vectors}.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> boolean_vector_variable  @>
@q *** (3) PLUS_ASSIGN boolean_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�boolean vector variable>
\.{PLUS\_ASSIGN} \�boolean expression>.

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: boolean_vector_variable PLUS_ASSIGN boolean_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "boolean_vector_variable PLUS_ASSIGN boolean_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  status
    = vector_type_plus_assign<bool>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     BOOLEAN_VECTOR,
                                     BOOLEAN,
                                     static_cast<bool*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "boolean_vector_variable PLUS_ASSIGN boolean_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `boolean' to `boolean_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.07.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "boolean_vector_variable PLUS_ASSIGN boolean_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |numeric_vectors|.@>  

@*2 {\bf numeric\_vectors}.
\initials{LDF 2005.01.06.}

\LOG
\initials{LDF 2005.01.06.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> numeric_vector_variable  @>
@q *** (3) PLUS_ASSIGN numeric_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�numeric vector variable>
\.{PLUS\_ASSIGN} \�numeric expression>.
\initials{LDF 2005.01.06.}

\LOG
\initials{LDF 2005.01.06.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: numeric_vector_variable PLUS_ASSIGN @>
@=numeric_expression@>@/ 
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "numeric_vector_variable PLUS_ASSIGN numeric_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.06.}

@<Define rules@>=

   real* r;

   try 
      {
          r = new real;
      }

@q ***** (5).@> 

   catch (bad_alloc)
      {
          cerr_strm << thread_name << "ERROR!  In `yyparse()', rule "
                    << "`operation_assignment -->"
                    << endl 
                    << "numeric_vector_variable PLUS_ASSIGN numeric_expression':"
                    << endl 
                    << "`new real' failed.  Rethrowing `bad_alloc'.";

          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");

      }  /* |catch (bad_alloc)|  */

@q ***** (5).@> 

  *r = @=$3@>;

  status = vector_type_plus_assign<real>(scanner_node,
                                         static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                         NUMERIC_VECTOR,
                                         NUMERIC,
                                         r);

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.06.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "numeric_vector_variable PLUS_ASSIGN numeric_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `numeric' to `numeric_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.06.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "numeric_vector_variable PLUS_ASSIGN numeric_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.06.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |string_vectors|.@>  

@*2 {\bf string\_vectors}.
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> string_vector_variable  @>
@q *** (3) PLUS_ASSIGN string_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�string vector variable>
\.{PLUS\_ASSIGN} \�string expression>.
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: string_vector_variable PLUS_ASSIGN string_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "string_vector_variable PLUS_ASSIGN string_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.09.}

@<Define rules@>=

  status
    = vector_type_plus_assign<string>(scanner_node,
                                      static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                      STRING_VECTOR,
                                      STRING,
                                      static_cast<string*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.09.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "string_vector_variable PLUS_ASSIGN string_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `string' to `string_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.09.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "string_vector_variable PLUS_ASSIGN string_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |pen_vectors|.@>  

@*2 {\bf pen\_vectors}.
\initials{LDF 2005.01.11.}

\LOG
\initials{LDF 2005.01.11.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> pen_vector_variable  @>
@q *** (3) PLUS_ASSIGN pen_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�pen vector variable>
\.{PLUS\_ASSIGN} \�pen expression>.

\LOG
\initials{LDF 2005.01.11.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: pen_vector_variable PLUS_ASSIGN pen_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "pen_vector_variable PLUS_ASSIGN pen_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.11.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Pen>(scanner_node,
                                   static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                   PEN_VECTOR,
                                   PEN,
                                   static_cast<Pen*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.11.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "pen_vector_variable PLUS_ASSIGN pen_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `pen' to `pen_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.11.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "pen_vector_variable PLUS_ASSIGN pen_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.11.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |dash_pattern_vectors|.@>  

@*2 \�dash pattern vectors>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> dash_pattern_vector_variable  @>
@q *** (3) PLUS_ASSIGN dash_pattern_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�dash pattern vector variable>
\.{PLUS\_ASSIGN} \�dash pattern expression>.

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: dash_pattern_vector_variable @>
@=PLUS_ASSIGN dash_pattern_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "dash_pattern_vector_variable PLUS_ASSIGN "
                  << "dash_pattern_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Dash_Pattern>(scanner_node,
                                   static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                   DASH_PATTERN_VECTOR,
                                   DASH_PATTERN,
                                   static_cast<Dash_Pattern*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "dash_pattern_vector_variable PLUS_ASSIGN dash_pattern_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  "
                << "Didn't add `dash_pattern' to `dash_pattern_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.13.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "dash_pattern_vector_variable PLUS_ASSIGN "
                    << "dash_pattern_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |color_vectors|.@>  

@*2 {\bf color\_vectors}.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> color_vector_variable     @>
@q *** (3) PLUS_ASSIGN color_expression.                      @>

@*3 \�operation assignment> $\longrightarrow$ \�color vector variable>
\.{PLUS\_ASSIGN} \�color expression>.

\LOG
\initials{LDF 2004.08.23.}
Added this rule.

\initials{LDF 2004.08.26.}
Got this rule to work.

\initials{LDF 2004.09.01.}
Removed most of the code from this rule to 
|Scan_Parse::vector_type_plus_assign|, which is now called in this
rule.  

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|color_vector_assignment| to |operation_assignment|.

\initials{LDF 2004.12.04.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: color_vector_variable PLUS_ASSIGN color_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "color_vector_variable PLUS_ASSIGN color_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.08.31.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Color>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     COLOR_VECTOR,
                                     COLOR,
                                     static_cast<Color*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.08.31.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "color_vector_variable PLUS_ASSIGN color_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `color' to `color_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.08.31.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "color_vector_variable PLUS_ASSIGN color_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> color_vector_variable  @>
@q *** (3) PLUS_ASSIGN color_vector_variable.            @>

@*3 \�operation assignment> $\longrightarrow$ \�color vector variable>
\.{PLUS\_ASSIGN} \�color vector variable>.
\initials{LDF 2023.08.24.}

\LOG
\initials{LDF 2023.08.24.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: color_vector_variable PLUS_ASSIGN color_vector_variable@>
{
@q ***** (5) @>

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `operation_assignment --> "
                 << "color_vector_variable PLUS_ASSIGN color_vector_variable'.";
     
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Id_Map_Entry_Node entry_d = static_cast<Id_Map_Entry_Node>(@=$3@>);

   Pointer_Vector<Color> *cv = 0;
   Pointer_Vector<Color> *dv = 0;

@q ***** (5) @>

   if (entry && entry->object && entry_d && entry_d->object)
   {
@q ****** (6) @>
       cv = static_cast<Pointer_Vector<Color> *>(entry->object);
       dv = static_cast<Pointer_Vector<Color> *>(entry_d->object);

#if 0 
cerr << "cv->ctr == " << cv->ctr << endl;
cerr << "dv->ctr == " << dv->ctr << endl;
#endif 

@q ****** (6) @>
@ A |Color| object must be appended to |*cv| rather than a pointer to a |Color| object.
|Pointer_Vector<C, D>::operator+=(D* d)| pushes the pointer itself onto the vector.  
That is, it {\it does not\/} allocate memory for a new pointer, copy the pointer, and put the
copy onto the vector.
\initials{LDF 2023.08.24.}
@<Define rules@>=

       for (vector<Color*>::iterator iter = dv->v.begin();
            iter != dv->v.end();
            ++iter)
       {
          *cv += **iter;
       }

@q ****** (6) @>
@
@<Define rules@>=

#if 0 
cerr << "After loop:  cv->ctr == " << cv->ctr << endl;
#endif 

@q ****** (6) @>

   }  /* |if|  */


@q ***** (5) @>
@
@<Define rules@>=
 
   @=$$@> = static_cast<void*>(0);

};

@q *** (3) |transform_vectors|.@>  

@*2 \�transform vectors>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> transform_vector_variable  @>
@q *** (3) PLUS_ASSIGN transform_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�transform vector variable>
\.{PLUS\_ASSIGN} \�transform expression>.

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: transform_vector_variable PLUS_ASSIGN transform_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "transform_vector_variable PLUS_ASSIGN "
                  << "transform_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  status = vector_type_plus_assign<Transform>(scanner_node,
                                              static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                              TRANSFORM_VECTOR,
                                              TRANSFORM,
                                              static_cast<Transform*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "transform_vector_variable PLUS_ASSIGN transform_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  "
                << "Didn't add `transform' to `transform_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.13.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "transform_vector_variable PLUS_ASSIGN "
                    << "transform_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> transform_vector_variable PLUS_ASSIGN numeric_list. @>

@*3 \�operation assignment> $\longrightarrow$ \�transform vector variable> \.{PLUS\_ASSIGN} 
\�numeric_list>.
\initials{LDF 2023.01.20.}

\LOG
\initials{LDF 2023.01.20.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: transform_vector_variable PLUS_ASSIGN numeric_list@>@/
{
   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
   {
      cerr_strm << thread_name 
                << "*** Parser: `operation_assignment --> transform_vector_variable PLUS_ASSIGN "
                << "numeric_list'.";
     
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Pointer_Vector<real> *real_ptr_vector = static_cast<Pointer_Vector<real>*>(@=$3@>);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "real_ptr_vector->v.size() == " << real_ptr_vector->v.size() << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Transform *t = create_new<Transform>(0);

   t->set(*(real_ptr_vector->v[0]), *(real_ptr_vector->v[1]), *(real_ptr_vector->v[2]), *(real_ptr_vector->v[3]), 
          *(real_ptr_vector->v[4]), *(real_ptr_vector->v[5]), *(real_ptr_vector->v[6]), *(real_ptr_vector->v[7]), 
          *(real_ptr_vector->v[8]), *(real_ptr_vector->v[9]), *(real_ptr_vector->v[10]), *(real_ptr_vector->v[11]), 
          *(real_ptr_vector->v[12]), *(real_ptr_vector->v[13]), *(real_ptr_vector->v[14]), *(real_ptr_vector->v[15]));

   status = vector_type_plus_assign<Transform>(scanner_node,
                                               static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                               TRANSFORM_VECTOR,
                                               TRANSFORM,
                                               t);

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2023.01.20.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "transform_vector_variable PLUS_ASSIGN numeric_list':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  "
                << "Didn't add `transform' to `transform_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2023.01.20.}

@<Define rules@>=

    else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "transform_vector_variable PLUS_ASSIGN "
                    << "numeric_list':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2023.01.20.}

@<Define rules@>=

   delete real_ptr_vector;
   real_ptr_vector = 0;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) |focus_vectors|.@> 
@* \�focus vectors>.
\initials{LDF 2005.01.17.}

\LOG
\initials{LDF 2005.01.17.}
Added this section.
\ENDLOG

@q **** (4) @>   
@

\LOG
\initials{LDF 2005.01.18.}
Added this rule.
\ENDLOG 

@q ***** (5) Definition.@> 

@<Define rules@>=

@=operation_assignment: focus_vector_variable@>@/
@=PLUS_ASSIGN WITH_POSITION point_expression@>@/
@=WITH_DIRECTION point_expression WITH_DISTANCE@>@/
@=numeric_expression with_angle_optional with_axis_optional@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name << "*** Parser:  `operation_assignment -->  "
                 << "focus_vector_variable PLUS_ASSIGN "
                 << "WITH_POSITION point_expression"
                 << endl 
                 << "WITH_DIRECTION point_expression "
                 << "WITH_DISTANCE numeric_expression"
                 << endl 
                 << "with_angle_optional with_axis_optional'.";

       log_message(cerr_strm);       
       cerr_message(cerr_strm); 
       cerr_strm.str(""); 
    }
#endif /* |DEBUG_COMPILE|  */@;
   
   entry            = static_cast<Id_Map_Entry_Node>(@=$1@>);
   Point* position  = static_cast<Point*>(@=$4@>); 
   Point* direction = static_cast<Point*>(@=$6@>); 
   real distance    = @=$8@>;
   real angle       = @=$9@>;
   real axis        = @=$10@>;  

@q ****** (6) Error handling:  |entry == 0|.@> 
@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.18.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {
          cerr_strm << thread_name << "ERROR!  In `yyparse()', rule "
                    << "`operation_assignment -->  "
                    << "focus_vector_variable PLUS_ASSIGN "
                    << "WITH_POSITION point_expression"
                    << endl 
                    << "WITH_DIRECTION point_expression "
                    << "WITH_DISTANCE numeric_expression"
                    << endl 
                    << "with_angle_optional with_axis_optional':"
                    << endl << "`focus_vector_variable' is invalid.  "
                    << "Not assigning, deleting the `point_expressions', "
                    << "and will try to continue.";

          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");
         
          delete position;
          delete direction; 

          @=$$@> = static_cast<void*>(0);
          
      }  /* |if (entry == 0)|  */
 
@q ****** (6) |entry != 0|.@> 
@ |entry != 0|.
\initials{LDF 2005.01.18.}

@<Define rules@>=

   else /* |entry != 0|  */
      {

         Focus* f;

         try 
            {
               f = new Focus;
            }

@q ******* (7) @> 

         catch (bad_alloc) 
            {
                cerr_strm << thread_name << "ERROR!  "
                          << "In `yyparse()', rule "
                          << "`operation_assignment -->  "
                          << "focus_vector_variable PLUS_ASSIGN "
                          << "WITH_POSITION point_expression"
                          << endl 
                          << "WITH_DIRECTION point_expression "
                          << "WITH_DISTANCE numeric_expression"
                          << endl 
                          << "with_angle_optional with_axis_optional':"
                          << endl 
                          << "`new Focus' failed.  "
                          << "Deleting the `point_expressions' and "
                          << "rethrowing `bad_alloc'.";

                log_message(cerr_strm);
                cerr_message(cerr_strm, error_stop_value);
                cerr_strm.str("");
         
                delete position;
                delete direction; 

                throw;
                      
            }  /* |catch (bad_alloc)|  */

@q ******* (7) @> 

         char axis_char;

         if (axis == Z_AXIS)
            axis_char = 'z';
         else if (axis == X_AXIS)
            axis_char = 'x';
         else if (axis == Y_AXIS)
            axis_char = 'y';

@q ******** (8) Error handling:  |axis| has invalid value.@>  
@  Error handling:  |axis| has invalid value.
\initials{LDF 2005.01.18.}

@<Define rules@>=

  else /* |axis| has invalid value.  */
     {
         cerr_strm << thread_name << "ERROR!  "
                   << "In `yyparse()', rule "
                   << "`operation_assignment -->  "
                   << "focus_vector_variable PLUS_ASSIGN "
                   << "WITH_POSITION point_expression"
                   << endl 
                   << "WITH_DIRECTION point_expression "
                   << "WITH_DISTANCE numeric_expression"
                   << endl 
                   << "with_angle_optional with_axis_optional':"
                   << endl 
                   << "`with_axis_optional' has invalid value.  "
                   << "Setting `axis_char' to 'z'.";

         log_message(cerr_strm); 
         cerr_message(cerr_strm, error_stop_value); 
         cerr_strm.str(""); 
       
         axis_char = 'z';

     }  /* |else|  (|axis| has invalid value.)  */

@q ******** (8) @> 

         f->set(*position, *direction, distance, angle, axis_char);

         status = vector_type_plus_assign(scanner_node,
                                          entry,
                                          FOCUS_VECTOR,
                                          FOCUS,
                                          f,
                                          false);

@q ******* (7) @> 

         if (status != 0)
            {

                cerr_strm << thread_name << "ERROR!  "
                          << "In `yyparse()', rule "
                          << "`operation_assignment -->  "
                          << "focus_vector_variable PLUS_ASSIGN "
                          << "WITH_POSITION point_expression"
                          << endl 
                          << "WITH_DIRECTION point_expression "
                          << "WITH_DISTANCE numeric_expression"
                          << endl 
                          << "with_angle_optional with_axis_optional':"
                          << endl 
                          << "`Scan_Parse::vector_type_plus_assign()' failed.  "
                          << "Deleting the `point_expressions' and "
                          << "will try to continue.";

                log_message(cerr_strm);
                cerr_message(cerr_strm, error_stop_value);
                cerr_strm.str("");
         
                delete position;
                delete direction; 

               @=$$@> = static_cast<void*>(0);
 
            }  /* |if (status != 0)|  */

@q ******* (7) @>

        else /* |status != 0|  */
           {
                delete position;
                delete direction; 
 
                @=$$@> = static_cast<void*>(0);

           }  /* |else| (|status != 0|)  */

@q ******* (7) @>

      }  /* |else| (|entry != 0|)  */

@q ****** (6).@> 

};

@q *** (3) |star_vectors|.@>  

@*2 {\bf star\_vectors}.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> star_vector_variable  @>
@q *** (3) PLUS_ASSIGN star_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�star vector variable>
\.{PLUS\_ASSIGN} \�star expression>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: star_vector_variable PLUS_ASSIGN star_expression@>
{
   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "star_vector_variable PLUS_ASSIGN star_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Star>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     STAR_VECTOR,
                                     STAR,
                                     static_cast<Star*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "star_vector_variable PLUS_ASSIGN star_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `star' to `star_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2021.06.26.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "star_vector_variable PLUS_ASSIGN star_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> newwrite_vector_variable  @>
@q *** (3) PLUS_ASSIGN string_primary.                     @>

@*3 \�operation assignment> $\longrightarrow$ \�newwrite vector variable>
\.{PLUS\_ASSIGN} \�string expression>.

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: newwrite_vector_variable PLUS_ASSIGN string_primary@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "newwrite_vector_variable PLUS_ASSIGN string_primary'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2021.07.05.}

@<Define rules@>=

  Newwrite *n = new Newwrite;

  string *s = static_cast<string *>(@=$3@>);

  *n = *s;

  delete s;
  s = 0;

  status
    = vector_type_plus_assign<Newwrite>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     NEWWRITE_VECTOR,
                                     NEWWRITE,
                                     n);

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2021.07.05.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "newwrite_vector_variable PLUS_ASSIGN string_primary':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `newwrite' to `newwrite_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2021.07.05.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "newwrite_vector_variable PLUS_ASSIGN string_primary':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2021.07.05.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};


@q *** (3) |constellation_vectors|.@>  

@*2 {\bf constellation\_vectors}.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> constellation_vector_variable  @>
@q *** (3) PLUS_ASSIGN constellation_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�constellation vector variable>
\.{PLUS\_ASSIGN} \�constellation expression>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: constellation_vector_variable PLUS_ASSIGN constellation_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "constellation_vector_variable PLUS_ASSIGN constellation_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Constellation>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     CONSTELLATION_VECTOR,
                                     CONSTELLATION,
                                     static_cast<Constellation*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "constellation_vector_variable PLUS_ASSIGN constellation_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `constellation' to `constellation_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2021.06.26.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "constellation_vector_variable PLUS_ASSIGN constellation_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |planet_vectors|.@>  

@*2 {\bf planet\_vectors}.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> planet_vector_variable  @>
@q *** (3) PLUS_ASSIGN planet_expression.                   @>

@*3 \�operation assignment> $\longrightarrow$ \�planet vector variable>
\.{PLUS\_ASSIGN} \�planet expression>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>   

@<Define rules@>=
 
@=operation_assignment: planet_vector_variable PLUS_ASSIGN planet_expression@>
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "planet_vector_variable PLUS_ASSIGN planet_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Planet>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     PLANET_VECTOR,
                                     PLANET,
                                     static_cast<Planet*>(@=$3@>));

@q ***** (5) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "planet_vector_variable PLUS_ASSIGN planet_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `planet' to `planet_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ***** (5) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2021.06.26.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "planet_vector_variable PLUS_ASSIGN planet_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |Shape| types.@> 
@*2 {\bf Shape} types.
\initials{LDF 2005.01.17.}

@q **** (4) |point_vectors|.@>  

@*3 \�point vectors>.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> point_vector_variable  @>
@q *** (3) PLUS_ASSIGN point_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�point vector variable>
\.{PLUS\_ASSIGN} \�point expression>.

\LOG
\initials{LDF 2004.09.01.}
Added this rule.

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|point_vector_assignment| to |operation_assignment|.

\initials{LDF 2004.12.04.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: point_vector_variable PLUS_ASSIGN point_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "point_vector_variable PLUS_ASSIGN point_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.08.31.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Point>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     POINT_VECTOR,
                                     POINT,
                                     static_cast<Point*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.08.31.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "point_vector_variable PLUS_ASSIGN point_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `point' to `point_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.08.31.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "point_vector_variable PLUS_ASSIGN point_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> point_vector_variable @>  
@q *** (3) PLUS_ASSIGN point_vector_expression.           @> 

@*2 \�point vector assignment>
$\longrightarrow$ \�point vector variable> 
\.{PLUS\_ASSIGN} \�point vector expression>.      

\LOG
\initials{LDF 2004.11.10.}
Added this rule.

\initials{LDF 2004.11.11.}
Changed the code in this rule to account for changes
I've made in |Scan_Parse::vector_type_assign|, which is 
defined in \filename{scanprse.web}.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: point_vector_variable @>  
@=PLUS_ASSIGN point_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "point_vector_variable "
                  << "PLUS_ASSIGN point_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Point> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "point_vector_variable PLUS_ASSIGN "
                << "point_vector_expression':"
                << endl 
                << "`point_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `point_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "point_vector_variable PLUS_ASSIGN "
                    << "point_vector_expression':"
                    << endl 
                    << "`point_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Point, Point>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "point_vector_variable PLUS_ASSIGN "
                      << "point_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `point_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |bool_point_vectors|.@>  
@*2 {\bf bool\_point\_vectors}.
\initials{LDF 2004.12.04.}

\LOG
\initials{LDF 2004.12.04.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> bool_point_vector_variable @>
@q *** (3)  := bool_point_variable.                            @>
@*2 \�operation assignment> $\longrightarrow$ 
\�bool-point vector variable>
\.{+=} \�bool-point expression>.

\LOG
\initials{LDF 2004.09.01.}
Added this rule.

\initials{LDF 2004.09.05.}
@:BUG FIX@> BUG FIX:  Now allocating memory for a new |Bool_Point|.

\initials{LDF 2004.09.05.}
Changed |@=$3@>| from |bool_point_variable| to 
|bool_point_expression|.  This makes it unnecessary to allocate 
memory for a new |Bool_Point|. 

\initials{LDF 2004.11.05.}
Changed the symbol on the left-hand side of this rule from 
|bool_point_vector_assignment| to |operation_assignment|.

\initials{LDF 2004.12.03.}
Moved this rule from \filename{passign.w} to this file 
(\filename{popassgn.w}). 
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: bool_point_vector_variable @>
@=PLUS_ASSIGN bool_point_expression                @>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << endl 
                  << "bool_point_vector_variable PLUS_ASSIGN "
                  << "bool_point_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

Bool_Point* bp = static_cast<Bool_Point*>(@=$3@>);

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.09.01.}

@<Define rules@>=

   status 
      = vector_type_plus_assign<Bool_Point>(scanner_node,
                             static_cast<Id_Map_Entry_Node>(@=$1@>), 
                             BOOL_POINT_VECTOR,
                             BOOL_POINT,
                             bp);   

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "bool_point_vector_variable PLUS_ASSIGN "
                << "bool_point_variable':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed. Didn't add `bool_point' "
                << "to `bool_point_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.09.01.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "bool_point_vector_variable "
                    << "PLUS_ASSIGN bool_point_variable':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Now setting |@=$$@>| to 0.  Formerly, it was set to |@=$3@>|. 
\ENDLOG 

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |path_vectors|.@>  

@*2 \�path vectors>.
\initials{LDF 2004.12.12.}

\LOG
\initials{LDF 2004.12.12.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> path_vector_variable  @>
@q *** (3) PLUS_ASSIGN path_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�path vector variable>
\.{PLUS\_ASSIGN} \�path expression>.

\LOG
\initials{LDF 2004.12.12.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: path_vector_variable PLUS_ASSIGN path_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "path_vector_variable PLUS_ASSIGN path_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.12.}

@<Define rules@>=

  status
    = vector_type_plus_assign<Path>(scanner_node,
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     PATH_VECTOR,
                                     PATH,
                                     static_cast<Path*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.12.12.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "path_vector_variable PLUS_ASSIGN path_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `path' to `path_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.12.12.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "path_vector_variable PLUS_ASSIGN path_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.12.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> path_vector_variable @>  
@q *** (3) PLUS_ASSIGN path_vector_expression.           @> 

@*2 \�path vector assignment>
$\longrightarrow$ \�path vector variable> 
\.{PLUS\_ASSIGN} \�path vector expression>.      

\LOG
\initials{LDF 2004.12.12.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: path_vector_variable @>  
@=PLUS_ASSIGN path_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "path_vector_variable "
                  << "PLUS_ASSIGN path_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Path> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.12.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "path_vector_variable PLUS_ASSIGN "
                << "path_vector_expression':"
                << endl 
                << "`path_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `path_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.12.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "path_vector_variable PLUS_ASSIGN "
                    << "path_vector_expression':"
                    << endl 
                    << "`path_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Path, Path>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.12.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "path_vector_variable PLUS_ASSIGN "
                      << "path_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `path_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.12.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |ellipse_vectors|.@>  

@*2 \�ellipse vectors>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> ellipse_vector_variable  @>
@q *** (3) PLUS_ASSIGN ellipse_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�ellipse vector variable>
\.{PLUS\_ASSIGN} \�ellipse expression>.

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: ellipse_vector_variable PLUS_ASSIGN ellipse_expression@>
{

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

   vector_type_plus_assign<Ellipse>(static_cast<Scanner_Node>(parameter),
                                    static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                    ELLIPSE_VECTOR,
                                    ELLIPSE,
                                    static_cast<Ellipse*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> ellipse_vector_variable @>  
@q *** (3) PLUS_ASSIGN ellipse_vector_expression.           @> 

@*2 \�ellipse vector assignment>
$\longrightarrow$ \�ellipse vector variable> 
\.{PLUS\_ASSIGN} \�ellipse vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: ellipse_vector_variable @>  
@=PLUS_ASSIGN ellipse_vector_expression@>@/
{

   typedef Pointer_Vector<Ellipse> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        vector_type_assign<Ellipse, Ellipse>(static_cast<Scanner_Node>(parameter),
                                                          entry,
                                                          pv);          
        delete pv;

        @=$$@> = static_cast<void*>(0);
   }

}; 

@q *** (3) |circle_vectors|.@>  

@*2 \�circle vectors>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> circle_vector_variable  @>
@q *** (3) PLUS_ASSIGN circle_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�circle vector variable>
\.{PLUS\_ASSIGN} \�circle expression>.

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: circle_vector_variable PLUS_ASSIGN circle_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "circle_vector_variable PLUS_ASSIGN circle_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Circle>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        CIRCLE_VECTOR,
                                        CIRCLE,
                                        static_cast<Circle*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "circle_vector_variable PLUS_ASSIGN circle_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `circle' to `circle_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.12.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "circle_vector_variable PLUS_ASSIGN circle_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> circle_vector_variable @>  
@q *** (3) PLUS_ASSIGN circle_vector_expression.           @> 

@*2 \�circle vector assignment>
$\longrightarrow$ \�circle vector variable> 
\.{PLUS\_ASSIGN} \�circle vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: circle_vector_variable @>  
@=PLUS_ASSIGN circle_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "circle_vector_variable "
                  << "PLUS_ASSIGN circle_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Circle> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "circle_vector_variable PLUS_ASSIGN "
                << "circle_vector_expression':"
                << endl 
                << "`circle_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `circle_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "circle_vector_variable PLUS_ASSIGN "
                    << "circle_vector_expression':"
                    << endl 
                    << "`circle_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Circle, Circle>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "circle_vector_variable PLUS_ASSIGN "
                      << "circle_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `circle_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |parabola_vectors|.@>  

@*2 \�parabola vectors>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> parabola_vector_variable  @>
@q *** (3) PLUS_ASSIGN parabola_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�parabola vector variable>
\.{PLUS\_ASSIGN} \�parabola expression>.

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: parabola_vector_variable PLUS_ASSIGN parabola_expression@>
{

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

   vector_type_plus_assign<Parabola>(static_cast<Scanner_Node>(parameter),
                                    static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                    PARABOLA_VECTOR,
                                    PARABOLA,
                                    static_cast<Parabola*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> parabola_vector_variable @>  
@q *** (3) PLUS_ASSIGN parabola_vector_expression.           @> 

@*2 \�parabola vector assignment>
$\longrightarrow$ \�parabola vector variable> 
\.{PLUS\_ASSIGN} \�parabola vector expression>.      

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: parabola_vector_variable @>  
@=PLUS_ASSIGN parabola_vector_expression@>@/
{

   typedef Pointer_Vector<Parabola> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        vector_type_assign<Parabola, Parabola>(static_cast<Scanner_Node>(parameter),
                                                          entry,
                                                          pv);          
        delete pv;

        @=$$@> = static_cast<void*>(0);
   }

}; 

@q *** (3) |hyperbola_vectors|.@>  

@*2 \�hyperbola vectors>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> hyperbola_vector_variable  @>
@q *** (3) PLUS_ASSIGN hyperbola_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�hyperbola vector variable>
\.{PLUS\_ASSIGN} \�hyperbola expression>.

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: hyperbola_vector_variable PLUS_ASSIGN hyperbola_expression@>
{

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

   vector_type_plus_assign<Hyperbola>(static_cast<Scanner_Node>(parameter),
                                    static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                    HYPERBOLA_VECTOR,
                                    HYPERBOLA,
                                    static_cast<Hyperbola*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> hyperbola_vector_variable @>  
@q *** (3) PLUS_ASSIGN hyperbola_vector_expression.           @> 

@*2 \�hyperbola vector assignment>
$\longrightarrow$ \�hyperbola vector variable> 
\.{PLUS\_ASSIGN} \�hyperbola vector expression>.      

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: hyperbola_vector_variable @>  
@=PLUS_ASSIGN hyperbola_vector_expression@>@/
{

   typedef Pointer_Vector<Hyperbola> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        vector_type_assign<Hyperbola, Hyperbola>(static_cast<Scanner_Node>(parameter),
                                                          entry,
                                                          pv);          
        delete pv;

        @=$$@> = static_cast<void*>(0);
   }

}; 

@q *** (3) |polygon_vectors|.@>  

@*2 \�polygon vectors>.
\initials{LDF 2005.03.01.}

\LOG
\initials{LDF 2005.03.01.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> polygon_vector_variable  @>
@q *** (3) PLUS_ASSIGN polygon_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�polygon vector variable>
\.{PLUS\_ASSIGN} \�polygon expression>.
\initials{LDF 2005.03.01.}

\LOG
\initials{LDF 2005.03.01.}
Added this rule.

\initials{LDF 2005.03.01.}
Changed |polygon_expression| to |polygon_like_expression|.

\initials{LDF 2005.10.24.}
Changed |polygon_like_expression| back to |polygon_expression|.
Removed debugging code.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: polygon_vector_variable PLUS_ASSIGN polygon_expression@>
{

    vector_type_plus_assign<Polygon>(static_cast<Scanner_Node>(parameter),
                                     static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                     POLYGON_VECTOR,
                                     POLYGON,
                                     static_cast<Polygon*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> polygon_vector_variable @>  
@q *** (3) PLUS_ASSIGN polygon_vector_expression.           @> 

@*2 \�polygon vector assignment>
$\longrightarrow$ \�polygon vector variable> 
\.{PLUS\_ASSIGN} \�polygon vector expression>.      
\initials{LDF 2005.03.01.}

\LOG
\initials{LDF 2005.03.01.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: polygon_vector_variable @>  
@=PLUS_ASSIGN polygon_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "polygon_vector_variable "
                  << "PLUS_ASSIGN polygon_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Polygon> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.03.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "polygon_vector_variable PLUS_ASSIGN "
                << "polygon_vector_expression':"
                << endl 
                << "`polygon_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `polygon_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.03.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "polygon_vector_variable PLUS_ASSIGN "
                    << "polygon_vector_expression':"
                    << endl 
                    << "`polygon_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Polygon, Polygon>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.03.01.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "polygon_vector_variable PLUS_ASSIGN "
                      << "polygon_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `polygon_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.03.01.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |triangle_vectors|.@>  

@*2 \�triangle vectors>.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> triangle_vector_variable  @>
@q *** (3) PLUS_ASSIGN triangle_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�triangle vector variable>
\.{PLUS\_ASSIGN} \�triangle expression>.

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: triangle_vector_variable PLUS_ASSIGN triangle_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser:  `operation_assignment --> "
                  << "triangle_vector_variable PLUS_ASSIGN triangle_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Triangle>(scanner_node,
                                         static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                         TRIANGLE_VECTOR,
                                         TRIANGLE,
                                         static_cast<Triangle*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "triangle_vector_variable PLUS_ASSIGN triangle_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `triangle' to `triangle_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.25.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "triangle_vector_variable PLUS_ASSIGN triangle_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> triangle_vector_variable @>  
@q *** (3) PLUS_ASSIGN triangle_vector_expression.           @> 

@*2 \�triangle vector assignment>
$\longrightarrow$ \�triangle vector variable> 
\.{PLUS\_ASSIGN} \�triangle vector expression>.      

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: triangle_vector_variable @>  
@=PLUS_ASSIGN triangle_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "triangle_vector_variable "
                  << "PLUS_ASSIGN triangle_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Triangle> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "triangle_vector_variable PLUS_ASSIGN "
                << "triangle_vector_expression':"
                << endl 
                << "`triangle_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `triangle_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "triangle_vector_variable PLUS_ASSIGN "
                    << "triangle_vector_expression':"
                    << endl 
                    << "`triangle_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Triangle, Triangle>(scanner_node,
                                                      entry,
                                                      pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.25.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "triangle_vector_variable PLUS_ASSIGN "
                      << "triangle_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `triangle_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.25.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |rectangle_vectors|.@>  

@*2 \�rectangle vectors>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> rectangle_vector_variable  @>
@q *** (3) PLUS_ASSIGN rectangle_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�rectangle vector variable>
\.{PLUS\_ASSIGN} \�rectangle expression>.

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: rectangle_vector_variable PLUS_ASSIGN rectangle_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "rectangle_vector_variable PLUS_ASSIGN rectangle_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Rectangle>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        RECTANGLE_VECTOR,
                                        RECTANGLE,
                                        static_cast<Rectangle*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "rectangle_vector_variable PLUS_ASSIGN rectangle_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `rectangle' to `rectangle_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.12.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "rectangle_vector_variable PLUS_ASSIGN rectangle_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> rectangle_vector_variable @>  
@q *** (3) PLUS_ASSIGN rectangle_vector_expression.           @> 

@*2 \�rectangle vector assignment>
$\longrightarrow$ \�rectangle vector variable> 
\.{PLUS\_ASSIGN} \�rectangle vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: rectangle_vector_variable @>  
@=PLUS_ASSIGN rectangle_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "rectangle_vector_variable "
                  << "PLUS_ASSIGN rectangle_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Rectangle> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "rectangle_vector_variable PLUS_ASSIGN "
                << "rectangle_vector_expression':"
                << endl 
                << "`rectangle_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `rectangle_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "rectangle_vector_variable PLUS_ASSIGN "
                    << "rectangle_vector_expression':"
                    << endl 
                    << "`rectangle_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Rectangle, Rectangle>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "rectangle_vector_variable PLUS_ASSIGN "
                      << "rectangle_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `rectangle_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |reg_polygon_vectors|.@>  

@*2 \�regular polygon vectors>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> reg_polygon_vector_variable  @>
@q *** (3) PLUS_ASSIGN reg_polygon_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�regular polygon vector variable>
\.{PLUS\_ASSIGN} \�regular polygon expression>.

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: reg_polygon_vector_variable PLUS_ASSIGN reg_polygon_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "reg_polygon_vector_variable PLUS_ASSIGN reg_polygon_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Reg_Polygon>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        REG_POLYGON_VECTOR,
                                        REG_POLYGON,
                                        static_cast<Reg_Polygon*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "reg_polygon_vector_variable PLUS_ASSIGN reg_polygon_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `reg_polygon' to `reg_polygon_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.12.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "reg_polygon_vector_variable PLUS_ASSIGN reg_polygon_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> reg_polygon_vector_variable @>  
@q *** (3) PLUS_ASSIGN reg_polygon_vector_expression.           @> 

@*2 \�regular polygon vector assignment>
$\longrightarrow$ \�regular polygon vector variable> 
\.{PLUS\_ASSIGN} \�regular polygon vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: reg_polygon_vector_variable @>  
@=PLUS_ASSIGN reg_polygon_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "reg_polygon_vector_variable "
                  << "PLUS_ASSIGN reg_polygon_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Reg_Polygon> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "reg_polygon_vector_variable PLUS_ASSIGN "
                << "reg_polygon_vector_expression':"
                << endl 
                << "`reg_polygon_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `reg_polygon_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "reg_polygon_vector_variable PLUS_ASSIGN "
                    << "reg_polygon_vector_expression':"
                    << endl 
                    << "`reg_polygon_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Reg_Polygon, Reg_Polygon>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "reg_polygon_vector_variable PLUS_ASSIGN "
                      << "reg_polygon_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `reg_polygon_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |ellipsoid_vectors|.@>  

@*2 \�ellipsoid vectors>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> ellipsoid_vector_variable  @>
@q *** (3) PLUS_ASSIGN ellipsoid_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�ellipsoid vector variable>
\.{PLUS\_ASSIGN} \�ellipsoid expression>.

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: ellipsoid_vector_variable PLUS_ASSIGN ellipsoid_expression@>
{

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

   vector_type_plus_assign<Ellipsoid>(static_cast<Scanner_Node>(parameter),
                                    static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                    ELLIPSOID_VECTOR,
                                    ELLIPSOID,
                                    static_cast<Ellipsoid*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> ellipsoid_vector_variable @>  
@q *** (3) PLUS_ASSIGN ellipsoid_vector_expression.           @> 

@*2 \�ellipsoid vector assignment>
$\longrightarrow$ \�ellipsoid vector variable> 
\.{PLUS\_ASSIGN} \�ellipsoid vector expression>.      

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: ellipsoid_vector_variable @>  
@=PLUS_ASSIGN ellipsoid_vector_expression@>@/
{

   typedef Pointer_Vector<Ellipsoid> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        vector_type_assign<Ellipsoid, Ellipsoid>(static_cast<Scanner_Node>(parameter),
                                                          entry,
                                                          pv);          
        delete pv;

        @=$$@> = static_cast<void*>(0);
   }

}; 

@q *** (3) |sphere_vectors|.@>  

@*2 \�sphere vectors>.
\initials{LDF 2005.12.01.}

\LOG
\initials{LDF 2005.12.01.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> sphere_vector_variable  @>
@q *** (3) PLUS_ASSIGN sphere_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�sphere vector variable>
\.{PLUS\_ASSIGN} \�sphere expression>.

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: sphere_vector_variable PLUS_ASSIGN sphere_expression@>
{

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

   vector_type_plus_assign<Sphere>(static_cast<Scanner_Node>(parameter),
                                    static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                    SPHERE_VECTOR,
                                    SPHERE,
                                    static_cast<Sphere*>(@=$3@>));

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> sphere_vector_variable @>  
@q *** (3) PLUS_ASSIGN sphere_vector_expression.           @> 

@*2 \�sphere vector assignment>
$\longrightarrow$ \�sphere vector variable> 
\.{PLUS\_ASSIGN} \�sphere vector expression>.      

\LOG
\initials{LDF 2005.12.01.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: sphere_vector_variable @>  
@=PLUS_ASSIGN sphere_vector_expression@>@/
{

   typedef Pointer_Vector<Sphere> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.12.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        vector_type_assign<Sphere, Sphere>(static_cast<Scanner_Node>(parameter),
                                                          entry,
                                                          pv);          
        delete pv;

        @=$$@> = static_cast<void*>(0);
   }

}; 

@q *** (3) |cuboid_vectors|.@>  

@*2 \�cuboid vectors>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> cuboid_vector_variable  @>
@q *** (3) PLUS_ASSIGN cuboid_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�cuboid vector variable>
\.{PLUS\_ASSIGN} \�cuboid expression>.

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: cuboid_vector_variable PLUS_ASSIGN cuboid_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "cuboid_vector_variable PLUS_ASSIGN cuboid_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Cuboid>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        CUBOID_VECTOR,
                                        CUBOID,
                                        static_cast<Cuboid*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "cuboid_vector_variable PLUS_ASSIGN cuboid_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `cuboid' to `cuboid_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2004.12.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "cuboid_vector_variable PLUS_ASSIGN cuboid_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> cuboid_vector_variable @>  
@q *** (3) PLUS_ASSIGN cuboid_vector_expression.           @> 

@*2 \�cuboid vector assignment>
$\longrightarrow$ \�cuboid vector variable> 
\.{PLUS\_ASSIGN} \�cuboid vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=operation_assignment: cuboid_vector_variable @>  
@=PLUS_ASSIGN cuboid_vector_expression@>@/
{

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: "
                  << "`operation_assignment -->"
                  << endl << "cuboid_vector_variable "
                  << "PLUS_ASSIGN cuboid_vector_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

   typedef Pointer_Vector<Cuboid> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      cerr_strm << thread_name 
                << "ERROR! In parser rule "
                << "`operation_assignment --> "
                << endl 
                << "cuboid_vector_variable PLUS_ASSIGN "
                << "cuboid_vector_expression':"
                << endl 
                << "`cuboid_vector_variable' is invalid. "
                << "Can't assign to it."
                << endl << "Setting `operation_assignment' to 0, "
                << "deleting `cuboid_vector_expression', "
                << "and will try to continue.";
                
      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "cuboid_vector_variable PLUS_ASSIGN "
                    << "cuboid_vector_expression':"
                    << endl 
                    << "`cuboid_vector_variable' is valid.";
                
          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

status = vector_type_assign<Cuboid, Cuboid>(scanner_node,
                                                entry,
                                                pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {
            cerr_strm << thread_name 
                      << "ERROR! In parser rule "
                      << "`operation_assignment --> "
                      << endl 
                      << "cuboid_vector_variable PLUS_ASSIGN "
                      << "cuboid_vector_expression':"
                      << endl 
                      << "`Scan_Parse::vector_type_assign()' failed."
                      << endl << "Deleting `cuboid_vector_expression' "
                      << "and setting "
                      << "`operation_assignment' to 0.";

            log_message(cerr_strm);
            cerr_message(cerr_strm, error_stop_value);
            cerr_strm.str("");

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ***** (5).@> 

}; 

@q *** (3) |polyhedron_vectors|.@>  

@*2 \�polyhedron vectors>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> polyhedron_vector_variable  @>
@q *** (3) PLUS_ASSIGN polyhedron_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�polyhedron vector variable>
\.{PLUS\_ASSIGN} \�polyhedron expression>.

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: polyhedron_vector_variable PLUS_ASSIGN @>
@=polyhedron_expression@>@/
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "polyhedron_vector_variable "
                  << "PLUS_ASSIGN polyhedron_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Polyhedron>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        POLYHEDRON_VECTOR,
                                        POLYHEDRON,
                                        static_cast<Polyhedron*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "polyhedron_vector_variable PLUS_ASSIGN polyhedron_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `polyhedron' to `polyhedron_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "polyhedron_vector_variable "
                    << "PLUS_ASSIGN polyhedron_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) |prismatoid_vectors|.@>  

@*2 \�prismatoid vectors>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this section.
\ENDLOG

@q *** (3) operation_assignment --> prismatoid_vector_variable  @>
@q *** (3) PLUS_ASSIGN prismatoid_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�prismatoid vector variable>
\.{PLUS\_ASSIGN} \�prismatoid expression>.

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: prismatoid_vector_variable PLUS_ASSIGN @>
@=prismatoid_expression@>@/
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "prismatoid_vector_variable "
                  << "PLUS_ASSIGN prismatoid_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  status
     = vector_type_plus_assign<Prismatoid>(scanner_node,
                                        static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                        PRISMATOID_VECTOR,
                                        PRISMATOID,
                                        static_cast<Prismatoid*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "prismatoid_vector_variable PLUS_ASSIGN prismatoid_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `prismatoid' to `prismatoid_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2005.01.14.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "prismatoid_vector_variable "
                    << "PLUS_ASSIGN prismatoid_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};



@q *** (3) operation_assignment --> line_vector_variable  @>
@q *** (3) PLUS_ASSIGN line_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�line vector variable>
\.{PLUS\_ASSIGN} \�line expression>.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: line_vector_variable PLUS_ASSIGN line_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "line_vector_variable PLUS_ASSIGN line_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  status = vector_type_plus_assign<Line>(scanner_node,
                                         static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                         LINE_VECTOR,
                                         LINE,
                                         static_cast<Line*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "line_vector_variable PLUS_ASSIGN line_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `line' to `line_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2023.01.22.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "line_vector_variable PLUS_ASSIGN line_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> plane_vector_variable  @>
@q *** (3) PLUS_ASSIGN plane_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�plane vector variable>
\.{PLUS\_ASSIGN} \�plane expression>.
\initials{LDF 2023.01.22.}

\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: plane_vector_variable PLUS_ASSIGN plane_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "plane_vector_variable PLUS_ASSIGN plane_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  status = vector_type_plus_assign<Plane>(scanner_node,
                                         static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                         PLANE_VECTOR,
                                         PLANE,
                                         static_cast<Plane*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "plane_vector_variable PLUS_ASSIGN plane_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign()' "
                << "failed.  Didn't add `plane' to `plane_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2023.01.22.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "plane_vector_variable PLUS_ASSIGN plane_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign()' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2023.01.22.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};

@q *** (3) operation_assignment --> glyph_vector_variable  @>
@q *** (3) PLUS_ASSIGN glyph_expression.                   @>

@*2 \�operation assignment> $\longrightarrow$ \�glyph vector variable>
\.{PLUS\_ASSIGN} \�glyph expression>.
\initials{LDF 2023.04.08.}

\LOG
\initials{LDF 2023.04.08.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=
 
@=operation_assignment: glyph_vector_variable PLUS_ASSIGN glyph_expression@>
{

@q ***** (5).@>   

   @<Common declarations for rules@>@;
  
#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   
   if (DEBUG)
      {
        cerr_strm << thread_name 
                  << "*** Parser: `operation_assignment --> "
                  << "glyph_vector_variable PLUS_ASSIGN glyph_expression'.";
        
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) Call |Scan_Parse::vector_type_plus_assign|.@>   
@ Call |Scan_Parse::vector_type_plus_assign|.
\initials{LDF 2023.04.08.}

@<Define rules@>=

  status = vector_type_plus_assign<Glyph>(scanner_node,
                                          static_cast<Id_Map_Entry_Node>(@=$1@>), 
                                          GLYPH_VECTOR,
                                          GLYPH,
                                          static_cast<Glyph*>(@=$3@>));

@q ****** (6) |Scan_Parse::vector_type_plus_assign| failed.@> 
@ |Scan_Parse::vector_type_plus_assign| failed.
\initials{LDF 2023.04.08.}

@<Define rules@>=

  if (status != 0)
    {
      cerr_strm << thread_name 
                << "ERROR! In parser rule `operation_assignment --> "
                << endl 
                << "glyph_vector_variable PLUS_ASSIGN glyph_expression':"
                << endl << "`Scan_Parse::vector_type_plus_assign' "
                << "failed.  Didn't add `glyph' to `glyph_vector'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm, error_stop_value);
      cerr_strm.str("");

    }  /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_plus_assign| succeeded.@> 
@ |Scan_Parse::vector_type_plus_assign| succeeded.
\initials{LDF 2023.04.08.}

@<Define rules@>=

else /* |status == 0|  */
    {

#if DEBUG_COMPILE
      if (DEBUG)
        {
          cerr_strm << thread_name 
                    << "In parser rule `operation_assignment --> "
                    << endl 
                    << "glyph_vector_variable PLUS_ASSIGN glyph_expression':"
                    << endl << "`Scan_Parse::vector_type_plus_assign' "
                    << "succeeded.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
        }
#endif /* |DEBUG_COMPILE|  */@;

} /* |else| (|status == 0|)  */ 

@q ***** (5) Set |$$| to 0 and exit rule.@>   

@ Set |@=$$@>| to 0 and exit rule.
\initials{LDF 2023.04.08.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

};



@q * (0) @>

@q   Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables: @>
@q mode:CWEB  @>
@q eval:(outline-minor-mode t)  @>
@q abbrev-file-name:"~/.abbrev_defs" @>
@q eval:(read-abbrev-file)  @>
@q fill-column:70 @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End: @>
