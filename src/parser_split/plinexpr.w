@q plinexpr.w @> 
@q Created by Laurence D. Finston (LDF) @>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2022, 2023, 2024 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) line expressions.  @>
@** line expressions.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Created this file.
\ENDLOG 

@q * (1) line_primary.  @>
@* \�line primary>.
\initials{LDF 2022.12.04.}  

\LOG
\initials{LDF 2022.12.04.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> line_primary@>@/

@q ** (2) line_primary --> line_variable.@>
@*1 \�line primary> $\longrightarrow$ \�line variable>.  

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=line_primary: line_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

    @=$$@> = static_cast<void*>(create_new<Line>(
                                  static_cast<Line*>(
                                     entry->object))); 

};

@q *** (3) line_primary --> LEFT_PARENTHESIS point_expression COMMA @>
@q *** (3) point_expression COMMA point_expression RIGHT_PARENTHESIS.@>

@*2 \�line primary> $\longrightarrow$ etc.
%
\�line primary> $\longrightarrow$ \.{\LP} 
\�point expression>  \.{COMMA} \�point expression>  \.{COMMA}
\�point expression>  \.{\RP}.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=line_primary: LEFT_PARENTHESIS point_expression COMMA@>@/
@=point_expression RIGHT_PARENTHESIS@>@/
{

   Point* p0 = static_cast<Point*>(@=$2@>);
   Point* p1 = static_cast<Point*>(@=$4@>);
  
   delete p0;
   delete p1;


};

@q ** (2) Get |Line| from planar |path| expressions.@> 
@*1 Get |Line| from planar {\bf path} expressions.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this section.

\initials{LDF 2022.12.04.}
Changed all of these rules so that they use \.{GET\_LINE} instead 
of ``{\LP} \.{LINE\_DECLARATOR} {\RP}''.
\ENDLOG

@q *** (3) line_primary --> GET_LINE path_expression.@>
@*2 \�line primary> $\longrightarrow$ \.{GET\_LINE} \�path expression>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=line_primary: GET_LINE path_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: line_primary: GET_LINE path_expression.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Path *p = static_cast<Path*>(@=$2@>);

    Line *L = new Line;

    *L = p->get_line(); 

    delete p;
    p = 0;

    @=$$@> = static_cast<void*>(L);

};

@q ** (2) line_primary --> LEFT_PARENTHESIS line_expression @> 
@q ** (2) RIGHT_PARENTHESIS                                 @>

@*1 \�line primary> $\longrightarrow$ \.{\LP} 
\�line expression> \.{\RP}.

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=line_primary: LEFT_PARENTHESIS line_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q ***** (5) line_primary --> LAST line_vector_expression.@>

@*4 \�line primary> $\longrightarrow$ \.{LAST} \�line vector expression>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=line_primary: LAST line_vector_expression@>@/
{ 

   Line* c = create_new<Line>(0);

   Pointer_Vector<Line>* pv 
      = static_cast<Pointer_Vector<Line>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2022.12.04.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<Line>*>(0))
      {
#if 0 
          cerr_strm << thread_name 
                    << "ERROR!  In `yyparse()', rule "
                    << endl 
                    << "`line_primary "
                    << "--> LAST line_vector_expression':"
                    << endl << "Invalid `line_vector_expression'.  "
                    << "Setting `line_primary' to 0"
                    << endl << "and will try to continue.";

          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");
#endif 
          delete c;
          c = 0;

          @=$$@> = static_cast<void*>(0);

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2022.12.04.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {
#if 0 
          cerr_strm << thread_name 
                    << "ERROR!  In `yyparse()', rule "
                    << endl 
                    << "`line_primary "
                    << "--> LAST line_vector_expression':"
                    << endl << "`line_vector_expression' is empty.  "
                    << "Setting `line_primary' to 0"
                    << endl << "and will try to continue.";

          log_message(cerr_strm);
          cerr_message(cerr_strm, error_stop_value);
          cerr_strm.str("");
#endif 
          delete c;
          c = 0;

          @=$$@> = static_cast<void*>(0);

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2022.12.04.}

@<Define rules@>=

   else 
      {
         *c = *(pv->v[pv->ctr - 1]);
         @=$$@> = static_cast<void*>(c); 
      }
@q ******* (7) @> 

};

@q * (1) line_secondary.  @>
@* \�line secondary>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> line_secondary@>
  
@q ** (2) line secondary --> line_primary.@>
@*1 \�line secondary> $\longrightarrow$ \�line primary>.

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=line_secondary: line_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: line_secondary --> line_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q * (1) line tertiary.@>
@* \�line tertiary>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> line_tertiary@>

@q ** (2) line tertiary --> line_secondary.  @>
@*1 \�line tertiary> $\longrightarrow$ \�line secondary>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=line_tertiary: line_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q * (1) line expression.@>
@* \�line expression>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> line_expression@>

@q ** (2) line expression --> line_tertiary.  @>
@*1 \�line expression> $\longrightarrow$ \�line tertiary>.

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=line_expression: line_tertiary@>@/
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:70                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

