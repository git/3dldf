@q pnmvexpr.w @> 
@q Created by Laurence Finston Fri Jan  7 11:08:25 CET 2005 @>
     
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) |numeric_vector| expressions.@>
@** {\bf numeric\_vector} expressions.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Created this file and wrote quite a few rules.  
\ENDLOG 

@q * (1) |numeric_vector| primary.  @>
@* \�numeric vector primary>.
\initials{LDF 2005.01.07.}
  
\LOG
\initials{LDF 2005.01.07.}
Added this type declaration.
\ENDLOG


@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_vector_primary@>@/

@q ** (2) numeric_vector_primary --> numeric_vector_variable.@>
@*1 \�numeric vector primary> $\longrightarrow$ 
\�numeric vector variable>.  
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_vector_primary: numeric_vector_variable@>@/ 
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser:  `numeric_vector_primary --> numeric_vector_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q **** (4) Error handling:  |entry == 0 || entry->object == 0|.@> 

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
  {

    @=$$@> = static_cast<void*>(0);

  } /* |if (entry == 0 || entry->object == 0)|  */

@q **** (4) |!(entry == 0 || entry->object == 0)|.@> 

@ |!(entry == 0 || entry->object == 0)|.
\initials{LDF 2005.01.07.}

@<Define rules@>=
  else /* |!(entry == 0 || entry->object == 0)|  */
  {
     typedef Pointer_Vector<real> PV;

     PV* pv = new PV;

     *pv = *static_cast<PV*>(entry->object);

     @=$$@> = static_cast<void*>(pv);                    

  }  /* |else| (|!(entry == 0 || entry->object == 0)|)  */
};

@q ** (2) numeric_vector_primary --> LEFT_PARENTHESIS  @>
@q ** (2) numeric_vector_expression  RIGHT_PARENTHESIS.@>

@*1 \�numeric vector primary> $\longrightarrow$ \.{LEFT\_PARENTHESIS}
\�numeric vector expression> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: LEFT_PARENTHESIS@>@/ 
@=numeric_vector_expression RIGHT_PARENTHESIS@>@/ 
{
   
  @=$$@> = @=$1@>;

};

@q ** (2) numeric_vector_primary --> MEASURE_TEXT string_expression.@>

@*1 \�numeric vector primary> $\longrightarrow$ 
\.{MEASURE\_TEXT} \�string expression>.
\initials{LDF 2005.08.30.}

\LOG
\initials{LDF 2005.08.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: MEASURE_TEXT string_expression@>@/ 
{
   
    Pointer_Vector<real>* pv = new Pointer_Vector<real>;
 
    int status = Scan_Parse::measure_text_func(static_cast<Scanner_Node>(parameter), 
                                               @=$2@>, 
                                               pv);

@q ****** (6)@> 

    if (status == 0)
       @=$$@> = static_cast<void*>(pv);

@q ****** (6)@> 

    else /* |status != 0|  */
       {

           @=$$@> = static_cast<void*>(0);

       }  /* |else|  (|status != 0|)  */
};

@q ***** (5) numeric_vector_primary --> DIRECTION_METAPOST numeric_expression @>
@q ***** (5) OF path_expression call_metapost_option_list                     @>

@ \�numeric vector primary> $\longrightarrow$ \.{DIRECTION\_METAPOST} 
\�numeric expression> \.{Of} \�path expression> \�x call metapost options>.
\initials{LDF 2022.05.17.}

\LOG
\initials{LDF 2022.05.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=numeric_vector_primary: DIRECTION_METAPOST numeric_expression OF path_expression @>@/
@=call_metapost_option_list@>@/
{

@q ****** (6) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `numeric_vector_primary:  DIRECTION_METAPOST numeric_expression"
                << endl 
                << "OF path_expression call_metapost_option_list.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

  bool save               = @=$5@> & 1U;
  bool clear              = @=$5@> & 2U;
  bool suppress_mp_stdout = @=$5@> & 4U;
  bool do_transform       = @=$5@> & 8U;

  Point *x_axis_pt = 0;
  Point *origin_pt = 0;

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



@q ****** (6) @>

  vector<real> rv;
  Path *q = static_cast<Path*>(@=$4@>); 


  Pointer_Vector<real> *pv = new Pointer_Vector<real>;

  status = q->get_metapost_path_info(@=$2@>, 
                                     &rv,
                                     0,
                                     0,
                                     0, 
                                     0, 
                                     origin_pt, 
                                     x_axis_pt, 
                                     do_transform,
                                     save,
                                     suppress_mp_stdout,
                                     scanner_node);


@q ****** (6) @>

  if (status != 0)
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `numeric_vector_primary: DIRECTION_METAPOST "
                << "numeric_expression OF path_expression call_metapost_option_list':"
                << endl 
                << "`Path::get_metapost_path_info' failed, returning " << status << "." 
                << endl
                << "Failed to obtain direction of path."
                << "Leaving `numeric_vector_primary' empty and continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

  }  /* |if (status != 0)| */

@q ****** (6) @>

  else
  {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr_strm << thread_name 
                    << "In parser, rule `numeric_vector_primary: DIRECTION_METAPOST "
                    << "numeric_expression OF path_expression call_metapost_option_list':"
                    << endl 
                    << "`Path::get_metapost_path_info' succeeded, returning 0." << endl
                    << "'rv.size()' == " << rv.size() << endl;

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
      }  
#endif /* |DEBUG_COMPILE|  */@; 

      for (vector<real>::iterator iter = rv.begin();
           iter != rv.end();
           ++iter)
      {
         *pv += new real(*iter);
      }

  }  /* |else| */

@q ****** (6) @>

  delete q;
  q = 0;

  if (scanner_node->metapost_output_struct)
  {
     delete scanner_node->metapost_output_struct;
     scanner_node->metapost_output_struct = 0;
  }

  if (x_axis_pt)
  {
     delete x_axis_pt;
     x_axis_pt = 0;
  }

  if (origin_pt)
  {
     delete origin_pt;
     origin_pt = 0;
  }

  if (scanner_node->tolerance)
  {
     delete scanner_node->tolerance;
     scanner_node->tolerance = 0;
  } 

  @=$$@> = static_cast<void*>(pv);
  
};

@q ** (2) numeric_vector_primary: GET_ELEMENTS transform_expression numeric_list. @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_ELEMENTS} \�transform expression> 
\�numeric list>.
\initials{LDF 2022.06.07.}

\LOG
\initials{LDF 2022.06.07.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_vector_primary: GET_ELEMENTS transform_expression numeric_list@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

   int ctr = 0;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: "
               << "`numeric_vector_primary: GET_ELEMENTS transform_expression numeric_list'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Transform *t = static_cast<Transform*>(@=$2@>);
   Pointer_Vector<real> *rv = static_cast<Pointer_Vector<real>*>(@=$3@>); 

   vector<real> results;
 
   int row;
   int col;
   real r = 0.0;

   if (t == 0)
   {
       cerr << "ERROR!  In parser, rule `numeric_vector_primary: GET_ELEMENTS transform_expression numeric_list':"
            << endl 
            << "`transform_expression' is NULL.  Can't get element.  Continuing." << endl;

       rv->clear(true);
       rv->v.push_back(new real(INVALID_REAL));
       
       goto END_GET_ELEMENTS_RULE;
 
   }
#if 0 
   else
   {
      cerr << "`t' is non-NULL." << endl;
   }
#endif 
   
@q *** (3) @>

   if (rv != 0)
   {
@q **** (4) @>

#if 0 
       cerr << "`rv' is non-NULL." << endl
            << "rv->v.size() == " << rv->v.size() 
            << endl;
#endif 

       if (rv->v.size() < 2 || rv->v.size() % 2 != 0)
       {
           cerr << "ERROR!  In parser, rule `numeric_vector_primary: GET_ELEMENTS transform_expression numeric_list':"
                << endl 
                << "The size of `numeric_list' is < 2 or not divisible by 2:  "  
                << endl << rv->v.size() << endl 
                << "Invalid `numeric_list'.  Can't get elements.  Continuing." << endl;

                rv->clear(true);
                rv->v.push_back(new real(INVALID_REAL));
       
                goto END_GET_ELEMENTS_RULE;

       } 
       else
       {
@q ***** (5) @>

           ctr = 0;

#if 0
           cerr << "Entering loop." << endl;
#endif 

           for (vector<real*>::iterator iter = rv->v.begin();
                iter != rv->v.end();
                ++iter)
           {
@q ****** (6) @>

#if 0 
              cerr << "ctr == " << ctr << endl;
#endif 

#if LDF_REAL_DOUBLE
              row = round(abs(**iter++));
              col = round(abs(**iter));
#else
              row = roundf(fabs(**iter++));
              col = roundf(fabs(**iter));
#endif 

#if 0
              cerr << "row == " << row << endl
                   << "col == " << col << endl;
#endif 

              r = t->get_element(row, col);

#if 0
              cerr << "r == " << r << endl;
#endif 

              results.push_back(r);

              ++ctr;

@q ****** (6) @>

           }  /* |for|  */
     
#if 0
           cerr << "After loop." << endl;
#endif 

@q ***** (5) @>

           vector<real*>::iterator rv_iter =  rv->v.begin();

           for (vector<real>::iterator iter = results.begin(); 
                iter != results.end(); 
                ++iter)
           {
              **rv_iter++ = *iter;
           }

           while (rv_iter != rv->v.end())
           {
              delete *rv_iter;
              *rv_iter++ = 0;
           }
           
           ctr = rv->v.size();

           for (int i = ctr / 2; i < ctr; ++i)
              rv->v.pop_back();

@q ***** (5) @>
       
       }  /* |else|  */

@q **** (4) @>

   }  /* |if (rv != 0)| */

@q *** (3) @>

END_GET_ELEMENTS_RULE:

#if 0 
   cerr << "At end of `get_elements' rule." << endl;
#endif 

   if (t)
   {
      delete t;
      t = 0;
   }

   @=$$@> = static_cast<void*>(rv);

};

@q ** (2) numeric_vector_primary: GET_ANGLES triangle_expression with_test_optional. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_ELEMENTS} \�triangle expression> 
\�with test optional>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_vector_primary: GET_ANGLES triangle_expression with_test_optional@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: "
               << "`numeric_vector_primary: GET_ANGLES triangle_expression with_test_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   bool do_test;

   if (@=$3@> == WITH_TEST)
      do_test = true;
   else 
      do_test = false;

   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   Triangle *t = static_cast<Triangle*>(@=$2@>);

   status = t->get_angles(pv, scanner_node, do_test);  

   delete t;
   t = 0; 

   @=$$@> = static_cast<void*>(pv); 

};

@q ** (2) numeric_vector_primary: GET_SIDE_LENGTHS triangle_expression with_test_optional. @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_ELEMENTS} \�triangle expression> 
\�with test optional>.
\initials{LDF 2022.12.17.}

\LOG
\initials{LDF 2022.12.17.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_vector_primary: GET_SIDE_LENGTHS triangle_expression with_test_optional@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: "
               << "`numeric_vector_primary: GET_SIDE_LENGTHS triangle_expression with_test_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   bool do_test;

   if (@=$3@> == WITH_TEST)
      do_test = true;
   else 
      do_test = false;

   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   Triangle *t = static_cast<Triangle*>(@=$2@>);

   status = t->get_side_lengths(pv, scanner_node, do_test);  

   delete t;
   t = 0; 

   @=$$@> = static_cast<void*>(pv); 

};

@q * (1) @>
@
@<Define rules@>=
@=numeric_vector_primary: GET_RANDOM_NUMBERS random_option_list@>
{
@q ** (2) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser:  `numeric_vector_primary:  GET_RANDOM_NUMBERS "
                << "random_option_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q ** (2) @>

    Pointer_Vector<real> *pv = new Pointer_Vector<real>;

    real range_low_value            = scanner_node->random_range_low_value;           
    real range_high_value           = scanner_node->random_range_high_value;          
    int  random_count_value         = scanner_node->random_count_value;       
    int modulus_value               = scanner_node->random_modulus_value;             
    bool random_unique_flag         = scanner_node->random_unique_flag;            
    int random_unique_factor        = scanner_node->random_unique_factor;            

    scanner_node->random_range_low_value            = 0.0;           
    scanner_node->random_range_high_value           = 0.0;          
    scanner_node->random_count_value                = 1;       
    scanner_node->random_modulus_value              = 0;
    scanner_node->random_unique_flag                = false;            
    scanner_node->random_unique_flag                = 5;            



    status = get_random_numbers(pv, 0, 
                                range_low_value,           
                                range_high_value,          
                                random_count_value,
                                modulus_value,             
                                random_unique_flag,
                                random_unique_factor,
                                scanner_node);

    if (status != 0)
    {
      cerr_strm << "ERROR!  In parser, rule `numeric_vector_primary:  GET_RANDOM_NUMBERS "
                << "random_option_list':"
                << endl
                << "`get_random_numbers' failed, returning " << status << "." << endl 
                << "Failed to get random numbers." << endl 
                << "Will try to continue." << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }

@q ** (2) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, rule `numeric_vector_primary:  GET_RANDOM_NUMBERS "
                << "random_option_list':"
                << endl
                << "`get_random_numbers' succeeded, returning 0."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
       
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ** (2) @>

    @=$$@> = static_cast<void*>(pv);

};

@q * (1) @>
@
@<Define rules@>=
@=numeric_vector_primary:  GET_INDICES sinewave_expression@>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `GET_INDICES sinewave_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

  Sinewave *s = static_cast<Sinewave*>(@=$2@>);

  Pointer_Vector<real> *rv = new Pointer_Vector<real>;

  status = s->get_indices(rv, scanner_node);

#if 0 
  cerr << "status == " << status << endl;
#endif 

  delete s;
  s = 0;

  @=$$@> = static_cast<void*>(rv); 

};

@q ***** (5) numeric_vector_primary --> SIZE cone_primary.  @>

@*4 \�numeric vector primary> $\longrightarrow$ \.{SIZE} \�cone primary>.
\initials{LDF 2023.11.15.}

\LOG
\initials{LDF 2023.11.15.}
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: SIZE cone_primary@>@/
{
   Cone *c = static_cast<Cone*>(@=$2@>);

   Pointer_Vector<real> *rv = new Pointer_Vector<real>;

   *rv += c->circles.size();
   *rv += c->paths.size();

   delete c;
   c = 0;

   @=$$@> = static_cast<void*>(rv);

};

@q * (1) numeric_vector_primary: GET_FACE_AXIS_LENGTHS polyhedron_expression  @>

@ \�numeric primary> $\longrightarrow$ \.{GET\_FACE\_AXIS\_LENGTHS} 
\�polyhedron expression>.
\initials{LDF 2023.12.31.}

\LOG
\initials{LDF 2023.12.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_FACE_AXIS_LENGTHS numeric_primary polyhedron_expression@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_FACE_AXIS_LENGTHS numeric_primary polyhedron_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Polyhedron *p = static_cast<Polyhedron*>(@=$3@>);

   if (p)
   {
#if 0 
      cerr << "p is non-NULL." << endl;

      cerr << "p->shape_type == " << p->shape_type << " == " 
           << Shape::type_name_map[p->shape_type] 
           << endl;
#endif 

      if (p->shape_type = Shape::PENTAGONAL_HEXECONTAHEDRON_TYPE)
      {
#if 0  
         r = Pentagonal_Hexecontahedron::face_axis_lengths;
#else

         Pointer_Vector<real> *rv = new Pointer_Vector<real>;

         *rv += 0.0;  /* !! START HERE  2023.12.31. */
         *rv += 0.0;
#endif 

         @=$$@> = static_cast<void*>(rv);

      }
      else
      {
     
         cerr << "In parser, rule `numeric_vector_primary: "
              << "GET_FACE_AXIS_LENGTHS numeric_primary polyhedron_expression':"
              << endl 
              << "This case hasn't been programmed.  Setting $$ to NULL and continuing." 
              << endl;

         @=$$@> = static_cast<void*>(0);
      }
   }

   else
   {
      cerr << "In parser, rule `numeric_vector_primary:"
           << "GET_FACE_AXIS_LENGTHS numeric_primary polyhedron_expression':"
            << endl 
           << "`Polyhedron *p' is NULL.  Setting $$ to NULL and continuing." 
           << endl;

      @=$$@> = static_cast<void*>(0);
   }

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MAX_X path_expression with_focus_optional verbose_optional@>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MAX\_X}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MAX_X path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MAX_X path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_max_x(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MIN_X path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MIN\_X}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MIN_X path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MIN_X path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_min_x(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MEAN_X path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MEAN\_X}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MEAN_X path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MEAN_X path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_mean_x(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MAX_Y path_expression with_focus_optional verbose_optional@>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MAX\_Y}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MAX_Y path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MAX_Y path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_max_y(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MIN_Y path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MIN\_Y}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MIN_Y path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MIN_Y path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_min_y(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MEAN_Y path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MEAN\_Y}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.10.09.}

\LOG
\initials{LDF 2024.10.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MEAN_Y path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MEAN_Y path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_mean_y(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};




@q * (1) numeric_vector_primary: GET_MAX_Z path_expression with_focus_optional verbose_optional@>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MAX\_Z}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.02.07.}

\LOG
\initials{LDF 2024.02.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MAX_Z path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MAX_Z path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_max_z(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MIN_Z path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MIN\_Z}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.02.07.}

\LOG
\initials{LDF 2024.02.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MIN_Z path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MIN_Z path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_min_z(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) numeric_vector_primary: GET_MEAN_Z path_expression with_focus_optional verbose_optional @>

@ \�numeric vector primary> $\longrightarrow$ \.{GET\_MEAN\_Z}  
\�path expression> \�with focus optional>.
\initials{LDF 2024.02.07.}

\LOG
\initials{LDF 2024.02.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_MEAN_Z path_expression with_focus_optional verbose_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_MEAN_Z path_expression with_focus_optional verbose_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Path *p = static_cast<Path*>(@=$2@>);
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = p->get_mean_z(pv, @=$3@>, @=$4@>, scanner_node);

   @=$$@> = static_cast<void*>(pv);

   delete p;
   p = 0;

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <uint_value> verbose_optional@>@/

@
@<Define rules@>=
@=verbose_optional: /* Empty */@>
{
   @=$$@> = 0U;
};

@
@<Define rules@>=
@=verbose_optional: VERBOSE@>
{
   @=$$@> = 1U;
};


@q * (1) numeric_vector_primary: GET_DISTANCES cuboid_primary TO focus_expression.@>
@ \�numeric vector primary> $\longrightarrow$ \.{GET\_DISTANCE} \�cuboid primary> 
\.{TO} \�focus expression>.
\initials{LDF 2024.02.18.}

\LOG
\initials{LDF 2024.02.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_DISTANCES cuboid_primary TO focus_expression @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_DISTANCES cuboid_primary TO focus_expression'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Focus  *f = static_cast<Focus*>(@=$4@>);
   Cuboid *c = static_cast<Cuboid*>(@=$2@>);

   Pointer_Vector<real> *rv = new Pointer_Vector<real>;


   delete c;
   c = 0;

  /* !! START HERE:  LDF 2024.02.18.  Try deleting focus.  In other places, this didn't work.
   Check why not.  Are foci meant to be persistent?
  */

#if 0 
   delete f;
   f = 0;
#endif 

   @=$$@> = static_cast<void*>(rv);

};


@q * (1) @>
@
\LOG
\initials{LDF 2024.08.17.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_SECTOR point_primary get_sector_option_list @>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "numeric_vector_primary: GET_SECTOR point_primary get_sector_option_list'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Pointer_Vector<real> *rv = new Pointer_Vector<real>;  

   real radius_val = scanner_node->sphere_sector_radius;
   Transform *t    = scanner_node->sphere_sector_transform;

   entry = scanner_node->sphere_sector_trace_point_entry;

   Point *p = static_cast<Point*>(@=$2@>);

@q ** (2) @>

   status = Sphere::get_sector(scanner_node, radius_val, p, 1, 1, rv, t, entry);

@q *** (3) @>

   if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `numeric_vector_primary: GET_SECTOR "
            << "point_primary get_sector_option_list':"
            << endl 
            << "`Sphere::get_sector' failed, returning " << status << "." 
            << endl 
            << "Failed to get sector for point on sphere." << endl 
            << "Will try to continue."
            << endl;
   }         

@q *** (3) @>
    
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr << "In parser, rule `numeric_vector_primary: GET_SECTOR "
            << "point_primary get_sector_option_list':"
            << endl 
            << "`Sphere::get_sector' succeeded, returning 0."
            << endl; 

cerr << "Type <ENTER> to continue:";
getchar();

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

@q ** (2) @>

   if (p)
   {
      delete p;
      p = 0;
   }

   if (scanner_node->sphere_sector_transform)
   {
      delete scanner_node->sphere_sector_transform;
      scanner_node->sphere_sector_transform = 0;
   }

   if (scanner_node->sphere_sector_sphere)
   {
      delete scanner_node->sphere_sector_sphere;
      scanner_node->sphere_sector_sphere = 0;
   }

   scanner_node->sphere_sector_radius = 0;

   scanner_node->sphere_sector_trace_point_entry = 0; /* Persistent object.  Do not delete!  
                                                         \initials{LDF 2024.08.18.} */

@q ** (2) @>

   @=$$@> = static_cast<void*>(rv);    

};

@q ** (2) get_sector_option_list.  @>

@ \�get sector option list>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> get_sector_option_list@>@/

@q *** (3) get_sector_option_list: /* Empty  */@>

@ \�get sector option list> $\longrightarrow$ \.{Empty}.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: /* Empty  */@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: /* Empty */'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

   @=$$@> = 0;

};

@q *** (3) get_sector_option_list: get_sector_option_list WITH_TRANSFORM transform_expression@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_TRANSFORM} \�transform expression>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_TRANSFORM transform_expression@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_TRANSFORM transform_expression'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_transform = static_cast<Transform*>(@=$3@>);

  @=$$@> = 0;

};

@q *** (3) get_sector_option_list: get_sector_option_list WITH_RADIUS numeric_expression@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_RADIUS} \�numeric expression>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_RADIUS numeric_expression@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_RADIUS numeric_expression'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_radius = @=$3@>;

  @=$$@> = 0;

};

@q *** (3) get_sector_option_list: WITH_SPHERE sphere_primary@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_SPHERE} \�sphere primary>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_SPHERE sphere_primary@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_SPHERE sphere_primary'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_sphere = static_cast<Sphere*>(@=$3@>);

  @=$$@> = 0;

};

@q *** (3) get_sector_option_list: WITH_ANGLE numeric_primary@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_ANGLE} \�numeric primary>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_ANGLE numeric_primary@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_ANGLE numeric_primary'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_angle_ra = scanner_node->sphere_sector_angle_decl = @=$3@>;

  @=$$@> = 0;

};

@q *** (3) get_sector_option_list: WITH_ANGLES numeric_primary COMMA numeric_primary@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_ANGLES} \�numeric primary> \.{COMMA} \�numeric primary>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_ANGLES numeric_primary COMMA numeric_primary@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_ANGLES numeric_primary COMMA numeric_primary'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_angle_ra   = @=$3@>;
  scanner_node->sphere_sector_angle_decl = @=$5@>;

  @=$$@> = 0;

};

@q *** (3) get_sector_option_list: WITH_TRACE_POINT point_variable@>

@ \�get sector option list> $\longrightarrow$ \�get sector option list>
\.{WITH\_TRACE\_POINT} \�point variablenumeric primary>.
\initials{LDF 2024.08.18.}

\LOG
\initials{LDF 2024.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=get_sector_option_list: get_sector_option_list WITH_TRACE_POINT point_variable@>
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG) 
  {
     cerr_strm << thread_name 
               << "*** Parser: `get_sector_option_list: get_sector_option_list WITH_TRACE_POINT point_variable'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */

  scanner_node->sphere_sector_trace_point_entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

  @=$$@> = 0;

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.10.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_HMS numeric_primary with_angle_type_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: GET_HMS numeric_primary "
               << "with_angle_type_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */
   
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = get_hms_func(@=$2@>, pv, @=$3@>, scanner_node);

   if (status != 0)
   {
     cerr << "ERROR! In parser, rule`numeric_vector_primary: GET_HMS numeric_primary"
          << endl 
          << "with_angle_type_optional':"
          << endl 
          << "`Scan_Parse::get_hms_func' failed, returning " << status << "." 
          << endl
          << "Failed to get HMS values for angle." << endl
          << "Will try to continue." << endl;
   }
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr << "In parser, rule`numeric_vector_primary: GET_HMS numeric_primary"
          << endl 
          << "with_angle_type_optional':"
          << endl 
          << "`Scan_Parse::get_hms_func' succeeded, returning 0."
          << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ** (2) @>

   @=$$@> = static_cast<void*>(pv);    

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.10.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_DMS numeric_primary with_angle_type_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: GET_DMS numeric_primary "
               << "with_angle_type_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */
   
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   status = get_dms_func(@=$2@>, pv, @=$3@>, scanner_node);

   if (status != 0)
   {
     cerr << "ERROR! In parser, rule`numeric_vector_primary: GET_DMS numeric_primary"
          << endl 
          << "with_angle_type_optional':"
          << endl 
          << "`Scan_Parse::get_dms_func' failed, returning " << status << "." 
          << endl
          << "Failed to get DMS values for angle." << endl
          << "Will try to continue." << endl;
   }
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr << "In parser, rule`numeric_vector_primary: GET_DMS numeric_primary"
          << endl 
          << "with_angle_type_optional':"
          << endl 
          << "`Scan_Parse::get_dms_func' succeeded, returning 0."
          << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ** (2) @>

   @=$$@> = static_cast<void*>(pv);    

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.10.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_DECIMAL_HOURS numeric_list with_angle_type_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_DECIMAL_HOURS "
                << "numeric_list with_angle_type_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */


@q ** (2) @>

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$2@>);

   real *r = new real;  /* For some reason, it didn't work to declare |r|
                           as a plain |real| and pass a pointer or reference
                           to it to |get_decimal_hours_func|.  I don't know
                           why not.  \initials{LDF 2024.10.22.}  */ 

   status = get_decimal_hours_func(pv, r, @=$3@>, scanner_node);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "`get_decimal_hours_func' returned status == " << status << endl;

   }  
#endif /* |DEBUG_COMPILE|  */@;

        
   @=$$@> = *r;

   delete r;
   r = 0;

   delete pv;
   pv = 0;

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.10.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_DECIMAL_DEGREES numeric_list with_angle_type_optional@>
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: GET_DECIMAL_DEGREES "
               << "numeric_list with_angle_type_optional'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */


@q ** (2) @>

   @=$$@> = 0;    

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_angle_type_optional@>@/

@q * (1) @>
@
@<Define rules@>=
@=with_angle_type_optional: /* Empty  */@>@/ 
{
   @=$$@> = 0;   
};

@q * (1) @>
@
@<Define rules@>=
@=with_angle_type_optional: WITH_HOURS@>@/ 
{
   @=$$@> = 0;   
};

@q * (1) @>
@
@<Define rules@>=
@=with_angle_type_optional: WITH_DEGREES@>@/ 
{
   @=$$@> = 2;   
};

@q * (1) @>
@
@<Define rules@>=
@=with_angle_type_optional: WITH_RADIANS@>@/ 
{
   @=$$@> = 3;   
};


@q * (1) @>
@
\LOG
\initials{LDF 2024.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: GET_ANGLES numeric_list@>@/ 
{
@q ** (2) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_vector_primary: "
               << "GET_ANGLES numeric_list'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

@q ** (2) @>

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$2@>);

   status = Triangle::solve_triangle(pv, Triangle::SSS, 0, scanner_node);

   if (status != 0)
   {
      cerr << "ERROR!  In parser, rule `numeric_vector_primary: "
           << "GET_ANGLES numeric_list':" 
           << endl 
           << "`Triangle::solve_triangle' failed, returning " << status << "."
           << endl
           << "Failed to solve triangle.  Will try to continue." << endl;
   }

@q ** (2) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `numeric_vector_primary: "
           << "GET_ANGLES numeric_list':" 
           << endl 
           << "`Triangle::solve_triangle' succeeded, returning 0." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(pv);

@q ** (2) @>

};

@q ** (2) numeric_vector_primary: SOLVE_TRIANGLE numeric_vector_expression @> 
@q triangle_solution_type. @>
@
\LOG
\initials{LDF 2024.05.23.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_primary: SOLVE_TRIANGLE numeric_vector_expression @>
@=triangle_solution_type@> 
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `numeric_vector_primary: SOLVE_TRIANGLE numeric_vector_expression"
                << endl
                << " triangle_solution_type'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Pointer_Vector<real> *result = new Pointer_Vector<real>;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$2@>);

   status = Triangle::solve_triangle(pv, @=$3@>, result, scanner_node);

@q **** (4) @>

   if (status != 0)
   {
      cerr << "Error!  In parser, rule `numeric_vector_primary: SOLVE_TRIANGLE numeric_vector_expression"
           << endl
           << " triangle_solution_type':" 
           << "`Triangle::solve_triangle' failed, returning " << status << "." << endl
           << "Failed to solve triangle." << endl
           << "Will try to continue." << endl;
   }

@q **** (4) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `numeric_vector_primary: SOLVE_TRIANGLE numeric_vector_expression"
           << endl
           << " triangle_solution_type':" 
           << "`Triangle::solve_triangle' succeeded, returning 0." << endl;

   }     
#endif /* |DEBUG_COMPILE|  */@;

   delete pv;
   pv = 0;

@q *** (3) @>

   @=$$@> = static_cast<void*>(result);

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.10.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> triangle_solution_type@>@/

@q ** (2) @>
@
\LOG
\initials{LDF 2024.10.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: /* Empty  */@>
{
   @=$$@> = 0;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.10.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: WITH_SSS@>
{
   @=$$@> = Triangle::SSS;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.10.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: WITH_SAS@>
{
   @=$$@> = Triangle::SAS;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: WITH_ASA@>
{
   @=$$@> = Triangle::ASA;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: WITH_SAA@>
{
   @=$$@> = Triangle::SAA;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=triangle_solution_type: WITH_SSA@>
{
   @=$$@> = Triangle::SSA;
};





@q * (1) numeric_vector secondary.@>

@* \�numeric vector secondary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> numeric_vector_secondary@>
  
@q ** (2) numeric_vector secondary --> numeric_vector_primary.@>
@*1 \�numeric vector secondary> $\longrightarrow$ 
\�numeric vector primary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_secondary: numeric_vector_primary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q ***** (5) numeric_vector_secondary --> GET_LOCATIONS point_vector_secondary @>
@q ***** (5) FROM superellipse_primary call_metapost_option_list.              @>

@*4 \�numeric vector secondary> $\longrightarrow$ \.{GET\_LOCATIONS} 
\�point vector secondary> \.{FROM} \�superellipse primary> 
\�call metapost option list>. 
\initials{LDF 2022.05.30.}

\LOG
\initials{LDF 2022.05.30.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@>  

@<Define rules@>= 
@=numeric_vector_secondary: GET_LOCATIONS point_vector_secondary FROM @>
@=superellipse_primary call_metapost_option_list@>
{
@q ******* (7) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
    if (DEBUG) 
      {
        cerr_strm << thread_name 
                  << "*** Parser: `numeric_vector_secondary: GET_LOCATIONS  "
                  << "point_vector_secondary FROM superellipse_primary "
                  << "call_metapost_option_list'.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");
      }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @>

   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;

   Pointer_Vector<Point> *pv   = static_cast<Pointer_Vector<Point>*>(@=$2@>);
   Superellipse* s = static_cast<Superellipse*>(@=$4@>);  

   Pointer_Vector<real> *rv = new Pointer_Vector<real>;

   /* !!TODO:  LDF 2022.05.30.  Add an option for this.  */

   real eps = Point::epsilon();

   status = s->get_locations(pv, 
                             rv,
                             scanner_node,
                             static_cast<Transform*>(0),    /* |Transform*| Not needed in this rule.  */ 
                             static_cast<Superellipse*>(0), /* |Superellipse*| Not needed in this rule.   */
                             false,                         /* Don't quit if a |Point| isn't coplanar.  */
                             false,                         /* Don't quit if a |Point| is outside the superellipse.  */
                             true,                          /* |do_test|  */
                             true,                          /* |do_transform|  */
                             save,
                             static_cast<real*>(0),         /* Offset.  Not needed in this rule.     */
                             static_cast<Point*>(0),        /* Closest point.  Not needed in this rule.     */
                             eps                            /* Epsilon.   */
                            );    
 
   if (status != 0)
   {

      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `numeric_vector_secondary: GET_LOCATIONS "
                << "point_vector_secondary FROM superellipse_primary call_metapost_option_list':"
                << endl 
                << "`Superellipse::location' (version for multiple points) failed, returning " 
                << status << "."
                << endl 
                << "Failed to find location of point on superellipse.  Continuing." << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }
#if DEBUG_COMPILE
   else if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "In parser, rule `numeric_vector_secondary: GET_LOCATIONS "
                 << "point_vector_secondary FROM superellipse_primary "
                 << "call_metapost_option_list':"
                 << endl 
                 << "`Superellipse::location' (version for multiple points) succeeded, "
                 << "returning 0." 
                 << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   delete pv;
   delete s;   

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = static_cast<void*>(rv);

};



@q * (1) numeric_vector tertiary.  @>

@* \�numeric vector tertiary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> numeric_vector_tertiary@>

@q ***** (5) numeric_vector tertiary --> numeric_vector_secondary.@>
@ \�numeric vector tertiary> $\longrightarrow$ 
\�numeric vector secondary>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_tertiary: numeric_vector_secondary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q ***** (5) numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES path_secondary @>
@q ***** (5) call_metapost_option_list.                                               @>
@ \�numeric vector tertiary> $\longrightarrow$ \path tertiary> \.{INTERSECTIONTIMES}
\�path secondary> \�call metapost option list>.
\initials{LDF 2022.05.12.}

\LOG
\initials{LDF 2022.05.12.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES path_secondary call_metapost_option_list@>@/ 
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @;
   if (DEBUG)
     {
       cerr_strm << thread_name 
                 << "*** Parser: `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES "
                 << "path_secondary call_metapost_option_list'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>


   Pointer_Vector<real> *nv = new Pointer_Vector<real>;
   Path *p = static_cast<Path*>(@=$1@>);
   Path *q = static_cast<Path*>(@=$3@>);
   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

   bool save               = @=$4@> & 1U;
   bool clear              = @=$4@> & 2U;
   bool suppress_mp_stdout = @=$4@> & 4U;
   bool do_transform       = @=$4@> & 8U;    

@q *** (3) @>

   if (@=$4@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$4@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



   if (!p)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES"
                 << endl 
                 << "path_secondary call_metapost_option_list':"
                 << endl
                 << "`path_tertiary' is NULL.  Can't find intersectiontimes.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }
   else if (!q)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES"
                 << endl 
                 << "path_secondary call_metapost_option_list':"
                 << endl
                 << "`path_secondary' is NULL.  Can't find intersectiontimes.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }

@q ****** (6) @>

   else /* (|(p && q)|) */
   {
@q ******* (7) @>

       status = p->get_metapost_intersectiontimes(q, nv, scanner_node, 0,
                                                  origin_pt, x_axis_pt, do_transform, 
                                                  save, suppress_mp_stdout);

@q ******* (7) @>

       if (status != 0)
       {
           cerr_strm << thread_name 
                     << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary "
                     << "INTERSECTIONTIMES"
                     << endl 
                     << "path_secondary call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' failed, returning " << status << "."
                     << endl 
                     << "Failed to obtain intersectiontimes.  Continuing.";

           log_message(cerr_strm);
           cerr_message(cerr_strm, true);
           cerr_strm.str("");

       }  /* |if (status != 0)| */

@q ******* (7) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
           cerr_strm << thread_name 
                     << "In parser, rule `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES"
                     << endl 
                     << "path_secondary call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
       }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   }  /* |else| (|(p && q)|) */

@q ****** (6) @>

   if (p)
   {
      delete p;
      p = 0;
   }

   if (q)
   {
      delete q;
      q = 0;
   }

   if (scanner_node->metapost_output_struct)
   {
      delete scanner_node->metapost_output_struct;
      scanner_node->metapost_output_struct = 0;
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = static_cast<Pointer_Vector<real>*>(nv);

};

@q ***** (5) numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES_ALL path_secondary @>
@q ***** (5) with_resolutions_optional call_metapost_option_list.                         @>

@ \�numeric vector tertiary> $\longrightarrow$ \path tertiary> \.{INTERSECTIONTIMES\_ALL}
\�path secondary> \�with resolutions optional> \�call metapost option list>.
\initials{LDF 2022.05.14.}

\LOG
\initials{LDF 2022.05.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES_ALL path_secondary with_resolutions_optional @>
@=call_metapost_option_list@>@/ 
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @;
   if (DEBUG)
     {
       cerr_strm << thread_name 
                 << "*** Parser: `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES_ALL "
                 << "path_secondary with_resolutions_optional call_metapost_option_list'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>

   Pointer_Vector<real> *nv = static_cast<Pointer_Vector<real>*>(@=$4@>);

   real res0 = 2;
   real res1 = 2;

   if (nv == 0)
   {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`with_resolutions_optional' is NULL." << endl;
      }     
#endif /* |DEBUG_COMPILE|  */@; 

      nv = new Pointer_Vector<real>;
   }
   else
   {
      cerr << "`with_resolutions_optional' is non-NULL." << endl
           << "`nv->v.size()' == " << nv->v.size() << endl;

      if (nv->v.size() > 0)
      {
         res0 = *(nv->v[0]);

         if (nv->v.size() > 1)
            res1 = *(nv->v[1]);

         nv->v.clear();
      }
   }

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "res0 == " << res0 << endl
           << "res1 == " << res1 << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   Path *p = static_cast<Path*>(@=$1@>);
   Path *q = static_cast<Path*>(@=$3@>);
   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;    

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



   if (!p)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES_ALL"
                 << endl 
                 << "path_secondary with_resolutions_optional call_metapost_option_list':"
                 << endl
                 << "`path_tertiary' is NULL.  Can't find intersectiontimes.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }
   else if (!q)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary INTERSECTIONTIMES_ALL"
                 << endl 
                 << "path_secondary with_resolutions_optional call_metapost_option_list':"
                 << endl
                 << "`path_secondary' is NULL.  Can't find intersectiontimes.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }

@q ****** (6) @>

   else /* (|(p && q)|) */
   {
@q ******* (7) @>

       status = p->get_metapost_intersectiontimes(q, nv, scanner_node, 0,
                                                  origin_pt, x_axis_pt, do_transform, 
                                                  save, suppress_mp_stdout, true);

@q ******* (7) @>

       if (status != 0)
       {
           cerr_strm << thread_name 
                     << "ERROR!  In parser, rule `numeric_vector_tertiary:  path_tertiary "
                     << "INTERSECTIONTIMES_ALL"
                     << endl 
                     << "path_secondary with_resolutions_optional call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' failed, returning " << status << "."
                     << endl 
                     << "Failed to obtain intersectiontimes.  Continuing.";

           log_message(cerr_strm);
           cerr_message(cerr_strm, true);
           cerr_strm.str("");

       }  /* |if (status != 0)| */

@q ******* (7) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
           cerr_strm << thread_name 
                     << "In parser, rule `numeric_vector_tertiary:  path_tertiary "
                     << "INTERSECTIONTIMES_ALL"
                     << endl 
                     << "path_secondary with_resolutions_optional call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
       }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   }  /* |else| (|(p && q)|) */

@q ****** (6) @>

   if (p)
   {
      delete p;
      p = 0;
   }

   if (q)
   {
      delete q;
      q = 0;
   }

   if (scanner_node->metapost_output_struct)
   {
      delete scanner_node->metapost_output_struct;
      scanner_node->metapost_output_struct = 0;
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "nv->v.size() == " << nv->v.size() << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = static_cast<Pointer_Vector<real>*>(nv);

};

@q ** (2) with_resolutions_optional @>

@ \�with resolutions optional>.
\initials{LDF 2022.05.14.}

\LOG
\initials{LDF 2022.05.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_resolutions_optional@>

@q ** (2) with_resolutions_optional: /* Empty  */@>
@
@<Define rules@>=
@=with_resolutions_optional: /* Empty  */@>@/
{
   @=$$@> =  static_cast<void*>(0); 
}

@q ** (2) with_resolutions_optional: WITH_RESOLUTIONS numeric_list@>
@
@<Define rules@>=
@=with_resolutions_optional: WITH_RESOLUTIONS numeric_list@>@/
{
   
   @=$$@> =  static_cast<void*>($2); 
}

@q ** (2) with_resolutions_optional: WITH_RESOLUTION numeric_expression@>
@
@<Define rules@>=
@=with_resolutions_optional: WITH_RESOLUTION numeric_expression@>@/
{
   Pointer_Vector<real> *pv = new Pointer_Vector<real>;
   real *r = new real;
   *r = @=$2@>;

   *pv += r;
  
   @=$$@> =  static_cast<void*>(pv); 
}

@q * (1) numeric_vector expression.@>
@* \�numeric vector expression>.

\LOG
\initials{LDF 2005.01.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_vector_expression@>

@q ** (2) numeric_vector expression --> numeric_vector_tertiary.  @>
@*1 \�numeric vector expression> $\longrightarrow$ 
\�numeric vector tertiary>.

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_vector_expression: numeric_vector_tertiary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

