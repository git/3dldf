@q pbpvexpr.w @> 
@q Created by Laurence Finston Fr Nov  5 22:10:03 CET 2004  @>
     
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) |bool_point_vector| expressions.@>
@** {\bf bool\_point\_vector} expressions.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Created this file and wrote quite a few rules.  
\ENDLOG 

@q * (1) |bool_point_vector| primary.  @>
@* \�bool-point vector primary>.
\initials{LDF 2004.11.05.}
  
\LOG
\initials{LDF 2004.11.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> bool_point_vector_primary@>@/

@q ** (2) bool_point_vector_primary --> bool_point_vector_variable.@>
@*1 \�bool-point vector primary> $\longrightarrow$ 
\�bool-point vector variable>.  
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=bool_point_vector_primary: bool_point_vector_variable@>@/ 
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q **** (4) Error handling:  |entry == 0 || entry->object == 0|.@> 

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.11.05.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

@q **** (4) |!(entry == 0 || entry->object == 0)|.@> 

@ |!(entry == 0 || entry->object == 0)|.
\initials{LDF 2004.11.05.}

@<Define rules@>=
  else /* |!(entry == 0 || entry->object == 0)|  */

     {

        typedef Pointer_Vector<Bool_Point> BPV;

        BPV* bpv = new BPV;

        *bpv = *static_cast<BPV*>(entry->object);

        @=$$@> = static_cast<void*>(bpv);                    

     }  /* |else| (|!(entry == 0 || entry->object == 0)|)  */

};

@q ** (2) bool_point_vector_primary --> LEFT_PARENTHESIS  @>
@q ** (2) bool_point_vector_expression  RIGHT_PARENTHESIS.@>

@*1 \�bool-point vector primary> $\longrightarrow$ \.{LEFT\_PARENTHESIS}
\�bool-point vector expression> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=bool_point_vector_primary: LEFT_PARENTHESIS@>@/ 
@=bool_point_vector_expression RIGHT_PARENTHESIS@>@/ 
{
   
  @=$$@> = @=$1@>;

};

@q * (1) bool_point_vector secondary.@>

@* \�bool-point vector secondary>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> bool_point_vector_secondary@>
  
@q ** (2) bool_point_vector secondary --> bool_point_vector_primary.@>
@*1 \�bool-point vector secondary> $\longrightarrow$ 
\�bool-point vector primary>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=bool_point_vector_secondary: bool_point_vector_primary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q * (1) bool_point_vector tertiary.  @>

@* \�bool-point vector tertiary>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> bool_point_vector_tertiary@>

@q ***** (5) bool_point_vector tertiary --> bool_point_vector_secondary.@>
@ \�bool-point vector tertiary> $\longrightarrow$ 
\�bool-point vector secondary>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=bool_point_vector_tertiary: bool_point_vector_secondary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q **** (4) bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS  @>
@q **** (4) ellipse_secondary with_test_optional with_tolerance_optional.  @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ \�path tertiary> 
\.{INTERSECTION\_POINTS} \�ellipse secondary> \�with test optional> 
\�with tolerance optional>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.05.}
Added this rule.

\initials{LDF 2004.11.06.}
Corrected errors.

\initials{LDF 2005.10.24.}
Changed |ellipse_like_secondary| to |ellipse_secondary|.
Removed debugging code.

\initials{LDF 2005.10.27.}
Removed code.  Now calling |Scan_Parse::intersection_points_func|.

\initials{LDF 2005.10.27.}
Removed more code.  Now calling 
|Pointer_Vector<Bool_Point>::convert_pair_pointer|.
|Ellipse::intersection_points(Path)| returns a |Bool_Point_Pair|.
|Scan_Parse::intersection_points_func| creates a new |Bool_Point_Pair|
on the free store, assigns to it from the first one, and returns a 
|void*| that points to the new one.
|Pointer_Vector<Bool_Point>::convert_pair_pointer| creates two new
|Bool_Points| on the free store, pushes pointers to them onto 
the |Pointer_Vector<Bool_Point>|, and assigns to them from the 
two |Bool_Points| in the |Bool_Point_Pair|.  Then it deletes
the |Bool_Point_Pair*|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS ellipse_secondary @>@/
@=with_test_optional with_tolerance_optional @>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_secondary INTERSECTION_POINTS"
                << endl 
                << "ellipse_tertiary with_test_optional with_tolerance_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;

    Ellipse *e = static_cast<Ellipse*>(@=$3@>);
    Path    *p = static_cast<Path*>(@=$1@>);

    entry = 0;

    real tolerance = @=$5@>;  /* Default is $-1.0$, which is the same as the default value
                                 for the |tolerance| parameter of |Ellipse::intersection_points|.
                                 \initials{LDF 2022.12.24.}  */

    bool do_test = false;

    if (@=$4@> == WITH_TEST)
       do_test = true;

@q ****** (6) @>

    status = e->intersection_points(*p, bpv, do_test, tolerance, scanner_node);

    if (status != 0)
    {
       cerr_strm << "WARNING!  In parser, rule `bool_point_vector_tertiary: path_secondary INTERSECTION_POINTS"
                 << endl 
                 << "ellipse_tertiary with_test_optional with_tolerance_optional "
                 << "with_transform_optional with_path_optional"
                 << endl 
                 << "with_ellipse_optional':"
                 << endl
                 << "`Ellipse::intersection_points' failed, returning " << status << "."
                 << endl 
                 << "Failed to find intersection points of `path' and `ellipse'."
                 << endl 
                 << "Continuing." << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }

@q ****** (6) @>

    else
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
          cerr_strm << "In parser, rule `bool_point_vector_tertiary: path_secondary INTERSECTION_POINTS"
                    << endl 
                    << "ellipse_tertiary with_test_optional with_tolerance_optional "
                    << "with_transform_optional with_path_optional':"
                    << endl
                    << "`Ellipse::intersection_points' succeeded, returning 0."
                    << endl;

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
       }   
#endif /* |DEBUG_COMPILE|  */@;            

    }  /* |else|  */

@q ****** (6) @>

    @=$$@> = static_cast<void*>(bpv); 

@q ****** (6) @>

};

@q **** (4) bool_point_vector_tertiary: ellipse_tertiary  @> 
@q **** (4) INTERSECTION_POINTS path_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�ellipse tertiary> \.{INTERSECTION\_POINTS} \�path secondary>
\�with transform optional> \�with ellipse optional>.
\initials{LDF 2004.11.05.}

\LOG
\initials{LDF 2004.11.06.}
Added this rule.

\initials{LDF 2004.11.06.}
Changed |ellipse_like_tertiary| to |ellipse_tertiary| 
because the rule  \�ellipse-like tertiary> $\longrightarrow$ 
\�ellipse tertiary> was never reduced because of conflicts.

\initials{LDF 2005.10.27.}
Removed code.  Now calling |Scan_Parse::intersection_points_func|
and |Pointer_Vector<Bool_Point>::convert_pair_pointer|.
|Ellipse::intersection_points(Path)| returns a |Bool_Point_Pair|.
|Scan_Parse::intersection_points_func| creates a new |Bool_Point_Pair|
on the free store, assigns to it from the first one, and returns a 
|void*| that points to the new one.
|Pointer_Vector<Bool_Point>::convert_pair_pointer| creates two new
|Bool_Points| on the free store, pushes pointers to them onto 
the |Pointer_Vector<Bool_Point>|, and assigns to them from the 
two |Bool_Points| in the |Bool_Point_Pair|.  Then it deletes
the |Bool_Point_Pair*|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS @> 
@=path_secondary with_test_optional with_tolerance_optional @>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS"
                << endl 
                << "path_secondary with_test_optional with_tolerance_optional "
                << "with_transform_optional with_path_optional"
                << endl 
                << "with_ellipse_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "Not programmed yet." << endl;

   }  
#endif /* |DEBUG_COMPILE|  */@; 

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: ellipse_tertiary  @> 
@q **** (4) INTERSECTION_POINTS rectangle_secondary.      @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�ellipse tertiary> \.{INTERSECTION\_POINTS} \�rectangle secondary>.
\initials{LDF 2010.09.10.}

\LOG
\initials{LDF 2010.09.10.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS @> 
@=rectangle_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Ellipse, 
                                                                      Rectangle, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Ellipse*>(@=$1@>),
                                             static_cast<Rectangle*>(@=$3@>), 
                                             parameter)));   

    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: circle_tertiary  @> 
@q **** (4) INTERSECTION_POINTS path_secondary with_circle_optional.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�circle tertiary> \.{INTERSECTION\_POINTS} \�path secondary> \�with transform optional>
\�with circle optional>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: circle_tertiary INTERSECTION_POINTS @> 
@=path_secondary with_transform_optional with_circle_optional@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: circle_tertiary"
                << endl 
                << "INTERSECTION_POINTS path_secondary with_transform_optional with_circle_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
   Circle *c = static_cast<Circle*>(@=$1@>);
   Path   *p = static_cast<Path*>(@=$3@>);
   
   status = c->intersection_points(p, bpv, scanner_node);


    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: path_tertiary  @> 
@q **** (4) INTERSECTION_POINTS circle_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�circle secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS circle_secondary@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS circle_secondary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
 
    Path *p   = static_cast<Path*>(@=$1@>);
 
    Circle *c = static_cast<Circle*>(@=$3@>);
 
    status = c->intersection_points(p, bpv, scanner_node);
  
    @=$$@> = static_cast<void*>(bpv);

};

@q **** (4) bool_point_vector_tertiary: ellipse_tertiary  @> 
@q **** (4) INTERSECTION_POINTS ellipse_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�ellipse tertiary>.
\.{INTERSECTION\_POINTS} \�ellipse secondary>.
\initials{LDF 2004.11.06.}

\LOG
\initials{LDF 2004.11.06.}
Added this rule.

\initials{LDF 2005.10.27.}
Removed code.  Now calling |Scan_Parse::intersection_points_func| and
|Scan_Parse::convert|.

\initials{LDF 2005.10.28.}
Now calling |Scan_Parse::ellipse_intersection_func|.

\initials{LDF 2005.10.28.}
Now calling |Scan_Parse::ellipse_like_intersection_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS @> 
@=ellipse_secondary@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: ellipse_tertiary "
                << "INTERSECTION_POINTS ellipse_secondary'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   Ellipse *e0 = static_cast<Ellipse*>(@=$1@>);
   Ellipse *e1 = static_cast<Ellipse*>(@=$3@>);

   Bool_Point_Quadruple bpq = e0->intersection_points(*e1, scanner_node);

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   *bpv += bpq.first;
   *bpv += bpq.second;
   *bpv += bpq.third;
   *bpv += bpq.fourth;

   delete e0;
   e0 = 0;

   delete e1;
   e0 = 0;

   @=$$@> = static_cast<void*>(bpv);


};

@q **** (4) bool_point_vector_tertiary: circle_tertiary @> 
@q **** (4) INTERSECTION_POINTS circle_secondary.       @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�circle tertiary>.
\.{INTERSECTION\_POINTS} \�circle secondary>
\initials{LDF 2004.11.07.}

\LOG
\initials{LDF 2004.11.07.}
Added this rule.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::ellipse_like_intersection_func|.

\initials{LDF 2024.08.18.}
Revised.  Now calling |Circle::intersection_points| instead of 
|Scan_Parse::ellipse_like_intersection_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: circle_tertiary INTERSECTION_POINTS circle_secondary@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: circle_tertiary "
                << "INTERSECTION_POINTS circle_secondary'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   Circle *c0 = static_cast<Circle*>(@=$1@>);
   Circle *c1 = static_cast<Circle*>(@=$3@>);

   Bool_Point_Quadruple bpq = c0->intersection_points(*c1, scanner_node);

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   *bpv += bpq.first;
   *bpv += bpq.second;
   *bpv += bpq.third;
   *bpv += bpq.fourth;

   delete c0;
   c0 = 0;

   delete c1;
   c0 = 0;

   @=$$@> = static_cast<void*>(bpv);

};

@q **** (4) bool_point_vector_tertiary: ellipse_tertiary @> 
@q **** (4) INTERSECTION_POINTS circle_secondary.       @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�circle tertiary>.
\.{INTERSECTION\_POINTS} \�circle secondary>
\initials{LDF 2004.11.07.}

\LOG
\initials{LDF 2004.11.07.}
Added this rule.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::ellipse_like_intersection_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS @> 
@=circle_secondary@>@/
{

   // !! START HERE.  LDF 2024.08.18.  Program this.
   // There may be a more efficient way of finding the intersections
   // than just treating the circle as an ellipse.

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   Bool_Point bp;

   *bpv += bp;
  
   @=$$@> = static_cast<void*>(bpv);



};

@q **** (4) bool_point_vector_tertiary: circle_tertiary @> 
@q **** (4) INTERSECTION_POINTS ellipse_secondary.       @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�circle tertiary>.
\.{INTERSECTION\_POINTS} \�circle secondary>
\initials{LDF 2004.11.07.}

\LOG
\initials{LDF 2004.11.07.}
Added this rule.

\initials{LDF 2004.11.08.}
@:BUG FIX@> BUG FIX:  Changed |Bool_Point_Pair bpp| to 
|Bool_Point_Quadruple bpq|.  Now calling 
|c->Ellipse::intersection_points| explicitly, since the 
compiler didn't resolve the call to |c->intersection_points| 
this way.  Now allocating memory for four |Bool_Points| rather than
two and setting |bpv->ctr| accordingly.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::ellipse_like_intersection_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: circle_tertiary INTERSECTION_POINTS @> 
@=ellipse_secondary@>@/
{
   // !! START HERE.  LDF 2024.08.18.  Program this.
   // There may be a more efficient way of finding the intersections
   // than just treating the circle as an ellipse.

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   Bool_Point bp;

   *bpv += bp;
  
   @=$$@> = static_cast<void*>(bpv);

};

@q ** (2) bool_point_vector_tertiary: parabola_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�parabola tertiary>
\.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.11.17.}

\LOG
\initials{LDF 2005.11.17.}
Added this rule.

\initials{LDF 2005.11.21.}
Changed the left-hand side from \�point vector tertiary> to 
\�bool-point vector tertiary> and moved from \filename{pptvexpr.w} to 
this file (\filename{pbpvexpr.w}.

\initials{LDF 2005.11.21.}
Changed the name of |conic_section_line_intersection_func|
to |conic_section_intersection_func| and added a second 
template parameter.  Changed the |Path*| argument to 
a pointer to the type of the new template parameter.
Made the corresponding changes here.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: parabola_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Parabola, Path>(
                static_cast<Parabola*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: path_tertiary @> 
@q ** (2) INTERSECTION_POINTS parabola_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary>
\.{INTERSECTION\_POINTS} \�parabola secondary>.
\initials{LDF 2005.11.17.}

\LOG
\initials{LDF 2005.11.17.}
Added this rule.

\initials{LDF 2005.11.21.}
Changed the left-hand side from \�point vector tertiary> to 
\�bool-point vector tertiary> and moved from \filename{pptvexpr.w} to 
this file (\filename{pbpvexpr.w}.

\initials{LDF 2005.11.21.}
Changed the name of |conic_section_line_intersection_func|
to |conic_section_intersection_func| and added a second 
template parameter.  Changed the |Path*| argument to 
a pointer to the type of the new template parameter.
Made the corresponding changes here.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=parabola_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Parabola, Path>(
                static_cast<Parabola*>(@=$3@>),
                static_cast<Path*>(@=$1@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: parabola_tertiary @> 
@q ** (2) INTERSECTION_POINTS plane_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�parabola tertiary>
\.{INTERSECTION\_POINTS} \�plane secondary>.
\initials{LDF 2005.11.21.}

\LOG
\initials{LDF 2005.11.21.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: parabola_tertiary INTERSECTION_POINTS @> 
@=plane_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Parabola, Plane>(
                static_cast<Parabola*>(@=$1@>),
                static_cast<Plane*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: plane_tertiary @> 
@q ** (2) INTERSECTION_POINTS parabola_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�plane tertiary>
\.{INTERSECTION\_POINTS} \�parabola secondary>.
\initials{LDF 2005.11.21.}

\LOG
\initials{LDF 2005.11.21.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: plane_tertiary INTERSECTION_POINTS @> 
@=parabola_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Parabola, Plane>(
                static_cast<Parabola*>(@=$3@>),
                static_cast<Plane*>(@=$1@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: hyperbola_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�hyperbola tertiary>
\.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.11.25.}

\LOG
\initials{LDF 2005.11.25.}
Added this rule.

\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: hyperbola_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Hyperbola, Path>(
                static_cast<Hyperbola*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: path_tertiary @> 
@q ** (2) INTERSECTION_POINTS hyperbola_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary>
\.{INTERSECTION\_POINTS} \�hyperbola secondary>.
\initials{LDF 2005.11.25.}

\LOG
\initials{LDF 2005.11.25.}
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=hyperbola_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Hyperbola, Path>(
                static_cast<Hyperbola*>(@=$3@>),
                static_cast<Path*>(@=$1@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: hyperbola_tertiary @> 
@q ** (2) INTERSECTION_POINTS plane_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�hyperbola tertiary>
\.{INTERSECTION\_POINTS} \�plane secondary>.
\initials{LDF 2005.11.28.}

\LOG
\initials{LDF 2005.11.28.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: hyperbola_tertiary INTERSECTION_POINTS @> 
@=plane_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Hyperbola, Plane>(
                static_cast<Hyperbola*>(@=$1@>),
                static_cast<Plane*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: plane_tertiary @> 
@q ** (2) INTERSECTION_POINTS hyperbola_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�plane tertiary>
\.{INTERSECTION\_POINTS} \�hyperbola secondary>.
\initials{LDF 2005.11.28.}

\LOG
\initials{LDF 2005.11.28.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: plane_tertiary INTERSECTION_POINTS @> 
@=hyperbola_secondary@>@/
{

   @=$$@> = Scan_Parse::conic_section_intersection_func<Hyperbola, Plane>(
                static_cast<Hyperbola*>(@=$3@>),
                static_cast<Plane*>(@=$1@>),
                parameter);
};

@q **** (4) bool_point_vector_tertiary: path_tertiary  @> 
@q **** (4) INTERSECTION_POINTS polygon_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary>
\.{INTERSECTION\_POINTS} \�polygon secondary>.
\initials{LDF 2004.11.07.}

\LOG
\initials{LDF 2004.11.07.}
Added this rule.

\initials{LDF 2005.10.24.}
Changed |polygon_like_secondary| to |polygon_secondary|.
Removed debugging code.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::intersection_points_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=polygon_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Polygon, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Polygon*>(@=$3@>),
                                             static_cast<Path*>(@=$1@>), 
                                             parameter)));   
    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: polygon_tertiary @> 
@q **** (4) INTERSECTION_POINTS path_secondary.          @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�polygon tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2004.11.09.}

\LOG
\initials{LDF 2004.11.09.}
Added this rule.

\initials{LDF 2005.10.24.}
Changed |polygon_like_tertiary| to |polygon_tertiary|.
Removed debugging code.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::intersection_points_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: polygon_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Polygon, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Polygon*>(@=$1@>),
                                             static_cast<Path*>(@=$3@>), 
                                             parameter)));   

    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: path_tertiary  @> 
@q **** (4) INTERSECTION_POINTS reg_polygon_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary>
\.{INTERSECTION\_POINTS} \�regular polygon secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=reg_polygon_secondary@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS reg_polygon_secondary'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;

    Path *p        = static_cast<Path*>(@=$1@>);
    Reg_Polygon *r = static_cast<Reg_Polygon*>(@=$3@>);

    if (p && r)
    {
       Bool_Point_Pair bpp = r->intersection_points(*p, scanner_node);

       if (bpp.first.pt != INVALID_POINT)
          *bpv += bpp.first;
       if (bpp.second.pt != INVALID_POINT)
          *bpv += bpp.second;
    }

    if (p)
    {
       delete p;
       p = 0;
    }     

    if (r)
    {
       delete r;
       r = 0;
    }     

    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: reg_polygon_tertiary @> 
@q **** (4) INTERSECTION_POINTS path_secondary.          @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�regular polygon tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: reg_polygon_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;

bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Reg_Polygon, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Reg_Polygon*>(@=$1@>),
                                             static_cast<Path*>(@=$3@>), 
                                             parameter)));   
    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: path_tertiary  @> 
@q **** (4) INTERSECTION_POINTS rectangle_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary>
\.{INTERSECTION\_POINTS} \�rectangle secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=rectangle_secondary@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS rectangle_secondary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;   

    Path      *q = static_cast<Path*>(@=$1@>);    
    Rectangle *r = static_cast<Rectangle*>(@=$3@>);

@q ****** (6) @>

    status = r->intersection_points(q, bpv, scanner_node);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr_strm << "In parser, rule `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS rectangle_secondary':"
                << endl
                << " `status' == " << status << ".";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 


@q ****** (6) @>
@
@<Define rules@>= 

    if (q)
    {
       delete q;
       q = 0;
    }

    if (r)
    {
       delete r;
       r = 0;
    }

    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: rectangle_tertiary @> 
@q **** (4) INTERSECTION_POINTS path_secondary.          @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�rectangle tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: rectangle_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Rectangle, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Rectangle*>(@=$1@>),
                                             static_cast<Path*>(@=$3@>), 
                                             parameter)));   

    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: path_tertiary  @> 
@q **** (4) INTERSECTION_POINTS triangle_secondary.@> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�triangle secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=triangle_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Triangle, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Triangle*>(@=$3@>),
                                             static_cast<Path*>(@=$1@>), 
                                             parameter)));   
    @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: triangle_tertiary @> 
@q **** (4) INTERSECTION_POINTS path_secondary.          @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�triangle tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: triangle_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

    Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;
   
    bpv->convert_pair_pointer(static_cast<Bool_Point_Pair*>(
                                 Scan_Parse::intersection_points_func<Triangle, 
                                                                      Path, 
                                                                      Bool_Point_Pair>
                                            (static_cast<Triangle*>(@=$1@>),
                                             static_cast<Path*>(@=$3@>), 
                                             parameter)));   

    @=$$@> = static_cast<void*>(bpv); 

};

@q ** (2) bool_point_vector_tertiary: ellipsoid_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�ellipsoid tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.12.07.}

\LOG
\initials{LDF 2005.12.07.}
Added this rule.

\initials{LDF 2005.12.09.}
@:BUG FIX@> BUG FIX:  Now deleting |Ellipsoid* e| and |Path* p|.

\initials{LDF 2005.12.09.}
Changed \�point vector tertiary> to \�bool-point vector tertiary>, and
moved this rule from \filename{pptvexpr.w} to 
this file(\filename{pbpvexpr.w}).
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: ellipsoid_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{
     Ellipsoid* e = static_cast<Ellipsoid*>(@=$1@>);
     Path*      p = static_cast<Path*>(@=$3@>);
#if 0

     if (!(e && p))
        @=$$@> = static_cast<void*>(0);

     else 
        @=$$@> = e->intersection_points(p, static_cast<Scanner_Node>(parameter));
#else
        @=$$@> = static_cast<void*>(0);
#endif 

     delete e;
     delete p;
     e = 0;
     p = 0;

};

@q ** (2) bool_point_vector_tertiary: path_tertiary @> 
@q ** (2) INTERSECTION_POINTS ellipsoid_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�ellipsoid secondary>.
\initials{LDF 2005.12.07.}

\LOG
\initials{LDF 2005.12.07.}
Added this rule.

\initials{LDF 2005.12.09.}
Changed \�point vector tertiary> to \�bool-point vector tertiary>, and
moved this rule from \filename{pptvexpr.w} to 
this file(\filename{pbpvexpr.w}).
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=ellipsoid_secondary@>@/
{

     Path*      p = static_cast<Path*>(@=$1@>);
     Ellipsoid* e = static_cast<Ellipsoid*>(@=$3@>);

#if 0 
     if (!(e && p))
        @=$$@> = static_cast<void*>(0);

     else 
        @=$$@> = e->intersection_points(p, static_cast<Scanner_Node>(parameter));
#else
        @=$$@> = static_cast<void*>(0);
#endif 

     delete e;
     delete p;
     e = 0;
     p = 0;
};

@q **** (4) bool_point_vector_tertiary: sphere_tertiary @> 
@q **** (4) INTERSECTION_POINTS path_secondary.         @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�sphere tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: sphere_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{

    @=$$@> = Scan_Parse::sphere_line_intersection_func(
                static_cast<Sphere*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);  

};

@q **** (4) bool_point_vector_tertiary: path_tertiary @> 
@q **** (4) INTERSECTION_POINTS sphere_secondary.     @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�sphere secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=sphere_secondary@>@/
{

    @=$$@> = Scan_Parse::sphere_line_intersection_func(
                static_cast<Sphere*>(@=$3@>),
                static_cast<Path*>(@=$1@>),
                parameter);  

};

@q ** (2) bool_point_vector_tertiary: cuboid_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�cuboid tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2005.12.09.}

\LOG
\initials{LDF 2005.12.09.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: cuboid_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{
     Cuboid* c = static_cast<Cuboid*>(@=$1@>);
     Path*   p = static_cast<Path*>(@=$3@>);
#if 0 

     if (!(c && p))
        @=$$@> = static_cast<void*>(0);

     else 
        @=$$@> = c->intersection_points(*p, static_cast<Scanner_Node>(parameter));

#else
        @=$$@> = static_cast<void*>(0);

#endif 

     delete c;
     delete p;
     c = 0;
     p = 0;
};

@q ** (2) bool_point_vector_tertiary: path_tertiary @> 
@q ** (2) INTERSECTION_POINTS cuboid_secondary.  @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�cuboid secondary>.
\initials{LDF 2005.12.09.}

\LOG
\initials{LDF 2005.12.09.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=cuboid_secondary@>@/
{
     Cuboid* c = static_cast<Cuboid*>(@=$3@>);
     Path*      p = static_cast<Path*>(@=$1@>);

#if 0 
     if (!(c && p))
         @=$$@> = static_cast<void*>(0);

     else 
        @=$$@> = c->intersection_points(*p, static_cast<Scanner_Node>(parameter));
#else 
         @=$$@> = static_cast<void*>(0);
#endif 

     delete c;
     delete p;
     c = 0;
     p = 0;
};

@q ** (2) bool_point_vector_tertiary: polyhedron_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.             @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\�polyhedron tertiary>
\.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2006.01.20.}

\LOG
\initials{LDF 2006.01.20.}
Added this rule.

\initials{LDF 2006.01.23.}
Changed \�point vector tertiary> to \�bool-point vector tertiary>.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: polyhedron_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{
    @=$$@> = polyhedron_path_intersection_func(
                static_cast<Polyhedron*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: path_tertiary      @> 
@q ** (2) INTERSECTION_POINTS polyhedron_secondary. @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ 
\� path tertiary>
\.{INTERSECTION\_POINTS} \�polyhedron secondary>.
\initials{LDF 2006.01.20.}

\LOG
\initials{LDF 2006.01.20.}
Added this rule.

Changed \�point vector tertiary> to \�bool-point vector tertiary>.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=polyhedron_secondary@>@/
{
    @=$$@> = polyhedron_path_intersection_func(
                static_cast<Polyhedron*>(@=$3@>),
                static_cast<Path*>(@=$1@>),
                parameter);
};


@q ** (2) bool_point_vector_tertiary: path_tertiary @> 
@q ** (2) INTERSECTION_POINTS path_secondary.       @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ \� path tertiary> 
\.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2021.12.12.}

\LOG
\initials{LDF 2021.12.12.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS path_secondary@>@/
{
    @=$$@> = path_path_intersection_func(
                static_cast<Path*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);
};

@q ** (2) bool_point_vector_tertiary: triangle_tertiary @> 
@q ** (2) INTERSECTION_POINTS triangle_secondary.       @> 

@*1 \�bool-point vector tertiary> $\longrightarrow$ \� triangle tertiary> 
\.{INTERSECTION\_POINTS} \�triangle secondary>.
\initials{LDF 2021.12.12.}

\LOG
\initials{LDF 2021.12.12.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: triangle_tertiary INTERSECTION_POINTS triangle_secondary@>@/
{
    @=$$@> = path_path_intersection_func(
                static_cast<Path*>(@=$1@>),
                static_cast<Path*>(@=$3@>),
                parameter);
};


@q **** (4) bool_point_vector_tertiary: path_tertiary   @> 
@q **** (4) INTERSECTION_POINTS superellipse_secondary. @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�superellipse secondary>.
\initials{LDF 2022.05.27.}

\LOG
\initials{LDF 2022.05.27.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=superellipse_secondary call_metapost_option_list @>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 


#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS superellipse_secondary call_metapost_option_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   bool save               = @=$4@> &  1U;
   bool clear              = @=$4@> &  2U;
   bool suppress_mp_stdout = @=$4@> &  4U;
   bool do_transform       = @=$4@> &  8U;
   bool do_test            = @=$4@> & 64U;

   Path *p = static_cast<Path*>(@=$1@>);
   Superellipse *s = static_cast<Superellipse*>(@=$3@>);

   Pointer_Vector<Bool_Point>* bpv = new Pointer_Vector<Bool_Point>;

   status = s->intersection_points(*p, bpv, do_test, save, do_transform, scanner_node); 

   if (status != 0)
   {
      cerr_strm << "ERROR!  In parser, rule `bool_point_vector_tertiary:"
                << "path_tertiary" << endl 
                << "INTERSECTION_POINTS superellipse_secondary call_metapost_option_list':"
                << endl
                << "`Superellipse::intersection_points' failed, returning "
                << status << "." << endl 
                << "Failed to find intersection points.  Continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");


   }
   
@q ****** (6) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, rule `bool_point_vector_tertiary:"
                << "path_tertiary" << endl 
                << "INTERSECTION_POINTS superellipse_secondary call_metapost_option_list':"
                << endl
                << "`Superellipse::intersection_points' succeeded, returning 0."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

   delete p;
   delete s;

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = static_cast<void*>(bpv); 

};

@q **** (4) bool_point_vector_tertiary: superellipse_tertiary @> 
@q **** (4) INTERSECTION_POINTS superellipse_secondary        @> 
@q **** (4) call_metapost_option_list.                        @>

@*3 \�bool-point vector tertiary> $\longrightarrow$ \�superellipse tertiary>.
\.{INTERSECTION\_POINTS} \�superellipse secondary> \�call metapost option list>.
\initials{LDF 2022.06.01.}

\LOG
\initials{LDF 2022.06.01.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: superellipse_tertiary INTERSECTION_POINTS @> 
@=superellipse_secondary call_metapost_option_list@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: superellipse_tertiary "
                << "INTERSECTION_POINTS superellipse_secondary call_metapost_option_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   Superellipse *q0 = static_cast<Superellipse*>(@=$1@>); 
   Superellipse *q1 = static_cast<Superellipse*>(@=$3@>); 

#if 0 
   q0->show("*q0:");
   q1->show("*q1:");
#endif 

   bool save               = @=$4@> &  1U;
   bool clear              = @=$4@> &  2U;
   bool suppress_mp_stdout = @=$4@> &  4U;
   bool do_transform       = @=$4@> &  8U;
   bool do_test            = @=$4@> & 64U;
   bool with_tolerance     = @=$4@> & 128U;

   real tolerance;

   if (scanner_node->tolerance != 0)
   {
      if (*(scanner_node->tolerance) > 0)
         tolerance = *(scanner_node->tolerance);

      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   }
   else
      tolerance = Point::epsilon() * 10;

   tolerance = max(tolerance, Point::epsilon() * 10);  /* Ensure that |tolerance| is at 
                                                          least 10 times |Point::epsilon()|  
                                                          \initials{LDF 2022.06.01.}  */

   status = q0->intersection_points(*q1, *bpv, scanner_node, save, do_transform, do_test, tolerance);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "`Superellipse::intersection_points' returned " << status << endl;
      cerr << "bpv->v.size() == " << bpv->v.size() << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (q0)
     delete q0;

   if (q1)
     delete q1;

   @=$$@> = static_cast<void*>(bpv);     

};

@q **** (4) bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS    @> 
@q **** (4) cone_secondary cone_intersection_option_list. @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�cone secondary>
\�cone intersection option list>.
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=cone_secondary cone_intersection_option_list@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS cone_secondary cone_intersection_option_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>
 
   Cone *c = static_cast<Cone*>(@=$3@>);
   Path *q = static_cast<Path*>(@=$1@>);        

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   status = c->intersection_points(q, bpv, 0, -1, scanner_node);

@q ****** (6) @>

   if (status == 2)
   {
       cerr << "WARNING!  In parser, rule `bool_point_vector_tertiary: path_tertiary"
            << endl 
            << "INTERSECTION_POINTS cone_secondary cone_intersection_option_list':"
            << endl 
            << "`Cone::intersection_points' returned 2:"
            << endl 
            << "Failed to find first intersection point." << endl;

         @=$$@> = static_cast<void*>(bpv);
 
   }

@q ****** (6) @>

   else if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `bool_point_vector_tertiary: path_tertiary"
            << endl 
            << "INTERSECTION_POINTS cone_secondary cone_intersection_option_list':"
            << endl 
            << "`Cone::intersection_points' failed, returning " << status << "."
            << endl 
            << "Failed to find intersection points.  Will try to continue." << endl;
 
       delete bpv;
       bpv = 0;

       @=$$@> = static_cast<void*>(0);

   }

@q ****** (6) @>

   else
   {
#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "In parser, rule `bool_point_vector_tertiary: path_tertiary"
                  << endl 
                  << "INTERSECTION_POINTS cone_secondary cone_intersection_option_list':"
                  << endl 
                  << "`Cone::intersection_points' succeeded, returning 0."
                  << endl;
         }     
#endif /* |DEBUG_COMPILE|  */@;   

         @=$$@> = static_cast<void*>(bpv);
  
   }

@q ****** (6) @>
 
   if (c)
   {
      delete c;
      c = 0;
   }


   if (q)
   {
     delete q;
     q = 0;
   }

};

@q **** (4) bool_point_vector_tertiary: cone_tertiary INTERSECTION_POINTS @> 
@q **** (4) path_secondary cone_intersection_option_list. @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�cone tertiary> \.{INTERSECTION\_POINTS} \�path secondary>
\�cone intersection option list>.
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: cone_tertiary INTERSECTION_POINTS @> 
@=path_secondary cone_intersection_option_list@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: cone_tertiary INTERSECTION_POINTS"
                << "path_secondary cone_intersection_option_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>
 
   Cone *c = static_cast<Cone*>(@=$1@>);
   Path *q = static_cast<Path*>(@=$3@>);        

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   status = c->intersection_points(q, bpv, 0, -1, scanner_node);

   if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `bool_point_vector_tertiary: cone_tertiary"
            << endl 
            << "INTERSECTION_POINTS path_secondary cone_intersection_option_list':"
            << endl 
            << "`Cone::intersection_points' failed, returning " << status << "."
            << endl 
            << "Failed to find intersection points.  Will try to continue." << endl;
 
       delete bpv;
       bpv = 0;

       @=$$@> = static_cast<void*>(0);

   }

@q ****** (6) @>

   else
   {
#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "In parser, rule `bool_point_vector_tertiary: cone_tertiary"
                  << endl 
                  << "INTERSECTION_POINTS path_secondary cone_intersection_option_list':"
                  << endl 
                  << "`Cone::intersection_points' succeeded, returning 0."
                  << endl;
         }     
#endif /* |DEBUG_COMPILE|  */@;   

         @=$$@> = static_cast<void*>(bpv);
  
   }

@q ****** (6) @>

   if (c)
   {
      delete c;
      c = 0;
   }


   if (q)
   {
     delete q;
     q = 0;
   }

};

@q **** (4) bool_point_vector_tertiary: path_tertiary @> 
@q **** (4) INTERSECTION_POINTS cylinder_secondary.   @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�path tertiary> \.{INTERSECTION\_POINTS} \�cylinder secondary>
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: path_tertiary INTERSECTION_POINTS @> 
@=cylinder_secondary@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: path_tertiary "
                << "INTERSECTION_POINTS cylinder_secondary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>
 
   Cylinder *c = static_cast<Cylinder*>(@=$3@>);
   Path *q = static_cast<Path*>(@=$1@>);        

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   status = c->intersection_points(q, bpv, 0, scanner_node);

   if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `bool_point_vector_tertiary: path_tertiary"
            << endl 
            << "INTERSECTION_POINTS cylinder_secondary':"
            << endl 
            << "`Cylinder::intersection_points' failed, returning " << status << "."
            << endl 
            << "Failed to find intersection points.  Will try to continue." << endl;
 
       delete bpv;
       bpv = 0;

       @=$$@> = static_cast<void*>(0);

   }

@q ****** (6) @>

   else
   {
#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "In parser, rule `bool_point_vector_tertiary: path_tertiary"
                  << endl 
                  << "INTERSECTION_POINTS cylinder_secondary':"
                  << endl 
                  << "`Cylinder::intersection_points' succeeded, returning 0."
                  << endl;
         }     
#endif /* |DEBUG_COMPILE|  */@;   

         @=$$@> = static_cast<void*>(bpv);
  
   }

@q ****** (6) @>

   delete c;
   delete q;

   c = 0;
   q = 0;

};

@q **** (4) bool_point_vector_tertiary: cylinder_tertiary            @> 
@q **** (4) INTERSECTION_POINTS path_secondary. @> 

@*3 \�bool-point vector tertiary> $\longrightarrow$ 
\�cylinder tertiary> \.{INTERSECTION\_POINTS} \�path secondary>.
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=bool_point_vector_tertiary: cylinder_tertiary INTERSECTION_POINTS @> 
@=path_secondary@>@/
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `bool_point_vector_tertiary: cylinder_tertiary "
                << "INTERSECTION_POINTS path_secondary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>
 
   Cylinder *c = static_cast<Cylinder*>(@=$1@>);
   Path *q = static_cast<Path*>(@=$3@>);        

   Pointer_Vector<Bool_Point> *bpv = new Pointer_Vector<Bool_Point>;

   status = c->intersection_points(q, bpv, 0, scanner_node);

   if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `bool_point_vector_tertiary: cylinder_tertiary"
            << endl 
            << "INTERSECTION_POINTS path_secondary':"
            << endl 
            << "`Cylinder::intersection_points' failed, returning " << status << "."
            << endl 
            << "Failed to find intersection points.  Will try to continue." << endl;
 
       delete bpv;
       bpv = 0;

       @=$$@> = static_cast<void*>(0);

   }

@q ****** (6) @>

   else
   {
#if DEBUG_COMPILE
         if (DEBUG)
         { 
             cerr << "In parser, rule `bool_point_vector_tertiary: cylinder_tertiary"
                  << endl 
                  << "INTERSECTION_POINTS path_secondary':"
                  << endl 
                  << "`Cylinder::intersection_points' succeeded, returning 0."
                  << endl;
         }     
#endif /* |DEBUG_COMPILE|  */@;   

         @=$$@> = static_cast<void*>(bpv);
  
   }

@q ****** (6) @>

   delete c;
   delete q;

   c = 0;
   q = 0;

};

@q * (1) @>
@
\LOG
\initials{LDF 2024.12.02.}
Added these type declarations.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> cone_intersection_option_list@>
@=%type <int_value> cone_intersection_option@>

@q ** (2) @>
@
\LOG
\initials{LDF 2024.12.02.}
Added this rule
\ENDLOG

@<Define rules@>=
@=cone_intersection_option_list:  /* Empty */@>@/ 
{
   @=$$@> = 0;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.12.02.}
Added this rule
\ENDLOG

@<Define rules@>=
@=cone_intersection_option_list:  cone_intersection_option_list cone_intersection_option@>@/ 
{
   @=$$@> = 0;
};


@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.30.}
Added this rule
\ENDLOG

@<Define rules@>=
@=cone_intersection_option: WITH_ALL_POINTS@>@/ 
{
   @=$$@> = 0;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.30.}
Added this rule
\ENDLOG

@<Define rules@>=
@=cone_intersection_option: WITH_TOLERANCE numeric_expression@>@/ 
{
   @=$$@> = 0;
};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.11.30.}
Added this rule
\ENDLOG

@<Define rules@>=
@=cone_intersection_option: WITH_TEST_MANTLE@>@/ 
{
   @=$$@> = 0;
};

@q * (1) bool_point_vector expression.@>
@* \�bool-point vector expression>.

\LOG
\initials{LDF 2004.11.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> bool_point_vector_expression@>

@q ** (2) bool_point_vector expression --> bool_point_vector_tertiary.  @>
@*1 \�bool-point vector expression> $\longrightarrow$ 
\�bool-point vector tertiary>.

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=bool_point_vector_expression: bool_point_vector_tertiary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

