@q psetcmnd.w @> 
@q Created by Laurence Finston Do Okt 28 22:52:44 CEST 2004 @>
     
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (1) Set Commands.  @>
@* Set Commands.  

\LOG
\initials{LDF 2004.10.28.}
Created this file.  It contains rules removed from 
\filename{pcommand.w}.
\ENDLOG

@q ** (2) colors.@>  
@*1 {\bf colors}.
\initials{LDF 2004.12.19.}

\LOG
\initials{LDF 2004.12.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET color_variable LEFT_PARENTHESIS  @>
@q *** (3) numeric_expression COMMA numeric_expression COMMA    @> 
@q *** (3) numeric_expression RIGHT_PARENTHESIS.                @> 

@*2 \�command> $\longrightarrow$ \.{SET} \�color variable> 
\.{LEFT\_PARENTHESIS} \�numeric expression> \.{COMMA} 
\�numeric expression> \.{COMMA} \�numeric expression> 
\.{RIGHT\_PARENTHESIS}.                

!! PLEASE NOTE!  Using this rule is deprecated!  It's been replaced by the 
rule \�color assignment> $\longrightarrow$ \�color variable> \.{ASSIGN} \�numeric list>
\<with type optional> in \filename{passign.w}.  It won't be removed because doing so might
break old (3dldf) code.
\initials{LDF 2021.11.11.}

\LOG
\initials{LDF 2004.06.30.}
Added this rule.

\initials{LDF 2004.08.14.}
Removed code from this rule and put it into 
|Scan_Parse::set_color|, which is defined in 
\filename{scanprse.web}.
\ENDLOG 

@q **** (4) Definition.@> 

@<Define rules@>=

@=command: SET color_variable numeric_list with_type_optional@>@/
{
@q ***** (5) @>

  @<Common declarations for rules@>@;
         
  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  cerr << "entry->name == " << entry->name << endl;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET color_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>
@
@<Define rules@>=

  Pointer_Vector<real>* w = static_cast<Pointer_Vector<real>*>(@=$3@>); 

  for (vector<real*>::iterator iter = w->v.begin(); iter != w->v.end(); ++iter)
  {
     cerr << "**iter == " << **iter << endl;
  }

  real red_part     = 0.0;
  real green_part   = 0.0;
  real blue_part    = 0.0;
  real cyan_part    = 0.0;
  real magenta_part = 0.0;
  real yellow_part  = 0.0;
  real black_part   = 0.0;
  real grey_part    = 0.0;

  if (w->v.size() == 1)
  {
     grey_part = *(w->v[0]);
  }
  else if (w->v.size() > 1)
  {
     red_part = *(w->v[0]);
  }
  if (w->v.size() > 1)
  {
     green_part = *(w->v[1]);
  }
  if (w->v.size() > 2)
  {
     blue_part = *(w->v[2]);
  }
  if (w->v.size() > 3)
  {
     cyan_part = *(w->v[3]);
  }
  if (w->v.size() > 4)
  {
     magenta_part = *(w->v[4]);
  }
  if (w->v.size() > 5)
  {
     yellow_part = *(w->v[5]);
  }
  if (w->v.size() > 6)
  {
     black_part = *(w->v[6]);
  }

@q ***** (5) @>
@
@<Define rules@>=

#if LDF_REAL_FLOAT
  red_part = fmaxf(red_part, 0);
  red_part = fminf(red_part, 1);

  green_part = fmaxf(green_part, 0);
  green_part = fminf(green_part, 1);

  blue_part = fmaxf(blue_part, 0);
  blue_part = fminf(blue_part, 1);

  cyan_part = fmaxf(cyan_part, 0);
  cyan_part = fminf(cyan_part, 1);

  magenta_part = fmaxf(magenta_part, 0);
  magenta_part = fminf(magenta_part, 1);

  yellow_part = fmaxf(yellow_part, 0);
  yellow_part = fminf(yellow_part, 1);

  black_part = fmaxf(black_part, 0);
  black_part = fminf(black_part, 1);

  grey_part = fmaxf(grey_part, 0);
  grey_part = fminf(grey_part, 1);
#else 
  red_part = fmax(red_part, 0);
  red_part = fmin(red_part, 1);

  green_part = fmax(green_part, 0);
  green_part = fmin(green_part, 1);

  blue_part = fmax(blue_part, 0);
  blue_part = fmin(blue_part, 1);

  cyan_part = fmax(cyan_part, 0);
  cyan_part = fmin(cyan_part, 1);

  magenta_part = fmax(magenta_part, 0);
  magenta_part = fmin(magenta_part, 1);

  yellow_part = fmax(yellow_part, 0);
  yellow_part = fmin(yellow_part, 1);

  black_part = fmax(black_part, 0);
  black_part = fmin(black_part, 1);

  grey_part = fmax(grey_part, 0);
  grey_part = fmin(grey_part, 1);
#endif 

@q ***** (5) @>
@
@<Define rules@>=

  Int_Void_Ptr ivp = set_color(static_cast<Scanner_Node>(parameter),
                               entry, 
                               red_part, green_part, blue_part,
                               cyan_part, magenta_part, yellow_part,
                               black_part, grey_part, entry->name, @=$4@>);

@q ***** (5) Error handling:  |Scan_Parse::set_color| failed.@>

@ Error handling:  |Scan_Parse::set_color| failed.
\initials{LDF 2004.10.28.}

@<Define rules@>=

  if (ivp.i != 0) /*   */
  {
    
    @=$$@> = static_cast<void*>(0); 
    
  } /* |if (ivp.i != 0)| (|set_color| failed.)  */

@q ***** (5) |Scan_Parse::set_color| succeeded.@>

@ |Scan_Parse::set_color| succeeded.
\initials{LDF 2004.10.28.}

@<Define rules@>=
   
  else /* (|ivp.i == 0|,  |set_color| succeeded.)  */
  {
      @=$$@> = ivp.v;

  } /* |else| (|ivp.i == 0|, |set_color| succeeded.)  */

@q ***** (5).@> 

  delete w;
  w = 0;

};

@q *** (3) command --> SET color_part   @> 
@q *** (3) numeric_secondary color_variable.@> 

@*2 \�command> $\longrightarrow$ \.{SET} \�color part> 
\�numeric secondary> \�color variable>.
\initials{LDF 2004.12.20.}

\LOG
\initials{LDF 2004.12.20.}
Added this rule.  When I tried using |numeric_expression| or
|numeric_tertiary|, this rule wasn't reduced and the parser 
signalled an error.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=command: SET color_part numeric_secondary color_variable@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$4@>); 

@q ***** (5) Error handling:  |entry == 0 |.@> 
@ Error handling:  |entry == 0 |.
\initials{LDF 2004.12.20.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

} /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@> 

@ |entry != 0|.
\initials{LDF 2004.12.20.}

@<Define rules@>=
 
   else /* |entry != 0|  */

      {

          Color* c;

@q ****** (6) |entry->object == 0|.@> 

@ |entry->object == 0|.  Try to allocate memory for a new |Color|
for |entry->object|.
Rethrow |bad_alloc| upon error.
\initials{LDF 2004.12.20.}

@<Define rules@>=
   
         if (entry->object == static_cast<void*>(0))
            {

                c = create_new<Color>(0);

                entry->object = static_cast<void*>(c); 

            }  /* |if (entry->object == 0)|  */

@q ****** (6) |entry->object != 0|.@> 

@ |entry->object != 0|.
\initials{LDF 2004.12.20.}

@<Define rules@>=         
 
         else  /* |entry->object != 0|  */
             c = static_cast<Color*>(entry->object);

@q ****** (6).@> 

          if (@=$2@> == RED_PART)
             c->set_red_part(@=$3@>);

          else if (@=$2@> == GREEN_PART)
             c->set_green_part(@=$3@>);

          else if (@=$2@> == BLUE_PART)
             c->set_blue_part(@=$3@>);

@q ****** (6) Error handling:  Invalid |color_part|.@> 
@ Error handling:  Invalid |color_part|.
\initials{LDF 2004.12.20.}

@<Define rules@>=

          else /* Invalid |color_part|.  */
             {

}  /* |else| (Invalid |color_part|).  */

@q ****** (6) @> 

      }   /* |else| (|entry != 0|)  */

@q ***** (5) @> 
   
   @=$$@> = static_cast<void*>(0);

};

@q ** (2) focuses.@>
@*1 {\bf focuses}.  
\initials{LDF 2004.12.19.}

\LOG
\initials{LDF 2004.12.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET focus_variable WITH_POSITION @>
@q *** (3) point_expression WITH_DIRECTION point_expression @> 
@q *** (3) WITH_DISTANCE numeric_expression                 @>
@q *** (3) with_angle_optional with_axis_optional.          @>

@*2 \�command> $\longrightarrow$ \.{SET} \�focus variable>
\.{WITH\_POSITION}
\�point expression> \.{WITH\_DIRECTION} \�point expression>  
\.{WITH\_DISTANCE} \�numeric expression>                 
\�with angle optional> \�with axis optional>.          

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.

\initials{LDF 2004.06.24.} 
Now trying to lock and unlock |entry->mutex|.  
Setting |@=$$@>| to |entry->object|, so that 
I'll be able to put |set_commands| on the right-hand 
side of assignments. 

\initials{LDF 2004.08.14.}
Removed code from this rule and put it into 
|Scan_Parse::set_focus|, which is defined in 
\filename{scanprse.web}.

\initials{LDF 2004.08.14.}
Now passing |X_AXIS|, |Y_AXIS|, and |Z_AXIS| to
|set_focus|.  This makes it possible to leave
the definition of this function in \filename{scanprse.web}.  
|X_AXIS|, |Y_AXIS|, and |Z_AXIS| are
terminal symbols declared in \filename{pbsndecl.w} 
and therefore unknown when 
\filename{scanprse.web} is compiled.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 

@=command: SET focus_variable WITH_POSITION point_expression@>@/
@= WITH_DIRECTION point_expression WITH_DISTANCE numeric_expression@>@/
@= with_angle_optional with_axis_optional@>@/
{

  Int_Void_Ptr ivp = set_focus(static_cast<Scanner_Node>(parameter),
                               static_cast<Id_Map_Entry_Node>(@=$2@>),
                               static_cast<Point*>(@=$4@>),
                               static_cast<Point*>(@=$6@>),
                               @=$8@>,
                               @=$9@>,
                               @=$10@>,
                               X_AXIS,
                               Y_AXIS,
                               Z_AXIS);
@q **** (4) Error handling:  |Scan_Parse::set_focus| failed.@> 

@ Error handling:  |Scan_Parse::set_focus| failed.
\initials{LDF 2004.10.28.}

@<Define rules@>=

  if (ivp.i != 0) 
    {
      
      @=$$@> = static_cast<void*>(0); 
      
    } /* |if (ivp.i != 0)| (|set_focus| failed.)  */

@q **** (4) |Scan_Parse::set_focus| succeeded.@> 

@ |Scan_Parse::set_focus| succeeded.
\initials{LDF 2004.10.28.}

@<Define rules@>=

  else /* |ivp.i == 0|  */
    {

        @=$$@> = ivp.v;

    } /* |else| (|ivp.i == 0|.)  */

};

@q **** (4) with_angle_optional   @>

@*3 \�with angle optional>. 

\LOG
\initials{LDF 2004.06.08.}  
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> with_angle_optional@>

@q ***** (5) with_angle_optional --> `Empty'.  @>

@*4  \�with angle optional> $\longrightarrow$ `\.{Empty}'.  

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=with_angle_optional: /* Empty.  */@>@/
{

  @=$$@> = 0;

};

@q ***** (5) with_angle_optional --> WITH_ANGLE numeric_expression.  @>

@*4  \�with angle optional> $\longrightarrow$ `\.{WITH\_ANGLE}'
\�numeric expression>.

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=with_angle_optional: WITH_ANGLE numeric_expression@>@/
{

  @=$$@> = @=$2@>;

};

@q **** (4) with_axis_optional   @>

@*3 \�with axis optional>. 
\LOG
\initials{LDF 2004.06.08.}  
Added this section.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_axis_optional@>

@q ***** (5) with_axis_optional --> EMPTY.  @>

@*4  \�with axis optional> $\longrightarrow$ \.{EMPTY}.  

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=with_axis_optional: /* Empty.  */@>@/
{

  @=$$@> = Z_AXIS;

};

@q ***** (5) with_axis_optional --> WITH_AXIS axis_specifier.  @>

@*4 \�with axis optional> $\longrightarrow$ `\.{WITH\_AXIS}'
\�axis specifier>.

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=with_axis_optional: WITH_AXIS axis_specifier@>@/
{

  @=$$@> = @=$2@>;

};

@q **** (4) axis_specifier   @>

@*3 \�axis specifier>. 

\LOG
\initials{LDF 2004.06.08.}  
Added this section.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> axis_specifier@>

@q ***** (5) axis_specifier --> X_AXIS.@>

@*4  \�with axis optional> $\longrightarrow$ `\.{X\_AXIS}'.  

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=axis_specifier: X_AXIS@>@/
{

  @=$$@> = X_AXIS;

};

@q ***** (5) axis_specifier --> Y_AXIS.  @>

@*4  \�with axis optional> $\longrightarrow$ `\.{Y\_AXIS}'.  

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=axis_specifier: Y_AXIS@>@/
{

  @=$$@> = Y_AXIS;

};

@q ***** (5) axis_specifier --> Z_AXIS.@>

@*4  \�with axis optional> $\longrightarrow$ `\.{Z\_AXIS}'.  

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=axis_specifier: Z_AXIS@>@/
{

  @=$$@> = Z_AXIS;

};

@q ** (2) ellipses.@>  
@*1 {\bf ellipses}.
\initials{LDF 2004.12.19.}

\LOG
\initials{LDF 2004.12.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET ellipse_variable      @>  
@q *** (3) with_center_optional with_axis_h_optional      @>
@q *** (3) with_axis_v_optional with_point_count_optional @>
@q *** (3) with_normal_optional.                          @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�ellipse variable> \�with center optional>  
\�with axis h optional> \�with axis v optional> 
\�with point count optional> \�with normal optional>.

\LOG
\initials{LDF 2004.11.01.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET ellipse_variable@>@/        
@= with_center_optional  with_axis_h_optional@>@/             
@= with_axis_v_optional with_point_count_optional@>@/         
@= with_normal_optional@>@/                           
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

@q **** (4) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
     {
        
        @=$$@> = static_cast<void*>(0);

     }  /* |if (entry == 0)|  */

@q **** (4) |entry != 0|.@>   
@ |entry != 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

    else /* |entry != 0|  */
       {

          Ellipse* e = static_cast<Ellipse*>(entry->object); 

@q ***** (5) |e == 0|.@>   
@ |e == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

          if (e == static_cast<Ellipse*>(0))
             {

                    e = create_new<Ellipse>(0);

             }  /* |if (e == 0)|  */

@q ***** (5).@>               
@
\LOG
\initials{LDF 2004.11.02.}
@:BUG FIX@> BUG FIX:  Now passing |Point origin| as the |center|
argument to |Ellipse::set| and shifting |e| later, 
if |*center != origin|.
\ENDLOG 

@<Define rules@>=

         Point origin(0, 0, 0);
         Point* center              = static_cast<Point*>(@=$3@>); 
         real axis_h                = @=$4@>;
         real axis_v                = @=$5@>; 
         unsigned short point_count = @=$6@>;
         Point* normal    = static_cast<Point*>(@=$7@>); 

         e->set(origin, axis_h, axis_v, 0, 0, 0, point_count); 

         Point y_axis_pt(0, 1, 0);

         normal->unit_vector(true);

@q ***** (5) |*normal != y_axis_pt|.  Rotate |*e|.@>  

@ |normal != y_axis_pt|.  Rotate |*e|.
\initials{LDF 2004.11.02.}
            
@<Define rules@>=

         if (*normal != y_axis_pt)
           {
             Transform t;
             t.align_with_axis(origin, *normal, 'y');
             t.inverse(true);
             *e *= t;
           } /* |if (*normal != y_axis_pt)|  */

@q ***** (5) |*center != origin|.  Shift |*e|.@>  

@ |*center != origin|.  Shift |*e|.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this section.
\ENDLOG
            
@<Define rules@>=

   if (*center != origin)
    e->shift(*center);

@q ***** (5) Delete |center| and |normal|, set |entry->object| and   @> 
@q ***** (5) |command| to |static_cast<void*>(e)|, and exit rule.@> 

@ Delete |center| and |normal|, set |entry->object| and    
|command| to |static_cast<void*>(e)|, and exit rule.
\initials{LDF 2004.11.02.}

@<Define rules@>=

         delete center;
         delete normal;

         @=$$@> = entry->object = static_cast<void*>(e); 

       } /* |else| (|entry != 0|)  */

@q **** (4) @>   

};

@q ** (2) circles.@>  
@*1 {\bf circles}.
\initials{LDF 2004.12.19.}

\LOG
\initials{LDF 2004.12.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET circle_variable with_center_optional @>  
@q *** (3) with_radius_optional with_diameter_optional         @>
@q *** (3) with_point_count_optional with_normal_optional.      @> 

@*2 \�command> $\longrightarrow$ \.{SET} \�circle variable> 
\�with center optional>  \�with radius optional> 
\�with diameter optional> \�with point count optional> 
\�with normal optional>.

\LOG
\initials{LDF 2004.11.01.}
Added this rule.

\initials{LDF 2021.6.30.}
Added \�with radius optional>.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET circle_variable with_center_optional with_radius_optional @>@/
@=with_diameter_optional with_point_count_optional with_normal_optional@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

@q **** (4) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
     {
        @=$$@> = static_cast<void*>(0);

     }  /* |if (entry == 0)|  */

@q **** (4) |entry != 0|.@>   
@ |entry != 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

    else /* |entry != 0|  */
       {

          Circle* c = static_cast<Circle*>(entry->object); 

@q ***** (5) |c == 0|.@>   
@ |c == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

          if (c == static_cast<Circle*>(0))
             {

                    c = create_new<Circle>(0);

             }  /* |if (c == 0)|  */

@q ***** (5).@>               
@
\LOG
\initials{LDF 2004.11.02.}
@:BUG FIX@> BUG FIX:  Now passing |Point origin| as the |center|
argument to |Circle::set| and shifting |c| later, 
if |*center != origin|.
\ENDLOG 
@<Define rules@>=

         Point origin(0, 0, 0);
         Point* center              = static_cast<Point*>(@=$3@>); 
         real diameter = 2.0;              

         if (@=$4@> == 0.0 && @=$5@> > 0.0)
            diameter = @=$5@>;
         else if (@=$4@> > 0.0 && @=$5@> == 0.0)
            diameter = 2 * @=$4@>;
         else if (@=$4@> > 0.0 && @=$5@> > 0.0 && 2 * @=$4@> == @=$5@>)
         { 
             cerr << "WARNING!  In Parser, rule `command: SET circle_variable with_center_optional" 
                  << endl 
                  << "with_radius_optional with_diameter_optional "
                  << endl 
                  << "with_point_count_optional with_normal_optional':"
                  << endl 
                  << "Radius and diameter both specified.  However, diameter == 2 X radius,"
                  << endl 
                  << "so this isn't a problem.  Radius == " << @=$4@> << ", diameter == " << @=$5@>
                  << endl
                  << "Continuing."
                  << endl;
           
             diameter = @=$5@>;
         }
         else if (@=$4@> < 0.0 && @=$5@> == 0.0)
         {
             cerr << "ERROR!  In Parser, rule `command: SET circle_variable with_center_optional" 
                  << endl 
                  << "with_radius_optional with_diameter_optional "
                  << endl 
                  << "with_point_count_optional with_normal_optional':"
                  << endl 
                  << "Diameter is 0 and radius has an invalid value:  " << @=$4@> << " (< 0.0)."
                  << endl 
                  << "Setting diameter to default value (2) and continuing."
                  << endl;

             diameter = 2;
         }
         else if (@=$4@> == 0.0 && @=$5@> < 0.0)
         {
             cerr << "ERROR!  In Parser, rule `command: SET circle_variable with_center_optional" 
                  << endl 
                  << "with_radius_optional with_diameter_optional "
                  << endl 
                  << "with_point_count_optional with_normal_optional':"
                  << endl 
                  << "Radius is 0 and diameter has an invalid value:  " << @=$5@> << " (< 0.0)."
                  << endl 
                  << "Setting diameter to default value (2) and continuing."
                  << endl;

             diameter = 2;
         }

         unsigned short point_count = @=$6@>;
         Point* normal    = static_cast<Point*>(@=$7@>); 

         c->set(origin, diameter, 0, 0, 0, point_count); 

         Point y_axis_pt(0, 1, 0);

         normal->unit_vector(true);

@q ***** (5) |*normal != y_axis_pt|.  Rotate |*c|.@>  

@ |normal != y_axis_pt|.  Rotate |*c|.
\initials{LDF 2004.11.02.}
            
@<Define rules@>=

         if (*normal != y_axis_pt)
           {
             Transform t;
             t.align_with_axis(origin, *normal, 'y');
             t.inverse(true);
             *c *= t;
           } /* |if (*normal != y_axis_pt)|  */

@q ***** (5) |*center != origin|.  Shift |*c|.@>  

@ |*center != origin|.  Shift |*c|.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this section.
\ENDLOG
            
@<Define rules@>=

   if (*center != origin)
    c->shift(*center);

@q ***** (5) Delete |center| and |normal|, set |entry->object| and   @> 
@q ***** (5) |command| to |static_cast<void*>(c)|, and exit rule.@> 

@ Delete |center| and |normal|, set |entry->object| and    
|command| to |static_cast<void*>(c)|, and exit rule.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2022.08.31.}
!! BUG FIX:  Now setting |entry->known_state = Id_Map_Entry_Type::KNOWN|.
\ENDLOG 

@<Define rules@>=

         delete center;
         delete normal;

         @=$$@> = entry->object = static_cast<void*>(c); 

       } /* |else| (|entry != 0|)  */

       entry->known_state = Id_Map_Entry_Type::KNOWN;
       

@q **** (4) @>   

};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.06.09.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET rectangle_variable LEFT_PARENTHESIS point_expression COMMA @>
@= point_expression COMMA point_expression COMMA point_expression @>
@= RIGHT_PARENTHESIS @>@/
{

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET rectangle_variable LEFT_PARENTHESIS"
                << endl 
                << "point_expression COMMA point_expression COMMA"
                << endl 
                << "point_expression COMMA point_expression RIGHT_PARENTHESIS'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Rectangle *r = 0;

   if (entry->object)
     r = static_cast<Rectangle*>(entry->object);
   else
   {
     r = create_new<Rectangle>(0);
     entry->object = static_cast<void*>(r);
   }

   Point *p0 = static_cast<Point*>(@=$4@>);
   Point *p1 = static_cast<Point*>(@=$6@>);
   Point *p2 = static_cast<Point*>(@=$8@>);
   Point *p3 = static_cast<Point*>(@=$10@>);

   r->set(*p0, *p1, *p2, *p3);

   delete p0;
   delete p1;
   delete p2;
   delete p3;   

   p0 = 0;
   p1 = 0;
   p2 = 0;
   p3 = 0;   

   @=$$@> = 0;

};



@q ** (2) parabolae.@>  
@*1 {\bf parabolae}.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this section.
\ENDLOG

@q *** (3) command --> SET parabola_variable @>  
@q *** (3) set_parabola_option_list.             @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�parabola variable> \�set parabola option list>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET parabola_variable set_parabola_option_list@>@/
{

   Scan_Parse::set_parabola_rule_func_0(@=$2@>, 
                                        parameter);

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_parabola_option_list.@>   
@* \�set parabola option list>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_parabola_option_list@>

@q ** (2) set_parabola_option_list --> EMPTY.@>   
@* \�set parabola option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option_list: /* Empty  */@>@/        
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (   scanner_node->parabola_set_option_struct 
       != static_cast<Parabola_Set_Option_Struct*>(0))
      scanner_node->parabola_set_option_struct->clear();

   else
      scanner_node->parabola_set_option_struct = new Parabola_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option_list --> set_parabola_option_list @>   
@q ** (2) set_parabola_option.                                  @>   

@* \�set parabola option list> $\longrightarrow$ 
\�set parabola option list> \�set parabola option>. 
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option_list: set_parabola_option_list set_parabola_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_parabola_option.@>   
@* \�set parabola option>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_parabola_option@>

@q ** (2) set_parabola_option --> WITH_FOCUS point_expression.@>   
@* \�set parabola option> $\longrightarrow$ \.{WITH\_FOCUS}
\�point expression>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_FOCUS point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->focus 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option --> WITH_VERTEX point_expression.@>   
@* \�set parabola option> $\longrightarrow$ \.{WITH\_VERTEX}
\�point expression>.
\initials{LDF 2005.11.08.}

\LOG
\initials{LDF 2005.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_VERTEX point_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->vertex 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option --> WITH_PARAMETER numeric_expression.@>   
@* \�set parabola option> $\longrightarrow$ \.{WITH\_PARAMETER}
\�numeric expression>.
\initials{LDF 2005.11.08.}

\LOG
\initials{LDF 2005.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_PARAMETER numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->parameter 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option -->           @>   
@q ** (2) WITH_HALF_PARAMETER numeric_expression.@>   
@* \�set parabola option> $\longrightarrow$ 
\.{WITH\_HALF\_PARAMETER} \�numeric expression>.
\initials{LDF 2005.11.08.}

\LOG
\initials{LDF 2005.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_HALF_PARAMETER numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->parameter 
      = @=$2@> / 2; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option --> WITH_DIRECTRIX path_expression.@>   

@* \�set parabola option> $\longrightarrow$ \.{WITH\_DIRECTRIX} \�path expression>.
\initials{LDF 2005.11.08.}

\LOG
\initials{LDF 2005.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_DIRECTRIX path_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->directrix 
      = static_cast<Path*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option --> WITH_MAX_EXTENT numeric_expression.@>   
@* \�set parabola option> $\longrightarrow$ \.{WITH\_MAX\_EXTENT}
\�numeric expression>.
\initials{LDF 2005.11.09.}

\LOG
\initials{LDF 2005.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_MAX_EXTENT numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->max_extent 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_parabola_option --> WITH_INCREMENT numeric_expression.@>   
@* \�set parabola option> $\longrightarrow$ \.{WITH\_INCREMENT}
\�numeric expression>.
\initials{LDF 2005.11.09.}

\LOG
\initials{LDF 2005.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_parabola_option: WITH_INCREMENT numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->parabola_set_option_struct->increment_value 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) hyperbolae.@>  
@*1 {\bf hyperbolae}.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this section.
\ENDLOG

@q *** (3) command --> SET hyperbola_variable set_hyperbola_option_list@> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�hyperbola variable> \�set hyperbola option list>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET hyperbola_variable set_hyperbola_option_list@>@/
{

   Scan_Parse::set_hyperbola_rule_func_0(@=$2@>, 
                                        parameter);

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_hyperbola_option_list.@>   
@* \�set hyperbola option list>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_hyperbola_option_list@>

@q ** (2) set_hyperbola_option_list --> EMPTY.@>   
@* \�set hyperbola option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option_list: /* Empty  */@>@/        
{
   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->hyperbola_set_option_struct != static_cast<Hyperbola_Set_Option_Struct*>(0))
      scanner_node->hyperbola_set_option_struct->clear();

   else
      scanner_node->hyperbola_set_option_struct = new Hyperbola_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option_list --> set_hyperbola_option_list @>   
@q ** (2) set_hyperbola_option.                                   @>   

@* \�set hyperbola option list> $\longrightarrow$ 
\�set hyperbola option list> \�set hyperbola option>. 
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option_list: set_hyperbola_option_list set_hyperbola_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_hyperbola_option.@>   
@* \�set hyperbola option>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_hyperbola_option@>

@q ** (2) set_hyperbola_option --> WITH_CENTER point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.11.15.}

\LOG
\initials{LDF 2005.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_CENTER point_expression@>@/        
{
#if 0 
   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->center
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_FOCUS point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_FOCUS}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_FOCUS point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->focus_0
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_FOCUS_0 point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_FOCUS\_0}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_FOCUS_0 point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->focus_0
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_FOCUS_1 point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_FOCUS\_1}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_FOCUS_1 point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->focus_1
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_VERTEX point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_VERTEX}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_VERTEX point_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->vertex_0 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_VERTEX_0 point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_VERTEX\_0}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_VERTEX_0 point_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->vertex_0 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_VERTEX_1 point_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_VERTEX\_1}
\�point expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_VERTEX_1 point_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->vertex_1 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_PARAMETER numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_PARAMETER}
\�numeric expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_PARAMETER numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->parameter 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->           @>   
@q ** (2) WITH_HALF_PARAMETER numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ 
\.{WITH\_HALF\_PARAMETER} \�numeric expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_HALF_PARAMETER numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->parameter 
      = @=$2@> / 2; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->  WITH_DIRECTRIX path_expression.@>   

@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_DIRECTRIX} \�path expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_DIRECTRIX path_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->directrix 
      = static_cast<Path*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_MAX_EXTENT numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_MAX\_EXTENT}
\�numeric expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_MAX_EXTENT numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->max_extent 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option --> WITH_INCREMENT numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ \.{WITH\_INCREMENT}
\�numeric expression>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_INCREMENT numeric_expression@>@/
{

   static_cast<Scanner_Node>(parameter)->hyperbola_set_option_struct->increment_value 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->           @>   
@q ** (2) WITH_MAJOR_AXIS_LENGTH numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ 
\.{WITH\_MAJOR\_AXIS\_LENGTH} \�numeric expression>.
\initials{LDF 2005.11.15.}

\LOG
\initials{LDF 2005.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_MAJOR_AXIS_LENGTH numeric_expression@>@/
{

   static_cast<Scanner_Node>(
      parameter)->hyperbola_set_option_struct->major_axis_length 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->           @>   
@q ** (2) WITH_HALF_MAJOR_AXIS_LENGTH numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ 
\.{WITH\_HALF\_MAJOR\_AXIS\_LENGTH} \�numeric expression>.
\initials{LDF 2005.11.15.}

\LOG
\initials{LDF 2005.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_HALF_MAJOR_AXIS_LENGTH numeric_expression@>@/
{

   static_cast<Scanner_Node>(
       parameter)->hyperbola_set_option_struct->major_axis_length 
      = @=$2@> / 2; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->           @>   
@q ** (2) WITH_MINOR_AXIS_LENGTH numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ 
\.{WITH\_MINOR\_AXIS\_LENGTH} \�numeric expression>.
\initials{LDF 2005.11.15.}

\LOG
\initials{LDF 2005.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_MINOR_AXIS_LENGTH numeric_expression@>@/
{

   static_cast<Scanner_Node>(
      parameter)->hyperbola_set_option_struct->minor_axis_length 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_hyperbola_option -->           @>   
@q ** (2) WITH_HALF_MINOR_AXIS_LENGTH numeric_expression.@>   
@* \�set hyperbola option> $\longrightarrow$ 
\.{WITH\_HALF\_MINOR\_AXIS\_LENGTH} \�numeric expression>.
\initials{LDF 2005.11.15.}

\LOG
\initials{LDF 2005.11.15.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_hyperbola_option: WITH_HALF_MINOR_AXIS_LENGTH numeric_expression@>@/
{

   static_cast<Scanner_Node>(
      parameter)->hyperbola_set_option_struct->minor_axis_length 
      = @=$2@> / 2; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) helices.@>  
@*1 {\bf helices}.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET helix_variable @>  
@q *** (3) set_helix_option_list.             @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�helix variable> \�set helix option list>.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this rule.

\initials{LDF 2005.05.20.}
Now passing |scanner_node->helix_set_option_struct| to
|Scan_Parse::set_helix_rule_func_0|.

\initials{LDF 2005.05.21.}
No longer passing |scanner_node->helix_set_option_struct| to
|Scan_Parse::set_helix_rule_func_0|.  It's unnecessary, since it's already accessible via
|void* parameter|.

\initials{LDF 2005.05.21.}
@:BUG FIX@> BUG FIX:  Now setting |scanner_node->helix_set_option_struct| to 0 
before exiting rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET helix_variable set_helix_option_list@>@/
{

   set_helix_rule_func_0(@=$2@>, 
                         parameter);

   static_cast<Scanner_Node>(parameter)->helix_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_helix_option_list.@>   
@* \�set helix option list>.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_helix_option_list@>

@q ** (2) set_helix_option_list --> EMPTY.@>   
@* \�set helix option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option_list: /* Empty  */@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter); 

   if (scanner_node->helix_set_option_struct != static_cast<Helix_Set_Option_Struct*>(0))
      scanner_node->helix_set_option_struct->clear();

   else
      scanner_node->helix_set_option_struct = new Helix_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option_list --> set_helix_option_list @>   
@q ** (2) set_helix_option.                               @>   

@* \�set helix option list> $\longrightarrow$ 
\�set helix option list> \�set helix option>. 
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option_list: set_helix_option_list set_helix_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_helix_option.@>   
@* \�set helix option>.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_helix_option@>

@q ** (2) set_helix_option --> WITH_CENTER point_expression.@>   
@* \�set helix option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.05.19.}

\LOG
\initials{LDF 2005.05.19.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_CENTER point_expression@>@/        
{

#if 0 
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_DIRECTION point_expression.@>   
@* \�set helix option> $\longrightarrow$ \.{WITH\_DIRECTION}
\�point expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_DIRECTION point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->direction 
       = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_DIRECTION_VECTOR point_vector_expression.@>   
@* \�set helix option> $\longrightarrow$ \.{WITH\_DIRECTION\_VECTOR}
\�point vector expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_DIRECTION_VECTOR point_vector_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->direction_vector 
      = static_cast<Pointer_Vector<Point>*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_START_DIAMETER numeric_expression.@>

@* \�set helix option> $\longrightarrow$ \.{WITH\_START\_DIAMETER}
\�numeric expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_START_DIAMETER numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->start_diameter 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q START HERE!!  Commenting out unnecessary debugging code.  LDF 2005.10.26.@>

@q ** (2) set_helix_option --> WITH_POINTS_PER_CYCLE numeric_expression.@>

@* \�set helix option> $\longrightarrow$ \.{WITH\_POINTS\_PER\_CYCLE}
\�numeric expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_POINTS_PER_CYCLE numeric_expression@>@/        
{
   
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->points_per_cycle 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_CYCLES numeric_expression.@>

@* \�set helix option> $\longrightarrow$ \.{WITH\_CYCLES}
\�numeric expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_CYCLES numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->cycles = fabs(@=$2@>);

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_ANGLE numeric_expression.@>

@* \�set helix option> $\longrightarrow$ \.{WITH\_ANGLE}
\�numeric expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_ANGLE numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->angle 
      = fmod(fabs(@=$2@>), 360);

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_helix_option --> WITH_TYPE helix_type_specifier.@>

@* \�set helix option> $\longrightarrow$ \.{WITH\_TYPE}
\�helix type specifier>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_helix_option: WITH_TYPE helix_type_specifier@>@/        
{
   
   static_cast<Scanner_Node>(parameter)->helix_set_option_struct->type = @=$2@>;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) helix_type_specifier.@>   
@*1 \�helix type specifier>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> helix_type_specifier@>

@q *** (3) helix_type_specifier --> NULL_TYPE.@>

@*2 \�helix type specifier> $\longrightarrow$ \.{NULL\_TYPE}.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=helix_type_specifier: NULL_TYPE@>@/        
{
 
   @=$$@> = Helix::HELIX_NULL_TYPE@>;
  
};

@q *** (3) helix_type_specifier --> ARCHIMEDEAN.@>

@*2 \�helix type specifier> $\longrightarrow$ \.{ARCHIMEDEAN}.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=helix_type_specifier: ARCHIMEDEAN@>@/        
{
   
   @=$$@> = Helix::HELIX_ARCHIMEDEAN_TYPE@>;

};

@q *** (3) helix_type_specifier --> LOGARITHMIC.@>

@*2 \�helix type specifier> $\longrightarrow$ \.{LOGARITHMIC}.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=helix_type_specifier: LOGARITHMIC@>@/        
{
   
   @=$$@> = Helix::HELIX_LOGARITHMIC_TYPE@>;

};

@q *** (3) helix_type_specifier --> PARABOLIC.@>

@*2 \�helix type specifier> $\longrightarrow$ \.{PARABOLIC}.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=helix_type_specifier: PARABOLIC@>@/        
{
   
   @=$$@> = Helix::HELIX_PARABOLIC_TYPE@>;

};

@q *** (3) helix_type_specifier --> HYPERBOLIC.@>

@*2 \�helix type specifier> $\longrightarrow$ \.{HYPERBOLIC}.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=helix_type_specifier: HYPERBOLIC@>@/        
{
   
   @=$$@> = Helix::HELIX_HYPERBOLIC_TYPE@>;

};

@q ** (2) triangles.@>  
@*1 {\bf triangles}.
\initials{LDF 2005.01.20.}

\LOG
\initials{LDF 2005.01.20.}
Added this section.
\ENDLOG

@q *** (3) command --> SET triangle_variable WITH_POINTS@>        
@q *** (3) LEFT_PARENTHESIS point_expression COMMA point_expression   @>
@q *** (3) COMMA point_expression RIGHT_PARENTHESIS                     @>

@*2 \�command> $\longrightarrow$ \.{SET} 
\�triangle variable> \.{WITH\_POINTS} \.{LEFT\_PARENTHESIS} \�point expression> \.{COMMA} 
\�point expression> \.{COMMA} \�point expression> 
\.{RIGHT\_PARENTHESIS}.
\initials{LDF 2005.01.23.}

\LOG
\initials{LDF 2005.01.23.}
Started working on this rule.
\ENDLOG 

@q **** (4) Definition.@>

@<Define rules@>= 
@=command: SET triangle_variable WITH_POINTS@>@/        
@=LEFT_PARENTHESIS point_expression COMMA point_expression@>@/
@=COMMA point_expression RIGHT_PARENTHESIS@>@/        
{

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET triangle_variable WITH_POINTS"
                << endl 
                << "LEFT_PARENTHESIS point_expression COMMA point_expression"
                << endl 
                << "COMMA point_expression RIGHT_PARENTHESIS'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

@q ***** (5) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
     {
        
        @=$$@> = static_cast<void*>(0);

     }  /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   
@ |entry != 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

    else /* |entry != 0|  */
    {

       Triangle* t = static_cast<Triangle*>(entry->object); 

@q ****** (6) |t == 0|.@>   
@ |t == 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

       if (t == static_cast<Triangle*>(0))
       {

          t = create_new<Triangle>(0);

          entry->object = static_cast<void*>(t); 

       }  /* |if (t == 0)|  */

@q ****** (6).@> 

       Point *p0 = static_cast<Point*>(@=$5@>);
       Point *p1 = static_cast<Point*>(@=$7@>);
       Point *p2 = static_cast<Point*>(@=$9@>);

#if 0 
       p0->show("*p0:");
       p1->show("*p1:");
       p2->show("*p2:");
#endif

       status = t->set(p0, p1, p2, scanner_node);

@q ******* (7) @>

       if (status != 0)
       {
          cerr << "ERROR!  In parser, rule `command: SET triangle_variable WITH_POINTS"
               << endl 
               << "LEFT_PARENTHESIS point_expression COMMA point_expression"
               << endl 
               << "COMMA point_expression RIGHT_PARENTHESIS':"
               << endl
               << "`Triangle::set' failed, returning " << status << "."
               << endl 
               << "Failed to set triangle.  Will try to continue." << endl;
       }

@q ******* (7) @>
   
#if DEBUG_COMPILE
       else if (DEBUG)
       { 
          cerr << "In parser, rule `command: SET triangle_variable WITH_POINTS"
               << endl 
               << "LEFT_PARENTHESIS point_expression COMMA point_expression"
               << endl 
               << "COMMA point_expression RIGHT_PARENTHESIS':"
               << endl
               << "`Triangle::set' succeeded, returning 0." << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@;        

@q ******* (7) @>

@q ****** (6).@> 

   }  /* |else| (|entry != 0|)  */

@q ***** (5).@> 

};

@q *** (3) command --> SET triangle_variable WITH_POINTS@>        
@q *** (3) point_expression COMMA point_expression      @>
@q *** (3) COMMA point_expression                       @>

@*2 \�command> $\longrightarrow$ \.{SET} 
\�triangle variable> \.{WITH\_POINTS} \�point expression> \.{COMMA} 
\�point expression> \.{COMMA} \�point expression>.
\initials{LDF 2024.11.08.}

\LOG
\initials{LDF 2024.11.08.}
Started working on this rule.
\ENDLOG 

@q **** (4) Definition.@>

@<Define rules@>= 
@=command: SET triangle_variable WITH_POINTS@>@/
@=point_expression COMMA point_expression@>@/
@=COMMA point_expression@>@/        
{

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET triangle_variable WITH_POINTS"
                << endl 
                << "point_expression COMMA point_expression"
                << endl 
                << "COMMA point_expression'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

@q ***** (5) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
     {
        
        @=$$@> = static_cast<void*>(0);

     }  /* |if (entry == 0)|  */

@q ***** (5) |entry != 0|.@>   
@ |entry != 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

    else /* |entry != 0|  */
    {

       Triangle* t = static_cast<Triangle*>(entry->object); 

@q ****** (6) |t == 0|.@>   
@ |t == 0|.
\initials{LDF 2005.01.23.}

@<Define rules@>=

       if (t == static_cast<Triangle*>(0))
       {

          t = create_new<Triangle>(0);

          entry->object = static_cast<void*>(t); 

       }  /* |if (t == 0)|  */

@q ****** (6).@> 

       Point *p0 = static_cast<Point*>(@=$4@>);
       Point *p1 = static_cast<Point*>(@=$6@>);
       Point *p2 = static_cast<Point*>(@=$8@>);

#if 0 
       p0->show("*p0:");
       p1->show("*p1:");
       p2->show("*p2:");
#endif

       status = t->set(p0, p1, p2, scanner_node);

@q ******* (7) @>

       if (status != 0)
       {
          cerr << "ERROR!  In parser, rule `command: SET triangle_variable WITH_POINTS"
               << endl 
               << "point_expression COMMA point_expression"
               << endl 
               << "COMMA point_expression':"
               << endl
               << "`Triangle::set' failed, returning " << status << "."
               << endl 
               << "Failed to set triangle.  Will try to continue." << endl;
       }

@q ******* (7) @>
   
#if DEBUG_COMPILE
       else if (DEBUG)
       { 
          cerr << "In parser, rule `command: SET triangle_variable WITH_POINTS"
               << endl 
               << "point_expression COMMA point_expression"
               << endl 
               << "COMMA point_expression':"
               << endl
               << "`Triangle::set' succeeded, returning 0." << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@;        

@q ******* (7) @>

@q ****** (6).@> 

   }  /* |else| (|entry != 0|)  */

@q ***** (5).@> 

};

@q *** (3) command --> SET triangle_variable numeric_list numeric_vector_variable @>        
@q *** (3) triangle_solution_type                                                 @>

@*2 \�command> $\longrightarrow$ \.{SET} \�triangle variable> \�numeric list> 
\�numeric vector variable> \�triangle solution type>. 
\initials{LDF 2024.11.03.}

\LOG
\initials{LDF 2024.11.03.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>

@<Define rules@>= 
@=command: SET triangle_variable numeric_list numeric_vector_variable@>        
@=triangle_solution_type@>
{
@q ***** (5) @>

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET triangle_variable numeric_list "
                << "numeric_vector_variable triangle_solution_type'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;
 
@q ***** (5) @>
@
@<Define rules@>= 

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Id_Map_Entry_Node pv_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

    Triangle *T = 0;
    Pointer_Vector<real> *pv = 0;

    if (entry && !entry->object)
    {
       T = create_new<Triangle>(0);
       entry->object = static_cast<void*>(T);
    }

    if (pv_entry)
    {
       if (!pv_entry->object)
       {
          pv = new Pointer_Vector<real>;
          pv_entry->object = static_cast<void*>(pv);
       }
       else
       {
          pv = static_cast<Pointer_Vector<real>*>(pv_entry->object);
          pv->clear();
       }
    }

@q ***** (5) @>
@
@<Define rules@>= 

    if (entry && entry->object && pv_entry && pv)
    {
       T = static_cast<Triangle*>(entry->object);

       pv = static_cast<Pointer_Vector<real>*>(pv_entry->object);

       status = Triangle::set(T, static_cast<Pointer_Vector<real>*>(@=$3@>), pv, @=$5@>, scanner_node);

       if (status != 0)
        {
            cerr << "ERROR!  In parser, rule `command: SET triangle_variable numeric_list"
                 << endl 
                 << "numeric_vector_variable triangle_solution_type':"
                 << endl
                 << "`Triangle::set' failed, returning " << status << "."
                 << endl
                 << "Failed to set triangle." << endl 
                 << "Will try to continue." << endl;
        }   
    }

@q ***** (5) @>
@
@<Define rules@>= 

    else
    {
        cerr << "ERROR!  In parser, rule `command: SET triangle_variable numeric_list"
             << endl 
             << "numeric_vector_variable triangle_solution_type':"
             << endl;

        if (!(entry && entry->object))     
           cerr << "`triangle_variable' is NULL.  ";

        if (!(pv_entry && pv_entry->object))     
           cerr << "`numeric_vector_variable' is NULL.  ";
 
        cerr << "Can't set triangle." 
             << endl
             << "Will try to continue." << endl;
    }

    @=$$@> = 0;

@q ***** (5) @>

};

@q *** (3) command --> SET triangle_variable COMMA numeric_vector_expression  @>        
@q *** (3) COMMA numeric_vector_variable triangle_solution_type               @>

@*2 \�command> $\longrightarrow$ \.{SET} 
\�triangle variable> \.{COMMA} \�numeric vector expression> \.{COMMA} \�numeric vector variable> 
\�triangle solution type>. 
\initials{LDF 2024.11.03.}

\LOG
\initials{LDF 2024.11.03.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>

@<Define rules@>= 
@=command: SET triangle_variable COMMA numeric_vector_expression COMMA numeric_vector_variable @>        
@=triangle_solution_type@>
{
@q ***** (5) @>

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET triangle_variable COMMA numeric_vector_expression "
                << "COMMA numeric_vector_variable triangle_solution_type'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;
 
@q ***** (5) @>
@
@<Define rules@>= 

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Id_Map_Entry_Node pv_entry = static_cast<Id_Map_Entry_Node>(@=$6@>);

    Triangle *T = 0;
    Pointer_Vector<real> *pv = 0;

    if (entry && !entry->object)
    {
       T = create_new<Triangle>(0);
       entry->object = static_cast<void*>(T);
    }

    if (pv_entry)
    {
       if (!pv_entry->object)
       {
          pv = new Pointer_Vector<real>;
          pv_entry->object = static_cast<void*>(pv);
       }
       else
       {
          pv = static_cast<Pointer_Vector<real>*>(pv_entry->object);
          pv->clear();
       }
    }

@q ***** (5) @>
@
@<Define rules@>= 

    if (entry && entry->object && pv_entry && pv)
    {
       T = static_cast<Triangle*>(entry->object);

#if 0 
       /* This shouldn't be needed.  \initials{LDF 2024.11.13.}  */
       pv = static_cast<Pointer_Vector<real>*>(pv_entry->object);
#endif 
       status = Triangle::set(T, static_cast<Pointer_Vector<real>*>(@=$4@>), pv, @=$7@>, scanner_node);

       if (status != 0)
        {
            cerr << "ERROR!  In parser, rule `command: SET triangle_variable "
                 << "COMMA numeric_vector_expression COMMA"
                 << endl 
                 << "numeric_vector_variable triangle_solution_type':"
                 << endl
                 << "`Triangle::set' failed, returning " << status << "."
                 << endl
                 << "Failed to set triangle." << endl 
                 << "Will try to continue." << endl;
        }   
    }

@q ***** (5) @>
@
@<Define rules@>= 

    else
    {
        cerr << "ERROR!  In parser, rule `command: SET triangle_variable "
             << endl 
             << "COMMA numeric_vector_expression COMMA numeric_vector_variable "
             << "triangle_solution_type':"
             << endl;

        if (!(entry && entry->object))     
           cerr << "`triangle_variable' is NULL.  ";

        if (!(pv_entry && pv_entry->object))     
           cerr << "`numeric_vector_variable' is NULL.  ";
 
        cerr << "Can't set triangle." 
             << endl
             << "Will try to continue." << endl;
    }

    @=$$@> = 0;

@q ***** (5) @>

};

@q *** (3) command --> SET triangle_variable COMMA numeric_vector_expression @>        
@q *** (3) triangle_solution_type                                            @>

@*2 \�command> $\longrightarrow$ \.{SET} 
\�triangle variable> \.{COMMA} \�numeric vector expression> \�triangle solution type>. 
\initials{LDF 2024.11.13.}

\LOG
\initials{LDF 2024.11.13.}
Added this rule.
\ENDLOG 

@q **** (4) Definition.@>

@<Define rules@>= 
@=command: SET triangle_variable COMMA numeric_vector_expression triangle_solution_type@>
{
@q ***** (5) @>

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET triangle_variable COMMA numeric_vector_expression "
                << "triangle_solution_type'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;
 
@q ***** (5) @>
@
@<Define rules@>= 

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Triangle *T = 0;

    if (entry && !entry->object)
    {
       T = create_new<Triangle>(0);
       entry->object = static_cast<void*>(T);
    }

@q ***** (5) @>
@
@<Define rules@>= 

    if (entry && entry->object)
    {
       T = static_cast<Triangle*>(entry->object);

       status = Triangle::set(T, static_cast<Pointer_Vector<real>*>(@=$4@>), 0, @=$5@>, scanner_node);

       if (status != 0)
        {
            cerr << "ERROR!  In parser, rule `command: SET triangle_variable "
                 << "COMMA numeric_vector_expression triangle_solution_type':"
                 << endl
                 << "`Triangle::set' failed, returning " << status << "."
                 << endl
                 << "Failed to set triangle." << endl 
                 << "Will try to continue." << endl;
        }   
    }

@q ***** (5) @>
@
@<Define rules@>= 

    else
    {
        cerr << "ERROR!  In parser, rule `command: SET triangle_variable "
             << endl 
             << "COMMA numeric_vector_expression triangle_solution_type':"
             << endl;

        if (!(entry && entry->object))     
           cerr << "`triangle_variable' is NULL.  ";

        if (@=$4@> == 0)
           cerr << "`numeric_vector_expression' is NULL.  ";

        cerr << "Can't set triangle." 
             << endl
             << "Will try to continue." << endl;
    }

    @=$$@> = 0;

@q ***** (5) @>

};




@q ** (2) reg_polygons.@>  

@*1 {\bf reg\_polygons}.

\LOG
\initials{LDF 2004.12.19.}
Added this section.
\ENDLOG

@q *** (3) command --> SET reg_polygon_variable            @>  
@q *** (3) WITH_SIDES numeric_expression with_center_optional  @>
@q *** (3) with_diameter_optional with_normal_optional.        @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�regular polygon variable> \.{WITH\_SIDES} \�numeric expression>
\�with center optional>  \�with diameter optional> 
\�with normal optional>.

\LOG
\initials{LDF 2004.11.01.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET reg_polygon_variable@>@/        
@= WITH_SIDES numeric_expression with_center_optional@>@/             
@= with_diameter_optional with_normal_optional@>@/                           
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

@q **** (4) Error handling:  |entry == 0|.@>   

@ Error handling:  |entry == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
     {
        @=$$@> = static_cast<void*>(0);

     }  /* |if (entry == 0)|  */

@q **** (4) |entry != 0|.@>   
@ |entry != 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

    else /* |entry != 0|  */
       {

          Reg_Polygon* r = static_cast<Reg_Polygon*>(entry->object); 

@q ***** (5) |r == 0|.@>   
@ |r == 0|.
\initials{LDF 2004.11.01.}

@<Define rules@>=

          if (r == static_cast<Reg_Polygon*>(0))
             {

                    r = create_new<Reg_Polygon>(0);

             }  /* |if (r == 0)|  */

@q ***** (5).@>               
@
\LOG
\initials{LDF 2004.11.02.}
@:BUG FIX@> BUG FIX:  Now passing |Point origin| as the |center|
argument to |Reg_Polygon::set| and shifting |r| later, 
if |*center != origin|.
\ENDLOG 
@<Define rules@>=

         Point origin(0, 0, 0);
         real sides    = @=$4@>;
         Point* center = static_cast<Point*>(@=$5@>); 
         real diameter = @=$6@>;
         Point* normal = static_cast<Point*>(@=$7@>); 

@q ****** (6) Error handling:  |sides < 0|.@> 
@ Error handling:  |sides < 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

         if (sides < 0)
           {

               sides *= -1;

           }  /* |if (sides < 0)|  */

@q ****** (6) Error handling:  |sides == 0|.@> 
@ Error handling:  |sides == 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

         else if (sides == 0)
            {
                 sides = 5;

            } /* |else if (sides == 0)|  */

@q ****** (6).@> 

         sides = floor(sides + .5);

         unsigned short u = static_cast<unsigned short>(sides); 

         r->set(origin, u, diameter); 

         Point y_axis_pt(0, 1, 0);

         normal->unit_vector(true);

@q ***** (5) |*normal != y_axis_pt|.  Rotate |*r|.@>  

@ |normal != y_axis_pt|.  Rotate |*r|.
\initials{LDF 2004.11.02.}
            
@<Define rules@>=

         if (*normal != y_axis_pt)
           {
             Transform t;
             t.align_with_axis(origin, *normal, 'y');
             t.inverse(true);
             *r *= t;
           } /* |if (*normal != y_axis_pt)|  */

@q ***** (5) |*center != origin|.  Shift |*r|.@>  

@ |*center != origin|.  Shift |*r|.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this section.
\ENDLOG
            
@<Define rules@>=

   if (*center != origin)
    r->shift(*center);

@q ***** (5) Delete |center| and |normal|, set |entry->object| and   @> 
@q ***** (5) |command| to |static_cast<void*>(r)|, and exit rule.@> 

@ Delete |center| and |normal|, set |entry->object| and    
|command| to |static_cast<void*>(r)|, and exit rule.
\initials{LDF 2004.11.02.}

@<Define rules@>=

         delete center;
         delete normal;

         @=$$@> = entry->object = static_cast<void*>(r); 

       } /* |else| (|entry != 0|)  */

@q **** (4) @>   

};

@q * (1) with_center_optional.@> 
@* \�with center optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> with_center_optional@>

@q ** (2) with_center_optional --> EMPTY.@> 
@*1 \�with center optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_center_optional: /* Empty.  */@>@/
{

    Point* p;

    p = create_new<Point>(0);

    p->set(0, 0, 0);

    @=$$@> = static_cast<void*>(p); 
 
};

@q ** (2) with_center_optional --> \.{WITH\_CENTER} point_expression.@> 
@*1 \�with center optional> $\longrightarrow$ \�point expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_center_optional: WITH_CENTER point_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) with_radius_optional.@> 
@* \�with radius optional>.
\initials{LDF 2021.6.30.}

\LOG
\initials{LDF 2021.6.30.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <real_value> with_radius_optional@>

@q ** (2) with_radius_optional --> EMPTY.@> 
@*1 \�with radius optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2021.6.30.}

\LOG
\initials{LDF 2021.6.30.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_radius_optional: /* Empty.  */@>@/
{
     @=$$@> = 0.0;
};

@q ** (2) with_radius_optional --> WITH_RADIUS numeric_expression.@> 
@*1 \�with radius optional> $\longrightarrow$ 
\.{WITH\_RADIUS}
\�numeric expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_radius_optional: WITH_RADIUS numeric_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) with_diameter_optional.@> 
@* \�with diameter optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <real_value> with_diameter_optional@>

@q ** (2) with_diameter_optional --> EMPTY.@> 
@*1 \�with diameter optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_diameter_optional: /* Empty.  */@>@/
{

     @=$$@> = 0.0;
 
};

@q ** (2) with_diameter_optional --> WITH_DIAMETER numeric_expression.@> 
@*1 \�with diameter optional> $\longrightarrow$ 
\.{WITH\_DIAMETER}
\�numeric expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_diameter_optional: WITH_DIAMETER numeric_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) with_axis_h_optional.@> 
@* \�with axis h optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <real_value> with_axis_h_optional@>

@q ** (2) with_axis_h_optional --> EMPTY.@> 
@*1 \�with axis h optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_axis_h_optional: /* Empty.  */@>@/
{

     @=$$@> = 1.0;
 
};

@q ** (2) with_axis_h_optional --> WITH_AXIS_H numeric_expression.@> 
@*1 \�with axis h optional> $\longrightarrow$ 
\.{WITH\_AXIS\_H}
\�numeric expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_axis_h_optional: WITH_AXIS_H numeric_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) with_axis_v_optional.@> 
@* \�with axis v optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <real_value> with_axis_v_optional@>

@q ** (2) with_axis_v_optional --> EMPTY.@> 
@*1 \�with axis v optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_axis_v_optional: /* Empty.  */@>@/
{

     @=$$@> = 1.0;
 
};

@q ** (2) with_axis_v_optional --> WITH_AXIS_V numeric_expression.@> 
@*1 \�with axis v optional> $\longrightarrow$ 
\.{WITH\_AXIS\_V} \�numeric expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_axis_v_optional: WITH_AXIS_V numeric_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q * (1) with_point_count_optional.@> 
@* \�with point count optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> with_point_count_optional@>

@q ** (2) with_point_count_optional --> EMPTY.@> 
@*1 \�with point count optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_point_count_optional: /* Empty.  */@>@/
{

     @=$$@> = 32;
 
};

@q ** (2) with_point_count_optional --> WITH_POINT_COUNT numeric_expression.@> 
@*1 \�with point count optional> $\longrightarrow$ 
\.{WITH\_POINT\_COUNT} \�numeric expression>.

\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=with_point_count_optional: WITH_POINT_COUNT numeric_expression@>@/
{

   real r = @=$2@>;

@q **** (4) Error handling:  |r < 0|.@>   

@ Error handling:  |r < 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

   if (r == ZERO_REAL)
      {

        @=$$@> = 32;

      } /* |if (r < 0)|  */

@q **** (4) |r != 0|.@>   

@ |r != 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

   else /* |r != 0|  */
     {

@q ***** (5) Error handling:  |r < 0|.@> 

@ Error handling:  |r < 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

        if (r < 0)
           {

              r = -r;

           }  /* |if (r < 0)|  */

         r = floor(r + .5); 
        
         unsigned short u = static_cast<unsigned short>(r);

@q ***** (5) Error handling:  |u % 4 != 0|.@> 
@ Error handling:  |u % 4 != 0|.
\initials{LDF 2004.11.02.}

@<Define rules@>=

         unsigned short v = u % 4;

         if (v != 0)
           {

               u += (4 - v);

           } /* |if (u % 4 != 0)|  */

@q ***** (5).@> 

        @=$$@> = u;

    } /* |else| (|r != 0|)  */
 
};

@q * (1) with_normal_optional.@> 
@* \�with normal optional>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> with_normal_optional@>

@q ** (2) with_normal_optional --> EMPTY.@> 
@*1 \�with normal optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_normal_optional: /* Empty.  */@>@/
{
      Point* p;

      p = create_new<Point>(0);

      p->set(0, 1, 0);

     @=$$@> = static_cast<void*>(p); 
};

@q ** (2) with_normal_optional --> WITH_NORMAL point_expression.@> 
@*1 \�with normal optional> $\longrightarrow$ 
\.{WITH\_NORMAL} \�point expression>.
\initials{LDF 2004.11.02.}

\LOG
\initials{LDF 2004.11.02.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=with_normal_optional: WITH_NORMAL point_expression@>@/
{

   @=$$@> = @=$2@>;
 
};

@q ** (2) Cones.@>  
@*1 Cones.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this section.
\ENDLOG

@q *** (3) command --> SET cone_variable @>  
@q *** (3) set_cone_option_list.             @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�cone variable> \�set cone option list>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.

\initials{LDF 2005.05.25.}
Commented-in the call to |Scan_Parse::set_cone_rule_func_0|.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET cone_variable set_cone_option_list@>@/
{

   set_cone_rule_func_0(@=$2@>, 
                        parameter);

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_cone_option_list.@>   
@* \�set cone option list>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_cone_option_list@>

@q ** (2) set_cone_option_list --> EMPTY.@>   
@* \�set cone option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option_list: /* Empty  */@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->cone_set_option_struct != static_cast<Cone_Set_Option_Struct*>(0))
      scanner_node->cone_set_option_struct->clear();

   else
      scanner_node->cone_set_option_struct = new Cone_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option_list --> set_cone_option_list @>   
@q ** (2) set_cone_option.                               @>   

@* \�set cone option list> $\longrightarrow$ 
\�set cone option list> \�set cone option>. 
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option_list: set_cone_option_list set_cone_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_cone_option.@>   
@* \�set cone option>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_cone_option@>

@q ** (2) set_cone_option --> WITH_CENTER point_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_CENTER point_expression@>@/        
{

#if 0 
   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_DIRECTION point_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_DIRECTION}
\�point expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_DIRECTION point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->direction 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_RADIUS numeric_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_RADIUS} \�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_RADIUS numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cone_set_option_struct->radius == static_cast<real*>(0))
       scanner_node->cone_set_option_struct->radius = new real(@=$2@>); 
    else 
       *scanner_node->cone_set_option_struct->radius = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_AXIS_X numeric_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_AXIS\_X}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_AXIS_X numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cone_set_option_struct->axis_x == static_cast<real*>(0))
       scanner_node->cone_set_option_struct->axis_x = new real(@=$2@>); 
    else 
       *scanner_node->cone_set_option_struct->axis_x = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_AXIS_Y numeric_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_AXIS\_Y}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_AXIS_Y numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cone_set_option_struct->axis_y == static_cast<real*>(0))
       scanner_node->cone_set_option_struct->axis_y = new real(@=$2@>); 
    else 
       *scanner_node->cone_set_option_struct->axis_y = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_AXIS_Z numeric_expression.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_AXIS\_Z}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_AXIS_Z numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cone_set_option_struct->axis_z == static_cast<real*>(0))
       scanner_node->cone_set_option_struct->axis_z = new real(@=$2@>); 
    else 
       *scanner_node->cone_set_option_struct->axis_z = @=$2@>; 
    
   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_cone_option --> WITH_POINT_COUNT numeric_expression.@>   
@*2 \�set cone option> $\longrightarrow$ \.{WITH\_POINT\_COUNT}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_POINT_COUNT numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->base_point_count
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_cone_option --> WITH_DIVISIONS numeric_expression.@>   
@*2 \�set cone option> $\longrightarrow$ \.{WITH\_DIVISIONS}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_DIVISIONS numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->divisions
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cone_option --> WITH_TYPE cone_type.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_TYPE}
\�cone type>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_TYPE cone_type@>@/        
{

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->type 
      = @=$2@>;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) cone_type.@> 
@* \�cone type>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> cone_type@>

@q ** (2) cone_type: ELLIPTICAL.@> 
@*1 \�cone type> $\longrightarrow$ \.{ELLIPTICAL}.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cone_type: ELLIPTICAL@>@/
{

   @=$$@> = Shape::ELLIPTICAL_CONE_TYPE;

};

@q ** (2) cone_type: CIRCULAR.@> 
@*1 \�cone type> $\longrightarrow$ \.{CIRCULAR}.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cone_type: CIRCULAR@>@/
{

   @=$$@> = Shape::CIRCULAR_CONE_TYPE;

};

@q ** (2) cone_type: PARABOLIC.@> 
@*1 \�cone type> $\longrightarrow$ \.{PARABOLIC}.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cone_type: PARABOLIC@>@/
{

   @=$$@> = Shape::PARABOLIC_CONE_TYPE;

};

@q ** (2) cone_type: HYPERBOLIC.@> 
@*1 \�cone type> $\longrightarrow$ \.{HYPERBOLIC}.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cone_type: HYPERBOLIC@>@/
{

   @=$$@> = Shape::HYPERBOLIC_CONE_TYPE;

};

@q ** (2) set_cone_option --> WITH_TYPE nap_type.@>   
@* \�set cone option> $\longrightarrow$ \.{WITH\_TYPE}
\�nap type>.
\initials{LDF 2006.11.09.}

\LOG
\initials{LDF 2006.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cone_option: WITH_TYPE nap_type@>@/        
{

   static_cast<Scanner_Node>(parameter)->cone_set_option_struct->nap_type 
      = @=$2@>;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) nap_type.@> 
@* \�nap type>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.09.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> nap_type@>

@q ** (2) nap_type: SINGLE_NAPPED.@> 
@*1 \�nap type> $\longrightarrow$ \.{SINGLE\_NAPPED}.
\initials{LDF 2006.11.09.}

\LOG
\initials{LDF 2006.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=nap_type: SINGLE_NAPPED@>@/
{

   @=$$@> = Cone::SINGLE_NAPPED_TYPE;

};

@q ** (2) nap_type: DOUBLE_NAPPED.@> 
@*1 \�nap type> $\longrightarrow$ \.{DOUBLE\_NAPPED}.
\initials{LDF 2006.11.09.}

\LOG
\initials{LDF 2006.11.09.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=nap_type: DOUBLE_NAPPED@>@/
{

   @=$$@> = Cone::DOUBLE_NAPPED_TYPE;

};

@q ** (2) Prismatoids.@>  
@*1 Prismatoids.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this section.
\ENDLOG

@q *** (3) command --> SET prismatoid_variable set_prismatoid_option_list @>

@*2 \�command> $\longrightarrow$ \.{SET} \�prismatoid variable> \�set prismatoid option list>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET prismatoid_variable set_prismatoid_option_list@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET prismatoid_variable set_prismatoid_option_list'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    if (!entry)
    {
        cerr_strm << thread_name 
                  << "ERROR!   In parser, rule `command: SET prismatoid_variable "
                  << "set_prismatoid_option_list':"
                  << endl 
                  << "`Id_Map_Entry_Node entry' and/or `Point *p' is NULL." << endl 
                  << "Can't set `prismatoid'.  Will try to continue.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");

       goto END_SET_PRISMATOID_RULE;
    }

     set_prismatoid_rule_func_0(entry, 
                                scanner_node);

END_SET_PRISMATOID_RULE:

    if (scanner_node->prismatoid_set_options_struct)
    {
       delete scanner_node->prismatoid_set_options_struct;
       scanner_node->prismatoid_set_options_struct = 0;
    }

    @=$$@> = static_cast<void*>(0);

};

@q * (1) set_prismatoid_option_list.@>   
@* \�with base center optional>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.30.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_prismatoid_option_list@>

@q ** (2) set_prismatoid_option_list --> EMPTY.@>   
@* \�with base center optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_prismatoid_option_list: /* Empty  */@>@/        
{
   @<Common declarations for rules@>@;

   if (scanner_node->prismatoid_set_options_struct)
   {
       delete scanner_node->prismatoid_set_options_struct;
       scanner_node->prismatoid_set_options_struct = 0;
   }

   scanner_node->prismatoid_set_options_struct = new Prismatoid_Set_Options_Struct;

   @=$$@> = 0;

};

@q ** (2) set_prismatoid_option_list --> set_prismatoid_option_list WITH_TYPE prismatoid_type.@>   
@* \�set prismatoid option list> $\longrightarrow$ \�set prismatoid option list> 
\.{WITH\_TYPE} \�prismatoid type>.
\initials{LDF 2024.05.31.}

\LOG
\initials{LDF 2024.05.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_prismatoid_option_list: set_prismatoid_option_list WITH_TYPE prismatoid_type@>@/        
{
   @<Common declarations for rules@>@;
  
   scanner_node->prismatoid_set_options_struct->prismatoid_type = @=$3@>;
 
   @=$$@> = 0;

};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> prismatoid_type@>

@
@<Define rules@>= 
@=prismatoid_type: PRISM_TYPE@>
{
   @=$$@> = Prismatoid::PRISMATOID_PRISM_TYPE;
};

@
@<Define rules@>= 
@=prismatoid_type: PYRAMID_TYPE@>
{
   @=$$@> = Prismatoid::PRISMATOID_PYRAMID_TYPE;
};

@
@<Define rules@>= 
@=prismatoid_type: OBELISK_TYPE@>
{
   @=$$@> = Prismatoid::PRISMATOID_OBELISK_TYPE;
};

@
@<Define rules@>= 
@=prismatoid_type: SPINDLE_TYPE@>
{
   @=$$@> = Prismatoid::PRISMATOID_SPINDLE_TYPE;
};

@q ** (2) set_prismatoid_option_list --> set_prismatoid_option_list WITH_BASE_CENTER point_expression.@>   
@* \�set prismatoid option list> $\longrightarrow$ \�set prismatoid option list> 
\.{WITH\_BASE\_CENTER} \�point expression>.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_prismatoid_option_list: set_prismatoid_option_list WITH_BASE_CENTER point_expression@>@/        
{
   @<Common declarations for rules@>@;
  
   scanner_node->prismatoid_set_options_struct->base_center = static_cast<Point*>(@=$3@>);
 
   @=$$@> = 0;

};

@q ** (2) set_prismatoid_option_list --> set_prismatoid_option_list WITH_APEX point_expression.@>   
@* \�set prismatoid option list> $\longrightarrow$ \�set prismatoid option list> 
\.{WITH\_APEX} \�point expression>.
\initials{LDF 2024.05.31.}

\LOG
\initials{LDF 2024.05.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_prismatoid_option_list: set_prismatoid_option_list WITH_APEX point_expression@>@/        
{
   @<Common declarations for rules@>@;
  
   scanner_node->prismatoid_set_options_struct->apex0 = static_cast<Point*>(@=$3@>);
 
   @=$$@> = 0;

};

@q ** (2) set_prismatoid_option_list --> set_prismatoid_option_list WITH_APEX_ONE point_expression.@>   
@* \�set prismatoid option list> $\longrightarrow$ \�set prismatoid option list> 
\.{WITH\_APEX\_ONE} \�point expression>.
\initials{LDF 2024.05.31.}

\LOG
\initials{LDF 2024.05.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_prismatoid_option_list: set_prismatoid_option_list WITH_APEX_ONE point_expression@>@/        
{
   @<Common declarations for rules@>@;
  
   scanner_node->prismatoid_set_options_struct->apex1 = static_cast<Point*>(@=$3@>);
 
   @=$$@> = 0;

};




@*2 \�command> $\longrightarrow$ \.{SET\_PYRAMID} \�prismatoid variable> \�set prismatoid option list>.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET_PYRAMID prismatoid_variable set_prismatoid_option_list@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_PYRAMID prismatoid_variable set_prismatoid_option_list'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

END_SET_PYRAMID_RULE:

    @=$$@> = 0;

};

@*2 \�command> $\longrightarrow$ \.{SET\_PRISM} \�prismatoid variable> \�set prismatoid option list>.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET_PRISM prismatoid_variable set_prismatoid_option_list@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_PRISM prismatoid_variable set_prismatoid_option_list'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

END_SET_PRISM_RULE:

    @=$$@> = 0;

};

@*2 \�command> $\longrightarrow$ \.{SET\_OBELISK} \�prismatoid variable> \�set prismatoid option list>.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET_OBELISK prismatoid_variable set_prismatoid_option_list@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_OBELISK prismatoid_variable set_prismatoid_option_list'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

END_SET_OBELISK_RULE:

    @=$$@> = 0;

};

@*2 \�command> $\longrightarrow$ \.{SET\_SPINDLE} \�prismatoid variable> \�set prismatoid option list>.
\initials{LDF 2024.05.30.}

\LOG
\initials{LDF 2024.05.30.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET_SPINDLE prismatoid_variable set_prismatoid_option_list@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_SPINDLE prismatoid_variable set_prismatoid_option_list'."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

END_SET_SPINDLE_RULE:

    @=$$@> = 0;

};

@q ** (2) cylinders.@>  
@*1 {\bf cylinders}.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this section.
\ENDLOG

@q *** (3) command --> SET cylinder_variable @>  
@q *** (3) set_cylinder_option_list.             @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�cylinder variable> \�set cylinder option list>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.

\initials{LDF 2005.05.25.}
Commented-in the call to |Scan_Parse::set_cylinder_rule_func_0|.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET cylinder_variable set_cylinder_option_list@>@/
{

   set_cylinder_rule_func_0(@=$2@>, 
                            parameter);

   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_cylinder_option_list.@>   
@* \�set cylinder option list>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_cylinder_option_list@>

@q ** (2) set_cylinder_option_list --> EMPTY.@>   
@* \�set cylinder option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option_list: /* Empty  */@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->cylinder_set_option_struct != static_cast<Cylinder_Set_Option_Struct*>(0))
      scanner_node->cylinder_set_option_struct->clear();

   else
      scanner_node->cylinder_set_option_struct = new Cylinder_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option_list --> set_cylinder_option_list @>   
@q ** (2) set_cylinder_option.                                  @>   

@* \�set cylinder option list> $\longrightarrow$ 
\�set cylinder option list> \�set cylinder option>. 
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option_list: set_cylinder_option_list set_cylinder_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_cylinder_option.@>   
@* \�set cylinder option>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_cylinder_option@>

@q ** (2) set_cylinder_option --> WITH_CENTER point_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.05.25.}

\LOG
\initials{LDF 2005.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_CENTER point_expression@>@/        
{

#if 0 
   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_DIRECTION point_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_DIRECTION}
\�point expression>.
\initials{LDF 2005.05.21.}

\LOG
\initials{LDF 2005.05.21.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_DIRECTION point_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct->direction 
      = static_cast<Point*>(@=$2@>); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_RADIUS numeric_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_RADIUS}
\�numeric expression>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_RADIUS numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cylinder_set_option_struct->radius == static_cast<real*>(0))
       scanner_node->cylinder_set_option_struct->radius = new real(@=$2@>); 
    else 
       *scanner_node->cylinder_set_option_struct->radius = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_AXIS_X numeric_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_AXIS\_X}
\�numeric expression>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_AXIS_X numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cylinder_set_option_struct->axis_x == static_cast<real*>(0))
       scanner_node->cylinder_set_option_struct->axis_x = new real(@=$2@>); 
    else 
       *scanner_node->cylinder_set_option_struct->axis_x = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_AXIS_Y numeric_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_AXIS\_Y}
\�numeric expression>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_AXIS_Y numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cylinder_set_option_struct->axis_y == static_cast<real*>(0))
       scanner_node->cylinder_set_option_struct->axis_y = new real(@=$2@>); 
    else 
       *scanner_node->cylinder_set_option_struct->axis_y = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_AXIS_Z numeric_expression.@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_AXIS\_Z}
\�numeric expression>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_AXIS_Z numeric_expression@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);
 
    if (scanner_node->cylinder_set_option_struct->axis_z == static_cast<real*>(0))
       scanner_node->cylinder_set_option_struct->axis_z = new real(@=$2@>); 
    else 
       *scanner_node->cylinder_set_option_struct->axis_z = @=$2@>; 
    
   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_cylinder_option --> WITH_POINT_COUNT numeric_expression.@>   
@*2 \�set cylinder option> $\longrightarrow$ \.{WITH\_POINT\_COUNT}
\�numeric expression>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_POINT_COUNT numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct->base_point_count
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_cylinder_option --> WITH_DIVISIONS numeric_expression.@>   
@*2 \�set cylinder option> $\longrightarrow$ \.{WITH\_DIVISIONS}
\�numeric expression>.
\initials{LDF 2006.11.08.}

\LOG
\initials{LDF 2006.11.08.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_DIVISIONS numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct->divisions
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_cylinder_option --> WITH_TYPE .@>   
@* \�set cylinder option> $\longrightarrow$ \.{WITH\_TYPE}
\�cylinder type>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_cylinder_option: WITH_TYPE cylinder_type@>@/        
{

   static_cast<Scanner_Node>(parameter)->cylinder_set_option_struct->type 
      = @=$2@>;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) cylinder_type.@> 
@* \�cylinder type>.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> cylinder_type@>

@q ** (2) cylinder_type: ELLIPTICAL.@> 
@*1 \�cylinder type> $\longrightarrow$ \.{ELLIPTICAL}.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cylinder_type: ELLIPTICAL@>@/
{

   @=$$@> = Shape::ELLIPTICAL_CYLINDER_TYPE;

};

@q ** (2) cylinder_type: CIRCULAR.@> 
@*1 \�cylinder type> $\longrightarrow$ \.{CIRCULAR}.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cylinder_type: CIRCULAR@>@/
{

   @=$$@> = Shape::CIRCULAR_CYLINDER_TYPE;

};

@q ** (2) cylinder_type: PARABOLIC.@> 
@*1 \�cylinder type> $\longrightarrow$ \.{PARABOLIC}.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cylinder_type: PARABOLIC@>@/
{

   @=$$@> = Shape::PARABOLIC_CYLINDER_TYPE;

};

@q ** (2) cylinder_type: HYPERBOLIC.@> 
@*1 \�cylinder type> $\longrightarrow$ \.{HYPERBOLIC}.
\initials{LDF 2006.11.06.}

\LOG
\initials{LDF 2006.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=cylinder_type: HYPERBOLIC@>@/
{

   @=$$@> = Shape::HYPERBOLIC_CYLINDER_TYPE;

};

@q ** (2) ellipsoids.@>  
@*1 {\bf ellipsoids}.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this section.
\ENDLOG

@q *** (3) command --> SET ellipsoid_variable @>  
@q *** (3) set_ellipsoid_option_list.         @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�ellipsoid variable> \�set ellipsoid option list>.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET ellipsoid_variable set_ellipsoid_option_list@>@/
{

   Scan_Parse::set_ellipsoid_rule_func_0(@=$2@>, 
                                         parameter);

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_ellipsoid_option_list.@>   
@* \�set ellipsoid option list>.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_ellipsoid_option_list@>

@q ** (2) set_ellipsoid_option_list --> EMPTY.@>   
@* \�set ellipsoid option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this rule.

\initials{LDF 2005.10.31.}
Commented-in the code in this rule.  Removed debugging code.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option_list: /* Empty  */@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->ellipsoid_set_option_struct != static_cast<Ellipsoid_Set_Option_Struct*>(0))
      scanner_node->ellipsoid_set_option_struct->clear();

   else
      scanner_node->ellipsoid_set_option_struct = new Ellipsoid_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_ellipsoid_option_list --> set_ellipsoid_option_list @>   
@q ** (2) set_ellipsoid_option.                                   @>   

@* \�set ellipsoid option list> $\longrightarrow$ 
\�set ellipsoid option list> \�set ellipsoid option>. 
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option_list: set_ellipsoid_option_list set_ellipsoid_option@>@/        
{
   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_ellipsoid_option.@>   
@* \�set ellipsoid option>.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_ellipsoid_option@>

@q ** (2) set_ellipsoid_option --> WITH_CENTER point_expression.@>   
@* \�set ellipsoid option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.05.26.}

\LOG
\initials{LDF 2005.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_CENTER point_expression@>@/        
{

#if 0 
   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) Axes.@> 
@*1 Axes.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this section.
\ENDLOG

@q *** (3) set_ellipsoid_option --> WITH_AXIS_X point_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_X}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_AXIS_X numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->axis_x 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_ellipsoid_option --> WITH_AXIS_Y numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_Y}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_AXIS_Y numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->axis_y 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_ellipsoid_option --> WITH_AXIS_Z numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_Z}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_AXIS_Z numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->axis_z 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) Divisions.@> 
@*1 Divisions.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this section.
\ENDLOG

@q *** (3) set_ellipsoid_option --> WITH_DIVISIONS_X numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_X}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_DIVISIONS_X numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->divisions_x 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_ellipsoid_option --> WITH_DIVISIONS_Y numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_Y}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_DIVISIONS_Y numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->divisions_y 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_ellipsoid_option --> WITH_DIVISIONS_Z numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_Z}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_DIVISIONS_Z numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->divisions_z 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_ellipsoid_option --> WITH_POINT_COUNT numeric_expression.@>   
@*2 \�set ellipsoid option> $\longrightarrow$ \.{WITH\_POINT\_COUNT}
\�numeric expression>.
\initials{LDF 2005.10.31.}

\LOG
\initials{LDF 2005.10.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_ellipsoid_option: WITH_POINT_COUNT numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->ellipsoid_set_option_struct->ellipse_point_count
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};
  
@q ** (2) superellipsoids.@>  
@*1 {\bf superellipsoids}.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this section.
\ENDLOG

@q *** (3) command --> SET superellipsoid_variable @>  
@q *** (3) set_superellipsoid_option_list.         @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�superellipsoid variable> \�set superellipsoid option list>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET superellipsoid_variable set_superellipsoid_option_list@>@/
{


   Scan_Parse::set_superellipsoid_rule_func_0(@=$2@>, 
                                              parameter);


   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_superellipsoid_option_list.@>   
@* \�set superellipsoid option list>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_superellipsoid_option_list@>

@q ** (2) set_superellipsoid_option_list --> EMPTY.@>   
@* \�set superellipsoid option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option_list: /* Empty  */@>@/        
{

   Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

   if (scanner_node->superellipsoid_set_option_struct != static_cast<Superellipsoid_Set_Option_Struct*>(0))
      scanner_node->superellipsoid_set_option_struct->clear();

   else
      scanner_node->superellipsoid_set_option_struct = new Superellipsoid_Set_Option_Struct;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_superellipsoid_option_list --> set_superellipsoid_option_list @>   
@q ** (2) set_superellipsoid_option.                                        @>   

@* \�set superellipsoid option list> $\longrightarrow$ 
\�set superellipsoid option list> \�set superellipsoid option>. 
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option_list: set_superellipsoid_option_list set_superellipsoid_option@>@/        
{
   @=$$@> = static_cast<void*>(0);

};

@q * (1) set_superellipsoid_option.@>   
@* \�set superellipsoid option>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_superellipsoid_option@>

@q ** (2) set_superellipsoid_option --> WITH_CENTER point_expression.@>   
@* \�set superellipsoid option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_CENTER point_expression@>@/        
{

#if 0 
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) Axes.@> 
@*1 Axes.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this section.
\ENDLOG

@q *** (3) set_superellipsoid_option --> WITH_AXIS_X point_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_X}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_AXIS_X numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->axis_x 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_superellipsoid_option --> WITH_AXIS_Y numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_Y}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_AXIS_Y numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->axis_y 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_superellipsoid_option --> WITH_AXIS_Z numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_AXIS\_Z}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_AXIS_Z numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->axis_z 
      = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) Divisions.@> 
@*1 Divisions.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this section.
\ENDLOG

@q *** (3) set_superellipsoid_option --> WITH_DIVISIONS_X numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_X}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_DIVISIONS_X numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->divisions_x 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_superellipsoid_option --> WITH_DIVISIONS_Y numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_Y}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_DIVISIONS_Y numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->divisions_y 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_superellipsoid_option --> WITH_DIVISIONS_Z numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_DIVISIONS\_Z}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_DIVISIONS_Z numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->divisions_z 
      = static_cast<unsigned short>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_superellipsoid_option --> WITH_POINT_COUNT numeric_expression.@>   
@*2 \�set superellipsoid option> $\longrightarrow$ \.{WITH\_POINT\_COUNT}
\�numeric expression>.
\initials{LDF 2022.05.26.}

\LOG
\initials{LDF 2022.05.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_superellipsoid_option: WITH_POINT_COUNT numeric_expression@>@/        
{
   static_cast<Scanner_Node>(parameter)->superellipsoid_set_option_struct->superellipse_point_count
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};
  
@q ** (2) spheres.@>  
@*1 {\bf spheres}.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this section.
\ENDLOG

@q *** (3) command --> SET sphere_variable set_sphere_option_list.@> 

@*2 \�command> $\longrightarrow$ \.{SET} \�sphere variable> 
\�set sphere option list>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET sphere_variable set_sphere_option_list@>@/
{

   Scan_Parse::set_sphere_rule_func_0(@=$2@>, 
                                      parameter);

   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_sphere_option_list.@>   
@*1 \�set sphere option list>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_sphere_option_list@>

@q *** (3) set_sphere_option_list --> EMPTY.@>   
@*2 \�set sphere option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.

\initials{LDF 2005.10.19.}
Now compiling the code that checks whether |scanner_node->sphere_set_option_struct|
exists or not and proceeds accordingly.
\ENDLOG

@<Define rules@>=
@=set_sphere_option_list: /* Empty  */@>@/        
{
    Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

    if (scanner_node->sphere_set_option_struct != static_cast<Sphere_Set_Option_Struct*>(0))
       scanner_node->sphere_set_option_struct->clear();

    else
       scanner_node->sphere_set_option_struct = new Sphere_Set_Option_Struct;

    @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_sphere_option_list --> set_sphere_option_list @>   
@q ** (2) set_sphere_option.                                @>   

@* \�set sphere option list> $\longrightarrow$ 
\�set sphere option list> \�set sphere option>. 
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_sphere_option_list: set_sphere_option_list set_sphere_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) set_sphere_option.@>   
@*1 \�set sphere option>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_sphere_option@>

@q *** (3) set_sphere_option --> WITH_CENTER point_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_CENTER}
\�point expression>.
\initials{LDF 2005.06.06.}

\LOG
\initials{LDF 2005.06.06.}
Added this rule.

\initials{LDF 2005.10.19.}
Now compiling the code that sets 
|scanner_node->sphere_set_option_struct->center| to 
|static_cast<Point*>(@=$2@>)|.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_CENTER point_expression@>@/        
{
#if 0 
   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->center 
      = static_cast<Point*>(@=$2@>); 
#endif 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_DIVISIONS numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_DIVISIONS}
\�numeric expression>.
\initials{LDF 2005.10.19.}

\LOG
\initials{LDF 2005.10.19.}
Added this rule.

\initials{LDF 2005.10.20.}
Now rounding |@=$2@>| before setting 
|scanner_node->sphere_set_option_struct->divisions| to it.

\initials{LDF 2005.10.25.}
Now setting |scanner_node->sphere_set_option_struct->divisions_vertical|
and
|scanner_node->sphere_set_option_struct->divisions_horizontal|,
instead of |scanner_node->sphere_set_option_struct->divisions|.
I've removed |unsigned int Sphere_Set_Option_Struct::divisions|.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_DIVISIONS numeric_expression@>@/        
{
    Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

    scanner_node->sphere_set_option_struct->divisions_vertical
       = scanner_node->sphere_set_option_struct->divisions_horizontal
       = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_DIVISIONS_VERTICAL numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_DIVISIONS\_VERTICAL}
\�numeric expression>.
\initials{LDF 2005.10.19.}

\LOG
\initials{LDF 2005.10.19.}
Added this rule.

\initials{LDF 2005.10.20.}
Now rounding |@=$2@>| before setting 
|scanner_node->sphere_set_option_struct->divisions_vertical| to it.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_DIVISIONS_VERTICAL numeric_expression@>@/        
{

    static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->divisions_vertical
       = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_DIVISIONS_HORIZONTAL numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_DIVISIONS\_HORIZONTAL}
\�numeric expression>.
\initials{LDF 2005.10.19.}

\LOG
\initials{LDF 2005.10.19.}
Added this rule.

\initials{LDF 2005.10.20.}
Now rounding |@=$2@>| before setting 
|scanner_node->sphere_set_option_struct->divisions_vertical| to it.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_DIVISIONS_HORIZONTAL numeric_expression@>@/        
{

    static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->divisions_horizontal
       = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

    @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_RADIUS numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_RADIUS}
\�numeric expression>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_RADIUS numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->radius = @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_DIAMETER numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_DIAMETER}
\�numeric expression>.
\initials{LDF 2005.10.20.}

\LOG
\initials{LDF 2005.10.20.}
Added this rule.

\initials{LDF 2005.10.28.}
Now setting |scanner_node->sphere_set_option_struct->radius|
instead of 
|scanner_node->sphere_set_option_struct->diameter|.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_DIAMETER numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->radius = .5 * @=$2@>; 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_POINT_COUNT numeric_expression.@>   
@*2 \�set sphere option> $\longrightarrow$ \.{WITH\_POINT\_COUNT}
\�numeric expression>.
\initials{LDF 2005.10.20.}

\LOG
\initials{LDF 2005.10.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_POINT_COUNT numeric_expression@>@/        
{

   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->circle_point_count
      = static_cast<unsigned int>(floor(fabs(@=$2@>) + .5)); 

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_sphere_option --> WITH_TYPE sphere_type@>   
@*2 \�set sphere option> $\longrightarrow$ \�sphere type>.
\initials{LDF 2005.10.25.}

\LOG
\initials{LDF 2005.10.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_sphere_option: WITH_TYPE sphere_type@>@/        
{
   static_cast<Scanner_Node>(parameter)->sphere_set_option_struct->sphere_type 
      = @=$2@>;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) sphere_type.@>  
@*4 \�sphere type>.
\initials{LDF 2005.10.25.}

\LOG
\initials{LDF 2005.10.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> sphere_type@>  

@q **** (4) sphere_type --> SPHERE_GLOBE_TYPE_YY.@> 
@*5 \�sphere type> $\longrightarrow$ \.{SPHERE\_GLOBE\_TYPE\_YY}.
\initials{LDF 2005.10.25.}

\LOG
\initials{LDF 2005.10.25.}
Added this rule.
\ENDLOG
 
@<Define rules@>=
@=sphere_type: SPHERE_GLOBE_TYPE_YY@>@/        
{

   @=$$@> = Sphere::GLOBE_TYPE;

};

@q **** (4) sphere_type --> SPHERE_PANEL_TYPE_YY.@> 
@*5 \�sphere type> $\longrightarrow$ \.{SPHERE\_PANEL\_TYPE\_YY}.
\initials{LDF 2005.10.25.}

\LOG
\initials{LDF 2005.10.25.}
Added this rule.
\ENDLOG
 
@<Define rules@>=
@=sphere_type: SPHERE_PANEL_TYPE_YY@>@/        
{

   @=$$@> = Sphere::PANEL_TYPE;

};


@q ** (2) paraboloids.@>  
@*1 {\bf paraboloids}.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this section.
\ENDLOG

@q *** (3) command --> SET paraboloid_variable @>  
@q *** (3) set_paraboloid_option_list.             @> 

@*2 \�command> $\longrightarrow$ \.{SET} 
\�paraboloid variable> \�set paraboloid option list>.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.

\initials{LDF 2006.01.25.}
Working on this rule.
\ENDLOG 

@q *** (3) Definition.@>

@<Define rules@>= 
@=command: SET paraboloid_variable set_paraboloid_option_list@>@/
{
   Scan_Parse::set_paraboloid_rule_func_0(@=$2@>, 
                                          parameter);
   static_cast<Scanner_Node>(parameter)->paraboloid_set_option_struct = 0;

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_paraboloid_option_list.@>   
@*2 \�set paraboloid option list>.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_paraboloid_option_list@>

@q **** (4) set_paraboloid_option_list --> EMPTY.@>   
@*3 \�set paraboloid option list> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_paraboloid_option_list: /* Empty  */@>@/        
{

    Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

    if (   scanner_node->paraboloid_set_option_struct 
        != static_cast<Paraboloid_Set_Option_Struct*>(0))
       scanner_node->paraboloid_set_option_struct->clear();

    else
       scanner_node->paraboloid_set_option_struct = new Paraboloid_Set_Option_Struct;

    @=$$@> = static_cast<void*>(0);

};

@q **** (4) set_paraboloid_option_list --> set_paraboloid_option_list @>   
@q **** (4) set_paraboloid_option.                                    @>   

@*3 \�set paraboloid option list> $\longrightarrow$ 
\�set paraboloid option list> \�set paraboloid option>. 
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_paraboloid_option_list: set_paraboloid_option_list set_paraboloid_option@>@/        
{

   @=$$@> = static_cast<void*>(0);

};

@q *** (3) set_paraboloid_option.@>   
@*2 \�set paraboloid option>.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> set_paraboloid_option@>

@q **** (4) set_paraboloid_option --> WITH_TYPE paraboloid_type/@>   
@*3 \�set paraboloid option> $\longrightarrow$ \.{WITH\_TYPE} \�paraboloid type>.
\initials{LDF 2006.01.25.}

\LOG
\initials{LDF 2006.01.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_paraboloid_option: WITH_TYPE paraboloid_type@>@/        
{

    static_cast<Scanner_Node>(parameter)->paraboloid_set_option_struct->type = @=$2@>;

};

@q *** (3) paraboloid_type.@> 
@*2 \�paraboloid type>.
\initials{LDF 2006.01.25.}

\LOG
\initials{LDF 2006.01.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <int_value> paraboloid_type@>

@q **** (4) paraboloid_type --> ELLIPTICAL.@> 
@*3 \�paraboloid type> $\longrightarrow$ \.{ELLIPTICAL}.
\initials{LDF 2006.01.25.}

@<Define rules@>=
@=paraboloid_type: ELLIPTICAL@>@/        
{

   @=$$@> = Paraboloid::ELLIPTICAL_TYPE;

};

@q **** (4) paraboloid_type --> HYPERBOLIC.@> 
@*3 \�paraboloid type> $\longrightarrow$ \.{HYPERBOLIC}.
\initials{LDF 2006.01.25.}

@<Define rules@>=
@=paraboloid_type: HYPERBOLIC@>@/        
{

   @=$$@> = Paraboloid::HYPERBOLIC_TYPE;

};

@q **** (4) set_paraboloid_option --> WITH_MAX_EXTENT numeric_expression/@>   
@*3 \�set paraboloid option> $\longrightarrow$ \.{WITH\_MAX\_EXTENT} 
\�numeric expression>.
\initials{LDF 2006.01.25.}

\LOG
\initials{LDF 2006.01.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_paraboloid_option: WITH_MAX_EXTENT numeric_expression@>@/        
{

    static_cast<Scanner_Node>(parameter)->paraboloid_set_option_struct->max_extent = @=$2@>;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET transform_variable numeric_list@>@/
{
  @<Common declarations for rules@>@;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);
  Pointer_Vector<real> *real_vector = static_cast<Pointer_Vector<real>*>(@=$3@>);

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET transform numeric_list'."
                << endl 
                << "entry->name == " << entry->name;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      for (vector<real*>::iterator iter = real_vector->v.begin();
           iter != real_vector->v.end();
           ++iter)
      {
          cerr << "**iter == " << **iter << endl;
      } 
    }
#endif /* |DEBUG_COMPILE|  */@;

  Transform *t = 0;

  if (entry && !entry->object)
  {
     t = create_new<Transform>(0);
     entry->object = static_cast<void*>(t);

#if 0 
     cerr << "After allocation:" << endl;
     cerr << "(entry->object != 0) == " << (entry->object != 0) << endl;
#endif 

  }

  if (entry && entry->object && real_vector)
  {
     t = static_cast<Transform*>(entry->object);

     status = t->set(real_vector, scanner_node);
 
     if (status != 0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, rule `command: SET transform numeric_list':"
                   << endl 
                   << "`Transform::set' failed, returning " << status << "."
                   << endl 
                   << "Will try to continue.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }

#if DEBUG_COMPILE
     else if (DEBUG)
     { 
          cerr_strm << thread_name 
                    << "*** Parser: `command: SET transform numeric_list'."
                    << endl 
                    << "`Transform::set' returned 0.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
     } 
#endif /* |DEBUG_COMPILE|  */@; 

  } /* |if|  */
    
  delete real_vector;
  real_vector = 0;

  @=$$@> = 0;

};

@q **** (4) command: SET_CENTER ellipse_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�ellipse variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.11.27.}

\LOG
\initials{LDF 2022.11.27.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER ellipse_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER ellipse_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Ellipse *e = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Ellipse>(0));
   }

   if (entry && entry->object && p)
   {
       e = static_cast<Ellipse*>(entry->object);
       e->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER ellipse_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Ellipse*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_CENTER hyperbola_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�hyperbola variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.12.13.}

\LOG
\initials{LDF 2022.12.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER hyperbola_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER hyperbola_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Hyperbola *h = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Hyperbola>(0));
   }

   if (entry && entry->object && p)
   {
       h = static_cast<Hyperbola*>(entry->object);
       h->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER hyperbola_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Hyperbola*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_BRANCH hyperbola_variable COMMA numeric_expression COMMA path_expression@>

@ \�command> $\longrightarrow$ \.{SET\_BRANCH} \�hyperbola variable> \.{COMMA} \�numeric expression> 
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.13.}

\LOG
\initials{LDF 2022.12.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_BRANCH hyperbola_variable COMMA numeric_expression COMMA path_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_BRANCH hyperbola_variable COMMA numeric_expression COMMA path_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Hyperbola *h = 0;

   Path *p = static_cast<Path*>(@=$6@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   unsigned int index = static_cast<unsigned int>(floor(@=$4@>));

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Hyperbola>(0));
   }

   if (entry && entry->object && p)
   {
       h = static_cast<Hyperbola*>(entry->object);
 
       if (index == 0)
           *(static_cast<Path*>(h)) = *p;
       else
           h->branch_1 = *p;

       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_BRANCH hyperbola_variable COMMA numeric_expression "
                 << "COMMA path_expression':" 
                 << endl 
                 << "`entry', `entry->object' (Hyperbola*) and/or `Path *p' are NULL.  Can't set branch " << index << "."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};



@q **** (4) command: SET_FOCUS ellipse_variable COMMA numeric_expression COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_FOCUS\_0} \�ellipse variable> \.{COMMA} \�numeric expression>
\.{COMMA} \�point expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_FOCUS ellipse_variable COMMA numeric_expression COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_FOCUS ellipse_variable COMMA numeric_expression COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Ellipse *e = 0;

   Point *p = static_cast<Point*>(@=$6@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   unsigned int index = static_cast<unsigned int>(floor(@=$4@>));

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Ellipse>(0));
   }

   if (entry && entry->object && p)
   {
       e = static_cast<Ellipse*>(entry->object);

 
       if (index == 0)
          e->focus_0 = *p;
       else 
          e->focus_1 = *p;


       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_FOCUS ellipse_variable COMMA numeric_expression COMMA point_expression':"
                 << "`entry', `entry->object' (Ellipse*) and/or `Point *p' are NULL.  Can't set focus 0."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;
};

@q **** (4) command: SET_AXIS_H ellipse_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_AXIS\_H} \�ellipse_variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_AXIS_H ellipse_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_AXIS_H ellipse_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Ellipse *e = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     e = static_cast<Ellipse*>(entry->object);

  if (entry && !entry->object)
  {
     e = create_new<Ellipse>(0);
     entry->object = static_cast<void*>(e);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_AXIS_H ellipse_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `e.axis_h' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         e->axis_h = 1.0;

     }
     else
     {
        e->axis_h = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`e->axis_h' == " << e->axis_h 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_AXIS_V ellipse_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_AXIS\_V} \�ellipse_variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_AXIS_V ellipse_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_AXIS_V ellipse_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Ellipse *e = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     e = static_cast<Ellipse*>(entry->object);

  if (entry && !entry->object)
  {
     e = create_new<Ellipse>(0);
     entry->object = static_cast<void*>(e);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_AXIS_V ellipse_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `e.axis_v' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         e->axis_v = 1.0;

     }
     else
     {
        e->axis_v = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`e->axis_v' == " << e->axis_v 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_LINEAR_ECCENTRICITY ellipse_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_LINEAR\_ECCENTRICITY} \�ellipse_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_LINEAR_ECCENTRICITY ellipse_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_LINEAR_ECCENTRICITY ellipse_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Ellipse *e = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     e = static_cast<Ellipse*>(entry->object);

  if (entry && !entry->object)
  {
     e = create_new<Ellipse>(0);
     entry->object = static_cast<void*>(e);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_LINEAR_ECCENTRICITY ellipse_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `e.linear_eccentricity' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         e->linear_eccentricity = 1.0;

     }
     else
     {
        e->linear_eccentricity = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`e->linear_eccentricity' == " << e->linear_eccentricity 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_NUMERICAL_ECCENTRICITY ellipse_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_NUMERICAL\_ECCENTRICITY} \�ellipse_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.06.}

\LOG
\initials{LDF 2022.11.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_NUMERICAL_ECCENTRICITY ellipse_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_NUMERICAL_ECCENTRICITY ellipse_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Ellipse *e = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     e = static_cast<Ellipse*>(entry->object);

  if (entry && !entry->object)
  {
     e = create_new<Ellipse>(0);
     entry->object = static_cast<void*>(e);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_NUMERICAL_ECCENTRICITY ellipse_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `e.numerical_eccentricity' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         e->numerical_eccentricity = 1.0;

     }
     else
     {
        e->numerical_eccentricity = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`e->numerical_eccentricity' == " << e->numerical_eccentricity 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */

  @=$$@> = 0;

};

@q **** (4) command: SET_LINEAR_ECCENTRICITY hyperbola_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_LINEAR\_ECCENTRICITY} \�hyperbola_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_LINEAR_ECCENTRICITY hyperbola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_LINEAR_ECCENTRICITY hyperbola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Hyperbola *h = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     h = static_cast<Hyperbola*>(entry->object);

  if (entry && !entry->object)
  {
     h = create_new<Hyperbola>(0);
     entry->object = static_cast<void*>(h);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_LINEAR_ECCENTRICITY hyperbola_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `h.linear_eccentricity' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         h->linear_eccentricity = 1.0;

     }
     else
     {
        h->linear_eccentricity = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`h->linear_eccentricity' == " << h->linear_eccentricity 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_NUMERICAL_ECCENTRICITY hyperbola_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_NUMERICAL\_ECCENTRICITY} \�hyperbola_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_NUMERICAL_ECCENTRICITY hyperbola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_NUMERICAL_ECCENTRICITY hyperbola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Hyperbola *h = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     h = static_cast<Hyperbola*>(entry->object);

  if (entry && !entry->object)
  {
     h = create_new<Hyperbola>(0);
     entry->object = static_cast<void*>(h);
  }

  if (entry && entry->object)
  {
 
     if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_NUMERICAL_ECCENTRICITY hyperbola_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `h.numerical_eccentricity' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         h->numerical_eccentricity = 1.0;

     }
     else
     {
        h->numerical_eccentricity = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`h->numerical_eccentricity' == " << h->numerical_eccentricity 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */

  @=$$@> = 0;

};


@q **** (4) command: SET_FOCUS parabola_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_FOCUS} \�parabola variable> 
\.{COMMA} \�numeric expression> \.{COMMA} \�point expression>.
\initials{LDF 2022.12.05.}

\LOG
\initials{LDF 2022.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_FOCUS parabola_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_FOCUS parabola_variable COMMA point_expression'." 
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Parabola *prb = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Parabola>(0));
   }

   if (entry && entry->object && p)
   {
       prb = static_cast<Parabola*>(entry->object);

       prb->focus_0 = *p;

       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_FOCUS parabola_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Parabola*) and/or `Point *p' are NULL.  Can't set focus 0."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;
};

@q **** (4) command: SET_FOCUS hyperbola_variable COMMA numeric_expression COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_FOCUS\_ZERO} \�hyperbola variable> 
\.{COMMA} \�numeric expression> \.{COMMA} \�point expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_FOCUS hyperbola_variable COMMA numeric_expression COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_FOCUS hyperbola_variable COMMA numeric_expression COMMA point_expression'." 
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Hyperbola *h = 0;

   Point *p = static_cast<Point*>(@=$6@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   unsigned int index = static_cast<unsigned int>(floor(@=$4@>));


   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Hyperbola>(0));
   }

   if (entry && entry->object && p)
   {
       h = static_cast<Hyperbola*>(entry->object);

       if (index == 0)
          h->focus_0 = *p;
       else 
          h->focus_1 = *p;

       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_FOCUS hyperbola_variable COMMA numeric_expression COMMA point_expression':"
                 << "`entry', `entry->object' (Hyperbola*) and/or `Point *p' are NULL.  Can't set focus 0."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;
};


@q **** (4) command: SET_VERTEX parabola_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_VERTEX} \�parabola variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.12.05.}

\LOG
\initials{LDF 2022.12.05.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_VERTEX parabola_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_VERTEX parabola_variable COMMA point_expression'." 
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Parabola *prb = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Parabola>(0));
   }

   if (entry && entry->object && p)
   {
       prb = static_cast<Parabola*>(entry->object);
       prb->vertex_0 = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_VERTEX parabola_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Parabola*) and/or `Point *p' are NULL.  Can't set vertex 0."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;
};

@q **** (4) command: SET_VERTEX hyperbola_variable COMMA numeric_expression COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_VERTEX} \�hyperbola variable> 
\.{COMMA} \�numeric expression> \.{COMMA} \�point expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_VERTEX hyperbola_variable COMMA numeric_expression COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_VERTEX hyperbola_variable COMMA numeric_expression COMMA point_expression'." 
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Hyperbola *h = 0;

   Point *p = static_cast<Point*>(@=$6@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   unsigned int index = static_cast<unsigned int>(floor(@=$4@>));

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Hyperbola>(0));
   }

   if (entry && entry->object && p)
   {
       h = static_cast<Hyperbola*>(entry->object);

       if (index == 0)
          h->vertex_0 = *p;
       else 
          h->vertex_1 = *p;

       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_VERTEX hyperbola_variable COMMA numeric_expression COMMA point_expression':"
                 << "`entry', `entry->object' (Hyperbola*) and/or `Point *p' are NULL.  Can't set vertex 0."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;
};



@q **** (4) command: SET_CENTER rectangle_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�rectangle variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.11.25.}

\LOG
\initials{LDF 2022.11.25.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER rectangle_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER rectangle_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Rectangle *r = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Rectangle>(0));
   }

   if (entry && entry->object && p)
   {
       r = static_cast<Rectangle*>(entry->object);
       r->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER rectangle_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Rectangle*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_CENTER reg_polygon_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�reg_polygon variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER reg_polygon_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER reg_polygon_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Reg_Polygon *r = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Reg_Polygon>(0));
   }

   if (entry && entry->object && p)
   {
       r = static_cast<Reg_Polygon*>(entry->object);
       r->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER reg_polygon_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Reg_Polygon*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_CENTER cuboid_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�cuboid variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.11.27.}

\LOG
\initials{LDF 2022.11.27.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER cuboid_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER cuboid_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Cuboid *c = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Cuboid>(0));
   }

   if (entry && entry->object && p)
   {
       c = static_cast<Cuboid*>(entry->object);
       c->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER cuboid_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Cuboid*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_WIDTH cuboid_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_WIDTH} \�cuboid variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_WIDTH cuboid_variable COMMA numeric_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_WIDTH cuboid_variable COMMA numeric_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Cuboid *c = 0;

   real r = @=$4@>;

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Cuboid>(0));
   }

   if (entry && entry->object)
   {
       c = static_cast<Cuboid*>(entry->object);
       c->width = r;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_WIDTH cuboid_variable COMMA numeric_expression':"
                 << "`entry' or `entry->object' (Cuboid*) is NULL.  Can't set width."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_HEIGHT cuboid_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_HEIGHT} \�cuboid variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_HEIGHT cuboid_variable COMMA numeric_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_HEIGHT cuboid_variable COMMA numeric_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Cuboid *c = 0;

   real r = @=$4@>;

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Cuboid>(0));
   }

   if (entry && entry->object)
   {
       c = static_cast<Cuboid*>(entry->object);
       c->height = r;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_HEIGHT cuboid_variable COMMA numeric_expression':"
                 << "`entry' or `entry->object' (Cuboid*) is NULL.  Can't set height."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_DEPTH cuboid_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_DEPTH} \�cuboid variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_DEPTH cuboid_variable COMMA numeric_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_DEPTH cuboid_variable COMMA numeric_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Cuboid *c = 0;

   real r = @=$4@>;

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Cuboid>(0));
   }

   if (entry && entry->object)
   {
       c = static_cast<Cuboid*>(entry->object);
       c->depth = r;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_DEPTH cuboid_variable COMMA numeric_expression':"
                 << "`entry' or `entry->object' (Cuboid*) is NULL.  Can't set depth."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_SIDES reg_polygon_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_SIDES} \�reg_polygon_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_SIDES reg_polygon_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_SIDES reg_polygon_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Reg_Polygon *rp = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     rp = static_cast<Reg_Polygon*>(entry->object);

  if (entry && !entry->object)
  {
     rp = create_new<Reg_Polygon>(0);
     entry->object = static_cast<void*>(rp);
  }

  if (entry && entry->object)
  {
 
     if (r < 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_SIDES reg_polygon_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (< 0.0)"
                   << endl 
                   << "Invalid value.  Setting `rp.sides' to 0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         rp->sides = 0U;

     }
     else
     {
 #if LDF_REAL_FLOAT
         rp->sides = static_cast<unsigned int>(roundf(r));
#elif LDF_REAL_DOUBLE
         rp->sides = static_cast<unsigned int>(round(r));
#else /* Default.  */
         rp->sides = static_cast<unsigned int>(roundf(r));
#endif
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`rp->sides' == " << rp->sides 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */

  @=$$@> = 0;

};

@q **** (4) command: SET_RADIUS reg_polygon_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_RADIUS} \�reg_polygon_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_RADIUS reg_polygon_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_RADIUS reg_polygon_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Reg_Polygon *rp = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     rp = static_cast<Reg_Polygon*>(entry->object);

  if (entry && !entry->object)
  {
     rp = create_new<Reg_Polygon>(0);
     entry->object = static_cast<void*>(rp);
  }

  if (entry && entry->object)
  {
 
     if (r < 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_RADIUS reg_polygon_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (< 0.0)"
                   << endl 
                   << "Invalid value.  Setting `rp.radius' to 0.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         rp->radius = 0.0;

     }
     else
     {
         rp->radius = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`rp->radius' == " << rp->radius 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */

  @=$$@> = 0;

};

@q **** (4) command: SET_INTERNAL_ANGLE reg_polygon_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_INTERNAL\_ANGLE} \�reg_polygon_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_INTERNAL_ANGLE reg_polygon_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_INTERNAL_ANGLE reg_polygon_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Reg_Polygon *rp = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     rp = static_cast<Reg_Polygon*>(entry->object);

  if (entry && !entry->object)
  {
     rp = create_new<Reg_Polygon>(0);
     entry->object = static_cast<void*>(rp);
  }

  if (entry && entry->object)
  {
 
     if (r < 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_INTERNAL_ANGLE reg_polygon_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (< 0.0)"
                   << endl 
                   << "Invalid value.  Setting `rp.internal_angle' to 0.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         rp->internal_angle = 0.0;

     }
     else
     {
         rp->internal_angle = r;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`rp->internal_angle' == " << rp->internal_angle 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */

  @=$$@> = 0;

};

@q **** (4) command: SET_AXIS_H rectangle_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_AXIS\_H} \�rectangle_variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.19.}

\LOG
\initials{LDF 2022.11.19.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_AXIS_H rectangle_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_AXIS_H rectangle_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Rectangle *r = 0;

  real n = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     r = static_cast<Rectangle*>(entry->object);

  if (entry && !entry->object)
  {
     r = create_new<Rectangle>(0);
     entry->object = static_cast<void*>(r);
  }

  if (entry && entry->object)
  {
 
     if (n <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_AXIS_H rectangle_variable COMMA numeric_expression':"
                   << endl 
                   << "`n' (numeric_expression) == " << n << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `r.axis_h' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         r->axis_h = 1.0;

     }
     else
     {
        r->axis_h = n;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "n (numeric_expression) == " << n << endl 
              << "`r->axis_h' == " << r->axis_h 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_AXIS_V rectangle_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_AXIS\_V} \�rectangle_variable> \.{COMMA} \�numeric expression>.
\initials{LDF 2022.11.19.}

\LOG
\initials{LDF 2022.11.19.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_AXIS_V rectangle_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_AXIS_V rectangle_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Rectangle *r = 0;

  real n = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     r = static_cast<Rectangle*>(entry->object);

  if (entry && !entry->object)
  {
     r = create_new<Rectangle>(0);
     entry->object = static_cast<void*>(r);
  }

  if (entry && entry->object)
  {
 
     if (n <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_AXIS_V rectangle_variable COMMA numeric_expression':"
                   << endl 
                   << "`n' (numeric_expression) == " << n << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting `r.axis_v' to 1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         r->axis_v = 1.0;

     }
     else
     {
        r->axis_v = n;
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "n (numeric_expression) == " << n << endl 
              << "`r->axis_v' == " << r->axis_v 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) command: SET_CENTER circle_variable COMMA point_expression@>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�circle variable> \.{COMMA} \�point expression>.
\initials{LDF 2022.11.27.}

\LOG
\initials{LDF 2022.11.27.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: SET_CENTER circle_variable COMMA point_expression@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET_CENTER circle_variable COMMA point_expression'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

   Circle *c = 0;

   Point *p = static_cast<Point*>(@=$4@>);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && !entry->object)
   {
      entry->object = static_cast<void*>(create_new<Circle>(0));
   }

   if (entry && entry->object && p)
   {
       c = static_cast<Circle*>(entry->object);
       c->center = *p;
       delete p;
       p = 0;
   }
   else
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET_CENTER circle_variable COMMA point_expression':"
                 << "`entry', `entry->object' (Circle*) and/or `Point *p' are NULL.  Can't set center."
                 << endl 
                 << "Will try to continue."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       if (p)
       {
          delete p;
          p = 0;
       }

   } 

   @=$$@> = 0;

};

@q **** (4) command: SET_RADIUS circle_variable COMMA numeric_expression.  @>
@ \�command> $\longrightarrow$ \.{SET\_RADIUS} \�circle_variable> \.{COMMA} 
\�numeric expression>.
\initials{LDF 2022.11.26.}

\LOG
\initials{LDF 2022.11.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_RADIUS circle_variable COMMA numeric_expression@>@/
{
@q ***** (5) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_RADIUS circle_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

  Circle *c = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     c = static_cast<Circle*>(entry->object);

  if (entry && !entry->object)
  {
     c = create_new<Circle>(0);
     entry->object = static_cast<void*>(c);
  }

@q ***** (5) @>

  if (entry && entry->object)
  {
 @q ****** (6) @>
 
     if (r < 0.0)
     {
         cerr_strm << thread_name 
                   << "ERROR!  In parser, `command: SET_RADIUS circle_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (< 0.0)"
                   << endl 
                   << "Invalid value.  Setting `c.radius' to -1.0." 
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         c->radius = -1.0;

     }

@q ****** (6) @>

     else
     {
         c->radius = r;
     }

@q ****** (6) @>

#if DEBUG_COMPILE
     if (DEBUG && c->points.size() < 1)
     { 
         cerr_strm << thread_name 
                   << "In parser, `command: SET_RADIUS circle_variable COMMA numeric_expression':"
                   << endl 
                   << "`c->points.size()' == " << c->points.size() << " (< 1)"
                   << endl 
                   << "This can happen if `radius' is set before points are appended to the `circle'."
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`c->radius' == " << c->radius 
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ****** (6) @>

  }  /* |if (entry && entry->object)|  */

@q ***** (5) @>

  @=$$@> = 0;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET_TYPE sphere_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_TYPE sphere_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry)
     s = static_cast<Sphere*>(entry->object);

  if (entry && !entry->object)
  {
     s = create_new<Sphere>(0);
     entry->object = static_cast<void*>(s);
  }

  if (entry && entry->object)
  {
  
     if (r > Sphere::MAX_SPHERE_TYPE)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_TYPE sphere_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) is greater than `Sphere::MAX_SPHERE_TYPE':" << endl 
                   << "`r' == " << r << endl 
                   << "`Sphere::MAX_SPHERE_TYPE' == " << Sphere::MAX_SPHERE_TYPE 
                   << endl
                   << "Setting sphere type to " << 1 << " "
                   << "(" << Sphere::sphere_type_name_map[1] << ")"
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         s->sphere_type = static_cast<unsigned short>(1);         

     }
     else if (r <= 0.0)
     {
         cerr_strm << thread_name 
                   << "WARNING!  In parser, `command: SET_TYPE sphere_variable COMMA numeric_expression':"
                   << endl 
                   << "`r' (numeric_expression) == " << r << " (<= 0.0)"
                   << endl 
                   << "Invalid value.  Setting sphere type to 1 " 
                   << "(" << Sphere::sphere_type_name_map[1] << ")."
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         s->sphere_type = static_cast<unsigned short>(1);         

     }
     else
     {
#if LDF_REAL_FLOAT
        s->sphere_type = static_cast<unsigned short>(roundf(r));
#else
        s->sphere_type = static_cast<unsigned short>(round(r));
#endif 
     }

#if DEBUG_COMPILE
     if (DEBUG)
     { 
         cerr << "entry->name  == " << entry->name << endl
              << "r (numeric_expression) == " << r << endl 
              << "`s->sphere_type' == " << s->sphere_type << " == " 
              << Sphere::sphere_type_name_map[s->sphere_type]
              << endl;
     }  
#endif /* |DEBUG_COMPILE|  */@; 

  }  /* |if (entry && entry->object)|  */


  @=$$@> = 0;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET_RADIUS sphere_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_RADIUS sphere_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
  {
      s = static_cast<Sphere*>(entry->object);
      s->radius = r;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_RADIUS sphere_variable COMMA numeric_expression':"
                << "`entry' and `entry->object' are both NULL.  Sphere object doesn't exist.  Can't set radius."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  } 

  @=$$@> = 0;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET_CIRCLE_POINT_COUNT sphere_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_CIRCLE_POINT_COUNT sphere_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
  {
      s = static_cast<Sphere*>(entry->object);
      s->circle_point_count = r;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_CIRCLE_POINT_COUNT sphere_variable COMMA numeric_expression':"
                << "`entry' and `entry->object' are both NULL.  Sphere object doesn't exist.  Can't set circle point count."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  } 

  @=$$@> = 0;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET_DIVISIONS_VERTICAL sphere_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_DIVISIONS_VERTICAL sphere_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
  {
      s = static_cast<Sphere*>(entry->object);
      s->divisions_vertical = r;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_DIVISIONS_VERTICAL sphere_variable COMMA numeric_expression':"
                << "`entry' and `entry->object' are both NULL.  Sphere object doesn't exist.  Can't set divisions vertical."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  } 

  @=$$@> = 0;

};

@q **** (4) @>
@
@<Define rules@>=
@=command: SET_DIVISIONS_HORIZONTAL sphere_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_DIVISIONS_HORIZONTAL sphere_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  real r = @=$4@>;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
  {
      s = static_cast<Sphere*>(entry->object);
      s->divisions_horizontal = r;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_DIVISIONS_HORIZONTAL sphere_variable COMMA numeric_expression':"
                << "`entry' and `entry->object' are both NULL.  Sphere object doesn't exist.  Can't set divisions horizontal."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  } 

  @=$$@> = 0;

};

@q **** (4) command: SET_CENTER sphere_variable COMMA point_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�sphere variable> \.{COMMA} \�point expression>.

@<Define rules@>=
@=command: SET_CENTER sphere_variable COMMA point_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_CENTER sphere_variable COMMA point_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Sphere *s = 0;

  Point *p = static_cast<Point*>(@=$4@>);

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && !entry->object)
  {
     entry->object = static_cast<void*>(create_new<Sphere>(0));
  }

  if (entry && entry->object && p)
  {
      s = static_cast<Sphere*>(entry->object);
      s->center = *p;
      delete p;
      p = 0;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_CENTER sphere_variable COMMA point_expression':"
                << "`entry', `entry->object' (Sphere*) and/or `Point *p' are NULL.  Can't set center."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      if (p)
      {
         delete p;
         p = 0;
      }

  } 

  @=$$@> = 0;

};

@q **** (4) command: SET_CENTER polyhedron_variable COMMA point_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_CENTER} \�polyhedron variable> \.{COMMA} \�point expression>.

@<Define rules@>=
@=command: SET_CENTER polyhedron_variable COMMA point_expression@>@/
{
#if 0 

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_CENTER polyhedron_variable COMMA point_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

  Polyhedron *s = 0;

  Point *p = static_cast<Point*>(@=$4@>);

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && !entry->object)
  {
     entry->object = static_cast<void*>(create_new<Polyhedron>(0));
  }

  if (entry && entry->object && p)
  {
      s = static_cast<Polyhedron*>(entry->object);
      s->center = *p;
      delete p;
      p = 0;
  }
  else
  {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_CENTER polyhedron_variable COMMA point_expression':"
                << "`entry', `entry->object' (Polyhedron*) and/or `Point *p' are NULL.  Can't set center."
                << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      if (p)
      {
         delete p;
         p = 0;
      }

  } 
#endif 

  @=$$@> = 0;

};


@q **** (4) command: SET_PATH set_path_list.  @>

@ \�command> $\longrightarrow$ \.{SET\_PATH} \�set path list>.

@<Define rules@>=
@=command: SET_PATH set_path_list@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_PATH set_path_list'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = 0;

};

@q **** (4) command: SET_DIRECTRIX parabola_variable COMMA path_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_DIRECTRIX} \�parabola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.05.}

\LOG
\initials{LDF 2022.12.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_DIRECTRIX parabola_variable COMMA path_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_DIRECTRIX parabola_variable COMMA path_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Path *q = static_cast<Path*>(@=$4@>);

    if (entry && entry->object && q)
    {
        Parabola *p = static_cast<Parabola*>(entry->object);

        p->directrix = *q;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_DIRECTRIX parabola_variable COMMA path_expression':"
                << endl
                << "`entry', `entry->object' and/or `path_expression' are NULL." << endl 
                << "Can't set directrix of parabola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    if (q == 0)
    {
        delete q;
        q = 0;
    }

    @=$$@> = 0;

};

@q **** (4) command: SET_DIRECTRIX hyperbola_variable COMMA numeric_expression COMMA path_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_DIRECTRIX} \�hyperbola variable> \.{COMMA} \�numeric expression>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_DIRECTRIX hyperbola_variable COMMA numeric_expression COMMA path_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_DIRECTRIX hyperbola_variable COMMA numeric_expression COMMA path_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Path *q = static_cast<Path*>(@=$6@>);

    unsigned int index = static_cast<unsigned int>(floor(@=$4@>));

    if (entry && entry->object && q)
    {
        Hyperbola *h = static_cast<Hyperbola*>(entry->object);

        if (index == 0)
           h->directrix = *q;
        else
           h->directrix_1 = *q;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_DIRECTRIX hyperbola_variable COMMA "
		<< "numeric_expression COMMA path_expression':"
                << endl
                << "`entry', `entry->object' and/or `path_expression' are NULL." << endl 
                << "Can't set directrix of hyperbola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    if (q == 0)
    {
        delete q;
        q = 0;
    }

    @=$$@> = 0;

};

@q **** (4) command: SET_ASYMPTOTE hyperbola_variable COMMA numeric_expression COMMA path_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_ASYMPTOTE} \�hyperbola variable> \.{COMMA} \�numeric expression>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.14.}

\LOG
\initials{LDF 2022.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_ASYMPTOTE hyperbola_variable COMMA numeric_expression COMMA path_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_ASYMPTOTE hyperbola_variable COMMA numeric_expression COMMA path_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Path *q = static_cast<Path*>(@=$6@>);

    unsigned int index = static_cast<unsigned int>(floor(@=$4@>));

    if (entry && entry->object && q)
    {
        Hyperbola *h = static_cast<Hyperbola*>(entry->object);

        if (index == 0)
           h->asymptote_0 = *q;
        else
           h->asymptote_1 = *q;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_ASYMPTOTE hyperbola_variable COMMA "
		<< "numeric_expression COMMA path_expression':"
                << endl
                << "`entry', `entry->object' and/or `path_expression' are NULL." << endl 
                << "Can't set asymptote of hyperbola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    if (q == 0)
    {
        delete q;
        q = 0;
    }

    @=$$@> = 0;

};

@q **** (4) command: SET_AXIS parabola_variable COMMA path_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_AXIS} \�parabola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.09.}

\LOG
\initials{LDF 2022.12.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_AXIS parabola_variable COMMA path_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_AXIS parabola_variable COMMA path_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Path *q = static_cast<Path*>(@=$4@>);

    if (entry && entry->object && q)
    {
        Parabola *p = static_cast<Parabola*>(entry->object);

        p->axis = *q;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_AXIS parabola_variable COMMA path_expression':"
                << endl
                << "`entry', `entry->object' and/or `path_expression' are NULL." << endl 
                << "Can't set axis of parabola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    if (q == 0)
    {
        delete q;
        q = 0;
    }

    @=$$@> = 0;

};

@q **** (4) command: SET_LATUS_RECTUM parabola_variable COMMA path_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_LATUS_RECTUM} \�parabola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.09.}

\LOG
\initials{LDF 2022.12.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_LATUS_RECTUM parabola_variable COMMA path_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_LATUS_RECTUM parabola_variable COMMA path_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Path *q = static_cast<Path*>(@=$4@>);

    if (entry && entry->object && q)
    {
        Parabola *p = static_cast<Parabola*>(entry->object);

        p->latus_rectum = *q;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_LATUS_RECTUM parabola_variable COMMA path_expression':"
                << endl
                << "`entry', `entry->object' and/or `path_expression' are NULL." << endl 
                << "Can't set latus_rectum of parabola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    if (q == 0)
    {
        delete q;
        q = 0;
    }

    @=$$@> = 0;

};

@q **** (4) command: SET_PARAMETER parabola_variable COMMA numeric_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_PARAMETER} \�parabola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.09.}

\LOG
\initials{LDF 2022.12.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_PARAMETER parabola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_PARAMETER parabola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    real r = @=$4@>;

    if (entry && entry->object)
    {
        Parabola *p = static_cast<Parabola*>(entry->object);

        p->parameter = r;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_PARAMETER parabola_variable COMMA numeric_expression':"
                << endl
                << "`entry' or `entry->object' is NULL." << endl 
                << "Can't set parameter of parabola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    @=$$@> = 0;

};

@q **** (4) command: SET_MAX_EXTENT parabola_variable COMMA numeric_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_MAX\_EXTENT} \�parabola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.09.}

\LOG
\initials{LDF 2022.12.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_MAX_EXTENT parabola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_MAX_EXTENT parabola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    real r = @=$4@>;

    if (entry && entry->object)
    {
        Parabola *p = static_cast<Parabola*>(entry->object);

        p->max_extent = r;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_MAX_EXTENT parabola_variable COMMA numeric_expression':"
                << endl
                << "`entry' or `entry->object' is NULL." << endl 
                << "Can't set max_extent of parabola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    @=$$@> = 0;

};

@q **** (4) command: SET_PARAMETER hyperbola_variable COMMA numeric_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_PARAMETER} \�hyperbola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.09.}

\LOG
\initials{LDF 2022.12.09.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_PARAMETER hyperbola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_PARAMETER hyperbola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    real r = @=$4@>;

    if (entry && entry->object)
    {
        Hyperbola *h = static_cast<Hyperbola*>(entry->object);

        h->parameter = r;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_PARAMETER hyperbola_variable COMMA numeric_expression':"
                << endl
                << "`entry' or `entry->object' is NULL." << endl 
                << "Can't set parameter of hyperbola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    @=$$@> = 0;

};

@q **** (4) command: SET_MAX_EXTENT hyperbola_variable COMMA numeric_expression.  @>

@ \�command> $\longrightarrow$ \.{SET\_MAX\_EXTENT} \�hyperbola variable>
\.{COMMA} \�path expression>.
\initials{LDF 2022.12.13.}

\LOG
\initials{LDF 2022.12.13.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_MAX_EXTENT hyperbola_variable COMMA numeric_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `command: SET_MAX_EXTENT hyperbola_variable COMMA numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    real r = @=$4@>;

    if (entry && entry->object)
    {
        Hyperbola *h = static_cast<Hyperbola*>(entry->object);

        h->max_extent = r;
    }
    else
    {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `command: SET_MAX_EXTENT hyperbola_variable COMMA numeric_expression':"
                << endl
                << "`entry' or `entry->object' is NULL." << endl 
                << "Can't set max_extent of hyperbola variable.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
         
    }
   
    @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.16.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> set_path_list@>

@q **** (4) set_path_list: any_variable  @>

@ \�set path list> $\longrightarrow$ \�any variable>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_path_list: any_variable @>@/
{
@q ***** (5) @>

  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `set_path_list: any_variable'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

    entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ***** (5) @>

    if (entry->type == PATH)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a path." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Path *p = 0;

       if (entry->object == 0)
       {
           p = create_new<Path>(0);
           entry->object = static_cast<void*>(p);
       }
       else 
           p = static_cast<Path*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == RECTANGLE)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `rectangle'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Rectangle *r = 0;

       if (entry->object == 0)
       {
           r = create_new<Rectangle>(0);
           entry->object = static_cast<void*>(r);
       }
       else 
           r = static_cast<Rectangle*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == REG_POLYGON)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `reg_polygon'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Reg_Polygon *rp = 0;

       if (entry->object == 0)
       {
           rp = create_new<Reg_Polygon>(0);
           entry->object = static_cast<void*>(rp);
       }
       else 
           rp = static_cast<Reg_Polygon*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == POLYGON)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `polygon'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Polygon *p = 0;

       if (entry->object == 0)
       {
           p = create_new<Polygon>(0);
           entry->object = static_cast<void*>(p);
       }
       else 
           p = static_cast<Polygon*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == CIRCLE)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `circle'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Circle *c = 0;

       if (entry->object == 0)
       {
           c = create_new<Circle>(0);
           entry->object = static_cast<void*>(c);
       }
       else 
           c = static_cast<Circle*>(entry->object);

       @=$$@> = @=$1@>;     

    }


@q ***** (5) @>

    else if (entry->type == ELLIPSE)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `ellipse'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Ellipse *e = 0;

       if (entry->object == 0)
       {
           e = create_new<Ellipse>(0);
           entry->object = static_cast<void*>(e);
       }
       else 
           e = static_cast<Ellipse*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == PARABOLA)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `parabola'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Parabola *e = 0;

       if (entry->object == 0)
       {
           e = create_new<Parabola>(0);
           entry->object = static_cast<void*>(e);
       }
       else 
           e = static_cast<Parabola*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == HYPERBOLA)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's a `hyperbola'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Hyperbola *h = 0;

       if (entry->object == 0)
       {
           h = create_new<Hyperbola>(0);
           entry->object = static_cast<void*>(h);
       }
       else 
           h = static_cast<Hyperbola*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else if (entry->type == RECTANGLE)
    {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "It's an `rectangle'." << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 

       Rectangle *e = 0;

       if (entry->object == 0)
       {
           e = create_new<Rectangle>(0);
           entry->object = static_cast<void*>(e);
       }
       else 
           e = static_cast<Rectangle*>(entry->object);

       @=$$@> = @=$1@>;     

    }

@q ***** (5) @>

    else
    {
       cerr << "WARNING!  In `set_path_list: any_variable'  The type of `any_variable' hasn't been accounted for:"
            << endl 
            << "`entry->type' == " << entry->type << " == " << name_map[entry->type] << endl 
            << "Not setting.  Continuing." << endl;

       @=$$@> = 0;
    }
   
};

@q **** (4) set_path_list: set_path_list COMMA point_expression  @>

@ \�set path list> $\longrightarrow$ \.{COMMA} \�point expression>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_path_list: set_path_list COMMA point_expression@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `set_path_list: set_path_list COMMA point_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

    if (entry == 0)
    {
       delete static_cast<Point*>(@=$3@>);
       @=$$@> = 0;  
    }
    else
    {
        Path *q = static_cast<Path*>(entry->object);
        Point *p = static_cast<Point*>(@=$3@>);
    
        *q += p;

        @=$$@> = @=$1@>;     
    }
};

@q **** (4) set_path_list: set_path_list COMMA path_join  @>

@ \�set path list> $\longrightarrow$ \.{COMMA} \�path join>.
\initials{LDF 2022.11.16.}

\LOG
\initials{LDF 2022.11.16.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=set_path_list: set_path_list COMMA path_join@>@/
{
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `set_path_list: set_path_list COMMA path_join'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

    if (entry == 0)
    {
       delete static_cast<Connector_Type*>(@=$3@>);
       @=$$@> = 0;
    }
    else
    {
        Path *q = static_cast<Path*>(entry->object);
        Connector_Type *c = static_cast<Connector_Type*>(@=$3@>);
    
        *q += c;

        @=$$@> = @=$1@>;     
    }
};

@q **** (4) command: SET_GLOBAL_DEBUG@>

@ 
\initials{LDF 2022.11.24.}

\LOG
\initials{LDF 2022.11.24.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_GLOBAL_DEBUG@>@/
{
   GLOBAL_DEBUG = true;  /* Don't delete or change to |false|  \initials{LDF 2022.11.28.}  */
};

@q **** (4) command: UNSET_GLOBAL_DEBUG  @>

@ 
\initials{LDF 2022.11.24.}

\LOG
\initials{LDF 2022.11.24.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: UNSET_GLOBAL_DEBUG@>@/
{
   GLOBAL_DEBUG = false;
};

@q **** (4) command: SHOW_GLOBAL_DEBUG  @>

@ 
\initials{LDF 2022.11.24.}

\LOG
\initials{LDF 2022.11.24.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SHOW_GLOBAL_DEBUG@>@/
{
   if (GLOBAL_DEBUG)
      cerr << "true" << endl;
   else
      cerr << "false" << endl;
};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_FACES polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_faces(static_cast<unsigned short>(roundf(fabsf(@=$4@>))));
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_VERTICES polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_vertices(static_cast<unsigned short>(roundf(fabsf(@=$4@>))));
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_EDGES polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_edges(static_cast<unsigned short>(roundf(fabsf(@=$4@>))));
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_NUMBER_OF_POLYGON_TYPES polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_number_of_polygon_types(static_cast<unsigned short>(roundf(fabsf(@=$4@>))));
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_FACE_RADIUS polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_face_radius(@=$4@>);
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_EDGE_RADIUS polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_edge_radius(@=$4@>);
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_VERTEX_RADIUS polyhedron_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry)
   {
       if (!entry->object)
       {
          entry->object = static_cast<void*>(create_new<Polyhedron>(0));
       }

       static_cast<Polyhedron*>(entry->object)->set_vertex_radius(@=$4@>);
   }

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_POSITION line_variable COMMA point_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Point *p = static_cast<Point*>(@=$4@>);

   if (entry)
   {
      Line *L = 0;

      if (entry->object == 0)
      {
         L = new Line;
         entry->object = static_cast<void*>(L);
      }
      else
         L = static_cast<Line*>(entry->object);

      L->position = *p;
   }

   delete p;
   p = 0;

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_DIRECTION line_variable COMMA point_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Point *p = static_cast<Point*>(@=$4@>);

   if (entry)
   {
      Line *L = 0;

      if (entry->object == 0)
      {
         L = new Line;
         entry->object = static_cast<void*>(L);
      }
      else
         L = static_cast<Line*>(entry->object);

      L->direction = *p;
   }

   delete p;
   p = 0;

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_POINT plane_variable COMMA point_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Point *p = static_cast<Point*>(@=$4@>);

   if (entry)
   {
      Plane *q = 0;

      if (entry->object == 0)
      {
         q = new Plane;
         entry->object = static_cast<void*>(q);
      }
      else
         q = static_cast<Plane*>(entry->object);

      q->point = *p;
   }

   delete p;
   p = 0;

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_NORMAL plane_variable COMMA point_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Point *p = static_cast<Point*>(@=$4@>);

   if (entry)
   {
      Plane *q = 0;

      if (entry->object == 0)
      {
         q = new Plane;
         entry->object = static_cast<void*>(q);
      }
      else
         q = static_cast<Plane*>(entry->object);

      q->normal = *p;
   }

   delete p;
   p = 0;

   @=$$@> = 0;

};

@q **** (4) @>
@
\LOG
\initials{LDF 2023.01.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET_DISTANCE plane_variable COMMA numeric_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real n = @=$4@>;

   if (entry)
   {
      Plane *q = 0;

      if (entry->object == 0)
      {
         q = new Plane;
         entry->object = static_cast<void*>(q);
      }
      else
         q = static_cast<Plane*>(entry->object);

      q->distance = n;
   }

   @=$$@> = 0;

};

@q **** (4) Set sine wave.  @>
@ Set sine wave.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SET sinewave_variable set_sinewave_option_list@>@/
{
@q ***** (5) @>

   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `command: SET sinewave_variable set_sinewave_option_list'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Sinewave *s = 0;  

   if (entry)
   {
      if (entry->object == 0)
      {
         s = new Sinewave;
         entry->object = static_cast<void*>(s);
      }
      else
         s = static_cast<Sinewave*>(entry->object);
   }

@q ***** (5) @>
@
@<Define rules@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
   cerr << "scanner_node->sinewave_frequency_value == " << scanner_node->sinewave_frequency_value 
        << endl 
        << "scanner_node->sinewave_amplitude_value == " << scanner_node->sinewave_amplitude_value 
        << endl 
        << "scanner_node->sinewave_period_value    == " << scanner_node->sinewave_period_value    
        << endl 
        << "scanner_node->sinewave_phase_value     == " << scanner_node->sinewave_phase_value
        << endl 
        << "scanner_node->sinewave_increment_value == " << scanner_node->sinewave_increment_value
        << endl; 

   if (scanner_node->sinewave_start_pt)
      static_cast<Point*>(scanner_node->sinewave_start_pt)->show("*scanner_node->sinewave_start_pt:");
   else
      cerr << "`scanner_node->sinewave_start_pt' is NULL." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>
@
@<Define rules@>=

   if (scanner_node->sinewave_frequency_value <= 0)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `command: SET sinewave_variable set_sinewave_option_list':"
                 << endl 
                 << "`scanner_node->sinewave_frequency_value' == 0" << endl 
                 << endl
                 << "This is not permitted.  Frequency must be > 0." << endl 
                 << "Not calling `Sinewave::set'.  Will try to continue.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
   }

@q ***** (5) @>
@
@<Define rules@>=
   else
   {
@q ****** (6) @>

      status = s->set(scanner_node);

      if (status != 0)
      {
          cerr_strm << thread_name 
                    << "ERROR!  In parser, rule `command: SET sinewave_variable set_sinewave_option_list':"
                    << endl 
                    << "`Sinewave::set' failed, returning " << status << "." << endl 
                    << endl
                    << "Failed to set sinewave.  Will try to continue.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");
      }      

@q ****** (6) @>

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
          cerr_strm << thread_name 
                    << "In parser, rule `command: SET sinewave_variable set_sinewave_option_list':"
                    << endl 
                    << "`Sinewave::set' succeeded, returning 0.";

          log_message(cerr_strm);
          cerr_message(cerr_strm);
          cerr_strm.str("");

      }  
#endif /* |DEBUG_COMPILE|  */@;      


@q ****** (6) @>

   } 

@q ***** (5) @>
   
   scanner_node->sinewave_period_value     = 0;
   scanner_node->sinewave_amplitude_value  = 0;
   scanner_node->sinewave_frequency_value  = 0;
   scanner_node->sinewave_phase_value      = 0;
   scanner_node->sinewave_increment_value  = 0;
   scanner_node->sinewave_overshoot_value  = false;
   scanner_node->sinewave_undershoot_value = false;
   scanner_node->sinewave_truncate_value   = false;

   if (scanner_node->sinewave_start_pt)
   {
       delete static_cast<Point*>(scanner_node->sinewave_start_pt);
       scanner_node->sinewave_start_pt = 0;
   }

   if (scanner_node->sinewave_transform)
   {
       delete static_cast<Point*>(scanner_node->sinewave_transform);
       scanner_node->sinewave_transform = 0;
   }

@q ***** (5) @>

   @=$$@> = 0;

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> set_sinewave_option_list@>

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: /* Empty  */@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: /* Empty  */'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_frequency_value  = 0;
    scanner_node->sinewave_amplitude_value  = 0;
    scanner_node->sinewave_period_value     = 0;
    scanner_node->sinewave_phase_value      = 0;

    scanner_node->sinewave_increment_value  = 0;
    scanner_node->sinewave_overshoot_value  = false;
    scanner_node->sinewave_undershoot_value = false;
    scanner_node->sinewave_truncate_value   = false;

    if (scanner_node->sinewave_start_pt) 
    {
        delete static_cast<Point*>(scanner_node->sinewave_start_pt);
        scanner_node->sinewave_start_pt = 0;
    }

    if (scanner_node->sinewave_transform)
    {
        delete static_cast<Point*>(scanner_node->sinewave_transform);
        scanner_node->sinewave_transform = 0;
    }

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_FREQUENCY numeric_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_FREQUENCY numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_frequency_value = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_AMPLITUDE numeric_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_AMPLITUDE numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_amplitude_value = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_PERIOD numeric_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_PERIOD numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_period_value = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_PHASE numeric_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_PHASE numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_phase_value = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_INCREMENT numeric_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_INCREMENT numeric_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_increment_value = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list SINEWAVE_OVERSHOOT@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list "
                 << "SINEWAVE_OVERSHOOT'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_overshoot_value  = true;
    scanner_node->sinewave_undershoot_value = false;
    scanner_node->sinewave_truncate_value   = false;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list SINEWAVE_UNDERSHOOT@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list "
                 << "SINEWAVE_UNDERSHOOT'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_overshoot_value  = false;
    scanner_node->sinewave_undershoot_value = true;
    scanner_node->sinewave_truncate_value   = false;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list TRUNCATE@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list "
                 << "TRUNCATE'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_overshoot_value  = false;
    scanner_node->sinewave_undershoot_value = false;
    scanner_node->sinewave_truncate_value   = true;

};


@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list WITH_START_POINT point_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "WITH_START_POINT point_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_start_pt = @=$3@>;

};

@q ** (2) @>
@
@<Define rules@>=
@=set_sinewave_option_list: set_sinewave_option_list TRANSFORMED transform_expression @>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr_strm << thread_name 
                 << "*** Parser: `set_sinewave_option_list: set_sinewave_option_list"
                 << endl 
                 << "TRANSFORMED transform_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@; 

    scanner_node->sinewave_transform = @=$3@>;

};


@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables: @>
@q mode:CWEB  @>
@q eval:(outline-minor-mode t)  @>
@q abbrev-file-name:"~/.abbrev_defs" @>
@q eval:(read-abbrev-file)  @>
@q fill-column:80 @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End: @>
