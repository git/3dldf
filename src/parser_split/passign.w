@q passign.w @> 
@q Created by Laurence Finston Thu Apr 29 13:55:05 MEST 2004  @>

@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) Assignments.  @>
@** Assignments.  

\LOG
\initials{LDF 2004.05.03.}  
Completely rewrote the rule for assignment to
|numerics|.  The way it was before failed when I tried to use |variable| with no
further qualification in rules for both |numeric| and |point| variables. 

\initials{LDF 2004.11.05.}
Added type declaration for |operation_assignment|.

\initials{LDF 2004.12.04.}
Removed type declaration for |operation_assignment| to 
\filename{popassgn.w}.
\ENDLOG 

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.06.30.}
Make it possible to chain assignments for all types.  
\ENDTODO 

@q * (1) assignment.  @>
@* \�assignment>.

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> assignment@>

@q ** (2) Non-vector types.@> 
@*1 Non-vector types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q *** (3) Non-|Shape| types.@> 
@*1 Non-{\bf Shape}vector types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) assignment --> boolean_assignment. @>
@*3 \�assignment> $\longrightarrow$ \�boolean assignment>.

@<Define rules@>= 
  
@=assignment: boolean_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> numeric_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�numeric assignment>.
\�assignment>.

@<Define rules@>= 
  
@=assignment: numeric_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> ulong_long_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�ulong long assignment>.
\�assignment>.
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this rule.
\ENDLOG

@<Define rules@>= 
  
@=assignment: ulong_long_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) assignment --> string_assignment.@>
@*1 \�assignment> $\longrightarrow$ \�string assignment>.

\LOG
\initials{LDF 2004.05.19.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: string_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) assignment --> pen_assignment.  @>
@*1 \�assignment> $\longrightarrow$ \�pen assignment>.

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: pen_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) assignment --> dash_pattern_assignment.@>
@*1 \�assignment> $\longrightarrow$ \�dash pattern assignment>.

\LOG
\initials{LDF 2004.06.07.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: dash_pattern_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) assignment --> color_assignment.@>
@*1 \�assignment> $\longrightarrow$ \�color assignment>.

\LOG
\initials{LDF 2004.05.25.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: color_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> transform_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�transform assignment>.

@<Define rules@>= 
@=assignment: transform_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> picture_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�picture assignment>.

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: picture_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> focus_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�focus assignment>.

\LOG
\initials{LDF 2004.06.08.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: focus_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> macro_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�macro assignment>.

\LOG
\initials{LDF 2004.12.30.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=assignment: macro_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) |Shape| types.@> 
@*2 {\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) assignment --> bool_point_assignment. @>
@*3 \�assignment> $\longrightarrow$ \�bool-point assignment>.

\LOG
\initials{LDF 2004.09.01.}
Added this rule.
\ENDLOG

@<Define rules@>= 
  
@=assignment: bool_point_assignment@>
{
  @=$$@> = @=$1@>;

};

@q **** (4) assignment --> point_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�point assignment>.
@<Define rules@>= 
@=assignment: point_assignment@>
{

  @=$$@> = @=$1@>;

}; 

@q **** (4) assignment --> path_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�path assignment>.
@<Define rules@>= 
@=assignment: path_assignment@>
{
   @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> glyph_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�glyph assignment>.

\LOG
\initials{LDF 2022.01.16.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: glyph_assignment@>
{
   @=$$@> = @=$1@>;
}; 


@q **** (4) assignment --> catenary_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�catenary assignment>.

\LOG
\initials{LDF 2022.01.16.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: catenary_assignment@>
{
   @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> ellipse_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�ellipse assignment>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: ellipse_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> sinewave_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�sinewave assignment>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: sinewave_assignment@>
{
  @=$$@> = @=$1@>;
}; 


@q **** (4) assignment --> circle_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�circle assignment>.
@<Define rules@>= 
@=assignment: circle_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> parabola_assignment.@>

@*3 \�assignment> $\longrightarrow$ \�parabola assignment>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: parabola_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> hyperbola_assignment.@>

@*3 \�assignment> $\longrightarrow$ \�hyperbola assignment>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: hyperbola_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> helix_assignment.@>

@*3 \�assignment> $\longrightarrow$ \�helix assignment>.
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: helix_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> superellipse_assignment.@>

@*3 \�assignment> $\longrightarrow$ \�superellipse assignment>.
\initials{LDF 2022.04.27.}

\LOG
\initials{LDF 2022.04.27.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=assignment: superellipse_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> rectangle_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�rectangle assignment>.

\LOG
\initials{LDF 2004.06.30.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: rectangle_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> polygon_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�polygon assignment>.
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: polygon_assignment@>
{
  @=$$@> = @=$1@>;

}; 
@q **** (4) assignment --> reg_polygon_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�regular polygon assignment>.

\LOG
\initials{LDF 2004.07.06.}
Added this rule.

\initials{LDF 2004.08.18.}
Now setting |@=$$@> = @=$1@>|.
\ENDLOG 

@<Define rules@>= 
@=assignment: reg_polygon_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> triangle_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�triangle assignment>.
\initials{2009.01.06.}

\LOG
\initials{2009.01.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: triangle_assignment@>
{
  @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> cuboid_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�cuboid assignment>.

\LOG
\initials{LDF 2004.08.18.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: cuboid_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> polyhedron_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�polyhedron assignment>.

\LOG
\initials{LDF 2004.08.31.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: polyhedron_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> sphere_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�sphere assignment>.

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: sphere_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> ellipsoid_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�ellipsoid assignment>.

\LOG
\initials{LDF 2004.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: ellipsoid_assignment@>
{
  @=$$@> = @=$1@>;
}; 


@q **** (4) assignment --> cone_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�cone assignment>.
\initials{LDF 2023.12.10.}

\LOG
\initials{LDF 2023.12.10.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: cone_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> cylinder_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�cylinder assignment>.
\initials{LDF 2024.12.02.}

\LOG
\initials{LDF 2024.12.02.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: cylinder_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> prismatoid_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�prismatoid assignment>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: prismatoid_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> plane_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�plane assignment>.

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: plane_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> line_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�line assignment>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: line_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> star_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�star assignment>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: star_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> constellation_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�constellation assignment>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: constellation_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> planet_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�planet assignment>.

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: planet_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q **** (4) assignment --> newwrite_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�newwrite assignment>.

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: newwrite_assignment@>
{
      @=$$@> = @=$1@>;
}; 

@q ** (2) Vector types.@> 
@*1 Vector types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q *** (3) Non-|Shape| types.@> 
@*2 Non-{\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) assignment --> boolean_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�boolean vector assignment>.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=assignment: boolean_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q ** (2) numeric_vector_assignment.@> 
@*1 \�numeric vector assignment>.
\initials{LDF 2005.08.31.}

\LOG
\initials{LDF 2005.08.31.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> numeric_vector_assignment@>

@q *** (3) assignment: numeric_vector_assignment.@> 
@*2 \�assignment> $\longrightarrow$ \�numeric vector assignment>.
\initials{LDF 2005.08.31.}

\LOG
\initials{LDF 2005.08.31.}
Added this rule.  I believe it existed before, but I had gotten rid of
it in the course of debugging the rule
\�numeric vector assignment>
$\longrightarrow$ \�numeric vector variable> 
\.{ASSIGN} \�numeric vector expression>.      
\ENDLOG

@<Define rules@>=
@=assignment: numeric_vector_assignment@>  
{
   @=$$@> = @=$1@>;
};

@q *** (3) numeric_vector_assignment --> numeric_vector_variable @>  
@q *** (3) ASSIGN numeric_vector_expression.                     @> 

@*2 \�numeric vector assignment>
$\longrightarrow$ \�numeric vector variable> 
\.{ASSIGN} \�numeric vector expression>.      
\initials{LDF 2005.08.30.}

It is currently not possible to chain \�numeric vector assignments>.
\initials{LDF 2005.08.31.}

\LOG
\initials{LDF 2005.08.30.}
Added this rule.

\initials{LDF 2005.08.30.}
Changed |PLUS_ASSIGN| to |ASSIGN|.

\initials{LDF 2005.08.31.}
@:BUG FIX@> BUG FIX: 
Rewrote this rule.  There was a bug in the previous version.  
I've managed to get rid of it, but I don't know what it was.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=numeric_vector_assignment: numeric_vector_variable @>  
@=ASSIGN numeric_vector_expression@>@/
{
   typedef Pointer_Vector<real> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ***** (5) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.08.30.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete pv;

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ***** (5) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.08.30.}

\LOG
\initials{LDF 2024.02.19.}
!! BUG FIX:  Now calling |Scan_Parse::clear_vector_func|.  !! TODO:  Do this for other vector-type
assignements.
\ENDLOG 

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {
      
      Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                    entry); 

      int status = Scan_Parse::vector_type_assign<real, real>(
                      static_cast<Scanner_Node>(parameter),
                      entry,
                      pv);          

@q ****** (6) Error handling:                                @> 
@q ****** (6) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.08.30.}

@<Define rules@>=

if (status != 0)
         {
            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ****** (6) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.08.30.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(0);
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ***** (5).@> 

}; 

@q ** (2) ulong_long_vector_assignment.@> 
@*1 \�ulong long vector assignment>.
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> ulong_long_vector_assignment@>

@q *** (3) assignment: ulong_long_vector_assignment.@> 
@*2 \�assignment> $\longrightarrow$ \�ulong long vector assignment>.
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=assignment: ulong_long_vector_assignment@>  
{
   @=$$@> = @=$1@>;
};

@q *** (3) ulong_long_vector_assignment --> ulong_long_vector_variable @>  
@q *** (3) ASSIGN ulong_long_vector_expression.                     @> 

@*2 \�ulong long vector assignment>
$\longrightarrow$ \�ulong long vector variable> 
\.{ASSIGN} \�ulong long vector expression>.      
\initials{LDF 2005.12.05.}

It is currently not possible to chain \�ulong long vector assignments>.
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=ulong_long_vector_assignment: ulong_long_vector_variable @>  
@=ASSIGN ulong_long_vector_expression@>@/
{
   typedef Pointer_Vector<unsigned long long> PV;

   PV* pv = static_cast<PV*>(@=$3@>); 

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

         Scan_Parse::vector_type_assign<unsigned long long, unsigned long long>(
               static_cast<Scanner_Node>(parameter),
               entry,
               pv);          

            delete pv;

            @=$$@> = static_cast<void*>(0);

    }  /* |else|  */
}; 

@q **** (4) assignment --> string_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�string vector assignment>.
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this rule.
\ENDLOG 

@<Define rules@>=

@=assignment: string_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> pen_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�pen vector assignment>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=

@=assignment: pen_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> dash_pattern_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�dash pattern vector assignment>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=

@=assignment: dash_pattern_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> color_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�color vector assignment>.

\LOG
\initials{LDF 2004.08.23.}  
Added this rule.

\initials{LDF 2004.11.05.}
Commented-out this rule.

\initials{LDF 2005.01.07.}
Commented this rule back in.
\ENDLOG 

@<Define rules@>=

@=assignment: color_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> transform_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�transform vector assignment>.
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=

@=assignment: transform_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> focus_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�focus vector assignment>.
\initials{LDF 2005.01.18.}

\LOG
\initials{LDF 2005.01.18.}
Added this rule.
\ENDLOG 

@<Define rules@>=

@=assignment: focus_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q *** (3) |Shape| types.@> 
@*2 {\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) assignment --> point_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�point vector assignment>.

\LOG
\initials{LDF 2004.09.01.}  
Added this rule.

\initials{LDF 2004.11.05.}
Commented-out this rule.

\initials{LDF 2004.11.10.}
Commented this rule back in.
\ENDLOG 

@<Define rules@>=

@=assignment: point_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> bool_point_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�bool-point vector assignment>.

\LOG
\initials{LDF 2004.09.01.}  
Added this rule.

\initials{LDF 2004.11.05.}
Commented-out this rule.

\initials{LDF 2004.11.06.}
Commented this rule in again.
\ENDLOG 

@<Define rules@>=
   
@=assignment: bool_point_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> path_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�path vector assignment>.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2004.12.10.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: path_vector_assignment@>
{
  @=$$@> = @=$1@>;
};
@q **** (4) assignment --> ellipse_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�ellipse vector assignment>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: ellipse_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> circle_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�circle vector assignment>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: circle_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> parabola_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�parabola vector assignment>.
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: parabola_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> hyperbola_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�hyperbola vector assignment>.
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: hyperbola_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> helix_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�helix vector assignment>.
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: helix_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> nurb_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�nurb vector assignment>.
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: nurb_vector_assignment@>
{
   @=$$@> = @=$1@>;
};

@q **** (4) assignment --> triangle_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�triangle vector assignment>.
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: triangle_vector_assignment@>
{
   @=$$@> = @=$1@>;
};

@q **** (4) assignment --> rectangle_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�rectangle vector assignment>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: rectangle_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> polygon_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�polygon vector assignment>.
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: polygon_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> reg_polygon_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�regular polygon vector assignment>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: reg_polygon_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> cuboid_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�cuboid vector assignment>.
\initials{LDF 2004.12.14.}

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: cuboid_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> polyhedron_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�polyhedron vector assignment>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: polyhedron_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> sphere_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�sphere vector assignment>.
\initials{LDF 2005.10.30.}

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: sphere_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> plane_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�plane vector assignment>.
\initials{LDF 2005.10.30.}

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: plane_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> line_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�line vector assignment>.
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: line_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> star_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�star vector assignment>.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: star_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> constellation_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�constellation vector assignment>.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: constellation_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> planet_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�planet vector assignment>.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: planet_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> glyph_vector_assignment.@>
@*3 \�assignment> $\longrightarrow$ \�glyph vector assignment>.
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this rule.
\ENDLOG 

@<Define rules@>=
  
@=assignment: glyph_vector_assignment@>
{
  @=$$@> = @=$1@>;
};

@q **** (4) assignment --> assignment_command.@>
@*3 \�assignment> $\longrightarrow$ 
\�assignment command>.

\LOG
\initials{LDF 2004.09.05.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=assignment: assignment_command@>
{
      @=$$@> = @=$1@>;
}; 
@q * (1) Type assignments.@> 
@* Type assignments.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q ** (2) Non-vector types.@> 
@*1 Non-vector types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q *** (3) Non-|Shape| types.@> 
@*1 Non-{\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) |boolean_assignment|.@>
@*2 \�boolean assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> boolean_assignment@>

@q ***** (5) boolean_assignment --> boolean_variable := boolean_expression.  @>
@*3 \�boolean assignment> $\longrightarrow$ \�boolean variable>
\.{:=} \�boolean expression>.  

\LOG
\initials{LDF 2004.07.26.}
Removed code from this rule to |Scan_Parse::assign_simple| in
\filename{scanprse.web}.

\initials{LDF 2004.07.26.}
@:BUG FIX@> BUG FIX:  
Now using |int i| rather than |int* int_ptr|.  This rule
failed because no memory had been allocated for |int_ptr|.

\initials{LDF 2004.10.26.}
Changed |int i| to |int* i|, because I've changed the type of 
|boolean_expression| from |int_value| to |pointer_value| in 
\filename{pblexpr.w}.
\ENDLOG

@<Define rules@>= 
 
@=boolean_assignment: boolean_variable ASSIGN boolean_expression@>
{
  bool* b = static_cast<bool*>(@=$3@>);
  
  Int_Void_Ptr ivp = Scan_Parse::assign_simple<bool>(
                        static_cast<Scanner_Node>(parameter), 
                        "bool", 
                        @=$1@>, 
                        b);

  @=$$@> = ivp.v;
 
};

@q ***** (5) boolean_assignment --> boolean_variable := boolean_assignment.  @>

@*3 \�boolean assignment> $\longrightarrow$ \�boolean variable>
\.{:=} \�boolean assignment>.

This rule makes it possible to chain |boolean_assignments|.
\initials{LDF 2004.05.04.}

\LOG
\initials{LDF 2004.05.04.}  
Added this rule.

\initials{LDF 2004.07.27.} 
Removed code from this rule.  Now calling |Scan_Parse::assign_chained<int>|.
\ENDLOG 
  
@ 
@<Define rules@>= 
 
@=boolean_assignment: boolean_variable ASSIGN boolean_assignment@>
{

  Int_Void_Ptr ivp = Scan_Parse::assign_chained<int>(
                        static_cast<Scanner_Node>(parameter),
                        @=$1@>,
                        static_cast<int*>(@=$3@>));

  @=$$@> = ivp.v;

};

@q ***** (5) boolean_assignment --> boolean_variable := bool_point_expression.@>
@*3 \�boolean assignment> $\longrightarrow$ \�boolean variable>
\.{:=} \�bool-point expression>.  

\LOG
\initials{LDF 2004.11.05.}
Added this rule.

\initials{LDF 2004.12.01.}
Now using |bool| instead of |int| for the type of the object 
referenced by |boolean_variables|, |boolean_primaries|,
|secondaries|, |tertiaries|, and |expressions|.
\ENDLOG

@q ****** (6) Definition.@> 
 
@<Define rules@>= 
 
@=boolean_assignment: boolean_variable ASSIGN bool_point_expression@>
{
  Bool_Point* bp = static_cast<Bool_Point*>(@=$3@>);

  int* i; 

  i = new int;

  *i = (bp->b) ? 1 : 0;

  delete bp;

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<int>(
                        static_cast<Scanner_Node>(parameter),
                        "int", 
                        @=$1@>, 
                        i);

  @=$$@> = ivp.v;
 
};

@q **** (4) string_assignment.  @>
@*2 \�string assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> string_assignment@>

@q **** (4) string_assignment --> string_variable := string_expression.  @>

@ \�string assignment> $\longrightarrow$ \�string variable>
\.{:=} \�string expression>.

\LOG
\initials{LDF 2004.05.19.}  Added this rule.

\initials{LDF 2004.07.26.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG 

@<Define rules@>= 
 
@=string_assignment: string_variable ASSIGN string_expression@>
{
  string* string_ptr = static_cast<string*>(@=$3@>); 

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<string>(
                                   static_cast<Scanner_Node>(parameter),
                                   "string",
                                   @=$1@>,
                                   string_ptr);

  @=$$@> = ivp.v;

};

@q **** (4) string_assignment --> string_variable := string_assignment.  @>
@*3 \�string assignment> $\longrightarrow$ \�string variable>
\.{:=} \�string assignment>.

This rule makes it possible to chain |string_assignments|.
\initials{LDF 2004.05.19.}

\LOG
\initials{LDF 2004.05.19.}  
Added this rule.

\initials{LDF 2004.07.27.} 
Removed code from this rule.  Now calling 
|Scan_Parse::assign_chained<string>|.
\ENDLOG 
  
@<Define rules@>= 
 
@=string_assignment: string_variable ASSIGN string_assignment@>
{
  Int_Void_Ptr ivp = assign_chained<string>(static_cast<Scanner_Node>(parameter),
                                            @=$1@>,
					    static_cast<string*>(@=$3@>));

  @=$$@> = ivp.v;
};

@q **** (4) pen_assignment.@>
@*2 \�pen assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> pen_assignment@>

@q **** (4) pen_assignment --> pen_variable := pen_expression.  @>
@*3 \�pen assignment> $\longrightarrow$ \�pen variable>
\.{:=} \�pen expression>.

\LOG
\initials{LDF 2004.05.21.}  Added this rule.

\initials{LDF 2004.06.07.}  Now calling |create_new<Pen>| rather than
|new Pen|.

\initials{LDF 2004.07.26.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG 

@q **** Definition.@>

@<Define rules@>= 
 
@=pen_assignment: pen_variable ASSIGN pen_expression@>
{
  Pen* pen_ptr = static_cast<Pen*>(@=$3@>); 

  Int_Void_Ptr ivp = assign_simple<Pen>(static_cast<Scanner_Node>(parameter),
                                   "Pen",
                                   @=$1@>,
                                   pen_ptr);

  @=$$@> = ivp.v;

};

@q **** (4) pen_assignment --> pen_variable := pen_assignment.@>
@*3 \�pen assignment> $\longrightarrow$ \�pen variable>
\.{:=} \�pen assignment>.

This rule makes it possible to chain |pen_assignments|.
\initials{LDF 2004.05.21.}

\LOG
\initials{LDF 2004.05.21.}  Added this rule.

\initials{LDF 2004.06.07.}  Now calling |create_new<Pen>| rather than
|new Pen|.

\initials{LDF 2004.07.27.} 
Removed code from this rule.  Now calling 
|Scan_Parse::assign_chained<Pen>|.
\ENDLOG 

@q ***** (5) Definition.@>   

@<Define rules@>= 
 
@=pen_assignment: pen_variable ASSIGN pen_assignment@>
{
  Int_Void_Ptr ivp = assign_chained<Pen>(static_cast<Scanner_Node>(parameter),
                                         @=$1@>,
                                         static_cast<Pen*>(@=$3@>));

  @=$$@> = ivp.v;

};

@q **** (4) dash_pattern_assignment.  @>
@*2 \�dash pattern assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> dash_pattern_assignment@>

@q **** (4) dash_pattern_assignment --> dash_pattern_variable @>
@q **** (4) := dash_pattern_expression.                       @>

@*3 \�dash pattern assignment> $\longrightarrow$ \�dash pattern variable>
\.{:=} \�dash pattern expression>.  

\LOG
\initials{LDF 2004.06.07.}  
Added this rule.

\initials{LDF 2004.07.26.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG 

@<Define rules@>= 
 
@=dash_pattern_assignment: dash_pattern_variable ASSIGN dash_pattern_expression@>
{
  Dash_Pattern* dash_pattern_ptr = static_cast<Dash_Pattern*>(@=$3@>); 

  Int_Void_Ptr ivp = assign_simple<Dash_Pattern>(static_cast<Scanner_Node>(parameter),
                                   "Dash_Pattern",
                                   @=$1@>,
                                   dash_pattern_ptr);

  @=$$@> = ivp.v;

};

@q **** (4) dash_pattern_assignment --> dash_pattern_variable @>
@q **** (4) := dash_pattern_assignment.                       @>

@*3 \�dash pattern assignment> $\longrightarrow$ \�dash pattern variable>
\.{:=} \�dash pattern assignment>.

This rule makes it possible to chain |dash_pattern_assignments|.
\initials{LDF 2004.06.07.}

\LOG
\initials{LDF 2004.06.07.}  Added this rule.

\initials{LDF 2004.07.27.} 
Removed code from this rule.  Now calling 
|Scan_Parse::assign_chained<Dash_Pattern>|.
\ENDLOG 
  
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=dash_pattern_assignment: dash_pattern_variable ASSIGN dash_pattern_assignment@>
{
  Int_Void_Ptr ivp = assign_chained<Dash_Pattern>(static_cast<Scanner_Node>(parameter),
                                                  @=$1@>,
                                                  static_cast<Dash_Pattern*>(@=$3@>));

  @=$$@> = ivp.v;

};

@q **** (4) color_assignment.  @>
@*2 \�color assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> color_assignment@>

@q **** (4) color_assignment --> color_variable := color_expression.@>

@*3 \�color assignment> $\longrightarrow$ \�color variable>
\.{:=} \�color expression>.

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.

\initials{LDF 2004.07.26.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG 

@<Define rules@>= 
 
@=color_assignment: color_variable ASSIGN color_expression@>
{
  Color* color_ptr = static_cast<Color*>(@=$3@>); 

  Int_Void_Ptr ivp = assign_simple<Color>(static_cast<Scanner_Node>(parameter),
                                   "Color",
                                   @=$1@>,
                                   color_ptr);

  @=$$@> = ivp.v;

};

@q **** (4) color_assignment --> color_variable := color_assignment.@>
@*3 \�color assignment> $\longrightarrow$ \�color variable>
\.{:=} \�color assignment>.

This rule makes it possible to chain |color_assignments|.
\initials{LDF 2004.05.21.}

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.

\initials{LDF 2004.07.27.}
Removed code from this rule.  Now calling |Scan_Parse::assign_chained<Color>|.

\Initials{Ldf 2023.07.07.}
!! BUG FIX:  No longer calling |Scan_Parse::assign_chained<Color>| in this rule.
It caused a segmentation fault.  !! TODO:  Find out why.  It appears to have occurred
in |Color::operator=|, but I wasn't able to find out why.
\ENDLOG 
  
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=color_assignment: color_variable ASSIGN color_assignment@>
{
    @<Common declarations for rules@>@;
         
    entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

    Color *c = static_cast<Color*>(entry->object);

    Color *d = static_cast<Color*>(@=$3@>);

    if (d)
    {
       if (c == 0)
          c = new Color;

        *c = *d;
    }

    @=$$@> = static_cast<void*>(c);

};

@q **** (4) color_assignment --> color_variable ASSIGN numeric_list with_type_optional.@>
@*3 \�color assignment> $\longrightarrow$ \�color variable> \.{ASSIGN} \�numeric list>
\<with type optional>.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG 
  
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=color_assignment: color_variable ASSIGN numeric_list with_type_optional@>
{
@q ****** (6) @>

  @<Common declarations for rules@>@;
         
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `color_assignment: color_variable ASSIGN numeric_list with_type_optional'."
                << endl 
                << "`with_type_optional' ($4) == " << @=$4@> << endl;

      cerr << "entry->name == " << entry->name << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>
@
@<Define rules@>=

  Pointer_Vector<real>* w = static_cast<Pointer_Vector<real>*>(@=$3@>); 

  vector<real> rv;

  int ttype = @=$4@>;

  if (w->v.size() == 4)
    ttype = Color::CMYK_COLOR;

  if (ttype == Color::CMYK_COLOR)
  {
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
  }
  else if (ttype == Color::GREYSCALE_COLOR)
  {
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
     rv.push_back(0.0);
  }

  for (vector<real*>::iterator iter = w->v.begin(); iter != w->v.end(); ++iter)
  {
     rv.push_back(**iter);
  }

#if 0 
  cerr << "rv == " << endl;

  for (vector<real>::iterator iter = rv.begin(); iter != rv.end(); ++iter)
  {
     cerr << "*iter == " << *iter << endl;
  }
#endif 

  real red_part     = 0.0;
  real green_part   = 0.0;
  real blue_part    = 0.0;
  real cyan_part    = 0.0;
  real magenta_part = 0.0;
  real yellow_part  = 0.0;
  real black_part   = 0.0;
  real grey_part    = 0.0;

  if (rv.size() > 1)
  {
     red_part = rv[0];
  }
  if (rv.size() > 1)
  {
     green_part = rv[1];
  }
  if (rv.size() > 2)
  {
     blue_part = rv[2];
  }
  if (rv.size() > 3)
  {
     cyan_part = rv[3];
  }
  if (rv.size() > 4)
  {
     magenta_part = rv[4];
  }
  if (rv.size() > 5)
  {
     yellow_part = rv[5];
  }
  if (rv.size() > 6)
  {
     black_part = rv[6];
  }
  if (rv.size() > 7)
  {
     grey_part = rv[7];
  }

@q ****** (6) @>
@
@<Define rules@>=

#if LDF_REAL_FLOAT
  red_part = fmaxf(red_part, 0);
  red_part = fminf(red_part, 1);

  green_part = fmaxf(green_part, 0);
  green_part = fminf(green_part, 1);

  blue_part = fmaxf(blue_part, 0);
  blue_part = fminf(blue_part, 1);

  cyan_part = fmaxf(cyan_part, 0);
  cyan_part = fminf(cyan_part, 1);

  magenta_part = fmaxf(magenta_part, 0);
  magenta_part = fminf(magenta_part, 1);

  yellow_part = fmaxf(yellow_part, 0);
  yellow_part = fminf(yellow_part, 1);

  black_part = fmaxf(black_part, 0);
  black_part = fminf(black_part, 1);

  grey_part = fmaxf(grey_part, 0);
  grey_part = fminf(grey_part, 1);
#else 
  red_part = fmax(red_part, 0);
  red_part = fmin(red_part, 1);

  green_part = fmax(green_part, 0);
  green_part = fmin(green_part, 1);

  blue_part = fmax(blue_part, 0);
  blue_part = fmin(blue_part, 1);

  cyan_part = fmax(cyan_part, 0);
  cyan_part = fmin(cyan_part, 1);

  magenta_part = fmax(magenta_part, 0);
  magenta_part = fmin(magenta_part, 1);

  yellow_part = fmax(yellow_part, 0);
  yellow_part = fmin(yellow_part, 1);

  black_part = fmax(black_part, 0);
  black_part = fmin(black_part, 1);

  grey_part = fmax(grey_part, 0);
  grey_part = fmin(grey_part, 1);
#endif 

@q ****** (6) @>
@
@<Define rules@>=

  Int_Void_Ptr ivp = set_color(static_cast<Scanner_Node>(parameter),
                               entry, 
                               red_part, green_part, blue_part,
                               cyan_part, magenta_part, yellow_part,
                               black_part, grey_part, entry->name, ttype);

@q ****** (6) Error handling:  |Scan_Parse::set_color| failed.@>

@ Error handling:  |Scan_Parse::set_color| failed.
\initials{LDF 2021.11.11.}

@<Define rules@>=

  if (ivp.i != 0) /*   */
  {
    
    @=$$@> = static_cast<void*>(0); 
    
  } /* |if (ivp.i != 0)| (|set_color| failed.)  */

@q ****** (6) |Scan_Parse::set_color| succeeded.@>

@ |Scan_Parse::set_color| succeeded.
\initials{LDF 2021.11.11.}

@<Define rules@>=
   
  else /* (|ivp.i == 0|,  |set_color| succeeded.)  */
  {
      @=$$@> = ivp.v;

  } /* |else| (|ivp.i == 0|, |set_color| succeeded.)  */

@q ****** (6) @>

  delete w;
  w = 0;

};

@q **** (4) color_assignment --> color_variable ASSIGN numeric_expression.@>
@*3 \�color assignment> $\longrightarrow$ \�color variable> \.{ASSIGN} \�numeric expression>.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG 
  
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=color_assignment: color_variable ASSIGN numeric_expression@>
{
@q ****** (6) @>

  @<Common declarations for rules@>@;
         
  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `color_assignment: color_variable ASSIGN numeric_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>
@
@<Define rules@>=

  Int_Void_Ptr ivp = set_color(static_cast<Scanner_Node>(parameter), entry,
                               0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, @=$3@>,
                               entry->name, Color::GREYSCALE_COLOR);

@q ****** (6) Error handling:  |Scan_Parse::set_color| failed.@>

@ Error handling:  |Scan_Parse::set_color| failed.
\initials{LDF 2021.11.11.}

@<Define rules@>=

  if (ivp.i != 0) /*   */
  {
    
    @=$$@> = static_cast<void*>(0); 
    
  } /* |if (ivp.i != 0)| (|set_color| failed.)  */

@q ****** (6) |Scan_Parse::set_color| succeeded.@>

@ |Scan_Parse::set_color| succeeded.
\initials{LDF 2021.11.11.}

@<Define rules@>=
   
  else /* (|ivp.i == 0|,  |set_color| succeeded.)  */
  {
      @=$$@> = ivp.v;

  } /* |else| (|ivp.i == 0|, |set_color| succeeded.)  */

@q ****** (6) @>

};

@q ***** (5) @>

@q **** (4) with_type_optional.  @>

@ \�with type optional>.

\LOG
\initials{LDF 2021.11.11.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_type_optional@>

@q ***** (5) with_type_optional: EMPTY.  @>
@ \�with type optional> $\longrightarrow$ \.{EMPTY}. 

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional:  /* EMPTY */@>@/
{
   @=$$@> = Color::RGB_COLOR;

};

@q ***** (5) with_type_optional: RGB.  @>
@ \�with type optional> $\longrightarrow$ \.{RGB}.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: RGB@>@/
{
   @=$$@> = Color::RGB_COLOR;

};

@q ***** (5) with_type_optional: CMYK.  @>
@ \�with type optional> $\longrightarrow$ \.{CMYK}.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: CMYK@>@/
{
   @=$$@> = Color::CMYK_COLOR;

};

@q ***** (5) with_type_optional: GREYSCALE.  @>
@ \�with type optional> $\longrightarrow$ \.{GREYSCALE}.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: GREYSCALE@>@/
{
   @=$$@> = Color::GREYSCALE_COLOR;

};

@q ***** (5) with_type_optional: WITH_TYPE RGB.  @>
@ \�with type optional> $\longrightarrow$ \.{WITH\_TYPE} \.{RGB}.  @>

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: WITH_TYPE RGB@>@/
{
   @=$$@> = Color::RGB_COLOR;

};

@q ***** (5) with_type_optional: WITH_TYPE CMYK.  @>
@ \�with type optional> $\longrightarrow$ \.{WITH\_TYPE} \.{CMYK}.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: WITH_TYPE CMYK@>@/
{
   @=$$@> = Color::CMYK_COLOR;

};

@q ***** (5) with_type_optional: WITH_TYPE GREYSCALE.  @>
@ \�with type optional> $\longrightarrow$ \.{WITH\_TYPE} \.{GREYSCALE}.

\LOG
\initials{LDF 2021.11.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=with_type_optional: WITH_TYPE GREYSCALE@>@/
{
   @=$$@> = Color::GREYSCALE_COLOR;
};



@q **** (4) numeric_assignment.  @>
@*2 \�numeric assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_assignment@>

@q **** (4) numeric_assignment --> numeric_variable := numeric_expression.@>

@*3 \�numeric assignment> $\longrightarrow$ \�numeric variable>
\.{:=} \�numeric expression>.  

\LOG
\initials{LDF 2004.07.26.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.

\initials{LDF 2004.10.26.}
Now calling |Scan_Parse::assign_real| instead of
|Scan_Parse::assign_simple|.

\initials{LDF 2004.10.27.}
Now calling |Scan_Parse::assign_simple<real>| again 
instead of |Scan_Parse::assign_real|.
\ENDLOG 

@<Define rules@>= 
 
@=numeric_assignment: numeric_variable ASSIGN numeric_expression@>@/
{
  real r = @=$3@>; 

  Int_Void_Ptr ivp = assign_simple<real>(static_cast<Scanner_Node>(parameter),
                                         "real", 
                                         @=$1@>,
                                         &r,
                                         false);    
  @=$$@> = ivp.v;

};

@q **** (4) numeric_assignment --> numeric_variable := numeric_vector_expression.@>

@*3 \�numeric assignment> $\longrightarrow$ \�numeric variable>
\.{:=} \�numeric vector expression>.  
\initials{LDF 2006.01.23.}

If the \�numeric vector expression> is non-null and has at least one element, 
its first element is used for the assignment.  Otherwise, |INVALID_REAL| is used.
\initials{LDF 2006.01.23.}

\LOG
\initials{LDF 2006.01.23.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=numeric_assignment: numeric_variable ASSIGN numeric_vector_expression@>@/
{
    real r;

    Pointer_Vector<real>* pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

    if (pv == static_cast<Pointer_Vector<real>*>(0)  || pv->ctr <= 0)
       {
           r = INVALID_REAL;
       }
    else
       {
           r = *(pv->v[0]);
           delete pv;
           pv = 0;
       }

    Int_Void_Ptr ivp = assign_simple<real>(static_cast<Scanner_Node>(parameter),
                                         "real", 
                                         @=$1@>,
                                         &r,
                                         false);    
    @=$$@> = ivp.v;

};

@q **** (4) numeric_assignment --> numeric_variable := numeric_assignment.  @>

@*3 \�numeric assignment> $\longrightarrow$ \�numeric variable>
\.{:=} \�numeric assignment>.

This rule makes it possible to chain |numeric_assignments|.
\initials{LDF 2004.05.04.}

\LOG
\initials{LDF 2004.05.04.}  Added this rule.

\initials{LDF 2004.07.27.}
Removed code from this rule.  Now calling 
|Scan_Parse::assign_chained<real>|.
\ENDLOG 
 
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=numeric_assignment: numeric_variable ASSIGN numeric_assignment@>
{
  Int_Void_Ptr ivp = assign_chained<real>(static_cast<Scanner_Node>(parameter),
                                          @=$1@>,
                                          static_cast<real*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q **** (4) ulong_long_assignment.  @>
@*2 \�ulong long assignment>. 
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ulong_long_assignment@>

@q **** (4) ulong_long_assignment --> ulong_long_variable := ulong_long_expression.@>

@*3 \�ulong long assignment> $\longrightarrow$ \�ulong long variable>
\.{:=} \�ulong long expression>.  
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=ulong_long_assignment: ulong_long_variable ASSIGN ulong_long_expression@>@/
{

  unsigned long long r = @=$3@>; 

  Int_Void_Ptr ivp = assign_simple<unsigned long long>(static_cast<Scanner_Node>(parameter),
                                         "unsigned long long", 
                                         @=$1@>,
                                         &r,
                                         false);    
  @=$$@> = ivp.v;

};

@q **** (4) ulong_long_assignment --> ulong_long_variable := ulong_long_assignment.  @>

@*3 \�ulong long assignment> $\longrightarrow$ \�ulong long variable>
\.{:=} \�ulong long assignment>.

This rule makes it possible to chain |ulong_long_assignments|.
\initials{LDF 2005.12.05.}

\LOG
\initials{LDF 2005.12.05.}
Added this rule.
\ENDLOG
 
@q ***** (5) Definition.@> 
 
@<Define rules@>= 
 
@=ulong_long_assignment: ulong_long_variable ASSIGN ulong_long_assignment@>
{

  Int_Void_Ptr ivp = assign_chained<unsigned long long>(static_cast<Scanner_Node>(parameter),
                                          @=$1@>,
                                          static_cast<unsigned long long*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q **** (4) transform_assignment.  @>
@*2 \�transform assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> transform_assignment@>@/

@q **** (4) transform_assignment --> transform_variable @>   
@q **** (4) := transform_expression.                    @>

@*3 \�transform assignment> $\longrightarrow$ \�transform variable> 
\.{:=} \�transform expression>. 

\LOG
\initials{LDF 2004.05.06.}  
Added this rule.

\initials{LDF 2004.07.27.}
Removed code from this rule.  Now calling 
|Scan_Parse::assign_simple_1<Transform>|.

\initials{LDF 2004.07.28.}
Now calling |Scan_Parse::assign_simple| rather than 
|Scan_Parse::assign_simple_1<Transform>|, since I've gotten rid of 
the latter function. 

\initials{LDF 2004.11.05.}
@:BUG FIX@> BUG FIX:  No longer deleting |transform_expression|.
\ENDLOG 

@<Define rules@>= 
 
@=transform_assignment: transform_variable ASSIGN transform_expression@>
{

  Transform* t = static_cast<Transform*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Transform>(
                                   static_cast<Scanner_Node>(parameter),
                                   "Transform",
                                   @=$1@>,
                                   t);

  @=$$@> = ivp.v;

};

@q **** (4) picture_assignment.@>
@*2 \�picture assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> picture_assignment@>

@q **** (4) picture_assignment --> picture_variable := picture_expression.@>
@*3 \�picture assignment> $\longrightarrow$ \�picture variable>
\.{:=} \�picture expression>.

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.

\initials{LDF 2004.06.30.}
Changed the code in this rule to account for the fact that 
|picture_expression| is now a |Id_Map_Entry_Node|, cast to |void*|.
Formerly, it was a |Picture*|, cast to |void*|.

\initials{LDF 2004.06.30.}
Now locking and unlocking |entry->mutex|, |entry_0->mutex|, |s->mutex|, 
and |t->mutex|.

\initials{LDF 2004.08.08.}
Removed code from this rule and put it in 
|Scan_Parse::assign_picture_simple|.

\initials{LDF 2004.12.02.}
Rewrote this rule so that |picture_expression| is copied and the copy 
passed to |Scan_Parse::assign_picture_simple|.

\initials{LDF 2004.12.03.}
Reversed the change made in the last version, i.e.,
|picture_expression| itself is again passed to 
|Scan_Parse::assign_picture_simple|. 
\ENDLOG 

@q ***** (5) Definition.@> 

@<Define rules@>=
 
@=picture_assignment: picture_variable ASSIGN picture_expression@>
{

  Int_Void_Ptr ivp
    = assign_picture_simple(static_cast<Scanner_Node>(parameter),
                            static_cast<Id_Map_Entry_Node>($1),
                            static_cast<Id_Map_Entry_Node>($3));

  @=$$@> = static_cast<void*>(ivp.v); 

};

@q **** (4) picture_assignment --> picture_variable := picture_assignment.@>
@*3 \�picture assignment> $\longrightarrow$ \�picture variable>
\.{:=} \�picture assignment>.

This rule makes it possible to chain |picture_assignments|.
\initials{LDF 2004.05.21.}

\LOG
\initials{LDF 2004.05.21.}  
Added this rule.

\initials{LDF 2004.06.30.}
Changed the code in this rule to account for the fact that 
|picture_assignment| is now a |Id_Map_Entry_Node|, cast to |void*|.
Formerly, it was a |Picture*|, cast to |void*|.

\initials{LDF 2004.06.30.}
Now locking and unlocking |entry->mutex|, |entry_0->mutex|, |s->mutex|, 
and |t->mutex|.

\initials{LDF 2004.08.13.}
Removed code from this rule to the function 
|Scan_Parse::assign_picture_chained| in \filename{scanprse.web}.
\ENDLOG 

@q ***** (5) Definition.@>   
 
@<Define rules@>= 
 
@=picture_assignment: picture_variable ASSIGN picture_assignment@>
{

  Int_Void_Ptr ivp
    = assign_picture_chained(static_cast<Scanner_Node>(parameter),
                             static_cast<Id_Map_Entry_Node>($1),
                             static_cast<Id_Map_Entry_Node>($3));

  @=$$@> = static_cast<void*>(ivp.v); 

};

@q **** (4) focus_assignment.  @>
@*2 \�focus assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> focus_assignment@>

@q **** (4) focus_assignment --> focus_variable := focus_expression.@>

@*3 \�focus assignment> $\longrightarrow$ \�focus variable>
\.{:=} \�focus expression>.  

\LOG
\initials{LDF 2004.06.08.}  Added this rule.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG 

@<Define rules@>= 
 
@=focus_assignment: focus_variable ASSIGN focus_expression@>
{

  Focus* f = static_cast<Focus*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Focus>(static_cast<Scanner_Node>(parameter),
                                   "Focus",
                                   @=$1@>,
                                   f);

  @=$$@> = ivp.v;

};

@q **** (4) focus_assignment --> focus_variable := focus_assignment.  @>

@*3 \�focus assignment> $\longrightarrow$ \�focus variable>
\.{:=} \�focus assignment>.

This rule makes it possible to chain |focus_assignments|.
\initials{LDF 2004.06.08.}

\LOG
\initials{LDF 2004.06.08.}  Added this rule.

\initials{LDF 2004.08.08.}
Now using |assign_chained| in this rule.
\ENDLOG 
  
@ 
@<Define rules@>= 
 
@=focus_assignment: focus_variable ASSIGN focus_assignment@>
{

    Int_Void_Ptr ivp = assign_chained<Focus>(static_cast<Scanner_Node>(parameter),
                                             @=$1@>,
                                             static_cast<Focus*>(@=$3@>));

    @=$$@> = ivp.v;

};

@q **** (4) macro_assignment.  @>
@*2 \�macro assignment>. 
\initials{LDF 2004.12.30.}

\LOG
\initials{LDF 2004.12.30.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> macro_assignment@>

@q **** (4) macro_assignment --> macro_variable := macro_variable.@>

@*3 \�macro assignment> $\longrightarrow$ \�macro variable>
\.{ASSIGN} \�macro expression>.  
\initials{LDF 2004.12.30.}

\LOG
\initials{LDF 2004.12.30.}
Added this rule.
\ENDLOG 

@q ***** (5) Definition.@> 

@<Define rules@>= 
 
@=macro_assignment: macro_variable ASSIGN macro_variable@>
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ****** (6) Error handling:  |entry == static_cast<Id_Map_Entry_Node>(0)|.@> 

@ Error handling:  |entry == static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.30.}

@<Define rules@>=

   if (entry == static_cast<Id_Map_Entry_Node>(0))
      {

          @=$$@> = static_cast<void*>(0);
        
      }  /* |if (entry == static_cast<Id_Map_Entry_Node>(0))| */        

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@> 

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.30.}

@<Define rules@>=
 
   else  /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
      {
               
         Id_Map_Entry_Node entry_0 = static_cast<Id_Map_Entry_Node>(@=$3@>);

@q ******* (7) Error handling:                                                         @>
@q ******* (7) |entry_0 == static_cast<Id_Map_Entry_Node>(0) || entry_0->object == 0|. @>
@ Error handling:  |entry_0 == 0 || entry_0->object == 0|.
\initials{LDF 2004.12.30.}

@<Define rules@>=

         if (   entry_0 == static_cast<Id_Map_Entry_Node>(0) 
             || entry_0->object == static_cast<Id_Map_Entry_Node>(0))
            {

                @=$$@> = static_cast<void*>(0);

            }  /* |if| */

@q ******* (7) |entry_0 != 0 && entry_0->object != 0|.@> 

@ |entry_0 != 0 && entry_0->object != 0|.
\initials{LDF 2004.12.30.}

@<Define rules@>=

      else  /*  |entry_0 != 0 && entry_0->object != 0|  */
         {

            Definition_Info_Node d;

@q ******** (8) |entry->object == 0|.@> 

@ |entry->object == 0|.
\initials{LDF 2004.12.30.}

@<Define rules@>=

            if (entry->object == static_cast<Id_Map_Entry_Node>(0))
               {

                  d = new Definition_Info_Type;

                  entry->object = static_cast<void*>(d); 

               }  /* |if (entry->object == static_cast<Id_Map_Entry_Node>(0))|  */

@q ******** (8) |entry->object != static_cast<void*>(0)|.@> 

@ |entry->object == static_cast<void*>(0)|.
\initials{LDF 2004.12.30.}

@<Define rules@>=

            else  /* |entry->object != static_cast<void*>(0)|  */
               {
                   d = static_cast<Definition_Info_Node>(entry->object);

               }  /* |else| (|entry->object != static_cast<void*>(0)|)  */

@q ******** (8) @> 

            *d = *(static_cast<Definition_Info_Node>(entry_0->object));  

            @=$$@> = static_cast<void*>(entry); 

         }  /* |else| ( |entry_0 != 0 && entry_0->object != 0|)  */

@q ******* (7) @> 

   }  /* |else| (|entry != 0|)  */

@q ****** (6).@> 

};

@q *** (3) Shape types.@> 
@*1 {\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) point_assignment.  @>
@*2 \�point assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> point_assignment@>@/

@q ***** (5) point_assignment --> point_variable := point_expression.@>
@*3 \�point assignment> $\longrightarrow$ \�point variable> \.{:=} 
\�point expression>. 

\LOG
\initials{LDF 2004.05.04.}  Added this rule.

\initials{LDF 2004.07.27.}
Removed code from this rule.  Now calling 
|Scan_Parse::assign_simple_1<Point>|.

\initials{LDF 2004.07.28.}
Now calling |Scan_Parse::assign_simple| rather than 
|Scan_Parse::assign_simple_1<Transform>|, since I've gotten rid of 
the latter function. 
\ENDLOG 

@<Define rules@>= 
 
@=point_assignment: point_variable ASSIGN point_expression@>
{

  Point* p = static_cast<Point*>(@=$3@>); 
  
  Int_Void_Ptr ivp = assign_simple<Point>(static_cast<Scanner_Node>(parameter),
                                   "Point",
                                   @=$1@>,
                                   p);

  @=$$@> = ivp.v;

};

@q ***** (5) point_assignment --> point_variable ASSIGN bool_point_expression.@>
@*3 \�point assignment> $\longrightarrow$ \�point variable> \.{ASSIGN} 
\�bool-point expression>. 

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>= 
 
@=point_assignment: point_variable ASSIGN bool_point_expression@>
{

  Bool_Point* bp = static_cast<Bool_Point*>(@=$3@>); 
  
  Point* p;

  p = create_new<Point>(bp->pt);

  Int_Void_Ptr ivp = assign_simple<Point>(static_cast<Scanner_Node>(parameter),
                                          "Point",
                                          @=$1@>,
                                          p);

  @=$$@> = ivp.v;

};

@q ***** (5) point_assignment --> point_variable := numeric_expression.@>

@*3 \�point assignment> $\longrightarrow$ \�point variable> 
\.{:=} \�numeric expression>. 

We can't use |@<Common code for variable assignments@>| in this
rule, because that section assumes that the |expression| on the
right-hand side of the assignment is of the same type as the
|variable| on the left-hand side.   
\initials{LDF 2004.05.05.}

\LOG
\initials{LDF 2004.05.03.}  
Added this rule.

\initials{LDF 2004.05.05.}  
Added error handling for the cases that |entry == 0|.

\initials{LDF 2004.08.13.}
Removed code from this rule to the function 
|Scan_Parse::assign_point_numeric| in \filename{scanprse.web}.
\ENDLOG 

@q ***** (5)  @>
@<Define rules@>=
 
@=point_assignment: point_variable ASSIGN numeric_expression@>
{

  Int_Void_Ptr ivp
    = assign_point_numeric(static_cast<Scanner_Node>(parameter),
                           static_cast<Id_Map_Entry_Node>(@=$1@>),
                           @=$3@>);

  @=$$@> = ivp.v;

};

@q ***** (5) point_assignment --> point_variable := point_assignment.  @>

@*3 \�point assignment> $\longrightarrow$ \�point variable>
\.{:=} \�point assignment>.

This rule makes it possible to chain |point_assignments|.
\initials{LDF 2004.08.08.}

\LOG
\initials{LDF 2004.08.08.}  
Tried to add this rule.  I wasn't able to get it to work, though.

\initials{LDF 2004.10.25.}
Got this rule to work.
\ENDLOG 

@q ***** (5) Definition.@> 
 
@<Define rules@>=
@=point_assignment: point_variable ASSIGN point_assignment@>
{

    Int_Void_Ptr ivp = assign_chained<Point>(static_cast<Scanner_Node>(parameter),
                                             @=$1@>,
                                             static_cast<Point*>(@=$3@>));

    @=$$@> = ivp.v;

};

@q **** (4) bool_point_assignment.@>
@*2 \�bool-point assignment>. 
\initials{LDF 2004.09.01.}

\LOG
\initials{LDF 2004.09.01.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> bool_point_assignment@>

@q ***** (5) bool_point_assignment --> bool_point_variable @>
@q ***** (5) ASSIGN bool_point_expression.                 @>
                                                            
@*3 \�bool-point assignment> $\longrightarrow$ 
\�bool-point variable> \.{ASSIGN} \�bool-point expression>.

\LOG
\initials{LDF 2004.09.01.}
Added this rule.

\initials{LDF 2004.11.03.}
Changed this rule from 
``\�bool-point assignment> $\longrightarrow$ 
\�bool-point variable> \.{ASSIGN} \.{LEFT\_PARENTHESIS} 
\�boolean expression> \.{COMMA} \�point expression>
\.{RIGHT\_PARENTHESIS}''
to 
``\�bool-point assignment> $\longrightarrow$ 
\�bool-point variable> \.{ASSIGN} \�bool-point expression>''.
\ENDLOG

@q ****** (6) Definition.@>

@<Define rules@>=
 
@=bool_point_assignment: bool_point_variable ASSIGN @>@/
@=bool_point_expression@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q ******* (7) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ******* (7) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != 0|.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

         Bool_Point* bp = static_cast<Bool_Point*>(entry->object);

         Bool_Point* bpe = static_cast<Bool_Point*>(@=$3@>); 

@q ******** (8) |bp == 0|.@> 

@ |bp == 0|.
\initials{LDF 2004.11.03.}

@<Define rules@>=

  if (bp == static_cast<Bool_Point*>(0))
    {
      
      bp = bpe;
      
      @=$$@> = entry->object = static_cast<void*>(bp);

} /* |if (bp == static_cast<Bool_Point*>(0))|  */

@q ******** (8) |bp != static_cast<Bool_Point*>(0)|.@> 

@ |bp != static_cast<Bool_Point*>(0)|.
\initials{LDF 2004.11.03.}

@<Define rules@>=

   else  /* |bp != static_cast<Bool_Point*>(0)|  */
     {
        bp->b  = bpe->b,
        bp->pt = bpe->pt;

        delete bpe;

        @=$$@> = entry->object;

     }   /* |else| (|bp != static_cast<Bool_Point*>(0)|)  */

   } /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

};

@q ***** (5) bool_point_assignment --> bool_point_variable @>
@q ***** (5) := boolean_expression.                        @>
                                                            
@*3 \�bool-point assignment> $\longrightarrow$ 
\�bool-point variable> \.{ASSIGN} \�boolean expression>.

\LOG
\initials{LDF 2004.11.05.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@>

@<Define rules@>=
 
@=bool_point_assignment: bool_point_variable ASSIGN @>@/
@=boolean_expression@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  bool* b = static_cast<bool*>(@=$3@>); 
  
@q ****** (6) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete b;

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

Bool_Point* bp = static_cast<Bool_Point*>(entry->object);

@q ******* (7) |bp == 0|.@> 

@ |bp == 0|.
\initials{LDF 2004.11.03.}

@<Define rules@>=

  if (bp == static_cast<Bool_Point*>(0))
    {
      
        bp = new Bool_Point;

    } /* |if (bp == 0)|  */

@q ******* (7) @> 

     bp->b = *b;
      
     delete b;

     @=$$@> = entry->object = static_cast<void*>(bp); 

   } /* |else| (|entry != 0|)  */

@q ****** (6).@> 

};

@q ***** (5) bool_point_assignment --> bool_point_variable @>
@q ***** (5) := point_expression.                          @>
                                                            
@*3 \�bool-point assignment> $\longrightarrow$ 
\�bool-point variable> \.{ASSIGN} \�point expression>.

\LOG
\initials{LDF 2004.11.05.}
Added this rule.

\initials{LDF 2004.12.01.}
Now using |bool| instead of |int| for the type of the object 
referenced by |boolean_variables|, |boolean_primaries|,
|secondaries|, |tertiaries|, and |expressions|.
\ENDLOG

@q ***** (5) Definition.@>

@<Define rules@>=
 
@=bool_point_assignment: bool_point_variable ASSIGN @>@/
@=point_expression@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  Point* p = static_cast<Point*>(@=$3@>); 
  
@q ****** (6) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      delete p;

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2004.09.01.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

        Bool_Point* bp = static_cast<Bool_Point*>(entry->object);

@q ******* (7) |bp == 0|.@> 

@ |bp == 0|.
\initials{LDF 2004.11.03.}

@<Define rules@>=

  if (bp == static_cast<Bool_Point*>(0))
    {
      
       bp = new Bool_Point;

    } /* |if (bp == 0)|  */

@q ******* (7) @> 

     bp->pt = *p;
      
     delete p;

     @=$$@> = entry->object = static_cast<void*>(bp); 

   } /* |else| (|entry != 0|)  */

@q ****** (6).@> 

};

@q **** (4) path_assignment.  @>
@*3 \�path assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> path_assignment@>@/

@q ***** (5) path_assignment --> path_variable := path_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{ASSIGN} \�path expression>. 

\LOG
\initials{LDF 2004.05.06.}
Added this rule.

\initials{LDF 2004.05.13.}  
Replaced the dummy code in this rule with 
code that works. 

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.

\initials{LDF 2004.12.10.}
Changed \�path expression> to \�path-like expression>. 

\initials{LDF 2005.10.24.}
Changed \�path-like expression> back to \�path expression>.
Removed all debugging code. 
\ENDLOG 

@<Define rules@>= 
@=path_assignment: path_variable ASSIGN path_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);
  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := sinewave_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{ASSIGN} \�sinewave expression>. 

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=path_assignment: path_variable ASSIGN sinewave_expression@>
{
  
  Sinewave* s = static_cast<Sinewave*>(@=$3@>);

  Path *p = create_new<Path>(0);

  *p = *static_cast<Path*>(s);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);
  if (s)
  {
     delete s;
     s = 0;
  }

  @=$$@> = ivp.v;

};

@q **** (4) path_assignment --> path_variable APPEND path_expression.  @>

@*3 \�path assignment> $\longrightarrow$ \�path variable> \.{APPEND} 
\�path expression>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=path_assignment: path_variable APPEND path_expression@>@/
{
#if 0 

/* !!START HERE:  LDF 2022.05.02.  */ 

  @<Common declarations for rules@>@; 

 #if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
     {

          cerr_strm << thread_name << "*** Parser: `path_assignment --> "
                    << "path_variable APPEND path_expression'.";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@; 

  Path *p = static_cast<Path*>(@=$3@>);

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry && entry->object && p)
  {
     Path *q = static_cast<Path*>(entry->object);
  
     Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                            "Path",
                                            @=$1@>,
                                            p);

  @=$$@> = ivp.v;

     q->append(*p, "..", true);

     delete p; 
     p = 0;
  } 

@q ***** (5)  Set |$$| to |static_cast<void*>(p)| and exit rule.@> 

@ Set |@=$$@>| to |static_cast<void*>(p)| and exit rule.
\initials{LDF 2022.05.02.}

@<Define rules@>=

  @=$$@> = static_cast<void*>(0);

#endif 

};

@q ***** (5) path_assignment --> path_variable := circle_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{ASSIGN} \�circle expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN circle_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := ellipse_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�ellipse expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN ellipse_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := superellipse_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�superellipse expression>. 
\initials{LDF 2022.04.27.}

\LOG
\initials{LDF 2022.04.27.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN superellipse_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := rectangle_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�rectangle expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN rectangle_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := triangle_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�triangle expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN triangle_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := polygon_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�polygon expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN polygon_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := reg_polygon_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable> 
\.{:=} \�regular polygon expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN reg_polygon_expression@>
{
  
  Path* p = static_cast<Path*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                         "Path",
                                         @=$1@>,
                                         p);

  @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := path_assignment.@>

@*3 \�path assignment> $\longrightarrow$ \�path variable>
\.{:=} \�path assignment>.

This rule makes it possible to chain |path_assignments|.
\initials{LDF 2004.08.08.}

\LOG
\initials{LDF 2004.08.08.}  
Tried to add this rule, but I couldn't get it to work.

\initials{LDF 2004.10.27.}
Got this rule to work.
\ENDLOG 

@<Define rules@>=
@=path_assignment: path_variable ASSIGN path_assignment@>@/
{

    Int_Void_Ptr ivp = assign_chained<Path>(static_cast<Scanner_Node>(parameter),
                                            @=$1@>,
                                            static_cast<Path*>(@=$3@>));

    @=$$@> = ivp.v;

};

@q ***** (5) path_assignment --> path_variable := path_vector_expression.@>   

@*4 \�path assignment> $\longrightarrow$ \�path variable>. 
\.{ASSIGN} \�path vector expression>. 

\LOG
\initials{LDF 2022.04.22.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=path_assignment: path_variable ASSIGN path_vector_expression@>
{
@q ****** (6) @>

    @<Common declarations for rules@>@;

    Pointer_Vector<Path, Path> *pv = static_cast<Pointer_Vector<Path, Path>*>(@=$3@>);

    Path *p = 0;

    if (pv != 0 && pv->v.size() > 0 && pv->v[0] != 0)
    {
       p = pv->v[0];
       pv->v[0] = 0;

       Int_Void_Ptr ivp = assign_simple<Path>(static_cast<Scanner_Node>(parameter),
                                              "Path",
                                              @=$1@>,
                                              p);
       @=$$@> = ivp.v;

    }
    else
    {
       cerr_strm << "WARNING!  In parser, rule `path_assignment: path_variable ASSIGN path_vector_expression':"
                 << endl;

       if (pv == 0)
          cerr_strm << "path vector is NULL." << endl;
       else if (pv->v.size() == 0 || (pv->v.size() > 0 && pv->v[0] == 0))
          cerr_strm << "path_vector is empty." << endl;

       cerr_strm << "Can't assign to path.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

       @=$$@> = static_cast<void*>(0);
    }

@q ****** (6) @>
    if (pv)
    {
       delete pv;
       pv = 0;
    }
};

@q **** (4) glyph_assignment.  @>
@*3 \�glyph assignment>. 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> glyph_assignment@>@/

@q ***** (5) glyph_assignment --> glyph_variable += path_expression.@>   

@*4 \�glyph assignment> $\longrightarrow$ \�glyph variable> 
\.{ASSIGN} \�path expression>. 

\LOG
\initials{LDF 2022.01.16.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=glyph_assignment: glyph_variable PLUS_ASSIGN path_expression@>
{
   if (@=$1@> == 0)
      cerr << "`glyph_variable' is NULL." << endl;
   else 
      cerr << "`glyph_variable' is non-NULL." << endl;

   Glyph *g = create_new<Glyph>(0);

   *g += static_cast<Path*>(@=$3@>);

   g->show("In parser rule:  g:");    

   Int_Void_Ptr ivp = assign_simple<Glyph>(static_cast<Scanner_Node>(parameter),
                                           "Glyph",
                                           @=$1@>,
                                           g);

  @=$$@> = ivp.v;

};

@q ***** (5) glyph_assignment --> glyph_variable ASSIGN glyph_expression.@>   

@*4 \�glyph assignment> $\longrightarrow$ \�glyph variable> 
\.{ASSIGN} \�glyph expression>. 

\LOG
\initials{LDF 2022.01.16.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=glyph_assignment: glyph_variable ASSIGN glyph_expression@>
{

   @<Common declarations for rules@>@;

#if DEBUG_COMPILE

   DEBUG = false; /* |true|  */

   if (DEBUG)
   { 
       cerr << "*** Parser: `glyph_assignment: glyph_variable ASSIGN glyph_expression'."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  Glyph* g = static_cast<Glyph*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Glyph>(static_cast<Scanner_Node>(parameter),
                                          "Glyph",
                                           @=$1@>,
                                           g);

  @=$$@> = ivp.v;

};

@q **** (4) catenary_assignment.  @>
@*3 \�catenary assignment>. 
\initials{LDF 2023.06.20.}

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> catenary_assignment@>@/

@q ***** (5) catenary_assignment --> catenary_variable ASSIGN catenary_expression.@>   

@*4 \�catenary assignment> $\longrightarrow$ \�catenary variable> 
\.{ASSIGN} \�catenary expression>. 

\LOG
\initials{LDF 2023.06.20.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
 
@=catenary_assignment: catenary_variable ASSIGN catenary_expression@>
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE

   DEBUG = true; /* |false|  */

   if (DEBUG)
   { 
       cerr << "*** Parser: `catenary_assignment: catenary_variable ASSIGN catenary_expression'."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

#if 0 
  Catenary* c = static_cast<Catenary*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Catenary>(static_cast<Scanner_Node>(parameter),
                                          "Catenary",
                                           @=$1@>,
                                           c);

  @=$$@> = ivp.v;
#endif 

  @=$$@> = 0;

};


@q **** (4) ellipse_assignment.  @>
@*2 \�ellipse assignment>. 

\LOG
\initials{LDF 2004.06.29.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipse_assignment@>@/

@q ***** (5) ellipse_assignment --> ellipse_variable @>
@q ***** (5) := ellipse_expression.                  @>   

@*3 \�ellipse assignment> $\longrightarrow$ \�ellipse variable> 
\.{ASSIGN} \�ellipse expression>. 

\LOG
\initials{LDF 2004.06.29.}
Added this rule.

\initials{LDF 2004.06.29.}
Changed |ellipse_expression| to |ellipse_like_expression|.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.

\initials{LDF 2005.10.24.}
Changed |ellipse_like_expression| back to |ellipse_expression|.
Removed all debugging code.
\ENDLOG

@<Define rules@>= 
 
@=ellipse_assignment: ellipse_variable ASSIGN ellipse_expression@>
{

  Ellipse* f = static_cast<Ellipse*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Ellipse>(static_cast<Scanner_Node>(parameter),
                                            "Ellipse",
                                            @=$1@>,
                                            f);

  @=$$@> = ivp.v;

};

@q ***** (5) ellipse_assignment --> ellipse_variable @>
@q ***** (5) := circle_expression.                   @>   

@*3 \�ellipse assignment> $\longrightarrow$ \�ellipse variable> 
\.{ASSIGN} \�circle expression>. 

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=ellipse_assignment: ellipse_variable ASSIGN circle_expression@>
{

  Ellipse* f = static_cast<Ellipse*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Ellipse>(static_cast<Scanner_Node>(parameter),
                                            "Ellipse",
                                            @=$1@>,
                                            f);

  @=$$@> = ivp.v;

};

@q **** (4) circle_assignment.@>
@*3 \�circle assignment>. 
\initials{LDF 2004.06.17.}  

\LOG
\initials{LDF 2004.06.17.}  
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_assignment@>@/

@q ***** (5) circle_assignment --> circle_variable := circle_expression.@>   
@*4 \�circle assignment> $\longrightarrow$ \�circle variable> 
\.{:=} \�circle expression>. 
\initials{LDF 2004.06.17.}

\LOG
\initials{LDF 2004.06.17.}
Added this rule.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>= 
 
@=circle_assignment: circle_variable ASSIGN circle_expression@>
{

  Circle* f = static_cast<Circle*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Circle>(static_cast<Scanner_Node>(parameter),
                                   "Circle",
                                   @=$1@>,
                                   f);

  @=$$@> = ivp.v;

};

@q ***** (5) circle_assignment --> circle_variable := path_expression.@>   
@*4 \�circle assignment> $\longrightarrow$ \�circle variable> 
\.{:=} \�path expression>. 
\initials{LDF 2022.09.03.}

\LOG
\initials{LDF 2022.09.03.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>= 
 
@=circle_assignment: circle_variable ASSIGN path_expression @>
{

  Path *p = static_cast<Path*>(@=$3@>);

  Circle *c = create_new<Circle>(0);

  for (vector<Point*>::iterator iter = p->points.begin();
       iter != p->points.end();
       ++iter)
  {
      c->points.push_back(*iter);
  }

  for (vector<Connector_Type*>::iterator iter = p->connector_type_vector.begin();
       iter != p->connector_type_vector.end();
       ++iter)
  {
     c->connector_type_vector.push_back(*iter);
  }

  p->points.clear();
  p->connector_type_vector.clear();

  delete p;
  p = 0;


  Int_Void_Ptr ivp = assign_simple<Circle>(static_cast<Scanner_Node>(parameter),
                                   "Circle",
                                   @=$1@>,
                                   c);
  @=$$@> = ivp.v;

};



@q **** (4) parabola_assignment.@>
@*3 \�parabola assignment>. 
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> parabola_assignment@>@/

@q ***** (5) parabola_assignment --> parabola_variable := parabola_expression.@>   
@*4 \�parabola assignment> $\longrightarrow$ \�parabola variable> 
\.{:=} \�parabola expression>. 
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
 
@=parabola_assignment: parabola_variable ASSIGN parabola_expression@>
{

  Parabola* f = static_cast<Parabola*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Parabola>(static_cast<Scanner_Node>(parameter),
                                                      "Parabola",
                                                      @=$1@>,
                                                      f);

  @=$$@> = ivp.v;

};

@q **** (4) hyperbola_assignment.@>
@*3 \�hyperbola assignment>. 
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> hyperbola_assignment@>@/

@q ***** (5) hyperbola_assignment --> hyperbola_variable := hyperbola_expression.@>   
@*4 \�hyperbola assignment> $\longrightarrow$ \�hyperbola variable> 
\.{:=} \�hyperbola expression>. 
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
 
@=hyperbola_assignment: hyperbola_variable ASSIGN hyperbola_expression@>
{

  Hyperbola* f = static_cast<Hyperbola*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Hyperbola>(static_cast<Scanner_Node>(parameter),
                                                      "Hyperbola",
                                                      @=$1@>,
                                                      f);

  @=$$@> = ivp.v;

};

@q **** (4) helix_assignment.@>
@*3 \�helix assignment>. 
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> helix_assignment@>@/

@q ***** (5) helix_assignment --> helix_variable := helix_expression.@>   
@*4 \�helix assignment> $\longrightarrow$ \�helix variable> 
\.{:=} \�helix expression>. 
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this rule.

\initials{LDF 2005.11.07.}
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
 
@=helix_assignment: helix_variable ASSIGN helix_expression@>
{

  Helix* f = static_cast<Helix*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Helix>(static_cast<Scanner_Node>(parameter),
                                                      "Helix",
                                                      @=$1@>,
                                                      f);

  @=$$@> = ivp.v;

};

@q **** (4) superellipse_assignment.@>
@*3 \�superellipse assignment>. 
\initials{LDF 2022.04.27.}

\LOG
\initials{LDF 2022.04.27.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> superellipse_assignment@>@/

@q ***** (5) superellipse_assignment --> superellipse_variable := superellipse_expression.@>   
@*4 \�superellipse assignment> $\longrightarrow$ \�superellipse variable> 
\.{:=} \�superellipse expression>. 
\initials{LDF 2022.04.27.}

\LOG
\initials{LDF 2022.04.27.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
 
@=superellipse_assignment: superellipse_variable ASSIGN superellipse_expression@>
{

  Superellipse* f = static_cast<Superellipse*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Superellipse>(static_cast<Scanner_Node>(parameter),
                                                      "Superellipse",
                                                      @=$1@>,
                                                      f);

  @=$$@> = ivp.v;

};

@q **** (4) sinewave_assignment.@>
@*3 \�sinewave assignment>. 
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sinewave_assignment@>@/

@q ***** (5) sinewave_assignment --> sinewave_variable := sinewave_expression.@>   
@*4 \�sinewave assignment> $\longrightarrow$ \�sinewave variable> 
\.{:=} \�sinewave expression>. 
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
 
@=sinewave_assignment: sinewave_variable ASSIGN sinewave_expression@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `sinewave_variable ASSIGN sinewave_expression'."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

  Sinewave* s = static_cast<Sinewave*>(@=$3@>);

  Int_Void_Ptr ivp = Scan_Parse::assign_simple<Sinewave>(static_cast<Scanner_Node>(parameter),
                                                      "Sinewave",
                                                      @=$1@>,
                                                      s);
  @=$$@> = ivp.v;

};

@q **** (4) polygon_assignment.  @>
@*3 \�polygon assignment>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> polygon_assignment@>@/

@q **** (4) polygon_assignment --> polygon_variable @>
@q **** (4) ASSIGN polygon_expression.              @>   

@*3 \�polygon assignment> $\longrightarrow$ \�polygon variable> 
\.{ASSIGN} \�polygon expression>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this rule.

\initials{LDF 2005.03.01.}
Changed \�polygon expression> to \�polygon-like expression>.

\initials{LDF 2005.10.24.}
Changed |polygon_like_expression| back to |polygon_expression|.
Removed all debugging code.
\ENDLOG

@<Define rules@>= 
 
@=polygon_assignment: polygon_variable ASSIGN polygon_expression@>
{

  Polygon* f = static_cast<Polygon*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Polygon>(static_cast<Scanner_Node>(parameter),
                                            "Polygon",
                                            @=$1@>,
                                            f);      

  @=$$@> = ivp.v;

};

@q **** (4) polygon_assignment --> polygon_variable @>
@q **** (4) ASSIGN rectangle_expression.            @>   

@*3 \�polygon assignment> $\longrightarrow$ \�polygon variable> 
\.{ASSIGN} \�rectangle expression>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=polygon_assignment: polygon_variable ASSIGN rectangle_expression@>
{

  Polygon* f = static_cast<Polygon*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Polygon>(static_cast<Scanner_Node>(parameter),
                                            "Polygon",
                                            @=$1@>,
                                            f);      

  @=$$@> = ivp.v;

};

@q **** (4) polygon_assignment --> polygon_variable @>
@q **** (4) ASSIGN triangle_expression.         @>   

@*3 \�polygon assignment> $\longrightarrow$ \�polygon variable> 
\.{ASSIGN} \�triangle expression>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=polygon_assignment: polygon_variable ASSIGN triangle_expression@>
{

  Polygon* f = static_cast<Polygon*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Polygon>(static_cast<Scanner_Node>(parameter),
                                            "Polygon",
                                            @=$1@>,
                                            f);      

  @=$$@> = ivp.v;

};

@q **** (4) polygon_assignment --> polygon_variable @>
@q **** (4) ASSIGN reg_polygon_expression.          @>   

@*3 \�polygon assignment> $\longrightarrow$ \�polygon variable> 
\.{ASSIGN} \�regular polygon expression>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.10.24.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=polygon_assignment: polygon_variable ASSIGN reg_polygon_expression@>
{

  Polygon* f = static_cast<Polygon*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Polygon>(static_cast<Scanner_Node>(parameter),
                                            "Polygon",
                                            @=$1@>,
                                            f);      

  @=$$@> = ivp.v;

};

@q **** (4) reg_polygon_assignment.  @>
@*3 \�regular polygon assignment>. 

\LOG
\initials{LDF 2004.07.06.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> reg_polygon_assignment@>@/

@q ***** (5) reg_polygon_assignment --> reg_polygon_variable @>
@q ***** (5) := reg_polygon_expression.  @>   

@*4 \�regular polygon assignment> 
$\longrightarrow$ \�regular polygon variable> 
\.{:=} \�regular polygon expression>. 

\LOG
\initials{LDF 2004.07.06.}
Added this rule.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.
\ENDLOG

@<Define rules@>= 
 
@=reg_polygon_assignment: reg_polygon_variable ASSIGN reg_polygon_expression@>
{

  Reg_Polygon* f = static_cast<Reg_Polygon*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Reg_Polygon>(static_cast<Scanner_Node>(parameter),
                                   "Reg_Polygon",
                                   @=$1@>,
                                   f);

  @=$$@> = ivp.v;

};



@q **** (4) rectangle_assignment.  @>
@*2 \�rectangle assignment>. 

\LOG
\initials{LDF 2004.06.30.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> rectangle_assignment@>@/

@q ***** (5) rectangle_assignment --> rectangle_variable @>
@q ***** (5) := rectangle_expression.                    @>   

@*3 \�rectangle assignment> $\longrightarrow$ \�rectangle variable> 
\.{:=} \�rectangle expression>. 

\LOG
\initials{LDF 2004.06.30.}
Added this rule.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.

\initials{LDF 2005.10.24.}
Changed |rectangle_like_expression| to |rectangle_expression|.
Removed all debugging code.
\ENDLOG

@<Define rules@>= 
 
@=rectangle_assignment: rectangle_variable ASSIGN rectangle_expression@>
{

   Rectangle* r = static_cast<Rectangle*>(@=$3@>);

   Int_Void_Ptr ivp = assign_simple<Rectangle>(static_cast<Scanner_Node>(parameter),
                                               "Rectangle",
                                               @=$1@>,
                                               r);

  @=$$@> = ivp.v;

};

@q ***** (5) rectangle_assignment --> rectangle_variable @>
@q ***** (5) := path_expression.                    @>   

@*3 \�rectangle assignment> $\longrightarrow$ \�rectangle variable> 
\.{:=} \�rectangle expression>. 

\LOG
\initials{LDF 2004.06.30.}
Added this rule.

\initials{LDF 2004.07.28.}
Removed code from this rule.  
Now calling |Scan_Parse::assign_simple|.

\initials{LDF 2005.10.24.}
Changed |rectangle_like_expression| to |path_expression|.
Removed all debugging code.
\ENDLOG

@<Define rules@>= 
 
@=rectangle_assignment: rectangle_variable ASSIGN path_expression@>
{

   Path* p = static_cast<Path*>(@=$3@>);

   if (p && p->points.size() == 4)
   {
      Rectangle *r = create_new<Rectangle>(0);

      r->set(p->get_point(0), p->get_point(1), p->get_point(2), p->get_point(3));


      Int_Void_Ptr ivp = assign_simple<Rectangle>(static_cast<Scanner_Node>(parameter),
                                                  "Rectangle",
                                                  @=$1@>,
                                                  r);
 
      @=$$@> = ivp.v;
   }
   else
   {
      cerr << "ERROR!  In parser, rule `rectangle_assignment: "
           << "rectangle_variable ASSIGN path_expression':"
           << endl 
           << "`path_expression' is NULL or doesn't have exactly 4 points."
           << endl 
           << "Not assigning to `rectangle_variable'." << endl 
           << "Will try to continue." << endl;

      @=$$@> = 0;
   }

   delete p;
   p = 0;

};

@q ***** (5) reg_polygon_assignment --> reg_polygon_variable ASSIGN path_expression with_test_optional @>   

@*4 \�regular polygon assignment> $\longrightarrow$ \�regular polygon variable> 
\.{ASSIGN} \�path expression> \�with test optional>. 
\initials{LDF 2022.11.30.}

\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=reg_polygon_assignment: reg_polygon_variable ASSIGN path_expression with_test_optional @>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `reg_polygon_assignment: reg_polygon_variable ASSIGN "
                 << "path_expression with_test_optional."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Path *p = static_cast<Path*>(@=$3@>);

   bool do_test;

   if (@=$4@> == WITH_TEST)
      do_test = true;
   else 
      do_test = false;

   if (entry && p)
   {
       Reg_Polygon *rp = 0;

       if (entry->object)
          rp = static_cast<Reg_Polygon*>(entry->object);
       else
       {
          rp = create_new<Reg_Polygon>(0);
          entry->object = rp;
       }

       if (rp)
       {
           *(static_cast<Path*>(rp)) = *p;

           /* !! TODO: LDF 2022.12.17.  Add test.  */

       }

       @=$$@> = static_cast<void*>(p);
      
   }
   else
      @=$$@> = static_cast<void*>(0);
};

@q ***** (5) reg_polygon_assignment --> reg_polygon_variable ASSIGN triangle_expression  with_test_optional@>   

@*4 \�regular polygon assignment> $\longrightarrow$ \�regular polygon variable> 
\.{ASSIGN} \�triangle expression> \�with test optional>. 
\initials{LDF 2022.11.30.}

\LOG
\initials{LDF 2022.11.30.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=reg_polygon_assignment: reg_polygon_variable ASSIGN triangle_expression with_test_optional@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */
   if (DEBUG)
   {
       cerr_strm << thread_name 
                 << "*** Parser: `reg_polygon_assignment: reg_polygon_variable ASSIGN "
                 << "triangle_expression with_test_optional."
                 << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   Triangle *t = static_cast<Triangle*>(@=$3@>);

   bool do_test;

   if (@=$4@> == WITH_TEST)
      do_test = true;
   else 
      do_test = false;

   if (entry && t)
   {
       Reg_Polygon *rp = 0;

       if (entry->object)
          rp = static_cast<Reg_Polygon*>(entry->object);
       else
       {
          rp = create_new<Reg_Polygon>(0);
          entry->object = rp;
       }

       if (rp)
       {
           *(static_cast<Path*>(rp)) = *(static_cast<Path*>(t));

           /* !! TODO: LDF 2022.12.17.  Add test.  */

       }

       @=$$@> = static_cast<void*>(t);
   }
   else
      @=$$@> = static_cast<void*>(0);
};

@q **** (4) triangle_assignment.  @>
@*3 \�triangle assignment>. 
\initials{2009.01.06.}

\LOG
\initials{2009.01.06.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> triangle_assignment@>@/

@q **** (4) triangle_assignment --> triangle_variable @>
@q **** (4) ASSIGN triangle_expression.               @>   

@*3 \�triangle assignment> $\longrightarrow$ \�triangle variable> 
\.{ASSIGN} \�triangle expression>. 
\initials{2009.01.06.}

\LOG
\initials{2009.01.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=triangle_assignment: triangle_variable ASSIGN triangle_expression@>
{

  Triangle* t = static_cast<Triangle*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Triangle>(static_cast<Scanner_Node>(parameter),
                                             "Triangle",
                                             @=$1@>,
                                             t);      

  @=$$@> = ivp.v;

};

@q **** (4) cuboid_assignment.  @>
@*2 \�cuboid assignment>. 

\LOG
\initials{LDF 2004.08.18.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> cuboid_assignment@>@/

@q ***** (5) cuboid_assignment --> cuboid_variable @>
@q ***** (5) := cuboid_expression.  @>   

@*3 \�cuboid assignment> 
$\longrightarrow$ \�cuboid variable> 
\.{:=} \�cuboid expression>. 

\LOG
\initials{LDF 2004.08.18.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=cuboid_assignment: cuboid_variable ASSIGN cuboid_expression@>
{

  Cuboid* c = static_cast<Cuboid*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Cuboid>(static_cast<Scanner_Node>(parameter),
                                   "Cuboid",
                                   @=$1@>,
                                   c);

  @=$$@> = ivp.v;

};

@q **** (4) polyhedron_assignment.  @>
@*2 \�polyhedron assignment>. 

\LOG
\initials{LDF 2004.08.31.}  
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> polyhedron_assignment@>@/

@q ***** (5) polyhedron_assignment --> polyhedron_variable @>
@q ***** (5) := polyhedron_expression.  @>   

@*3 \�polyhedron assignment> 
$\longrightarrow$ \�polyhedron variable> 
\.{:=} \�polyhedron expression>. 

\LOG
\initials{LDF 2004.08.31.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=polyhedron_assignment: polyhedron_variable ASSIGN polyhedron_expression@>
{

Polyhedron* c = static_cast<Polyhedron*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Polyhedron>(static_cast<Scanner_Node>(parameter),
                                   "Polyhedron",
                                   @=$1@>,
                                   c);

  @=$$@> = ivp.v;

};

@q **** (4) sphere_assignment.@>
@*2 \�sphere assignment>. 

\LOG
\initials{LDF 2005.10.30.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sphere_assignment@>@/

@q ***** (5) sphere_assignment --> sphere_variable @>
@q ***** (5) ASSIGN sphere_expression.  @>   

@*3 \�sphere assignment> 
$\longrightarrow$ \�sphere variable> 
\.{ASSIGN} \�sphere expression>. 

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=sphere_assignment: sphere_variable ASSIGN sphere_expression@>
{

  Sphere* c = static_cast<Sphere*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Sphere>(static_cast<Scanner_Node>(parameter),
                                           "Sphere",
                                           @=$1@>,
                                           c);

  @=$$@> = ivp.v;

};

@q **** (4) ellipsoid_assignment.@>
@*2 \�ellipsoid assignment>. 

\LOG
\initials{LDF 2022.06.02.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> ellipsoid_assignment@>@/

@q ***** (5) ellipsoid_assignment --> ellipsoid_variable @>
@q ***** (5) ASSIGN ellipsoid_expression.  @>   

@*3 \�ellipsoid assignment> 
$\longrightarrow$ \�ellipsoid variable> 
\.{ASSIGN} \�ellipsoid expression>. 
\initials{LDF 2022.06.02.}

\LOG
\initials{LDF 2022.06.02.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=ellipsoid_assignment: ellipsoid_variable ASSIGN ellipsoid_expression@>
{

  Ellipsoid* e = static_cast<Ellipsoid*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Ellipsoid>(static_cast<Scanner_Node>(parameter),
                                              "Ellipsoid",
                                              @=$1@>,
                                              e);

  @=$$@> = ivp.v;

};

@q **** (4) cone_assignment.@>
@*2 \�cone assignment>. 

\LOG
\initials{LDF 2023.12.10.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> cone_assignment@>@/

@q ***** (5) cone_assignment --> cone_variable @>
@q ***** (5) ASSIGN cone_expression.  @>   

@*3 \�cone assignment> $\longrightarrow$ \�cone variable> \.{ASSIGN} 
\�cone expression>. 

\LOG
\initials{LDF 2023.12.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=cone_assignment: cone_variable ASSIGN cone_expression@>
{

  Cone* c = static_cast<Cone*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Cone>(static_cast<Scanner_Node>(parameter),
                                         "Cone",
                                         @=$1@>,
                                         c);

  @=$$@> = ivp.v;

};

@q **** (4) cylinder_assignment.@>
@*2 \�cylinder assignment>. 
\initials{LDF 2024.12.02.}

\LOG
\initials{LDF 2024.12.02.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> cylinder_assignment@>@/

@q ***** (5) cylinder_assignment --> cylinder_variable @>
@q ***** (5) ASSIGN cylinder_expression.  @>   

@*3 \�cylinder assignment> $\longrightarrow$ \�cylinder variable> \.{ASSIGN} 
\�cylinder expression>. 

\LOG
\initials{LDF 2024.12.02.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=cylinder_assignment: cylinder_variable ASSIGN cylinder_expression@>
{

  Cylinder* c = static_cast<Cylinder*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Cylinder>(static_cast<Scanner_Node>(parameter),
                                         "Cylinder",
                                         @=$1@>,
                                         c);

  @=$$@> = ivp.v;

};

@q **** (4) prismatoid_assignment.@>
@*2 \�prismatoid assignment>. 

\LOG
\initials{LDF 2024.05.27.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> prismatoid_assignment@>@/

@q ***** (5) prismatoid_assignment --> prismatoid_variable @>
@q ***** (5) ASSIGN prismatoid_expression.  @>   

@*3 \�prismatoid assignment> $\longrightarrow$ \�prismatoid variable> \.{ASSIGN} 
\�prismatoid expression>. 

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=prismatoid_assignment: prismatoid_variable ASSIGN prismatoid_expression@>
{

  Prismatoid* c = static_cast<Prismatoid*>(@=$3@>);

  Int_Void_Ptr ivp = assign_simple<Prismatoid>(static_cast<Scanner_Node>(parameter),
                                         "Prismatoid",
                                         @=$1@>,
                                         c);

  @=$$@> = ivp.v;

};

@q **** (4) plane_assignment.@>
@*2 \�plane assignment>. 

\LOG
\initials{LDF 2005.10.30.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> plane_assignment@>@/

@q ***** (5) plane_assignment --> plane_variable ASSIGN plane_expression.@>   

@*4 \�plane assignment> $\longrightarrow$ \�plane variable> 
\.{ASSIGN} \�plane expression>. 

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=plane_assignment: plane_variable ASSIGN plane_expression@>
{

  Int_Void_Ptr ivp = assign_simple<Plane>(static_cast<Scanner_Node>(parameter),
                                           "Plane",
                                           @=$1@>,
                                           static_cast<Plane*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q ***** (5) plane_assignment --> plane_variable ASSIGN path_expression.@>   

@*3 \�plane assignment> $\longrightarrow$ \�plane variable> 
\.{ASSIGN} \�path expression>. 

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=plane_assignment: plane_variable ASSIGN path_expression@>
{

    @=$$@> = Scan_Parse::plane_assignment_func<Path>(@=$1@>,
                                                     static_cast<Path*>(@=$3@>),
                                                     parameter);
};

@q **** (4) line_assignment.@>
@*2 \�line assignment>. 

\LOG
\initials{LDF 2022.12.04.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> line_assignment@>@/

@q ***** (5) line_assignment --> line_variable ASSIGN line_expression.@>   

@*4 \�line assignment> $\longrightarrow$ \�line variable> 
\.{ASSIGN} \�line expression>. 

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=line_assignment: line_variable ASSIGN line_expression@>
{

  Int_Void_Ptr ivp = assign_simple<Line>(static_cast<Scanner_Node>(parameter),
                                           "Line",
                                           @=$1@>,
                                           static_cast<Line*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q ***** (5) line_assignment --> line_variable ASSIGN path_expression.@>   

@*3 \�line assignment> $\longrightarrow$ \�line variable> 
\.{ASSIGN} \�path expression>. 

\LOG
\initials{LDF 2022.12.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=line_assignment: line_variable ASSIGN path_expression@>
{

   /* !!START HERE:  LDF 2022.12.04.*/ 

#if 0 
    @=$$@> = Scan_Parse::line_assignment_func<Path>(@=$1@>,
                                                     static_cast<Path*>(@=$3@>),
                                                     parameter);
#endif 

   @=$$@> = 0;

};

@q **** (4) star_assignment.@>
@*2 \�star assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> star_assignment@>@/

@q ***** (5) star_assignment --> star_variable ASSIGN star_expression.@>   

@*4 \�star assignment> $\longrightarrow$ \�star variable> 
\.{ASSIGN} \�star expression>. 

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=star_assignment: star_variable ASSIGN star_expression@>
{

  Int_Void_Ptr ivp = assign_simple<Star>(static_cast<Scanner_Node>(parameter),
                                          "Star",
                                          @=$1@>,
                                          static_cast<Star*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q **** (4) constellation_assignment.@>
@*2 \�constellation assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> constellation_assignment@>@/

@q ***** (5) constellation_assignment --> constellation_variable ASSIGN constellation_expression.@>   

@*4 \�constellation assignment> $\longrightarrow$ \�constellation variable> 
\.{ASSIGN} \�constellation expression>. 

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=constellation_assignment: constellation_variable ASSIGN constellation_expression@>
{

  Int_Void_Ptr ivp = assign_simple<Constellation>(static_cast<Scanner_Node>(parameter),
                                          "Constellation",
                                          @=$1@>,
                                          static_cast<Constellation*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q **** (4) planet_assignment.@>
@*2 \�planet assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> planet_assignment@>@/

@q ***** (5) planet_assignment --> planet_variable ASSIGN planet_expression.@>   

@*4 \�planet assignment> $\longrightarrow$ \�planet variable> 
\.{ASSIGN} \�planet expression>. 

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=planet_assignment: planet_variable ASSIGN planet_expression@>
{

  Int_Void_Ptr ivp = assign_simple<Planet>(static_cast<Scanner_Node>(parameter),
                                          "Planet",
                                          @=$1@>,
                                          static_cast<Planet*>(@=$3@>));
  @=$$@> = ivp.v;

};

@q **** (4) newwrite_assignment.@>
@*2 \�newwrite assignment>. 
\initials{LDF 2021.07.05.}

\LOG
\initials{LDF 2021.07.05.}
Added this section.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> newwrite_assignment@>@/

@q ***** (5) newwrite_assignment --> newwrite_variable ASSIGN string_expression.@>   

@*4 \�newwrite assignment> $\longrightarrow$ \�newwrite variable> 
\.{ASSIGN} \�string expression>. 

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG

@<Define rules@>=
 
@=newwrite_assignment: newwrite_variable ASSIGN string_expression@>
{

   @<Common declarations for rules@>@;

#if DEBUG_COMPILE

   DEBUG = false; /* |true|  */

   if (DEBUG)
   { 
       cerr << "*** Parser: newwrite_assignment: newwrite_variable ASSIGN string_expression."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  Newwrite *n;
  string *s = static_cast<string*>(@=$3@>);

  n = new Newwrite;

  *n = *s;

#if 0 
  n->show("In rule newwrite_assignment ... *n:");
#endif

  delete s;
  s = 0;

  Int_Void_Ptr ivp = assign_simple<Newwrite>(static_cast<Scanner_Node>(parameter),
                                            "Newwrite",
                                             @=$1@>,
                                             n);
  @=$$@> = ivp.v;

#if 0
  static_cast<Newwrite*>(ivp.v)->show("*ivp.v:");
#endif 

};

@q ** (2) Vector types.@> 
@*1 Vector types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q *** (3) Non-|Shape| types.@> 
@*1 Non-{\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) boolean_vector_assignment.  @>
@*2 \�boolean vector assignment>. 
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> boolean_vector_assignment@>

@q **** (4) boolean_vector_assignment -->  @>  
@q **** (4) boolean_vector_variable ASSIGN @>  
@q **** (4) boolean_vector_expression.     @> 

@*3 \�boolean vector assignment>
$\longrightarrow$ \�boolean vector variable> 
\.{ASSIGN} \�boolean vector expression>.      
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=boolean_vector_assignment: boolean_vector_variable @>  
@=ASSIGN boolean_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

   typedef Pointer_Vector<bool> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<bool, bool>(static_cast<Scanner_Node>(parameter),
                                               entry,
                                               pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.07.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.07.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ****** (6).@> 

}; 


@q **** (4) string_vector_assignment.  @>
@*2 \�string vector assignment>. 
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> string_vector_assignment@>

@q **** (4) string_vector_assignment -->  @>  
@q **** (4) string_vector_variable ASSIGN @>  
@q **** (4) string_vector_expression.     @> 

@*3 \�string vector assignment>
$\longrightarrow$ \�string vector variable> 
\.{ASSIGN} \�string vector expression>.      
\initials{LDF 2005.01.09.}

\LOG
\initials{LDF 2005.01.09.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=string_vector_assignment: string_vector_variable @>  
@=ASSIGN string_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.01.09.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == 0)|  */

@q ****** (6) |entry != 0|.@>   

@ |entry != 0|.
\initials{LDF 2005.01.09.}

@<Define rules@>=

  else /* |entry != 0|  */
    {

   typedef Pointer_Vector<string> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<string, string>(static_cast<Scanner_Node>(parameter),
                                                  entry,
                                                  pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.09.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.09.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != 0|)  */

@q ****** (6).@> 

}; 

@q **** (4) pen_vector_assignment.  @>
@*2 \�pen vector assignment>. 
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> pen_vector_assignment@>

@q **** (4) pen_vector_assignment -->  @>  
@q **** (4) pen_vector_variable ASSIGN @>  
@q **** (4) pen_vector_expression.     @> 

@*3 \�pen vector assignment>
$\longrightarrow$ \�pen vector variable> 
\.{ASSIGN} \�pen vector expression>.      
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=pen_vector_assignment: pen_vector_variable @>  
@=ASSIGN pen_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == 0|.@>

@ Error handling for the case that |entry == 0 |.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Pen> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Pen, Pen>(static_cast<Scanner_Node>(parameter),
                                                entry,
                                                pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) dash_pattern_vector_assignment.  @>
@*2 \�dash pattern vector assignment>. 
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> dash_pattern_vector_assignment@>

@q **** (4) dash_pattern_vector_assignment -->  @>  
@q **** (4) dash_pattern_vector_variable ASSIGN @>  
@q **** (4) dash_pattern_vector_expression.     @> 

@*3 \�dash pattern vector assignment>
$\longrightarrow$ \�dash pattern vector variable> 
\.{ASSIGN} \�dash pattern vector expression>.      
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=dash_pattern_vector_assignment: dash_pattern_vector_variable @>  
@=ASSIGN dash_pattern_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Dash_Pattern> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Dash_Pattern, Dash_Pattern>(
                                             static_cast<Scanner_Node>(parameter),
                                             entry,
                                             pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) color_vector_assignment.  @>
@*2 \�color vector assignment>. 

\LOG
\initials{LDF 2004.11.05.}
Commented-out this type declaration.

\initials{LDF 2005.01.07.}
Commented this type declaration back in.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> color_vector_assignment@>

@q **** (4) color_vector_assignment -->  @>  
@q **** (4) color_vector_variable ASSIGN @>  
@q **** (4) color_vector_expression.     @> 

@*3 \�color vector assignment>
$\longrightarrow$ \�color vector variable> 
\.{ASSIGN} \�color vector expression>.      
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=color_vector_assignment: color_vector_variable @>  
@=ASSIGN color_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.07.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Color> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Color, Color>(static_cast<Scanner_Node>(parameter),
                                                 entry,
                                                 pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.07.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.07.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) transform_vector_assignment.  @>
@*2 \�transform vector assignment>. 
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> transform_vector_assignment@>

@q **** (4) transform_vector_assignment -->  @>  
@q **** (4) transform_vector_variable ASSIGN @>  
@q **** (4) transform_vector_expression.     @> 

@*3 \�transform vector assignment>
$\longrightarrow$ \�transform vector variable> 
\.{ASSIGN} \�transform vector expression>.      
\initials{LDF 2005.01.13.}

\LOG
\initials{LDF 2005.01.13.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=transform_vector_assignment: transform_vector_variable @>  
@=ASSIGN transform_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.13.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Transform> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Transform, Transform>(static_cast<Scanner_Node>(parameter),
                                                entry,
                                                pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.13.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) focus_vector_assignment.  @>
@*2 \�focus vector assignment>. 
\initials{LDF 2005.01.18.}

\LOG
\initials{LDF 2005.01.18.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> focus_vector_assignment@>

@q **** (4) focus_vector_assignment -->  @>  
@q **** (4) focus_vector_variable ASSIGN @>  
@q **** (4) focus_vector_expression.     @> 

@*3 \�focus vector assignment>
$\longrightarrow$ \�focus vector variable> 
\.{ASSIGN} \�focus vector expression>.      
\initials{LDF 2005.01.18.}

\LOG
\initials{LDF 2005.01.18.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=focus_vector_assignment: focus_vector_variable @>  
@=ASSIGN focus_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.18.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.18.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Focus> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Focus, Focus>(static_cast<Scanner_Node>(parameter),
                                                entry,
                                                pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.18.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.18.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q *** (3) |Shape| types.@> 
@*1 {\bf Shape} types.
\initials{LDF 2005.01.07.}

\LOG
\initials{LDF 2005.01.07.}
Added this section.
\ENDLOG

@q **** (4) |point_vector_assignment|.  @>
@*2 \�point vector assignment>. 

\LOG
\initials{LDF 2004.09.01.}
Added this section.

\initials{LDF 2004.11.05.}
Commented-out this type declaration.

\initials{LDF 2004.11.10.}
Commented this type declaration back in.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> point_vector_assignment@>

@q ***** (5) point_vector_assignment -->  @>  
@q ***** (5) point_vector_variable ASSIGN @>  
@q ***** (5) point_vector_expression.     @> 

@*3 \�point vector assignment> $\longrightarrow$ \�point vector variable> 
\.{ASSIGN} \�point vector expression>.      

\LOG
\initials{LDF 2004.11.10.}
Added this rule.

\initials{LDF 2004.11.11.}
Changed the code in this rule to account for changes
I've made in |Scan_Parse::vector_type_assign|, which is 
defined in \filename{scanprse.web}.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=point_vector_assignment: point_vector_variable @>  
@=ASSIGN point_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.11.06.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.09.01.}

\LOG
\initials{LDF 2024.02.19.}
!! BUG FIX:  Now calling |Scan_Parse::clear_vector_func|.  !! TODO:  Do this for other vector-type
assignements.
\ENDLOG 

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
  {

     typedef Pointer_Vector<Point> PV;
 
     PV* pv = static_cast<PV*>(@=$3@>);

     Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                   entry); 

     int status = vector_type_assign<Point, Point>(static_cast<Scanner_Node>(parameter),
                                                   entry,
                                                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

  }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q ***** (5) point_vector_assignment --> point_vector_variable @>  
@q ***** (5) ASSIGN bool_point_vector_expression.              @> 

@*3 \�point vector assignment>
$\longrightarrow$ \�point vector variable> 
\.{ASSIGN} \�bool-point vector expression>.      

\LOG
\initials{LDF 2004.11.10.}
Added this rule.

\initials{LDF 2004.11.11.}
Changed the code in this rule to account for changes
I've made in |Scan_Parse::vector_type_assign|, which is 
defined in \filename{scanprse.web}.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=point_vector_assignment: point_vector_variable @>  
@=ASSIGN bool_point_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
  {

      @=$$@> = static_cast<void*>(0); 

  } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
  {

   typedef Pointer_Vector<Point> PV;
   typedef Pointer_Vector<Bool_Point> BPV;
 
   BPV* bpv = static_cast<BPV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

int status = vector_type_assign<Point, Bool_Point>(static_cast<Scanner_Node>(parameter),
                                                  entry,                   
                                                  bpv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

if (status != 0)
         {

            delete bpv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

   else /* |status == 0|  */
   {
      delete bpv;

      @=$$@> = static_cast<void*>(entry->object); 
 
   }  /* |else| (|status == 0|)  */

  }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@*3 \�point vector assignment> $\longrightarrow$ \�point vector variable> 
\.{ASSIGN} \�superellipse expression>.      

\LOG
\initials{LDF 2022.05.30.}n
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=point_vector_assignment: point_vector_variable ASSIGN superellipse_expression@>@/
{

 Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2022.05.30.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2022.05.30.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
  {

     Superellipse *s = static_cast<Superellipse*>(@=$3@>);

     Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

     Point *p = 0;

     for (vector<Point*>::iterator iter = s->points.begin();
          iter != s->points.end();
          ++iter)
     {
         p = create_new<Point>(0);
         *p = **iter;
         *pv += p;
     }

     Pointer_Vector<Point> *entry_pv = static_cast<Pointer_Vector<Point>*>(entry->object);

     if (entry_pv)
       entry_pv->clear();

     int status = vector_type_assign<Point, Point>(static_cast<Scanner_Node>(parameter),
                                                   entry,
                                                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

   if (status != 0)
   {

        delete pv;

        @=$$@> = static_cast<void*>(0);

   } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

   else /* |status == 0|  */
   {
      delete pv;

      @=$$@> = static_cast<void*>(entry->object); 
 
   }  /* |else| (|status == 0|)  */

  }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 



@q **** (4) |bool_point_vector_assignment|.  @>
@*2 \�bool-point vector assignment>. 

\LOG
\initials{LDF 2004.09.01.}
Added this section.

\initials{LDF 2004.11.05.}
Commented-out this type declaration.

\initials{LDF 2004.11.05.}
Commented this type declaration back in.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> bool_point_vector_assignment@>

@q ***** (5) bool_point_vector_assignment -->  @>  
@q ***** (5) bool_point_vector_variable ASSIGN @>  
@q ***** (5) bool_point_vector_expression.     @> 

@*3 \�bool-point vector assignment>
$\longrightarrow$ \�bool-point vector variable> 
\.{ASSIGN} \�bool-point vector expression>.      

\LOG
\initials{LDF 2004.11.05.}
Added this rule.

\initials{LDF 2004.11.06.}
Finished writing this rule.

\initials{LDF 2004.11.11.}
Changed the code in this rule to account for changes
I've made in |Scan_Parse::vector_type_assign|, which is 
defined in \filename{scanprse.web}.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=bool_point_vector_assignment: bool_point_vector_variable @>  
@=ASSIGN bool_point_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.11.06.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.09.01.}

\LOG
initials{LDF 2004.11.06.}
@:BUG FIX@> BUG FIX:  If |entry->object != 0|, 
now clearing the |Pointer_Vector<Bool_Point>| it points to 
before looping over the |bool_point_vector_expression|
and calling |Scan_Parse::vector_type_plus_assign|.

\initials{LDF 2004.11.08.}
Now calling |Scan_Parse::vector_type_assign| instead of 
|Scan_Parse::vector_type_plus_assign|.  This function loops over 
the |bool_point_vector_expression|, so we don't have to do so here.

\initials{LDF 2005.11.28.}
@:BUG FIX@> BUG FIX:  Now calling |Scan_Parse::clear_vector_func|.
\ENDLOG 

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

        Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter),
                                      entry);

        typedef Pointer_Vector<Bool_Point> BPV;
 
        BPV* bpv = static_cast<BPV*>(@=$3@>);

int status = vector_type_assign<Bool_Point, Bool_Point>(
                          static_cast<Scanner_Node>(parameter),
                          entry,
                          bpv);

@q ******* (7) Error handling:                                @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

if (status != 0)
         {

            delete bpv;
            bpv = 0;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.06.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete bpv;
         bpv = 0;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q ***** (5) bool_point_vector_assignment -->  @>  
@q ***** (5) bool_point_vector_variable ASSIGN @>  
@q ***** (5) point_vector_expression.     @> 

@*3 \�bool-point vector assignment>
$\longrightarrow$ \�bool-point vector variable> 
\.{ASSIGN} \�point vector expression>.      

\LOG
\initials{LDF 2004.11.10.}
Added this rule.

\initials{LDF 2004.11.11.}
Changed the code in this rule to account for changes
I've made in |Scan_Parse::vector_type_assign|, which is 
defined in \filename{scanprse.web}.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=bool_point_vector_assignment: bool_point_vector_variable @>  
@=ASSIGN point_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Point> PV;
   typedef Pointer_Vector<Bool_Point> BPV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   BPV* entry_bpv = static_cast<BPV*>(entry->object);

   if (entry_bpv)
     entry_bpv->clear();
   
   int status = vector_type_assign<Bool_Point, Point>(static_cast<Scanner_Node>(parameter),
                                                  entry,
                                                  pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

if (status != 0)
         {
            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.11.10.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |path_vector_assignment|.  @>
@*2 \�path vector assignment>. 

\LOG
\initials{LDF 2004.12.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> path_vector_assignment@>

@q ***** (5) path_vector_assignment -->  @>  
@q ***** (5) path_vector_variable ASSIGN @>  
@q ***** (5) path_vector_expression.     @> 

@*3 \�path vector assignment> $\longrightarrow$ \�path vector variable> \.{ASSIGN} 
\�path vector expression>.      

\LOG
\initials{LDF 2004.12.10.}
Added this rule.

\initials{LDF 2022.05.01.}
Added code for clearing |entry->pv|.  Now passing |bool clear_vector = false| to 
|Scan_Parse::vector_type_assign|.  I added this argument because previously 
this rule would fail if the \�path vector expression> hadn't been cleared first.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=path_vector_assignment: path_vector_variable ASSIGN path_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
  {

    @=$$@> = static_cast<void*>(0); 

  } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

  else if (@=$3@> == 0)
  {
     @=$$@> = static_cast<void*>(0);
  }

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.10.}

\LOG
\initials{LDF 2024.02.19.}
!! BUG FIX:  Now calling |Scan_Parse::clear_vector_func|.  !! TODO:  Do this for other vector-type
assignements.
\ENDLOG 

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
  {

   typedef Pointer_Vector<Path> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                 entry); 

   int status = vector_type_assign<Path, Path>(static_cast<Scanner_Node>(parameter),
                                           entry,
                                           pv,
                                           static_cast<Path*>(0),
                                           false);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.10.}

@<Define rules@>=

   if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.10.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |ellipse_vector_assignment|.  @>
@*2 \�ellipse vector assignment>. 

\LOG
\initials{LDF 2004.12.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> ellipse_vector_assignment@>

@q ***** (5) ellipse_vector_assignment -->  @>  
@q ***** (5) ellipse_vector_variable ASSIGN @>  
@q ***** (5) ellipse_vector_expression.     @> 

@*3 \�ellipse vector assignment>
$\longrightarrow$ \�ellipse vector variable> 
\.{ASSIGN} \�ellipse vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=ellipse_vector_assignment: ellipse_vector_variable @>  
@=ASSIGN ellipse_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Ellipse> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Ellipse, Ellipse>(static_cast<Scanner_Node>(parameter),
                                           entry,
                                           pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |circle_vector_assignment|.  @>
@*2 \�circle vector assignment>. 

\LOG
\initials{LDF 2004.12.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> circle_vector_assignment@>

@q ***** (5) circle_vector_assignment -->  @>  
@q ***** (5) circle_vector_variable ASSIGN @>  
@q ***** (5) circle_vector_expression.     @> 

@*3 \�circle vector assignment>
$\longrightarrow$ \�circle vector variable> 
\.{ASSIGN} \�circle vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=circle_vector_assignment: circle_vector_variable @>  
@=ASSIGN circle_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Circle> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Circle, Circle>(static_cast<Scanner_Node>(parameter),
                                           entry,
                                           pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |parabola_vector_assignment|.  @>
@*2 \�parabola vector assignment>. 
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> parabola_vector_assignment@>

@q ***** (5) parabola_vector_assignment -->  @>  
@q ***** (5) parabola_vector_variable ASSIGN @>  
@q ***** (5) parabola_vector_expression.     @> 

@*3 \�parabola vector assignment>
$\longrightarrow$ \�parabola vector variable> 
\.{ASSIGN} \�parabola vector expression>.      
\initials{LDF 2005.11.07.}

\LOG
\initials{LDF 2005.11.07.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=parabola_vector_assignment: parabola_vector_variable @>  
@=ASSIGN parabola_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.11.07.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.11.07.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Parabola> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status 
       = Scan_Parse::vector_type_assign<Parabola, Parabola>(
            static_cast<Scanner_Node>(parameter),
            entry,
            pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.11.07.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.11.07.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |hyperbola_vector_assignment|.  @>
@*2 \�hyperbola vector assignment>. 
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> hyperbola_vector_assignment@>

@q ***** (5) hyperbola_vector_assignment -->  @>  
@q ***** (5) hyperbola_vector_variable ASSIGN @>  
@q ***** (5) hyperbola_vector_expression.     @> 

@*3 \�hyperbola vector assignment>
$\longrightarrow$ \�hyperbola vector variable> 
\.{ASSIGN} \�hyperbola vector expression>.      
\initials{LDF 2005.11.14.}

\LOG
\initials{LDF 2005.11.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=hyperbola_vector_assignment: hyperbola_vector_variable @>  
@=ASSIGN hyperbola_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.11.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.11.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Hyperbola> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status 
       = Scan_Parse::vector_type_assign<Hyperbola, Hyperbola>(
            static_cast<Scanner_Node>(parameter),
            entry,
            pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.11.14.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.11.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |helix_vector_assignment|.  @>
@*2 \�helix vector assignment>. 
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> helix_vector_assignment@>

@q ***** (5) helix_vector_assignment -->  @>  
@q ***** (5) helix_vector_variable ASSIGN @>  
@q ***** (5) helix_vector_expression.     @> 

@*3 \�helix vector assignment>
$\longrightarrow$ \�helix vector variable> 
\.{ASSIGN} \�helix vector expression>.      
\initials{LDF 2005.05.20.}

\LOG
\initials{LDF 2005.05.20.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=helix_vector_assignment: helix_vector_variable @>  
@=ASSIGN helix_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.05.20.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.05.20.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Helix> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Helix, Helix>(static_cast<Scanner_Node>(parameter),
                                                 entry,
                                                 pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.05.20.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.05.20.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |triangle_vector_assignment|.  @>
@*2 \�triangle vector assignment>. 
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> triangle_vector_assignment@>

@q ***** (5) triangle_vector_assignment -->  @>  
@q ***** (5) triangle_vector_variable ASSIGN @>  
@q ***** (5) triangle_vector_expression.     @> 

@*3 \�triangle vector assignment>
$\longrightarrow$ \�triangle vector variable> 
\.{ASSIGN} \�triangle vector expression>.      
\initials{LDF 2005.01.25.}

\LOG
\initials{LDF 2005.01.25.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=triangle_vector_assignment: triangle_vector_variable @>  
@=ASSIGN triangle_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.25.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Triangle> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Triangle, Triangle>(static_cast<Scanner_Node>(parameter),
                                                       entry,
                                                       pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.25.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.25.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |nurb_vector_assignment|.  @>
@*2 \�nurb vector assignment>. 
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> nurb_vector_assignment@>

@q ***** (5) nurb_vector_assignment -->  @>  
@q ***** (5) nurb_vector_variable ASSIGN @>  
@q ***** (5) nurb_vector_expression.     @> 

@*3 \�nurb vector assignment>
$\longrightarrow$ \�nurb vector variable> 
\.{ASSIGN} \�nurb vector expression>.      
\initials{LDF 2005.01.26.}

\LOG
\initials{LDF 2005.01.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=nurb_vector_assignment: nurb_vector_variable @>  
@=ASSIGN nurb_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.26.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Nurb> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Nurb, Nurb>(static_cast<Scanner_Node>(parameter),
                                               entry,
                                               pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.26.}

@<Define rules@>=

if (status != 0)
         {
            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.26.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |polygon_vector_assignment|.  @>
@*2 \�polygon vector assignment>. 
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> polygon_vector_assignment@>

@q ***** (5) polygon_vector_assignment -->  @>  
@q ***** (5) polygon_vector_variable ASSIGN @>  
@q ***** (5) polygon_vector_expression.     @> 

@*3 \�polygon vector assignment>
$\longrightarrow$ \�polygon vector variable> 
\.{ASSIGN} \�polygon vector expression>.      
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.11.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=polygon_vector_assignment: polygon_vector_variable @>  
@=ASSIGN polygon_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

   typedef Pointer_Vector<Polygon> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

@q ****** (6).@> 
@ 

\LOG
\initials{LDF 2005.03.01.}
Now calling |Scan_Parse::clear_vector_func| on |entry|.

\initials{LDF 2005.03.01.}
Removed the call to |Scan_Parse::clear_vector_func| on |entry|
to |Scan_Parse::vector_type_assign| in \filename{scanprse.web}.

\initials{LDF 2005.03.01.}
Now calling |Scan_Parse::vector_type_assign| in a |try| block
and catching |bad_alloc|.
\ENDLOG 

@<Define rules@>=

          int status = vector_type_assign<Polygon, Polygon>(
                          static_cast<Scanner_Node>(parameter),
                                                        entry,
                                                        pv);

@q ******* (7) Error handling:                            @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed, @> 
@q ******* (7) returning a non-zero value.                @> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed, returning a non-zero value.
\initials{LDF 2005.02.11.}

\LOG
\initials{LDF 2005.02.28.}
Now deleting |entry_pv| and setting it to 0 if 
|vector_type_assign<Polygon, Polygon>| failed.

\initials{LDF 2005.03.01.}
No longer declaring |Pointer_Vector<Polygon>* entry_pv|, 
and no longer deleting it and setting it to 0 
if |vector_type_assign<Polygon, Polygon>| failed.
\ENDLOG 

@<Define rules@>=

    if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.02.11.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

@q ****** (6).@> 

}; 

@q **** (4) |rectangle_vector_assignment|.  @>
@*2 \�rectangle vector assignment>. 

\LOG
\initials{LDF 2004.12.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> rectangle_vector_assignment@>

@q ***** (5) rectangle_vector_assignment -->  @>  
@q ***** (5) rectangle_vector_variable ASSIGN @>  
@q ***** (5) rectangle_vector_expression.     @> 

@*3 \�rectangle vector assignment>
$\longrightarrow$ \�rectangle vector variable> 
\.{ASSIGN} \�rectangle vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.

\initials{LDF 2024.02.19.}
!! BUG FIX:  Now calling |Scan_Parse::clear_vector_func|.  !! TODO:  Do this for other vector-type
assignements.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=rectangle_vector_assignment: rectangle_vector_variable @>  
@=ASSIGN rectangle_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Rectangle> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   Scan_Parse::clear_vector_func(static_cast<Scanner_Node>(parameter), 
                                 entry); 

   int status = vector_type_assign<Rectangle, Rectangle>(static_cast<Scanner_Node>(parameter),
                                                         entry,
                                                         pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {
            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |reg_polygon_vector_assignment|.  @>
@*2 \�regular polygon vector assignment>. 

\LOG
\initials{LDF 2004.12.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> reg_polygon_vector_assignment@>

@q ***** (5) reg_polygon_vector_assignment -->  @>  
@q ***** (5) reg_polygon_vector_variable ASSIGN @>  
@q ***** (5) reg_polygon_vector_expression.     @> 

@*3 \�regular polygon vector assignment>
$\longrightarrow$ \�regular polygon vector variable> 
\.{ASSIGN} \�regular polygon vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=reg_polygon_vector_assignment: reg_polygon_vector_variable @>  
@=ASSIGN reg_polygon_vector_expression@>@/
{

    @<Common declarations for rules@>@;

#if DEBUG_COMPILE
    DEBUG = false; /* |true| */ 
    if (DEBUG)
    { 
       cerr << "*** Parser, rule `reg_polygon_vector_assignment:"
            << endl 
            << "reg_polygon_vector_variable ASSIGN reg_polygon_vector_expression'."
            << endl;

    }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
  {

   typedef Pointer_Vector<Reg_Polygon> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

#if 0 
   if (pv)
   {
      cerr << "pv->v.size() == " << pv->v.size() << endl;
   } 
   else
   {
      cerr << "pv is NULL." << endl;
   }
#endif 

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   status = vector_type_assign<Reg_Polygon, Reg_Polygon>(
                    static_cast<Scanner_Node>(parameter),
                    entry,
                    pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   if (status != 0)
   {

      delete pv;

      @=$$@> = static_cast<void*>(0);

   } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

      else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |cuboid_vector_assignment|.  @>
@*2 \�cuboid vector assignment>. 

\LOG
\initials{LDF 2004.12.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> cuboid_vector_assignment@>

@q ***** (5) cuboid_vector_assignment -->  @>  
@q ***** (5) cuboid_vector_variable ASSIGN @>  
@q ***** (5) cuboid_vector_expression.     @> 

@*3 \�cuboid vector assignment>
$\longrightarrow$ \�cuboid vector variable> 
\.{ASSIGN} \�cuboid vector expression>.      

\LOG
\initials{LDF 2004.12.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=cuboid_vector_assignment: cuboid_vector_variable @>  
@=ASSIGN cuboid_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2004.12.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Cuboid> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Cuboid, Cuboid>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2004.12.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |polyhedron_vector_assignment|.  @>
@*2 \�polyhedron vector assignment>. 

\LOG
\initials{LDF 2005.01.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> polyhedron_vector_assignment@>

@q ***** (5) polyhedron_vector_assignment -->  @>  
@q ***** (5) polyhedron_vector_variable ASSIGN @>  
@q ***** (5) polyhedron_vector_expression.     @> 

@*3 \�polyhedron vector assignment>
$\longrightarrow$ \�polyhedron vector variable> 
\.{ASSIGN} \�polyhedron vector expression>.      

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=polyhedron_vector_assignment: polyhedron_vector_variable @>  
@=ASSIGN polyhedron_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Polyhedron> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Polyhedron, Polyhedron>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.01.14.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.01.14.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |sphere_vector_assignment|.  @>
@*2 \�sphere vector assignment>. 

\LOG
\initials{LDF 2005.10.30.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> sphere_vector_assignment@>

@q ***** (5) sphere_vector_assignment -->  @>  
@q ***** (5) sphere_vector_variable ASSIGN @>  
@q ***** (5) sphere_vector_expression.     @> 

@*3 \�sphere vector assignment>
$\longrightarrow$ \�sphere vector variable> 
\.{ASSIGN} \�sphere vector expression>.      

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=sphere_vector_assignment: sphere_vector_variable @>  
@=ASSIGN sphere_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Sphere> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Sphere, Sphere>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

if (status != 0)
         {
            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 


@q **** (4) |plane_vector_assignment|.  @>
@*2 \�plane vector assignment>. 

\LOG
\initials{LDF 2005.10.30.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> plane_vector_assignment@>

@q ***** (5) plane_vector_assignment -->  @>  
@q ***** (5) plane_vector_variable ASSIGN @>  
@q ***** (5) plane_vector_expression.     @> 

@*3 \�plane vector assignment>
$\longrightarrow$ \�plane vector variable> 
\.{ASSIGN} \�plane vector expression>.      

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=plane_vector_assignment: plane_vector_variable @>  
@=ASSIGN plane_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Plane> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Plane, Plane>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |line_vector_assignment|.  @>
@*2 \�line vector assignment>. 
\initials{LDF 2022.12.04.}

\LOG
\initials{LDF 2022.12.04.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> line_vector_assignment@>

@q ***** (5) line_vector_assignment -->  @>  
@q ***** (5) line_vector_variable ASSIGN @>  
@q ***** (5) line_vector_expression.     @> 

@*3 \�line vector assignment>
$\longrightarrow$ \�line vector variable> 
\.{ASSIGN} \�line vector expression>.      

\LOG
\initials{LDF 2005.10.30.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=line_vector_assignment: line_vector_variable @>  
@=ASSIGN line_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2005.10.30.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Line> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Line, Line>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2005.10.30.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |star_vector_assignment|.  @>
@*2 \�star vector assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> star_vector_assignment@>

@q ***** (5) star_vector_assignment -->  @>  
@q ***** (5) star_vector_variable ASSIGN @>  
@q ***** (5) star_vector_expression.     @> 

@*3 \�star vector assignment>
$\longrightarrow$ \�star vector variable> 
\.{ASSIGN} \�star vector expression>.      
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=star_vector_assignment: star_vector_variable ASSIGN star_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Star> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Star, Star>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q ***** (5) star_vector_assignment -->  star_vector_variable ASSIGN LAST @>  

@*3 \�star vector assignment> $\longrightarrow$ \�star vector variable> \.{ASSIGN} \.{LAST}. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=star_vector_assignment: star_vector_variable ASSIGN LAST@>@/
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE

   DEBUG = false; /* |true|  */

   if (DEBUG)
   { 
       cerr << "*** Parser: `star_vector_assignment: star_vector_variable ASSIGN LAST'."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

      goto END_STAR_VECTOR_ASSIGNMENT_0;

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) @>
@
@<Define rules@>=

   else if (scanner_node->last_star_vector.size() == 0)
   {

      @=$$@> = static_cast<void*>(0); 

      goto END_STAR_VECTOR_ASSIGNMENT_0;

   }

@q ****** (6) @>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)| and |scanner_node->last_star_vector| 
is not empty.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)| and |scanner_node->last_star_vector| is not empty. */
  {

   typedef Pointer_Vector<Star> PV;

   PV* entry_pv = static_cast<PV*>(entry->object);

   PV *pv = new PV;

   *pv = scanner_node->last_star_vector;
        
   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Star, Star>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);


@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   if (status != 0)
   {

       delete pv;
       pv = 0;     

       @=$$@> = static_cast<void*>(0);

   } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   else /* |status == 0|  */
   {
      delete pv;
      pv = 0;

      @=$$@> = static_cast<void*>(entry->object); 
 
   }  /* |else| (|status == 0|)  */

 }   /* |else| (|status = 0|) */


@q ****** (6).@> 

END_STAR_VECTOR_ASSIGNMENT_0:

   if (scanner_node->stars_get_option_struct != 0)
   {
         delete scanner_node->stars_get_option_struct;
         scanner_node->stars_get_option_struct = 0;
   }
}; 

@q ***** (5) star_vector_assignment -->  star_vector_variable ASSIGN STARS stars_field_list stars_option_list @>  

@*3 \�star vector assignment> $\longrightarrow$ \�star vector variable> \.{ASSIGN} \.{STARS} 
\�stars field list> \�stars option list>.
\initials{LDF 2021.06.26.}

\�star vector variable> is ``syntactic sugar''.  It's ignored, but it's helpful for it to exist,
since it makes the syntax of this rule similar to that of the rule
\�command> $\longrightarrow$ \.{SHOW} \.{STARS} \<stars field list> \�stars option list>.
\par
If \�star vector variable> wasn't present, it would cause a syntax error if a ``field list''
was specified, which is an easy error to commit.
\initials{LDF 2021.6.27.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ****** (6) @>

@<Define rules@>=

@=star_vector_assignment: star_vector_variable ASSIGN STARS stars_field_list stars_option_list@>@/
{
@q ******* (7) @>

   @<Common declarations for rules@>@;

   vector<Star*> v;
   vector<Star> w;

   typedef Pointer_Vector<Star> PV;
   PV *pv;
   PV* entry_pv;

#if DEBUG_COMPILE

   DEBUG = false; /* |true|  */

   if (DEBUG)
   { 
       cerr << "*** Parser: `star_vector_assignment: star_vector_variable ASSIGN STARS stars_field_list stars_option_list'."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ******* (7) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

      goto END_STAR_VECTOR_ASSIGNMENT_1;

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ******* (7) @>
@
@<Define rules@>=

   status = get_stars_func(scanner_node, v);

   if (status == 2)
   {
      cerr_strm << "WARNING!  In parser, `star_vector_assignment: star_vector_variable "
                << "ASSIGN STARS stars_field_list stars_option_list':"
                << endl
                << "`Scan_Parse::get_stars_func' returned 2."
                << endl 
                << "Failed to find any `stars' fulfilling the search criteria."
		<< endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      delete scanner_node->stars_get_option_struct;
      scanner_node->stars_get_option_struct = 0;

      @=$$@> = static_cast<void*>(0);

      goto END_STAR_VECTOR_ASSIGNMENT_1;

   }
   else if (status != 0)
   {
      cerr_strm << "ERROR!  In parser, `star_vector_assignment: star_vector_variable "
                << "stars_field_list ASSIGN STARS stars_option_list':"
                << endl
                << "`Scan_Parse::get_stars_func' failed, returning << " << status << "."
		<< endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      delete scanner_node->stars_get_option_struct;
      scanner_node->stars_get_option_struct = 0;

      @=$$@> = static_cast<void*>(0);

      goto END_STAR_VECTOR_ASSIGNMENT_1;


   }
   else if (status == 0 && v.size() == 0)
   {
      cerr_strm << "ERROR!  In parser, `star_vector_assignment: star_vector_variable "
                << "ASSIGN STARS stars_field_list stars_option_list':"
                << endl
                << "`Scan_Parse::get_stars_func' returned 0 (Success) but `vector<Star*> v' is empty."
                << endl 
                << "This shouldn't be possible." << endl 
                << "Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      delete scanner_node->stars_get_option_struct;
      scanner_node->stars_get_option_struct = 0;

      @=$$@> = static_cast<void*>(0);

      goto END_STAR_VECTOR_ASSIGNMENT_1;

   }
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, `star_vector_assignment: star_vector_variable ASSIGN "
                << "STARS stars_field_list stars_option_list':"
                << endl
                << "`Scan_Parse::get_stars_func' succeeded, returning 0  and `vector<Star*> v' is non-empty:"
                << endl 
                << "`v.size()' == " << v.size()
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

#if 0 
   cerr << "Stars:" << endl << endl;

   i = 1;

   for (vector<Star*>::iterator iter = v.begin();
        iter !=  v.end();
        ++iter)
   {

       s << "Star " << i++ << ":";

       (*iter)->show(s.str(), scanner_node->stars_get_option_struct->fields);
  
       s.str("");
   }
#endif 

@q ******* (7) @>
@ 
@<Define rules@>=

    entry_pv = static_cast<PV*>(entry->object);
    
    pv = new PV;

    for (vector<Star*>::iterator iter = v.begin();
         iter != v.end();
         ++iter)
    {
       w.push_back(**iter);
    }

    *pv = w;
         
    scanner_node->last_star_vector = w;

#if 0 
    cerr << "scanner_node->last_star_vector.size() == " << scanner_node->last_star_vector.size()
         << endl;
#endif 

    if (entry_pv)
      entry_pv->clear();

    status = vector_type_assign<Star, Star>(
                    static_cast<Scanner_Node>(parameter),
                    entry,
                    pv);


@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   if (status != 0)
   {

       delete pv;
       pv = 0;     

       @=$$@> = static_cast<void*>(0);

   } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   else /* |status == 0|  */
   {
      delete pv;
      pv = 0;

      @=$$@> = static_cast<void*>(entry->object); 
 
   }  /* |else| (|status == 0|)  */


@q ******* (7) @>

END_STAR_VECTOR_ASSIGNMENT_1:
   if (scanner_node->stars_get_option_struct != 0)
   {
         delete scanner_node->stars_get_option_struct;
         scanner_node->stars_get_option_struct = 0;
   }

}; 

@q ****** (6) @>

@q ***** (5) @>

@q **** (4) |constellation_vector_assignment|.  @>
@*2 \�constellation vector assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> constellation_vector_assignment@>

@q ***** (5) constellation_vector_assignment -->  @>  
@q ***** (5) constellation_vector_variable ASSIGN @>  
@q ***** (5) constellation_vector_expression.     @> 

@*3 \�constellation vector assignment>
$\longrightarrow$ \�constellation vector variable> 
\.{ASSIGN} \�constellation vector expression>.      
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=constellation_vector_assignment: constellation_vector_variable @>  
@=ASSIGN constellation_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Constellation> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Constellation, Constellation>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 


@q **** (4) |planet_vector_assignment|.  @>
@*2 \�planet vector assignment>. 
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> planet_vector_assignment@>

@q ***** (5) planet_vector_assignment -->  @>  
@q ***** (5) planet_vector_variable ASSIGN @>  
@q ***** (5) planet_vector_expression.     @> 

@*3 \�planet vector assignment>
$\longrightarrow$ \�planet vector variable> 
\.{ASSIGN} \�planet vector expression>.      
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=planet_vector_assignment: planet_vector_variable @>  
@=ASSIGN planet_vector_expression@>@/
{

   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2021.06.26.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Planet> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Planet, Planet>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| 
failed. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2021.06.26.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q **** (4) |glyph_vector_assignment|.  @>
@*2 \�glyph vector assignment>. 
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> glyph_vector_assignment@>

@q ***** (5) glyph_vector_assignment -->  glyph_vector_variable ASSIGN @>  
@q ***** (5) glyph_vector_expression.                                  @> 

@*3 \�glyph vector assignment> $\longrightarrow$ \�glyph vector variable> 
\.{ASSIGN} \�glyph vector expression>.      
\initials{LDF 2023.04.10.}

\LOG
\initials{LDF 2023.04.10.}
Added this rule.
\ENDLOG

@q ***** (5) Definition.@> 

@<Define rules@>=

@=glyph_vector_assignment: glyph_vector_variable ASSIGN glyph_vector_expression@>@/
{
   Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>); 

@q ****** (6) Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0)|.@>

@ Error handling for the case that |entry == static_cast<Id_Map_Entry_Node>(0) |.
\initials{LDF 2023.04.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0))
    {

      @=$$@> = static_cast<void*>(0); 

    } /* |if (entry == static_cast<Id_Map_Entry_Node>(0))|  */

@q ****** (6) |entry != static_cast<Id_Map_Entry_Node>(0)|.@>   

@ |entry != static_cast<Id_Map_Entry_Node>(0)|.
\initials{LDF 2023.04.10.}

@<Define rules@>=

  else /* |entry != static_cast<Id_Map_Entry_Node>(0)|  */
    {

   typedef Pointer_Vector<Glyph> PV;
 
   PV* pv = static_cast<PV*>(@=$3@>);

   PV* entry_pv = static_cast<PV*>(entry->object);

   if (entry_pv)
     entry_pv->clear();

   int status = vector_type_assign<Glyph, Glyph>(
                   static_cast<Scanner_Node>(parameter),
                   entry,
                   pv);

@q ******* (7) Error handling:                           @> 
@q ******* (7) |Scan_Parse::vector_type_assign| failed.@> 

@ Error handling:  |Scan_Parse::vector_type_assign| failed. 
\initials{LDF 2023.04.10.}

@<Define rules@>=

if (status != 0)
         {

            delete pv;

            @=$$@> = static_cast<void*>(0);

         } /* |if (status != 0)|  */

@q ******* (7) |Scan_Parse::vector_type_assign| succeeded.@> 

@ |Scan_Parse::vector_type_assign| succeeded. 
\initials{LDF 2023.04.10.}

@<Define rules@>=

   else /* |status == 0|  */
      {
         delete pv;

         @=$$@> = static_cast<void*>(entry->object); 
 
      }  /* |else| (|status == 0|)  */

   }   /* |else| (|entry != static_cast<Id_Map_Entry_Node>(0)|)  */

@q ****** (6).@> 

}; 

@q * (1) |assignment_command|.@>
@** \�assignment command>. 
\initials{LDF Undated.}

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> assignment_command@>

@q ** (2) assignment_command --> intersection_assignment_command.  @>
@* \�assignment command> $\longrightarrow$ 
\�intersection assignment command>.

\LOG
\initials{LDF 2004.09.05.}
Added this rule.

\initials{LDF 2004.11.05.}
Commented-out this rule.
\ENDLOG

@<Garbage@>= 

@q <Define rules@>
 
#if 0 
@=assignment_command: intersection_assignment_command@>
{

  @=$$@> = @=$1@>;
   
};

#endif 

@q ** (2) assignment_command --> transformation_assignment_command.@>
@* \�assignment command> $\longrightarrow$ 
\�transformation assignment command>.

\LOG
\initials{LDF 2004.10.01.}
Added this rule.

\initials{LDF 2004.10.01.}
Now deleting |transformation_assignment_command|.
\ENDLOG
 
@<Define rules@>= 
 
@=assignment_command: transformation_assignment_command@>
{

  delete static_cast<Transform*>(@=$1@>); 

  @=$$@> = static_cast<void*>(0);
   
};

@q * @>

@q   Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables: @>
@q mode:CWEB  @>
@q eval:(outline-minor-mode t)  @>
@q abbrev-file-name:"~/.abbrev_defs" @>
@q eval:(read-abbrev-file)  @>
@q fill-column:80 @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End: @>
