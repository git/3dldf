@q pcommand.w @> 
@q Created by Laurence Finston Wed Jun  2 19:02:39 CEST 2004  @>
     
@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) Commands.  @>
@** Commands.  

\LOG
\initials{LDF 2004.06.02.}  
Created this file.
\ENDLOG 

\TODO
@q { @>
@:TO DO}{{\bf TO DO}@>
@q } @> 
@q !! TO DO:  @>
\initials{LDF 2004.09.22.}
Fix the outline headings and the levels of the starred sections.
\ENDTODO 

@q * (1) command.  @>
@* \�command>.

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> command@>

@q ** (2) command --> label_command.  @>
@*1 \�command> $\longrightarrow$ \�label command>.

\LOG
\initials{LDF 2004.06.30.}  Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=command: label_command@>
{

  @=$$@> = @=$1@>;

};

@q ** (2) command --> message_command.  @>
@*1 \�command> $\longrightarrow$ \�message command>.

\LOG
\initials{LDF 2004.09.22.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=command: message_command@>
{

  @=$$@> = @=$1@>;

};

@q ** (2) command --> CLIP_TO closed_path_expression with_projection_optional with_focus_optional.@> 
@*2 \�command> $\longrightarrow$ \.{CLIP\_TO} \�closed path expression> \�with projection optional>
\�with focus optional>.
\initials{LDF 2005.08.16.}

\LOG
\initials{LDF 2005.08.16.}
Added this rule.

\initials{LDF 2005.10.24.}
Changed |path_like_expression| to |path_expression|.
Removed debugging code.
\ENDLOG

@<Define rules@>=
@=command: CLIP_TO closed_path_expression with_projection_optional with_focus_optional@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: CLIP_TO closed_path_expression with_projection_optional "
                << "with_focus_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Path  *p = static_cast<Path*>(@=$2@>);
   Focus *f = static_cast<Focus*>(@=$4@>);

   int projection_type;

   clip_to_func(p, @=$3@>, f, scanner_node);

   if (p != 0)
   {
      delete p;
      p = 0;
   }

   if (f != 0)
   {
      delete f;
      f = 0;
   }

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) command --> CLIP picture_expression TO closed_path_expression.@> 
@*2 \�command> $\longrightarrow$ \.{CLIP} \�picture expression> \.{TO} \�closed path expression>.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: CLIP picture_expression TO closed_path_expression@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: CLIP picture_expression TO closed_path_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);  

   Picture *pic = 0;    

   if (entry && entry->object)
      pic = static_cast<Picture*>(entry->object);
   
   Path *p   = static_cast<Path*>(@=$4@>);

#if 0
   if (pic != 0)
      cerr << "pic is non-NULL." << endl;
   else 
      cerr << "pic is NULL." << endl;
#endif 

   if (pic && p)
   {
      if (pic->clip_path != 0)
        delete pic->clip_path;

      pic->clip_path = p;

#if 0 
   cerr << "In conditional:" << endl;

   if (pic->clip_path != 0)
      cerr << "pic->clip_path is non-NULL." << endl;
   else 
      cerr << "pic->clip_path is NULL." << endl;
#endif 

   }

   else if (p)
   {
      delete p;
      p = 0;
   }

#if 0 
   cerr << "After conditional:" << endl;

   if (pic->clip_path != 0)
      cerr << "pic->clip_path is non-NULL." << endl;
   else 
      cerr << "pic->clip_path is NULL." << endl;
#endif

   @=$$@> = 0;

};

@q ** (2) command --> UNCLIP picture_expression.@> 
@*2 \�command> $\longrightarrow$ \.{UNCLIP} \�path expression>.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: UNCLIP picture_expression@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: UNCLIP picture_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);  

   Picture *pic = 0;    

   if (entry && entry->object)
      pic = static_cast<Picture*>(entry->object);
   
   if (pic && pic->clip_path)
   {
      delete pic->clip_path;
      pic->clip_path = 0;
   }

   @=$$@> = 0;

};

@q ** (2) |with_focus_optional|.  @>
@ |with_focus_optional|.  
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_focus_optional@>

@q ** (2) with_focus_optional: /* Empty  */   @>

@*1 \�with focus optional> $\longrightarrow$ \.{Empty}.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_focus_optional: /* Empty  */@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_focus_optional: /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = static_cast<void*>(0);

};

@q ** (2) with_focus_optional: WITH_FOCUS focus_expression  @>
@*1 \�with focus optional> $\longrightarrow$ \.{WITH\_FOCUS} \�focus expression>.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_focus_optional: WITH_FOCUS focus_expression@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_focus_optional: focus_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = @=$2@>;

};

@q ** (2) |with_projection_optional|.  @>
@ |with_projection_optional|.  
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_projection_optional@>

@q ** (2) with_projection_optional: /* Empty  */   @>

@*1 \�with projection optional> $\longrightarrow$ \.{Empty}.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_projection_optional: /* Empty  */@>
{

  @=$$@> = Projections::persp;

};

@q ** (2) with_projection_optional: WITH_PROJECTION projection_type  @>
@*1 \�with projection optional> $\longrightarrow$ \.{WITH\_PROJECTION} \�projection type>.
\initials{LDF 2023.06.29.}

\LOG
\initials{LDF 2023.06.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_projection_optional: WITH_PROJECTION projection_type@>
{

  @=$$@> = @=$2@>;

};

@q ** (2) command --> group_command.  @>
@*1 \�command> $\longrightarrow$ \�group command>.

\LOG
\initials{LDF 2004.09.12.}  
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=command: group_command@>
{

  @=$$@> = @=$1@>;

};

@q * (1) message_command.@> 
@* \�message command>.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2004.09.22.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> message_command@>

@q ** (2) message_or_errmessage.@> 
@*1 \�message or errmessage>.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2004.09.22.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> message_or_errmessage@>

@q ** (2) message_or_errmessage --> MESSAGE.@> 
@*1 \�message or errmessage> $\longrightarrow$ \.{MESSAGE}.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2004.09.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=message_or_errmessage: MESSAGE@>@/
{

  @=$$@> = MESSAGE;

};

@q ** (2) message_or_errmessage --> ERRMESSAGE.@> 
@*1 \�message or errmessage> $\longrightarrow$ \.{ERRMESSAGE}.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2004.09.22.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=message_or_errmessage: ERRMESSAGE@>@/
{

  @=$$@> = ERRMESSAGE;

};

@q * (1) message_command --> message_or_errmessage string_expression.@>

@* \�message command> $\longrightarrow$ \�message or errmessage> \.{string expression}.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2005.11.03.}
Changed |STRING| to |string_expression|.  
Removed code.  Now calling |Scan_Parse::message_command_func| instead.
\ENDLOG 

@<Define rules@>=
@=message_command: message_or_errmessage string_expression@>@/
{

    Scan_Parse::message_command_func(@=$1@>, @=$2@>, parameter);
    @=$$@> = static_cast<void*>(0);
};

@q ** (2) command --> PAUSE.@> 
@*1 \�command> $\longrightarrow$ \.{PAUSE}.
\initials{LDF 2004.09.22.}

\LOG
\initials{LDF 2004.09.22.}
Added this rule.

\initials{LDF 2005.11.09.}
Removed debugging code.  Added code for pausing.  
I must have removed it at some time in the past.
\ENDLOG

@<Define rules@>=
@=command: PAUSE@>@/
{

    cerr_mutex.lock(); 
    cerr << "Type <RETURN> to continue: ";  /* Don't delete this!  */
    getchar();                              /* Don't delete this!  */
    cerr_mutex.unlock();  

    @=$$@> = static_cast<void*>(0);
};

@q ** (2) command --> SCANTOKENS string_expression.@> 
@*1 \�command> $\longrightarrow$ \.{SCANTOKENS}.
\�string expression>.
\initials{LDF 2004.12.01.}

\LOG
\initials{LDF 2004.12.01.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SCANTOKENS string_expression@>@/
{

   string* str_ptr = static_cast<string*>(@=$2@>); 

   if (str_ptr == static_cast<string*>(0))
     {
     
         @=$$@> = static_cast<void*>(0);

    }  /* |if (str_ptr == 0)|  */

   else /* |str_ptr != 0|  */
      {

          stringstream* str_strm_ptr;

          Input_Struct* curr_input_struct;

          str_strm_ptr = new stringstream;
          curr_input_struct = new Input_Struct;

          *str_strm_ptr << *str_ptr << ";";

          curr_input_struct->up = static_cast<Scanner_Node>(parameter)->in;
          curr_input_struct->stream_ptr 
            = static_cast<istream*>(str_strm_ptr);

          curr_input_struct->type = Io_Struct::SCANTOKENS_STRING_TYPE;
           
          static_cast<Scanner_Node>(parameter)->in = curr_input_struct;

          @=$$@> = static_cast<void*>(0);

      }   /* |else| (|str_ptr != 0|)  */
};

@q ** (2) verbatim_command. @>

@ \�verbatim command>.
\initials{LDF 2022.04.18.}

\LOG
\initials{LDF 2022.04.18.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> verbatim_command@>

@q ** (2) verbatim_command --> VERBATIM_METAPOST.@> 
@*1 \�verbatim command> $\longrightarrow$ \.{VERBATIM\_METAPOST}.
\initials{LDF 2022.04.18.}

\LOG
\initials{LDF 2022.04.18.}
Added this rule.
\ENDLOG

@q *** (3).@> 

@<Define rules@>=
@=verbatim_command: VERBATIM_METAPOST@>@/
{
   @=$$@> = Scan_Parse::VERBATIM_METAPOST_COMMAND;
 
};

@q ** (2) verbatim_command --> VERBATIM_TEX.@> 
@*1 \�verbatim command> $\longrightarrow$ \.{VERBATIM\_TEX}.
\initials{LDF 2022.04.18.}

\LOG
\initials{LDF 2022.04.18.}
Added this rule.
\ENDLOG

@q *** (3).@> 

@<Define rules@>=
@=verbatim_command: VERBATIM_TEX@>@/
{
   @=$$@> = Scan_Parse::VERBATIM_TEX_COMMAND;
 
};

@q ** (2) verbatim_command --> VERBATIM_METAFONT.@> 
@*1 \�verbatim command> $\longrightarrow$ \.{VERBATIM\_METAFONT}.
\initials{LDF 2022.04.18.}

\LOG
\initials{LDF 2022.04.18.}
Added this rule.
\ENDLOG

@q *** (3).@> 

@<Define rules@>=
@=verbatim_command: VERBATIM_METAFONT@>@/
{
   @=$$@> = Scan_Parse::VERBATIM_METAFONT_COMMAND;
 
};

@q ** (2) command --> verbatim_command string_expression.@> 
@*1 \�command> $\longrightarrow$ \�verbatim command> \�string expression>.
\initials{LDF 2022.04.18.}

\LOG
\initials{LDF 2022.04.18.}
Added this rule.
\ENDLOG

@q *** (3).@> 

@<Define rules@>=
@=command: verbatim_command string_expression@>@/
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: verbatim_command string_expression."
                << endl 
                << "`verbatim_command' == $1 == " << @=$1@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   string* s = static_cast<string*>(@=$2@>); 

@q **** (4) Error handling:  |s == 0|.@>   

@ Error handling:  |s == 0|.
\initials{LDF 2004.12.13.}

@<Define rules@>=

   if (s == static_cast<string*>(0))
   {
       @=$$@> = static_cast<void*>(0);

   } /* |s == 0|  */

@q **** (4) |s != 0|.@>   

@ |s != 0|.
\initials{LDF 2004.12.13.}

@<Define rules@>=

   else /* |s != 0|  */
   {
         int status = verbatim_func(static_cast<Scanner_Node>(parameter), s, @=$1@>);

@q ***** (5) Error handling:  |verbatim_func| failed.@>   

@ Error handling:  |verbatim_func| failed.
\initials{LDF 2004.12.13.}

@<Define rules@>=
 
         if (status != 0)
         {
                cerr_strm << thread_name 
                          << "ERROR!  In `yyparse()', rule "
                          << "`command: verbatim_command string_expression':"
                          << endl << "`verbatim_func' failed.  "
                          << "Will try to continue.";

                log_message(cerr_strm); 
                cerr_message(cerr_strm, error_stop_value); 
                cerr_strm.str("");

         } /* |if (status != 0)|  */

@q ***** (5) @>   

         delete s;

         @=$$@> = static_cast<void*>(0);

@q ***** (5).@> 

   }   /* |else| (|s != 0|)  */  

};

@q **** (4) command --> PLOT STARS sphere_expression stars_option_list @>

@*3 \�command> $\longrightarrow$ \.{PLOT} \.{STARS} \�sphere expression> \�stars option list>.
\initials{LDF 2021.06.26.}

\LOG
\initials{LDF 2021.06.26.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
  
@=command: PLOT STARS sphere_expression stars_option_list@>@/
{
@q ******* (7) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: `command --> PLOT STARS sphere_expression stars_option_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    status = plot_stars_func(static_cast<Sphere*>(@=$3@>), scanner_node);

    if (status != 0)
    {
      cerr_strm << "ERROR!  In Parser: `command --> PLOT STARS sphere_expression stars_option_list':"
                << "`Scan_Parse::plot_stars_func' failed, returning " << status << "."
                << endl
                << "Failed to plot `stars'.  Will try to continue."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#if DEBUG_COMPILE
    else if (DEBUG)
    { 
      cerr_strm << "*** Parser: `command --> PLOT STARS sphere_expression stars_option_list':"
                << "`Scan_Parse::plot_stars_func' succeeded, returning 0."
                << endl
                << "Plotted `stars' successfully."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }  
#endif /* |DEBUG_COMPILE|  */@; 

    @=$$@> = static_cast<void*>(0);

@q ******* (7) @>

};

@q **** (4) command --> WRITE newwrite_variable string_expression no_newline_optional. @>

@*3 \�command> $\longrightarrow$ \.{WRITE} \�newwrite variable> \�string_expression> \�no newline optional>.
\initials{LDF 2021.07.05.}

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
  
@=command: WRITE newwrite_variable string_expression no_newline_optional@>@/
{
@q ******* (7) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: `command --> WRITE newwrite_variable string_expression no_newline_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    Newwrite *nw = 0;    

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

    string *s = static_cast<string*>(@=$3@>);

#if 0
    cerr << "*s == " << *s << endl;
#endif

    if (entry == 0)
      cerr << "entry is NULL." << endl;
    else if (entry->object == 0)
      cerr << "entry->object is NULL." << endl;

    if (entry != 0 && entry->object != 0)
    {
        nw = static_cast<Newwrite*>(entry->object); 
        
        if (nw->out_strm.is_open())
        {
            nw->out_strm << *s;

            if (@=$4@> == 0)
              nw->out_strm << endl;
        }
    }

    delete s;
    s = 0;

    @=$$@> = static_cast<void*>(0);

@q ******* (7) @>

};

@q **** (4) command --> WRITE string_expression TO newwrite_variable no_newline_optional. @>

@*3 \�command> $\longrightarrow$ \.{WRITE} \�string_expression> \.{TO} \�newwrite variable> \�no newline optional>.
\initials{LDF 2021.07.05.}

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
  
@=command: WRITE string_expression TO newwrite_variable no_newline_optional@>@/
{
@q ******* (7) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: `command --> WRITE string_expression TO newwrite_variable no_newline_optional'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    Newwrite *nw = 0;    

    entry = static_cast<Id_Map_Entry_Node>(@=$4@>); 

    string *s = static_cast<string*>(@=$2@>);

#if 0
    cerr << "*s == " << *s << endl;
#endif 

    if (entry == 0)
      cerr << "entry is NULL." << endl;
    else if (entry->object == 0)
      cerr << "entry->object is NULL." << endl;

    if (entry != 0 && entry->object != 0)
    {
        nw = static_cast<Newwrite*>(entry->object); 
        
        if (nw->out_strm.is_open())
        {
            nw->out_strm << *s;

            if (@=$5@> == 0)
              nw->out_strm << endl;
        }
    }

    delete s;
    s = 0;

    @=$$@> = static_cast<void*>(0);

@q ******* (7) @>

};

@q ***** (5) no_newline_optional.  @>
@* \�no newline optional>.
\initials{LDF 2021.07.05.}

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> no_newline_optional@>

@q ***** (6) no_newline_optional --> EMPTY.  @>
@*5 \�no newline optional> $\longrightarrow$ \.{EMPTY}.

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=no_newline_optional: /* Empty  */@>
{

  @=$$@> = 0;

};

@q ***** (6) no_newline_optional --> NO_NEWLINE.  @>
@*5 \�no newline optional> $\longrightarrow$ \.{NO_NEWLINE}.

\LOG
\initials{LDF 2021.07.05.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=no_newline_optional: NO_NEWLINE@>
{
  @=$$@> = 1;
};

@q ***** (5) @>

@q **** (4) command --> CLOSE newwrite_variable. @>

@*3 \�command> $\longrightarrow$ \.{CLOSE} \�newwrite_variable>.
\initials{LDF 2021.07.06.}

\LOG
\initials{LDF 2021.07.06.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>= 
  
@=command: CLOSE newwrite_variable@>@/
{
@q ******* (7) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << "*** Parser: `command --> CLOSE newwrite_variable'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
      
    }
#endif /* |DEBUG_COMPILE|  */@;

    Newwrite *nw = 0;    

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 

    if (entry == 0)
    {
        cerr << "entry is NULL." << endl;
    }

    else if (entry->object == 0)
    {
       cerr << "entry->object is NULL." << endl;
    }

    if (entry != 0 && entry->object != 0)
    {
        nw = static_cast<Newwrite*>(entry->object); 
        
        if (nw->out_strm.is_open())
        {
           nw->out_strm.close();
        }
        else 
        {
           cerr_strm << "WARNING!  In parser, `command --> CLOSE newwrite_variable':"
                     << endl
                     << "`nw->out_strm' isn't open.  Not closing.  Continuing."
                     << endl;

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

        }
    }

    @=$$@> = static_cast<void*>(0);

@q ******* (7) @>

};

@q **** (4) command --> SET_NAME color_variable TO string_expression.@>

@*4 \�command> $\longrightarrow$ \.{SET\_NAME} \�color variable primary> \.{TO} \�string expression>.
\initials{LDF 2021.11.12.}

The \.{TO} token is needed in order for the \�string expression> to be recognized by the 
scanner/parser pair.  I don't know why this is so.
\initials{LDF 2021.11.13.}

\LOG
\initials{LDF 2021.11.12.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=command: SET_NAME color_variable TO string_expression@>@/
{ 
   @<Common declarations for rules@>@; 

   string *s = static_cast<string*>(@=$4@>);

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
     {
         cerr_strm << thread_name << "*** Parser:  `command --> SET_NAME color_variable TO "
                   << "string_expression'." << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");

         cerr << "*s == " << *s << endl;

     }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @> 

     entry = static_cast<Id_Map_Entry_Node>(@=$2@>);  

     Color *c = static_cast<Color*>(entry->object);

     if (c != 0)
     {
        c->set_name(*s);
     }

     delete s;
     s = 0;   

     @=$$@> = 0;

};

@q **** (4) command --> UNSET_NAME color_variable.@>

@*4 \�command> $\longrightarrow$ \.{UNSET\_NAME} \�color variable primary>.
\initials{LDF 2021.11.13.}

\LOG
\initials{LDF 2021.11.13.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=command: UNSET_NAME color_variable@>@/
{ 
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG) 
     {
         cerr_strm << thread_name << "*** Parser:  `command --> UNSET_NAME color_variable'."
                   << endl;

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */

@q ******* (7) @> 

     entry = static_cast<Id_Map_Entry_Node>(@=$2@>);  

     Color *c = static_cast<Color*>(entry->object);

     if (c != 0)
     {
        c->set_name("");
     }

     @=$$@> = 0;

};

@q **** (4) @>

@q *** (3) @>

@q ** (2) numeric_list_optional.  @>

@ \�numeric list optional>.
\initials{LDF 2022.04.28.}

\LOG
\initials{LDF 2022.04.28.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_list_optional@>

@q ** (2) numeric_list_optional: /* Empty  */@>

@ \�numeric list optional> $\longrightarrow$ \.{EMPTY}.
\initials{LDF 2022.04.28.}

\LOG
\initials{LDF 2022.04.28.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_list_optional: /* Empty  */@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `numeric_list_optional: /* Empty */"
                << endl;
        
      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = static_cast<void*>(0);

};

@q ** (2) numeric_list_optional: numeric_list @>

@ \�numeric list optional> $\longrightarrow$ \�numeric list>.
\initials{LDF 2022.04.28.}

\LOG
\initials{LDF 2022.04.28.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_list_optional: numeric_list@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `numeric_list_optional: numeric_list."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = static_cast<void*>(@=$1@>);
};

@q ** (2) numeric_list: WITH_VALUE LEFT_PARENTHESIS numeric_expression RIGHT_PARENTHESIS @>

@ \�numeric list> $\longrightarrow$ \.{WITH\_VALUE} \.{LEFT\_PARENTHESIS} \�numeric expression>
\.{RIGHT\_PARENTHESIS}.
\initials{LDF 2023.04.17.}

\LOG
\initials{LDF 2023.04.17.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=numeric_list: WITH_VALUE LEFT_PARENTHESIS numeric_expression RIGHT_PARENTHESIS @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `numeric_list: WITH_VALUE LEFT_PARENTHESIS numeric_expression "
                << "RIGHT_PARENTHESIS."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Pointer_Vector<real> *pv = new Pointer_Vector<real>;

   *pv += @=$3@>;

   @=$$@> = static_cast<void*>(pv);
};

@q ** (2) flatten_command @>

@ \�flatten command>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> flatten_command@>

@q ** (2) flatten_command: FLATTEN_X  @>

@ \�flatten command> $\longrightarrow$ \.{FLATTEN\_X}.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=flatten_command: FLATTEN_X@>
{
   @=$$@> = 0;
};

@q ** (2) flatten_command: FLATTEN_Y  @>

@ \�flatten command> $\longrightarrow$ \.{FLATTEN\_Y}.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG

@q ** (2) flatten_command: FLATTEN_Y@>

@ \�flatten command> $\longrightarrow$ \.{FLATTEN\_Y}.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=flatten_command: FLATTEN_Y@>
{
   @=$$@> = 1;
};

@q ** (2) flatten_command: FLATTEN_Z  @>
@ \�flatten command> $\longrightarrow$ \.{FLATTEN\_Z}.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=flatten_command: FLATTEN_Z@>
{
   @=$$@> = 2;
};

@q ** (2) command: flatten_command path_variable @>

@ command> $\longrightarrow$ \.{RESET\_ARC} \�numeric list optional>.
\�command> \�flatten command> \�path variable> \�numeric optional>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command path_variable numeric_optional@>
{
@q *** (3) @>

     @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command path_variable numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Path *p = static_cast<Path*>(entry->object);
       p->flatten(@=$1@>, r);
#if 0 
       p->show("*p:");
#endif 
   }

   @=$$@> = static_cast<void*>(0);

@q *** (3) @>

};


@q ** (2) command: flatten_command ellipse_variable numeric_optional. @>

@ command> $\longrightarrow$ \.{RESET\_ARC} \�numeric list optional>.
\�command> \�flatten command> \�ellipse variable> \�numeric optional>.
\initials{LDF 2022.12.24.}

\LOG
\initials{LDF 2022.12.24.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command ellipse_variable numeric_optional@>
{
@q *** (3) @>

     @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command ellipse_variable numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Ellipse *e = static_cast<Ellipse*>(entry->object);
       e->flatten(@=$1@>, r);
#if 0 
       e->show("*e:");
#endif 
   }

   @=$$@> = static_cast<void*>(0);

@q *** (3) @>

};

@q ** (2) command: flatten_command rectangle_variable numeric_optional. @>

@ command> $\longrightarrow$ \.{RESET\_ARC} \�numeric list optional>.
\�command> \�flatten command> \�rectangle variable> \�numeric optional>.
\initials{LDF 2023.07.12.}

\LOG
\initials{LDF 2023.07.12.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command rectangle_variable numeric_optional@>
{
@q *** (3) @>

     @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command rectangle_variable numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Rectangle *e = static_cast<Rectangle*>(entry->object);
       e->flatten(@=$1@>, r);
#if 0 
       e->show("*e:");
#endif 
   }

   @=$$@> = static_cast<void*>(0);

@q *** (3) @>

};

@q ** (2) command: flatten_command circle_variable numeric_optional. @>

@ command> $\longrightarrow$ \.{RESET\_ARC} \�numeric list optional>.
\�command> \�flatten command> \�circle variable> \�numeric optional>.
\initials{LDF 2023.07.12.}

\LOG
\initials{LDF 2023.07.12.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command circle_variable numeric_optional@>
{
@q *** (3) @>

     @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command circle_variable numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Circle *e = static_cast<Circle*>(entry->object);
       e->flatten(@=$1@>, r);
#if 0 
       e->show("*e:");
#endif 
   }

   @=$$@> = static_cast<void*>(0);

@q *** (3) @>

};





@q ** (2) command: flatten_command picture_variable @>

@ command> $\longrightarrow$ \.{RESET\_ARC} \�numeric list optional>.
\�command> \�flatten command> \�picture variable> \�numeric optional>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command picture_variable numeric_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command picture_variable."
                << endl 
                << "`flatten_command' == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Picture *p = static_cast<Picture*>(entry->object);
       p->flatten(@=$1@>, r);
   }

   @=$$@> = static_cast<void*>(0);
};

@q ** (2) command: flatten_command point_variable numeric_optional @>

@ command> $\longrightarrow$ \�command> \�flatten command> \�point variable> 
\�numeric optional>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command point_variable numeric_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command point_variable numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Point *p = static_cast<Point*>(entry->object);
       p->flatten(@=$1@>, r);
#if 0 
       p->show("*p:");
#endif 
   }

   @=$$@> = static_cast<void*>(0);
};

@q ** (2) command: flatten_command point_vector_variable numeric_optional @>

@ command> $\longrightarrow$ \�command> \�flatten command> \�point vector variable> 
\�numeric optional>.
\initials{LDF 2022.05.03.}

\LOG
\initials{LDF 2022.05.03.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command point_vector_variable numeric_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command point_vector_variable "
                << "numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Pointer_Vector<Point> *pv = static_cast<Pointer_Vector<Point>*>(entry->object);
  
       for (vector<Point*>::iterator iter = pv->v.begin();
            iter != pv->v.end();
            ++iter)
       {
          (*iter)->flatten(@=$1@>, r);
       }
   }

   @=$$@> = static_cast<void*>(0);
};

@q ** (2) command: flatten_command path_vector_variable numeric_optional @>

@ command> $\longrightarrow$ \�command> \�flatten command> \�path vector variable> 
\�numeric optional>.
\initials{LDF 2022.05.03.}

\LOG
\initials{LDF 2022.05.03.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command path_vector_variable numeric_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command path_vector_variable "
                << "numeric_optional."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Pointer_Vector<Path> *pv = static_cast<Pointer_Vector<Path>*>(entry->object);
  
       for (vector<Path*>::iterator iter = pv->v.begin();
            iter != pv->v.end();
            ++iter)
       {
          (*iter)->flatten(@=$1@>, r);
       }
   }

   @=$$@> = static_cast<void*>(0);
};

@q ** (2) command: flatten_command picture_vector_variable numeric_optional @>

@ command> $\longrightarrow$ \�command> \�flatten command> \�picture vector variable> 
\�numeric optional>.
\initials{LDF 2022.05.03.}

\LOG
\initials{LDF 2022.05.03.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: flatten_command picture_vector_variable numeric_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: flatten_command picture_vector_variable "
                << "numeric_optional'."
                << endl 
                << "`flatten_command'  == $1 == " << @=$1@> << endl
                << "`numeric_optional' == $3 == " << @=$3@> << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   real r = @=$3@>;

   if (entry && entry->object)
   {
       Pointer_Vector<Picture> *pv 
          = static_cast<Pointer_Vector<Picture>*>(entry->object);
  
       for (vector<Picture*>::iterator iter = pv->v.begin();
            iter != pv->v.end();
            ++iter)
       {
          (*iter)->flatten(@=$1@>, r);
       }
   }

   @=$$@> = static_cast<void*>(0);
};

@q ** (2) numeric_optional @>

@ \�numeric optional>.
\initials{LDF 2022.05.02.}

\LOG
\initials{LDF 2022.05.02.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <real_value> numeric_optional@>

@q ** (2) numeric_optional: /* Empty  */@>
@
@<Define rules@>= 

@=numeric_optional: /* Empty  */@>
{
   @=$$@> = 0.0;
};

@q ** (2) numeric_optional: COMMA numeric_expression @>
@
@<Define rules@>= 

@=numeric_optional: COMMA numeric_expression @>
{
   @=$$@> = @=$2@>;
};


@q ** (2) command: PROJECT point_variable WITH_FOCUS focus_primary with_z_optional@>

@ command> $\longrightarrow$ \.{PROJECT} \�point variable> 
\�focus primary> \�with z optional>.
\initials{LDF 2023.11.13.}

\LOG
\initials{LDF 2023.11.13.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: PROJECT point_variable WITH_FOCUS focus_primary with_z_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: PROJECT point_variable WITH_FOCUS "
                << "focus_primary with_z_optional."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Focus *f = static_cast<Focus*>(@=$4@>);

   Point *q = 0;

   if (entry && entry->object)
   {
       Point *p = static_cast<Point*>(entry->object);
       q = static_cast<Point*>(Scan_Parse::point_secondary_rule_func_0(static_cast<void*>(p), 
                                                                       static_cast<void*>(f), 
                                                                       @=$5@>, 
                                                                       scanner_node));
       *p = *q;
#if 0
       delete q;
       q = 0;
#endif 
   }

#if 0 
/* !! START HERE:  LDF 2023.11.13.  Check whether foci are persistent.  Deleting
   |f| here causes a segmentation fault.  */
   delete f;
   f = 0;
#endif 

   @=$$@> = static_cast<void*>(0);
};


@q *** (3) command: REVERSE path_variable  @>
@*2 \�command> $\longrightarrow$ \.{REVERSE} \�path variable>.

\LOG
\initials{LDF 2004.05.13.}  
Added this rule.

\initials{LDF 2004.11.26.}
Changed |Path::reverse|, so that cyclical |Paths| can be reversed. 

\initials{LDF 2005.11.09.}
Removed debugging code.

\initials{LDF 2022.05.04.}
Changed this rule from \�path primary> $\longrightarrow$ \.{REVERSE}
\�path primary> to \�command> $\longrightarrow$ \.{REVERSE} 
\�path variable>.
\ENDLOG 

@<Define rules@>=
@=command: REVERSE path_variable@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command:  REVERSE path_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
     Path* p = static_cast<Path*>(entry->object); 
     p->reverse(1, static_cast<Scanner_Node>(parameter));
   }

  @=$$@> = static_cast<void*>(0);  

};

@q *** (3) command: REVERSE ellipse_variable  @>
@*2 \�command> $\longrightarrow$ \.{REVERSE} \�ellipse variable>.

\LOG
\initials{LDF 2024.06.13.}
Added this rule.
\ENDLOG 

@<Define rules@>=
@=command: REVERSE ellipse_variable@>@/
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command:  REVERSE ellipse_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
     Ellipse* e = static_cast<Ellipse*>(entry->object); 
     e->reverse(1, static_cast<Scanner_Node>(parameter));
   }

  @=$$@> = static_cast<void*>(0);  

};

@q ** (2) command: CALL_METAPOST string_expression LEFT_PARENTHESIS path_vector_variable_optional @>
@q point_vector_variable_optional numeric_vector_variable_optional RIGHT_PARENTHESIS              @>
@q call_metapost_option_list                                                                      @>


@ \�command> $\longrightarrow$ \.{CALL\_METAPOST} \�string expression>  $\ldots$
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: CALL_METAPOST string_expression LEFT_PARENTHESIS path_vector_variable_optional @>
@=point_vector_variable_optional numeric_vector_variable_optional RIGHT_PARENTHESIS @>
@=call_metapost_option_list @>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: CALL_METAPOST path_vector_variable string_expression "
                << "call_metapost_option_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   string *str = static_cast<string*>(@=$2@>);
  
   Id_Map_Entry_Node path_entry = 0;
   Id_Map_Entry_Node point_entry = 0;
   Id_Map_Entry_Node real_entry = 0;


   if (@=$4@> != 0)
      path_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (!path_entry)
      cerr << "path_entry is NULL." << endl;
   else
   {
      cerr << "path_entry is non-NULL." << endl;
   }

   if (@=$5@> != 0)
      point_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (!point_entry)
      cerr << "point_entry is NULL." << endl;
   else
   {
      cerr << "point_entry is non-NULL." << endl;
   }

   if (@=$6@> != 0)
      real_entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (!real_entry)
      cerr << "real_entry is NULL." << endl;
   else
   {
      cerr << "real_entry is non-NULL." << endl;
   }

@q *** (3) @>
@
@<Define rules@>= 

   bool save               = @=$8@> & 1U;
   bool clear              = @=$8@> & 2U;
   bool suppress_mp_stdout = @=$8@> & 4U;
   bool do_transform       = @=$8@> & 8U;

   status = call_metapost(*str, 
                          path_entry, 
                          point_entry, 
                          real_entry, 
                          save, 
                          suppress_mp_stdout, 
                          scanner_node);

   if (status != 0)
   {
     cerr_strm << "ERROR!  In parser, `command: CALL_METAPOST string_expression "
                << "path_vector_variable call_metapost_option_list':"
                << endl 
                << "`call_metapost' failed, returning " << status << "."
                << endl
                << "Failed to execute MetaPost in child process."
                << endl
                << "Continuing.";

     log_message(cerr_strm);
     cerr_message(cerr_strm, true);
     cerr_strm.str("");
 

   }  /* |if (status != 0)| */

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
     cerr_strm << "In parser, `command: CALL_METAPOST string_expression "
                << "path_vector_variable call_metapost_option_list':"
                << endl 
                << "`call_metapost' succeeded, returning 0.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     if (scanner_node->metapost_output_struct)
        cerr << "scanner_node->metapost_output_struct->path_vector.size() == " 
             << scanner_node->metapost_output_struct->path_vector.size() << endl;
     else 
        cerr << "scanner_node->metapost_output_struct is NULL." << endl;


   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = 0;
};

@q ** (2) call_metapost_option_list @>

@ \�call metapost option list>.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <uint_value> call_metapost_option_list@>

@q *** (3) call_metapost_option_list: /* Empty  */ @>

@ \�call metapost option list> $\longrightarrow$ \.{Empty}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: /* Empty  */ @>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: /* Empty */'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 8U | 64U;  /* The defaults for |do_transform| and |do_test| are |true|.  
                          \initials{LDF 2022.05.29.}  */


};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> path_vector_variable_optional@>

@q ** (2) @>
@
@<Define rules@>= 
@=path_vector_variable_optional: /* Empty  */@>
{
  @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>= 
@=path_vector_variable_optional: path_vector_variable@>
{
  @=$$@> = @=$1@>;
};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> point_vector_variable_optional@>

@q ** (2) @>
@
@<Define rules@>= 
@=point_vector_variable_optional: /* Empty  */@>
{
  @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>= 
@=point_vector_variable_optional: COMMA point_vector_variable@>
{
  @=$$@> = @=$2@>;
};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> numeric_vector_variable_optional@>

@q ** (2) @>
@
@<Define rules@>= 
@=numeric_vector_variable_optional: /* Empty  */@>
{
  @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>= 
@=numeric_vector_variable_optional: COMMA numeric_vector_variable@>
{
  @=$$@> = @=$2@>;
};


@q *** (3) call_metapost_option_list: call_metapost_option_list SAVE @>

@ \�call metapost option list> $\longrightarrow$ \�call metapost option list> \.{SAVE}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list SAVE @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list SAVE'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> |= 1U; 

};

@q *** (3) call_metapost_option_list: NO_SAVE @>

@ \�call metapost option list> $\longrightarrow$ \.{NO\_SAVE}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list NO_SAVE @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list NO_SAVE'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> &= ~1U; 

};

@q *** (3) call_metapost_option_list: call_metapost_option_list CLEAR @>

@ \�call metapost option list> $\longrightarrow$ \�call metapost option list> \.{CLEAR}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list CLEAR @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list CLEAR'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 


      @=$$@> = @=$1@> |= 2U;

};

@q *** (3) call_metapost_option_list: NO_CLEAR @>

@ \�call metapost option list> $\longrightarrow$ \.{NO\_CLEAR}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list NO_CLEAR @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list NO_CLEAR'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> &= ~2U;

};

@q *** (3) call_metapost_option_list: SUPPRESS @>

@ \�call metapost option list> $\longrightarrow$ \.{SUPPRESS}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list SUPPRESS @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list SUPPRESS'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> |= 4U;

};

@q *** (3) call_metapost_option_list: NO_SUPPRESS @>

@ \�call metapost option list> $\longrightarrow$ \.{NO\_SUPPRESS}.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list NO_SUPPRESS @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list NO_SUPPRESS'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> &= ~4U;

};

@q *** (3) call_metapost_option_list: WITH_TRANSFORM @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_TRANSFORM}.
\initials{LDF 2022.05.08.}

\LOG
\initials{LDF 2022.05.08.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_TRANSFORM @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list WITH_TRANSFORM'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> |= 8U;

};

@q *** (3) call_metapost_option_list: WITH_NO_TRANSFORM @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_NO\_TRANSFORM}.
\initials{LDF 2022.05.08.}

\LOG
\initials{LDF 2022.05.08.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_NO_TRANSFORM @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list WITH_NO_TRANSFORM'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> &= ~8U;

};

@q *** (3) call_metapost_option_list: WITH_X_AXIS_POINT @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_X\_AXIS\_POINT}
\�point expression>.
\initials{LDF 2022.05.18.}

\LOG
\initials{LDF 2022.05.18.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_X_AXIS_POINT point_expression@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list "
                << "WITH_X_AXIS_POINT point_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->x_axis_pt = static_cast<Point*>(@=$3@>);

   @=$$@> = @=$1@> |= 16U;

};

@q *** (3) call_metapost_option_list: WITH_ORIGIN_POINT @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_X\_AXIS\_POINT}
\�point expression>.
\initials{LDF 2022.05.18.}

\LOG
\initials{LDF 2022.05.18.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_ORIGIN_POINT point_expression@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list "
                << "WITH_ORIGIN_POINT point_expression'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->origin_pt = static_cast<Point*>(@=$3@>);

   @=$$@> = @=$1@> |= 32U;

};

@q *** (3) call_metapost_option_list: WITH_TEST @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_TEST}.
\initials{LDF 2022.05.29.}

\LOG
\initials{LDF 2022.05.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_TEST@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list "
                << "WITH_TEST'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> |= 64U;

};

@q *** (3) call_metapost_option_list: WITH_NO_TEST @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_NO\_TEST}.
\initials{LDF 2023.04.20.}

\LOG
\initials{LDF 2023.04.20.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_NO_TEST@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list "
                << "WITH_NO_TEST'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> &= ~64U;

};

@q *** (3) call_metapost_option_list: WITH_TOLERANCE @>

@ \�call metapost option list> $\longrightarrow$ \.{WITH\_TOLERANCE}.
\initials{LDF 2022.05.29.}

\LOG
\initials{LDF 2022.05.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=call_metapost_option_list: call_metapost_option_list WITH_TOLERANCE numeric_expression @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `call_metapost_option_list: call_metapost_option_list "
                << "WITH_TOLERANCE'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> |= 128U;

   if (scanner_node->tolerance != 0)
      scanner_node->tolerance = new real;

   *(scanner_node->tolerance) = @=$2@>;

};

@q ** (2) command: CALL_TEX string_expression COMMA numeric_vector_variable save_optional@>

@ \�command> $\longrightarrow$ \.{CALL\_TEX} \�string expression> \.{COMMA} 
\�numeric vector variable> \�save optional>.
\initials{LDF 2022.05.06.}

\LOG
\initials{LDF 2022.05.06.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: CALL_TEX string_expression COMMA numeric_vector_variable save_optional@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: CALL_TEX string_expression "
                << "COMMA numeric_vector_variable save_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   string *str = static_cast<string*>(@=$2@>);
  
   entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

   if (!str)
   {
      cerr_strm << "ERROR!  In parser, rule `command: CALL_TEX string_expression"
                << endl 
                << "COMMA numeric_vector_variable save_optional':"
                << endl 
                << "`string_expression' is NULL.  Not calling TeX.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }

@q *** (3) @>

   else if (!entry)
   {
      cerr_strm << "ERROR!  In parser, rule `command: CALL_TEX string_expression"
                << endl 
                << "COMMA numeric_vector_variable save_optional':"
                << endl 
                << "`numeric_vector_variable' is NULL.  Not calling TeX.  Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }
@q *** (3) @> 

  else
  {
@q **** (4) @>

#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`entry' and `str' are non-NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>
@
@<Define rules@>= 

      status = call_tex(*str, entry, scanner_node, @=$5@>);

      if (status != 0)
      {
        cerr_strm << "ERROR!  In parser, `command: CALL_TEX string_expression COMMA"
                   << endl 
                   << "numeric_vector_variable save_optional':"
                   << endl 
                   << "`call_tex' failed, returning " << status << "."
                   << endl
                   << "Failed to execute TeX in child process."
                   << endl
                   << "Continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm, true);
        cerr_strm.str("");
 

      }  /* |if (status != 0)| */

@q **** (4) @>

      else 
      { 
@q ***** (5) @>

#if DEBUG_COMPILE
         if (DEBUG)
         {
            cerr_strm << "In parser, `command: CALL_TEX string_expression COMMA"
                      << endl 
                      << "numeric_vector_variable save_optional':"
                      << endl 
                      << "`call_tex' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(entry->object);

           int i = 0;

           cerr << "`pv->v.size()' == " << pv->v.size() << endl;

           if (pv->v.size() > 0)
              cerr << "`pv->v':" << endl;

           for (vector<real*>::iterator iter = pv->v.begin();
                iter != pv->v.end();
                ++iter)
           {
               cerr << i++ << ":  " << **iter << endl;
           }

         }    
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>

      }  /* |else|  */

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

   delete str;
   str = 0;

   @=$$@> = 0;

};

@q ** (2) command: NORMALIZE path_variable call_metapost_option_list@>

@ \�command> $\longrightarrow$ \.{NORMALIZE} \�path variable> \�call metapost option list>. 
\initials{LDF 2022.05.18.}

\LOG
\initials{LDF 2022.05.18.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: NORMALIZE path_variable call_metapost_option_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: NORMALIZE path_variable call_metapost_option_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   bool save               = @=$3@> & 1U;
   bool clear              = @=$3@> & 2U;
   bool suppress_mp_stdout = @=$3@> & 4U;
   bool do_transform       = @=$3@> & 8U;    

   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

@q *** (3) @>

   if (@=$3@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$3@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 


@q *** (3) @>

   Path *p = 0;

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
      p = static_cast<Path*>(entry->object);

   Path q;
   Transform t;

@q ***** (5) @>
@
@<Define rules@>=

  if (p)
  {
@q ****** (6) @>

     status = p->normalize_path(&q, &t, 0, origin_pt, x_axis_pt, save, suppress_mp_stdout, scanner_node);

     if (status != 0)
     {
        cerr_strm << thread_name 
                  << "ERROR!  In parser, rule `command: NORMALIZE path_variable "
                  << "call_metapost_option_list':"
                  << endl 
                  << "`Path::normalize_path' failed, returning " << status << "."
                  << endl 
                  << "Couldn't normalize path."
                  << endl; 

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str(""); 

     }

@q ****** (6) @>

     else
     { 
#if DEBUG_COMPILE
        if (DEBUG)
     	{ 
     	   cerr_strm << thread_name 
     	             << "In parser, rule `command: NORMALIZE path_variable call_metapost_option_list':"
     	             << endl 
     	             << "`Path::normalize_path' succeeded, returning 0.";

     	   q.show("q:");

     	   log_message(cerr_strm);
     	   cerr_message(cerr_strm);
     	   cerr_strm.str(""); 
     	}  
#endif /* |DEBUG_COMPILE|  */@; 

        *p = q;

     }


  }  /* |if (p)| */

@q ***** (5) @>

  else
  {
      cerr_strm << thread_name 
                << "WARNING!  In parser, rule `command: NORMALIZE path_variable "
                << "call_metapost_option_list':"
                << endl 
                << "`path_variable' on right-hand side is NULL.  Can't normalize path."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str(""); 

  }

@q ***** (5) @>

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> save_optional@>

@q ** (2) @>
@ 
@<Define rules@>= 
@=save_optional: /* Empty  */ @>@/
{
   @=$$@> = 0;
};

@q ** (2) @>
@ 
@<Define rules@>= 
@=save_optional: SAVE @>@/
{
   @=$$@> = 1;
};

@q ** (2) command: SET_ELEMENT transform_variable numeric_list @>

@ \�command> $\longrightarrow$ \.{SET\_ELEMENT} \�transform variable> \�numeric list>.
\initials{LDF 2022.06.06.}

\LOG
\initials{LDF 2022.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=command: SET_ELEMENT transform_variable numeric_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SET_ELEMENT transform_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 
   Transform *t = 0;
   Pointer_Vector<real> *rv = static_cast<Pointer_Vector<real>*>(@=$3@>); 

   int row;
   int col;
   real r;

   if (entry && entry->object)
   {
      t = static_cast<Transform*>(entry->object);
#if 0
      t->show("*t:");
#endif 
   }
   else
   {
#if 0 
       cerr << "`transform_variable' is NULL." << endl;
#endif 

       t = create_new<Transform>(0);

#if 0 
       t->show("After allocation:  *t:");
#endif 

       entry->object =  static_cast<void*>(t); 
   }
   
@q *** (3) @>

   if (rv != 0)
   {
#if 0
       cerr << "`rv' is non-NULL." << endl;
#endif 

       if (rv->v.size() < 3 || rv->v.size() % 3 != 0)
       {
          cerr << "ERROR!  In parser, rule `command: SET_ELEMENT transform_variable numeric_list':"
               << endl 
               << "The size of `numeric_list' is < 3 or not divisible by 3."  << endl 
               << "Invalid `numeric_list'.  Can't set element.  Continuing." << endl;
       } 
       else
       {
           vector<real*>::iterator iter = rv->v.begin();

#if LDF_REAL_DOUBLE
           row = round(abs(**iter++));
           col = round(abs(**iter++));
#else
           row = roundf(fabs(**iter++));
           col = roundf(fabs(**iter++));
#endif 
           r = **iter;

#if 0
           cerr << "row == " << row << endl
                << "col == " << col << endl
                << "r   == " << r << endl;
#endif 

           t->set_element(row, col, r);

       }

   }  /* |if (rv != 0)| */

   else
   {
       cerr << "ERROR!  In parser, rule `command: SET_ELEMENT transform_variable numeric_list':"
            << endl 
            << "`numeric_list' is NULL.  Can't set element.  Continuing." << endl;
   }

@q *** (3) @>

  delete rv;
  rv = 0;

  @=$$@> = static_cast<void*>(0);
};

@q ** (2) command: SET_ELEMENTS transform_variable numeric_list @>

@ \�command> $\longrightarrow$ \.{SET\_ELEMENTS} \�transform variable> \�numeric list>.
\initials{LDF 2022.06.06.}

\LOG
\initials{LDF 2022.06.06.}
Added this rule.
\ENDLOG

@<Define rules@>= 
@=command: SET_ELEMENTS transform_variable numeric_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SET_ELEMENTS transform_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>); 
   Transform *t = 0;
   Pointer_Vector<real> *rv = static_cast<Pointer_Vector<real>*>(@=$3@>); 

   int ctr = 0;
   int row = 0;
   int col = 0;
   real r  = 0.0;

   if (entry && entry->object)
   {
      t = static_cast<Transform*>(entry->object);
#if 0 
      t->show("*t:");
#endif 
   }
   else
   {
#if 0 
       cerr << "`transform_variable' is NULL." << endl;
#endif 
       t = create_new<Transform>(0);
#if 0 
       t->show("After allocation:  *t:");
#endif 
       entry->object = t;
   }
   
@q *** (3) @>

   if (rv != 0)
   {
#if 0 
       cerr << "`rv' is non-NULL." << endl
            << "rv->v.size() == " << rv->v.size() 
            << endl;
#endif 

@q **** (4) @>

       if (rv->v.size() < 3 || rv->v.size() % 3 != 0)
       {
           cerr << "ERROR!  In parser, rule `numeric_vector_primary: SET_ELEMENTS transform_expression numeric_list':"
                << endl 
                << "The size of `numeric_list' is < 2 or not divisible by 2:  "  
                << endl << rv->v.size() << endl 
                << "Invalid `numeric_list'.  Can't set elements.  Continuing." << endl;

           goto END_SET_ELEMENTS_RULE;

       }
       
@q **** (4) @>

       else 
       {
          ctr = 0;
#if 0 
          cerr << "Entering loop." << endl;
#endif 

          for (vector<real*>::iterator iter = rv->v.begin();
               iter != rv->v.end();
               ++iter)
          {
@q ***** (5) @>

#if LDF_REAL_DOUBLE
              row = round(abs(**iter++));
              col = round(abs(**iter++));
              r    = **iter;
#else
              row = roundf(fabs(**iter++));
              col = roundf(fabs(**iter++));
              r    = **iter;
#endif 

#if 0 
              cerr << "row == " << row << endl
                   << "col == " << col << endl
                   << "r   == " << r   << endl;
#endif 

              status = t->set_element(row, col, r);

@q ***** (5) @>

          }  /* |for|  */
#if 0      
          cerr << "After loop." << endl;
#endif 

@q ***** (5) @>

       }  /* |else|  */
  
@q **** (4) @>

   } /* |if (rv != 0)| */

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `numeric_vector_primary: SET_ELEMENTS transform_expression numeric_list':"
            << endl 
            << "`numeric_list' is NULL.  Can't set elements.  Continuing." << endl;

       goto END_SET_ELEMENTS_RULE;

   }

@q *** (3) @>

END_SET_ELEMENTS_RULE:

  if (rv)
  {
     rv->clear(true);
  }

  @=$$@> = static_cast<void*>(0);

};

@q ** (2) command: READ FROM BINARY FFILE binary_file_option_list@>

@ |FFILE| must be used because |FILE| is the name of a datatype used as the return type 
for I/O functions defined by the C Standard Library.
\initials{LDF 2022.06.10.}

@<Define rules@>=
@=command: READ FROM BINARY FFILE binary_file_option_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command: READ FROM BINARY FFILE binary_file_option_list'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   status = Binary_File_Format::read_from_binary_file(scanner_node, @=$5@>);

   if (status != 0)
   {
       cerr_strm << "ERROR!  In parser, rule `command: READ FROM BINARY FFILE "
                 << "binary_file_option_list':"
                 << endl 
                 << "`Binary_File_Format::read_from_binary_file' failed, returning "
                 << status << "." << endl 
                 << "Failed to read from binary file.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm, true);
       cerr_strm.str("");

   }  /* |if (status != 0)| */

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << "In parser, rule `command: READ FROM BINARY FFILE "
                 << "binary_file_option_list':"
                 << endl 
                 << "`Binary_File_Format::read_from_binary_file' succeeded, returning 0."
                 << endl 
                 << "Read from binary file successfully.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   @=$$@> = static_cast<void*>(0);

};


@q ** (2) command: WRITE TO BINARY FFILE any_variable binary_file_option_list@>

@ |FFILE| must be used because |FILE| is the name of a datatype used as the return type 
for I/O functions defined by the C Standard Library.
\initials{LDF 2022.06.10.}

@<Define rules@>=
@=command: WRITE TO BINARY FFILE any_variable binary_file_option_list@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command: WRITE TO BINARY FFILE any_variable binary_file_option_list'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");


       cerr << "$6 == " << @=$6@> << endl;


   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   status = Binary_File_Format::write_to_binary_file(scanner_node, 
                                                     static_cast<Id_Map_Entry_Node>(@=$5@>), 
                                                     @=$6@>);

   if (status != 0)
   {
       cerr_strm << "ERROR!  In parser, rule `command: WRITE TO BINARY FFILE any_variable binary_file_option_list':"
                 << endl 
                 << "`Binary_File_Format::write_to_binary_file' failed, returning " 
                 << status << "."
                 << endl 
                 << "Failed to write to binary file.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm, error_stop_value);
       cerr_strm.str("");
   }
#if DEBUG_COMPILE
   else if (DEBUG)
   { 
       cerr_strm << "In parser, rule `command: WRITE TO BINARY FFILE any_variable binary_file_option_list':"
                 << endl 
                 << "`Binary_File_Format::write_to_binary_file' succeeded, returning 0."
                 << endl 
                 << "Wrote to binary file successfully.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = static_cast<void*>(0);

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <uint_value> binary_file_option_list@>

@q ** (2) @>
@
@<Define rules@>= 
@=binary_file_option_list: /* Empty  */@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `binary_file_option_list: /* Empty  */'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 0U;
};

@q ** (2) @>
@
@<Define rules@>= 
@=binary_file_option_list: binary_file_option_list OVERWRITE@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `binary_file_option_list: binary_file_option_list OVERWRITE'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = @=$1@> | 1U;
};

@q ** (2) @>
@
@<Define rules@>= 
@=binary_file_option_list: binary_file_option_list NO_OVERWRITE@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `binary_file_option_list: binary_file_option_list NO_OVERWRITE'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = @=$1@> & ~1U;
};

@q ** (2) @>
@
@<Define rules@>= 
@=binary_file_option_list: binary_file_option_list WITH_FILE_NAME string_expression@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `binary_file_option_list: binary_file_option_list "
                 << "WITH_FILE_NAME string_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->binary_filename_temp_ptr = @=$3@>;

   @=$$@> = @=$1@> | 2U;

};

@q ** (2) @>
@
@<Define rules@>= 
@=binary_file_option_list: binary_file_option_list COMMA string_optional@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `binary_file_option_list: binary_file_option_list "
                 << "COMMA string_optional'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->binary_filename_temp_ptr = @=$3@>;

   @=$$@> = @=$1@> | 2U;

};

@q ** (2) command: CLOSE OUT BINARY FFILE@>

@ |FFILE| must be used because |FILE| is the name of a datatype used as the return type 
for I/O functions defined by the C Standard Library.
\initials{LDF 2022.06.10.}

@<Define rules@>=
@=command: CLOSE OUT BINARY FFILE@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command: CLOSE OUT BINARY FFILE'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   if (scanner_node->binary_output_file.is_open())
   {
      unsigned int ui = Binary_File_Format::END_B;
      scanner_node->binary_output_file.write(static_cast<char*>(static_cast<void*>(&ui)), 4);
      scanner_node->binary_output_file.close();
   }

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) command: CLOSE IN BINARY FFILE@>

@ |FFILE| must be used because |FILE| is the name of a datatype used as the return type 
for I/O functions defined by the C Standard Library.
\initials{LDF 2022.06.13.}

@<Define rules@>=
@=command: CLOSE IN BINARY FFILE@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command: CLOSE IN BINARY FFILE'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   if (scanner_node->binary_input_file.is_open())
      scanner_node->binary_input_file.close();

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) command: CLOSE BINARY FFILES@>
@ 
@<Define rules@>=
@=command: CLOSE BINARY FFILES@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `command: CLOSE BINARY FFILES'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */
#endif /* |DEBUG_COMPILE|  */@;

   if (scanner_node->binary_input_file.is_open())
      scanner_node->binary_input_file.close();



   if (scanner_node->binary_output_file.is_open())
   {
      unsigned int ui = Binary_File_Format::END_B;
      scanner_node->binary_output_file.write(static_cast<char*>(static_cast<void*>(&ui)), 4);
      scanner_node->binary_output_file.close();
   }

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) command: RESET_ARC numeric_list_optional @>

@ \�command> $\longrightarrow$ \.{RESET\_ARC} $\ldots$ \�numeric list optional>.
\initials{LDF 2022.04.29.}

!! PLEASE NOTE:  This is a dummy rule.  It's needed so that Bison won't issue 
 warnings about ``useless'' nonterminals and rules.  I want to 
keep \�numeric list optional> and its associated rules because I think I will
use them in the future.
\initials{LDF 2022.04.29.}
\par
%%
This applies to other non-terminals that I may add here, too.
\initials{LDF 2022.05.16.}

!! TODO:  \initials{LDF 2022.05.16.} Maybe get rid of 
|minus_optional|.

\LOG
\initials{LDF 2022.04.29.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: RESET_ARC numeric_list_optional minus_optional with_x_axis_point_optional@>
{
  @=$$@> = static_cast<void*>(0);
};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.04.19.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: REVERSE_TANGENT path_variable@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: REVERSE_TANGENT path_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   status = Path::reverse_tangent_or_perpendicular(entry, scanner_node);

   @=$$@> = static_cast<void*>(0);

};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.04.19.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: REVERSE_PERPENDICULAR path_variable@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: REVERSE_PERPENDICULAR path_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   status = Path::reverse_tangent_or_perpendicular(entry, scanner_node);

  @=$$@> = static_cast<void*>(0); 

  @=$$@> = static_cast<void*>(0);
};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.06.09.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: EXTEPS string_expression exteps_option_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EXTEPS string_expression exteps_option_list'."
                << "`string_expression' == $2 == " << *static_cast<string*>(@=$2@>) 
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (scanner_node->exteps_flag)
   {
      cerr_strm << "WARNING!  In parser, rule `command: EXTEPS string_expression exteps_option_list':" 
                << endl
                << "`scanner_node->exteps_flag' == `true'.  "
                << "Not calling `Scan_Parse::exteps_func'."
                << endl
                << "Continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      delete static_cast<string*>(@=$2@>);

   }

@q *** (3) @>

   else
   {
@q **** (4) @>

      scanner_node->exteps_filename = static_cast<string*>(@=$2@>);

      status = exteps_func(scanner_node, 0);

      if (status != 0)
      {
          cerr << "ERROR!  In parser, rule `command: EXTEPS string_expression exteps_option_list':"
               << endl 
               << "`exteps_func' failed with return value " << status << "."
               << endl 
               << "Will try to continue." << endl;
      }   

@q **** (4) @>

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
          cerr << "In parser, rule `command: EXTEPS string_expression exteps_option_list':"
               << endl 
               << "`exteps_func' succeeded with return value 0."
               << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

   @=$$@> = 0;
   
};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.06.09.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: BEGINEPS string_expression exteps_option_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: BEGINEPS string_expression exteps_option_list'."
                << "`string_expression' == $2 == " << *static_cast<string*>(@=$2@>) 
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (scanner_node->exteps_flag)
   {
      cerr_strm << "WARNING!  In parser, rule `command: BEGINEPS string_expression exteps_option_list':" 
                << endl
                << "`scanner_node->exteps_flag' == `true'.  "
                << "Not calling `Scan_Parse::exteps_func'."
                << endl
                << "Continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      delete static_cast<string*>(@=$2@>);
   }

@q *** (3) @>

   else
   {
@q **** (4) @>

      scanner_node->exteps_filename = static_cast<string*>(@=$2@>);

      status = exteps_func(scanner_node, 1);

      if (status != 0)
      {
          cerr << "ERROR!  In parser, rule `command: BEGINEPS string_expression exteps_option_list':"
               << endl 
               << "`exteps_func' failed with return value " << status << "."
               << endl 
               << "Will try to continue." << endl;
      }   

@q **** (4) @>

#if DEBUG_COMPILE
      else if (DEBUG)
      { 
          cerr << "In parser, rule `command: BEGINEPS string_expression exteps_option_list':"
               << endl 
               << "`exteps_func' succeeded with return value 0."
               << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.06.09.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: ENDEPS@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: ENDEPS'."
                << endl
                << "`scanner_node->exteps_flag' == " << scanner_node->exteps_flag;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (scanner_node->exteps_flag)
   {
      string s = "endeps;";
   
      status = verbatim_func(scanner_node, &s, VERBATIM_METAPOST_COMMAND);

#if 0 
      cerr << "verbatim_func returned " << status << endl;
#endif 
      scanner_node->exteps_flag = false;

   }

@q *** (3) @>

   else
   {
      cerr_strm << "WARNING!  In parser, rule `command: ENDEPS':" << endl
                << "`scanner_node->exteps_flag' == `false'.  "
                << "Not writing \"endeps\" to MetaPost output file." << endl
                << "Continuing."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
 
   }
   
   @=$$@> = 0;

};

@q ** (2) @>
@
\LOG
\initials{LDF 2023.06.09.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> exteps_option_list@>
@=%type <int_value> exteps_option@>

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option_list: /* Empty  */@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option_list: /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->exteps_clipping = 0;
   scanner_node->exteps_clippath = 0;
   scanner_node->exteps_base     = 0;
   scanner_node->exteps_width    = 0;
   scanner_node->exteps_height   = 0;
   scanner_node->exteps_scale    = 0;
   scanner_node->exteps_grid     = false;
   scanner_node->exteps_gridstep = 0.0;
   scanner_node->exteps_gridll   = 0;
   scanner_node->exteps_gridur   = 0;

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option_list: exteps_option_list exteps_option@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option_list: exteps_option_list exteps_option'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSCLIPPING boolean_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSCLIPPING boolean_expression'."
                << endl 
                << "`boolean_expression' == " << *static_cast<bool*>(@=$2@>);

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   bool *b = static_cast<bool*>(@=$2@>);

   if (b)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
          cerr << "$2 == `bool *b' is non-NULL:" << endl 
               << "`*b' == " << *b << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@;       

       scanner_node->exteps_clipping = b;
   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSCLIPPING boolean_expression':"
            << endl 
            << "`boolean_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting clipping to `true'.  Will try to continue."
            << endl;
   }

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSCLIPPATH path_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSCLIPPATH path_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Path *p = static_cast<Path*>(@=$2@>);

   if (p)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
#if 0 
          p->show("*p:");
#endif 
       }   
#endif /* |DEBUG_COMPILE|  */@;       
   
       scanner_node->exteps_clippath = p;
   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSCLIPPATH path_expression':"
            << endl 
            << "`path_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting clippath to `true'.  Will try to continue."
            << endl;
   }

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSANGLE numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSANGLE numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
 
  scanner_node->exteps_angle = @=$2@>;

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSBASE point_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSBASE point_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Point *p = static_cast<Point*>(@=$2@>);

   if (p)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 

       }   
#endif /* |DEBUG_COMPILE|  */@;       

       scanner_node->exteps_base = static_cast<Point*>(p);
   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSBASE point_expression':"
            << endl 
            << "`point_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting base to `true'.  Will try to continue."
            << endl;

       scanner_node->exteps_base = 0;
   }

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSWIDTH numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSWIDTH numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   scanner_node->exteps_width = static_cast<real>(@=$2@>);

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSHEIGHT numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSHEIGHT numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   scanner_node->exteps_height = static_cast<real>(@=$2@>);

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSSCALE numeric_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSSCALE numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   scanner_node->exteps_scale = static_cast<Pointer_Vector<real>*>(@=$2@>);

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSGRID@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSGRID'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   scanner_node->exteps_grid = true;

};


@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSGRID boolean_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSGRID boolean_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   bool *b = static_cast<bool*>(@=$2@>);

   if (b)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 
          cerr << "$2 == `bool *b' is non-NULL:" << endl 
               << "`*b' == " << *b << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@;       

       scanner_node->exteps_grid = *b;
 
       delete b;
       b = 0;

   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSGRID boolean_expression':"
            << endl 
            << "`boolean_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting `scanner_node->exteps_grid'.  Will try to continue."
            << endl;
   }

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSGRIDSTEP numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSGRIDSTEP numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   scanner_node->exteps_gridstep = @=$2@>;

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSGRIDLL point_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSGRIDLL point_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Point *p = static_cast<Point*>(@=$2@>);

   if (p)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 

       }   
#endif /* |DEBUG_COMPILE|  */@;       

       scanner_node->exteps_gridll = static_cast<Point*>(p);
   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSGRIDLL point_expression':"
            << endl 
            << "`point_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting gridll to `true'.  Will try to continue."
            << endl;

       scanner_node->exteps_gridll = 0;
   }

@q *** (3) @>

   @=$$@> = 0;

};


@q ** (2) @>
@
@<Define rules@>= 
@=exteps_option: EPSGRIDUR point_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `exteps_option: EPSGRIDUR point_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Point *p = static_cast<Point*>(@=$2@>);

   if (p)
   {
#if DEBUG_COMPILE
       if (DEBUG)
       { 

       }   
#endif /* |DEBUG_COMPILE|  */@;       

       scanner_node->exteps_gridur = static_cast<Point*>(p);
   }

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `exteps_option: EPSGRIDUR point_expression':"
            << endl 
            << "`point_expression' is NULL.  This shouldn't be possible."
            << endl 
            << "Not setting gridur to `true'.  Will try to continue."
            << endl;

       scanner_node->exteps_gridur = 0;
   }

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: EPSDRAW path_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EPSDRAW path_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Path *p = static_cast<Path*>(@=$2@>);

   if (!scanner_node->exteps_flag)
   {
       delete p;
       p = 0;
   }


   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: EPSFILL path_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EPSFILL path_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Path *p = static_cast<Path*>(@=$2@>);

   if (p == 0)
   {

   }

   else if (!scanner_node->exteps_flag)
   {
       delete p;
       p = 0;
   }
   else
   {

   }

   @=$$@> = 0;

};


@q ** (2) @>
@
@<Define rules@>= 
@=command: EPSFILLDRAW path_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EPSFILLDRAW path_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Path *p = static_cast<Path*>(@=$2@>);

   if (!scanner_node->exteps_flag)
   {
       delete p;
       p = 0;
   }

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: EPSDRAWDOT point_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EPSDRAWDOT point_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   Point *p = static_cast<Point*>(@=$2@>); 
 
   if (!scanner_node->exteps_flag)
   {
       delete p;
       p = 0;
   }


   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: EPSLABEL string_expression point_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: EPSLABEL string_expression point_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   string *s = static_cast<string*>(@=$2@>); 

   Point *p  = static_cast<Point*>(@=$3@>); 


   if (!(scanner_node->exteps_flag && s && p))
   {

       cerr << "WARNING!  In parser, rule `command: EPSLABEL string_expression point_expression':"
            << endl 
            << "`scanner_node->exteps_flag', `string_expression' and/or `point_expression' are NULL:"
            << endl
            << "`scanner_node->exteps_flag':  " << scanner_node->exteps_flag << endl 
            << "`string_expression' == " << ((s) ? *s : "NULL") << endl
            << "`point_expression' is " << ((p) ? "non-NULL." : "NULL.") << endl
            << "Not drawing label on epstex picture.  Continuing." << endl;

       delete s;
       s = 0;

       delete p;
       p = 0;
   }

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: SET_RANDOM_SEED numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SET_RANDOM_SEED numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "$2 == " << @=$2@> << endl;

      cerr << "static_cast<unsigned int>(roundf(fabsf($2))) == " 
           << static_cast<unsigned int>(roundf(fabsf(@=$2@>))) << endl;

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   status = set_random_seed(static_cast<unsigned int>(roundf(fabsf(@=$2@>))),
                            scanner_node);

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: SET_RANDOM_SEED WITH_GET_TIME@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SET_RANDOM_SEED WITH_GET_TIME'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   time_t tt;
   tt = time(0);

   status = set_random_seed(static_cast<unsigned int>(tt),
                            scanner_node);

   @=$$@> = 0;

};


@q ** (2) @>
@
@<Define rules@>= 
@=command: CLIP sinewave_variable TO numeric_expression clipping_type_optional with_threshold_optional@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: CLIP sinewave_variable TO numeric_expression "
                << "clipping_type_optional with_threshold_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

#if 0 
      cerr << "$4 == " << @=$4@> << endl
           << "$5 == " << @=$5@> << endl
           << "$6 == " << @=$6@> << endl;
#endif 

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {

   }
   else
   {
      cerr_strm << "ERROR!  In parser, rule `command: CLIP sinewave_variable "
                << "clipping_type_optional':"
                << endl;

      if (!entry)   
         cerr_strm << "`Id_Map_Entry_Node entry' is NULL." << endl;
      else 
         cerr_strm << "`void* entry->object' is NULL." << endl;

      cerr_strm << "`sinewave_variable' is NULL.  Can't clip sinewave." << endl
                << "Will try to continue." << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }

@q *** (3) @>

   status = static_cast<Sinewave*>(entry->object)->clip(@=$4@>, @=$5@>, @=$6@>, scanner_node);

   @=$$@> = 0;

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> clipping_type_optional@>
@=%type <real_value> with_threshold_optional@>

@q ** (2) @>
@
@<Define rules@>= 
@=clipping_type_optional: /* Empty  */@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `clipping_type_optional: /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=clipping_type_optional: WITH_HARD@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `clipping_type_optional: WITH_HARD'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 1;

};

@q ** (2) @>
@
@<Define rules@>= 
@=clipping_type_optional: WITH_SOFT@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `clipping_type_optional: WITH_SOFT'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 2;

};

@q ** (2) @>
@
@<Define rules@>= 
@=with_threshold_optional: /* Empty  */@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_threshold_optional: /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=with_threshold_optional: WITH_THRESHOLD numeric_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_threshold_optional: WITH_THRESHOLD numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$2@>;

};

@q ** (2) @>
@
@<Define rules@>= 
@=clipping_type_optional: WITH_PERCENTAGE@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `clipping_type_optional: WITH_PERCENTAGE'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 3;

};


@q ** (2) @>
@
@<Define rules@>= 
@=command:  PERTURB sinewave_variable numeric_list@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: PERTURB sinewave_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Sinewave *s = static_cast<Sinewave*>(entry->object);

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);


#if 0
   s->show("s:");
#endif 

#if 0 
   for (vector<real*>::iterator iter = pv->v.begin();
        iter != pv->v.end();
        ++iter)
   {
       cerr << "(**iter) == " << (**iter) << endl;
   }
#endif 

@q *** (3) @>

   status = s->perturb(pv, scanner_node);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "`Sinewave::perturb' returned `status' == " << status << endl;

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (status != 0)
   {
      cerr_strm << "ERROR!  In parser, rule `command: PERTURB sinewave_variable numeric_list':"
                << endl 
                << "`Sinewave::perturb' failed, returning " << status << "."
                << endl
                << "Failed to perturb `sinewave_variable'.  Will try to continue.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << "In parser, rule `command: PERTURB sinewave_variable numeric_list':"
                << endl 
                << "`Sinewave::perturb' succeeded, returning 0.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   delete pv;
   pv = 0;

@q *** (3) @>

   @=$$@> = 0;
};

@q ** (2) command:  PERTURB path_variable numeric_vector_variable.  @>

@ \�command> $\longrightarrow$ \.{PERTURB} \�path variable> \�numeric vector variable>.
\initials{LDF 2023.12.04.}


\LOG
\initials{LDF 2023.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command:  PERTURB path_variable BY numeric_vector_variable perturb_path_options@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: PERTURB path_variable BY "
                << "numeric_vector_variable perturb_path_options'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Id_Map_Entry_Node entry_1 = static_cast<Id_Map_Entry_Node>(@=$4@>);

   Path *q = 0;
   Pointer_Vector<real> *nv = 0;

   if (entry)
   {
      q = static_cast<Path*>(entry->object);
   }

   if (entry_1)
   {
      nv = static_cast<Pointer_Vector<real>*>(entry_1->object);
   }

@q *** (3) @>

   if (q && nv)
   {
@q **** (4) @>

#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "In parser, rule `command:  PERTURB path_variable BY "
                << "numeric_vector_variable perturb_path_options':" 
                << endl 
                << "`path_variable' and `numeric_vector_variable' are both non- NULL." << endl 
                << "Calling `Path::perturb'." << endl;
       }   
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

       status = q->perturb(nv, @=$5@>, scanner_node);

       if (status != 0)
       {
          cerr << "ERROR!  In parser, rule `command:  PERTURB path_variable BY "
               << "numeric_vector_variable perturb_path_options':" 
               << endl 
               << "`Path::perturb' failed, returning " << status << "." << endl 
               << "Failed to perturb `path_variable'.  Will try to continue." << endl;
       }

@q **** (4) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
          cerr << "In parser, rule `command:  PERTURB path_variable BY "
               << "numeric_vector_variable perturb_path_options':" 
               << endl 
               << "`Path::perturb' succeeded, returning 0." << endl; 
       }   
#endif /* |DEBUG_COMPILE|  */@; 
  
@q **** (4) @>

   }  /* |if (q && nv)| */

@q *** (3) @>

   else
   {
       cerr << "ERROR!  In parser, rule `command:  PERTURB path_variable BY "
            << "numeric_vector_variable perturb_path_options':" 
            << endl 
            << "`path_variable' and/or `numeric_vector_variable' is NULL." << endl 
            << "Can't perturb `path_variable'.  Will try to continue." << endl;
   }


@q *** (3) @>

   @=$$@> = 0;
};

@q ** (2) |perturb_path_options|.  @>

@ |perturb_path_options|.
\initials{LDF 2023.12.04.}

\LOG
\initials{LDF 2023.12.04.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <uint_value> perturb_path_options@>

@q *** (3) perturb_path_options: /* Empty  */@>

@ \�perturb path options> $\longrightarrow$ \.{Empty}.
\initials{LDF 2023.12.04.}

\LOG
\initials{LDF 2023.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=perturb_path_options: /* Empty  */@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `perturb_path_options:  /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = 0U;

};

@q *** (3) perturb_path_options: perturb_path_options X_Y_ONLY @>

@ \�perturb path options> $\longrightarrow$ \�perturb path options> \.{X\_Y\_ONLY}.
\initials{LDF 2023.12.04.}

\LOG
\initials{LDF 2023.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=perturb_path_options: perturb_path_options X_Y_ONLY @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `perturb_path_options:  perturb_path_options X_Y_ONLY'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> | 1U;      

};

@q *** (3) perturb_path_options: perturb_path_options X_Z_ONLY @>

@ \�perturb path options> $\longrightarrow$ \�perturb path options> \.{X\_Z\_ONLY}.
\initials{LDF 2023.12.04.}

\LOG
\initials{LDF 2023.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=perturb_path_options: perturb_path_options X_Z_ONLY @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `perturb_path_options:  perturb_path_options X_Z_ONLY'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> | 2U;      

};

@q *** (3) perturb_path_options: perturb_path_options Z_Y_ONLY @>

@ \�perturb path options> $\longrightarrow$ \�perturb path options> \.{Z\_Y\_ONLY}.
\initials{LDF 2023.12.04.}

\LOG
\initials{LDF 2023.12.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=perturb_path_options: perturb_path_options Z_Y_ONLY @>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `perturb_path_options:  perturb_path_options Z_Y_ONLY'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   @=$$@> = @=$1@> | 4U;

};

@q ** (2) command: RANDOMIZE_SIGNS numeric_vector_variable@>

@ \�command> $\longrightarrow$ \.{RANDOMIZE\_SIGNS} \�numeric vector variable>.
\initials{LDF 2023.08.30.}

\LOG
\initials{LDF 2023.08.30.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=command: RANDOMIZE_SIGNS numeric_vector_variable @>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: RANDOMIZE_SIGNS numeric_vector_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

@q *** (3) @>

   if (!(entry && entry->object))
   {
      cerr_strm << "ERROR!  In parser, rule `command: RANDOMIZE_SIGNS "
                << "numeric_vector_variable':"
                << endl 
                << "`numeric_vector_variable' is NULL.  Can't randomize signs." 
                << endl
                << "Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }
@q *** (3) @> 

  else
  {
@q **** (4) @>

#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`entry' and `entry->object' are non-NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>
@
@<Define rules@>= 

      status = randomize_signs(entry, scanner_node);

      if (status != 0)
      {
        cerr_strm << "ERROR!  In parser, `command: RANDOMIZE_SIGNS  "
                   << endl 
                   << "numeric_vector_variable ':"
                   << endl 
                   << "`randomize_signs' failed, returning " << status << "."
                   << endl
                   << "Failed to randomize signs."
                   << endl
                   << "Continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm, true);
        cerr_strm.str("");
 
      }  /* |if (status != 0)| */

@q **** (4) @>

      else 
      { 
@q ***** (5) @>

#if DEBUG_COMPILE
         if (DEBUG)
         {
            cerr_strm << "In parser, `command: RANDOMIZE_SIGNS numeric_vector_variable':"
                      << endl 
                      << "`randomize_signs' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
         }    
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>

      }  /* |else|  */

@q **** (4) @>

   }  /* |else|  */

@q *** (3) @>

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: SHUFFLE numeric_vector_variable with_range_optional@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SHUFFLE numeric_vector_variable "
                << "with_range_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      status = shuffle(entry->object, 0, static_cast<void*>(@=$3@>), scanner_node);
   }

   @=$$@> = 0;

};

@q ** (2) @>
@
@<Define rules@>= 
@=command: SHUFFLE color_vector_variable with_range_optional@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SHUFFLE color_vector_variable "
                << "with_range_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      status = shuffle(entry->object, 1, static_cast<void*>(@=$3@>), scanner_node);
#if 0 
      cerr << "status == " << status << endl;
#endif 

   }

   @=$$@> = 0;

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_range_optional@>

@q ** (2) @>
@
@<Define rules@>= 
@=with_range_optional: /* Empty  */@>
{
   @=$$@> = static_cast<void*>(0);
};


@q ** (2) @>
@
@<Define rules@>= 
@=with_range_optional: WITH_RANGE numeric_list@>
{
   @=$$@> = static_cast<void*>(@=$2@>);
};


@q ** (2) @>
@
@<Define rules@>= 
@=command: POP_BACK path_variable with_count_optional@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: POP_BACK path_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   if (entry && entry->object)
   {
      status = static_cast<Path*>(entry->object)->pop_back(@=$3@>, scanner_node);

#if 0 
      cerr << "status == " << status << endl;
#endif 

   }

   @=$$@> = 0;

};

@q * (1) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_count_optional@>

@q ** (2) with_count_optional: /* Empty  */   @>

@*1 \�with count optional> $\longrightarrow$ \.{Empty}.
\initials{LDF 2023.09.04.}

\LOG
\initials{LDF 2023.09.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_count_optional: /* Empty  */@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_count_optional: /* Empty  */'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = 1;

};

@*1 \�with count optional> $\longrightarrow$ \.{WITH\_COUNT} \�numeric expression>.
\initials{LDF 2023.09.04.}

\LOG
\initials{LDF 2023.09.04.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
  
@=with_count_optional: WITH_COUNT numeric_expression@>
{

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `with_count_optional: WITH_COUNT numeric_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

  @=$$@> = @=$2@>;

};

@q ** (2) command:  SYSTEM string_expression.  @> 

@ \�command> $\longrightarrow$  \.{SYSTEM} \�string expression>.
\initials{LDF 2023.10.03.}

\LOG
\initials{LDF 2023.10.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SYSTEM string_expression@>
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SYSTEM string_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

   }  
#endif /* |DEBUG_COMPILE|  */@; 

  string *s = static_cast<string*>(@=$2@>);

  errno = 0;
  status = system(s->c_str());

  if (errno != 0 || WIFEXITED(status) != 1 || WEXITSTATUS(status) == 127)
  {
      cerr << "ERROR!  In parser, rule `command: SYSTEM string_expression':"
           << endl 
           << "`system' failed, returning `status' == " << status << endl
           << "WIFEXITED(status)   == " << WIFEXITED(status)
           << endl 
           << "WEXITSTATUS(status) == " << WEXITSTATUS(status)
           << endl
           << "`errno'             == " << errno << endl
           << "`strerror(errno)'   == " << errno << endl    
           << "Failed to execute command in shell." << endl
           << "Will try to continue." << endl;
  }
  else
  {
      cerr << "Shell command succeeded with return value " << WEXITSTATUS(status) << "."
           << endl;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`system' succeeded, returning `status' == " << status << endl
              << "WIFEXITED(status)   == " << WIFEXITED(status)
              << endl 
              << "WEXITSTATUS(status) == " << WEXITSTATUS(status)
              << endl
              << "`errno'             == " << errno << endl
              << "`strerror(errno)'   == " << errno << endl    
              << "Executed command in shell successfully." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 
  }


  delete s;
  s = 0;

  @=$$@> = 0;

};


@q ** (2) command:  string_variable PLUS_ASSIGN string_expression.  @> 

@ \�command> $\longrightarrow$  \�string variable> \.{PLUS\_ASSIGN} \�string expression>.
\initials{LDF 2023.10.06.}

\LOG
\initials{LDF 2023.10.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: string_variable PLUS_ASSIGN string_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: string_variable PLUS_ASSIGN string_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");


   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   string *s = 0;

   string *t = static_cast<string*>(@=$3@>);

   if (entry && t)
   {
      if (!entry->object)
      {
         s = new string;
         entry->object = static_cast<void*>(s);
      }
      else
         s = static_cast<string*>(entry->object);

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "*s == " << *s << endl
               << "*t == " << *t << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

      *s += *t;

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "After appending:  *s == " << *s << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   }  /* |if (entry && t)|  */

@q *** (3) @>

  delete t;
  t = 0;

  @=$$@> = 0;

};

@q ** (2) command:  string_variable PLUS_ASSIGN_STRING string_expression.  @> 

@ \�command> $\longrightarrow$  \�string variable> \.{PLUS\_ASSIGN\_STRING} \�string expression>.
\initials{LDF 2023.10.06.}

\LOG
\initials{LDF 2023.10.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: string_variable PLUS_ASSIGN_STRING string_expression@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: string_variable PLUS_ASSIGN_STRING string_expression'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");


   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

   string *s = 0;

   string *t = static_cast<string*>(@=$3@>);

   if (entry && t)
   {
      if (!entry->object)
      {
         s = new string;
         entry->object = static_cast<void*>(s);
      }
      else
         s = static_cast<string*>(entry->object);

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "*s == " << *s << endl
               << "*t == " << *t << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

      *s += *t;

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "After appending:  *s == " << *s << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   }  /* |if (entry && t)|  */

@q *** (3) @>

  delete t;
  t = 0;

  @=$$@> = 0;

};

@q ** (2) command:  string POP_BACK string_variable.  @> 

@ \�command> $\longrightarrow$  \�string variable> \.{POP\_BACK} \�string expression>.
\initials{LDF 2023.10.06.}

\LOG
\initials{LDF 2023.10.06.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: POP_BACK string_variable@>
{
@q *** (3) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: POP_BACK string_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   string *s = 0;

   if (entry && entry->object)
   {
@q **** (4) @>

     s = static_cast<string*>(entry->object);

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "*s == " << *s << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

      if (s->size() > 0)
      {
         s->pop_back();

#if DEBUG_COMPILE
         if (DEBUG)
         { 
            cerr << "After popping:  *s == " << *s << endl;
         }     
#endif /* |DEBUG_COMPILE|  */@; 

      }  /* |if (s->size() > 0)| */

@q **** (4) @>

   }  /* |if (entry && entry->object)|  */

@q *** (3) @>

  @=$$@> = 0;

};

@q ** (2) command:  ROTATE numeric_primary vector_type_variable.  @> 
@
@<Define rules@>=
@=command:  ROTATE numeric_primary vector_type_variable@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: ROTATE numeric_primary vector_type_variable.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   status = rotate_vector_func(@=$3@>, @=$2@>, scanner_node);
 
   if (status != 0)
   {
       cerr << "ERROR!  In parser, rule `command:  ROTATE numeric_primary vector_type_variable':"
            << endl 
            << "`rotate_vector_func' failed, returning " << status << "." << endl 
            << "Failed to rotate vector-type object." << endl 
            << "Will try to continue." << endl;
   }

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "status == " << status << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



   @=$$@> = 0;

};

@q ** (2) command: SELF_ROTATE cuboid_variable numeric_list.  @> 
@
\LOG
\initials{LDF 2024.02.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SELF_ROTATE cuboid_variable numeric_list@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SELF_ROTATE cuboid_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Transform *t = create_new<Transform>(0);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Cuboid *c = static_cast<Cuboid*>(entry->object);

   real x = 0.0;
   real y = 0.0;
   real z = 0.0;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

   if (pv->v.size() > 0)
      x = *(pv->v[0]);

   if (pv->v.size() > 1)
      y = *(pv->v[1]);

   if (pv->v.size() > 2)
      z = *(pv->v[2]);

   c->rotate_cuboid(t, x, y, z, scanner_node);

   delete pv;
   pv = 0;

   @=$$@> = 0;

};

@q ** (2) command: SELF_ROTATE polyhedron_variable numeric_list.  @> 
@
\LOG
\initials{LDF 2024.02.20.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SELF_ROTATE polyhedron_variable numeric_list@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SELF_ROTATE polyhedron_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Transform *t = create_new<Transform>(0);

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Polyhedron *p = static_cast<Polyhedron*>(entry->object);

   real x = 0.0;
   real y = 0.0;
   real z = 0.0;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

   if (pv->v.size() > 0)
      x = *(pv->v[0]);

   if (pv->v.size() > 1)
      y = *(pv->v[1]);

   if (pv->v.size() > 2)
      z = *(pv->v[2]);

   p->rotate_polyhedron(t, x, y, z, scanner_node);

   delete pv;
   pv = 0;

#if 0 
// !! START HERE:  LDF 2024.04.04.  Check this!  Is there any reason why it
// shouldn't be deleted?  Is it used in |rotate_polyhedron| at all?
// Check this for the cuboid version as well.

   delete t; 
   t = 0;
#endif 

   @=$$@> = 0;

};

@q ** (2) command: SELF_ROTATE rectangle_variable numeric_list.  @> 
@
\LOG
\initials{LDF 2024.04.04.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SELF_ROTATE rectangle_variable numeric_list@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SELF_ROTATE rectangle_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Rectangle *r = static_cast<Rectangle*>(entry->object);

   real x     = 0.0;
   real y     = 0.0;
   real z     = 0.0;
   real axis0 = 0.0;
   real axis1 = 0.0;    
   real axis2 = 0.0;
   real axis3 = 0.0;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

   if (pv->v.size() > 0)
      x = *(pv->v[0]);

   if (pv->v.size() > 1)
      y = *(pv->v[1]);

   if (pv->v.size() > 2)
      z = *(pv->v[2]);

   if (pv->v.size() > 3)
      axis0 = *(pv->v[3]);

   if (pv->v.size() > 4)
      axis1 = *(pv->v[4]);

   if (pv->v.size() > 5)
      axis2 = *(pv->v[5]);

   if (pv->v.size() > 6)
      axis3 = *(pv->v[6]);


   // !! START HERE LDF 2024.04.04.  See also above for other things to check.

   r->self_rotate(x, y, z, axis0, axis1, axis2, axis3, true, 0, scanner_node);

   delete pv;
   pv = 0;

   @=$$@> = 0;

};

@q ** (2) command: SELF_SCALE rectangle_variable numeric_list.  @> 
@
\LOG
\initials{LDF 2024.05.23.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=command: SELF_SCALE rectangle_variable numeric_list@> 
{
   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `command: SELF_SCALE rectangle_variable numeric_list'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   Rectangle *r = static_cast<Rectangle*>(entry->object);

   real x     = 0.0;
   real y     = 0.0;
   real z     = 0.0;

   Pointer_Vector<real> *pv = static_cast<Pointer_Vector<real>*>(@=$3@>);

   if (pv->v.size() > 0)
      x = *(pv->v[0]);

   if (pv->v.size() > 1)
      y = *(pv->v[1]);

   if (pv->v.size() > 2)
      z = *(pv->v[2]);

   r->self_scale(x, y, z, true, 0, scanner_node);

   delete pv;
   pv = 0;

   @=$$@> = 0;

};


@q * (1) @>

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables: @>
@q mode:CWEB  @>
@q eval:(outline-minor-mode t)  @>
@q abbrev-file-name:"~/.abbrev_defs" @>
@q eval:(read-abbrev-file)  @>
@q fill-column:70 @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End: @>
