@q psinwavs.w  @> 
@q Created by Laurence Finston Mo 14. Aug 07:21:30 CEST 2023 @>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2023 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) sinewave expressions.  @>
@** sinewave expressions.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Created this file.
\ENDLOG 

@q * (1) sinewave_primary.  @>
@* \�sinewave primary>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sinewave_primary@>@/

@q ** (2) sinewave_primary --> sinewave_variable.@>
@*1 \�sinewave primary> $\longrightarrow$ \�sinewave variable>.  

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=sinewave_primary: sinewave_variable@>@/
{
  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `sinewave_primary: sinewave_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

    }
#endif /* |DEBUG_COMPILE|  */@;

  entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
  {

      @=$$@> = static_cast<void*>(0);

  } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */
  {
     Sinewave *s = new Sinewave;

     *s = *static_cast<Sinewave*>(entry->object);

     @=$$@> = static_cast<void*>(s);
  }

};

@q ** (2) sinewave_primary --> LEFT_PARENTHESIS sinewave_expression @> 
@q ** (2) RIGHT_PARENTHESIS                                         @>

@*1 \�sinewave primary> $\longrightarrow$ \.{\LP} 
\�sinewave expression> \.{\RP}.

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sinewave_primary: LEFT_PARENTHESIS sinewave_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q * (1) sinewave_secondary.  @>
@* \�sinewave secondary>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sinewave_secondary@>
  
@q ** (2) sinewave secondary --> sinewave_primary.@>
@*1 \�sinewave secondary> $\longrightarrow$ \�sinewave primary>.

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sinewave_secondary: sinewave_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: sinewave_secondary --> sinewave_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) sinewave secondary --> sinewave_secondary transformer.  @>
@*1 \�sinewave secondary> $\longrightarrow$ \�sinewave secondary> 
\�transformer>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX: Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=sinewave_secondary: sinewave_secondary transformer@>@/
{



#if 0 
/* !! START HERE LDF 2023.06.20. */

  Sinewave* p = static_cast<Sinewave*>(@=$1@>);
  Transform* t = static_cast<Transform*>(@=$2@>);

  *p *= *t;

  @=$$@> = static_cast<void*>(p); 

  delete t;
#endif 

  @=$$@> = 0;

};

@q * (1) sinewave tertiary.@>
@* \�sinewave tertiary>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sinewave_tertiary@>

@q ** (2) sinewave tertiary --> sinewave_secondary.  @>
@*1 \�sinewave tertiary> $\longrightarrow$ \�sinewave secondary>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sinewave_tertiary: sinewave_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q ***** (5) sinewave_tertiary --> sinewave_tertiary @>
@q ***** (5) PLUS sinewave_secondary.                @>

@ \�sinewave tertiary> $\longrightarrow$ \�sinewave tertiary> 
\.{PLUS} \�sinewave secondary> \�with rectify optional>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=sinewave_tertiary: sinewave_tertiary PLUS sinewave_secondary with_rectify_optional@>
{
@q ****** (6) @>

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `sinewave_tertiary: sinewave_tertiary PLUS sinewave_secondary "
                << "with_rectify_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "`with_rectify_optional' ($4) == " << @=$4@> << endl;

    }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>

    Sinewave *s = static_cast<Sinewave*>(@=$1@>);
    Sinewave *t = static_cast<Sinewave*>(@=$3@>);

@q ****** (6) Error handling.  @>

@ Error handling.  
\initials{LDF 2023.08.20.}

@<Define rules@>= 

    if (s == 0 || t == 0)
    {
        cerr_strm << thread_name 
                  << "ERROR!  In parser: `sinewave_tertiary: sinewave_tertiary PLUS sinewave_secondary "
                  << "with_rectify_optional':"
                  << endl;

        if (s == 0)
           cerr_strm << "`sinewave_tertiary' is NULL";

        if (t == 0)
        {
            if (s == 0)
               cerr_strm << " and ";
            
            cerr_strm << "`sinewave_secondary' is NULL";
         }
  
        cerr_strm << "." << "Can't add sinewaves." << endl
             << "Setting `sinewave_tertiary' (result of rule) to NULL."
             << endl 
             << "Will try to continue.";
   
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");

    }  /* |if (s == 0 || t == 0)|  */

@q ****** (6) @>
@
@<Define rules@>= 

    else
    {
@q ******* (7) @>

       *s += *t;

@q ******* (7) @>

       if (s == 0)
       {
        cerr_strm << thread_name 
                  << "ERROR!  In parser: `sinewave_tertiary: sinewave_tertiary PLUS sinewave_secondary "
                  << "with_rectify_optional':"
                  << endl 
                  << "`Sinewave::operator+=' failed, returning NULL."
                  << endl 
                  << "Failed to add sinewaves.  " 
                  << "Setting `sinewave_tertiary' (result of rule) to NULL."
                  << endl 
                  << "Will try to continue.";
   
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");

       }  /* |if (s == 0)|  */

@q ******* (7) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
           cerr_strm << thread_name 
                     << "In parser: `sinewave_tertiary: sinewave_tertiary PLUS sinewave_secondary "
                     << "with_rectify_optional':"
                     << endl 
                     << "`Sinewave::operator+=' succeeded.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           s->show("s:");

       }    
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

    }  /* |else| (|!(s == 0 || t == 0)|)  */

@q ****** (6) @>
@
@<Define rules@>= 

    @=$$@> = static_cast<void*>(s);

    delete t;
    t = 0;

};

@q ***** (5) sinewave_tertiary --> sinewave_tertiary         @>
@q ***** (5) MINUS sinewave_secondary with_rectify_optional. @>

@ \�sinewave tertiary> $\longrightarrow$ \�sinewave tertiary> 
\.{MINUS} \�sinewave secondary> \�with rectify optional>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG 

@<Define rules@>= 
@=sinewave_tertiary: sinewave_tertiary MINUS sinewave_secondary with_rectify_optional@>
{

@q ****** (6) @>

  @<Common declarations for rules@>@;
         
#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `sinewave_tertiary: sinewave_tertiary MINUS sinewave_secondary "
                << "with_rectify_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");

      cerr << "`with_rectify_optional' ($4) == " << @=$4@> << endl;

    }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>

    Sinewave *s = static_cast<Sinewave*>(@=$1@>);
    Sinewave *t = static_cast<Sinewave*>(@=$3@>);

@q ****** (6) Error handling.  @>

@ Error handling.  
\initials{LDF 2023.08.20.}

@<Define rules@>= 

    if (s == 0 || t == 0)
    {
        cerr_strm << thread_name 
                  << "ERROR!  In parser: `sinewave_tertiary: sinewave_tertiary MINUS sinewave_secondary "
                  << "with_rectify_optional':"
                  << endl;

        if (s == 0)
           cerr_strm << "`sinewave_tertiary' is NULL";

        if (t == 0)
        {
            if (s == 0)
               cerr_strm << " and ";
            
            cerr_strm << "`sinewave_secondary' is NULL";
         }
  
        cerr_strm << "." << "Can't subtract `sinewave_secondary' from `sinewave_tertiary' "
                  << "(on right side of rule)." 
                  << endl
                  << "Setting `sinewave_tertiary' (result of rule) to NULL."
                  << endl 
                  << "Will try to continue.";
   
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");

    }  /* |if (s == 0 || t == 0)|  */

@q ****** (6) @>
@
@<Define rules@>= 

    else
    {
@q ******* (7) @>

       *s -= *t;

@q ******* (7) @>

       if (s == 0)
       {
        cerr_strm << thread_name 
                  << "ERROR!  In parser: `sinewave_tertiary: sinewave_tertiary MINUS sinewave_secondary "
                  << "with_rectify_optional':"
                  << endl 
                  << "`Sinewave::operator+=' failed, returning NULL."
                  << endl 
                  << "Failed to add sinewaves.  " 
                  << "Setting `sinewave_tertiary' (result of rule) to NULL."
                  << endl 
                  << "Will try to continue.";
   
        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str("");

       }  /* |if (s == 0)|  */

@q ******* (7) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
           cerr_strm << thread_name 
                     << "In parser: `sinewave_tertiary: sinewave_tertiary MINUS sinewave_secondary "
                     << "with_rectify_optional':"
                     << endl 
                     << "`Sinewave::operator+=' succeeded.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           s->show("s:");

       }    
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

    }  /* |else| (|!(s == 0 || t == 0)|)  */

@q ****** (6) @>
@
@<Define rules@>= 

    @=$$@> = static_cast<void*>(s);

    delete t;
    t = 0;

};

@q * (1) sinewave expression.@>
@* \�sinewave expression>.
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> sinewave_expression@>

@q ** (2) sinewave expression --> sinewave_tertiary.  @>
@*1 \�sinewave expression> $\longrightarrow$ \�sinewave tertiary>.

\LOG
\initials{LDF 2023.08.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=sinewave_expression: sinewave_tertiary@>@/
{

  @=$$@> = @=$1@>;

};


@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 70))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:70                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

