@q ptmastro.w @> 
@q Created by Laurence Finston Sun 23 May 2021 03:21:07 PM CEST @>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) Time and astronomy rules.  @>
@** Time and astronomy expressions.
\initials{LDF 2021.05.23.}

\LOG
\initials{LDF 2021.05.23.}
Created this file.
\ENDLOG 

@q ** (2) @>

@q ** (2) numeric_primary --> GET_TIME get_time_option_list.@> 
@*1 \�command> $\longrightarrow$ \.{GET\_TIME} \�get time option list>.
\initials{LDF 2024.09.07.}

\LOG
\initials{LDF 2024.09.07.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=numeric_primary: GET_TIME get_time_option_list@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `numeric_primary: GET_TIME get_time_option_list'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   real r = 0;
   string *s = 0;

   status = get_time_ldf(scanner_node, &r);

   if (status != 0)
   {
       cerr << "Error!  In parser, rule `numeric_primary: GET_TIME get_time_option_list':"
            << endl
            << "`get_time_ldf' failed, returning " << endl << status << "." << endl 
            << "Failed to get time.  Setting `numeric_primary' to `INVALID_REAL' and "
            << "continuing." << endl;

       @=$$@> = INVALID_REAL;  
   }
@q *** (3) @>

   else
   {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "In parser, rule `numeric_primary: GET_TIME get_time_option_list':"
               << endl
               << "`get_time_ldf' succeeded, returning 0." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

      @=$$@> = r;
   }

@q *** (3) @>

   scanner_node->time_type         = 0;
   scanner_node->time_result_entry = 0;
   scanner_node->time_longitude_val = INVALID_REAL;
   scanner_node->time_gst_zero_val = INVALID_REAL;  

   if (scanner_node->time_string_ptr)
   {
       delete scanner_node->time_string_ptr;
       scanner_node->time_string_ptr = 0;
   }

   if (scanner_node->time_longitude_str)
   {
       delete scanner_node->time_longitude_str;
       scanner_node->time_longitude_str = 0;
   }

   if (scanner_node->time_longitude_str)
   {
       delete scanner_node->time_longitude_str;
       scanner_node->time_longitude_str = 0;
   }

   if (scanner_node->time_gst_zero_str)
   {
       delete scanner_node->time_gst_zero_str;
       scanner_node->time_gst_zero_str = 0;
   }


};

@q ** (2) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> get_time_option_list@>@/
@=%type <int_value> get_time_option@>@/
@=%type <int_value> time_type@>@/

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option_list: /* Empty  */@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option_list: /* Empty  */'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_type         = 0;
   scanner_node->time_result_entry = 0;
   scanner_node->time_longitude_val = INVALID_REAL;
   scanner_node->time_gst_zero_val = INVALID_REAL;  

   if (scanner_node->time_string_ptr)
   {
       delete scanner_node->time_string_ptr;
       scanner_node->time_string_ptr = 0;
   }

   if (scanner_node->time_longitude_str)
   {
       delete scanner_node->time_longitude_str;
       scanner_node->time_longitude_str = 0;
   }

   if (scanner_node->time_gst_zero_str)
   {
       delete scanner_node->time_gst_zero_str;
       scanner_node->time_gst_zero_str = 0;
   }

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option_list: get_time_option_list get_time_option@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option_list: get_time_option_list get_time_option'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 0;
};



@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: time_type@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: time_type'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "time_type == " << @=$1@> << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_type = @=$1@>;

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=time_type: LOCAL@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `time_type: LOCAL'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 0;
};


@q ** (2) @>
@
@<Define rules@>=
@=time_type: SIDEREAL@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `time_type: SIDEREAL'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 1;
};

@q ** (2) @>
@
@<Define rules@>=
@=time_type: TRUE_SOLAR@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `time_type: TRUE_SOLAR'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 2;
};

@q ** (2) @>
@
@<Define rules@>=
@=time_type: MEAN_SOLAR@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `time_type: MEAN_SOLAR'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 3;
};

@q ** (2) @>
@
@<Define rules@>=
@=time_type: UNIVERSAL@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `time_type: UNIVERSAL'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   @=$$@> = 4;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_RESULT numeric_vector_variable@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_RESULT numeric_vector_variable'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_result_entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_DATE string_primary@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_DATE string_primary'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_string_ptr = static_cast<string*>(@=$2@>);

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_LONGITUDE numeric_primary@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_LONGITUDE numeric_primary'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_longitude_val = static_cast<real>(@=$2@>);

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_LONGITUDE string_primary@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_LONGITUDE string_primary'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   scanner_node->time_longitude_str = static_cast<string*>(@=$2@>);

   @=$$@> = 0;

@q *** (3) @>

};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_GST_ZERO numeric_primary@>@/
{
   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_GST_ZERO numeric_primary'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

   scanner_node->time_gst_zero_val = static_cast<real>(@=$2@>);

   @=$$@> = 0;
};

@q ** (2) @>
@
@<Define rules@>=
@=get_time_option: WITH_GST_ZERO string_primary@>@/
{
@q *** (3) @>

   @<Common declarations for rules@>@; 
 
#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
       cerr_strm << "*** Parser: Rule `get_time_option: WITH_GST_ZERO string_primary'."
                 << endl;  

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }  /* |if (DEBUG)|  */

#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   scanner_node->time_gst_zero_str = static_cast<string*>(@=$2@>);

   @=$$@> = 0;

@q *** (3) @>

};


@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

