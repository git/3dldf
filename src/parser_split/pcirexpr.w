@q pcirexpr.w @> 
@q Created by Laurence Finston Thu May  6 15:21:19 MEST 2004  @>
       
@q * (1) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing.  @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version.  @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details.  @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA@>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html.@>

@q ("@@" stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de@>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)@>

@q * (0) circle expressions.  @>
@** circle expressions.

\LOG
\initials{LDF 2004.06.17.}
Created this file.
\ENDLOG 

@q * (1) circle primary.  @>
@* \�circle primary>.
  
\LOG
\initials{LDF 2004.06.17.}  
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_primary@>@/

@q ** (2) circle_primary --> circle_variable.@>
@*1 \�circle primary> $\longrightarrow$ \�circle variable>.  

\LOG
\initials{LDF 2004.06.17.}
Added this rule.

\initials{LDF 2004.11.22.}
No longer issuing an error message if |entry->object == 0|.
This condition occurs legitimately when one tries to show
an ``unknown |circle|''.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=circle_primary: circle_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

    @=$$@> = static_cast<void*>(create_new<Circle>(
                                  static_cast<Circle*>(
                                     entry->object))); 

};

@q ** (2) circle_primary --> circle_argument.@>
@*1 \�circle primary> $\longrightarrow$ \�circle argument>.  

@q ** (2) circle_primary --> ( circle_expression )  @>
@*1 \�circle primary> $\longrightarrow$ `\.{\LP}' 
\�circle expression> `\.{\RP}'.

\LOG
\initials{LDF 2004.06.17.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=circle_primary: LEFT_PARENTHESIS circle_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q ** (2) circle_primary --> GET_CIRCLE numeric_primary sphere_primary@>
@*1 \�circle primary> $\longrightarrow$ \.{GET\_CIRCLE}
\�numeric primary> \�sphere primary>. 

\LOG
\initials{LDF 2004.10.13.}
Added this rule.

\initials{LDF 2009.09.09.}
Changed |@=$2@>| from \.{INTEGER} to \�numeric primary>.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: GET_CIRCLE numeric_primary sphere_primary@>@/
{

  Sphere* s = static_cast<Sphere*>(@=$3@>);

  Circle* c;

  const Circle* t = s->get_circle_ptr(@=$2@>); 

@q **** (4) Error handling:  |Sphere::get_circle_ptr| returned 0.@>   

@ Error handling:  |Sphere::get_circle_ptr| returned 0.
\initials{LDF 2004.10.13.}

@<Define rules@>=

  if (t == static_cast<const Circle*>(0))
   {

      delete s;

      @=$$@> = static_cast<void*>(0);

   } /* |if (t == 0)|  */

@q **** (4) Try to allocate memory on free store for |circle_primary|.@>   

@ Try to allocate memory on free store for \�circle primary>.
\initials{LDF 2004.10.13.}

@<Define rules@>=

  else /* |t != 0|  */
     {

         c = create_new<Circle>(t, static_cast<Scanner_Node>(parameter));

@q **** (4) Delete |s|, set |circle_primary| to                 @>   
@q **** (4) |static_cast<void*>(c)|, and exit rule successfully.@>   

@ Delete |s|, set |circle_primary| to                 
|static_cast<void*>(c)|, and exit rule successfully.   
\initials{LDF 2004.10.13.}

@<Define rules@>=

  delete s;

  @=$$@> = static_cast<void*>(c);

  }  /* |else| (|t != 0|)  */

};

@q ** (2) circle_primary --> GET_LAST_CIRCLE sphere_variable@>
@*1 \�circle primary> $\longrightarrow$ \.{GET\_LAST\_CIRCLE} \�sphere variable>. 
\initials{LDF 2022.11.29.}

\LOG
\initials{LDF 2022.11.29.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: GET_LAST_CIRCLE sphere_variable@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_primary: circle_primary: GET_LAST_CIRCLE sphere_variable'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = static_cast<void*>(0);

  Sphere *s = 0;

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  if (entry && entry->object)
  {
     s = static_cast<Sphere*>(entry->object);
   
     if (s->circles.size() > 0)
     {
        Circle* c = create_new<Circle>(0);
        *c = *(s->circles[s->circles.size() - 1]);    
        @=$$@> = static_cast<void*>(c);
     }

  }  /* |if|  */

};

@q ** (2) circle_primary --> GET_CIRCLE numeric_primary cone_primary@>
@*1 \�circle primary> $\longrightarrow$ \.{GET\_CIRCLE}
\�numeric primary> \�cone primary>. 
\initials{LDF 2006.11.09.}

\LOG
\initials{LDF 2006.11.09.}
Added this rule.

\initials{LDF 2023.11.15.}
Changed \.{INTEGER} to \�numeric primary>.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: GET_CIRCLE numeric_primary cone_primary@>@/
{

   @=$$@> = quad_surf_get_element_func(parameter, 
                                       GET_CIRCLE, 
                                       Shape::CONE_TYPE, 
                                       @=$3@>,
                                       @=$2@>);

};

@q ** (2) circle_primary --> PROJECT circle_expression ONTO sphere_variable with_test_optional@>
@*1 \�circle primary> $\longrightarrow$ \.{PROJECT} \�circle expression> ONTO \�sphere variable>
\�with test optional>.
\initials{LDF 2022.12.15.}

\LOG
\initials{LDF 2022.12.15.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: PROJECT circle_expression ONTO sphere_variable with_test_optional@>@/
{
@q **** (4) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_primary: PROJECT circle_expression ONTO sphere_variable with_test_optional'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

  Circle *c = static_cast<Circle*>(@=$2@>);

  bool do_test;
    
  if (@=$5@> == WITH_TEST)
     do_test = true;
  else 
     do_test = false;
        
#if DEBUG_COMPILE
   if (DEBUG)
   {       
      cerr << "do_test == " << do_test << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

  Sphere *s = 0;

  entry = static_cast<Id_Map_Entry_Node>(@=$4@>);

@q **** (4) @>

  if (entry && entry->object)
  {
@q ***** (5) @>

     s = static_cast<Sphere*>(entry->object);
  
     status = c->project_onto_sphere(*s, 0, do_test, scanner_node);

     if (status != 0)
     {
        cerr_strm << thread_name 
                  << "ERROR!  In parser, rule `circle_primary: PROJECT circle_expression ONTO sphere_variable with_test_optional':"
                  << endl 
                  << "`Circle::project_onto_sphere' failed, returning " << status << "."
                  << endl 
                  << "Failed to project circle onto sphere."
                  << endl 
                  << "Setting `circle_primary' to NULL and continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str(""); 

        @=$$@> = static_cast<void*>(0);

     }
     else
     { 
#if DEBUG_COMPILE
        if (DEBUG)
        { 
           cerr_strm << thread_name 
                     << "In parser, rule `circle_primary: PROJECT circle_expression ONTO sphere_variable with_test_optional':"
                     << endl 
                     << "`Circle::project_onto_sphere' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str(""); 
        }     
#endif /* |DEBUG_COMPILE|  */@; 

        @=$$@> = static_cast<void*>(c);
 
     }  /* |else|  */

@q ***** (5) @>

  } /* |if (entry && entry->object)| */
  
@q **** (4) @>
 
  else
  {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `circle_primary: PROJECT circle_expression ONTO sphere_variable with_test_optional':"
                 << endl 
                 << "`Id_Map_Entry_Node entry' or `entry->object' is NULL."
                 << endl 
                 << "`sphere_variable' is invalid.  Can't project `circle' onto `sphere'."
                 << endl 
                 << "Setting `circle_primary' to NULL and continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str(""); 

      delete c;
      c = 0;
      @=$$@> = static_cast<void*>(0);
  }

@q **** (4) @>  

};

@q ** (2) circle_primary --> IN_CIRCLE reg_polygon_primary circle_option_list@>
@*1 \�circle primary> $\longrightarrow$ \.{IN\_CIRCLE} 
\�regular polygon primary> \�circle option list>. 

\LOG
\initials{LDF 2004.11.16.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: IN_CIRCLE reg_polygon_primary circle_option_list@>@/
{

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_primary: IN_CIRCLE reg_polygon_primary "
               << "circle_option_list'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

   Reg_Polygon* r = static_cast<Reg_Polygon*>(@=$2@>);

   Circle* c;

   c = create_new<Circle>(0);

   *c = r->in_circle(scanner_node->circle_option_axis, 
                     scanner_node->circle_option_do_test, 
                     scanner_node);
   
   @=$$@> = static_cast<void*>(c); 

};

@q ** (2) @>
@
\LOG
\initials{LDF 2024.04.20.}
Added these type declarations.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <int_value> circle_option_list@>@/
@=%type <int_value> circle_option@>@/

@q *** (3) @>
@
@<Define rules@>=
@=circle_option_list: /* Empty  */@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_option_list: /* Empty  */'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  scanner_node->circle_option_axis    = 1;  /* y-axis is default  */
  scanner_node->circle_option_do_test = true;

   @=$$@> = 0;
};

@q *** (3) @>
@
@<Define rules@>=
@=circle_option_list: circle_option_list circle_option@>@/
{
   @=$$@> = 0;
};

@q *** (3) @>
@
@<Define rules@>=
@=circle_option: WITH_AXIS_X@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_option: WITH_AXIS_X'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  scanner_node->circle_option_axis = 0;

   @=$$@> = 0;
};

@q *** (3) @>
@
@<Define rules@>=
@=circle_option: WITH_AXIS_Y@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_option: WITH_AXIS_Y'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  scanner_node->circle_option_axis = 1;

  @=$$@> = 0;

};

@q *** (3) @>
@
@<Define rules@>=
@=circle_option: WITH_AXIS_Z@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_option: WITH_AXIS_Z'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  scanner_node->circle_option_axis = 2;

  @=$$@> = 0;
};

@q *** (3) @>
@
@<Define rules@>=
@=circle_option: WITH_NO_TEST@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_option: WITH_NO_TEST'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

  scanner_node->circle_option_do_test = false;

  @=$$@> = 0;
};



@q ** (2) circle_primary --> OUT_CIRCLE reg_polygon_primary@>
@*1 \�circle primary> $\longrightarrow$ \.{OUT\_CIRCLE}
\�regular polygon primary>. 

\LOG
\initials{LDF 2004.11.16.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=

@=circle_primary: OUT_CIRCLE reg_polygon_primary@>@/
{

   Reg_Polygon* r = static_cast<Reg_Polygon*>(@=$2@>);

   Circle* c;

         c = create_new<Circle>(0);

   *c = r->out_circle();
   
   @=$$@> = static_cast<void*>(c); 

};

@q ***** (5) circle_primary --> LAST @>
@q ***** (5) circle_vector_expression.@>

@*4 \�circle primary> $\longrightarrow$ 
\.{LAST} \�circle vector expression>.
\initials{LDF 2005.01.14.}

\LOG
\initials{LDF 2005.01.14.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=circle_primary: LAST circle_vector_expression@>@/
{ 
   Circle* c;

         c = create_new<Circle>(0);

   Pointer_Vector<Circle>* pv 
      = static_cast<Pointer_Vector<Circle>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<Circle>*>(0))
      {

          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {

          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2005.01.14.}

@<Define rules@>=

   else 
      {
         *c = *(pv->v[pv->ctr - 1]);
         @=$$@> = static_cast<void*>(c); 
      }
@q ******* (7) @> 

};

@q *** (3) circle_primary: circle_primary NORMALIZED call_metapost_option_list.  @>

@ \�circle primary> $\longrightarrow$ \�circle primary> \.{NORMALIZED} 
\�call metapost option list>.
\initials{LDF 2022.05.18.}

\LOG
\initials{LDF 2022.05.18.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=
@=circle_primary: circle_primary NORMALIZED call_metapost_option_list@>@/
{
@q ***** (5) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
     cerr_strm << thread_name 
               << "*** Parser: `circle_primary: circle_primary NORMALIZED call_metapost_option_list'.";

     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

  }
#endif /* |DEBUG_COMPILE|  */@;

   bool save               = @=$3@> & 1U;
   bool clear              = @=$3@> & 2U;
   bool suppress_mp_stdout = @=$3@> & 4U;
   bool do_transform       = @=$3@> & 8U;    

   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

@q *** (3) @>

   if (@=$3@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$3@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Circle *c = static_cast<Circle*>(@=$1@>);
   Circle q;
   Transform t;

@q ***** (5) @>
@
@<Define rules@>=

  if (c)
  {
@q ****** (6) @>

     status = c->normalize_circle(&q, &t, 0, origin_pt, x_axis_pt, save, 
                                   suppress_mp_stdout, scanner_node);

     if (status != 0)
     {
        cerr_strm << thread_name 
                  << "ERROR!  In parser, rule `circle_primary: circle_primary NORMALIZED "
		  << "call_metapost_option_list':"
                  << endl 
                  << "`Circle::normalize_circle' failed, returning " << status << "."
                  << endl 
                  << "Couldn't normalize circle."
                  << endl 
                  << "Returning unchanged `circle_primary' from right-hand side as result "
		  << "and continuing.";

        log_message(cerr_strm);
        cerr_message(cerr_strm);
        cerr_strm.str(""); 

     }

@q ****** (6) @>

     else
     { 
#if DEBUG_COMPILE
        if (DEBUG)
     	{ 
     	   cerr_strm << thread_name 
     	             << "In parser, rule `circle_primary: circle_primary NORMALIZED "
                     << "call_metapost_option_list':"
     	             << endl 
     	             << "`Circle::normalize_circle' succeeded, returning 0.";

     	   log_message(cerr_strm);
     	   cerr_message(cerr_strm);
     	   cerr_strm.str(""); 



#if 0
     	   q.show("q:");
#endif 
 
     	}  
#endif /* |DEBUG_COMPILE|  */@; 

        *c = q;

#if 0 
        c->show("*c after assignment:");
#endif 


     }

  }  /* |if (c)| */

@q ***** (5) @>

  else
  {
      cerr_strm << thread_name 
                << "WARNING!  In parser, rule `circle_primary: circle_primary NORMALIZED "
		<< "call_metapost_option_list':"
                << endl 
                << "`circle_primary' on right-hand side is NULL.  Can't normalize circle."
                << endl 
                << "Returning `circle_primary' from right-hand side as result unchanged."
                << endl; 

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str(""); 
 
  }

  if (x_axis_pt)
  {
     delete x_axis_pt;
     x_axis_pt = 0;
  }

  if (origin_pt)
  {
     delete origin_pt;
     origin_pt = 0;
  }

  if (scanner_node->tolerance)
  {
     delete scanner_node->tolerance;
     scanner_node->tolerance = 0;
  } 

  @=$$@> = static_cast<void*>(c);

@q ***** (5) @>

};

@q *** (3) circle_primary --> GET_BASE_CIRCLE cone_primary.@>
@*1 \�circle primary> $\longrightarrow$ \.{GET\_BASE\_CIRCLE}
\�cone primary>. 
\initials{LDF 2024.11.28.}

\LOG
\initials{LDF 2024.11.28.}
Added this rule.
\ENDLOG

@q **** (4) Definition.@> 

@<Define rules@>=

@=circle_primary: GET_BASE_CIRCLE cone_primary@>@/
{
@q ***** (5) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
     {
         cerr_strm << thread_name << "*** Parser:  `circle_primary: "
                   << "GET_BASE_CIRCLE cone_primary'. ";

         log_message(cerr_strm);
         cerr_message(cerr_strm);
         cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */

@q ***** (5) @>

   Cone *c = static_cast<Cone*>(@=$2@>);

   if (c->type == Shape::CIRCULAR_CONE_TYPE)
   {
       Circle *d = create_new<Circle>(0);

       *d = *(c->circles[0]);

       @=$$@> = static_cast<void*>(d);

   }

@q ***** (5) @>

   else
   {
      cerr << "WARNING!  In parser, rule `circle_primary: GET_BASE_CIRCLE cone_primary':" 
           << endl 
           << "`cone_primary' isn't a circular cone:" << endl 
           << "c->type == " << c->type << " (" << type_name_map[c->type] << ")" 
           << endl
           << "Can't return base.  Will try to continue." << endl;

      @=$$@> = static_cast<void*>(0);

   }



@q ***** (5) @>

   delete c;
   c = 0;

};

@q * (1) circle secondary.  @>
@* \�circle secondary>.
\initials{LDF 2004.06.17.}  

\LOG
\initials{LDF 2004.06.17.}  
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_secondary@>
  
@q ** (2) circle secondary --> circle_primary.@>
@*1 \�circle secondary> $\longrightarrow$ \�circle primary>.

\LOG
\initials{LDF 2004.06.17.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=circle_secondary: circle_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: circle_secondary --> circle_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) circle secondary --> circle_secondary transformer.  @>
@*1 \�circle secondary> $\longrightarrow$ \�circle secondary> 
\�transformer>.
\initials{LDF 2004.06.17.}

\LOG
\initials{LDF 2004.06.17.}
Added this rule.

\initials{LDF 2004.10.05.}
Made the debugging output thread-safe.

\initials{LDF 2005.12.16.}
@:BUG FIX@> BUG FIX: Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=circle_secondary: circle_secondary transformer@>@/
{
   Circle* c = static_cast<Circle*>(@=$1@>);
   Transform* t = static_cast<Transform*>(@=$2@>);

   *c *= *t;

   delete t;
   t = 0;

   @=$$@> = static_cast<void*>(c); 

};

@q ** (2) circle secondary --> circle_secondary REFLECTED_IN @>
@q ** (2) path_expression.@> 

@*1 \�circle secondary> $\longrightarrow$ 
\�circle secondary> 
\.{REFLECTED\_IN} \�path expression>.

\LOG
\initials{LDF 2004.10.05.}
Added this rule.

\initials{LDF 2004.10.09.}
Changed \�optional in> to \.{IN}.

\initials{LDF 2004.12.03.}
Changed |REFLECTED IN| to |REFLECTED_IN|.

\initials{LDF 2005.10.24.}
Changed |path_like_expression| to |path_expression|.
Removed debugging code.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=circle_secondary: circle_secondary REFLECTED_IN @>
@=path_expression@>@/ 
{

     Circle* c = reflect_in_func<Circle>(static_cast<Scanner_Node>(parameter),
                                         static_cast<Circle*>(@=$1@>),
                                         static_cast<Path*>(@=$3@>));

     @=$$@> = static_cast<void*>(c);

};

@q * (1) circle tertiary.@>
@* \�circle tertiary>.

\LOG
\initials{LDF 2004.06.17.} 
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_tertiary@>

@q ** (2) circle tertiary --> circle_secondary.  @>
@*1 \�circle tertiary> $\longrightarrow$ \�circle secondary>.

\LOG
\initials{LDF 2004.06.17.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=circle_tertiary: circle_secondary@>@/
{
  @=$$@> = @=$1@>;
};

@q **** (4) circle_tertiary: sphere_tertiary INTERSECTION plane_secondary. @> 

@*3 \�circle tertiary> $\longrightarrow$ \�sphere tertiary> \.{INTERSECTION} 
\�plane secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.

\initials{LDF 2005.10.30.}
Changed |rectangle_secondary| to |plane_secondary|.

\initials{LDF 2005.10.31.}
Changed the call to |Scan_Parse::sphere_plane_intersection_func| 
to one to |Scan_Parse::ellipsoid_like_plane_intersection_func|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=circle_tertiary: sphere_tertiary INTERSECTION plane_secondary@>@/
{

   @=$$@> = Scan_Parse::ellipsoid_like_plane_intersection_func<Sphere>(
                                    static_cast<Sphere*>(@=$1@>),
                                    static_cast<Plane*>(@=$3@>),
                                    parameter);         
};

@q * (1) circle expression.  @>
@* \�circle expression>.

\LOG
\initials{LDF 2004.06.17.}  
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> circle_expression@>

@q ** (2) circle expression --> circle_tertiary.  @>
@*1 \�circle expression> $\longrightarrow$ \�circle tertiary>.

\LOG
\initials{LDF 2004.06.17.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=circle_expression: circle_tertiary@>@/
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

