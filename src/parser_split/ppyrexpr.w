@q ppyrexpr.w @> 
@q Created by Laurence Finston Mo 27. Mai 06:47:20 CEST 2024 @>
       
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 024 The Free Software Foundation, Inc.  @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) prismatoid expressions.  @>
@** prismatoid expressions.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Created this file.
\ENDLOG 

@q * (1) prismatoid_primary.  @>
@* \§prismatoid primary>.
  
\LOG
\initials{LDF 2024.05.27.}
Added this type declaration.
\ENDLOG 

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> prismatoid_primary@>@/

@q ** (2) prismatoid_primary --> prismatoid_variable.@>
@*1 \§prismatoid primary> $\longrightarrow$ \§prismatoid variable>.  

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG 

@q *** (3) Definition.@> 

@<Define rules@>=
@=prismatoid_primary: prismatoid_variable@>@/
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

  else /* |entry != 0 && entry->object != 0|  */

    @=$$@> = static_cast<void*>(create_new<Prismatoid>(
                                  static_cast<Prismatoid*>(
                                     entry->object))); 

};

@q ** (2) prismatoid_primary --> LEFT_PARENTHESIS prismatoid_expression @> 
@q ** (2) RIGHT_PARENTHESIS                                   @>

@*1 \§prismatoid primary> $\longrightarrow$ \.{\LP} 
\§prismatoid expression> \.{\RP}.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=prismatoid_primary: LEFT_PARENTHESIS prismatoid_expression RIGHT_PARENTHESIS@>@/
{

  @=$$@> = @=$2@>;

};

@q ***** (5) prismatoid_primary --> LAST @>
@q ***** (5) prismatoid_vector_expression.@>

@*4 \§prismatoid primary> $\longrightarrow$ 
\.{LAST} \§prismatoid vector expression>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=prismatoid_primary: LAST prismatoid_vector_expression@>@/
{
   Prismatoid* c;

         c = create_new<Prismatoid>(0);

   Pointer_Vector<Prismatoid>* pv 
      = static_cast<Pointer_Vector<Prismatoid>*>(@=$2@>);

@q ******* (7) Error handling:  |pv == 0|.@> 

@ Error handling:  |pv == 0|.
\initials{LDF 2024.05.27.}

@<Define rules@>=

   if (pv == static_cast<Pointer_Vector<Prismatoid>*>(0))
      {

          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |if (pv == 0)|  */

@q ******* (7) Error handling:  |pv->ctr == 0|.@> 

@ Error handling:  |pv->ctr == 0|.
\initials{LDF 2024.05.27.}

@<Define rules@>=

   else if (pv->ctr == 0)
      {

          delete c;

          @=$$@> = static_cast<void*>(0);

      }  /* |else if (pv->ctr == 0)|  */

@q ******* (7) |pv != 0 && pv->ctr > 0|.@> 

@ |pv != 0 && pv->ctr > 0|.  Set |@=$$@>| to |*(pv->v[pv->ctr - 1])|.
\initials{LDF 2024.05.27.}

@<Define rules@>=

   else 
      {
         *c = *(pv->v[pv->ctr - 1]);
         @=$$@> = static_cast<void*>(c); 
      }
@q ******* (7) @> 

};

@q ***** (5) prismatoid_primary --> GET_INNER_PRISMATOID ellipsoid_primary.@>

@*4 \§prismatoid primary> $\longrightarrow$ \.{GET\_INNER\_PRISMATOID} \§ellipsoid primary>.
\initials{LDF 2024.12.03.}

\LOG
\initials{LDF 2024.12.03.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=prismatoid_primary: GET_INNER_PRISMATOID ellipsoid_primary@>@/
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `prismatoid_primary: GET_INNER_PRISMATOID ellipsoid_primary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Ellipsoid *ellipsoid = static_cast<Ellipsoid*>(@=$2@>);

   Prismatoid *prismatoid = create_new<Prismatoid>(0);

   status = ellipsoid->get_prismatoid(prismatoid, 0, scanner_node);

@q ******* (7) @>
 
   if (status != 0)
   {
      cerr << "ERROR! In parser, rule `prismatoid_primary: GET_INNER_PRISMATOID ellipsoid_primary':"
           << endl 
           << "`Ellipsoid::get_prismatoid' failed, returning " << status << "." << endl 
           << "Failed to get prismatoid.  Will try to continue." << endl;
   }

@q ******* (7) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `prismatoid_primary: GET_INNER_PRISMATOID ellipsoid_primary':"
           << endl 
           << "`Ellipsoid::get_prismatoid' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

cerr << "Type <ENTER> to continue:";
getchar();

@q ******* (7) @>

   @=$$@> = static_cast<void*>(prismatoid);

   delete ellipsoid;
   ellipsoid = 0;

@q ******* (7) @>

};

@q ***** (5) prismatoid_primary --> GET_OUTER_PRISMATOID ellipsoid_primary.@>

@*4 \§prismatoid primary> $\longrightarrow$ \.{GET\_OUTER\_PRISMATOID} \§ellipsoid primary>.
\initials{LDF 2024.12.03.}

\LOG
\initials{LDF 2024.12.03.}
Added this rule.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=prismatoid_primary: GET_OUTER_PRISMATOID ellipsoid_primary@>@/
{ 
@q ******* (7) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
      cerr_strm << "*** Parser: `prismatoid_primary: GET_OUTER_PRISMATOID ellipsoid_primary'."
                << endl;

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   Ellipsoid *ellipsoid = static_cast<Ellipsoid*>(@=$2@>);

   Prismatoid *prismatoid = create_new<Prismatoid>(0);

   status = ellipsoid->get_prismatoid(prismatoid, 0, scanner_node);

@q ******* (7) @>
 
   if (status != 0)
   {
      cerr << "ERROR! In parser, rule `prismatoid_primary: GET_OUTER_PRISMATOID ellipsoid_primary':"
           << endl 
           << "`Ellipsoid::get_prismatoid' failed, returning " << status << "." << endl 
           << "Failed to get prismatoid.  Will try to continue." << endl;
   }

@q ******* (7) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `prismatoid_primary: GET_OUTER_PRISMATOID ellipsoid_primary':"
           << endl 
           << "`Ellipsoid::get_prismatoid' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

cerr << "Type <ENTER> to continue:";
getchar();

@q ******* (7) @>

   @=$$@> = static_cast<void*>(prismatoid);

   delete ellipsoid;
   ellipsoid = 0;

@q ******* (7) @>

};



@q * (1) prismatoid_secondary.  @>
@* \§prismatoid secondary>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> prismatoid_secondary@>
  
@q ** (2) prismatoid secondary --> prismatoid_primary.@>
@*1 \§prismatoid secondary> $\longrightarrow$ \§prismatoid primary>.

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=prismatoid_secondary: prismatoid_primary@>@/
{
#if DEBUG_COMPILE
  bool DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr << "\n*** Parser: prismatoid_secondary --> prismatoid_primary "
           << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

  @=$$@> = @=$1@>;

};

@q ** (2) prismatoid secondary --> prismatoid_secondary transformer.  @>
@*1 \§prismatoid secondary> $\longrightarrow$ \§prismatoid secondary> 
\§transformer>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.

\initials{LDF 2024.05.27.}
@:BUG FIX@> BUG FIX:  Now deleting |Transform* t|.
\ENDLOG

@<Define rules@>=
@=prismatoid_secondary: prismatoid_secondary transformer@>@/
{

  Prismatoid* p = static_cast<Prismatoid*>(@=$1@>);
  Transform* t = static_cast<Transform*>(@=$2@>);

  *p *= *t;

  delete t;

  @=$$@> = static_cast<void*>(p); 

};

@q * (1) prismatoid tertiary.@>
@* \§prismatoid tertiary>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> prismatoid_tertiary@>

@q ** (2) prismatoid tertiary --> prismatoid_secondary.  @>
@*1 \§prismatoid tertiary> $\longrightarrow$ \§prismatoid secondary>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=prismatoid_tertiary: prismatoid_secondary@>@/
{

  @=$$@> = @=$1@>;

};

@q * (1) prismatoid expression.@>
@* \§prismatoid expression>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> prismatoid_expression@>

@q ** (2) prismatoid expression --> prismatoid_tertiary.  @>
@*1 \§prismatoid expression> $\longrightarrow$ \§prismatoid tertiary>.
\initials{LDF 2024.05.27.}

\LOG
\initials{LDF 2024.05.27.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=prismatoid_expression: prismatoid_tertiary@>@/
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

