/* process_files_functions.cxx  */
/* Created by Laurence D. Finston (LDF) Do 26. Sep 14:01:44 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;


#include "colordefs_extern.hxx"

int
divide_image(string filename, string prefix, int shift_val_x, int shift_val_y, 
             string gravity_val, 
             int limit = 1000,
             int target_width = 1726, 
             int target_height = 982);

/* ** (2) divide_image  */

int
divide_image(string filename, string prefix, int shift_val_x, int shift_val_y, 
             string gravity_val, int limit, int target_width, int target_height)
{
/* *** (3) */

   int status;

   stringstream s;

   char buffer[128];

   string zeroes;

   int width  = 0;
   int height = 0;

   FILE *fp = 0;

   cerr << "Entering `divide_image'." << endl;

   cerr << "filename      == " << filename << endl
        << "prefix        == " << prefix << endl
        << "shift_val_x   == " << shift_val_x << endl
        << "shift_val_y   == " << shift_val_y << endl
        << "gravity_val   == " << gravity_val << endl
        << "limit         == " << limit << endl
        << "target_width  == " << target_width << endl
        << "target_height == " << target_height << endl;

/* *** (3) */

   for (int i = 0; i <= limit; ++i)
   {

      cerr << "i == " << i << endl;

      memset(buffer, '\0', 128);
      s.str("");
  
      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      s << "convert -crop " << target_width << "x" << target_height << "+" 
        << (i*shift_val_x) << "+" << (i*shift_val_y) << " -gravity " << gravity_val << " "
        << filename << " " << prefix << zeroes << i << ".png;";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   

      s.str("");

      s << "identify " << prefix << zeroes << i << ".png | cut -d \" \" -f 3 | tr x \" \"";
      cerr << "s.str() == " << s.str() << endl;

      fp = popen(s.str().c_str(), "r");

      fread(buffer, 1, 127, fp);

      cerr << "buffer == " << buffer << endl;

      pclose(fp);
      fp = 0;

      s.str("");

      s << buffer;

      cerr << "s.str() == " << s.str() << endl;

      s >> width;
      s >> height;

      cerr << "width  == " << width << endl
           << "height == " << height << endl;

      if (width < target_width)
      {
         cerr << "width < target_width:" << endl 
              << "width        == " << width << endl
              << "target_width == " << target_width << endl
              << "Breaking." << endl;

         s.str("");
         s << "rm " << prefix << zeroes << i << ".png";

         cerr << "s.str() == " << s.str() << endl;

         status = system(s.str().c_str());

         cerr << "status == " << status << endl;


  
         break;
      }

      if (height < target_height)
      {
         cerr << "height < target_height:" << endl 
              << "height        == " << height << endl
              << "target_height == " << target_height << endl
              << "Breaking." << endl;

         s.str("");
         s << "rm " << prefix << zeroes << i << ".png";

         cerr << "s.str() == " << s.str() << endl;

         status = system(s.str().c_str());

         cerr << "status == " << status << endl;

  
         break;
      }

   }  /* |for|  */


/* *** (3) */

   cerr << "Exiting `divide_image' successfully with return value 0." << endl;

   return 0;

}  /* End of |divide_image| definition  */

/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */
