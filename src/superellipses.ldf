%% superellipses.ldf
%% Created by Laurence D. Finston (LDF) Di 22. Okt 23:21:02 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

verbatim_metapost "prologues := 3;outputtemplate := \"%j_%2c.eps\";";


input "plainldf.lmc";

fig_ctr := 0;

%% ** (2) Superellipses

%% *** (3) Get rectangle

beginfig(fig_ctr);

  rectangle r[];
  superellipse s[];
  point p[];

  s0 := get_superellipse with_resolution 64 with_superness .75 with_a 2 with_b 3;
  r0 := get_rectangle s0;

  rotate s0 (90, 0);
  rotate r0 (90, 0);

 

  draw r0 with_color blue;
  draw s0 with_color red;

  n := (size s0) - 1;
  
  for i = 0 step 1 until n:
    p[i] := get_point (i) s0;
  endfor;
  
  dotlabel.rt( "0",  p[0]);
  dotlabel.rt( "4",  p[4]);
  dotlabel.urt( "8 ", p[8]);
  dotlabel.urt("12", p[12]);
  dotlabel.top("16", p[16]);
  dotlabel.ulft("20", p[20]);
  dotlabel.ulft("24", p[24]);
  dotlabel.lft("28", p[28]);
  dotlabel.lft("32", p[32]);
  dotlabel.lft("36", p[36]);
  dotlabel.llft("40", p[40]);
  dotlabel.llft("44", p[44]);
  dotlabel.bot("48", p[48]);
  dotlabel.lrt("52", p[52]);
  dotlabel.lrt("56", p[56]);
  dotlabel.rt("60", p[60]);

  
  
endfig with_projection parallel_x_y;
bye;

  % numeric n;
  
  % pickup medium_pen;


  % 


  
  % 



  % rotate s0 (90, 0);

  % rotate s0 (0, 45);
  % rotate s0 (45, 0);

  
  % endfig with_projection parallel_x_y;

  % n := (size s0) - 1;






%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

