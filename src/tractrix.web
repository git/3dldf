@q tractrix.web @>
    
@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2023, 2024, 2025 The Free Software Foundation, Inc.                    @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * Tractrix.@>
@** Tractrix\quad ({\tt tractrix\PERIOD web}).\hfil

\LOG
Added this file.
\ENDLOG 

@f Tractrix Path

@q * Include files.@>
@ Include files.

\LOG
\initials{LDF 2004.05.21.}  Now including \filename{pens.web}.
\ENDLOG 

@<Include files@>=
#include "loader.h++"
#include "pspglb.h++"
#include "io.h++"
#include "creatnew.h++"
#include "pntrvctr.h++"
#include "primes.h++"
#include "complex.h++"
#include "matrices.h++"
#include "colors.h++"
#include "transfor.h++"
#include "pens.h++"
#include "dashptrn.h++"
#include "shapes.h++"
#include "pictures.h++"  
#include "points.h++"
#include "lines.h++"
#include "planes.h++"
#include "nurbs.h++"
#include "cnnctrtp.h++"
#include "paths.h++"
#include "focsfncs.h++"
#include "curves.h++"
#include "polygons.h++"
#include "triangle.h++"
#include "rectangs.h++"
#include "conicsct.h++"
#include "ellipses.h++"
  
@q * (1) Tractrix Class definition.@>
@* {\bf Tractrix} class definition.

\LOG
\initials{LDF 2023.04.25.}
Added this section.
\ENDLOG 

@q ** (2) Definition.@> 

@<Define |class Tractrix|@>=
class Tractrix
{

  friend int yyparse(yyscan_t);

  protected:

  public:

    @<Declare |Tractrix| functions@>@;
};

@q * (1) @>
@
@<Declare |Tractrix| functions@>=
Tractrix(void);

@
@<Define |Tractrix| functions@>=
Tractrix::Tractrix(void)
{
   return;
}

@q ** (2) @>
@
@q *** (3) @>

@<Declare |Tractrix| functions@>=
void 
show(string text = "", 
     char coords = 'w',
     const bool do_persp = true, 
     const bool do_apply = true,
     Focus* f = 0, 
     const unsigned short proj = 0,
     const real factor = 1,
     int show_connectors = 0);


@q *** (3) @>
@
@<Define |Tractrix| functions@>=
void
Tractrix::show(string text, char coords,
               const bool do_persp, const bool do_apply,
               Focus* f, const unsigned short proj,
               const real factor,
               int show_connectors)
{
   cerr << "Tractrix:" << endl;

   return;
}

@q * (1) Putting Tractrix together.@>
@* Putting {\bf Tractrix} together.
\initials{LDF Undated.}

This is what's compiled.
\initials{LDF Undated.}

@c
@<Include files@>@;
@<Define |class Tractrix|@>@;
@<Define |Tractrix| functions@>@;

@ This is what's written to \filename{tractrix.h}.
\initials{LDF 2023.04.25.}

@(tractrix.h@>=
@<Define |class Tractrix|@>@;

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q * Local variables for Emacs.@>

@q Local Variables: @>
@q mode:CWEB @>
@q eval:(display-time) @>
@q eval:(read-abbrev-file) @>
@q indent-tabs-mode:nil @>
@q eval:(outline-minor-mode) @>
@q End: @>
