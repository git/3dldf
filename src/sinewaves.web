@q sinewaves.web @>
@q Created by Laurence D. Finston (LDF) Mo 14. Aug 07:09:31 CEST 2023 @>
    
@q * Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2023, 2024, 2025 The Free Software Foundation, Inc.                    @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * Sinewave.@>
@** Sinewave\quad ({\tt sinewaves\PERIOD web}).\hfil

\LOG
Added this file.
\ENDLOG 

@f Sinewave Path

@q * Include files.@>
@ Include files.
\initials{LDF 2023.08.14.}

@<Include files@>=
#include "loader.h++"
#include "pspglb.h++"
#include "io.h++"
#include "creatnew.h++"
#include "pntrvctr.h++"
#include "primes.h++"
#include "complex.h++"
#include "matrices.h++"
#include "colors.h++"
#include "transfor.h++"
#include "pens.h++"
#include "dashptrn.h++"
#include "shapes.h++"
#include "pictures.h++"  
#include "points.h++"
#include "lines.h++"
#include "planes.h++"
#include "nurbs.h++"
#include "cnnctrtp.h++"
#include "paths.h++"
#include "focsfncs.h++"
#include "curves.h++"
#include "polygons.h++"
#include "triangle.h++"
#include "rectangs.h++"
#include "conicsct.h++"
#include "ellipses.h++"
#include "utility.h++"
  
@q * (1) Sinewave Class definition.@>
@* {\bf Sinewave} class definition.

\LOG
\initials{LDF 2023.04.25.}
Added this section.
\ENDLOG 

@q ** (2) Definition.@> 

@<Define |class Sinewave|@>=
class Sinewave : public Path
{

  friend int yyparse(yyscan_t);

  protected:

  Point center;
  Point start_pt;
  Transform transform;

  vector<pair<int,Point> > zero_points;
  vector<Path>  enclosing_paths; 

  real period;
  real amplitude;
  real frequency;
  real phase;
  real increment;

  public:

    @<Declare |Sinewave| functions@>@;
};

@q * (1) @>
@
@<Declare |Sinewave| functions@>=
Sinewave(void);

@
@<Define |Sinewave| functions@>=
Sinewave::Sinewave(void)
{

   start_pt = ORIGIN;
   center   = ORIGIN;

   Point *p = create_new<Point>(0);
   points.push_back(p);

   period    = 0;
   amplitude = 0;
   frequency = 0;
   phase     = 0;
   increment = 0;

   return;

}


@q * (1) Set.  @>
@ Set.  
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this function.
\ENDLOG

@q ** (2) Declaration  @>

@<Declare |Sinewave| functions@>=
int
set(Scanner_Node scanner_node);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::set(Scanner_Node scanner_node)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::set'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   zero_points.clear();
   enclosing_paths.clear(); 

   period    = scanner_node->sinewave_period_value;
   amplitude = scanner_node->sinewave_amplitude_value;
   frequency = scanner_node->sinewave_frequency_value;
   phase     = scanner_node->sinewave_phase_value;
   increment = scanner_node->sinewave_increment_value;
 
   if (frequency <= 0)
   {
       cerr << "ERROR!  In `Sinewave::set':" << endl 
            << "`frequency' == " << frequency << " (<= 0)" 
            << endl
            << "This is not permitted.  `frequency' must be > 0."
            << endl 
            << "Exiting function unsuccessfully with return value 1."
            << endl;

       return 1;
   }

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "period     == " << period << endl 
           << "amplitude  == " << amplitude << endl  
           << "frequency  == " << frequency << endl  
           << "phase      == " << phase << endl      
           << "increment  == " << increment << endl
           << "truncate   == " << truncate << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (scanner_node->sinewave_start_pt)
      start_pt = *static_cast<Point*>(scanner_node->sinewave_start_pt);
   else
      start_pt = ORIGIN;

   int index = -1;
int x;
   zero_points.push_back(make_pair(index++, start_pt));

   if (scanner_node->sinewave_transform)
      transform = *static_cast<Transform*>(scanner_node->sinewave_transform);
   else
      transform.reset();

   center.set(.5*period, 0, 0);

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   Path::clear();

   Path::operator+=("..");
   Point A;

   Path Q;

   bool start_pt_flag = false;

/* !! START HERE:  LDF 2023.09.03.  Put a point on the x-axis with y-coord 0
   on Q first.  Do something similar for the last point.   */

   Point next_pt;
   Point temp_pt0;
   Point temp_pt1;
   Point temp_pt2;

@q *** (3) @>

   for (real i = 0; i <= period + increment; i += increment)
   {
@q **** (4) @>

 
       A = start_pt;
       A.shift(i, amplitude*sin((i*frequency) + (phase*frequency)));
       Path::operator+=(A);

       if (A.get_y() == 0.0)
       {
          zero_points.push_back(make_pair(index, temp_pt2));
       }

       ++index;

       next_pt = start_pt;
       next_pt.shift(i+increment, amplitude*sin(((i+increment)*frequency) + (phase*frequency)));

       if (   (A.get_y() > 0 && next_pt.get_y() < 0)
           || (A.get_y() < 0 && next_pt.get_y() > 0))
       {

          if (A.get_y() < next_pt.get_y())
          {
             temp_pt0 = A;
             temp_pt1 = next_pt;
          }
          else if (A.get_y() > next_pt.get_y())
          {
             temp_pt0 = next_pt;
             temp_pt1 = A;
          }

          for(;;)
          {
             temp_pt2 = temp_pt0.mediate(temp_pt1);

             if (fabsf(temp_pt2.get_y()) < 0.001)
             {
#if 0 
                 temp_pt2.show("temp_pt2:");
#endif 
 
                 break;
             }
             else if (temp_pt2.get_y() > 0)
                temp_pt1 = temp_pt2;
             else 
                temp_pt0 = temp_pt2;
          }

          Path::operator+=(temp_pt2);   
          zero_points.push_back(make_pair(index++, temp_pt2));

       } /* |if|  */

   } /* |for|  */

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   if (fabsf(get_last_point().get_y()) > 0.001)
   {
      temp_pt0.set(get_last_point().get_x(), 0, 0);
      zero_points.push_back(make_pair(index++, temp_pt0));
   }

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::set' successfully with return value 0." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return 0;

}  /* End of |Sinewave::set| definition  */

@q ** (2) @>
@
@q *** (3) @>

@<Declare |Sinewave| functions@>=
void 
show(string text = "", 
     char coords = 'w',
     const bool do_persp = true, 
     const bool do_apply = true,
     Focus* f = 0, 
     const unsigned short proj = 0,
     const real factor = 1,
     int show_connectors = 0);


@q *** (3) @>
@
@<Define |Sinewave| functions@>=
void
Sinewave::show(string text, char coords,
               const bool do_persp, const bool do_apply,
               Focus* f, const unsigned short proj,
               const real factor,
               int show_connectors)
{
   cerr << "sinewave:" << endl;

   cerr << "period:     " << period << endl 
        << "amplitude:  " << amplitude << endl 
        << "frequency:  " << frequency << endl 
        << "phase:      " << phase << endl 
        << "increment:  " << increment << endl << endl;

   start_pt.show("start_pt:");

   transform.show("transform:");

   Path::show("path:", coords, do_persp, do_apply,
              f, proj, factor, show_connectors);

   return;

}

@q * (1) Assignment.  @>
@ Assignment.  
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this function.
\ENDLOG

@q ** (2) Declaration. @>

@<Declare |Sinewave| functions@>=
virtual
Sinewave&
operator=(const Sinewave& s);

@q ** (2) Definition. @>
@
@<Define |Sinewave| functions@>=

Sinewave&
Sinewave::operator=(const Sinewave& s)
{
@q *** (3) Preliminaries.@>   

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ @; 
#endif /* |DEBUG_COMPILE|  */@; 

  if (this == &s) /* Make sure it's not self-assignment. */
    return *this;

  center    = s.center;
  period    = s.period;
  amplitude = s.amplitude;
  frequency = s.frequency;
  phase     = s.phase;
  increment = s.increment;

  start_pt = s.start_pt;

  transform = s.transform;

  zero_points.clear();
  enclosing_paths.clear(); 

  zero_points = s.zero_points;
  enclosing_paths   = s.enclosing_paths;

  Path::operator=(static_cast<Path>(s));

  return *this;

} /* End of |Sinewave::operator=(const Sinewave& s)| definition.  */


@q ** (2) Adding two sinewaves (|operator+|)@>
@ Adding two sinewaves (|operator+|)@>
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this function.
\ENDLOG

@q *** (3) Declaration @>

@<Declare |Sinewave| functions@>=
Sinewave
operator+(Sinewave &s);

@q *** (3) Definition @>
@
@<Define |Sinewave| functions@>=
Sinewave
Sinewave::operator+(Sinewave &s)
{
@q **** (4) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::operator+(Sinewave s)'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

   Sinewave u;

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::operator+(Sinewave s)' successfully." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return u;

}  /* End of |Sinewave::operator+| definition  */

@q ** (2) Subtracting one sinewave from another (|operator-|)@>
@ Subtracting one sinewave from another (|operator-|)@>
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this function.
\ENDLOG

@q *** (3) Declaration @>

@<Declare |Sinewave| functions@>=
Sinewave
operator-(Sinewave &s);

@q *** (3) Definition @>
@
@<Define |Sinewave| functions@>=
Sinewave
Sinewave::operator-(Sinewave &s)
{
@q **** (4) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::operator-(Sinewave s)'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

   Sinewave u;

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::operator-(Sinewave s)' successfully." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return u;

}  /* End of |Sinewave::operator-| definition  */

@q ** (2) Adding two sinewaves with assignment (|operator+=|)@>
@ Adding two sinewaves with assignment (|operator+=|)@>
\initials{LDF 2023.08.14.}

\LOG
\initials{LDF 2023.08.14.}
Added this function.
\ENDLOG

@q *** (3) Declaration @>

@<Declare |Sinewave| functions@>=
void
operator+=(Sinewave &s);

@q *** (3) Definition @>
@
@<Define |Sinewave| functions@>=
void
Sinewave::operator+=(Sinewave &s)
{
@q **** (4) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::operator+=(Sinewave s)'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>
@
@<Define |Sinewave| functions@>=

   vector<Point*>::iterator iter = points.begin();
   vector<Point*>::iterator s_iter = s.points.begin();

   int n = min(points.size(), s.points.size());

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "points.size()   == " << points.size() << endl
            << "s.points.size() == " << s.points.size() << endl
            << "n               == " << n << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;   

@q **** (4) @>
@
@<Define |Sinewave| functions@>=

   for (int i = 0; i < n; ++i)
   {
@q ***** (5) @>

#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "Before addition:" << endl;

           (**iter).show("**iter:");
           (**s_iter).show("**s_iter:");
       }   
#endif /* |DEBUG_COMPILE|  */@;

       (**iter).set_y((**iter).get_y() + (**s_iter).get_y());

@q ***** (5) @>

#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "After addition:" << endl;
           (**iter).show("**iter:");

       }   
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

       ++iter;
       ++s_iter;

   }  /* |for|  */

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::operator+=(Sinewave s)' successfully." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   

}  /* End of |Sinewave::operator+=| definition  */

@q ** (2) Subtracting one sinewave from another with assignment (|operator-=|)@>
@ Subtracting one sinewave from another with assignment (|operator-=|)@>
\initials{LDF 2023.08.20.}

\LOG
\initials{LDF 2023.08.20.}
Added this function.
\ENDLOG

@q *** (3) Declaration @>

@<Declare |Sinewave| functions@>=
void
operator-=(Sinewave &s);

@q *** (3) Definition @>
@
@<Define |Sinewave| functions@>=
void
Sinewave::operator-=(Sinewave &s)
{
@q **** (4) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::operator-=(Sinewave s)'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>
@
@<Define |Sinewave| functions@>=

@q **** (4) @>
@
@<Define |Sinewave| functions@>=

   vector<Point*>::iterator iter = points.begin();
   vector<Point*>::iterator s_iter = s.points.begin();

   int n = min(points.size(), s.points.size());

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "points.size()   == " << points.size() << endl
            << "s.points.size() == " << s.points.size() << endl
            << "n               == " << n << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;   

@q **** (4) @>
@
@<Define |Sinewave| functions@>=

   for (int i = 0; i < n; ++i)
   {
@q ***** (5) @>

#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "Before subtraction:" << endl;

           (**iter).show("**iter:");
           (**s_iter).show("**s_iter:");
       }   
#endif /* |DEBUG_COMPILE|  */@;

       (**iter).set_y((**iter).get_y() - (**s_iter).get_y());

@q ***** (5) @>

#if DEBUG_COMPILE
       if (DEBUG)
       { 
           cerr << "After subtraction:" << endl;
           (**iter).show("**iter:");

       }   
#endif /* |DEBUG_COMPILE|  */@;

@q ***** (5) @>

       ++iter;
       ++s_iter;

   }  /* |for|  */


@q **** (4) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::operator-=(Sinewave s)' successfully." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return;

}  /* End of |Sinewave::operator-=| definition  */



@q ** (2) Get rectangle. @>
@ Get rectangle. @>
\initials{LDF 2023.08.15.}

\LOG
\initials{LDF 2023.08.15.}
Added this function.
\ENDLOG

@q *** (3) Declaration @>

@<Declare |Sinewave| functions@>=
int
get_rectangle(Rectangle *r, Transform &t, Scanner_Node scanner_node);

@q *** (3) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::get_rectangle(Rectangle *r, Transform &t, Scanner_Node scanner_node)
{
@q **** (4) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::get_rectangle'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

   if (r == 0)
   {
      cerr << "ERROR!  In `Sinewave::get_rectangle':" << endl
           << "`Rectangle *r' is NULL.  Can't create rectangle." << endl 
           << "Memory for `r' must be allocated before this function is called."
           << endl 
           << "Exiting function unsuccessfully with return value 1." << endl;

      return 1;

   }
   else if (period == 0 || period == INVALID_REAL || amplitude == 0 || amplitude == INVALID_REAL)
   {
      cerr << "WARNING!  In `Sinewave::get_rectangle':" << endl
           << "`period' and/or `amplitude' is 0 or `INVALID_REAL'." << endl 
           << "Can't create rectangle." << endl 
           << "Exiting function unsuccessfully with return value 1." << endl;

      return 1;
   }

@q **** (4) @>   
@
@<Define |Sinewave| functions@>=

   Point A, B, C, D, E;

   A.set(0, -amplitude, 0);
   B.set(get_last_point().get_x(), -amplitude, 0);
   C.set(get_last_point().get_x(), amplitude, 0);
   D.set(0, amplitude, 0);

   r->set(A, B, C, D);

   if (!t.is_identity())
   {
#if 0  
      cerr << "t is not identity transform.  Transforming *r." << endl;
#endif 

      *r *= t;
   
      /* Shifting |r| doesn't seem to be necessary if |t| just represents
         a rotation, but it may be if it represents a scaling or there
         is a scaling component in the transformation.
         !! TODO:  Test this!
         \initials{LDF 2023.08.16.}  */

      E = r->get_center();
      r->shift(center-E);

   }
#if 0 
   else
      cerr << "t is identity transform.  Not transforming *r." << endl;
#endif 

#if 0 
   cerr << "transform.is_identity() == " << transform.is_identity() << endl;
#endif 

   if (!transform.is_identity())
   {
#if 0 
      transform.show("transform:");

      cerr << "Transforming *r by `transform'." << endl;
#endif 


      *r *= transform;

      E = r->get_center();
      r->shift(center-E);

   }
#if 0 
   else
      cerr << "Not transforming *r by `transform'." << endl;
#endif 

@q **** (4) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::get_rectangle' successfully with return value 0." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return 0;

}  /* End of |Sinewave::get_rectangle| definition  */

@q * (1) @>

@q ** (2) Declaration @>
@
@<Declare |Sinewave| functions@>=
Transform
operator*=(const Transform& t);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
Transform
Sinewave::operator*=(const Transform &t)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::operator*=(const Transform &t)'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   center    *= t;
   transform *= t;
  
   Path::operator*=(t);

#if 0 
t.show("t:");
transform.show("transform:");
#endif 

@q *** (3) @>

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::operator*=(const Transform &t)' successfully "
            << "with return value `Transform &t'." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return t;

}  /* End of |Sinewave::operator*=(const Transform &t)| definition  */

@q * (1) @>

@q ** (2) Declaration @>
@
@<Declare |Sinewave| functions@>=
int
clip(real level, int clipping_type, real threshold, Scanner_Node scanner_node);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::clip(real level, int clipping_type, real threshold, Scanner_Node scanner_node)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::clip'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "level          == " << level << endl
            << "clipping_type  == " << clipping_type 
            << endl  
            << "threshold      == " << threshold
            << endl
            << "amplitude      == " << amplitude
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   if (level < 0)
   {
       cerr << "In `Sinewave::clip':  `level' is less than 0:" << endl 
            << "level == " << level << endl
            << "Changing sign of `level'." << endl;

      level *= -1; 

   }

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   if (clipping_type == 0)
   {
      cerr << "In `Sinewave::clip':  No clipping type specified." << endl
           << "Setting to hard clipping." << endl;

     clipping_type = 1;
   }
   else if (clipping_type == 1)
   {
      cerr << "Hard clipping specified." << endl;
   }
   else if (clipping_type == 2)
   {
#if 0 
      cerr << "Soft clipping specified." << endl;
#endif 
   }
   else if (clipping_type == 3)
   {
#if 0 
      cerr << "Clipping by percentage specified." << endl;       
#endif 
   }
   else
   {
      cerr << "WARNING!  In `Sinewave::clip':  Invalid clipping type specified:" 
           << endl
           << "`clipping_type' == " << clipping_type << endl 
           << "Setting `clipping_type' = 1 (hard clipping) and continuing."
           << endl;

      clipping_type = 1;
   }

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "amplitude == " << amplitude << endl
           << "level == " << level << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (clipping_type != 3 && level >= amplitude)
   {
       cerr << "WARNING!  In `Sinewave::clip':" << endl
            << "Clipping type is not \"percentage\" and "
            << "the `level' argument is less than or equal to `amplitude'." 
            << endl 
            << "level     == " << level << endl
            << "amplitude == " << amplitude << endl
            << "Can't clip sine wave." << endl
            << "Exiting function unsuccessfully with return value 2."
            << endl;

       return 2;
   }

@q *** (3) @>

@ !! TODO:  It might make sense to allow soft clipping together with clipping by
a percentage.  In that case, I'd have to use a bit mask for the options and 
make it possible to have multiple options.
\initials{LDF 2023.08.20.}

@<Define |Sinewave| functions@>=

   if (clipping_type != 2 && threshold != 0.0)
   {
      cerr << "WARNING!  In `Sinewave::clip':  Clipping type is not 2 (\"soft\") and "
           << "`threshold' == " << threshold << " (!= 0)"
           << endl
           << "`threshold' is only relevant for soft clipping."
           << endl 
           << "Setting `threshold' to 0.0 and continuing."
           << endl;

      threshold = 0.0;

   }

@q *** (3) @>

@ Only hard clipping is implemented as of this date.
\initials{LDF 2023.08.20.}

@<Define |Sinewave| functions@>=

   for (vector<Point*>::iterator iter = points.begin();
        iter != points.end();
        ++iter)
   {
@q **** (4) @>

       if ((**iter).get_y() >= 0 && (**iter).get_y() > level)
       {
           (**iter).set_y(level); 
       }

       else if ((**iter).get_y() < 0 && (**iter).get_y() < -level)
       {
           (**iter).set_y(-level); 
       }

@q **** (4) @>

   } /* |for|  */

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::clip' successfully with return value 0."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return 0;

}  /* End of |Sinewave::clip| definition  */

@q ** (2) Declaration @>
@
@<Declare |Sinewave| functions@>=
int
perturb(Pointer_Vector<real> *pv, Scanner_Node scanner_node);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::perturb(Pointer_Vector<real> *pv, Scanner_Node scanner_node)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::perturb'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   real curr_x = 0.0;
   real curr_y = 0.0;

   real low_value_x_positive  = .125;
   real high_value_x_positive = .75;
   int step_positive        = 5;    

   real low_value_y_positive  = .125; 
   real high_value_y_positive = .75;  

   real low_value_x_negative  = 0;
   real high_value_x_negative = 0;
   int step_negative        = 0;

   real low_value_y_negative  = 0;
   real high_value_y_negative = 0;


@q *** (3) @>
@
@<Define |Sinewave| functions@>=
  
#if DEBUG_COMPILE
   if (DEBUG)
   { 
       for (vector<real*>::iterator iter = pv->v.begin();
            iter != pv->v.end();
            ++iter)
       {
           cerr << "**iter == " << **iter << endl;
       }
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=


   if (pv->v.size() > 0)
      low_value_x_positive = low_value_y_positive = *(pv->v[0]);

   if (pv->v.size() > 1)
      high_value_x_positive = high_value_y_positive = *(pv->v[1]);

   if (pv->v.size() > 2)
      step_positive = static_cast<int>(*(pv->v[2]));

   if (pv->v.size() > 3)
      low_value_y_negative = *(pv->v[3]);

   if (pv->v.size() > 4)
      high_value_y_negative = *(pv->v[4]);

   if (pv->v.size() > 5)
      low_value_x_negative = low_value_y_negative = *(pv->v[5]);

   if (pv->v.size() > 6)
      high_value_x_negative = high_value_y_negative = *(pv->v[6]);

   if (pv->v.size() > 7)
      step_negative = static_cast<int>(*(pv->v[7]));

   if (pv->v.size() > 8)
      low_value_y_negative = *(pv->v[8]);

   if (pv->v.size() > 9)
      high_value_y_negative = *(pv->v[9]);

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
    cerr << "low_value_x_positive  == " << low_value_x_positive  << endl
         << "high_value_x_positive == " << high_value_x_positive << endl 
         << "step_positive       == " << step_positive       << endl 
         << "low_value_y_positive  == " << low_value_y_positive  << endl 
         << "high_value_y_positive == " << high_value_y_positive << endl 
         << "low_value_x_negative  == " << low_value_x_negative  << endl 
         << "high_value_x_negative == " << high_value_x_negative << endl 
         << "step_negative       == " << step_negative       << endl 
         << "low_value_y_negative  == " << low_value_y_negative  << endl 
         << "high_value_y_negative == " << high_value_y_negative << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 
 
@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   if (low_value_x_negative == 0)
      low_value_x_negative = low_value_x_positive;

   if (high_value_x_negative == 0)
      high_value_x_negative = high_value_x_positive;
   
   if (step_negative == 0)
      step_negative = step_positive;

   if (low_value_y_negative == 0)
      low_value_y_negative = low_value_y_positive;

   if (high_value_y_negative == 0)
      high_value_y_negative = high_value_y_positive;
   
#if DEBUG_COMPILE
   if (DEBUG)
   { 
    cerr << "After conditionals:"                                << endl 
         << "low_value_x_positive  == " << low_value_x_positive  << endl
         << "high_value_x_positive == " << high_value_x_positive << endl 
         << "step_positive         == " << step_positive         << endl 
         << "low_value_y_positive  == " << low_value_y_positive  << endl 
         << "high_value_y_positive == " << high_value_y_positive << endl 
         << "low_value_x_negative  == " << low_value_x_negative  << endl 
         << "high_value_x_negative == " << high_value_x_negative << endl 
         << "step_negative         == " << step_negative         << endl 
         << "low_value_y_negative  == " << low_value_y_negative  << endl 
         << "high_value_y_negative == " << high_value_y_negative << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 


@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   int i = 1;

   real temp_val = 0.0;

   int status = 0;

   for (vector<Point*>::iterator iter = points.begin();
        iter != points.end();
        ++iter, ++i)
   {
@q **** (4) @>

#if DEBUG_COMPILE
    if (DEBUG)
       { 
           cerr << "i == " << i << endl;
       }     
#endif /* |DEBUG_COMPILE|  */@; 
         
       curr_x = (**iter).get_x();
       curr_y = (**iter).get_y();


#if 0 
       cerr << "curr_x == " << curr_x << endl
            << "curr_y == " << curr_y << endl;
#endif 


@q **** (4) @>

       if (curr_y >= 0)  /* Positive case  */
       {
@q ***** (5) @>
#if 0

           cerr << "Positive case." << endl;

           cerr << "step_positive == " << step_positive << endl
                << "i == " << i << endl;

           cerr << "(i % step_positive) == " << (i % step_positive) << endl;
#endif 

           if (!(i % step_positive))
           {

@q ****** (6) @>

#if 0 
   arg.s to get_random_number:

   real range_low_value = 0.0,
   real range_high_value = 0.0,
   real modulus_value = 0.0,
   int  modulus_shift_value = 0,
   int  rounding_type_value = 0,
   int  with_negative_values_value = 0;

   Variables in this function:

   low_value_x_positive, high_value_x_positive
   step_positive        
   low_value_y_positive, high_value_y_positive
   low_value_x_negative 
   high_value_x_negative
   step_negative        
   low_value_y_negative 
   high_value_y_negative
#endif 

              status = get_random_number(&temp_val, scanner_node, 
                                         low_value_x_positive, 
                                         high_value_x_positive,
                                         0);

#if 0 
              cerr << "temp_val for x-coord == " << temp_val << endl;
#endif 

              curr_x += temp_val;

              (**iter).set_x(curr_x);

              status = get_random_number(&temp_val, scanner_node, 
                                         low_value_y_positive, 
                                         high_value_y_positive,
                                         0);

#if 0 
cerr << "temp_val for y-coord == " << temp_val << endl;
cerr << "low_value_y_positive == " << low_value_y_positive << endl;
cerr << "high_value_y_positive == " << high_value_y_positive << endl;
#endif 

              curr_y += temp_val;

              (**iter).set_y(curr_y);



           }

@q ***** (5) @>

       } /* |if|  */
      
@q **** (4) @>

       else /* Negative case  */
       {
@q ***** (5) @>

#if 0 
           cerr << "Negative case." << endl;

           cerr << "step_negative == " << step_negative << endl
                << "i == " << i << endl;

           cerr << "(i % step_negative) == " << (i % step_negative) << endl;
#endif 

           if (!(i % step_negative))
           {
@q ****** (6) @>

#if 0 
   arg.s to get_random_number:

   real range_low_value = 0.0,
   real range_high_value = 0.0,
   real modulus_value = 0.0,
   int  modulus_shift_value = 0,
   int  rounding_type_value = 0,
   int  with_negative_values_value = 0;

   Variables in this function:

   low_value_x_positive, high_value_x_positive
   step_positive        
   low_value_y_positive, high_value_y_positive
   low_value_x_negative 
   high_value_x_negative
   step_negative        
   low_value_y_negative 
   high_value_y_negative
#endif 

              status = get_random_number(&temp_val, scanner_node, 
                                         low_value_x_negative, 
                                         high_value_x_negative,
                                         0);

#if 0 
              cerr << "temp_val for x-coord == " << temp_val << endl;
#endif 

              curr_x += temp_val;

              (**iter).set_x(curr_x);

              status = get_random_number(&temp_val, scanner_node, 
                                         low_value_y_negative, 
                                         high_value_y_negative,
                                         0);

#if 0 
cerr << "temp_val for y-coord == " << temp_val << endl;
cerr << "low_value_y_negative == " << low_value_y_negative << endl;
cerr << "high_value_y_negative == " << high_value_y_negative << endl;
#endif 


              curr_y += temp_val;

              (**iter).set_y(curr_y);

@q ****** (6) @>

           }

@q ***** (5) @>


       } /* |else|  */

@q **** (4) @>

   } /* |for|  */

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::perturb' successfully with return value 0."
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   return 0;

}  /* End of |Sinewave::perturb| definition  */


@q * (1) Get zero points (|get_zero_points|).  @>

@ Get zero points (|get_zero_points|).  
\initials{LDF 2023.09.03.}

\LOG
\initials{LDF 2023.09.03.}
Added this function.
\ENDLOG

@q ** (2) Declaration @>

@<Declare |Sinewave| functions@>=
int
get_zero_points(Pointer_Vector<Point> *pv, 
                Pointer_Vector<real> *rv = 0,
                Scanner_Node scanner_node = 0);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::get_zero_points(Pointer_Vector<Point> *pv, 
                          Pointer_Vector<real> *rv,
                          Scanner_Node scanner_node)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = false; /* |true| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::get_zero_points'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 


@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "zero_points.size() == " << zero_points.size() << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

   if (zero_points.size() > 0)
   {
#if 0 
       cerr << "zero_points:" << endl;
#endif 

       for (vector<pair<int, Point> >::iterator iter = zero_points.begin();
            iter != zero_points.end();
            ++iter)
       {
#if 0 
          cerr << "(*iter).first          == " << (*iter).first << endl
               << "(*iter).second.get_y() == " << (*iter).second.get_y() << endl;
#endif 

          if (rv)
             *rv += static_cast<real>(iter->first);

          *pv += iter->second;
       }
   }

#if 0 
   cerr << "rv->v.size() == " << rv->v.size() << endl;
   cerr << "*rv:" << endl;
#endif 

#if 0 
   for (vector<real*>::iterator iter = rv->v.begin();
        iter != rv->v.end();
        ++iter)
   {
       cerr << "**iter == " << **iter << endl;
   }
#endif 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::get_zero_points' successfully with return value 0." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  return 0;

} /* End of |Sinewave::get_zero_points| definition  */

@q * (1) Get enclosing paths (|get_enclosing_paths|).  @>

@ Get enclosing paths (|get_enclosing_paths|).  
\initials{LDF 2023.09.03.}

\LOG
\initials{LDF 2023.09.03.}
Added this function.
\ENDLOG

@q ** (2) Declaration @>

@<Declare |Sinewave| functions@>=
int
get_enclosing_paths(Pointer_Vector<Path> *qv, 
                    Pointer_Vector<pair<int, Point> >*pv = 0,
                    Scanner_Node scanner_node = 0);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::get_enclosing_paths(Pointer_Vector<Path> *qv,
                              Pointer_Vector<pair<int, Point> >*pv, 
                              Scanner_Node scanner_node)
{
@q *** (3) @>

#if DEBUG_COMPILE
   bool DEBUG = true; /* |false| */ 
   if (DEBUG)
   { 
       cerr << "Entering `Sinewave::get_enclosing_paths'." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

   if (enclosing_paths.size() > 0)
   {
       cerr << "enclosing_paths:" << endl;

       for (vector<Path>::iterator iter = enclosing_paths.begin();
            iter != enclosing_paths.end();
            ++iter)
       {
            (*iter).show("*iter:");

            *qv += *iter;
       }
   }

@q *** (3) @>
@
@<Define |Sinewave| functions@>=

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "Exiting `Sinewave::get_enclosing_paths' successfully with return value 0." 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

  return 0;

} /* End of |Sinewave::get_enclosing_paths| definition  */

@q * (1) @>
@

@q ** (2) Declaration @>

@<Declare |Sinewave| functions@>=
int
get_indices(Pointer_Vector<real> *rv, Scanner_Node scanner_node = 0);

@q ** (2) Definition @>
@
@<Define |Sinewave| functions@>=
int
Sinewave::get_indices(Pointer_Vector<real> *rv, Scanner_Node scanner_node)
{

   for (vector<pair<int, Point> >::iterator iter = zero_points.begin();
        iter != zero_points.end();
        ++iter)
   {
      *rv += iter->first;
   }

   return 0;

}

@q * (1) Putting Sinewave together.@>
@* Putting {\bf Sinewave} together.
\initials{LDF 2023.08.14.}

This is what's compiled.
\initials{LDF 2023.08.14.}

@c
@<Include files@>@;
@<Define |class Sinewave|@>@;
@<Define |Sinewave| functions@>@;

@ This is what's written to \filename{sinewaves.h}.
\initials{LDF 2023.04.25.}

@(sinewaves.h@>=
@<Define |class Sinewave|@>@;

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q * Local variables for Emacs.@>

@q Local Variables: @>
@q mode:CWEB @>
@q eval:(display-time) @>
@q eval:(read-abbrev-file) @>
@q indent-tabs-mode:nil @>
@q eval:(outline-minor-mode) @>
@q End: @>
