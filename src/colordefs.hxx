/* colordefs.hxx */
/* Created by Laurence D. Finston (LDF) Sa 2. Dez 14:24:37 CET 2023  */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


/* Values for the following colors don't need to be assigned to strings because
   the names are already defined by ImageMagick:
   
   red
   blue
   cyan
   magenta
   yellow
   orange

   LDF 2024.01.25.

*/

/* These colors are predefined by ImageMagick, but it is convenient to define them
   anyway, so that the corresponding variables can be used in the same way as
   the ones for the other colors, e.g., in loops.
   LDF 2024.01.31.  */

string white               = "'rgb(255, 255, 255)'";
string black               = "'rgb(0, 0, 0)'";

string red                 = "'rgb(255, 0, 0)'";
string blue                = "'rgb(0, 0, 255)'";

string cyan                = "'rgb(0, 255, 255)'";
string magenta             = "'rgb(255, 0, 255)'";
string yellow              = "'rgb(255, 255, 0)'";

string orange              = "'rgb(255, 165, 0)'";

string red_orange_neon     = "'rgb(255, 35, 0)'";

/* `green' (0, 255, 0) is apparently not "understood" by ImageMagick.
   LDF 2024.01.26. */

string green               = "'rgb(0, 255, 0)'";   

string orange_dvips        = "'rgb(255, 99, 33)'";
string brown               = "'rgb(102, 0, 0)'";
string pink                = "'rgb(255, 192, 203)'"; 
string purple              = "'rgb(160, 32, 240)'";
string violet              = "'rgb(238, 130, 238)'";


string apricot             = "'rgb(255, 174, 122)'";
string aquamarine          = "'rgb(46, 255, 179)'";
string bittersweet         = "'rgb(194, 2, 0)'";
string blue_violet         = "'rgb(25, 12, 245)'";
string blue_green          = "'rgb(38, 255, 171)'";
string brick_red           = "'rgb(184, 0, 0)'";
string burnt_orange        = "'rgb(255, 125, 0)'";
string cadet_blue          = "'rgb(97, 110, 197)'";
string carnation_pink      = "'rgb(255, 94, 255)'";
string cerulean_blue       = "'rgb(0, 128, 255)'";
string cerulean            = cerulean_blue;
string cerulean_dvips      = "'rgb(15, 227, 255)'";
string cornflower_blue     = "'rgb(89, 222, 255)'";
string dandelion           = "'rgb(255, 181, 40)'";
string dark_blue           = "'rgb(0, 0, 128)'";
string dark_green          = "'rgb(0, 100, 0)'";
string dark_olive_green    = "'rgb(64, 64, 0)'";
string dark_orchid         = "'rgb(153, 51, 204)'";
string emerald             = "'rgb(0, 255, 128)'";
string forest_green        = "'rgb(0, 225, 0)'";
string fuchsia             = "'rgb(115, 3, 235)'";
string goldenrod           = "'rgb(255, 230, 40)'";
string green_yellow        = "'rgb(217, 255, 79)'"; 
string jungle_green        = "'rgb(2, 255, 122)'";
string lavender            = "'rgb(255, 133, 255)'";
string lime_green          = "'rgb(128, 255, 0)'";
string mahogany            = "'rgb(166, 0, 0)'";
string maroon              = "'rgb(174, 0, 0)'";
string mauve               = "'rgb(128, 0, 128)'";
string melon               = "'rgb(255, 138, 128)'";
string midnight_blue       = "'rgb(0, 112, 145)'";
string mulberry            = "'rgb(163, 20, 250)'";
string navy_blue           = "'rgb(15, 117, 255)'";
string olive_green         = "'rgb(0, 153, 0)'";
string orange_red          = "'rgb(255, 69, 0)'";
string orange_red_dvips    = "'rgb(255, 0, 128)'";
string orchid              = "'rgb(173, 92, 255)'";
string peach               = "'rgb(255, 128, 76)'"; 
string periwinkle          = "'rgb(110, 115, 255)'";
string pine_green          = "'rgb(0, 191, 41)'";
string plum                = "'rgb(128, 0, 255)'";
string process_blue        = "'rgb(10, 255, 255)'";
string purple_dvips        = "'rgb(140, 36, 255)'";
string raw_sienna          = "'rgb(140, 0, 0)'";
string red_orange          = "'rgb(255, 58, 33)'";
string red_violet          = "'rgb(150, 0, 168)'";
string rhodamine           = "'rgb(255, 46, 255)'";
string rose_madder         = "'rgb(255, 0, 128)'";
string royal_blue          = "'rgb(0, 128, 255)'";
string royal_purple        = "'rgb(64, 26, 255)'";
string rubine_red          = "'rgb(255, 0, 222)'";
string salmon              = "'rgb(255, 120, 158)'";
string sea_green           = "'rgb(79, 255, 128)'";
string sepia               = "'rgb(77, 0, 0)'";
string sky_blue            = "'rgb(97, 255, 225)'";
string spring_green        = "'rgb(189, 255, 61)'";
string tan_dvips           = "'rgb(219, 148, 112)'";  /* Suffix `_dvips' is needed because
                                                         `tan' is the name of a C library 
                                                         function.  LDF 2024.01.26.  */
string teal_blue           = "'rgb(0, 128, 128)'";
string teal_blue_dvips     = "'rgb(31, 250, 163)'";
string thistle             = "'rgb(225, 104, 255)'";
string turquoise           = "'rgb(0, 255, 128)'";
string turquoise_dvips     = "'rgb(38, 255, 204)'";
string violet_dvips        = "'rgb(54, 31, 255)'";
string violet_red          = "'rgb(208, 32, 144)'";
string violet_red_dvips    = "'rgb(255, 48, 255)'";
string wild_strawberry     = "'rgb(255, 10, 156)'";
string yellow_green        = "'rgb(191, 191, 0)'";
string yellow_green_dvips  = "'rgb(143, 255, 66)'";
string yellow_orange       = "'rgb(255, 148, 0)'";

string light_gray          = "'rgb(211, 211, 211)'";
string gray                = "'rgb(192, 192, 192)'";
string medium_dark_gray    = "'rgb(150, 150, 150)'";
string dark_gray           = "'rgb(100, 100, 100)'";
string Dark_gray           = "'rgb(50, 50, 50)'";
string DARK_gray           = "'rgb(30, 30, 30)'";

/* * (1) */

vector<pair<string, string> > color_vector;

/* The arguments `include_value' and `exclude_value' are bit masks.
   LDF 2024.01.26.
*/

int
initialize_color_vector(unsigned int include_value = 0U, unsigned int exclude_value = 0U)
{
   /* black and white are normally not included in `color_vector'.  LDF 2024.01.26.  */


   /* The primary RGB and CMY colors are normally not included in `color_vector'.  LDF 2024.01.26.  */

   if (include_value & 1U)
   {

      color_vector.push_back(make_pair("red", "red"));          

      color_vector.push_back(make_pair("green", green));  /* See above for explanation of why
                                                             a variable is needed for `green'.
                                                             LDF 2024.01.26. */
      color_vector.push_back(make_pair("blue", "blue"));                

      color_vector.push_back(make_pair("cyan", "cyan"));                
      color_vector.push_back(make_pair("magenta", "magenta"));             
      color_vector.push_back(make_pair("yellow", "yellow"));              
   }

   if (include_value & 2U)
   {
      color_vector.push_back(make_pair("black", "black"));          
      color_vector.push_back(make_pair("white", "white"));          
   }


   /* orange, orange_dvips, purple and purple_dvips are normally included in `color_vector'.
      LDF 2024.01.26.
   */

   if (exclude_value & 1U)   
   {
      color_vector.push_back(make_pair("orange", "orange"));              
      color_vector.push_back(make_pair("orange_dvips", orange_dvips));        
      color_vector.push_back(make_pair("red_orange_neon", red_orange_neon));        
      color_vector.push_back(make_pair("purple", purple));                    
      color_vector.push_back(make_pair("purple_dvips", purple_dvips));        
      color_vector.push_back(make_pair("violet", violet));              
   }

   /* pink and brown are normally included in `color_vector'.
      LDF 2024.01.26.
   */

   if (exclude_value & 2U)   
   {
      color_vector.push_back(make_pair("brown", brown));               
      color_vector.push_back(make_pair("pink", pink));                
   }
   
   color_vector.push_back(make_pair("apricot", apricot));             
   color_vector.push_back(make_pair("aquamarine", aquamarine));          

   color_vector.push_back(make_pair("bittersweet", bittersweet));         
   color_vector.push_back(make_pair("blue_violet", blue_violet));         
   color_vector.push_back(make_pair("blue_green", blue_green));          

   color_vector.push_back(make_pair("brick_red", brick_red));           
   color_vector.push_back(make_pair("burnt_orange", burnt_orange));        

   color_vector.push_back(make_pair("cadet_blue", cadet_blue));          
   color_vector.push_back(make_pair("carnation_pink", carnation_pink));      
   color_vector.push_back(make_pair("cerulean_blue", cerulean_blue));       
   color_vector.push_back(make_pair("cerulean_dvips", cerulean_dvips));      
   color_vector.push_back(make_pair("cornflower_blue", cornflower_blue));     

   color_vector.push_back(make_pair("dandelion", dandelion));           
   color_vector.push_back(make_pair("dark_blue", dark_blue));           
   color_vector.push_back(make_pair("dark_green", dark_green));          
   color_vector.push_back(make_pair("dark_olive_green", dark_olive_green));    
   color_vector.push_back(make_pair("dark_orchid", dark_orchid));         

   color_vector.push_back(make_pair("emerald", emerald));             

   color_vector.push_back(make_pair("forest_green", forest_green));        
   color_vector.push_back(make_pair("fuchsia", fuchsia));             

   color_vector.push_back(make_pair("goldenrod", goldenrod));           
   color_vector.push_back(make_pair("green_yellow", green_yellow));        
   color_vector.push_back(make_pair("jungle_green", jungle_green));        
   color_vector.push_back(make_pair("lavender", lavender));            
   color_vector.push_back(make_pair("lime_green", lime_green));          

   color_vector.push_back(make_pair("mahogany", mahogany));            
   color_vector.push_back(make_pair("maroon", maroon));              
   color_vector.push_back(make_pair("mauve", mauve));               
   color_vector.push_back(make_pair("melon", melon));               
   color_vector.push_back(make_pair("midnight_blue", midnight_blue));       
   color_vector.push_back(make_pair("mulberry", mulberry));            

   color_vector.push_back(make_pair("navy_blue", navy_blue));           

   color_vector.push_back(make_pair("olive_green", olive_green));         
   color_vector.push_back(make_pair("orange_red", orange_red));          
   color_vector.push_back(make_pair("orange_red_dvips", orange_red_dvips));    
   color_vector.push_back(make_pair("orchid", orchid));              

   color_vector.push_back(make_pair("peach", peach));               
   color_vector.push_back(make_pair("periwinkle", periwinkle));          
   color_vector.push_back(make_pair("pine_green", pine_green));          
   color_vector.push_back(make_pair("plum", plum));                
   color_vector.push_back(make_pair("process_blue", process_blue));        
   color_vector.push_back(make_pair("purple_dvips", purple_dvips));        

   color_vector.push_back(make_pair("raw_sienna", raw_sienna));          
   color_vector.push_back(make_pair("red_orange", red_orange));          
   color_vector.push_back(make_pair("red_violet", red_violet));          
   color_vector.push_back(make_pair("rhodamine", rhodamine));           
   color_vector.push_back(make_pair("rose_madder", rose_madder));         
   color_vector.push_back(make_pair("royal_blue", royal_blue));          
   color_vector.push_back(make_pair("royal_purple", royal_purple));        
   color_vector.push_back(make_pair("rubine_red", rubine_red));          

   color_vector.push_back(make_pair("salmon", salmon));              
   color_vector.push_back(make_pair("sea_green", sea_green));           
   color_vector.push_back(make_pair("sepia", sepia));               
   color_vector.push_back(make_pair("sky_blue", sky_blue));            
   color_vector.push_back(make_pair("spring_green", spring_green));        

   color_vector.push_back(make_pair("tan_dvips", tan_dvips));           
   color_vector.push_back(make_pair("teal_blue", teal_blue));           
   color_vector.push_back(make_pair("teal_blue_dvips", teal_blue_dvips));      
   color_vector.push_back(make_pair("thistle", thistle));             
   color_vector.push_back(make_pair("turquoise", turquoise));           
   color_vector.push_back(make_pair("turquoise_dvips", turquoise_dvips));     

   color_vector.push_back(make_pair("violet_dvips", violet_dvips));        
   color_vector.push_back(make_pair("violet_red", violet_red));          
   color_vector.push_back(make_pair("violet_red_dvips", violet_red_dvips));    

   color_vector.push_back(make_pair("wild_strawberry", wild_strawberry));     

   color_vector.push_back(make_pair("yellow_green", yellow_green));        
   color_vector.push_back(make_pair("yellow_green_dvips", yellow_green_dvips));  
   color_vector.push_back(make_pair("yellow_orange", yellow_orange));       

   return 0;

}  /* End of `initialize_color_vector' definition   */


/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */
