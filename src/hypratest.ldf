%% hypratest.ldf
%% Created by Laurence D. Finston (LDF) Wed Dec  7 12:31:51 AM CET 2022

input "plainldf.lmc";

verbatim_metapost "prologues := 3;outputtemplate := \"%j%3c.eps\";";

numeric frame_wd, frame_ht;

frame_wd := 14;
frame_ht := 11;

rectangle frame;
frame := unit_rectangle scaled (frame_wd, 0, frame_ht);

focus f;

set f with_position (0, 15, -50) 
  with_direction (0, 15, 10) with_distance 70;

pickup pencircle scaled (.333mm, .333mm, .333mm);

pen big_pen;
big_pen := pencircle scaled (.5mm, .5mm, .5mm);

point p[];
numeric n[];
path q[];

picture v[];

%% ** (2) Fig. 0.  Parallel projection x-z

beginfig(0);

  draw frame shifted (0, 0, 0);
  output current_picture with_projection parallel_x_z;
  clear current_picture;

  hyperbola h[];
  set h0 with_major_axis_length 3 with_minor_axis_length 2;
  set h1 with_major_axis_length 4 with_minor_axis_length 1;


  n0 := (size h0) - 1;
  % message "n0:";
  % show n0;
  % pause;
  
  for i = 0 upto n0:
    p[i] := get_point (i) h0;
  endfor;

  dotlabel.top("$p_{0}$", p0);
  dotlabel.rt("$p_{29}$", p29);
  
  dotlabel.bot("$p_{57}$", p57);
  dotlabel.top("$p_{58}$", p58);

  dotlabel.lft("$p_{87}$", p87);
  
  dotlabel.bot("$p_{115}$", p115);
  
  draw h0;
  draw h1 with_color blue;
  
endfig with_projection parallel_x_z;
  
%% ** (2) End MP and End

end_mp;
end;


%% ** (2) End of 3DLDF code.

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%   	 GNU Emacs editor.  The local variable list is not evaluated when an 
%%   	 indirect buffer is visited, so it's necessary to evaluate the       
%%   	 following s-expression in order to use the facilities normally      
%%   	 accessed via the local variables list.                              
%%   	 \initials{LDF 2004.02.12}.                                          

%% (progn (metapost-mode) (outline-minor-mode t) (setq fill-column 80))    

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
