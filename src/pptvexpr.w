@q pptvexpr.w @>
@q Created by Laurence Finston Fr Nov  5 22:10:03 CET 2004  @>
     
@q * (0) Copyright and License.@>

@q This file is part of GNU 3DLDF, a package for three-dimensional drawing. @>
@q Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, @>
@q 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025 The Free Software Foundation, Inc. @>

@q GNU 3DLDF is free software; you can redistribute it and/or modify @>
@q it under the terms of the GNU General Public License as published by @>
@q the Free Software Foundation; either version 3 of the License, or @>
@q (at your option) any later version. @>

@q GNU 3DLDF is distributed in the hope that it will be useful, @>
@q but WITHOUT ANY WARRANTY; without even the implied warranty of @>
@q MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the @>
@q GNU General Public License for more details. @>

@q You should have received a copy of the GNU General Public License @>
@q along with GNU 3DLDF; if not, write to the Free Software @>
@q Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA @>

@q GNU 3DLDF is a GNU package.  @>
@q It is part of the GNU Project of the  @>
@q Free Software Foundation @>
@q and is published under the GNU General Public License. @>
@q See the website http://www.gnu.org @>
@q for more information.   @>
@q GNU 3DLDF is available for downloading from @>
@q http://www.gnu.org/software/3dldf/LDF.html. @>

@q (``@@'' stands for a single at-sign in the following paragraph.) @>

@q Please send bug reports to Laurence.Finston@@gmx.de @>
@q The mailing list help-3dldf@@gnu.org is available for people to @>
@q ask other users for help.  @>
@q The mailing list info-3dldf@@gnu.org is for sending @>
@q announcements to users. To subscribe to these mailing lists, send an @>
@q email with ``subscribe <email-address>'' as the subject.  @>

@q The author can be contacted at: @>

@q Laurence D. Finston                 @> 
@q c/o Free Software Foundation, Inc.  @>
@q 51 Franklin St, Fifth Floor         @> 
@q Boston, MA  02110-1301              @>
@q USA                                 @>

@q Laurence.Finston@@gmx.de (@@ stands for a single ``at'' sign.)@>

@q * (0) |point_vector| expressions.@>
@** {\bf point\_vector} expressions.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Created this file and wrote quite a few rules.  
\ENDLOG 

@q * (1) |point_vector| primary.  @>
@* \�point vector primary>.
\initials{LDF 2004.11.10.}
  
\LOG
\initials{LDF 2004.11.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> point_vector_primary@>@/

@q ** (2) point_vector_primary --> point_vector_variable.@>
@*1 \�point vector primary> $\longrightarrow$ 
\�point vector variable>.  
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=point_vector_primary: point_vector_variable@>@/ 
{

  Id_Map_Entry_Node entry = static_cast<Id_Map_Entry_Node>(@=$1@>);

@q **** (4) Error handling:  |entry == 0 || entry->object == 0|.@> 

@ Error handling:  |entry == 0 || entry->object == 0|.
\initials{LDF 2004.11.10.}

@<Define rules@>=

  if (entry == static_cast<Id_Map_Entry_Node>(0) || entry->object == static_cast<void*>(0))
    {

      @=$$@> = static_cast<void*>(0);

    } /* |if (entry == 0 || entry->object == 0)|  */

@q **** (4) |!(entry == 0 || entry->object == 0)|.@> 

@ |!(entry == 0 || entry->object == 0)|.
\initials{LDF 2004.11.10.}

@<Define rules@>=
  else /* |!(entry == 0 || entry->object == 0)|  */

     {

        typedef Pointer_Vector<Point> PV;

        PV* pv = new PV;

        *pv = *static_cast<PV*>(entry->object);

        @=$$@> = static_cast<void*>(pv);                    

     }  /* |else| (|!(entry == 0 || entry->object == 0)|)  */

};

@q ** (2) point_vector_primary --> LEFT_PARENTHESIS  @>
@q ** (2) point_vector_expression  RIGHT_PARENTHESIS.@>

@*1 \�point vector primary> $\longrightarrow$ \.{LEFT\_PARENTHESIS}
\�point vector expression> \.{RIGHT\_PARENTHESIS}.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: LEFT_PARENTHESIS@>@/ 
@=point_vector_expression RIGHT_PARENTHESIS@>@/ 
{
   
  @=$$@> = @=$1@>;

};

@q ** (2) @>
@
\LOG
\initials{LDF 2007.08.19.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=point_vector_primary:  GET_CONTROL_POINTS numeric_expression OF path_primary @>
@=call_metapost_option_list@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @; 
   if (DEBUG)
   {
      cerr_strm << thread_name 
                << "*** Parser: point_vector_primary:  GET_CONTROL_POINTS numeric_expression"
                << endl 
                << "OF path_primary call_metapost_option_list.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */@;

@q **** (4) @>

   Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

#if LDF_REAL_DOUBLE
   int index = fabs(round(@=$2@>));
#else
   int index = fabsf(roundf(@=$2@>));
#endif

   Path *q = static_cast<Path*>(@=$4@>);

   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;    

   Point *precontrol = create_new<Point>(0);
   Point *postcontrol = create_new<Point>(0);

   Point *x_axis_pt = 0;
   Point *origin_pt = 0;

@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



@q **** (4) @>

   status = q->get_control_points(index, precontrol, postcontrol, 0, origin_pt, x_axis_pt, 
                                  scanner_node, do_transform, 
                                  save, clear, suppress_mp_stdout);

   if (status != 0)
   {
      cerr_strm << thread_name 
                << "ERROR!  In parser, rule `point_vector_primary:  GET_CONTROL_POINTS "
                << "numeric_expression"
                << endl 
                << "OF path_primary call_metapost_option_list':"
                << endl 
                << "`Path::get_control_points' failed, returning " << status << "."
                << endl 
                << "Couldn't find `precontrol' and `postcontrol' points."
                << endl 
                << "Continuing.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str(""); 

      *precontrol = INVALID_POINT;
      *postcontrol = INVALID_POINT;

   }

@q **** (4) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr_strm << thread_name 
                << "In parser, rule `point_vector_primary:  GET_CONTROL_POINTS numeric_expression"
                << endl 
                << "OF path_primary call_metapost_option_list':"
                << endl 
                << "`Path::get_control_points' succeeded, returning 0.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str(""); 
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

   pv->v.push_back(precontrol);
   pv->v.push_back(postcontrol);
   pv->ctr = 2;

   delete q;
   q = 0;

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   @=$$@> = static_cast<void*>(pv);  

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

};

@q ** (2) with_paths_optional @>

@ \�with paths optional>.
\initials{LDF 2022.12.20.}

\LOG
\initials{LDF 2022.12.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_paths_optional@>

@q ** (2) with_paths_optional: /* Empty  */@>
@
@<Define rules@>=
@=with_paths_optional: /* Empty  */@>@/
{
   @=$$@> =  static_cast<void*>(0); 
}

@q ** (2) with_paths_optional: WITH_PATHS path_vector_variable@>
@
@<Define rules@>=
@=with_paths_optional: WITH_PATHS path_vector_variable@>@/
{
   
   @=$$@> = @=$2@>; 
}

@q ** (2) with_triangles_optional @>

@ \�with triangles optional>.
\initials{LDF 2022.12.20.}

\LOG
\initials{LDF 2022.12.20.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_triangles_optional@>

@q ** (2) with_triangles_optional: /* Empty  */@>
@
@<Define rules@>=
@=with_triangles_optional: /* Empty  */@>@/
{
   @=$$@> =  static_cast<void*>(0); 
}

@q ** (2) with_triangles_optional: WITH_TRIANGLES triangle_vector_variable@>
@
@<Define rules@>=
@=with_triangles_optional: WITH_TRIANGLES triangle_vector_variable@>@/
{
   
   @=$$@> = @=$2@>; 
}




@q *** (3) point_vector_primary: point_primary NEUSIS point_secondary COMMA point_secondary @>
@q *** (3) WITH_DISTANCE numeric_expression with_paths_optional                             @>

@ \�point vector primary> $\longrightarrow$ \�point primary> \.{NEUSIS} 
\�point secondary> \.{COMMA} \�point secondary> \.{WITH\_DISTANCE} 
\�numeric expression> \�with paths optional>.
\initials{LDF 2022.12.19.}

\LOG
\initials{LDF 2022.12.19.}
Added this rule.
\ENDLOG

@<Define rules@>= 
 
@=point_vector_primary: point_primary NEUSIS point_secondary COMMA point_secondary @>
@=WITH_DISTANCE numeric_expression with_paths_optional with_triangles_optional@>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: `point_vector_primary: NEUSIS point_secondary COMMA point_secondary"
                << endl 
                << "WITH_DISTANCE numeric_expression with_paths_optional with_triangles_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Point *p0 = static_cast<Point*>(@=$1@>);
    Point *p1 = static_cast<Point*>(@=$3@>);
    Point *p2 = static_cast<Point*>(@=$5@>);

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    status = p0->neusis(p1, p2, @=$7@>, pv, 0, 0, @=$8@>, @=$9@>);
 
    if (status != 0)
    {
       cerr_strm << "ERROR!  In parser, rule `point_vector_primary: NEUSIS point_secondary "
                 << endl 
                 << "COMMA point_secondary WITH_DISTANCE numeric_expression"
                 << endl 
                 << "with_paths_optional with_triangles_optional':" 
                 << endl
                 << "`Point::neusis' failed, returning " << status << "." << endl 
                 << "Failed to find `points' using the neusis construction." << endl 
                 << "Setting value of the `point_primary' (on the left-hand side) "
                 << "to `INVALID_POINT' and continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

    }

    delete p0;
    p0 = 0;     

    delete p1;
    p1 = 0;     

    delete p2;
    p2 = 0;     

    @=$$@> = static_cast<void*>(pv);
};

@q * (1) @>
@
@<Define rules@>=
@=point_vector_primary:  GET_ZERO_POINTS sinewave_expression with_indices_optional@>
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @;
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `GET_ZERO_POINTS sinewave_expression with_indices_optional'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

  Sinewave *s = static_cast<Sinewave*>(@=$2@>);

  Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

  entry = static_cast<Id_Map_Entry_Node>(@=$3@>);

  Pointer_Vector<real> rv;

  status = s->get_zero_points(pv, &rv, scanner_node);

  if (entry)
  {
     Pointer_Vector<real> *temp_rv = 0;

     if (entry->object)
     {
       temp_rv = static_cast<Pointer_Vector<real>*>(entry->object);
       temp_rv->clear();
     }
     else 
     {
        temp_rv = new Pointer_Vector<real>;
        entry->object = static_cast<void*>(temp_rv); 
     }

     for (vector<real*>::iterator iter = rv.v.begin();
          iter !=  rv.v.end();
          ++iter)
     {

         status = vector_type_plus_assign<real>(scanner_node,
                                                static_cast<Id_Map_Entry_Node>(entry), 
                                                NUMERIC_VECTOR,
                                                NUMERIC,
                                                *iter);
     }
  }

#if 0 
  cerr << "status == " << status << endl;
#endif 

  delete s;
  s = 0;

  @=$$@> = static_cast<void*>(pv); 

};

@q ** (2) with_indices_optional @>

@ \�with indices optional>.
\initials{LDF 2023.09.08.}

\LOG
\initials{LDF 2023.09.08.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> with_indices_optional@>

@q ** (2) with_indices_optional: /* Empty  */@>
@
@<Define rules@>=
@=with_indices_optional: /* Empty  */@>@/
{
   @=$$@> =  static_cast<void*>(0); 
}

@q ** (2) with_indices_optional: WITH_INDICES numeric_vector_variable@>
@
@<Define rules@>=
@=with_indices_optional: WITH_INDICES numeric_vector_variable@>@/
{
   
   @=$$@> = static_cast<void*>(@=$2@>); 
}

@q * (1) point_vector_primary: GET_VERTICES polyhedron_expression. @>

@ \�point vector primary> $\longrightarrow$ \.{GET\_VERTICES} \�polyhedron expression>. 
\initials{LDF 2023.12.11.}

\LOG
\initials{LDF 2023.12.11.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_VERTICES polyhedron_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `point_vector_primary:  GET_VERTICES "
                 << "polyhedron_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    Polyhedron *p = static_cast<Polyhedron*>(@=$2@>);

    status = p->get_vertices(pv, scanner_node);

#if 0 
    cerr << "In parser rule:  `Polyhedron::get_vertices' returned status == " << status 
         << endl;
#endif 

    delete p;

    p = 0;     

    @=$$@> = static_cast<void*>(pv); 

};

@q * (1) point_vector_primary: GET_VERTICES rectangle_expression. @>

@ \�point vector primary> $\longrightarrow$ \.{GET\_VERTICES} \�rectangle expression>. 
\initials{LDF 2024.12.29.}

\LOG
\initials{LDF 2024.12.29.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_VERTICES rectangle_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `point_vector_primary:  GET_VERTICES "
                 << "rectangle_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    Rectangle *r = static_cast<Rectangle*>(@=$2@>);

    status = r->get_vertices(pv, scanner_node);

#if 0 
    cerr << "In parser rule:  `Rectangle::get_vertices' returned status == " << status 
         << endl;
#endif 

    delete r;

    r = 0;     

    @=$$@> = static_cast<void*>(pv); 

};




@q * (1) point_vector_primary: GET_VERTICES cuboid_expression. @>

@ \�point vector primary> $\longrightarrow$ \.{GET\_VERTICES} \�cuboid expression>. 
\initials{LDF 2023.12.14.}

\LOG
\initials{LDF 2023.12.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_VERTICES cuboid_expression@>@/
{
  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                 << "*** Parser:  `point_vector_primary:  GET_VERTICES "
                 << "cuboid_expression'.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    Cuboid *c = static_cast<Cuboid*>(@=$2@>);

    status = c->get_vertices(pv, scanner_node);

#if DEBUG_COMPILE
   if (DEBUG)
   { 
       cerr << "In parser rule:  `point_vector_primary: GET_VERTICES cuboid_expression':"
            << "`Cuboid::get_vertices' returned status == " << status 
            << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

    delete c;

    c = 0;     

    @=$$@> = static_cast<void*>(pv); 

};

@q ** (2) point_vector_primary --> GET_NORMALS cuboid_expression.  @>

@*1 \�point vector primary> $\longrightarrow$ \.{GET\_NORMALS} \�cuboid expression> 
\�with scale value optional> \�with shift optional>.
\initials{LDF 2024.02.18.}

\LOG
\initials{LDF 2024.02.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_NORMALS cuboid_expression with_scale_value_optional with_shift_optional@>@/ 
{
   
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `point_vector_primary "
                << "--> GET_NORMALS cuboid_expression with_scale_value_optional with_shift_optional'.";
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Cuboid *c = static_cast<Cuboid*>(@=$2@>);

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    status = c->get_normals(pv, @=$3@>, @=$4@>, scanner_node);

#if DEBUG_COMPILE
  if (DEBUG)
    {
       cerr << "In parser, rule `point_vector_primary: GET_NORMALS cuboid_expression "
            << "with_scale_value_optional with_shift_optional':"
            << endl 
            << "`Cuboid::get_normals' returned `status' == " << status << "." 
            << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

    delete c;
    c = 0;

    @=$$@> = static_cast<void*>(pv);

};

@q ** (2) point_vector_primary --> GET_NORMALS polyhedron_variable with_scale_value_optional with_shift_optional@>@/ 

@*1 \�point vector primary> $\longrightarrow$ \.{GET\_NORMALS} \�polyhedron variable> \�with scale value optional> 
\�with shift optional>.
\initials{LDF 2024.05.25.}

\LOG
\initials{LDF 2024.05.25.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_NORMALS polyhedron_variable with_scale_value_optional with_shift_optional@>@/ 
{
   
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `point_vector_primary "
                << "--> GET_NORMALS polyhedron_variable with_scale_value_optional with_shift_optional'.";
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Polyhedron *P = 0; 

    Pointer_Vector<Point> *pv = 0; 
    
    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    if (entry && entry->object)
    {
        P = static_cast<Polyhedron*>(entry->object);     
        pv = new Pointer_Vector<Point>;
    }
    else
    {
        goto END_GET_NORMALS_POLYHEDRON;
    }

    status = P->Polyhedron::get_normals(pv, @=$3@>, @=$4@>, scanner_node);

#if DEBUG_COMPILE
  if (DEBUG)
    {
       cerr << "In parser, rule `point_vector_primary: GET_NORMALS polyhedron_variable "
            << "with_scale_value_optional with_shift_optional':"
            << endl 
            << "`Polyhedron::get_normals' returned `status' == " << status << "." 
            << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

    END_GET_NORMALS_POLYHEDRON:

    @=$$@> = static_cast<void*>(pv);
};


@q *** (3) @>
@
@<Type declarations for non-terminal symbols@>=
@=%type <int_value> with_shift_optional@>@/

@q **** (4) @>
@
@<Define rules@>=
@=with_shift_optional: /* Empty  */@>@/ 
{
   @=$$@> = 0;
 
};

@q **** (4) @>
@
@<Define rules@>=
@=with_shift_optional: WITH_SHIFT @>@/ 
{
   @=$$@> = 1;
 
};

@q **** (4) @>
@
@<Define rules@>=
@=with_shift_optional: WITH_NO_SHIFT @>@/ 
{
   @=$$@> = 0;
 
};

@q ** (2) point_vector_primary --> GET_CENTERS cuboid_expression.  @>

@*1 \�point vector primary> $\longrightarrow$ \.{GET_CENTERS} \�cuboid expression>.
\initials{LDF 2024.02.18.}

\LOG
\initials{LDF 2024.02.18.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_CENTERS cuboid_expression@>@/ 
{
   
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = false; /* |true| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `point_vector_primary "
                << "--> GET_CENTERS cuboid_expression'.";
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    Cuboid *c = static_cast<Cuboid*>(@=$2@>);

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    status = c->get_centers(pv, scanner_node);

#if DEBUG_COMPILE
  if (DEBUG)
    {
       cerr << "In parser rule `point_vector_primary "
            << "--> GET_CENTERS cuboid_expression':"
            << endl 
            << "`Cuboid::get_centers' returned `status' == " << status << "." 
            << endl;
    }
#endif /* |DEBUG_COMPILE|  */@;

    delete c;
    c = 0;

    @=$$@> = static_cast<void*>(pv);

};

@q ** (2) point_vector_primary --> GET_CENTERS polyhedron_variable.@>

@*1 \�regular polygon vector primary> $\longrightarrow$ \.{GET\_CENTERS}
\�polyhedron variable>
\initials{LDF 2024.04.28.}

\LOG
\initials{LDF 2024.04.28.}
Added this rule.
\ENDLOG

@<Define rules@>=

@=point_vector_primary: GET_CENTERS polyhedron_variable@>@/
{
   @<Common code for getting face centers@>@/
};

@=point_vector_primary: GET_FACE_CENTERS polyhedron_variable@>@/
{
   @<Common code for getting face centers@>@/
};


@q *** (3) @>
@
@<Common code for getting face centers@>=

@q *** (3) @>
   
  @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
  {
      cerr_strm << thread_name 
                << "*** Parser: `point_vector_primary: GET_FACE_CENTERS polyhedron_variable'.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
  }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

  entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

  Polyhedron *P = 0;

  if (entry && entry->object)
  {
@q **** (4) @>

#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "entry and entry->object are non-NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

@q **** (4) @>

      P = static_cast<Polyhedron*>(entry->object);

      Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

      status = P->get_face_centers(pv, scanner_node);


      @=$$@> = static_cast<void*>(pv);

@q **** (4) @>

  }

@q *** (3) @>
@
@<Common code for getting face centers@>=

  else
  {
@q **** (4) @>

     cerr << "WARNING!  In parser, rule `point_vector_primary:"
          << endl 
          << "GET_FACE_CENTERS polyhedron_variable':"
          << "entry and/or entry->object is NULL." << endl
          << "Cannot obtain face centers." << endl 
          << "Will try to continue"
          << endl;

     @=$$@> = static_cast<void*>(0);

@q **** (4) @>

  }

@q *** (3) @>

@q ** (2) point_vector_primary --> GET_MID_POINTS polyhedron_variable.  @>

@*1 \�point vector primary> $\longrightarrow$ \.{GET_MID_POINTS} \�polyhedron variable>.
\initials{LDF 2024.05.03.}

\LOG
\initials{LDF 2024.05.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_MID_POINTS cuboid_expression@>@/ 
{
   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `point_vector_primary "
                << "--> GET_MID_POINTS cuboid_expression'.";
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

    @=$$@> = 0;

};

@q ** (2) point_vector_primary --> GET_MID_POINTS polyhedron_variable.  @>

@*1 \�point vector primary> $\longrightarrow$ \.{GET_MID_POINTS} \�polyhedron variable>.
\initials{LDF 2024.05.03.}

\LOG
\initials{LDF 2024.05.03.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_primary: GET_MID_POINTS polyhedron_variable@>@/ 
{
@q *** (3) @>

   @<Common declarations for rules@>@;

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
       cerr_strm << thread_name 
                << "*** Parser:  `point_vector_primary "
                << "--> GET_MID_POINTS polyhedron_variable'.";
       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

    entry = static_cast<Id_Map_Entry_Node>(@=$2@>);

    Pointer_Vector<Point> *pv = 0;

    if (entry && entry->object)
    {
@q **** (4) @>

        Polyhedron *p = static_cast<Polyhedron*>(entry->object);

        if (p->shape_type == Shape::TETRAHEDRON_TYPE)
        {
@q ***** (5) @>

#if DEBUG_COMPILE
           if (DEBUG)
           { 
              cerr << "p->shape_type == Shape::TETRAHEDRON_TYPE" << endl;
           }       
#endif /* |DEBUG_COMPILE|  */@;  

           pv = new Pointer_Vector<Point>;

           status = static_cast<Tetrahedron*>(entry->object)->Tetrahedron::get_mid_points(pv, scanner_node);

           if (status != 0)
           {
               cerr << "ERROR!  In parser, rule `point_vector_primary: GET_MID_POINTS polyhedron_variable':"
                    << endl 
                    << "`Tetrahedron::get_mid_points' failed, returning " << status << "." << endl 
                    << "Failed to get mid-points.  Will try to continue." << endl;
           }

@q ***** (5) @>

#if DEBUG_COMPILE
           else if (DEBUG)
           { 
               cerr << "In parser, rule `point_vector_primary: GET_MID_POINTS polyhedron_variable':"
                    << endl 
                    << "`Tetrahedron::get_mid_points' succeeded, returning 0." << endl;
           }       
#endif /* |DEBUG_COMPILE|  */@; 

@q ***** (5) @>

        }
@q **** (4) @>

        else
        {
#if DEBUG_COMPILE
           if (DEBUG)
           { 
              cerr << "p->shape_type != Shape::TETRAHEDRON_TYPE" << endl
                   << "p->shape_type == " << p->shape_type << " " << type_name_map[p->shape_type] 
                   << endl;
           }       
#endif /* |DEBUG_COMPILE|  */@;  

        }

@q **** (4) @>

    }

@q *** (3) @>

    else
    {
       cerr << "WARNING!  In parser, rule `point_vector_primary: GET_MID_POINTS polyhedron_variable':"
            << endl 
            << "`polyhedron_variable' is NULL.  Can't get mid-points." << endl 
            << "Will try to continue." << endl;
    }

@q *** (3) @>

    @=$$@> = static_cast<void*>(pv);

};

@q ** (2) @>

@q ** (2) @>

@ \�point vector primary> $\longrightarrow$ \.{GET\_BISECTION\_POINTS} \�triangle expression>.
\�with test optional>.
\initials{LDF 2024.11.13.}

\LOG
\initials{LDF 2024.11.13.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=point_vector_primary: GET_BISECTION_POINTS triangle_expression with_test_optional@>@/
{
@q *** (3) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: point_vector_primary: GET_BISECTION_POINTS "
                << "triangle_expression with_test_optional.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

    Triangle *t = static_cast<Triangle*>(@=$2@>);

    bool do_test;

    if (@=$3@> == WITH_TEST)
       do_test = true;
    else
       do_test = false;

    Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

    status = t->get_bisectors(pv, 0, do_test, scanner_node);

@q *** (3) @>


    if (status != 0)
    {
      cerr << "ERROR!  In parser, rule `point_vector_primary: GET_BISECTION_POINTS "
           << "triangle_expression with_test_optional':"
           << endl 
           << "`Triangle::get_bisection_points' failed, returning " << status << "." 
           << endl 
           << "Failed to get bisection_points.  Will try to continue." << endl;
    }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
      cerr << "In parser, rule `point_vector_primary: GET_BISECTION_POINTS "
           << "triangle_expression with_test_optional':"
           << endl 
           << "`Triangle::get_bisection_points' succeeded, returning 0."
           << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

    delete t;
    t = 0;

    @=$$@> = static_cast<void*>(pv);

};

@q ** (2) @>

@ \�point vector primary> $\longrightarrow$ \.{GET\_FOCi} \�ellipse primary>.
\initials{LDF 2025.01.01.}

\LOG
\initials{LDF 2025.01.01.}
Added this rule.
\ENDLOG 

@<Define rules@>= 

@=point_vector_primary: GET_FOCI ellipse_primary@>@/
{
@q *** (3) @>

  @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
  DEBUG = true; /* |false| */ @; 
  if (DEBUG)
    {
      cerr_strm << thread_name 
                << "*** Parser: point_vector_primary: GET_FOCI ellipse_primary.";

      log_message(cerr_strm);
      cerr_message(cerr_strm);
      cerr_strm.str("");
    }
#endif /* |DEBUG_COMPILE|  */@;

@q *** (3) @>

   Ellipse *e = static_cast<Ellipse*>(@=$2@>);

   Point p0 = e->get_focus(0, scanner_node);
   Point p1 = e->get_focus(1, scanner_node);

   Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

   *pv += p0;
   *pv += p1;

@q *** (3) @>

    delete e;
    e = 0;

    @=$$@> = static_cast<void*>(pv);

};



@q * (1) point_vector secondary.@>
@* \�point vector secondary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> point_vector_secondary@>
  
@q ** (2) point_vector secondary --> point_vector_primary.@>
@*1 \�point vector secondary> $\longrightarrow$ 
\�point vector primary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_secondary: point_vector_primary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q ***** (5) point_vector_secondary --> point_secondary REFLECTED_OFF @>
@q ***** (5) path_expression WITH_DIRECTION point_expression.         @>

@*4 \�point vector secondary> $\longrightarrow$ \�point secondary> 
\.{REFLECTED\_OFF} \�path expression>
\.{WITH\_DIRECTION} \�point expression>.

\LOG
\initials{LDF 2004.12.04.}
Added this rule.

\initials{LDF 2004.12.06.}
Working on this rule.

\initials{LDF 2004.12.07.}
This rule seems to work now.

\initials{LDF 2004.12.09.}
Changed |point_secondary| on the left-hand side to 
|point_vector_secondary| and moved this rule from 
\filename{ppntexpr.w} to this file (\filename{pptvexpr.w}).

\initials{LDF 2005.10.24.}
Changed |path_like_expression| to |path_expression|.
Removed debugging code.
\ENDLOG

@q ****** (6) Definition.@> 

@<Define rules@>=
@=point_vector_secondary: point_secondary REFLECTED_OFF@>@/
@=path_expression WITH_DIRECTION point_expression@>@/ 
{

    Scanner_Node scanner_node = static_cast<Scanner_Node>(parameter);

    Pointer_Vector<Point>* pv = new Pointer_Vector<Point>;

    *pv += create_new<Point>(0, scanner_node);
    *pv += create_new<Point>(0, scanner_node);
    *pv += create_new<Point>(0, scanner_node);
    *pv += create_new<Point>(0, scanner_node);

@q ******* (7) Call |Scan_Parse::reflect_off_func|.@> 

@ Call |Scan_Parse::reflect_off_func|.
\initials{LDF 2004.12.06.}

\LOG
\initials{LDF 2004.12.08.}
No longer calling |Scan_Parse::reflect_off_func| in a |try| 
block and catching |bad_alloc|.  
|Point::reflect_off|, which |Scan_Parse::reflect_off_func<Point>| 
calls, no longer tries to allocate
memory for |reflection| if it's null, 
so it never throws |bad_alloc|, so 
|Scan_Parse::reflect_off_func<Point>| doesn't, either. 
\ENDLOG 

@<Define rules@>=

   int status = reflect_off_func<Point>(scanner_node,
                                        static_cast<Point*>(@=$1@>),
                                        static_cast<Path*>(@=$3@>),
                                        static_cast<Point*>(@=$5@>),
                                        pv);

@q ******** (8) Error handling:  |Scan_Parse::reflect_off_func<Point>|@> 
@q ******** (8) failed, returning a non-zero value.@> 

@ Error handling:  |Scan_Parse::reflect_off_func<Point>|
failed, returning a non-zero value.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   if (status != 0)
      {

          @=$$@> = static_cast<void*>(0);
       
      }  /* |if (status != 0)|  */

@q ******** (8) Success!  |status == 0|.@> 

@ Success!  |status == 0|.
\initials{LDF 2004.12.04.}

@<Define rules@>=

   else /* |status == 0|  */
      {

         @=$$@> = static_cast<void*>(pv);

      }   /* |else| (|status == 0|)  */

};

@q * (1) point_vector tertiary.  @>

@* \�point vector tertiary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=

@=%type <pointer_value> point_vector_tertiary@>

@q ** (2) point_vector tertiary --> point_vector_secondary.@>
@*1 \�point vector tertiary> $\longrightarrow$ 
\�point vector secondary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_tertiary: point_vector_secondary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q ** (2) point_vector_tertiary: ellipse_tertiary @> 
@q ** (2) INTERSECTION_POINTS plane_secondary.    @> 

@*1 \�point vector tertiary> $\longrightarrow$ 
\�ellipse tertiary> \.{INTERSECTION\_POINTS} \�plane secondary>.
\initials{LDF 2005.11.02.}

\LOG
\initials{LDF 2005.11.02.}
Added this rule.

\initials{LDF 2005.11.03.}
Changed code to use the version of |Ellipse::intersection_points| that
takes a |Path| argument.

\initials{LDF 2005.11.04.}
Changed |rectangle_secondary| to |plane_secondary|.

\initials{LDF 2005.11.04.}
Changed |bool_point_vector_tertiary| to |bool_point_vector_tertiary|.
\ENDLOG
 
@q ***** (5) Definition.@> 

@<Define rules@>= 
@=point_vector_tertiary: ellipse_tertiary INTERSECTION_POINTS @> 
@=plane_secondary@>@/
{
    @=$$@> = Scan_Parse::ellipse_like_plane_intersection_func<Ellipse>(
                               static_cast<Ellipse*>(@=$1@>), 
                               @=$3@>, 
                               parameter);
};

@q ** (2) point_vector_tertiary: polygon_tertiary @> 
@q ** (2) INTERSECTION_POINTS polygon_secondary.  @> 

@*1 \�point vector tertiary> $\longrightarrow$ 
\�polygon tertiary>
\.{INTERSECTION\_POINTS} \�polygon secondary>.
\initials{LDF 2004.11.10.}

\LOG
\initials{LDF 2004.11.10.}
Added this rule.

\initials{LDF 2005.10.24.}
Changed |polygon_like_tertiary| and |polygon_like_secondary| 
to |polygon_tertiary| and |polygon_secondary|, respectively.
Removed debugging code.

\initials{LDF 2005.10.28.}
Removed code.  Now calling |Scan_Parse::polygon_like_intersection_func|.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=point_vector_tertiary: polygon_tertiary INTERSECTION_POINTS @> 
@=polygon_secondary@>@/
{

   @=$$@> = Scan_Parse::polygon_like_intersection_func<Polygon, Polygon>
               (static_cast<Polygon*>(@=$1@>),
               static_cast<Polygon*>(@=$3@>),
               parameter);

};

@q ** (2) point_vector_tertiary: polygon_tertiary @> 
@q ** (2) INTERSECTION_POINTS reg_polygon_secondary.  @> 

@*1 \�point vector tertiary> $\longrightarrow$ 
\�polygon tertiary>
\.{INTERSECTION\_POINTS} \�regular polygon secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=point_vector_tertiary: polygon_tertiary INTERSECTION_POINTS @> 
@=reg_polygon_secondary@>@/
{

   @=$$@> = Scan_Parse::polygon_like_intersection_func<Polygon, Reg_Polygon>
               (static_cast<Polygon*>(@=$1@>),
               static_cast<Reg_Polygon*>(@=$3@>),
               parameter);

};

@q ** (2) point_vector_tertiary: reg_polygon_tertiary @> 
@q ** (2) INTERSECTION_POINTS polygon_secondary.  @> 

@*1 \�point vector tertiary> $\longrightarrow$ 
\�regular polygon tertiary>
\.{INTERSECTION\_POINTS} \�polygon secondary>.
\initials{LDF 2005.10.28.}

\LOG
\initials{LDF 2005.10.28.}
Added this rule.
\ENDLOG
 
@q *** (3) Definition.@> 

@<Define rules@>= 
@=point_vector_tertiary: reg_polygon_tertiary INTERSECTION_POINTS @> 
@=polygon_secondary@>@/
{

   @=$$@> = Scan_Parse::polygon_like_intersection_func<Reg_Polygon, Polygon>
               (static_cast<Reg_Polygon*>(@=$1@>),
               static_cast<Polygon*>(@=$3@>),
               parameter);

};

@q ***** (5) point_vector_tertiary:  path_tertiary INTERSECTIONPOINTS_METAPOST path_secondary @>
@q ***** (5) with_resolutions_optional call_metapost_option_list.                    @>

@ \�point vector tertiary> $\longrightarrow$ \path tertiary> \.{INTERSECTIONPOINTS\_METAPOST}
\�path secondary> \�with resolutions optional> \�call metapost option list>.
\initials{LDF 2022.05.14.}

\LOG
\initials{LDF 2022.05.14.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_tertiary:  path_tertiary INTERSECTIONPOINTS_METAPOST path_secondary  @>
@=with_resolutions_optional call_metapost_option_list@>@/ 
{
@q ****** (6) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = false; /* |true| */ @;
   if (DEBUG)
     {
       cerr_strm << thread_name 
                 << "*** Parser: `point_vector_tertiary:  path_tertiary INTERSECTIONPOINTS_METAPOST "
                 << "path_secondary with_resolutions_optional call_metapost_option_list'."
                 << endl;

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");
     }
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>

   Pointer_Vector<real> *nv = static_cast<Pointer_Vector<real>*>(@=$4@>);
   Pointer_Vector<Point> *pv = new Pointer_Vector<Point>;

   real res0 = 2;
   real res1 = 2;

   if (nv == 0)
   {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`with_resolutions_optional' is NULL." << endl;
      }     
#endif /* |DEBUG_COMPILE|  */@; 

      nv = new Pointer_Vector<real>;
   }
   else
   {
#if DEBUG_COMPILE
      if (DEBUG)
      { 
         cerr << "`with_resolutions_optional' is non-NULL." << endl
              << "`nv->v.size()' == " << nv->v.size() << endl;

      }    
#endif /* |DEBUG_COMPILE|  */@;

      if (nv->v.size() > 0)
      {
         res0 = *(nv->v[0]);
         if (nv->v.size() > 1)
            res1 = *(nv->v[1]);

         nv->v.clear();
      }
   }

#if DEBUG_COMPILE
   if (DEBUG)
   { 
      cerr << "res0 == " << res0 << endl
           << "res1 == " << res1 << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@;

@q ****** (6) @>

   Path *p = static_cast<Path*>(@=$1@>);
   Path *q = static_cast<Path*>(@=$3@>);

   bool save               = @=$5@> & 1U;
   bool clear              = @=$5@> & 2U;
   bool suppress_mp_stdout = @=$5@> & 4U;
   bool do_transform       = @=$5@> & 8U;    
   Point *x_axis_pt = 0;
   Point *origin_pt = 0;


@q *** (3) @>

   if (@=$5@> & 16U)
   {
      x_axis_pt = scanner_node->x_axis_pt;
      scanner_node->x_axis_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_x_axis_pt' option present." << endl;
          if (x_axis_pt)
             cerr << "`x_axis_pt' is non-NULL." << endl;
          else
             cerr << "`x_axis_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_x_axis_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 

@q *** (3) @>

   if (@=$5@> & 32U)
   {
      origin_pt = scanner_node->origin_pt;
      scanner_node->origin_pt = 0;

#if DEBUG_COMPILE
      if (DEBUG)
      { 
          cerr << "`with_origin_pt' option present." << endl;
          if (origin_pt)
             cerr << "`origin_pt' is non-NULL." << endl;
          else
             cerr << "`origin_pt' is NULL." << endl;
      }  
#endif /* |DEBUG_COMPILE|  */@; 

   }

@q *** (3) @>

#if DEBUG_COMPILE
   else if (DEBUG)
   { 
          cerr << "`with_origin_pt' option not present." << endl;
   }  
#endif /* |DEBUG_COMPILE|  */@; 



   if (!p)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `point_vector_tertiary:  "
                 << "path_tertiary INTERSECTIONPOINTS_METAPOST"
                 << endl 
                 << "path_secondary with_resolutions_optional call_metapost_option_list':"
                 << endl
                 << "`path_tertiary' is NULL.  Can't find intersectionpoints.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }
   else if (!q)
   {
       cerr_strm << thread_name 
                 << "ERROR!  In parser, rule `point_vector_tertiary:  "
                 << "path_tertiary INTERSECTIONPOINTS_METAPOST"
                 << endl 
                 << "path_secondary with_resolutions_optional call_metapost_option_list':"
                 << endl
                 << "`path_secondary' is NULL.  Can't find intersectionpoints.  Continuing.";

       log_message(cerr_strm);
       cerr_message(cerr_strm);
       cerr_strm.str("");

   }

@q ****** (6) @>

   else /* (|(p && q)|) */
   {
@q ******* (7) @>

       status = p->get_metapost_intersectiontimes(q, nv, scanner_node, 0, origin_pt, x_axis_pt, 
                                                  do_transform, save, suppress_mp_stdout, true, pv);

@q ******* (7) @>

       if (status != 0)
       {
           cerr_strm << thread_name 
                     << "ERROR!  In parser, rule `point_vector_tertiary:  path_tertiary "
                     << "INTERSECTIONPOINTS_METAPOST"
                     << endl 
                     << "path_secondary with_resolutions_optional call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' failed, returning " << status << "."
                     << endl 
                     << "Failed to obtain intersectionpoints.  Continuing.";

           log_message(cerr_strm);
           cerr_message(cerr_strm, true);
           cerr_strm.str("");

       }  /* |if (status != 0)| */

@q ******* (7) @>

#if DEBUG_COMPILE
       else if (DEBUG)
       { 
           cerr_strm << thread_name 
                     << "In parser, rule `point_vector_tertiary:  path_tertiary INTERSECTIONPOINTS_METAPOST"
                     << endl 
                     << "path_secondary with_resolutions_optional call_metapost_option_list':"
                     << endl
                     << "`Path::get_metapost_intersectiontimes' succeeded, returning 0.";

           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");
       }  
#endif /* |DEBUG_COMPILE|  */@; 

@q ******* (7) @>

   }  /* |else| (|(p && q)|) */

@q ****** (6) @>

   if (p)
   {
      delete p;
      p = 0;
   }

   if (q)
   {
      delete q;
      q = 0;
   }

   if (scanner_node->metapost_output_struct)
   {
      delete scanner_node->metapost_output_struct;
      scanner_node->metapost_output_struct = 0;
   }

   if (nv)
   {
      delete nv;
      nv = 0;
   }

   if (x_axis_pt)
   {
      delete x_axis_pt;
      x_axis_pt = 0;
   }

   if (origin_pt)
   {
      delete origin_pt;
      origin_pt = 0;
   }

   @=$$@> = static_cast<Pointer_Vector<Point>*>(pv);

   if (scanner_node->tolerance)
   {
      delete scanner_node->tolerance;
      scanner_node->tolerance = 0;
   } 

};

@q * (1) point_vector expression.@>
@* \�point vector expression>.

\LOG
\initials{LDF 2004.11.10.}
Added this type declaration.
\ENDLOG

@<Type declarations for non-terminal symbols@>=
@=%type <pointer_value> point_vector_expression@>

@q ** (2) point_vector expression --> point_vector_tertiary.  @>
@*1 \�point vector expression> $\longrightarrow$ 
\�point vector tertiary>.

\LOG
\initials{LDF 2004.11.10.}
Added this rule.
\ENDLOG

@<Define rules@>=
@=point_vector_expression: point_vector_tertiary@>@/ 
{

  @=$$@> = @=$1@>;

};

@q * Emacs-Lisp code for use in indirect buffers when using the          @>
@q   GNU Emacs editor.  The local variable list is not evaluated when an @>
@q   indirect buffer is visited, so it's necessary to evaluate the       @>
@q   following s-expression in order to use the facilities normally      @>
@q   accessed via the local variables list.                              @>
@q   \initials{LDF 2004.02.12}.                                          @>
@q   (progn (cweb-mode) (outline-minor-mode t) (setq fill-column 80))    @>

@q Local Variables:                   @>
@q mode:CWEB                          @>
@q eval:(outline-minor-mode t)        @>
@q abbrev-file-name:"~/.abbrev_defs"  @>
@q eval:(read-abbrev-file)            @>
@q fill-column:80                     @>
@q run-cweave-on-file:"3DLDFprg.web"  @>
@q End:                               @>

