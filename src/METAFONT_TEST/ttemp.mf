%% ttemp.mf
%% Created by Laurence D. Finston (LDF) Mon 09 May 2022 09:43:26 AM CEST

%% * (1) Copyright and License.

%%%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%%%% Copyright (C) 2022, 2023 The Free Software Foundation, Inc.

%%%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%%%% it under the terms of the GNU General Public License as published by 
%%%% the Free Software Foundation; either version 3 of the License, or 
%%%% (at your option) any later version. 

%%%% GNU 3DLDF is distributed in the hope that it will be useful, 
%%%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%%%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%%%% GNU General Public License for more details. 

%%%% You should have received a copy of the GNU General Public License 
%%%% along with GNU 3DLDF; if not, write to the Free Software 
%%%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

%%%% GNU 3DLDF is a GNU package.  
%%%% It is part of the GNU Project of the  
%%%% Free Software Foundation 
%%%% and is published under the GNU General Public License. 
%%%% See the website http://www.gnu.org 
%%%% for more information.   
%%%% GNU 3DLDF is available for downloading from 
%%%% http://www.gnu.org/software/3dldf/LDF.html. 

%%%% Please send bug reports to Laurence.Finston@gmx.de
%%%% The mailing list help-3dldf@gnu.org is available for people to 
%%%% ask other users for help.  
%%%% The mailing list info-3dldf@gnu.org is for the maintainer of 
%%%% GNU 3DLDF to send announcements to users. 
%%%% To subscribe to these mailing lists, send an 
%%%% email with ``subscribe <email-address>'' as the subject.  

%%%% The author can be contacted at: 

%%%% Laurence D. Finston 
%%%% Kreuzbergring 41 
%%%% D-37075 Goettingen 
%%%% Germany 

%%%% Laurence.Finston@gmx.de 
 


%% (/ 4095 600.0) 6.825 ;; 6.825 inches == (* 6.825 2.54) 17.3355 cm



beginchar("A", char_w, char_h + char_d, 0);
   path p;
   p = (origin -- (w, 0) -- (w, h) -- (0, h) -- cycle) scaled .99;
   draw p  withpen pencircle scaled 5pt;
   for i = 0 upto 3:
      z[i] = point i of p;
   endfor;
   pair center;
   center = .5[z0, z2];
   drawdot center withpen pencircle scaled .25in;
   message "w:"; 
   show w;
   message "h:"; 
   show h;
   message "d:"; 
   show d;
   message "h+d:"; 
   show h+d;
endchar;

%% B

%% (/ 6.825 2) 3.4125 ;; (* 3.4125 2.54) 8.66775 cm

beginchar("B", char_w, char_h, char_d);
   path p[];
   p0 = (origin -- (w, 0) -- (w, h) -- (0, h) -- cycle) scaled .99;
   draw p0  withpen pencircle scaled 5pt;
   for i = 0 upto 3:
      z[i] = point i of p0;
   endfor;
   pair center;
   center = (.5w, 0);
   drawdot center withpen pencircle scaled .25in;

   p1 = (origin -- (0, -d) -- (w, -d) -- (w, 0) -- cycle) scaled .99;
   draw p1  withpen pencircle scaled 5pt;
   for i = 0 upto 3:
      z[4+i] = point i of p1;
   endfor;

   message "w:"; 
   show w;
   message "h:"; 
   show h;
   message "d:"; 
   show d;
   message "h+d:"; 
   show h+d;
endchar;



endinput;
