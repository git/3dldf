#### Makefile.am
#### Created by Laurence D. Finston Di 13. Aug 17:43:14 CEST 2024

#### * Copyright and License.

#### This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
#### Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

#### GNU 3DLDF is free software; you can redistribute it and/or modify 
#### it under the terms of the GNU General Public License as published by 
#### the Free Software Foundation; either version 3 of the License, or 
#### (at your option) any later version. 

#### GNU 3DLDF is distributed in the hope that it will be useful, 
#### but WITHOUT ANY WARRANTY; without even the implied warranty of 
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#### GNU General Public License for more details. 

#### You should have received a copy of the GNU General Public License 
#### along with GNU 3DLDF; if not, write to the Free Software 
#### Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

#### GNU 3DLDF is a GNU package.  
#### It is part of the GNU Project of the  
#### Free Software Foundation 
#### and is published under the GNU General Public License. 
#### See the website http://www.gnu.org 
#### for more information.   
#### GNU 3DLDF is available for downloading from 
#### http://www.gnu.org/software/3dldf/LDF.html. 

#### Please send bug reports to Laurence.Finston@@gmx.de
#### The mailing list help-3dldf@gnu.org is available for people to 
#### ask other users for help.  
#### The mailing list info-3dldf@gnu.org is for sending 
#### announcements to users. To subscribe to these mailing lists, send an 
#### email with "subscribe <email-address>" as the subject.  

#### The author can be contacted at: 

#### Laurence D. Finston 
#### c/o Free Software Foundation, Inc. 
#### 51 Franklin St, Fifth Floor 
#### Boston, MA  02110-1301  
#### USA

#### Laurence.Finston@gmx.de 


## * (1) Top.

# ** (2) Variables.

#### Automatic Variables.  For more, see Section 10.5.3, "Automatic Variables"
#### of the Make info manual.
#### LDF 2009.12.29.

#### $@:  Target
#### $<:  First prerequisite
#### $^:  The names of all the prerequisites, with spaces between them.
#### $*:  The stem with which an implicit rule matches

#### * (1)

#### ** (2)

#### * (1)

.PHONY: pf prog 

prog: process_files

pf: process_files
	./process_files

process_files: process_files.o
	g++ -o $@ $^

process_files.o: process_files.cxx
	g++ -o $@ -c $< 

.PHONY: mp eps png dvi pdf

mp: astronomy_4.mp

eps png: astronomy_4_0000.eps

dvi: astronomy_4.dvi

pdf: astronomy_4.pdf

astronomy_4.pdf: astronomy_4.dvi
	dvipdfmx $<

astronomy_4.ps: astronomy_4.dvi
	dvips -o $@ $<

astronomy_4.dvi: astronomy_4.tex astronomy_4_0000.eps star_labels_00.eps
	tex $<

astronomy_4_0000.eps: astronomy_4.mp
	mpost -numbersystem "double" $<

# f_dodecahedron.ldf

astronomy_4.mp: astronomy_4.ldf plainldf.lmc astronomy_macros.tex setup.ldf top_view.ldf \
                side_view.ldf persp.ldf earth.ldf sectors.ldf 
	3dldf$(EXEEXT) $<

.PHONY: emp eeps epng edvi epdf

eeps epng: ecliptic_0000.eps

edvi: ecliptic.dvi

epdf: ecliptic.pdf

ecliptic.pdf: ecliptic.dvi
	dvipdfmx $<

ecliptic.ps: ecliptic.dvi
	dvips -o $@ $<

ecliptic.dvi: ecliptic.tex ecliptic_0000.eps 
	tex $<

ecliptic_0000.eps: ecliptic.mp
	mpost -numbersystem "double" $<

emp: ecliptic.mp

ecliptic.mp: ecliptic.ldf astronomy_macros.tex setup.ldf sector_01_01.ldf sector_02_01.ldf sector_03_01.ldf \
             sector_03_02.ldf sector_04_02.ldf sector_05_02.ldf sector_06_02.ldf sector_07_02.ldf \
             sector_08_02.ldf sector_09_02.ldf sector_10_02.ldf \
             sectors_combined_00.ldf sectors_combined_01.ldf sectors_combined_02.ldf
	3dldf$(EXEEXT) $<


.PHONY: tmp teps tpng tdvi tpdf

tmp: titles.mp

teps tpng: titles_00.eps

tdvi: titles.dvi

tpdf: titles.pdf

titles.mp: titles.ldf
	3dldf$(EXEEXT) $<

titles_00.eps: titles.mp
	mpost -numbersystem="double" $<

titles.dvi: titles.tex titles_00.eps
	tex $<

titles.pdf: titles.dvi
	dvipdfmx $<


.PHONY: smp seps spng sdvi spdf

smp: star_labels.mp

seps spng: star_labels_00.eps

sdvi: star_labels.dvi

spdf: star_labels.pdf

star_labels.mp: star_labels.ldf
	3dldf$(EXEEXT) $<

star_labels_00.eps: star_labels.mp
	mpost -numbersystem="double" $<

star_labels.dvi: star_labels.tex star_labels_00.eps
	tex $<

star_labels.pdf: star_labels.dvi
	dvipdfmx $<





## Local Variables:
## mode:Makefile
## outline-minor-mode:t
## outline-regexp:"## *\\*+"
## abbrev-mode:t
## End:
