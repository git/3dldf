%% sector_13_m01.ldf
%% Created by Laurence D. Finston (LDF) Sa 24. Aug 18:48:45 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 13-1

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 13:--1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)



  
  p161 := p155 rotated (0, -15);

  p162 := p155 rotated (-15, 0);

  p163 := p162 rotated (0, -15);
  
  label.bot("{\bigmath $12^{\rm{h}}$}", p162 shifted (0, -.15));
  label.bot("{\bigmath $13^{\rm{h}}$}", p163 shifted (0, -.15));
  
  label.lft("$0^\circ$", p155);
  label.lft("$-15^\circ$", p162);

  label.rt("$0^\circ$", p161);
  label.rt("$-15^\circ$", p163);

  label.urt("13:$-$1", p162) with_color Melon_rgb;
  label.lrt("13:$-$1", p155 shifted (0, -.25)) with_color Melon_rgb;
  label.llft("13:$-$1", p161) with_color Melon_rgb;
  label.ulft("13:$-$1", p163) with_color Melon_rgb;
  

  if do_labels:
    dotlabel.top("$p_{161}$", p161) with_color red;
    dotlabel.llft("$p_{162}$", p162) with_color red;
    dotlabel.lrt("$p_{163}$", p163) with_color red;
    label.llft("$p_{155}$", p155) with_color red; % Autumnal equinox.  Dot drawn below.
                                                  % LDF 2024.08.24.
  fi;


%% ** (2)

    for i = 0 upto 3:
      temp_path[i] := null_path;
    endfor;
    
    temp_path0 += ..;
    temp_path1 += ..;
    temp_path2 += ..;
    temp_path3 += ..;
    
    for i = 0 step 1 until 15:
      temp_pt := p163 rotated (0, i);
      temp_path0 += temp_pt;

      temp_pt := p161 rotated (0, i);
      temp_path1 += temp_pt;

      if i == 8:
        p164 := temp_pt;
      fi;
      
      temp_pt := p70 rotated (i, -180);
      temp_path2 += temp_pt;

      temp_pt := p70 rotated (i, -195);
      temp_path3 += temp_pt;
    endfor;

    reverse temp_path1;
    reverse temp_path2;

    if false: % true: % 
      drawarrow temp_path0 with_pen big_pen with_color red;
      drawarrow temp_path1 with_pen big_pen with_color blue;
      drawarrow temp_path2 with_pen big_pen with_color green;
      drawarrow temp_path3 with_pen big_pen with_color orange;
    fi;
    
    q42 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
    q42 += cycle;

    
    draw q42 with_pen big_pen with_color Melon_rgb;

    sector_path[13][-1] := q42;
    
    if do_labels:
      label.top("$q_{42}$", get_point ((size q42) * 5 / 8) q42) with_text_color Melon_rgb;
    fi;
      
%% ** (2) First point on the ecliptic

    % Not needed because of autumnal equinox.
    
    % if do_labels:
    %   dotlabel.lft("$p_{151}$", p151) with_text_color dark_green;
    % fi;


    
%% ** (2) Second point on the ecliptic

  clear bpv;
  bpv := Ecliptic intersection_points c22;

  % show bpv;

  % message "p163:";
  % show p163;


  p168 := bpv0;

  if do_labels:
    dotlabel.rt("$p_{168}$", p168) with_text_color dark_green;
  fi;
    
    ang := p168 angle p155;

    q43 += ..;

    for i = 0 step ang/30 until ang:
      temp_pt := p155 rotated_around (p34, p36) -i;
      q43 += temp_pt;
    endfor;

    q43 += p168;
    
    draw q43 with_color dark_green with_pen Big_pen;

    if do_labels:
      n := (size q43) / 2;
      label.urt("$q_{43}$", get_point (n) q43) with_color dark_green;
    fi;


%% ** (2)  Porrima

    % https://en.wikipedia.org/wiki/Gamma_Virginis
    % Gamma Virginis
    % Right ascension       12h 41m 39.64344s[1]
    % Declination   –01° 26′ 57.7421″[1]
    % Apparent magnitude (V)        2.74 (3.650/3.560[2])
    % 122nd brightest star.
    
    p165 := p162 rotated (0, -(41/60 + 39.64344/3600) * 15);  % RA - hours
    p166 := p70 rotated (1 + 26/60 + 57.7421/3600, 180);  % Decl.


    if do_labels:
      dotlabel.bot("$p_{165}$", p165) with_color red;
      dotlabel.llft("$p_{166}$", p166) with_color red;
    fi;

    p167 := p166 rotated (0, -(41/60 + 39.64344/3600) * 15); % Rotate by RA - hours.

    %dotlabel.rt("$p_{167}$", p167) with_color red;


    Porrima := p167;

    drawdot Porrima with_pen huge_star_pen;

    % Right ascension       12h 41m 39.64344s[1]
    % Declination   –01° 26′ 57.7421″[1]
    
    label("\starnamed{Porrima}{\gamma}{Virginis}{+12}{41}{39.6434}{-01}{26}{57.7421}", 
        Porrima shifted (-.6667, -1.05, 0));
    
%% ** (2)

  sector_center[13][-1] := p70 rotated (7.5, 180-7.5);

  P14 := sector_center[13][-1];

  if do_labels:
    %dotlabel.bot("$P_{14}$", P14) with_color BurntOrange_rgb;
  fi;

%% ** (2)
  
  drawdot p155 with_pen pencircle scaled (1, 1, 1) with_color purple;
  label.lrt("Autumnal Equinox", p155 shifted (.45, -.06667)) rotate_text -2.5 with_color purple;

  rotate t0 (15, 15, 3);
  shift t0 (0, 10);

  
  sector_picture[13][-1] := current_picture;

  scale current_picture (100, 100, 100);

  current_picture *= t0;

  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
