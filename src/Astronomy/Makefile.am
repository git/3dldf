#### Makefile.am
#### Created by Laurence D. Finston Di 13. Aug 17:43:14 CEST 2024

#### * Copyright and License.

#### This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
#### Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

#### GNU 3DLDF is free software; you can redistribute it and/or modify 
#### it under the terms of the GNU General Public License as published by 
#### the Free Software Foundation; either version 3 of the License, or 
#### (at your option) any later version. 

#### GNU 3DLDF is distributed in the hope that it will be useful, 
#### but WITHOUT ANY WARRANTY; without even the implied warranty of 
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#### GNU General Public License for more details. 

#### You should have received a copy of the GNU General Public License 
#### along with GNU 3DLDF; if not, write to the Free Software 
#### Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

#### GNU 3DLDF is a GNU package.  
#### It is part of the GNU Project of the  
#### Free Software Foundation 
#### and is published under the GNU General Public License. 
#### See the website http://www.gnu.org 
#### for more information.   
#### GNU 3DLDF is available for downloading from 
#### http://www.gnu.org/software/3dldf/LDF.html. 

#### Please send bug reports to Laurence.Finston@@gmx.de
#### The mailing list help-3dldf@gnu.org is available for people to 
#### ask other users for help.  
#### The mailing list info-3dldf@gnu.org is for sending 
#### announcements to users. To subscribe to these mailing lists, send an 
#### email with "subscribe <email-address>" as the subject.  

#### The author can be contacted at: 

#### Laurence D. Finston 
#### c/o Free Software Foundation, Inc. 
#### 51 Franklin St, Fifth Floor 
#### Boston, MA  02110-1301  
#### USA

#### Laurence.Finston@gmx.de 


## * (1) Top.

# ** (2) Variables.

#### Automatic Variables.  For more, see Section 10.5.3, "Automatic Variables"
#### of the Make info manual.
#### LDF 2009.12.29.

#### $@:  Target
#### $<:  First prerequisite
#### $^:  The names of all the prerequisites, with spaces between them.
#### $*:  The stem with which an implicit rule matches

#### * (1)

#### ** (2)

.PHONY: pf prog 

prog: process_files

pf: process_files
	./process_files

process_files: process_files.o
	g++ -o $@ $^

process_files.o: process_files.cxx
	g++ -o $@ -c $< 

#### ** (2)

.PHONY: mp eps png dvi pdf

mp: angles.mp

eps png: angles_00.eps

dvi: angles.dvi

pdf: angles.pdf

angles.pdf: angles.dvi
	dvipdfmx $<

angles.ps: angles.dvi
	dvips -o $@ $<

angles.dvi: angles.tex angles_00.eps
	tex $<

angles_00.eps: angles.mp
	mpost -numbersystem "double" $<

angles.mp: angles.ldf plainldf.lmc ../3dldf$(EXEEXT)
	3dldf$(EXEEXT) --no-database $<

#### ** (2)

astronomy_6.pdf: astronomy_6.dvi
	dvipdfmx $<

astronomy_6.ps: astronomy_6.dvi
	dvips -o $@ $<

astronomy_6.dvi: astronomy_6.tex astronomy_6_0000.eps star_labels_00.eps
	tex $<

astronomy_6_0000.eps: astronomy_6.mp
	mpost -numbersystem "double" $<

astronomy_6.mp: astronomy_6.ldf plainldf.lmc astronomy_macros.tex setup.ldf top_view.ldf \
                side_view.ldf persp.ldf earth.ldf
	3dldf$(EXEEXT) --no-database $<


#### ** (2)

.PHONY: dmp deps dpng ddvi dpdf

dmp: dials_with_scales.mp

deps dpng: dials_with_scales_00.eps

ddvi: dials_with_scales.dvi

dpdf: dials_with_scales.pdf

dials_with_scales.pdf: dials_with_scales.dvi
	dvipdfmx $<

dials_with_scales.ps: dials_with_scales.dvi
	dvips -o $@ $<

dials_with_scales.dvi: dials_with_scales.tex dials_with_scales_00.eps star_labels_00.eps
	tex $<

dials_with_scales_00.eps: dials_with_scales.mp
	mpost -numbersystem "double" $<

dials_with_scales.mp: dials_with_scales.ldf plainldf.lmc astronomy_macros.tex astronomy.lmc
	3dldf$(EXEEXT) --no-database $<

#### ** (2)

.PHONY: tmp teps tpng tdvi tpdf

tmp: titles.mp

teps tpng: titles_00.eps

tdvi: titles.dvi

tpdf: titles.pdf

titles.mp: titles.ldf astronomy_macros.tex
	3dldf$(EXEEXT) --no-database $<

titles_00.eps: titles.mp
	mpost -numbersystem="double" $<

titles.dvi: titles.tex titles_00.eps
	tex $<

titles.pdf: titles.dvi
	dvipdfmx $<

#### ** (2)

.PHONY: smp seps spng sdvi spdf

smp: star_labels.mp

seps spng: star_labels_00.eps

sdvi: star_labels.dvi

spdf: star_labels.pdf

star_labels.mp: star_labels.ldf
	3dldf$(EXEEXT) --no-database $<

star_labels_00.eps: star_labels.mp
	mpost -numbersystem="double" $<

star_labels.dvi: star_labels.tex star_labels_00.eps
	tex $<

star_labels.pdf: star_labels.dvi
	dvipdfmx $<

#### ** (2)

.PHONY: tags

tags:
	etags *.ldf

.PHONY: purge

purge:
	rm -f *.dvi *.eps *.log *.mf *.mp *.png mpxerr*

#### * (1)

## Local Variables:
## mode:Makefile
## outline-minor-mode:t
## outline-regexp:"## *\\*+"
## abbrev-mode:t
## End:
