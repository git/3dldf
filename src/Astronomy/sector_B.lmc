%% sector_B.lmc
%% Created by Laurence D. Finston (LDF) Di 10. Dez 18:36:31 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)



macro sector_B;

% PV:  point_vector
% QV:  path_vector


def sector_B (PV, QV) {point start_pt, numeric start_val, numeric end_val, numeric increment_val,
                     numeric cutoff, numeric rradius_val, numeric mmod_val, numeric bborder_val,
                     numeric hhoriz_val, numeric vvert_offset, numeric ttab_val,
                     boolean ddo_labels, boolean ddo_help_lines,
                     boolean rreverse} =

%% ** (2)

  message "Entering `sector_B'.";
    
  point p[];
  path q[];
  string s;
  transform t[];
  point temp_pt;

  
  t1 := identity;

  if rreverse:
    rotate_around t1 (start_pt shifted (-1, 0), start_pt shifted (1, 0)) 180;
  fi;
  
  color dot_color;

  if ddo_labels or ddo_help_lines:
    dot_color := blue;
  else:
    dot_color := black;
  fi;
  
  numeric n, m, j, k, d, e;
  
  p0 := start_pt;

  PV += p0 * t1;
  
  range := end_val - start_val;
  
  n := (cosd start_val)*2pi*rradius_val / 24;

  p1 := (-.5n, 0);
  p2 := (.5n, 0);

  PV += p1 * t1;
  PV += p2 * t1;
  
  draw (p1 -- p2) * t1;
  QV += (p1 -- p2)  * t1;

  if ddo_labels:
    dotlabel.bot("$\scriptstyle p_0$", p0);
    dotlabel.lft("$\scriptstyle p_1$", p1);
    dotlabel.lrt("$\scriptstyle p_2$", p2);
  fi;

%% ** (2)

  m := (end_val - start_val) / 15;

  % message "m: " & decimal m;
  % pause;
  
  n := m*2pi*rradius_val / 24;
  
  p3 := p0 shifted (0, n);

  PV += p3 * t1;
  
  if ddo_help_lines:
    draw (p0 -- p3) * t1;
  fi;

  QV += (p0 -- p3) * t1;
  
  
  if ddo_labels:
    dotlabel.top("$\scriptstyle p_3$", p3);
  fi;

%% ** (2)
  
  j := 4;
  k := 0;

  q[k]   += ..;
  q[k+1] += ..;
  q[k]   += p1;
  q[k+1] += p2;

  
  for i = increment_val step increment_val until 1:

%% *** (3)
    
    m := start_val+(range*i);
    %message "m: " & decimal m;

    exit_if m > (start_val + cutoff);

    n := (cosd m)*2pi*rradius_val / 24;
    %message "n: " & decimal n & "\n";
    p[j] := mediate(p0, p3, i);
    p[j+1] := p[j] shifted (-.5n, 0);
    p[j+2] := p[j] shifted (.5n, 0);

    q[k]   += p[j+1];
    q[k+1] += p[j+2];
    
    % if ddo_labels:
    %   s := "$\scriptstyle p_{" & decimal j & "}$";
    %   dotlabel.lrt(s, p[j]);
    %   s := "$\scriptstyle p_{" & decimal (j+1) & "}$";
    %   dotlabel.lft(s, p[j+1]);
    %   s := "$\scriptstyle p_{" & decimal (j+2) & "}$";
    %   dotlabel.rt(s, p[j+2]);
    % fi;

    if ddo_help_lines:
      draw (p[j+1] -- p[j+2]) * t1; % Horizontal lines between the points on the edges.
    fi;

    QV += (p[j+1] -- p[j+2]) * t1;

    j+=3;
    
%% *** (3)

  endfor;

  numeric top_radius_segment_val;

  n := (cosd end_val)*2pi*rradius_val / 24;
  
  top_radius_segment_val := n;
  
  q[k]   += p3 shifted (-.5n, 0);
  q[k+1] += p3 shifted (.5n, 0);

  draw q[k] * t1;   % Left border
  draw q[k+1] * t1; % Right border

  draw (p3 shifted (-.5n, 0) -- p3 shifted (.5n, 0)) * t1; % Top border

  QV += q[k] * t1;
  QV += q[k+1] * t1;

  p[j]   := get_last_point q[k];
  p[j+1] := get_last_point q[k+1];
  p[j+2] := p[j] shifted (0, .5);
  p[j+3] := p[j+1] shifted (-0, .5);
  
  QV += (p[j] -- p[j+1] -- p[j+3] -- p[j+2] -- cycle) * t1;


  
%% ** (2)

  % Outline for top tab.
  
  if true: % false
    draw (p[j] -- p[j+1] -- p[j+3] -- p[j+2] -- cycle)  * t1 with_color blue;
  fi;

  
%% ** (2)
  
  if ddo_labels:
    s := "$\scriptstyle p_{" & decimal j & "}$";    
    dotlabel.ulft(s, p[j]);
    s := "$\scriptstyle p_{" & decimal (j+1) & "}$";
    dotlabel.urt(s, p[j+1]);
    s := "$\scriptstyle p_{" & decimal (j+2) & "}$";
    dotlabel.ulft(s, p[j+2]);
    s := "$\scriptstyle p_{" & decimal (j+3) & "}$";
    dotlabel.urt(s, p[j+3]);
  fi;

  for i = 0 upto 3:
    PV += p[j+i] * t1;
  endfor;

  j += 4;

  

  p[j]   := p1 shifted (0,  -ttab_val);
  p[j+1] := p2 shifted (-0, -ttab_val);
  
  QV += (p1 -- p2 -- p[j+1] -- p[j] -- cycle) * t1;

  % Outline for bottom tab.  Currently enabled.  LDF 2024.10.20.
  
  % message "path:";
  % show ((p1 -- p2 -- p[j+1] -- p[j] -- cycle));
  

  draw ((p1 -- p2 -- p[j+1] -- p[j] -- cycle))  * t1; %  with_color red; % Bottom tab

  if ddo_labels: % true:
    s := "$\scriptstyle p_{" & decimal j & "}$";    
    dotlabel.lrt(s, p[j]);
    s := "$\scriptstyle p_{" & decimal (j+1) & "}$";
    dotlabel.llft(s, p[j+1]);
  fi;

  for i = 0 upto 1:
    PV += p[j+i] * t1;
  endfor;

  j += 2;  
  k += 2;
  
%% ** (2) Draw horizontal lines at 15° intervals, if the sector extends vertically by more
%% ** (2) than 15°.

  n := ((end_val - start_val) / 15) - 1;

  if n > 0:
    for i = 1 upto n:
      p[j] := mediate(p0, p3, i/6);

      if ddo_labels:
	s := "$p_{" & decimal j & "}$";
	dotlabel.lrt(s, p[j]);
      fi;
      

      m := cosd (15i) * 2pi * radius_val / 24;

      %message "m: " & decimal m;

      p[j+1] := p[j] shifted (-.5m, 0);
      p[j+2] := p[j] shifted (.5m, 0);

      draw (p[j+1] -- p[j+2]) * t1; % with_color red; % Don't know what this is.  LDF 2024.11.01.
      QV += (p[j+1] -- p[j+2]) * t1;

      PV += p[j] * t1;
      PV += p[j+1] * t1;
      PV += p[j+2] * t1;

      j += 3;
    endfor;
  fi;
  
%% ** (2) Right border

  q[k] += ..;

  d := 0;
  for i = 0 step increment_val until 1.01:

%% *** (3)    
    
    m := start_val+(range*i);
    %message "m: " & decimal m;

    exit_if m > (start_val + cutoff);

%% *** (3)
    
    A := rradius_val+bborder_val;
    n := (cosd m)*2pi*A / 24;
    
    %message "n: " & decimal n & "\n";
    p[j] := mediate(p0, p3, i) shifted (.5n, 0);

    q[k] += p[j];

    j+=1;

%% *** (3)
    
    A := rradius_val+.5bborder_val;
    n := (cosd m)*2pi*A / 24;
    
    %message "n: " & decimal n & "\n";
    p[j] := mediate(p0, p3, i) shifted (.5n, 0);

    if (i > 0) and (i < 1) and ((d mod mmod_val) == 0):
      drawdot p[j] * t1 with_pen dot_pen with_color dot_color; % Vertical tab dots, right
    fi;

    j+=1;


%% *** (3)
    
    A := rradius_val-.5bborder_val;
    n := (cosd m)*2pi*A / 24;
    
    %message "n: " & decimal n & "\n";
    p[j] := mediate(p0, p3, i) shifted (-.5n, 0);

    if (i > 0) and (i < 1) and ((d mod mmod_val) == 0):
      drawdot p[j] * t1 with_pen dot_pen with_color dot_color; % Vertical tab dots, left
    fi;

    j+=1;

    d += 1;
    
%% *** (3)        

  endfor;

  draw (p2 -- get_point 0 q[k])  * t1; % with_color red; % Lower border of vertical tab

  QV += (p2 -- get_point 0 q[k])  * t1;

  draw (get_last_point q[k-1] -- get_last_point q[k]) * t1; % with_color green; % Top border of vertical tab

  QV += (get_last_point q[k-1] -- get_last_point q[k]) * t1;
  
  draw q[k] * t1; % with_color blue; % Right border of vertical tab

  QV += q[k] * t1;
  
  PV += (get_point 0 q[k])  * t1;
  PV += (get_last_point q[k])  * t1;
    
%% ** (2) Horizontal dots

%% *** (3)  Bottom

  if not ((start_val == 0) and (end_val == 90)):
    
    n := (xpart p2) - .25mm;

    t0 := identity rotated_around (p1, p2) 180;
    
    %drawdot (p0 shifted (0, vvert_offset))  * t1 with_pen dot_pen;      % Within field
    drawdot ((p0 shifted (0, vvert_offset)) * t0) * t1 with_pen dot_pen; % Within tab
    
    for i = hhoriz_val step hhoriz_val until n:
      % Within field
      % drawdot (p0 shifted (i, vvert_offset)) * t1 with_pen dot_pen with_color red;
      % drawdot (p0 shifted (-i, vvert_offset)) * t1 with_pen dot_pen with_color blue;

      % Within tab
      drawdot ((p0 shifted (i, vvert_offset))  * t0) * t1  with_pen dot_pen; %  with_color green;
      drawdot ((p0 shifted (-i, vvert_offset)) * t0) * t1 with_pen dot_pen;  %  with_color magenta;
    endfor;

%% *** (3)  Top

    % Top dots.  Tab currently disabled.
    
    if end_val < 90:
      n := (xpart PV5) - .25mm;

      t0 := identity rotated_around (p3 shifted (-1, 0), p3 shifted (1, 0)) 180;

       

      drawdot (p3 shifted (0, -vvert_offset)) * t1 with_pen dot_pen;

      %drawdot ((p3 shifted (0, -vvert_offset)) * t0) * t1 with_pen dot_pen;
      

      for i = hhoriz_val step hhoriz_val until n:

	exit_if i > (xpart (p3 shifted (.5top_radius_segment_val)));
	
	drawdot (p3 shifted (i, -vvert_offset)) * t1  with_pen dot_pen; % Right side
	drawdot (p3 shifted (-i, -vvert_offset)) * t1 with_pen dot_pen; % Left  side

	% drawdot ((p3 shifted (i, -vvert_offset)) * t0) * t1 with_pen dot_pen;
	% drawdot ((p3 shifted (-i, -vvert_offset)) * t0) * t1 with_pen dot_pen;
      endfor;
    fi;
  fi; 
  
%% *** (3)

%% ** (2)  

  message "Exiting `sector_B'.";
  
enddef;

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
