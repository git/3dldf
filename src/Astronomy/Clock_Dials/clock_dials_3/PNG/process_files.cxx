/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Mi 4. Sep 00:00:39 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024, 2025 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/



#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

// composite H000.png I020.png I021.png

// mogrify -gravity center -crop 1726x982+0+0 merry_go_round_3_" << zeroes << i << ".png";

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   string zeroes;

   for (int i = 1; i <= 901; i++)
   {
      s.str("");

      if (i < 10)
         zeroes = "0000";
      else if (i < 100)
         zeroes = "000";
      else if (i < 1000)
         zeroes = "00";
      else if (i < 10000)
         zeroes = "0";
      else
         zeroes = "";


      s << "convert -fill black -opaque blue "
        << "-opaque red -opaque cyan -opaque " << green << " "
        << "-opaque " << teal_blue << " -opaque " 
        << rose_madder << " "
        << " -opaque " << purple << " "
        << " -opaque " << salmon << " "
        << " -opaque " << periwinkle << " "
        << "clock_dials_" << zeroes << i << ".png " 
        << "A" << zeroes << i << ".png";




      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
  }

   
  return 0;

}

#if 0

   for (int i = 1; i <= 121; i++)

      s << "convert -fill black -opaque blue -opaque red -opaque " << green << " "
        << "-opaque cyan -opaque " << purple << " -opaque " << teal_blue << " -opaque " << rose_madder << " "
        << "-opaque red -opaque " << violet << " grandfathers_clock_" << zeroes << i << ".png "
        << "A" << zeroes << i << ".png";

   for (int i = 122; i <= 242; i++)

      s << "convert -fill red -opaque black grandfathers_clock_" << zeroes << i << ".png X.png;"
        << "convert -fill black -opaque white -opaque blue "
        << "-fill cyan -opaque " << green << " -opaque " << teal_blue << " -opaque " << rose_madder << " "
        << "-fill violet -opaque " << purple << " X.png " 
        << "B" << zeroes << i << ".png";


   for (int i = 243; i <= 361; i++)

      s << "convert -fill yellow -opaque black grandfathers_clock_" << zeroes << i << ".png X.png;"
        << "convert -fill black -opaque white -opaque blue "
        << "-fill white -opaque red -opaque yellow -opaque cyan -opaque " << green << " "
        << "-opaque " << teal_blue << " -opaque " 
        << rose_madder << " "
        << "-fill white -opaque " << purple << " X.png " 
        << "C" << zeroes << i << ".png";

   for (int i = 362; i <= 481; i++)

      s << "convert -fill yellow -opaque black grandfathers_clock_" << zeroes << i << ".png X.png;"
        << "convert -fill black -opaque white -opaque blue "
        << "-fill yellow -opaque red -fill magenta -opaque cyan -opaque " << green << " "
        << "-opaque " << teal_blue << " -opaque " 
        << rose_madder << " "
        << "-fill " << periwinkle << " -opaque " << purple << " X.png " 
        << "D" << zeroes << i << ".png";

   for (int i = 482; i <= 601; i++)

      s << "convert -fill orange -opaque black grandfathers_clock_" << zeroes << i << ".png X.png;"
        << "convert -fill black -opaque white -opaque blue "
        << "-fill orange -opaque red -opaque cyan -opaque " << green << " "
        << "-opaque " << teal_blue << " -opaque " 
        << rose_madder << " "
        << "-fill " << orange << " -opaque " << purple << " X.png " 
        << "E" << zeroes << i << ".png";


#endif


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


