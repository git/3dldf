%% sectors.ldf
%% Created by Laurence D. Finston (LDF) Sa 17. Aug 16:50:55 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

verbatim_tex "\magnification=4000";

input "setup_sectors.ldf";

if do_png:
  do_labels := false;

  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%4c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";";
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";";
fi;


medium_pen := pencircle scaled (.5mm, .5mm, .5mm);
big_pen := pencircle scaled (1.75mm, 1.75mm, 1.75mm);
Big_pen := pencircle scaled (3mm, 3mm, 3mm);
BIG_pen := pencircle scaled (1.75mm, 1.75mm, 1.75mm);
huge_pen := pencircle scaled (2mm, 2mm, 2mm);
dot_pen := pencircle scaled (4mm, 4mm, 4mm);



verbatim_metapost "dotlabeldiam:=6mm;labeloffset:=.5cm;ahlength:=1cm;";

medium_star_pen := pencircle scaled (.5, .5, .5);
big_star_pen := pencircle scaled (.75, .75, .75);
Big_star_pen := pencircle scaled (1, 1, 1);
huge_star_pen := pencircle scaled (1.25, 1.25, 1.25);
Huge_star_pen := pencircle scaled (1.5, 1.5, 1.5); % Maybe for planets.

picture sector_picture[][];

fig_ctr := 0;

%% * (1) Front view

beginfig(fig_ctr);

%% ** (2) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Front view.  Parallel projection x--y}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
%% ** (2)

  draw S0 with_pen big_pen;

  temp_circle := unit_circle_one_hundred_twenty_eight scaled (radius_val0, 0, radius_val0) rotated (90, 0);

  for i = 15 step 30 until 165:
    draw temp_circle rotated (0, i) with_pen big_pen;
  endfor;

  draw Ecliptic with_color dark_green with_pen BIG_pen;

  draw q0 with_color dark_green with_pen Big_pen; % Axis through poles of the ecliptic

  p70 := (0, 0, -radius_val0);
  p71 := p70 rotated (0, -15);
  p72 := p70 rotated (-15, 0);
  p73 := p70 rotated (-15, -15);

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;

  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;

  for i = 0 step 1 until 15:
    temp_pt := p70 rotated (0, -i);
    temp_path0 += temp_pt;

    temp_pt := p70 rotated (-i, 0);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-i, -15);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-15, -i);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;

  % drawarrow temp_path0 with_color green with_pen Big_pen;
  % drawarrow temp_path1 with_color blue with_pen Big_pen;
  % drawarrow temp_path2 with_color magenta with_pen Big_pen;
  % drawarrow temp_path3 with_color cyan with_pen Big_pen;

  
  q12 := temp_path0 -- temp_path2 -- temp_path3 -- temp_path1;
  q12 += cycle;

  %dotlabel.top("$q_{12}$", mediate(p72, p73)) with_color blue;

  %draw q12 with_color blue with_pen Big_pen; % sector 0:0.
  

  % This works:  q12 is a cycle.  LDF 2024.08.17.
  %filldraw q12 with_fill_color Goldenrod_rgb;

  
  if do_labels:
    dotlabel.lrt("{\smallrm p4}", p4) with_color red; % p4 == p70.

    %dotlabel.lrt("{\mediumit o}", origin);
    % dotlabel.rt("{\mediumit p}{\mediumrm 34}", p34) with_color dark_green;
    % dotlabel.rt("{\mediumit p}{\mediumrm 36}", p36) with_color dark_green;
    % label.lft("{\mediumit q}{\mediumrm 0}", p34) with_color dark_green;
    % label.lft("{\mediumit q}{\mediumrm 0}", p36) with_color dark_green;

    % dotlabel.lrt("{\smallrm p71}", p71) with_color red;
    % dotlabel.lrt("{\smallrm p72}", p72) with_color red;
    % dotlabel.lrt("{\smallrm p73}", p73) with_color red;
  fi;
  
%% ** (2) Find intersection points of the ecliptic with the circles of right ascension
%% ** (2) and create paths corresponding to the portions of the ecliptic in each sector.

% This works.  LDF 2024.08.18.
  
%% *** (3) RA circle 23h (orange)
  
  c8 := unit_circle_one_hundred_twenty_eight scaled (radius_val0, 0, radius_val0)
    rotated (90, 90-15);

  %draw c8 with_pen big_pen with_color orange;


  
  bool_point_vector bpv;

  bpv := Ecliptic intersection_points c8;

  p74 := bpv0;
  p75 := bpv1;

  %dotlabel.lft("{\smallrm p74}", p74) with_color magenta;
  %dotlabel.lft("{\smallrm p75}", p75) with_color magenta;

%% **** (4) Section of Ecliptic (red)
  
  ang := p74 angle p70;

  q13 += ..;
  
  for i = 0 step ang/30 until ang:
    temp_pt := p70 rotated_around (p34, p36) -i;
    q13 += temp_pt;
  endfor;

  q13 += p74;
  
  %draw q13 with_color red with_pen big_pen;
  
  %message "ang: " & decimal ang;

%% **** (4)  

%% *** (3) RA circle 22h (magenta)

  c9 := unit_circle_one_hundred_twenty_eight scaled (radius_val0, 0, radius_val0)
    rotated (90, 90-30);

  %draw c9 with_pen big_pen with_color magenta;
  
  clear bpv;

  bpv := Ecliptic intersection_points c9;

  p76 := bpv0;
  p77 := bpv1;

  %dotlabel.lft("{\smallrm p76}", p76) with_color blue;
  %dotlabel.lft("{\smallrm p77}", p77) with_color blue;

%% **** (4) Section of Ecliptic (green)
  
  ang := p76 angle p74;
  
  %message "ang: " & decimal ang;

  q14 += ..;
  
  for i = 0 step ang/30 until ang:
    temp_pt := p74 rotated_around (p34, p36) -i;
    q14 += temp_pt;
  endfor;

  q14 += p76;
  
  %draw q14 with_color green with_pen big_pen;

%% *** (3) RA circle 22h (magenta)

  c10 := unit_circle_one_hundred_twenty_eight scaled (radius_val0, 0, radius_val0)
    rotated (90, 90-45);

  %draw c10 with_pen big_pen with_color magenta;
  
  clear bpv;

  bpv := Ecliptic intersection_points c10;

  p90 := bpv0;
  p91 := bpv1;

  %dotlabel.lft("{\smallrm p90}", p90) with_color blue;
  %dotlabel.lft("{\smallrm p91}", p91) with_color blue;

  % message "p72:";
  % show p72;

%% *** (3)  
  
  c13 := c10 rotated (0, -15); % RA line 30°, 4h, 16h

  celestial_meridian4 := c13;

  %draw c13 with_pen big_pen with_color rose_madder;

  c14 := c13 rotated (0, -15); % RA line 5h, 17h

  celestial_meridian5  := c14;
  celestial_meridian17 := c14;

%% *** (3)
  
  c15 := c14 rotated (0, -15); % RA line 6h, 18h

  celestial_meridian6  := c15;
  celestial_meridian18 := c15;
  
  c16 := c15 rotated (0, -15); % RA line 7h, 19h

  celestial_meridian7 := c16;
  celestial_meridian19 := c16;
  
  c17 := c16 rotated (0, -15); % RA line 8h, 20h

  celestial_meridian8 := c17;
  celestial_meridian20 := c17;
  
  c18 := c17 rotated (0, -15); % RA line 9h, 21h

  celestial_meridian9 := c18;
  celestial_meridian21 := c18;
  
  c19 := c18 rotated (0, -15); % RA line 10h, 22h

  celestial_meridian10 := c19;
  celestial_meridian22 := c19;
  
  c20 := c19 rotated (0, -15); % RA line 11h, 23h

  celestial_meridian11 := c20;
  celestial_meridian23 := c20;
  
  c21 := c20 rotated (0, -15); % RA line 12h (0h).

  celestial_meridian12 := c21;
  celestial_meridian0 := c21;
  celestial_meridian24 := c21;
  
  c22 := c21 rotated (0, -15); % RA line 13h, 1h

  celestial_meridian1 := c22;
  celestial_meridian13 := c22;
  
  c23 := c22 rotated (0, -15); % RA line 14h, 2h

  celestial_meridian2 := c23;
  celestial_meridian14 := c23;
  
  c24 := c23 rotated (0, -15); % RA line 15h, 3h

  celestial_meridian3 := c24;
  celestial_meridian15 := c24;
  
%% *** (3)

  c11 := unit_circle scaled (zpart p72, 0, zpart p72) shifted (0, ypart p72);

  %draw c11 with_pen Big_pen with_color teal_blue_rgb;

  %label.rt("{\mediumrm c11}", (get_point 12 c11) shifted (.75, 0)) with_color teal_blue_rgb;

  clear bpv;

  bpv := Ecliptic intersection_points c11;

  p92 := bpv0;
  p93 := bpv1;

  %dotlabel.llft("{\smallrm p92}", p92) with_color blue;
  %dotlabel.llft("{\smallrm p93}", p93) with_color blue;
  
%% **** (4) Section of Ecliptic (green)
  
  ang := p92 angle p76;
  
  %message "ang: " & decimal ang;

  q16 += ..;
  
  for i = 0 step ang/30 until ang:
    temp_pt := p76 rotated_around (p34, p36) -i;
    q16 += temp_pt;
  endfor;

  q16 += p92;
  
  %draw q16 with_color orange with_pen Big_pen;

%% **** (4) Corner points of the sector path.

  p94 := p71 rotated (0, -15);

  %dotlabel.lrt("{\smallrm p94}", p94) with_color red;

  p95 := p71 rotated (0, -30);

  %dotlabel.lrt("{\smallrm p95}", p95) with_color red;

  p96 := p73 rotated (0, -15);

  %dotlabel.lrt("{\smallrm p96}", p96) with_color red;

  p97 := p73 rotated (0, -30);

  %dotlabel.lrt("{\smallrm p97}", p97) with_color red;

%% *** (3)
  
%% ** (2) Sector 3:2


  %draw decl_circle30 with_pen big_pen with_color orange;

  c12 := decl_circle30;

  if do_labels:
    %n := (size c12) * 3/4;
    %label.rt("{\mediumrm c12}", (get_point (n) c12) shifted (.25, .5)) with_color orange;
  fi;
  
  
  p104 := p70 rotated (-30, -30);

  %dotlabel.urt("$p_{104}$", p104) with_color red;

  p105 := p70 rotated (-30, -45);

  %dotlabel.ulft("$p_{105}$", p105) with_color red;

%% ** (2) celestial_meridian circles.

  % This works.  Can be used for dividing the celestial meridian circles.
  % LDF 2024.08.26.

  if false:
    draw celestial_meridian0 with_color blue with_pen big_pen;
    draw celestial_meridian1 with_color red with_pen big_pen;

    n := size celestial_meridian0;
    P0 := get_point 0 celestial_meridian0;
    P1 := get_point (n/4) celestial_meridian0;
    P2 := get_point (n/2) celestial_meridian0;
    P3 := get_point (3n/4) celestial_meridian0;

    if do_labels:
      dotlabel.top("$P_0$", P0) with_color red;
      dotlabel.top("$P_1$", P1) with_color red;
      dotlabel.top("$P_2$", P2) with_color red;
      dotlabel.top("$P_3$", P3) with_color red;
    fi;
  fi;

%% ** (2) Declination circles

  % This works.  LDF 2024.08.26.
  
  if false:
    draw decl_circle[-60] with_color red with_pen big_pen;
    draw decl_circle[-75] with_color blue with_pen big_pen;
  fi; 
  
%% ** (2)
  
  v0 := current_picture;
  
%% ** (2)  

endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Top View.

input "top_view.ldf";

%% * (1) Loop 1.  Perspective projection

input "persp.ldf";

%% * (1) Sectors

%% ** (2)

%% * (1)
  
input "sector_01_p01.ldf";
input "sector_02_p01.ldf";
input "sector_03_p01.ldf";
input "sector_03_p02.ldf";
input "sector_04_p02.ldf";
input "sector_05_p02.ldf";
input "sector_06_p02.ldf";
input "sector_07_p02.ldf";
input "sector_08_p02.ldf";
input "sector_09_p02.ldf";
input "sector_10_p02.ldf";
input "sector_10_p01.ldf";
input "sector_11_p01.ldf";
input "sector_12_p01.ldf";
input "sector_13_m01.ldf";
input "sector_14_m01.ldf";
input "sector_15_m01.ldf";
input "sector_15_m02.ldf";
input "sector_16_m02.ldf";
input "sector_17_m02.ldf";
input "sector_18_m02.ldf";
input "sector_19_m02.ldf";
input "sector_20_m02.ldf";
input "sector_21_m02.ldf";
input "sector_22_m02.ldf";
input "sector_22_m01.ldf";
input "sector_23_m01.ldf";
input "sector_24_m01.ldf";

input "sector_07_m02.ldf"; % Sirius
input "sector_07_m04.ldf"; % Canopus
input "sector_15_m05.ldf"; % Rigil_Kentaurus_A
input "sector_15_p02.ldf"; % Arcturus
input "sector_19_p03.ldf"; % Vega
  
%% ** (2)

%% * (1) Loop.  Entire ecliptic with all sectors (smaller)

if true: % false; % 
  input "sectors_combined_all.ldf";
fi;

%% * (1) Loop.  Horizontal rotation.  Sectors 1:1, 2:1 and 3:1.

if false: % true: % 
  input "sectors_combined_00.ldf";
fi;

%% * (1) Loop.  Vertical rotation.  Sectors  3:1 and 3:2.

if false: % true: %
  input "sectors_combined_01.ldf";
fi;

%% * (1) Loop.  Horizontal rotation.  Sectors  4:2--10:2

if false: % true: %
  input "sectors_combined_02.ldf";
fi;

%% * (1) Loop. Vertical rotation.  Sectors  10:2--10:1

if false: % true: %
  input "sectors_combined_03.ldf";
fi;




%% * (1)

message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
if not do_png:
  pause;
fi;

bye;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
