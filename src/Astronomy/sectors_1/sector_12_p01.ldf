%% sector_12_p01.ldf
%% Created by Laurence D. Finston (LDF) Sa 24. Aug 15:32:35 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 12:1

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 12:1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)

  p155 := p148 rotated (0, -15);
  p156 := p149 rotated (0, -15);
  
  label.bot("{\bigmath $11^{\rm{h}}$}", p148 shifted (0, -.15));
  label.bot("{\bigmath $12^{\rm{h}}$}", p155 shifted (0, -.15));
  
  label.lft("$0^\circ$", p148);
  label.lft("$15^\circ$", p149);

  label.rt("$0^\circ$", p155);
  label.rt("$15^\circ$", p156);

  label.urt("12:1", p148) with_color OliveGreen_rgb;
  label.lrt("12:1", p149) with_color OliveGreen_rgb;
  label.ulft("12:1", p155 shifted (0, .25)) with_color OliveGreen_rgb;
  label.llft("12:1", p156) with_color OliveGreen_rgb;

  

  if do_labels:
    dotlabel.bot("$p_{148}$", p148) with_color red;
    dotlabel.top("$p_{149}$", p149) with_color red;
    label.bot("$p_{155}$", p155) with_color red; % Autumnal equinox.  Dot drawn below.
                                                 % LDF 2024.08.24.
    dotlabel.top("$p_{156}$", p156) with_color red;
  fi;


%% ** (2)
  
  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p155 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p156 rotated (0, i);
    temp_path1 += temp_pt;

    if i == 8:
      p157 := temp_pt;
    fi;
    
    temp_pt := p70 rotated (-i, -165);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-i, -180);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;

  if false: % true: % 
    drawarrow temp_path0 with_pen big_pen with_color red;
    drawarrow temp_path1 with_pen big_pen with_color blue;
    drawarrow temp_path2 with_pen big_pen with_color green;
    drawarrow temp_path3 with_pen big_pen with_color orange;
  fi;
  
  q40 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q40 += cycle;

  draw q40 with_pen big_pen with_color OliveGreen_rgb;

  sector_path[12][1] := q40;
  
%% ** (2) First point on the ecliptic

  if do_labels:
    dotlabel.lft("$p_{151}$", p151) with_text_color dark_green;
    label.urt("$q_{40}$", p157) with_text_color OliveGreen_rgb;
  fi;


  
%% ** (2) Second point on the ecliptic

  % if do_labels:
  %   dotlabel.rt("$p_{155}$", p155) with_text_color dark_green;
  % fi;

  ang := p155 angle p151;

  q41 += ..;

  for i = 0 step ang/30 until ang:
    temp_pt := p151 rotated_around (p34, p36) -i;
    q41 += temp_pt;
  endfor;

  q41 += p155;
  
  draw q41 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q41) / 2;
    label.urt("$q_{41}$", get_point (n) q41) with_color dark_green;
  fi;

  drawdot p155 with_pen pencircle scaled (1, 1, 1) with_color purple;
  label.ulft("Autumnal Equinox", p155 shifted (-.5, .05)) rotate_text -2.5 with_color purple;
  
%% ** (2) Denebola

% https://en.wikipedia.org/wiki/Denebola
% Beta Leonis.
% Right ascension       11h 49m 03.57834s[2]
% Declination   +14° 34′ 19.4090″[2]
% Apparent magnitude (V)        2.14[3]
    
  p158 := p148 rotated (0, -(49/60 + 3.57834/3600) * 15);  % RA - hours
  p159 := p70 rotated (-(14 + 34/60 + 19.4090/3600), -165);  % Decl.


  if do_labels:
    dotlabel.bot("$p_{158}$", p158) with_color red;
    dotlabel.llft("$p_{159}$", p159) with_color red;
  fi;



  p160 := p159 rotated (0, -(49/60 + 3.57834/3600) * 15); % Rotate by RA - hours.

  %dotlabel.rt("$p_{160}$", p160) with_color red;

  Denebola := p160;

  drawdot Denebola with_pen huge_star_pen;

  % Right ascension     11h 49m 03.57834s[2]
  % Declination         +14° 34′ 19.4090″[2]
  
  label("\starnamed{Denebola}{\beta}{Leonis}{11}{49}{03.5783}{14}{34}{19.4090}", 
      Denebola shifted (-.5, -.5, 0));
  
%% ** (2)

  sector_center[12][1] := sector_center[11][1] rotated (0, -15);

  P13 := sector_center[12][1];

  if do_labels and do_center_dots:
    dotlabel.bot("$P_{13}$", P13) with_color BurntOrange_rgb;
  fi;


%% ** (2)

  rotate t0 (0, 15);

  sector_picture[12][1] := current_picture;

  scale current_picture (100, 100, 100);

  current_picture *= t0;

  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
