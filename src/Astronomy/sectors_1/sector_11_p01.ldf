%% sector_11_p01.ldf
%% Created by Laurence D. Finston (LDF) Sa 24. Aug 10:36:32 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 11:1

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 11:1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
%% *** (3)

  p148 := p143 rotated (0, -15);
  p149 := p136 rotated (0, -15);

  label.bot("{\bigmath $10^{\rm{h}}$}", p143 shifted (0, -.15));
  label.bot("{\bigmath $11^{\rm{h}}$}", p148 shifted (0, -.15));
  
  
  label.lft("$0^\circ$", p143);
  label.lft("$15^\circ$", p136);
  label.rt("$0^\circ$", p148);
  label.rt("$15^\circ$", p149);

  label.lrt("11:1", p136) with_color Periwinkle_rgb;
  label.urt("11:1", p143) with_color Periwinkle_rgb;
  label.llft("11:1", p149) with_color Periwinkle_rgb;
  label.ulft("11:1", p148) with_color Periwinkle_rgb;  

  if do_labels:
    dotlabel.bot("$p_{143}$", p143) with_color red;
    dotlabel.top("$p_{136}$", p136) with_color red;
    dotlabel.bot("$p_{148}$", p148) with_color red;
    dotlabel.top("$p_{149}$", p149) with_color red;
  fi;



%% ** (2)
  
  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p148 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p149 rotated (0, i);
    temp_path1 += temp_pt;

    if i == 8:
      p150 := temp_pt;
    fi;
    
    temp_pt := p70 rotated (-i, -150);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-i, -165);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;

  if false: % true: % 
    drawarrow temp_path0 with_pen big_pen with_color red;
    drawarrow temp_path1 with_pen big_pen with_color blue;
    drawarrow temp_path2 with_pen big_pen with_color green;
    drawarrow temp_path3 with_pen big_pen with_color orange;
  fi;
  
  q38 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q38 += cycle;

  draw q38 with_pen big_pen with_color Periwinkle_rgb;

  sector_path[11][1] := q38;
  
%% ** (2) First point on the ecliptic

  if do_labels:
    dotlabel.ulft("$p_{144}$", p144) with_text_color dark_green;
    label.urt("$q_{38}$", p150) with_text_color Periwinkle_rgb;
  fi;
      
%% ** (2) Second point on the ecliptic

  clear bpv;
  bpv := Ecliptic intersection_points c20;

  % show bpv;
  % pause;

  % message "p143:";
  % show p143;
  % pause;

  p151 := bpv0;

  if do_labels:
    dotlabel.rt("$p_{151}$", p151) with_text_color dark_green;
  fi;

  ang := p151 angle p144;

  q39 += ..;

  for i = 0 step ang/30 until ang:
    temp_pt := p144 rotated_around (p34, p36) -i;
    q39 += temp_pt;
  endfor;

  q39 += p151;
  
  draw q39 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q39) / 2;
    label.urt("$q_{39}$", get_point (n) q39) with_color dark_green;
  fi;

%% ** (2)  Regulus.  Alpha Leonis.

  
  % https://en.wikipedia.org/wiki/Regulus
  % Right ascension 	10h 08m 22.311s[2]
  % Declination 	+11° 58′ 01.95″[2]

    
  p152 := p143 rotated (0, -(8/60 + 22.311/3600) * 15);  % RA - hours
  p153 := p70 rotated (-(11 + 58/60 + 1.95/3600), -150); % Decl.


  if do_labels:
    dotlabel.llft("$p_{152}$", p152) with_color red;
    dotlabel.llft("$p_{153}$", p153) with_color red;
  fi;



  p154 := p153 rotated (0, -(8/60 + 22.311/3600) * 15); % Rotate by RA - hours.



  %dotlabel.rt("$p_{154}$", p154) with_color red;

  Regulus := p154;

  drawdot Regulus with_pen huge_star_pen;

  % Right ascension 	10h 08m 22.311s[2]
  % Declination 	+11° 58′ 01.95″[2]

  label("\starnamed{{\smallrm Regulus}}{\alpha}{Leonis}{10}{08}{22.3110}{11}{58}{01.9500}", 
      Regulus shifted (1.25, .075, 0));
  
%% ** (2)

  sector_center[11][1] := sector_center[10][1] rotated (0, -15);

  P12 := sector_center[11][1];

  if do_labels and do_center_dots:
    dotlabel.bot("$P_{12}$", P12) with_color BurntOrange_rgb;
  fi;

%% ** (2)

  
  rotate t0 (0, 15);
  
  sector_picture[11][1] := current_picture;

  scale current_picture (100, 100, 100);

  current_picture *= t0;

  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
