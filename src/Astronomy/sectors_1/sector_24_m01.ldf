%% sector_24_m01.ldf
%% Created by Laurence D. Finston (LDF) Di 27. Aug 11:25:37 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 24:-1

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 24:--1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)


  p238 := p232 rotated (0, -15);
  p239 := p233 rotated (0, -15);
  
  label.bot("{\bigmath $23^{\rm{h}}$}", p232 shifted (0, -.15));
  label.bot("{\bigmath $0^{\rm{h}}$}", p238 shifted (0, -.15));

  label.lft("$0^\circ$", p233);
  label.lft("$-15^\circ$", p232);
  label.rt("$0^\circ$", p239);
  label.rt("$-15^\circ$", p238);
  

  label.urt("24:$-$1", p232) with_color magenta;
  label.lrt("24:$-$1", p233) with_color magenta;
  label.ulft("24:$-$1", p238) with_color magenta;
  label.llft("24:$-$1", p239) with_color magenta;
  
  if do_labels:
    dotlabel.bot("$p_{232}$", p232) with_color red;
    dotlabel.top("$p_{233}$", p233) with_color red;
    dotlabel.bot("$p_{238}$", p238) with_color red;
    dotlabel.top("$p_{239}$", p239) with_color red;
  fi;

%% ** (2)

    q68 := q66 rotated (0, -15);

    draw q68 with_pen big_pen with_color magenta;

    sector_path[24][-1] := q68;
    
    if do_labels:
      label.ulft("$q_{68}$", get_point ((size q68) * 5 / 8) q68) with_text_color magenta;
    fi;

      
%% ** (2) First point on the ecliptic
      
  if do_labels:
    dotlabel.lft("$p_{234}$", p234) with_text_color dark_green;
  fi;


  
%% ** (2) Second point on the ecliptic

  
  ang := p4 angle p234;

  q69 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p234 rotated_around (p34, p36) -i;
    q69 += temp_pt;
  endfor;

  q69 += p4;
  
  draw q69 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q69) / 2;
    label.ulft("$q_{69}$", get_point (n) q69) with_color dark_green;
  fi;

%% ** (2)
  
  sector_center[24][-1] := sector_center[23][-1] rotated (0, -15);

  P27:= sector_center[24][-1];

  if do_labels and do_center_dots:
    dotlabel.top("$P_{27}$", P27) with_color BurntOrange_rgb;
  fi;

%% ** (2) Phi_Aquarii

  % Right ascension 	23h 14m 19.35965s[1]
  % Declination 	–06° 02′ 56.3986″[1]

  p240 := p70 rotated (6+ 2/60 + 56.3986/3600, 15); % Decl. 
  p241 := p232 rotated (0, -(14/60 + 19.35965/3600)*15); % RA - hours
  p242 := p240 rotated (0, -(14/60 + 19.35965/3600)*15); % RA - hours

  % message "p240:";
  % % show p240;

  if do_labels:
    dotlabel.ulft("$p_{240}$", p240) with_color red;
    dotlabel.lrt("$p_{241}$", p241) with_color red;
  fi;

  %dotlabel.lft("$p_{242}$", p242);

  Phi_Aquarii := p242;

  drawdot Phi_Aquarii with_pen big_star_pen;

  % Right ascension 	23h 14m 19.35965s[1]
  % Declination 	–06° 02′ 56.3986″[1]
      
  label("\starunnamed{\phi}{Aquarii}{+23}{14}{19.35965}{-06}{02}{56.3986}", 
      Phi_Aquarii shifted (-.5, -.5));

%% ** (2)

  sector_picture[24][-1] := current_picture;
  
  rotate t0 (0, 15, 3);
  shift t0 (0, 5);

    
  scale current_picture (100, 100, 100);
  current_picture *= t0;
  shift current_picture (0, -200, -1100);
  
  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
