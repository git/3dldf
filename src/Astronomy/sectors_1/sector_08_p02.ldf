%% sector_08_p02.ldf
%% Created by Laurence D. Finston (LDF) Fr 23. Aug 10:01:33 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 8:2

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 8:2}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)

  p124 := p121 rotated (0, -15);
  p125 := p122 rotated (0, -15);
  
  label.bot("{\bigmath $7^{\rm{h}}$}", p121 shifted (0, -.15));
  label.bot("{\bigmath $8^{\rm{h}}$}", p124 shifted (0, -.15));


  label.lft("$15^\circ$", p121);
  label.rt("$15^\circ$", p124);

  label.lft("$30^\circ$", p122);
  label.rt("$30^\circ$", p125);

  label.urt("8:2", p121) with_color Plum_rgb;
  label.lrt("8:2", p122) with_color Plum_rgb;
  label.ulft("8:2", p124) with_color Plum_rgb;
  label.llft("8:2", p125) with_color Plum_rgb;

  if do_labels:
    dotlabel.bot("$p_{121}$", p121) with_color red;
    dotlabel.top("$p_{122}$", p122) with_color red;
    dotlabel.bot("$p_{124}$", p124) with_color red;
    dotlabel.top("$p_{125}$", p125) with_color red;
  fi;

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p124 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p125 rotated (0, i);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -105);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -120);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;

  if false: % true: % 
    drawarrow temp_path0 with_pen big_pen with_color red;
    drawarrow temp_path1 with_pen big_pen with_color blue;
    drawarrow temp_path2 with_pen big_pen with_color green;
    drawarrow temp_path3 with_pen big_pen with_color orange;
  fi;
  

  q28 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q28 += cycle;

  draw q28 with_pen big_pen with_color Plum_rgb;

  sector_path[8][2] := q28;

  if do_labels:
    label.top("$q_{28}$", get_point ((size q28) * 5 / 8) q28) with_text_color Plum_rgb;
  fi;

  
%% ** (2)

  if do_labels:
    dotlabel.lft("$p_{123}$", p123) with_text_color dark_green;
  fi;
  
  
%% ** (2)
  
  clear bpv;
  bpv := Ecliptic intersection_points c17;

  % show bpv;
  % pause;

  % message "p124:";
  % show p124;
  % pause;
  
  p126 := bpv0;

  if do_labels:
    dotlabel.rt("$p_{126}$", p126) with_text_color dark_green;
  fi;

  ang := p126 angle p123;
  
  % q29 += ..;
  
  for i = 0 step ang/30 until ang:
    temp_pt := p123 rotated_around (p34, p36) -i;
    q29 += temp_pt;
  endfor;

  q29 += p126;
  
  draw q29 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q29) / 2;
    label.urt("$q_{29}$", get_point (n) q29) with_color dark_green;
  fi;

%% ** (2) Pollux, Beta Geminorum

  % https://en.wikipedia.org/wiki/Pollux_(star)
  % β Geminorum
  % Right ascension  07h 45m 18.94987s
  % Declination     +28° 01′  34.3160″

  drawdot Pollux with_pen big_star_pen;

  % message "Pollux:";
  % show Pollux;

  % message "p123:";
  % show p123;

  % message "p126:";
  % show p126;

  % pause;
  
  label("\starnamed{Pollux}{\beta}{Geminorum}{07}{45}{18.9499}{28}{01}{34.3160}", 
      Pollux shifted (0, -.5, -.5));
  

%% ** (2)

  sector_center[8][2] := sector_center[7][2] rotated (0, -15);

  P8 := sector_center[8][2];

  if do_labels and do_center_dots:
    dotlabel.top("$P_8$", P8) with_color BurntOrange_rgb;
  fi;

  
%% ** (2)

  %draw q26 with_color Bittersweet_rgb with_pen big_pen; % Sector 7:2
  
  rotate t0 (-.6667, 15, -4);
  
  sector_picture[8][2] := current_picture;

  scale current_picture (100, 100, 100);

  current_picture *= t0;


  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
