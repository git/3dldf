%% sector_05_p02.ldf
%% Created by Laurence D. Finston (LDF) Do 22. Aug 17:57:18 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 5:2

% Tau_Tauri
% Right ascension  4h 42m 14.70161s[1]
% Declination 	   22° 57′ 24.9214″[1]

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 5:2}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  p111 := p97 rotated (0, -15);
  p112 := p105 rotated (0, -15);

  p114 := p111 rotated (0, -15);
  p115 := p112 rotated (0, -15);

  label.bot("{\bigmath $4^{\rm{h}}$}", p111 shifted (0, -.15));
  label.bot("{\bigmath $5^{\rm{h}}$}", p114 shifted (0, -.15));

  label.lft("$15^\circ$", p111);
  %label.lft("$20^\circ$", p108);
  %label.lft("$25^\circ$", p110);
  label.lft("$30^\circ$", p112);

  label.rt("$15^\circ$", p114);
  %label.rt("$20^\circ$", p107);
  %label.rt("$25^\circ$", p109);
  label.rt("$30^\circ$", p115);

  label.urt("5:2", p111) with_color RawSienna_rgb;
  label.ulft("5:2", p114) with_color RawSienna_rgb;
  label.lrt("5:2", p112) with_color RawSienna_rgb;
  label.llft("5:2", p115) with_color RawSienna_rgb;

  if do_labels:
    dotlabel.lrt("$p_{111}$", p111) with_color red;
    dotlabel.top("$p_{112}$", p112) with_color red;
    dotlabel.llft("$p_{114}$", p114) with_color red;
    dotlabel.top("$p_{115}$", p115) with_color red;
  fi;

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p114 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p115 rotated (0, i);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -60);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -75);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;
  
  % drawarrow temp_path0 with_pen big_pen with_color red;
  % drawarrow temp_path1 with_pen big_pen with_color blue;
  % drawarrow temp_path2 with_pen big_pen with_color green;
  % drawarrow temp_path3 with_pen big_pen with_color orange;

  q22 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q22 += cycle;

  draw q22 with_pen big_pen with_color RawSienna_rgb;

  sector_path[5][2] := q22;
  
  if do_labels:
    label.top("$q_{22}$", get_point ((size q22) * 5 / 8) q22) with_text_color RawSienna_rgb;
  fi;

%% ** (2)

  if do_labels:
    dotlabel.lft("$p_{113}$", p113) with_color dark_green;
  fi;
  
  
%% ** (2)
  
  clear bpv;
  bpv := Ecliptic intersection_points c14;
  
  p116 := bpv0;

  if do_labels:
    dotlabel.rt("$p_{116}$", p116) with_color dark_green;
  fi;

  ang := p116 angle p113;
  
  q23 += ..;
  
  for i = 0 step ang/30 until ang:
    temp_pt := p113 rotated_around (p34, p36) -i;
    q23 += temp_pt;
  endfor;

  q23 += p116;
  
  draw q23 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q23) * 3/8;
    label.ulft("$q_{23}$", get_point (n) q23) with_color dark_green;
  fi;

%% ** (2)
  
% Tau_Tauri
% Right ascension  4h 42m 14.70161s[1]
% Declination 	   22° 57′ 24.9214″[1]

  ra   := (4 + 42/60 + 14.70161/3600) * 15;
  decl := (22 + 57/60 + 24.9214/3600);
  
  Tau_Tauri := (0, 0, -radius_val0) rotated (-decl, -ra);
  
  drawdot Tau_Tauri with_pen medium_star_pen;

  s := "\setbox0=\hbox{{\smallrm Decl.}}"
     & "\setbox1=\hbox{{\smallrm Decl.~{\mediummath$22$ $57$ $24.9214$}}}"
     & "\vbox{\hbox to \wd1{\hfil {\bigmath$\csname tau\endcsname$\hskip1pt Tauri}}"
     & "\hbox{{\smallrm \hbox to \wd0{RA\hfil}~{\mediummath$04$ $42$ $14.7016$}}}"
     & "\box1}";

  label.ulft(s, Tau_Tauri shifted (.05, .05));

%% ** (2)

  sector_center[5][2] := sector_center[4][2] rotated (0, -15);

  P5 := sector_center[5][2];

  if do_labels and do_center_dots:
    dotlabel.rt("$P_5$", P5) with_color BurntOrange_rgb;
  fi;

%% ** (2)

  sector_picture[5][2] := current_picture;

  %current_picture += sector_picture[3][3];

  
  scale current_picture (100, 100, 100);
  rotate current_picture (37, 0);
  rotate current_picture (0, 180+65, -35);
  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
