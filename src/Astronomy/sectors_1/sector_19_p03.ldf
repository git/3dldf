%% sector_19_p03.ldf
%% Created by Laurence D. Finston (LDF) Mi 28. Aug 20:42:24 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 19:3

%% 005 Vega
% Alpha Lyrae and abbreviated Alpha Lyr
% Right ascension  18h 36m 56.33635s
% Declination     +38° 47′ 01.2802″

%% ** (2)

beginfig(fig_ctr);

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 19:3}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;


  
%% *** (3)

  % Right ascension  18h 36m 56.33635s
  % Declination     +38° 47′ 01.2802″
  
  p259 := p4 rotated (-30, -18*15);
  p260 := p259 rotated (0, -15);
  p261 := p4 rotated (-45, -18*15);
  p262 := p261 rotated (0, -15);

  P32 := p4 rotated (-(45-7.5), -18.5*15);

  if do_labels and do_center_dots:
    dotlabel.rt("$P_{32}$", P32) with_color BurntOrange_rgb;
  fi;



    
  if do_labels:
    dotlabel.bot("$p_{259}$", p259) with_color red;
    dotlabel.bot("$p_{260}$", p260) with_color red;
    dotlabel.top("$p_{261}$", p261) with_color red;
    dotlabel.top("$p_{262}$", p262) with_color red;
  fi;



%% ** (2)

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step 1 until 15:
    temp_pt := p260 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p262 rotated (0, i);
    temp_path1 += temp_pt;
    
    temp_pt := p70 rotated (-(i+30), -18*15);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-(i+30), -19*15);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;


  if false: % true: % 
    drawarrow temp_path0 with_pen big_pen with_color red;
    drawarrow temp_path1 with_pen big_pen with_color blue;
    drawarrow temp_path2 with_pen big_pen with_color green;
    drawarrow temp_path3 with_pen big_pen with_color orange;
  fi;

  
  q74 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q74 += cycle;


  draw q74 with_pen big_pen with_color Sepia_rgb;

  sector_path[19][3] := q74;

  if do_labels:
    label.top("$q_{74}$", get_point ((size q74) * 5 / 8) q74) with_text_color Sepia_rgb;
  fi;
  

  label.bot("{\bigmath $18^{\rm{h}}$}", p259 shifted (0, -.15));
  label.bot("{\bigmath $19^{\rm{h}}$}", p260 shifted (0, -.15));

  
  label.lft("$45^\circ$", p261);
  label.rt("$455^\circ$", p262);
  label.lft("$30^\circ$", p259);
  label.rt("$30^\circ$", p260);


  label.lrt("19:3", p261) with_color Sepia_rgb;
  label.llft("19:3", p262) with_color Sepia_rgb;
  label.urt("19:3", p259) with_color Sepia_rgb;
  label.ulft("19:3", p260) with_color Sepia_rgb;

%% ** (2) 005 Vega

  % Alpha Lyrae and abbreviated Alpha Lyr
  % Right ascension  18h 36m 56.33635s
  % Declination     +38° 47′ 01.2802″

    drawdot Vega with_pen huge_star_pen;

  % !! TODO:  Check seconds values.  Wikipedia lists these.  LDF 2024.08.28.
  
  label("\starnamed{Vega}{\alpha}{Lyr{\ae}}{18}{36}{56.3364}{38}{47}{01.2802}", 
      Vega shifted (.5, -.125, 0));

%% ** (2)

  sector_picture[19][3] := current_picture;

  t3 := align (P32 -- origin) with_axis z_axis;
  rotate t3 (195, 0, 0);
  shift t3 (0, 1.75, 14);
  
  current_picture *= t3;

  scale current_picture (100, 100, 100);
  shift current_picture (0, -200, -1100);
  
  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
