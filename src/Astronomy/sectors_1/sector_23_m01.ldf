%% sector_23_m01.ldf
%% Created by Laurence D. Finston (LDF) Di 27. Aug 11:25:37 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 23:-1

beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 23:--1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)

  
  p232 := p221 rotated (0, -15);
  p233 := p227 rotated (0, -15);
  

  label.bot("{\bigmath $22^{\rm{h}}$}", p221 shifted (0, -.15));
  label.bot("{\bigmath $23^{\rm{h}}$}", p232 shifted (0, -.15));

  label.lft("$0^\circ$", p227);
  label.rt("$0^\circ$", p233);
  label.lft("$-15^\circ$", p221);
  label.rt("$-15^\circ$", p232);

  label.urt("23:$-$1", p221) with_color blue;
  label.ulft("23:$-$1", p232) with_color blue;
  label.lrt("23:$-$1", p227) with_color blue;
  label.llft("23:$-$1", p233) with_color blue;
  
  if do_labels:
    dotlabel.bot("$p_{221}$", p221) with_color red;
    dotlabel.top("$p_{227}$", p227) with_color red;
    dotlabel.bot("$p_{232}$", p232) with_color red;
    dotlabel.top("$p_{233}$", p233) with_color red;
  fi;

%% ** (2)

    
    q66 := q64 rotated (0, -15);

    draw q66 with_pen big_pen with_color blue;

    sector_path[23][-1] := q66;
    
    if do_labels:
      label.ulft("$q_{66}$", get_point ((size q66) * 5 / 8) q66) with_text_color blue;
    fi;

      
%% ** (2) First point on the ecliptic
      
  if do_labels:
    dotlabel.lft("$p_{228}$", p228) with_text_color dark_green;
  fi;

  
%% ** (2) Second point on the ecliptic

  clear bpv;
  bpv := Ecliptic intersection_points celestial_meridian23;

  % show bpv;
  % message "p232:";
  % show p232;
  
  p234 := bpv1;

  if do_labels:
    dotlabel.lrt("$p_{234}$", p234) with_text_color dark_green;
  fi;


  ang := p234 angle p228;

  q67 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p228 rotated_around (p34, p36) -i;
    q67 += temp_pt;
  endfor;

  q67 += p234;
  
  draw q67 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q67) / 2;
    label.ulft("$q_{67}$", get_point (n) q67) with_color dark_green;
  fi;


  
%% ** (2)
  
  sector_center[23][-1] := sector_center[22][-1] rotated (0, -15);

  P26:= sector_center[23][-1];

  if do_labels and do_center_dots:
    dotlabel.top("$P_{26}$", P26) with_color BurntOrange_rgb;
  fi;

%% ** (2) Sadalmelik (Alpha Aquarii)

  % 164th brightest star.
  % Right ascension 	22h 05m 47.03555s[1]
  % Declination 	−00° 19′ 11.4634″[1]
  % Apparent magnitude (V) 	2.942[2]
    
  p235 := p70 rotated (19/60 + 11.4634/3600, 30); % Decl. 
  p236 := p221 rotated (0, -(5/60 + 47.03555/3600)*15); % RA - hours
  p237 := p235 rotated (0, -(5/60 + 47.03555/3600)*15); % RA - hours

  % message "p235:";
  % % show p235;

  if do_labels:
    dotlabel.llft("$p_{235}$", p235) with_color red;
    dotlabel.lrt("$p_{236}$", p236) with_color red;
  fi;

  %dotlabel.lft("$p_{237}$", p237);

  Sadalmelik := p237;

  drawdot Sadalmelik with_pen Big_star_pen;

  % Right ascension 	22h 05m 47.03555s[1]
  % Declination 	−00° 19′ 11.4634″[1]
    
  label("\starnamed{Sadalmelik}{\alpha}{Aquarii}{+22}{05}{47.0356}{-00}{19}{11.4634}", 
      Sadalmelik shifted (-1.25, -.75));

%% ** (2)

  
  sector_picture[23][-1] := current_picture;
  
  rotate t0 (0, 15);
  shift t0 (0, 0);




  
  scale current_picture (100, 100, 100);
  current_picture *= t0;
  shift current_picture (0, -200, -1100);
  
  output current_picture with_focus f;
  clear current_picture;
  clip current_picture to outer_frame;

  unfill frame_fill_path_left;
  unfill frame_fill_path_right;
  unfill frame_fill_path_bot;

  draw frame with_pen big_square_pen;
  draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
