%% sector_17_p03.ldf
%% Created by Laurence D. Finston (LDF) Di 17. Dez 22:24:39 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 17:3.

beginfig(fig_ctr);

%% *** (3)

  message "Fig. " & decimal fig_ctr & ".  Sector 17:3";
  
  s := "17_p03";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $17:3$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q221 := q220 rotated (0, -15);

  sector_path[17][3] := q221;

  draw q221 with_color blue with_pen big_pen;

  if do_labels:
    label.top("$q_{221}$", get_point ((size q221)*5/8) q221) with_text_color blue;
  fi;

  p544 := p542 rotated (0, -15);
  p545 := p543 rotated (0, -15);        

  P179 := P178 rotated (0, -15);

  sector_center[17][3] := P179;
  
  if do_labels and do_center_dots:
    drawdot P179 with_color BurntOrange_rgb with_pen dot_pen;
    label.bot("$P_{179}$", P179 shifted (0, 0)) with_color BurntOrange_rgb;
  fi;

  if do_labels:
    drawdot p542 with_color red with_pen dot_pen;
    drawdot p543 with_color red with_pen dot_pen;
    drawdot p544 with_color red with_pen dot_pen;
    drawdot p545 with_color red with_pen dot_pen;
    label("$p_{542}$", p542 shifted (0, -.125)) with_color red;
    label("$p_{544}$", p544 shifted (0, -.125)) with_color red;
    label("$p_{543}$", p543 shifted (0, .333)) with_color red;
    label("$p_{545}$", p545 shifted (0, .333)) with_color red;
  fi;

%% *** (3) Hour and declination labels

  label("{\bigmath $16^{\rm{h}}$}", p542 shifted (0, -.25));
  label("{\bigmath $17^{\rm{h}}$}", p544 shifted (0, -.25));

  label.lft("$30^\circ$",  p542 shifted (0, 0, 0));
  label.rt("$30^\circ$", p544 shifted (0, 0, 0));

  label.lft("$45^\circ$", p543 shifted (0, 0, 0));
  label.rt("$45^\circ$", p545 shifted (0, 0, 0));
  
%% *** (3) 133 Zeta_Herculis

  % 133 Zeta Herculis (Rutilicus). This name can also refer to Beta Her,
  % which is formally named Kornephoros.
  % For this reason, I don't use the name for this star.
  % LDF 2024.12.17.
  %
  % https://en.wikipedia.org/wiki/Zeta_Herculis
  % Constellation         Hercules, Herculis, Her
  % Right ascension       16h 41m 17.16104s[1]
  % Declination   +31° 36′ 09.7873″[1]
  % Apparent magnitude (V)        2.81[2]

  star_name := "";
  greek_letter := "\zeta";
  constellation_name_gen := "Herculis";
  constellation_name_abbrev := "Her";

  star_label {Zeta_Herculis, 0, .75, .25, huge_star_pen, true, star_name, false, 
       greek_letter, constellation_name_gen, constellation_name_abbrev,
       16, 41, 17.16104, 31, 36, 9.7873};
 
%% *** (3) 
  
  if false: % true
    drawarrow P179 -- P179 shifted (1, 0) with_color red;
    drawarrow P179 -- P179 shifted (0, 1) with_color blue;
    drawarrow P179 -- P179 shifted (0, 0, 1) with_color green;
  fi;
  
  sector_picture[17][3] := current_picture;

  t5 := align (origin -- P179) with_axis z_axis;
  current_picture *= t5;

  scale current_picture (100, 100, 100);
  rotate current_picture (0, 0, 0);
  shift current_picture (0, 0, -1125);
  
%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;

  
endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
