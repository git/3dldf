%% developments_bright_stars.ldf
%% Created by Laurence D. Finston (LDF) Mo 21. Okt 16:35:44 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% ** (2) 002. Canopus

%% *** (3) 002 Canopus.  Sector 7:-4

% https://en.wikipedia.org/wiki/Canopus
% 06h 23m 57.1099s, -52° 41′ 44.378″
% Alpha Carinae, 

  sector_A (pv, qv) {p8, 45, 60, 1/24, 100, radius_val, mod_val, border_val,
                   horiz_val, vert_offset, tab_val, false, false, false}; % -15°--(-30°);

  v0 := current_picture;
  clear current_picture;

  s := "\vbox{{\Tinyrm\hbox{Canopus}}}";

  plot_star {p8, radius_val, huge_star_pen,
             s, .175, -.35,           % label
             false, false, 0, 0, % x-ray source or variable star
             6, 23, 57.1099,     % RA
             52, 41, 44.378,     % Decl.
             false, false};      % help labels and lines

  v1 := current_picture;
  
  label.bot("{\smallrm Sector 7:4}", p8 shifted (0, -.5));



  current_picture += v0;
  current_picture += v1;
  shift current_picture (-10, 6.5);
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2) 003 Rigil_Kentaurus_A.  Sector 15:-5.

  % https://en.wikipedia.org/wiki/Alpha_Centauri
  % Alpha Centauri A
  % RA 14h 39m 36.49400s
  % Declination   -60° 50′ 02.3737″ 

  sector_A (pv, qv) {p8, 60, 75, 1/24, 100, radius_val, mod_val, border_val,
                   horiz_val, vert_offset, tab_val, false, false, true}; % -60--(-75°)

  v0 := current_picture;
  clear current_picture;

  s := "\vbox{{\Tinyrm\Tinymath\hbox{$\alpha$ Cen}}}";

  plot_star {p8, radius_val, huge_star_pen,
             s, -.1333, -.333,      % label
             false, false, 0, 0, % x-ray source or variable star
             14, 39, 36.49400,   % RA
             -60, 50, 2.3737,    % Decl.
             false, false};      % help labels and lines

  v1 := current_picture;
  
  label.bot("{\smallrm Sector 15:-5}", p8 shifted (0, -2.25));



  current_picture += v0;
  current_picture += v1;
  shift current_picture (-7, 8);
  output current_picture with_projection parallel_x_y;
  clear current_picture;

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
