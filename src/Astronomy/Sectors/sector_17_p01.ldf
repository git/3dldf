%% sector_17_p01.ldf
%% Created by Laurence D. Finston (LDF) Di 8. Okt 10:58:06 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 17:1.

beginfig(fig_ctr);

%% *** (3)

  s := "17_p01";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $17:1$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
    
  q140 := q139 rotated (0, -15);

  sector_path[17][1] := q140;

  draw q140 with_color blue with_pen big_pen;

  if do_labels:
    label.top("$q_{140}$", get_point ((size q140) *5/8) q140) with_text_color blue;
  fi;

  p404 := p402 rotated (0, -15);
  p405 := p403 rotated (0, -15);
  
  if do_labels:
    dotlabel.bot("$p_{402}$", p402) with_color red;
    dotlabel.bot("$p_{404}$", p404) with_color red;
    dotlabel.top("$p_{403}$", p403) with_color red;
    dotlabel.top("$p_{405}$", p405) with_color red;
  fi;

  P98 := P97 rotated (0, -15);

  sector_center[17][1] := P98;
  
  if do_labels and do_center_dots:
    dotlabel.top("$P_{98}$", P98) with_color BurntOrange_rgb;
  fi;

%% *** (3) Hour and declination labels

  label.bot("{\bigmath $16^{\rm{h}}$}", p402 shifted (0, -.2));
  label.bot("{\bigmath $17^{\rm{h}}$}", p404 shifted (0, -.2));
  
  label.lft("$0^\circ$", p402);
  label.rt("$0^\circ$", p404);
  label.lft("$15^\circ$", p403);
  label.rt("$15^\circ$", p405);

%% *** (3) Marfik

  % https://en.wikipedia.org/wiki/Lambda_Ophiuchi
  % Marfik
  % Constellation       Ophiuchus
  % Right ascension     16h 30m 54.82314s[1]
  % Declination         +01° 59′ 02.1209″[1]
  % Apparent magnitude (V)      3.82[2] 4.18 + 5.22 + 11.0)
 

  drawdot Marfik with_pen medium_star_pen;

  s := "\starnamed{Marfik}{\lambda}{Ophiuchi}{16}{30}{54.8231}{01}{59}{2.1209}";

  label.top(s, Marfik shifted (0, .125));

%% *** (3)

  sector_picture[17][1] := current_picture;
    
  t5 := align (origin -- P98) with_axis z_axis;

  shift t5 (0, 2);

  current_picture *= t5;

  scale current_picture (100, 100, 100);
  shift current_picture (0, -200, -1100);

  
%% *** (3)
  
endfig with_focus f suppress_warnings;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
