%% sector_19_p02.ldf
%% Created by Laurence D. Finston (LDF) Do 7. Nov 08:34:01 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 19:2.

beginfig(fig_ctr);

%% *** (3)

  s := "19_p02";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $19:2$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q190 := q189 rotated (0, -15);

  sector_path[19][2] := q190;

  draw q190 with_color blue with_pen big_pen;

  if do_labels:
    label.top("$q_{190}$", get_point ((size q190)*5/8) q190) with_text_color blue;
  fi;

  p482 := p480 rotated (0, -15);
  p483 := p481 rotated (0, -15);	

  P148 := P147 rotated (0, -15);

  sector_center[19][2] := P148;
  
  if do_labels and do_center_dots:
    dotlabel.bot("$P_{148}$", P148) with_color BurntOrange_rgb;
  fi;

  if do_labels:
    dotlabel.bot("$p_{480}$", p480) with_color red;
    dotlabel.bot("$p_{482}$", p482) with_color red;
    dotlabel.top("$p_{481}$", p481) with_color red;
    dotlabel.top("$p_{483}$", p483) with_color red;
  fi;

%% *** (3) Hour and declination labels

  label.bot("{\bigmath $18^{\rm{h}}$}", p480 shifted (0, -.1));
  label.bot("{\bigmath $19^{\rm{h}}$}", p482 shifted (0, -.1));


  label.lft("$15^\circ$",  p480);
  label.rt("$15^\circ$", p482);

  label.lft("$30^\circ$", p481);
  label.rt("$30^\circ$", p483);
  
%% *** (3) Omicron Herculis

  % https://en.wikipedia.org/wiki/Omicron_Herculis
  % Right ascension       18h 07m 32.55073s[1]
  % Declination   +28° 45′ 44.9679″[1]
  % Apparent magnitude (V)        3.83[2]
  
  star_name := "";
  greek_letter := "\omicron";
  constellation_name_gen := "Herculis";
  constellation_name_abbrev := "Her";

  star_label {Omicron_Herculis, 0, -.45, -.6, medium_star_pen, true, star_name, false, 
      greek_letter, constellation_name_gen, constellation_name_abbrev,
      18, 7, 32.55073, 28, 45, 44.9679};
  
%% *** (3) 
  
  if false: % true 
    drawarrow P148 -- P148 shifted (1, 0) with_color red;
    drawarrow P148 -- P148 shifted (0, 1) with_color blue;
    drawarrow P148 -- P148 shifted (0, 0, 1) with_color green;
  fi;

  sector_picture[19][2] := current_picture;

  t5 := align (origin -- P148) with_axis z_axis;

  rotate t5 (0, 0, -92.5);

  current_picture *= t5;
    
  scale current_picture (100, 100, 100);
  shift current_picture (0, 0, -1130);

%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
