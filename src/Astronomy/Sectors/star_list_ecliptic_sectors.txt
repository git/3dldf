%% star_list_ecliptic_sectors.txt
%% Created by Laurence D. Finston (LDF) Di 27. Aug 15:43:11 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Top

sector_01_p01.ldf:  Delta_Piscium 
sector_02_p01.ldf:  Mu_Piscium 
sector_03_p01.ldf:  Gamma_Ceti 
sector_03_p02.ldf:  Pi_Arietis 
sector_04_p02.ldf:  Alcyone 
sector_04_p02.ldf:  Atlas 
sector_04_p02.ldf:  Electra 
sector_04_p02.ldf:  Maia 
sector_04_p02.ldf:  Merope 
sector_04_p02.ldf:  Taygeta 
sector_04_p02.ldf:  Pleione 
sector_04_p02.ldf:  Atlas 
sector_05_p02.ldf:  Tau_Tauri 
sector_06_p02.ldf:  Elnath 
sector_07_p02.ldf:  Mebsuta 
sector_08_p02.ldf:  Pollux 
sector_09_p02.ldf:  Asellus_Australis 
sector_09_p02.ldf:  Asellus_Borealis 
sector_10_p01.ldf:  Psi_Leonis 
sector_10_p02.ldf:  Algenubi 
sector_11_p01.ldf:  Regulus 
sector_12_p01.ldf:  Denebola 
sector_13_m01.ldf:  Porrima 
sector_14_m01.ldf:  Spica 
sector_15_m01.ldf:  Kappa_Virginis 
sector_15_m02.ldf:  Zubenelgenubi 
sector_16_m02.ldf:  Sigma_Librae 
sector_17_m02.ldf:  Antares 
sector_18_m02.ldf:  Theta_Ophiuchi 
sector_19_m02.ldf:  Nunki 
sector_20_m02.ldf:  Albaldah 
sector_21_m02.ldf:  Upsilon_Capricorni 
sector_22_m01.ldf:  Lambda_Capricorni 
sector_22_m02.ldf:  Deneb_Algedi 
sector_23_m01.ldf:  Sadalmelik 
sector_24_m01.ldf:  Phi_Aquarii 

%% * (1) End of star_list_ecliptic_sectors.txt


