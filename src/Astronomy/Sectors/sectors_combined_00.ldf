%% sectors_combined_00.ldf
%% Created by Laurence D. Finston (LDF) Mi 21. Aug 14:01:18 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Loop, horizontal.  Sectors 1:1 -- 3:1 

for i = -15 step 1 until 14:

  beginfig(fig_ctr);

%% *** (3)

%% *** (3) Frame and labels

    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;
    fi;
    
    draw outer_frame with_pen big_square_pen;
    draw frame with_pen big_square_pen;

  %label("{\largerm Sectors}", mediate(p2, p3) shifted (0, -1.5));

    if do_labels:
      s := "{\bigmath$" & decimal i & "$}";
      label.top(s, mediate(p0, p1));
    fi;
    
    output current_picture with_projection parallel_x_y;
    clear current_picture;


    if i <= 14:
      current_picture += sector_picture[1][1];
    fi; 

    current_picture += sector_picture[2][1];

    if i >= -13:
      current_picture += sector_picture[3][1];
      current_picture += sector_picture[3][2];
    fi;

    if i >= 8:
      draw q34 with_pen big_pen with_color CadetBlue_rgb;
      draw q35 with_pen big_pen with_color CadetBlue_rgb;
      current_picture += sector_picture[4][2];
    fi;
    
    scale current_picture (100, 100, 100);
    rotate current_picture (0, 180+22.5+i); % 15 + 15/2 + i
    shift current_picture (0, -200, -1100);

    output current_picture with_focus f suppress_warnings;
    clear current_picture;

    %clip current_picture to outer_frame;
    % unfill frame_fill_path_left;
    % unfill frame_fill_path_right;
    % unfill frame_fill_path_top;
    % unfill frame_fill_path_bot;

    if do_labels:
      label.bot("{{\mediumtt sectors\char`_combined\char`_00.ldf}}", mediate(p0, p1) shifted (0, -.5));
    fi;
        
    draw frame with_pen big_square_pen;
    draw outer_frame with_pen big_square_pen;

%% *** (3)    
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
endfor;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
