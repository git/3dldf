%% sector_03_p02.ldf
%% Created by Laurence D. Finston (LDF) Mi 21. Aug 08:04:31 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 3:2

% https://en.wikipedia.org/wiki/Sigma_Arietis
% Sigma Arietis
% Right ascension       02h 51m 29.58618s[1]
% Declination   +15° 04′ 55.4438″

beginfig(fig_ctr);

%% *** (3)

  s := "03_p02";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 3:2}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  label.urt("3:2", p96) with_color rose_madder_rgb;
  label.ulft("3:2", p97) with_color rose_madder_rgb;
  label.lrt("3:2", p104) with_color rose_madder_rgb;
  label.llft("3:2", p105) with_color rose_madder_rgb;

  label.bot("{\bigmath $2^{\rm{h}}$}", p96 shifted (0, -.15));
  label.bot("{\bigmath $3^{\rm{h}}$}", p97 shifted (0, -.15));

  
  if do_labels:
    dotlabel.ulft("$p_{92}$", p92) with_color dark_green;
    dotlabel.bot("$p_{96}$", p96) with_color red;
    dotlabel.lrt("$p_{97}$", p97) with_color red;
    dotlabel.top("$p_{104}$", p104) with_color red;
    dotlabel.top("$p_{105}$", p105) with_color red;
  fi;

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;

  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;

  j := 107;
  
  for i = 0 step .25 until 15:
    temp_pt := p105 rotated (0, i);
    temp_path0 += temp_pt;
    
    temp_pt := p97 rotated (0, i);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -45);
    temp_path2 += temp_pt;

    if (i == 5) or (i == 10):
      p[j] := temp_pt;
      j += 1;
    fi;
      
    
    temp_pt := p70 rotated (-(i+15), -30);
    temp_path3 += temp_pt;

    if (i == 5) or (i == 10):
      p[j] := temp_pt;
      j += 1;
    fi;

  endfor;

  if do_labels:
    dotlabel.lft("$p_{107}$", p107) with_color red;
    dotlabel.rt("$p_{108}$", p108) with_color red;
    dotlabel.lft("$p_{109}$", p109) with_color red;
    dotlabel.rt("$p_{110}$", p110) with_color red;
  fi;
  

  
  reverse temp_path0;
  reverse temp_path2;

  % drawarrow temp_path0 with_pen big_pen with_color red;
  % drawarrow temp_path1 with_pen big_pen with_color blue;
  % drawarrow temp_path2 with_pen big_pen with_color green;
  % drawarrow temp_path3 with_pen big_pen with_color orange;
  
  q18 := temp_path1 -- temp_path3 -- temp_path0 --- temp_path2;
  q18 += cycle;

  clear bpv;
  bpv := c10 intersection_points Ecliptic;

  % show bpv;
  % pause;

  % message "p97:";
  % show p97;
  % pause;
  
  p106 := bpv1;

  if do_labels:
    n := (size q18) * .57;
    label.top("$q_{18}$", get_point (n) q18) with_color blue;

    dotlabel.rt("$p_{106}$", p106) with_color dark_green;

  fi; 
  
  draw q18 with_color blue with_pen big_pen; % Sector path 3:1
  sector_path[3][2] := q18;

  % reverse temp_path0;
  % reverse temp_path2;
  
  % drawarrow temp_path0 with_pen big_pen with_color red;
  % drawarrow temp_path1 with_pen big_pen with_color blue;
  % drawarrow temp_path2 with_pen big_pen with_color green;
  % drawarrow temp_path3 with_pen big_pen with_color orange;

  
%% *** (3) Section of the ecliptic q19
  
  q19 += ..;
  
  q19 += p92;

  ang := p106 angle p92;
  
  % message "ang: " & decimal ang;
  % pause;
  
  q19 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p92 rotated_around (p34, p36) -i;
    q19 += temp_pt;
  endfor;

  q19 += p106;
  
  draw q19 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q19) / 2;
    label.ulft("$q_{19}$", get_point (n) q19) with_color dark_green;
  fi;

%% *** (3) Labels for RA and declination 

  % label.bot("{\bigmath $2^{\rm{h}}$}", p96 shifted (0, -.15));
  % label.bot("{\bigmath $3^{\rm{h}}$}", p97 shifted (0, -.15));

  % label.top("{\bigmath $2^{\rm{h}}$}", p104 shifted (0, .15));
  % label.top("{\bigmath $3^{\rm{h}}$}", p105 shifted (0, .15));
  
  label.lft("$15^\circ$", p96);
  label.lft("$20^\circ$", p108);
  label.lft("$25^\circ$", p110);
  label.lft("$30^\circ$", p104);

  label.rt("$15^\circ$", p97);
  label.rt("$20^\circ$", p107);
  label.rt("$25^\circ$", p109);
  label.rt("$30^\circ$", p105);


  
%% *** (3) Pi Arietis

% Pi Arietis
% https://en.wikipedia.org/wiki/Pi_Arietis
% Right ascension 	02h 49m 17.55924s
% Declination 	+17° 27′ 51.5168″
% Apparent magnitude (V)        5.21[2]
  
  drawdot Pi_Arietis with_pen medium_star_pen;

  s := "\setbox0=\hbox{{\smallrm Decl.}}"
     & "\setbox1=\hbox{{\smallrm Decl.~{\mediummath$17$ $27$ $51.5168$}}}"
     & "\vbox{\hbox to \wd1{\hfil {\bigmath$\pi$\hskip1pt Arietis}}"
     & "\hbox{{\smallrm \hbox to \wd0{RA\hfil}~{\mediummath$02$ $49$ $17.5592$}}}"
     & "\box1}";

  label.ulft(s, Pi_Arietis shifted (0, 0));

%% *** (3)

  sector_center[3][2] := p70 rotated (-22.5, -37.5);

  P3 := sector_center[3][2];

  if do_labels and do_center_dots:
    dotlabel.top("$P_3$", P3) with_color BurntOrange_rgb;
  fi;

%% *** (3)  
  

  sector_picture[3][2] := current_picture;
  
  scale current_picture (100, 100, 100);
  rotate current_picture (18, 0);
  rotate current_picture (0, 180+37.5, -11);
  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  % clip current_picture to outer_frame;

  % unfill frame_fill_path_left;
  % unfill frame_fill_path_right;
  % unfill frame_fill_path_bot;

  % draw frame with_pen big_square_pen;
  % draw outer_frame with_pen big_square_pen;
  
%% *** (3)    


  
endfig with_projection parallel_x_y;
fig_ctr += 1;



endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
