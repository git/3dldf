%% sector_23_m03.ldf
%% Created by Laurence D. Finston (LDF) Mi 1. Jan 18:44:55 CET 2025

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 23:-3.

beginfig(fig_ctr);

%% *** (3)

  message "Fig. " & decimal fig_ctr & ".  Sector 23:-3";
  
  s := "23_m03";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $23:-3$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q247 := q246 rotated (0, -15);

  sector_path[23][-3] := q247;

  draw q247 with_color blue with_pen big_pen;

  if do_labels:
    label("$q_{247}$", (get_point ((size q247)/8) q247) shifted (0, .125, -.125)) with_text_color blue;
  fi;

  p596 := p594 rotated (0, -15);
  p597 := p595 rotated (0, -15);        

  P205 := P204 rotated (0, -15);

  sector_center[23][-3] := P205;
  
  if do_labels and do_center_dots:
    drawdot P205 with_color BurntOrange_rgb with_pen dot_pen;
    label("$P_{205}$", P205 shifted (0, .2, 0)) with_color BurntOrange_rgb;
  fi;

  if do_labels:
    drawdot p594 with_color red with_pen dot_pen;
    drawdot p595 with_color red with_pen dot_pen;
    drawdot p596 with_color red with_pen dot_pen;
    drawdot p597 with_color red with_pen dot_pen;
    label("$p_{594}$", p594 shifted (0, .333, .125)) with_color red;
    label("$p_{596}$", p596 shifted (-.1, .25, -.1)) with_color red;
    label("$p_{595}$", p595 shifted (0, .125, 0)) with_color red;
    label("$p_{597}$", p597 shifted (0, .125, 0)) with_color red;
  fi;

%% *** (3) Hour and declination labels

  label("{\bigmath $22^{\rm{h}}$}", p594 shifted (0, -.5));
  label("{\bigmath $23^{\rm{h}}$}", p596 shifted (0, -.5));

  label("$-30^\circ$",  p595 shifted (0, 0, .25));
  label("$-30^\circ$", p597 shifted (-.25, 0, -.25));

  label("$-45^\circ$", p594 shifted (0, .25, .25));
  label("$-45^\circ$", p596 shifted (-.333, -.333, -.333));
  
%% *** (3) 

  % https://en.wikipedia.org/wiki/Delta1_Gruis
  % Constellation Grus, Gruis, Gru
  % Right ascension       22h 29m 16.17481s[1]
  % Declination   −43° 29′ 44.0245″[1]
  % Apparent magnitude (V)        4.0 - 4.2[2]  

  star_name := "";
  greek_letter := "\delta^1";
  constellation_name_gen := "Gruis";
  constellation_name_abbrev := "Gru";
  
  star_label {Delta_One_Gruis, 0, .75, 0, medium_star_pen, true, star_name, false, 
       greek_letter, constellation_name_gen, constellation_name_abbrev,
       22, 29, 16.17481, -43, 29, 44.0245};
  
%% *** (3) 
  
  if false: % true
    drawarrow P205 -- P205 shifted (1, 0) with_color red;
    drawarrow P205 -- P205 shifted (0, 1) with_color blue;
    drawarrow P205 -- P205 shifted (0, 0, 1) with_color green;
  fi;
  
  sector_picture[23][-3] := current_picture;

  %t6 := identity rotated_around (mediate(p594, p595), mediate(p596, p597)) 5;
  %current_picture *= t6;
  
  t5 := align (origin -- (P205 * t6)) with_axis z_axis;
  current_picture *= t5;

  scale current_picture (100, 100, 100);
  rotate current_picture (0, 0, 180);
  shift current_picture (0, 165, -1125);

%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
