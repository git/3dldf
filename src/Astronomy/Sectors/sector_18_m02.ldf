%% sector_18_m02.ldf
%% Created by Laurence D. Finston (LDF) Mo 26. Aug 08:48:19 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 18:-2

beginfig(fig_ctr);

%% *** (3)

  s := "18_m02";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $18:-2$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
%% *** (3)

  p199 := p193 rotated (0, -15);
  p200 := p194 rotated (0, -15);
  
  
  label.bot("{\bigmath $17^{\rm{h}}$}", p193 shifted (0, -.15));
  label.bot("{\bigmath $18^{\rm{h}}$}", p199 shifted (0, -.15));

  label.lft("$-15^\circ$", p194);
  label.lft("$-30^\circ$", p193);

  label.rt("$-15^\circ$", p200);
  label.rt("$-30^\circ$", p199);

  label.urt("18:$-$2", p193) with_color blue;
  label.lrt("18:$-$2", p194) with_color blue;
  label.ulft("18:$-$2", p199) with_color blue;
  label.llft("18:$-$2", p200) with_color blue;
  
  if do_labels:
    dotlabel.bot("$p_{193}$", p193) with_color red;
    dotlabel.top("$p_{194}$", p194) with_color red;
    dotlabel.bot("$p_{199}$", p199) with_color red;
    dotlabel.top("$p_{200}$", p200) with_color red;
  fi;

%% ** (2)

  q54 := q52 rotated (0, -15);
  
  draw q54 with_pen big_pen with_color blue;

  sector_path[18][-2] := q54;
  
  if do_labels:
    label.ulft("$q_{54}$", get_point ((size q54) * 5 / 8) q54) with_text_color blue;
  fi;
      
%% ** (2) First point on the ecliptic
      
  if do_labels:
    dotlabel.lft("$p_{195}$", p195) with_text_color dark_green;
  fi;

%% ** (2) Second point on the ecliptic

  clear bpv;
  bpv := Ecliptic intersection_points celestial_meridian18;

  % show bpv;

  % message "p199:";
  % show p199;

  
  p201 := bpv1;

  if do_labels:
    dotlabel.rt("$p_{201}$", p201) with_text_color dark_green;
  fi;
    
  ang := p201 angle p195;

  q55 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p195 rotated_around (p34, p36) -i;
    q55 += temp_pt;
  endfor;

  q55 += p201;
  
  draw q55 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q55) / 2;
    label.urt("$q_{55}$", get_point (n) q55) with_color dark_green;
  fi;
    
%% ** (2) 

  % Theta Ophiuchi
  % Constellation   Ophiuchus
  % Right ascension 17h 22m 00.57935s[1]
  % Declination   −24° 59′ 58.3670″[1]
    
  
  p202 := p193 rotated (0, -(22/60 + 0.57935/3600)*15);
  p203 := p70 rotated ((24 + 59/60 + 58.3670/3600), 105);


  if do_labels:
    dotlabel.bot("$p_{202}$", p202) with_color red;
    dotlabel.lft("$p_{203}$", p203) with_color red;
  fi;

  p204 := p203 rotated (0, -(22/60 + 0.57935/3600)*15); % Rotate by RA - hours.

  %dotlabel.rt("$p_{204}$", p204) with_color red;

  Theta_Ophiuchi := p204;

  
  drawdot Theta_Ophiuchi with_pen big_star_pen;

  % Right ascension 17h 22m 00.57935
  % Declination   −24° 59′ 58.3670
  
  label("\starunnamed{\csname theta\endcsname}{Ophiuchi}{+17}{22}{00.5794}{-24}{59}{58.3670}", 
      Theta_Ophiuchi shifted (1, .75, 0));

  % Working on this macro.  LDF 2024.08.26.

  %star_label_unnamed {Theta_Ophiuchi, 17, 22, 0.5794, -24, 59, 58.3670};

%% ** (2)

  sector_center[18][-2] := sector_center[17][-2] rotated (0, -15);

  P20:= sector_center[18][-2];

  if do_labels and do_center_dots:
    dotlabel.lft("$P_{20}$", P20) with_color BurntOrange_rgb;
  fi;

%% ** (2)

  sector_picture[18][-2] := current_picture;

  rotate t0 (0, 13, 7);
  shift t0 (0, 30);
  
  scale current_picture (100, 100, 100);

  current_picture *= t0;

  shift current_picture (0, -200, -1100);

%% *** (3)    

  
endfig with_focus f;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
