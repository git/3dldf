%% sector_14_m06.ldf
%% Created by Laurence D. Finston (LDF) Do 24. Okt 08:39:30 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 14:-6.

beginfig(fig_ctr);

%% *** (3)

  s := "14_m06";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $14:-6$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q173 := q172 rotated (0, -15);

  sector_path[14][-6] := q173;

  draw q173 with_color blue with_pen big_pen;

  if do_labels:
    label.rt("$q_{173}$", get_point ((size q173)*9/24) q173) with_text_color blue;
  fi;

  p460 := p459 rotated (0, -15);

  P131 := P130 rotated (0, -15);

  sector_center[14][-6] := P131;
  
  if do_labels and do_center_dots:
    dotlabel.bot("$P_{131}$", P131) with_color BurntOrange_rgb;
  fi;

  if do_labels:
    dotlabel.bot("$p_{296}$", p296) with_color red;
    dotlabel.top("$p_{459}$", p459) with_color red;
    dotlabel.top("$p_{460}$", p460) with_color red;
  fi;

%% *** (3) Hour and declination labels
  
  label.top("{\bigmath $13^{\rm{h}}$}", p459 shifted (0, .2));
  label.top("{\bigmath $14^{\rm{h}}$}", p460 shifted (0, .2));

  label.lft("$-75^\circ$",  p459);
  label.rt("$-75^\circ$", p460);

  label.lft("$-90^\circ$", p296);

%% *** (3) HD 114533 == HR 4976

  % https://en.wikipedia.org/wiki/HD_114533
  % HD 114533, also known as HR 4976
  % Constellation 	Chamaeleon
  % Right ascension 	13h 14m 17.3297s[1]
  % Declination 	−78° 26′ 50.8362″[1]
  % Apparent magnitude (V) 	5.84±0.01[2]

  star_name := "HD 114533";
  greek_letter := "\deleteskip";
  constellation_name_gen := "Cham{\ae}leontis";
  constellation_name_abbrev := "Cha";

  star_label {HD114533, -.333, 0, -.333, medium_star_pen, true, star_name, false, 
      greek_letter, constellation_name_gen, constellation_name_abbrev,
      13, 14, 17.3297, -78, 26, 50.8362};

%% *** (3)  

  if false: % true 
    drawarrow P131 -- P131 shifted (1, 0) with_color red;
    drawarrow P131 -- P131 shifted (0, 1) with_color blue;
    drawarrow P131 -- P131 shifted (0, 0, 1) with_color green;
  fi;

  sector_picture[14][-6] := current_picture;

  t5 := align (origin -- P131) with_axis z_axis;

  rotate t5 (0, 0, 22.5);

  current_picture *= t5;
    
  scale current_picture (100, 100, 100);
  shift current_picture (0, -20, -1130);

%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
