%% sector_12_m02.ldf
%% Created by Laurence D. Finston (LDF) Mi 18. Sep 09:04:51 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 12:-2.

beginfig(fig_ctr);

%% *** (3)

  s := "12_m02";
  
  write_command {s};
  
%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $12:-2$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;



  
%% *** (3)    

  p348 := p283 rotated (0, 180+15);
  p349 := p284 rotated (0, 180+15);

  q107 := q82 rotated (0, 180+15);
  
  if do_labels:
    n := (size q107)/8;
    label.top("$q_{107}$", (get_point (n) q107)) with_color blue;
  fi;;

  draw q107 with_color blue with_pen big_pen;
  
  sector_path[12][-2] := q107;

  
  if do_labels:
    dotlabel.top("$p_{337}$", p337) with_color red;
    dotlabel.top("$p_{338}$", p338) with_color red;
    dotlabel.bot("$p_{348}$", p348) with_color red;
    dotlabel.bot("$p_{349}$", p349) with_color red;
  fi;

  P65 := P40 rotated (0, 180+15);

  sector_center[12][-2] := P65;
  
  if do_labels and do_center_dots:
    drawdot P65 with_pen dot_pen with_color BurntOrange_rgb;
    label.top("$P_{65}$", P65) with_color BurntOrange_rgb;
  fi;

  
%% *** (3) Hour and  declination labels
  
  label.bot("{\bigmath $11^{\rm{h}}$}", p348 shifted (0, -.2));
  label.bot("{\bigmath $12^{\rm{h}}$}", p349 shifted (0, -.2));

  label.lft("$-15^\circ$",  p337);
  label.rt("$-15^\circ$",  p338);
  label.lft("$-30^\circ$", p348);
  label.rt("$-30^\circ$", p349);


%% *** (3) Gamma_Crateris

  % https://en.wikipedia.org/wiki/Gamma_Crateris 
  % Right ascension       11h 24m 52.92362s[1]   
  % Declination   −17° 41′ 02.4300″[1]             
  % Apparent magnitude (V)        4.06[2]

  drawdot Gamma_Crateris with_pen medium_star_pen;

  s :=   "\starunnamed{\gamma}{Crateris}{+11}{24}{52.9236}{-17}{41}{02.4300}";
  
  label.bot(s, Gamma_Crateris shifted (0, -.125, 0));
  
%% *** (3)

  sector_picture[12][-2] := current_picture;
    
  scale current_picture (100, 100, 100);

  rotate t4 (105, 0);

  current_picture *= t4; % t4 is defined in sector_12_p01.ldf.  LDF 2024.09.16.

  shift current_picture (0, -210, -1100);

%% *** (3)

  
  
endfig with_focus f suppress_warnings;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
