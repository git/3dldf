%% sector_06_p02.ldf
%% Created by Laurence D. Finston (LDF) Do 22. Aug 18:59:56 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 6:2

beginfig(fig_ctr);

%% *** (3)

  s := "06_p02";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 6:2}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)


  p117 := p114 rotated (0, -15);
  p118 := p115 rotated (0, -15);
  
  label.bot("{\bigmath $5^{\rm{h}}$}", p114 shifted (0, -.15));
  label.bot("{\bigmath $6^{\rm{h}}$}", p117 shifted (0, -.15));
  
  label.lft("$15^\circ$", p114);
  label.rt("$15^\circ$", p117);

  label.lft("$30^\circ$", p115);
  label.rt("$30^\circ$", p118);

  label.urt("6:2", p114) with_color MidnightBlue_rgb;
  label.lrt("6:2", p115) with_color MidnightBlue_rgb;
  label.ulft("6:2", p117) with_color MidnightBlue_rgb;
  label.llft("6:2", p118) with_color MidnightBlue_rgb;


  if do_labels:
    dotlabel.lrt("$p_{114}$", p114) with_color red;
    dotlabel.top("$p_{115}$", p115) with_color red;
    dotlabel.llft("$p_{117}$", p117) with_color red;
    dotlabel.top("$p_{118}$", p118) with_color red;
  fi;

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p117 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p118 rotated (0, i);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -75);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-(i+15), -90);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;
  
  % drawarrow temp_path0 with_pen big_pen with_color red;
  % drawarrow temp_path1 with_pen big_pen with_color blue;
  % drawarrow temp_path2 with_pen big_pen with_color green;
  % drawarrow temp_path3 with_pen big_pen with_color orange;

  q24 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q24 += cycle;

  draw q24 with_pen big_pen with_color blue;

  sector_path[6][2] := q24;
  
  if do_labels:
    label.top("$q_{24}$", get_point ((size q24) * 5 / 8) q24) with_text_color blue;
  fi;

  
%% ** (2)

  if do_labels:
    dotlabel.lft("$p_{116}$", p116) with_color dark_green;
  fi;
  
  
%% ** (2)
  
  clear bpv;
  bpv := Ecliptic intersection_points c15;
  
  p119 := bpv0;

  if do_labels:
    dotlabel.rt("$p_{119}$", p119) with_color dark_green;
  fi;

  ang := p119 angle p116;
  
  q25 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p116 rotated_around (p34, p36) -i;
    q25 += temp_pt;
  endfor;

  q25 += p119;
  
  draw q25 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q25) / 2;
    label.ulft("$q_{25}$", get_point (n) q25) with_color dark_green;
  fi;

%% ** (2) Elnath.  Beta Tauri

  % Right ascension 	05h 26m 17.51312s[3]
  % Declination 	+28° 36′ 26.8262″[3]
  % Right ascension  
  
  ra   := (5 + 26/60 + 17.51312/3600) * 15;
  decl := (28 + 36/60 + 26.8262/3600);
  
  Elnath := (0, 0, -radius_val0) rotated (-decl, -ra);
  
  drawdot Elnath with_pen big_star_pen;

  label("\starnamed{Elnath}{\beta}{Tauri}{05}{26}{17.5131}{28}{36}{26.8262}", 
           Elnath shifted (-.2, -.5, -.05));

%% ** (2)

  sector_center[6][2] := sector_center[5][2] rotated (0, -15);

  P6 := sector_center[6][2];

  if do_labels and do_center_dots:
    dotlabel.bot("$P_6$", P6) with_color BurntOrange_rgb;
  fi;

%% ** (2)

  sector_picture[6][2] := current_picture;

  scale current_picture (100, 100, 100);
  rotate current_picture (57, 0);
  rotate current_picture (0, 180+72, -55.5);
  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  % clip current_picture to outer_frame;

  % unfill frame_fill_path_left;
  % unfill frame_fill_path_right;
  % unfill frame_fill_path_bot;

  % draw frame with_pen big_square_pen;
  % draw outer_frame with_pen big_square_pen;
  
%% *** (3)    

  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
