%% sector_06_p04.ldf
%% Created by Laurence D. Finston (LDF) Do 29. Aug 08:50:50 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 6:4

%% *** (3) 006 Capella
% alpha Aur (Aurigae) NH
% RA 05h 16m 41.35871
% Declination   +45° 59′ 52.7693″

beginfig(fig_ctr);

%% *** (3)
  
  s := "06_p04";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 6:4}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;


  
%% *** (3)

  p263 := p4 rotated (-45, -5*15);
  p264 := p263 rotated (0, -15);
  p265 := p4 rotated (-60, -5*15);
  p266 := p265 rotated (0, -15);

  P33 := p4 rotated (-(60-7.5), -5.5*15);

  if do_labels and do_center_dots:
    dotlabel.rt("$P_{33}$", P33) with_color BurntOrange_rgb;
  fi;



  
  if do_labels:
    dotlabel.bot("$p_{263}$", p263) with_color red;
    dotlabel.bot("$p_{264}$", p264) with_color red;
    dotlabel.top("$p_{265}$", p265) with_color red;
    dotlabel.top("$p_{266}$", p266) with_color red;
  fi;



  
%% ** (2)

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step 1 until 15:
    temp_pt := p264 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p266 rotated (0, i);
    temp_path1 += temp_pt;
    
    temp_pt := p70 rotated (-(i+45), -5*15);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-(i+45), -6*15);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path3;


  if false: % true: % 
    drawarrow temp_path0 with_pen big_pen with_color red;
    drawarrow temp_path1 with_pen big_pen with_color blue;
    drawarrow temp_path2 with_pen big_pen with_color green;
    drawarrow temp_path3 with_pen big_pen with_color orange;
  fi;

  
  q75 := temp_path0 -- temp_path2 -- temp_path1 -- temp_path3;
  q75 += cycle;


  draw q75 with_pen big_pen with_color Periwinkle_rgb;

  sector_path[6][4] := q75;

  if do_labels:
    label.top("$q_{75}$", get_point ((size q75) * 5 / 8) q75) with_text_color Periwinkle_rgb;
  fi;
  

  label.bot("{\bigmath $5^{\rm{h}}$}", p263 shifted (0, -.15));
  label.bot("{\bigmath $6^{\rm{h}}$}", p264 shifted (0, -.15));

  
  label.lft("$60^\circ$", p265);
  label.rt("$60^\circ$", p266);
  label.lft("$45^\circ$", p263);
  label.rt("$45^\circ$", p264);


  label.lrt("6:4", p265) with_color Periwinkle_rgb;
  label.llft("6:4", p266) with_color Periwinkle_rgb;
  label.urt("6:4", p263) with_color Periwinkle_rgb;
  label.ulft("6:4", p264) with_color Periwinkle_rgb;

%% ** (2) 006 Capella

  % alpha Aur (Aurigae) NH
  % RA 05h 16m 41.35871
  % Declination   +45° 59′ 52.7693″    
  % Apparent magnitude (V)  +0.08[3] (+0.03 – +0.16[4])

  drawdot Capella with_pen huge_star_pen;

  label.urt("\starnamed{Capella}{\alpha}{Aurig{\ae}}{05}{16}{41.3587}{45}{59}{52.7693}", 
      Capella);

%% ** (2)

  % temp_picture := cross_picture;
  % shift temp_picture by P33;
  % current_picture += temp_picture;

  
%% ** (2)
  

  sector_picture[6][4] := current_picture;

  t3 := align (origin -- P33) with_axis z_axis;
  rotate t3 (7.5, 0, 0);

  current_picture *= t3;
  
  scale current_picture (100, 100, 100);
  shift current_picture (0, -200, -1100);
  
%% *** (3)    

endfig with_focus f;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
