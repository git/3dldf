%% sector_02_p06.ldf
%% Created by Laurence D. Finston (LDF) Sa 14. Sep 08:21:50 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 2:6.

beginfig(fig_ctr);

%% *** (3)

  s := "02_p06";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $2:6$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  
%% *** (3)
  
  q95 := q84 rotated (0, -15);

  sector_path[2][6] := q95;

  draw q95 with_color blue with_pen big_pen;

  if do_labels:
    label.lft("$q_{95}$", get_point ((size q95) / 2) q95) with_text_color blue;
  fi;


  p309 := p288 rotated (0, -15);

  if do_labels:
    dotlabel.bot("$p_{288}$", p288) with_color red;
    dotlabel.top("$p_{289}$", p289) with_color red;
    dotlabel.bot("$p_{309}$", p309) with_color red;
  fi;


  P53 := P42 rotated (0, -15);

  sector_center[2][6] := P53;
  
  if do_labels and do_center_dots:
    dotlabel.bot("$P_{53}$", P53) with_color BurntOrange_rgb;
  fi;

  
%% *** (3) Hour and declination labels
  
  label.bot("{\bigmath $1^{\rm{h}}$}", p288 shifted (0, -.2));
  label.bot("{\bigmath $2^{\rm{h}}$}", p309 shifted (0, -.2));


  label.lft("$75^\circ$", p288);
  label.rt("$90^\circ$",  p289);

  label.rt("$75^\circ$", p309);

%% *** (3) 2_Ursae_Minoris.  Sector 2:6.

  drawdot Two_Ursae_Minoris with_pen medium_star_pen;
    
  % https://en.wikipedia.org/wiki/2_Ursae_Minoris
  % Constellation Cepheus
  % !! Not in Ursa Minor!
  % Right ascension       01h 08m 44.88005s[1]
  % Declination   86° 15′ 25.5240″[1]
  % Apparent magnitude (V)        4.244[2]

  label.bot("\starunnamed{2}{Ursae Minoris}{01}{08}{44.88005}{86}{15}{25.5240}", 
      Two_Ursae_Minoris shifted (0, -.333, 0));

%% *** (3)  

  sector_picture[2][6] := current_picture;
    
  scale current_picture (100, 100, 100);
  rotate current_picture (75, 180+7.5, -22);
  shift current_picture (0, -260, -1100);

%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
