%% sector_21_m02.ldf
%% Created by Laurence D. Finston (LDF) Mo 26. Aug 21:59:03 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 21:-2

beginfig(fig_ctr);

%% *** (3)

  s := "21_m02";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $21:-2$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;


  
%% *** (3)

  p214 := p208 rotated (0, -15);
  p215 := p209 rotated (0, -15);


  label.bot("{\bigmath $20^{\rm{h}}$}", p208 shifted (0, -.15));
  label.bot("{\bigmath $21^{\rm{h}}$}", p214 shifted (0, -.15));

  label.lft("$-30^\circ$", p208);
  label.lft("$-15^\circ$", p209);

  label.rt("$-30^\circ$", p214);
  label.rt("$-15^\circ$", p215);

  label.urt("21:$-$2", p208) with_color blue;
  label.lrt("21:$-$2", p209) with_color blue;

  label.ulft("21:$-$2", p214) with_color blue;
  label.llft("21:$-$2", p215) with_color blue;
  
  if do_labels:
    dotlabel.bot("$p_{208}$", p208) with_color red;
    dotlabel.top("$p_{209}$", p209) with_color red;
    dotlabel.bot("$p_{214}$", p214) with_color red;
    dotlabel.top("$p_{215}$", p215) with_color red;
  fi;


%% ** (2)

  q60 := q58 rotated (0, -15);
  
  draw q60 with_pen big_pen with_color blue;

  sector_path[21][-2] := q60;
  
  if do_labels:
    label.ulft("$q_{60}$", get_point ((size q60) * 5 / 8) q60) with_text_color blue;
  fi;


%% ** (2) First point on the ecliptic
      
  if do_labels:
    dotlabel.lft("$p_{210}$", p210) with_text_color dark_green;
  fi;

%% ** (2) Second point on the ecliptic

  clear bpv;
  bpv := Ecliptic intersection_points celestial_meridian21;

  % show bpv;
  % message "p214:";
  % show p214;

  
  p216 := bpv1;

  if do_labels:
    dotlabel.rt("$p_{216}$", p216) with_text_color dark_green;
  fi;



  ang := p216 angle p210;

  q61 += ..;
  
  for i = 0 step ang/120 until ang:
    temp_pt := p210 rotated_around (p34, p36) -i;
    q61 += temp_pt;
  endfor;

  q61 += p216;
  
  draw q61 with_color dark_green with_pen Big_pen;

  if do_labels:
    n := (size q61) / 2;
    label.ulft("$q_{61}$", get_point (n) q61) with_color dark_green;
  fi;

  
%% ** (2)
  
  sector_center[21][-2] := sector_center[20][-2] rotated (0, -15);

  P23:= sector_center[21][-2];

  if do_labels and do_center_dots:
    dotlabel.bot("$P_{23}$", P23) with_color BurntOrange_rgb;
  fi;

%% ** (2) Upsilon_Capricorni

  % Right ascension 	20h 40m 02.94518s[1]
  % Declination 	−18° 08′ 19.1664″[1]

  p217 := p70 rotated ((18 + 8/60 + 19.1664/3600), 60); % Decl. 

  p218 := p208 rotated (0, -(40/60 + 2.94518/3600)*15); % RA - hours
  p219 := p217 rotated (0, -(40/60 + 2.94518/3600)*15); % RA - hours

  % message "p199:";
  % show p199;
  
  % message "p217:";
  % show p217;

  %dotlabel.lft("$p_{219}$",  p219);

  Upsilon_Capricorni := p219;

  drawdot Upsilon_Capricorni with_pen big_star_pen;

  % Right ascension 	20h 40m 02.94518s[1]
  % Declination 	−18° 08′ 19.1664″[1]
  
  label("\starunnamed{\upsilon}{Capricorni}{+20}{40}{02.9452}{-18}{08}{19.1664}", 
      Upsilon_Capricorni shifted (0, -.75));

  if do_labels:
    dotlabel.llft("$p_{217}$", p217) with_color red;
    dotlabel.llft("$p_{218}$", p218) with_color red;
  fi;

%% ** (2)
  
   sector_picture[21][-2] := current_picture;

   rotate t0 (0, 13, 6.5);
   shift t0 (0, 30);



  
  scale current_picture (100, 100, 100);
  current_picture *= t0;
  shift current_picture (0, -200, -1100);
  
%% *** (3)    

  
endfig with_focus f;
fig_ctr += 1;

%% ** (2)

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
