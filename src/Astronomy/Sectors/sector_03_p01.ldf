%% sector_03_p01.ldf
%% Created by Laurence D. Finston (LDF) Mi 21. Aug 08:02:11 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 3:1

beginfig(fig_ctr);

%% *** (3)

  s := "03_p01";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  % draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector 3:1}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)

  label.lrt("3:1", p96) with_color teal_blue_rgb;
  
  label.llft("3:1", p97) with_color teal_blue_rgb;
  
  label.urt("3:1", p94) with_color teal_blue_rgb;
  label.ulft("3:1", p95) with_color teal_blue_rgb;

  for i = 0 upto 3:
    temp_path[i] := null_path;
  endfor;
  
  temp_path0 += ..;
  temp_path1 += ..;
  temp_path2 += ..;
  temp_path3 += ..;
  
  for i = 0 step .25 until 15:
    temp_pt := p95 rotated (0, i);
    temp_path0 += temp_pt;

    temp_pt := p97 rotated (0, i);
    temp_path1 += temp_pt;

    temp_pt := p70 rotated (-i, -45);
    temp_path2 += temp_pt;

    temp_pt := p70 rotated (-i, -30);
    temp_path3 += temp_pt;
  endfor;

  reverse temp_path1;
  reverse temp_path2;
  
  % drawarrow temp_path0 with_pen big_pen with_color red;
  % drawarrow temp_path1 with_pen big_pen with_color blue;
  % drawarrow temp_path2 with_pen big_pen with_color green;
  % drawarrow temp_path3 with_pen big_pen with_color orange;
  
  q17 := temp_path0 -- temp_path3 -- temp_path1 --- temp_path2;
  q17 += cycle;

  if do_labels:
    n := (size q17) * .57;
    label.top("$q_{17}$", get_point (n) q17) with_color teal_blue_rgb;
  fi; 
  
  draw q17 with_color blue with_pen big_pen; % Sector path 3:1
  sector_path[3][1] := q17;

   if do_labels:
     dotlabel.lrt("$p_{76}$", p76) with_color dark_green;
     dotlabel.top("$p_{92}$", p92) with_color dark_green;
     dotlabel.lrt("$p_{94}$", p94) with_color red;
     dotlabel.llft("$p_{95}$", p95) with_color red;
     dotlabel.top("$p_{96}$", p96) with_color red;
     dotlabel.top("$p_{97}$", p97) with_color red;
   fi;

   p98 := p87 rotated (0, -15);
   p99 := p86 rotated (0, -15);
   p100 := p87 rotated (0, -30);
   p101 := p86 rotated (0, -30);

   if do_labels:
     dotlabel.rt("$p_{98}$", p98) with_color red;
     dotlabel.rt("$p_{99}$", p99) with_color red;
     dotlabel.lft("$p_{100}$", p100) with_color red;
     dotlabel.lft("$p_{101}$", p101) with_color red;
   fi;
      
   label.llft("$0^\circ$", p94);
   label.lft("$5^\circ$", p99);
   label.lft("$10^\circ$", p98);
   label.llft("$15^\circ$", p96);

   label.lrt("$0^\circ$", p95);
   label.rt("$5^\circ$", p101);
   label.rt("$10^\circ$", p100);
   label.lrt("$15^\circ$", p97);
   
  label.bot("{\bigmath $2^{\rm{h}}$}", p94 shifted (0, -.15));
  label.bot("{\bigmath $3^{\rm{h}}$}", p95 shifted (0, -.15));

  % label.top("{\bigmath $2^{\rm{h}}$}", p96 shifted (0, .05));
  % label.top("{\bigmath $3^{\rm{h}}$}", p97 shifted (0, .05));
   
   draw q16 with_color dark_green with_pen Big_pen; % Section of ecliptic

   if do_labels:
     n := (size q16) / 2;
     label.ulft("$q_{16}$", get_point (n) q16) with_color dark_green;
   fi;

   % https://en.wikipedia.org/wiki/Gamma_Ceti
   % Gamma Ceti
   % RA 02h 43m 18.03910s[1]
   % Declination     +03° 14′ 08.9390″
   % Apparent magnitude (V) 	3.47[2] (3.56/6.63/10.16)

   drawdot Gamma_Ceti with_pen medium_star_pen;

   label.lft("{\bigmath$\gamma$} Ceti", Gamma_Ceti shifted (0, 0));
   
   s := "\setbox0=\hbox{{\smallrm Decl.}}"
     & "\vbox{"
     & "\hbox{{\smallrm \hbox to \wd0{RA\hfil}~02 43 18.0391}}\vskip-1pt"
     & "\hbox{{\smallrm Decl.~03 14 08.9390}}}";

   label.llft(s, Gamma_Ceti shifted (0, -.125));

   
%% *** (3)

  sector_center[3][1] := sector_center[2][1] rotated (0, -15);

  P2 := sector_center[3][1];

   if do_labels and do_center_dots:
       dotlabel.top("$P_2$", P2) with_color BurntOrange_rgb;
   fi;


%% *** (3)


  sector_picture[3][1] := current_picture;
  
  scale current_picture (100, 100, 100);
  rotate current_picture (0, 180+37.5);
  shift current_picture (0, -200, -1100);

  output current_picture with_focus f;
  clear current_picture;
  % clip current_picture to outer_frame;

  % unfill frame_fill_path_left;
  % unfill frame_fill_path_right;

  draw frame with_pen big_square_pen;
  % draw outer_frame with_pen big_square_pen;
  
%% *** (3)    


  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
