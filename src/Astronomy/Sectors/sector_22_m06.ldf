%% sector_22_m06.ldf
%% Created by Laurence D. Finston (LDF) Mi 30. Okt 10:15:27 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 22:-6.

beginfig(fig_ctr);

%% *** (3)

  s := "22_m06";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $22:-6$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q181 := q180 rotated (0, -15);

  sector_path[22][-6] := q181;

  draw q181 with_color blue with_pen big_pen;

  if do_labels:
    label.rt("$q_{181}$", get_point ((size q181)*9/24) q181) with_text_color blue;
  fi;

  p468 := p467 rotated (0, -15);

  P139 := P138 rotated (0, -15);

  sector_center[22][-6] := P139;
  
  if do_labels and do_center_dots:
    dotlabel.top("$P_{139}$", P139) with_color BurntOrange_rgb;
  fi;

  if do_labels:
    dotlabel.bot("$p_{296}$", p296) with_color red;
    dotlabel.top("$p_{467}$", p467) with_color red;
    dotlabel.top("$p_{468}$", p468) with_color red;
  fi;

%% *** (3) Hour and declination labels
  
  label.top("{\bigmath $21^{\rm{h}}$}", p467 shifted (0, .2));
  label.top("{\bigmath $22^{\rm{h}}$}", p468 shifted (0, .2));


  label.lft("$-75^\circ$",  p467);
  label.rt("$-75^\circ$", p468);

  label.lft("$-90^\circ$", p296);

%% *** (3) Nu Octantis

  % https://en.wikipedia.org/wiki/Nu_Octantis
  % Brightest star in Octans.
  % Constellation 	Octans
  % Right ascension 	21h 41m 28.64977s[1]
  % Declination 	−77° 23′ 24.1563″[1]
  % Apparent magnitude (V) 	3.73[2]
    
  star_name := "";
  greek_letter := "\csname nu\endcsname";
  constellation_name_gen := "Octantis";
  constellation_name_abbrev := "Oct";

  star_label {Nu_Octantis, -.3, 0, .3, medium_star_pen, true, star_name, false, 
      greek_letter, constellation_name_gen, constellation_name_abbrev,
      21, 41, 28.64977, -77, 23, 24.1563};
    
%% *** (3)  

  if false: % true 
    drawarrow P139 -- P139 shifted (1, 0) with_color red;
    drawarrow P139 -- P139 shifted (0, 1) with_color blue;
    drawarrow P139 -- P139 shifted (0, 0, 1) with_color green;
  fi;

  sector_picture[22][-6] := current_picture;

  t5 := align (origin -- P139) with_axis z_axis;

  rotate t5 (0, 0, 180);

  current_picture *= t5;
    
  scale current_picture (100, 100, 100);
  shift current_picture (0, -20, -1130);

%% *** (3)
  
endfig with_focus f;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
