%% sector_23_p01.ldf
%% Created by Laurence D. Finston (LDF) Fr 11. Okt 14:10:38 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 23:1.

beginfig(fig_ctr);

%% *** (3)

  s := "23_p01";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $23:1$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% *** (3)
  
  q146 := q145 rotated (0, -15);

  sector_path[23][1] := q146;

  draw q146 with_color blue with_pen big_pen;

  if do_labels:
    label.top("$q_{146}$", get_point ((size q146) *5/8) q146) with_text_color blue;
  fi;

  p416 := p414 rotated (0, -15);
  p417 := p415 rotated (0, -15);
  
  if do_labels:
    dotlabel.bot("$p_{414}$", p414) with_color red;
    dotlabel.bot("$p_{416}$", p416) with_color red;
    dotlabel.top("$p_{415}$", p415) with_color red;
    dotlabel.top("$p_{417}$", p417) with_color red;
  fi;

  P104 := P103 rotated (0, -15);

  sector_center[23][1] := P104;
  
  if do_labels and do_center_dots:
    drawdot P104 with_pen dot_pen with_color BurntOrange_rgb;
    label.bot("$P_{104}$", P104 shifted (0, 0)) with_color BurntOrange_rgb;
  fi;

%% *** (3) Hour and declination labels

  label.bot("{\bigmath $22^{\rm{h}}$}", p414 shifted (0, -.2));
  label.bot("{\bigmath $23^{\rm{h}}$}", p416 shifted (0, -.2));
  
  label.lft("$0^\circ$", p414);
  label.rt("$0^\circ$", p416);
  label.lft("$15^\circ$", p415);
  label.rt("$15^\circ$", p417);

    
%% *** (3) Homam

  % https://en.wikipedia.org/wiki/Zeta_Pegasi
  % Homam
  % Constellation 	Pegasus
  % Right ascension 	22h 41m 27.72072s[1]
  % Declination 	+10° 49′ 52.9079″[1]
  % Apparent magnitude (V) 	+3.414[2]

  drawdot Homam with_pen medium_star_pen;

  s := "\starnamed{Homam}{\zeta}{Pegasi}{22}{41}{27.7207}{10}{49}{52.9079}";

  label.lft(s, Homam shifted (.125, 0));

%% *** (3)
  
  if false: % true 
    drawarrow P104 -- P104 shifted (1, 0) with_color red;
    drawarrow P104 -- P104 shifted (0, 1) with_color blue;
    drawarrow P104 -- P104 shifted (0, 0, 1) with_color green;
  fi;

  sector_picture[23][1] := current_picture;

  rotate t5 (0, 15, -1.75); % -3.5
  
  current_picture *= t5;

  shift current_picture (0, -.25);
  
  scale current_picture (100, 100, 100);
  shift current_picture (0, -200, -1100);

  
%% *** (3)
  
endfig with_focus f suppress_warnings;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
