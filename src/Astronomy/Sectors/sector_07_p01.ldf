%% sector_07_p01.ldf
%% Created by Laurence D. Finston (LDF) Sa 5. Okt 09:08:43 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Sector 7:1.

beginfig(fig_ctr);

%% *** (3)

  s := "07_p01";
  
  write_command {s};

%% *** (3) Frame and labels

  if do_black:
    fill frame with_color black;
  else:
    if do_png:
      fill frame with_color white;
    fi;
  fi;
  
  %draw outer_frame with_pen big_square_pen;
  draw frame with_pen big_square_pen;

  label("{\largerm Sector {\Hugemath $7:1$}}", mediate(p2, p3) shifted (0, -1.5));
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;


  
  
%% *** (3)

  q133 := q89 rotated (0, -15);

  sector_path[7][1] := q133;

  draw q133 with_color blue with_pen big_pen;

  if do_labels:
    label.top("$q_{133}$", get_point ((size q133) *5/8) q133) with_text_color blue;
  fi;


  p390 := p297 rotated (0, -15);
  p391 := p298 rotated (0, -15);
  
  if do_labels:
    dotlabel.bot("$p_{297}$", p297) with_color red;
    dotlabel.bot("$p_{390}$", p390) with_color red;
    dotlabel.top("$p_{298}$", p298) with_color red;
    dotlabel.top("$p_{391}$", p391) with_color red;
  fi;

  P91 := P47 rotated (0, -15);

  sector_center[7][1] := P91;
  
  if do_labels and do_center_dots:
    dotlabel.bot("$P_{91}$", P91) with_color BurntOrange_rgb;
  fi;

%% *** (3) Hour and declination labels

  label.bot("{\bigmath $6^{\rm{h}}$}", p297 shifted (0, -.2));
  label.bot("{\bigmath $7^{\rm{h}}$}", p390 shifted (0, -.2));

  label.lft("$0^\circ$", p297);
  label.rt("$0^\circ$", p390);
  label.lft("$15^\circ$",  p298);
  label.rt("$15^\circ$",  p391);

%% *** (3) Alzirr

  % https://en.wikipedia.org/wiki/Xi_Geminorum
  % Alzirr
  % Constellation 	Gemini
  % Right ascension 	06h 45m 17.36432s[1]
  % Declination 	+12° 53′ 44.1311″[1]
  % Apparent magnitude (V) 	3.35[2]

  drawdot Alzirr with_pen big_star_pen;

  s := "\starnamed{Alzirr}{\xi}{Geminorum}{6}{45}{17.3643}{12}{53}{44.1311}";

  label.lft(s, Alzirr shifted (0, -.125, 0));

%% *** (3)

  sector_picture[7][1] := current_picture;
    
  t5 := align (origin -- P91) with_axis z_axis;

  shift t5 (0, 2);

  current_picture *= t5;

  scale current_picture (100, 100, 100);
  shift current_picture (0, -200, -1100);

  
%% *** (3)
  
endfig with_focus f suppress_warnings;
fig_ctr += 1;


endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
