#! /bin/bash

#### ttemp.sh
#### Created by Laurence D. Finston (LDF) Mon 06 Sep 2021 10:32:01 PM CEST

convert -delay 5 \
sinewaves_000.png \
sinewaves_001.png \
sinewaves_002.png \
sinewaves_003.png \
sinewaves_004.png \
sinewaves_005.png \
sinewaves_006.png \
sinewaves_007.png \
sinewaves_008.png \
sinewaves_009.png \
sinewaves_00.mp4

echo "Creating sinewaves_01.mp4"

convert -delay 5 \
sinewaves_010.png \
sinewaves_011.png \
sinewaves_012.png \
sinewaves_013.png \
sinewaves_014.png \
sinewaves_015.png \
sinewaves_016.png \
sinewaves_017.png \
sinewaves_018.png \
sinewaves_019.png \
sinewaves_01.mp4

echo "Creating sinewaves_02.mp4"

convert -delay 5 \
sinewaves_020.png \
sinewaves_021.png \
sinewaves_022.png \
sinewaves_023.png \
sinewaves_024.png \
sinewaves_025.png \
sinewaves_026.png \
sinewaves_027.png \
sinewaves_028.png \
sinewaves_029.png \
sinewaves_02.mp4

echo "Creating sinewaves_03.mp4"

convert -delay 5 \
sinewaves_030.png \
sinewaves_031.png \
sinewaves_032.png \
sinewaves_033.png \
sinewaves_034.png \
sinewaves_035.png \
sinewaves_036.png \
sinewaves_037.png \
sinewaves_038.png \
sinewaves_039.png \
sinewaves_03.mp4

echo "Creating sinewaves_04.mp4"

convert -delay 5 \
sinewaves_040.png \
sinewaves_041.png \
sinewaves_042.png \
sinewaves_043.png \
sinewaves_044.png \
sinewaves_045.png \
sinewaves_046.png \
sinewaves_047.png \
sinewaves_048.png \
sinewaves_049.png \
sinewaves_04.mp4

echo "Creating sinewaves_05.mp4"

convert -delay 5 \
sinewaves_050.png \
sinewaves_051.png \
sinewaves_052.png \
sinewaves_053.png \
sinewaves_054.png \
sinewaves_055.png \
sinewaves_056.png \
sinewaves_057.png \
sinewaves_058.png \
sinewaves_059.png \
sinewaves_05.mp4


echo "Creating sinewaves_06.mp4"

convert -delay 5 \
sinewaves_060.png \
sinewaves_061.png \
sinewaves_062.png \
sinewaves_063.png \
sinewaves_064.png \
sinewaves_065.png \
sinewaves_066.png \
sinewaves_067.png \
sinewaves_068.png \
sinewaves_069.png \
sinewaves_06.mp4

echo "Creating sinewaves_07.mp4"

convert -delay 5 \
sinewaves_070.png \
sinewaves_071.png \
sinewaves_072.png \
sinewaves_073.png \
sinewaves_074.png \
sinewaves_075.png \
sinewaves_076.png \
sinewaves_077.png \
sinewaves_078.png \
sinewaves_079.png \
sinewaves_07.mp4

echo "Creating sinewaves_08.mp4"

convert -delay 5 \
sinewaves_080.png \
sinewaves_081.png \
sinewaves_082.png \
sinewaves_083.png \
sinewaves_084.png \
sinewaves_085.png \
sinewaves_086.png \
sinewaves_087.png \
sinewaves_088.png \
sinewaves_089.png \
sinewaves_08.mp4

echo "Creating sinewaves_09.mp4"

convert -delay 5 \
sinewaves_090.png \
sinewaves_091.png \
sinewaves_092.png \
sinewaves_093.png \
sinewaves_094.png \
sinewaves_095.png \
sinewaves_096.png \
sinewaves_097.png \
sinewaves_098.png \
sinewaves_099.png \
sinewaves_09.mp4

convert -delay 5 \
sinewaves_100.png \
sinewaves_101.png \
sinewaves_102.png \
sinewaves_103.png \
sinewaves_104.png \
sinewaves_105.png \
sinewaves_106.png \
sinewaves_107.png \
sinewaves_108.png \
sinewaves_109.png \
sinewaves_10.mp4

echo "Creating sinewaves_11.mp4"

convert -delay 5 \
sinewaves_110.png \
sinewaves_111.png \
sinewaves_112.png \
sinewaves_113.png \
sinewaves_114.png \
sinewaves_115.png \
sinewaves_116.png \
sinewaves_117.png \
sinewaves_118.png \
sinewaves_119.png \
sinewaves_11.mp4

echo "Creating sinewaves_12.mp4"

convert -delay 5 \
sinewaves_120.png \
sinewaves_121.png \
sinewaves_122.png \
sinewaves_123.png \
sinewaves_124.png \
sinewaves_125.png \
sinewaves_126.png \
sinewaves_127.png \
sinewaves_128.png \
sinewaves_129.png \
sinewaves_12.mp4

echo "Creating sinewaves_13.mp4"

convert -delay 5 \
sinewaves_130.png \
sinewaves_131.png \
sinewaves_132.png \
sinewaves_133.png \
sinewaves_134.png \
sinewaves_135.png \
sinewaves_136.png \
sinewaves_137.png \
sinewaves_138.png \
sinewaves_139.png \
sinewaves_13.mp4

echo "Creating sinewaves_14.mp4"

convert -delay 5 \
sinewaves_140.png \
sinewaves_141.png \
sinewaves_142.png \
sinewaves_143.png \
sinewaves_144.png \
sinewaves_145.png \
sinewaves_146.png \
sinewaves_147.png \
sinewaves_148.png \
sinewaves_149.png \
sinewaves_14.mp4

echo "Creating sinewaves_15.mp4"

convert -delay 5 \
sinewaves_150.png \
sinewaves_151.png \
sinewaves_152.png \
sinewaves_153.png \
sinewaves_154.png \
sinewaves_155.png \
sinewaves_156.png \
sinewaves_157.png \
sinewaves_158.png \
sinewaves_159.png \
sinewaves_15.mp4


echo "Creating sinewaves_16.mp4"

convert -delay 5 \
sinewaves_160.png \
sinewaves_161.png \
sinewaves_162.png \
sinewaves_163.png \
sinewaves_164.png \
sinewaves_165.png \
sinewaves_166.png \
sinewaves_167.png \
sinewaves_168.png \
sinewaves_169.png \
sinewaves_16.mp4

echo "Creating sinewaves_17.mp4"

convert -delay 5 \
sinewaves_170.png \
sinewaves_171.png \
sinewaves_172.png \
sinewaves_173.png \
sinewaves_174.png \
sinewaves_175.png \
sinewaves_176.png \
sinewaves_177.png \
sinewaves_178.png \
sinewaves_179.png \
sinewaves_17.mp4

echo "Creating sinewaves_18.mp4"

convert -delay 5 \
sinewaves_180.png \
sinewaves_181.png \
sinewaves_182.png \
sinewaves_183.png \
sinewaves_184.png \
sinewaves_185.png \
sinewaves_186.png \
sinewaves_187.png \
sinewaves_188.png \
sinewaves_189.png \
sinewaves_18.mp4

echo "Creating sinewaves_19.mp4"

convert -delay 5 \
sinewaves_190.png \
sinewaves_191.png \
sinewaves_192.png \
sinewaves_193.png \
sinewaves_194.png \
sinewaves_195.png \
sinewaves_196.png \
sinewaves_197.png \
sinewaves_198.png \
sinewaves_199.png \
sinewaves_19.mp4

convert -delay 5 \
sinewaves_200.png \
sinewaves_201.png \
sinewaves_202.png \
sinewaves_203.png \
sinewaves_204.png \
sinewaves_205.png \
sinewaves_206.png \
sinewaves_207.png \
sinewaves_208.png \
sinewaves_209.png \
sinewaves_20.mp4

echo "Creating sinewaves_21.mp4"

convert -delay 5 \
sinewaves_210.png \
sinewaves_211.png \
sinewaves_212.png \
sinewaves_213.png \
sinewaves_214.png \
sinewaves_215.png \
sinewaves_216.png \
sinewaves_217.png \
sinewaves_218.png \
sinewaves_219.png \
sinewaves_21.mp4

echo "Creating sinewaves_22.mp4"

convert -delay 5 \
sinewaves_220.png \
sinewaves_221.png \
sinewaves_222.png \
sinewaves_223.png \
sinewaves_224.png \
sinewaves_225.png \
sinewaves_226.png \
sinewaves_227.png \
sinewaves_228.png \
sinewaves_229.png \
sinewaves_22.mp4

echo "Creating sinewaves_23.mp4"

convert -delay 5 \
sinewaves_230.png \
sinewaves_231.png \
sinewaves_232.png \
sinewaves_233.png \
sinewaves_234.png \
sinewaves_235.png \
sinewaves_236.png \
sinewaves_237.png \
sinewaves_238.png \
sinewaves_239.png \
sinewaves_23.mp4

echo "Creating sinewaves_24.mp4"

convert -delay 5 \
sinewaves_240.png \
sinewaves_241.png \
sinewaves_242.png \
sinewaves_243.png \
sinewaves_244.png \
sinewaves_245.png \
sinewaves_246.png \
sinewaves_247.png \
sinewaves_248.png \
sinewaves_249.png \
sinewaves_24.mp4

echo "Creating sinewaves_25.mp4"

convert -delay 5 \
sinewaves_250.png \
sinewaves_251.png \
sinewaves_252.png \
sinewaves_253.png \
sinewaves_254.png \
sinewaves_255.png \
sinewaves_256.png \
sinewaves_257.png \
sinewaves_258.png \
sinewaves_259.png \
sinewaves_25.mp4


echo "Creating sinewaves_26.mp4"

convert -delay 5 \
sinewaves_260.png \
sinewaves_261.png \
sinewaves_262.png \
sinewaves_263.png \
sinewaves_264.png \
sinewaves_265.png \
sinewaves_266.png \
sinewaves_267.png \
sinewaves_268.png \
sinewaves_269.png \
sinewaves_26.mp4

echo "Creating sinewaves_27.mp4"

convert -delay 5 \
sinewaves_270.png \
sinewaves_271.png \
sinewaves_272.png \
sinewaves_273.png \
sinewaves_274.png \
sinewaves_275.png \
sinewaves_276.png \
sinewaves_277.png \
sinewaves_278.png \
sinewaves_279.png \
sinewaves_27.mp4

echo "Creating sinewaves_28.mp4"

convert -delay 5 \
sinewaves_280.png \
sinewaves_281.png \
sinewaves_282.png \
sinewaves_283.png \
sinewaves_284.png \
sinewaves_285.png \
sinewaves_286.png \
sinewaves_287.png \
sinewaves_288.png \
sinewaves_289.png \
sinewaves_28.mp4

echo "Creating sinewaves_29.mp4"

convert -delay 5 \
sinewaves_290.png \
sinewaves_291.png \
sinewaves_292.png \
sinewaves_293.png \
sinewaves_294.png \
sinewaves_295.png \
sinewaves_296.png \
sinewaves_297.png \
sinewaves_298.png \
sinewaves_299.png \
sinewaves_300.png \
sinewaves_29.mp4

exit 0

## Local Variables:
## mode:shell-script
## abbrev-mode:t
## End:
