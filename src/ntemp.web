@q ** (2) numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary. @>

@ \§numeric primary> $\longrightarrow$ \.{GET\_BRANCH\_LENGTH} numeric_expression COMMA \§hyperbola primary>. 
\initials{LDF 2023.01.30.}

\LOG
\initials{LDF 2023.01.30.}
Added this rule.
\ENDLOG

@q *** (3) Definition.@> 

@<Define rules@>=
@=numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary@>
{
@q **** (4) @>

   @<Common declarations for rules@>@; 

#if DEBUG_COMPILE
   DEBUG = true; /* |false| */ @; 
   if (DEBUG) 
   {
     cerr_strm << thread_name << "*** Parser: `numeric_primary: GET_BRANCH_LENGTH numeric_expression COMMA hyperbola_primary'.";
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");
   }
#endif /* |DEBUG_COMPILE|  */

   Hyperbola *h = static_cast<Hyperbola*>(@=$4@>);

   int i = static_cast<int>(@=$2@>);

   if (h)
   {
      if (i == 0)
         @=$$@> = h->Path::size();
      else if (i == 1)
         @=$$@> = h->branch_1.size();
      else
      {
           cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_BRANCH_LENGTH "
                     << "numeric_expression COMMA hyperbola_primary':"
                     << endl
                     << "`numeric_expression' is neither 0 nor 1.  Invalid value:"
                     << endl
                     << "`numeric_expression' == " << @=$2@>
                     << endl 
                     << "Setting value of rule (`numeric_primary') to `INVALID_REAL' and continuing." 
                     << endl; 
     
           log_message(cerr_strm);
           cerr_message(cerr_strm);
           cerr_strm.str("");

           @=$$@> = INVALID_REAL;
      }
   }
   else
   {
     cerr_strm << thread_name << "ERROR!  In parser, rule `numeric_primary: GET_BRANCH_LENGTH "
               << "numeric_expression COMMA hyperbola_primary':"
               << endl
               << "Hyperbola is NULL or empty.  Can't return `Hyperbola::branch_length'." << endl
               << "Setting value of rule (`numeric_primary') to `INVALID_REAL' and continuing." 
               << endl; 
     
     log_message(cerr_strm);
     cerr_message(cerr_strm);
     cerr_strm.str("");

     @=$$@> = INVALID_REAL;
   }

};
