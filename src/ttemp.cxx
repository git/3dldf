#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <limits.h>
#include <limits>
#include <cfloat>

using namespace std;

int
main(int argc, char *arv[])
{
   printf("Entering `main'.\n");

   float f        = powf(2.0F, 1.0F/12.0F);
   double d       = pow(2.0D, 1.0D/12.0D);
   long double ld = powl(2.0L, 1.0L/12.0L);


   cout << setprecision(128)
        << "f ==  " << f << endl
        << "d ==  " << d << endl
        << "ld == " << ld << endl
     	<< "sizeof(f) == " << sizeof(f) << endl
	<< "sizeof(d) == " << sizeof(d) << endl
	<< "sizeof(ld) == " << sizeof(ld) << endl
	<< "sizeof(f) * 8 == " << (sizeof(f) * 8) << endl
	<< "sizeof(d) == " << sizeof(d) << endl
     	<< "sizeof(d) * 8 == " << (sizeof(d) * 8) << endl
	<< "sizeof(ld) == " << sizeof(ld) << endl
     	<< "sizeof(ld) * 8 == " << (sizeof(ld) * 8) << endl
	<< "FLT_DIG == " << FLT_DIG << endl
	<< "FLT_MANT_DIG == " << FLT_MANT_DIG << endl
	<< "DBL_DIG == " << DBL_DIG << endl
	<< "DBL_MANT_DIG == " << DBL_MANT_DIG << endl
	<< "LDBL_DIG == " << LDBL_DIG << endl
	<< "LDBL_MANT_DIG == " << LDBL_MANT_DIG << endl
        << "sizeof(unsigned int) * 8 (size of unsigned int in bits)  == " << (sizeof(unsigned int)*8) << endl
        << "sizeof(unsigned long) * 8 (size of unsigned long in bits) == " << (sizeof(unsigned long)*8)  << endl
        << "sizeof(unsigned long long) * 8 (size of unsigned long long in bits) == " << (sizeof(unsigned long long)*8)
        << endl
        << "ULLONG_MAX == " << ULLONG_MAX << endl 
	<< endl;


   // f ==  1.05946314334869384765625
   // d ==  1.0594630943592953098431053149397484958171844482421875
   // (length "1.0594630943592953098431053149397484958171844482421875") == 54
   // ld == 1.05946309435929526452345450504566315430565737187862396240234375
   // (length "1.05946309435929526452345450504566315430565737187862396240234375") == 64
   // Emacs-Lisp 1.0594630943592953
   
   return 0;
}
