%% 00INFO
%% Created by Laurence D. Finston (LDF) Mi 18. Okt 10:25:34 CEST 2023

%% * (1) Top

%% * (1)

Stardust.  Titles and audio only.

Words:  Mitchell Parish
Music:  Hoagy Carmichael
Published 1929.  Public domain.

This arrangement and video:
Copyright 2025 Laurence Finston

Instrumentation:

Tenor Ukulele
Soprano, alto, tenor and bass recorders
Melodica (two tracks)
Acoustic baritone guitar

The software used for this video was GNU 3DLDF, MetaPost, TeX, GIMP, ImageMagick, Audacity and Flowblade.

%% * (1)

Stardust.  Rotating Sphere Nr. 2.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf


%% * (1)

Mirror Images Nr. 2.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.


%% * (1)

Mirror Images Nr. 2.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.



%% * (1)

Stardust.  Diagonal Pan Cycle Nr. 3.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf



%% * (1)

Stardust.  Diagonal Pan Cycle Nr. 2.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The cycle consists of 200 images. The original scanned watercolor matte was reflected horizontally to create a second image and vertically to create a third. Then the original image, a copy of it, and the two reflected images were appended to each other to create a fourth. Finally, the fourth image is copied and attached to the former horizontally to create a fifth image. The size of the fifth image is 8000x4000 pixels. The images in the video (1728x982 pixels) were created by cutting out sections from the fifth image. This process starts at the lower left corner of the fifth image and each successive image is shifted by 20x10 pixels with respect to the preceding one. The panning is therefore at an angle of 22.5°.

Reflecting the images is necessary in order to avoid sharp borders within the cut-out images.  In the composite (fifth) image, the bottom left corner of the copy of the first image must lie at the top right corner of the first image.

Each image is displayed for two frames ("on twos") at 30 fps (frames per second).  A single cycle therefore takes 13 seconds plus 10 frames, i.e., 13 1/3 seconds.  In this video, there are 8 iterations of the cycle.

A diagonal pan is something that would be quite difficult to do by hand and is therefore rarely if ever seen in hand-drawn animation.

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf





%% * (1)

Stardust.  Diagonal Pan Cycle Nr. 1.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf




%% * (1)

Stardust.  Sequence Nr. 6.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf

%% * (1)

Stardust.  Sequence Nr. 5.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf



%% * (1)

Stardust.  Sequence Nr. 4.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf



%% * (1)

Stardust.  Sequence Nr. 3.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf


%% * (1)

Stardust.  Sequence Nr. 2.

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf





%% * (1)

Stardust.  Sequence Nr. 1

Author:  Laurence Finston

Copyright (C) 2024 Laurence Finston

The software used for this video was GNU 3DLDF, TeX, MetaPost, GIMP, ImageMagick and Flowblade.

The 3DLDF source code for this video can be found in this directory:  https://git.savannah.gnu.org/cgit/3dldf.git/tree/Videos/Stardust

An introduction to GNU 3DLDF:  https://www.gnu.org/software/3dldf/DOC/tb135finston-3dldf.pdf



