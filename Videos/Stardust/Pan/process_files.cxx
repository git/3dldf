/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Fr 20. Sep 20:37:33 CEST 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/

#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

// LDF 2023.11.13.
// Append images horizontally:
// convert +append Rubinrot_stripe.png Rubinrot_stripe_flipped.png R.png

#include "colordefs.hxx"


vector<string> cv;

int
main(int argc, char *argv[])
{
/* ** (2) */

   cv.push_back(red);              /*  0 S outline       */
   cv.push_back(green);            /*  1   fill          */

   cv.push_back(magenta);          /*  2 T outline       */
   cv.push_back(cyan);             /*  3   fill          */

   cv.push_back(orange);           /*  4 A outline       */
   cv.push_back(purple);           /*  5   fill          */
   cv.push_back(teal_blue);        /*  6   bowl outline  */ 
   cv.push_back(rose_madder);      /*  7   bowl fill     */

   cv.push_back(dark_green);       /*  8 R outline       */ 
   cv.push_back(violet);           /*  9   fill          */
   cv.push_back(sky_blue);         /* 10   bowl outline  */
   cv.push_back(apricot);          /* 11   bowl fill     */

   cv.push_back(bittersweet);      /* 12 D outline       */
   cv.push_back(plum);             /* 13   fill          */
   cv.push_back(emerald);          /* 14   bowl outline  */
   cv.push_back(periwinkle);       /* 15   bowl fill     */

   cv.push_back(peach);            /* 16 U outline       */
   cv.push_back(brick_red);        /* 17   fill          */

   cv.push_back(turquoise);        /* 18 S outline       */ 
   cv.push_back(dandelion);        /* 19   fill          */ 

   cv.push_back(salmon);           /* 20 T outline       */
   cv.push_back(cadet_blue);       /* 21   fill          */  

/* ** (2) */

   stringstream s;

   string zeroes;
   string zeroes_1;

   int status = 0;


   int j = 0;

   unsigned int shift_val = 0U;

   unsigned int shift_val_increment = 20;     

   bool set_shift_val_switch = true;

   j = 0;
   for (int i = 0; i <= 10; ++i, ++j)
   {

#if 1 // 0 
      if (set_shift_val_switch)
      {
         shift_val = shift_val_increment * i;

         set_shift_val_switch = false;
      }
#endif 

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s.str("");

#if 0
      s << "convert -transparent white stardust_" << zeroes << i << ".png "
        << "B.png; "
        << "composite B.png A" << zeroes_1 << j << ".png C.png;"
        << "convert -transparent black -transparent " << sky_blue << " C.png D.png;"
        << "composite D.png Lampenschwarz.png E.png;" 
        << "convert -fill black -opaque blue -transparent red E.png F.png;"
        << "composite F.png Lasurgoldgruen_1_coat_wc.png G" << zeroes << i << ".png;";


      s << "convert -crop 1726x982+" << shift_val << "+" << shift_val << " Scheveningen_greent_test_diag.png "
        << "P" << zeroes_1 << j << ".png;";


#endif
   
      s << "convert -flop P" << zeroes << i << ".png Q" << zeroes << i << ".png;";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      if (status != 0)
      {
         cerr << "status == " << status << endl
              << "Breaking." << endl;
         exit(1);
      }
      else
         cerr << "status == " << status << endl
              << "i      == " << i << endl;



      shift_val += shift_val_increment;

   }



   exit(0);

}

/* * (1) */

#if 0 


/* ** (2) */

      s << "convert -crop 1726x982+" << shift_val << "+0 K_combined.png "
        << "A" << zeroes << i << ".png;";



/* ** (2) */



      s << "cp E" << zeroes << i << ".png E" << zeroes_1 << j << ".png;";

#if 0

#endif 


/* ** (2) */


/* ** (2) */

      if (i < 171)
      {
        shift_val += shift_val_increment;
        continue;
      }
  
      if (set_shift_val_switch)
      {
         switch_val = shift_val_increment * i;
         set_shift_val_switch = false;
      }
      


      s << "convert -crop 1726x982+" << shift_val << "+0 " << bg_filename << " " 
        << "Paynesgrau_blaeulich_2_coats_horizontal_" << zeroes << i << ".png; ";

#endif 




/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */



   



