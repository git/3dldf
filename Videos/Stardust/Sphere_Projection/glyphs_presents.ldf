%% glyphs_presents.ldf
%% Created by Laurence D. Finston (LDF) Do 3. Okt 11:57:47 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)  

string font_name;
color font_color;
numeric_vector nv;

font_name  := "cmss10";
font_color := SkyBlue_rgb;

beginfig(fig_ctr);

%% ** (2)

  draw frame with_pen big_square_pen;

  if do_labels:
    dotlabel.urt("$p_0$", p0);
    dotlabel.ulft("$p_1$", p1);
    dotlabel.llft("$p_2$", p2);
    dotlabel.lrt("$p_3$", p3);
    dotlabel.top("$p_4$", p4);
    dotlabel.lft("$p_5$", p5);
    dotlabel.bot("$p_6$", p6);
    dotlabel.rt("$p_7$", p7);
    %dotlabel.lrt("$p_8$", p8);
  fi;
  
%% ** (2)  
  
  p9 := p3 shifted (3, -14);

  glyph L_letter;
  
  letter (L_letter) {font_name, font_color, 76, 1, 0, 10, nv, p9, 0, 0, false, % true, % 
    2, 2,
    0, 0,
    -.5, 2,
    0, 0,
    0, -3,
    0, 0,
    false, false, false};

  for i = 0 upto 3:
     p[15+i] := mediate(get_point (i) q1, get_point ((i+1) mod 4) q1);
  endfor;

  if do_labels:
    dotlabel.bot("$p_{15}$", p15);
    dotlabel.rt("$p_{16}$", p16);
    dotlabel.bot("$p_{17}$", p17);
    dotlabel.lft("$p_{18}$", p18);
  fi;
  

%% ** (2)  
  
  glyph S_letter;
  
  letter (S_letter) {font_name, font_color, 83, 1, 2, 19, nv, p11, 8, 0, false, % true, % 
    9, 10,
    0, 0,
    -.5, 2,
    0, 0,
    0, -3,
    0, 0,
    false, false, false};

  % for i = 0 upto ((size q2) - 1):
  %   dotlabel.top(decimal i, get_point (i) q2) with_color red;
  % endfor;

  clear qv;

  qv := resolve q2 to 100;

  q4 := qv0;

  clear_connectors q4;
  q4 += --;
  
  

%% ** (2)  
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

endinput;

%% * (1)
  


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
