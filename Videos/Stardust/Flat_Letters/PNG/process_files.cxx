/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

stringstream s;

#include "colordefs.hxx"
#include "process_files_functions.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int shift_val = 10;




vector<string> color_vector_0;

/* ** (2) Main */

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;



   color_vector_0.push_back(cyan);         
   color_vector_0.push_back(magenta);       

   color_vector_0.push_back(yellow);        
   color_vector_0.push_back(orange);        

   color_vector_0.push_back(brown);         
   color_vector_0.push_back(purple);        

   color_vector_0.push_back(pink);          
   color_vector_0.push_back(violet);        

   color_vector_0.push_back(apricot);       
   color_vector_0.push_back(blue_violet);   

   color_vector_0.push_back(blue_green);    
   color_vector_0.push_back(brick_red);     

   color_vector_0.push_back(burnt_orange);  
   color_vector_0.push_back(cadet_blue);    

   color_vector_0.push_back(carnation_pink);
   color_vector_0.push_back(aquamarine);

/* ** (2) */

#if 0

   color_vector_1.push_back(bittersweet);         
#endif



/* ** (2) */

   int i = 0;
   int j = 0;
   int m = 0;

/* ** (2) */

   for (int i = 513; i <= 802; ++i)
   {
/* *** (3) */

         if (i < 10)
            zeroes = "000";
         else if (i < 100)
            zeroes = "00";
         else if (i < 1000)
            zeroes = "0";
         else
            zeroes = "";

         s.str("");

/*  convert -transparent 'rgb(0, 255, 0)' -transparent blue sphere_projection_0070.png A.png */
/* convert -transparent blue sphere_projection_1070.png C.png */
/*  composite A.png C.png D.png */



         s << "convert -transparent red sphere_projection_" << (1000+i) << ".png A.png;"
           << "composite A.png ./Backgrounds/Scheveningen_blue_deep_2_coats_wc.png B.png;"
           << "convert -transparent blue B.png C.png;"
           << "composite C.png ./Backgrounds/Lampenschwarz.png D.png;";

         if (i < 13)
           s << "mv D.png Z" << zeroes << i << ".png;";
         else
          {

              s << "convert -transparent " << color_vector_0[0] 
                << " -transparent " << color_vector_0[1] 
                << " sphere_projection_" << zeroes << i << ".png E.png;"
                << "composite E.png ./Backgrounds/Ruegener_Kreide_2_coats_nat.png F.png;"

                << "convert -transparent " << color_vector_0[2] 
                << " -transparent " << color_vector_0[3] 
                << " F.png G.png;"
                << "composite G.png ./Backgrounds/Neapelgelb_1_coat_wc.png H.png;"

                << "convert -transparent " << color_vector_0[4] 
                << " -transparent " << color_vector_0[5] 
                << " H.png I.png;"
                << "composite I.png ./Backgrounds/Scheveningen_green_wc.png J.png;"

                << "convert -transparent " << color_vector_0[6] 
                << " -transparent " << color_vector_0[7] 
                << " J.png K.png;"
                << "composite K.png ./Backgrounds/Rotorange_gouache.png L.png;"

                << "convert -transparent " << color_vector_0[8] 
                << " -transparent " << color_vector_0[9] 
                << " L.png M.png;"
                << "composite M.png ./Backgrounds/Lasurgoldgruen_1_coat_wc.png N.png;"

                << "convert -transparent " << color_vector_0[10] 
                << " -transparent " << color_vector_0[11] 
                << " N.png O.png;"
                << "composite O.png ./Backgrounds/Englischrot_gouache.png P.png;"

                << "convert -transparent " << color_vector_0[12] 
                << " -transparent " << color_vector_0[13] 
                << " P.png Q.png;"
                << "composite Q.png ./Backgrounds/Indischgelb_2_coats_wc.png R.png;"



                << "convert -transparent blue R.png S.png;"
                << "composite S.png D.png Z" << zeroes << i << ".png;";

         }

         cerr << "s.str() == " << s.str() << endl;

         status = system(s.str().c_str());

         cerr << "`status' == " << status << endl;

         if (status != 0)
         {
             exit(1);
         }   

/* **** (4) */

   }  /* |for|  */

/* ** (2) */

   return 0;

}  /* End of |main| definition  */



/* ** (2) */


#if 0 

/* ** (2) */

   int j = 201;
   int m = 201;

   for (int i = 68; i <= 240; i++)
   {
   // Indischgelb_2_coats_wc.png
   // Pariserblau_2_coats_wc.png
   // Hookersgruen_1_1_coat_wc.png
   // Drachenblut_1_coat_nat.png

      s.str("");


      s << "composite C" << zeroes << i << ".png ";

      if (i < 60)
        s << "Indischgelb_2_coats_wc.png ";
      else if (i < 120)
        s << "Pariserblau_2_coats_wc.png ";
      else if (i < 180)
        s << "Hookersgruen_1_1_coat_wc.png ";
      else 
        s << "Drachenblut_1_coat_nat.png ";

      s << "D.png;"
        << "convert -transparent " << mulberry << " D.png E.png;";

      for (int k = 0; k < 3; ++k, ++j, ++m)
      {

         j %= 201;

         if (j < 10)
            zeroes_1 = "000";
         else if (j < 100)
            zeroes_1 = "00";
         else if (j < 1000)
            zeroes_1 = "0";
         else
            zeroes_1 = "";

         if (m < 10)
            zeroes_2 = "000";
         else if (m < 100)
            zeroes_2 = "00";
         else if (m < 1000)
            zeroes_2 = "0";
         else
            zeroes_2 = "";

         s << "composite E.png A" << zeroes_1 << j << ".png "
           << "F" << zeroes_2 << m << ".png;";


      }  /* inner |for|  */

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
   
   }  /* outer |for|  */

/* ** (2) */


      s << "convert -fill " << mulberry << " -opaque white sphere_" << zeroes << i << ".png "
        << "B.png;"
        << "convert -transparent cyan -transparent " << jungle_green 
        << " -transparent " << sky_blue
        << " -transparent " << yellow
        << " -transparent " << salmon << " -transparent " << goldenrod 
        << " -transparent " << green
        << " -transparent " << plum << " -transparent " << emerald 
        << " -transparent " << purple 
        << " -transparent blue -transparent " << dark_green
        << " -transparent magenta "
        << " -transparent " << violet << " -transparent " << periwinkle
        << " -transparent " << orange << " -transparent " << apricot
        << " -transparent " << bittersweet << " -transparent " << brick_red
        << " -transparent " << peach << " -transparent " << purple
        << " -transparent " << red
        << " -transparent " <<  rose_madder 
        << " -transparent " <<  turquoise_dvips
        << " -transparent " <<  dandelion
        << " -transparent " <<  cadet_blue
        << " -transparent " <<  teal_blue << " B.png C" << zeroes << i << ".png;";



/* ** (2) */

   s << "convert -crop 1726x982+" << (i*shift_val) << "+" << (i*shift_val) << " -gravity NorthEast "
     << "S0002.png A" << zeroes << i << ".png;";

/* ** (2) */

#endif




/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


