%% a.ldf
%% Created by Laurence D. Finston (LDF) So 14. Jul 19:45:59 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

%% * (1) "a" Set up sphere.

pickup big_pen;

% do_labels     := true; % false; % 
% % do_help_lines := true; % false; %
% do_originals  := true; % false; % 

% if do_png:
%   do_labels := false;
%   do_help_lines := false;
% fi; 


beginfig(fig_ctr);

%% ** (2)

  message "fig_ctr:  " & decimal fig_ctr;
  
  draw frame with_pen big_square_pen with_color frame_color;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  draw S0 with_color sphere_color;

  % message "q0:";
  % show q0;
  % pause;

  % message "p10:";
  % show p10;
  % pause;
  
  Q152 := q0 shifted by (origin - p10);
  Q153 := q1 shifted by (origin - p10);

  shift Q152 (0, 0, -8);
  shift Q153 (0, 0, -8);

  % n := (size Q152) - 1;
  % if do_labels:
  %   for i = 0 upto n:
  %     dotlabel.top(decimal i, get_point (i) Q152) with_color red;
  %   endfor;
  % fi;

  n := (size Q153) - 1;
  if do_labels:
    for i = 0 upto n:
      dotlabel.top(decimal i, get_point (i) Q153) with_color red;
    endfor;
  fi;

  
  if do_originals:
    filldraw Q152 with_fill_color cyan;
    unfilldraw Q153;
  fi;


%% ** (2)  
  
  clear qv;

  qv := resolve Q152 (0, 6) to 200;

  Q154 := qv0;

  shift Q154 (0, 0, -8);

  if do_originals:
    draw Q154 with_color blue with_pen Big_pen;
  fi;

%% ** (2)

  clear qv;

  qv := resolve Q152 (6, 9) to 300;

  Q155 := qv0;

  shift Q155 (0, 0, -8);

  if do_originals:
    draw Q155 with_color green with_pen Big_pen;
  fi;

%% ** (2)  
  
  clear qv;

  qv := resolve Q152 (9, 12) to 200;

  Q156 := qv0;
  clear_connectors Q156;
  Q156 += --;

  shift Q156 (0, 0, -8);

  if do_originals:
    draw Q156 with_color orange with_pen Big_pen;
  fi;


  
%% ** (2)

  Q157 := Q154 .. Q155 .. Q156;
  Q157 += cycle;

  
  if do_originals:
    draw Q157 with_color magenta with_pen Big_pen;
  fi;
  


%% ** (2)

  n := (size Q157) - 1;
  
  for i = 0 upto n:
    clear bpv;
    bpv := (origin -- get_point (i) Q157) intersection_points S0;
    temp_pt0 := bpv0;
    temp_pt1 := bpv1;
    if zpart temp_pt0 < zpart temp_pt1:
      Q158 += temp_pt0;
    else:
      Q158 += temp_pt1;
    fi;
  endfor;

  Q158 += cycle;
  
  filldraw Q158 with_pen Big_pen with_fill_color Peach_rgb;


%% ** (2)

  clear qv;

  qv := resolve Q153 to 300;

  Q159 := qv0;

  shift Q159 (0, 0, -8);

  if do_originals:
    draw Q159 with_color blue with_pen Big_pen;
  fi;


%% ** (2)

  n := (size Q159) - 1;
  
  for i = 0 upto n:
    clear bpv;
    bpv := (origin -- get_point (i) Q159) intersection_points S0;
    temp_pt0 := bpv0;
    temp_pt1 := bpv1;
    if zpart temp_pt0 < zpart temp_pt1:
      Q160 += temp_pt0;
    else:
      Q160 += temp_pt1;
    fi;
  endfor;

  Q160 += cycle;
  
  filldraw Q160 with_pen Big_pen with_fill_color cyan;

%% ** (2)

  v35 := current_picture;
  
  shift current_picture (0, 5, 20);
  
  output current_picture with_focus f;
  clear current_picture;
  
  draw frame with_pen big_square_pen with_color frame_color;

endfig with_projection parallel_x_y;
fig_ctr += 1;

endinput;

%% * (1) Loop

for loop_ctr = 15 step 15 until 360:
  beginfig(fig_ctr);

    message "loop_ctr: " & decimal loop_ctr;
    message "fig_ctr:  " & decimal fig_ctr;
    
    v36 := v35;
    rotate v36 (0, loop_ctr);

    current_picture += v36;

    shift current_picture (0, 5, 20);
    
    output current_picture with_focus f;
    clear current_picture;
    
    draw frame with_pen big_square_pen;

    message "";
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
endfor;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
