/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

stringstream s;

#include "colordefs.hxx"
#include "process_files_functions.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int shift_val = 10;



vector<string> color_vector_1;

int
reflect(string filename);


int
combine(string in_filename, string out_filename);

int
combine(string in_filename, string out_filename)
{
   int status = 0;

   cerr << "Entering `combine'." << endl;

   stringstream s;

   s << "rm -f " << out_filename << ".png;"
     << "convert +append " << in_filename << ".png " << in_filename << ".png A.png;"
     << "convert -append A.png A.png " << out_filename << ".png;"
     << "rm A.png;";

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "status == " << status << endl;

   if (status != 0)
      exit(1);


   cerr << "Exiting `combine' successfully with return value 0." << endl;

   return 0;

}




int
reflect(string in_filename, string out_filename)
{

   int status = 0;

   cerr << "Entering `reflect'." << endl;

   stringstream s;

   string zeroes;

   s << "rm -f ";

   for (int i = 0; i <= 3; ++i)
   {
      s << out_filename << "0" << i << ".png ";
   }

   s << ";";

   s << "convert -flop " << in_filename << ".png A.png;"

     << "convert +append " << in_filename << ".png A.png B.png;"

     << "convert -flip B.png C.png;"

     << "convert -append B.png C.png " << out_filename << "00.png;"

     << "convert -append C.png B.png " << out_filename << "01.png;"

     << "convert +append A.png " << in_filename << ".png B.png;"

     << "convert -flip B.png C.png;"

     << "convert -append B.png C.png " << out_filename << "02.png;"

     << "convert -append C.png B.png " << out_filename << "03.png;"

     << "rm A.png B.png C.png;";


   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "status == " << status << endl;

   if (status != 0)
      exit(1);

   cerr << "Exiting `reflect' successfully with return value 0." << endl;

   return 0;

}


/* ** (2) Main */

// Frame size:  1726x982

int main(int argc, char *argv[])
{
/* ** (2) */


   int status = 0;

   stringstream s;

   int i = 0;
   int j = 0;
   int m = 0;

   stringstream in_strm; 
   stringstream out_strm;

/* ** (2) */


   for (i = 0; i <= 3; ++i)
   {
      s << "convert -scale 982x982 V000" << i << ".png A.png;"
        << "composite -gravity Center A.png frame_black_border_black_bg.png W000" << i << ".png;";

      s << "convert -scale 982x982 V001" << i << ".png A.png;"
        << "composite -gravity Center A.png frame_black_border_black_bg.png W001" << i << ".png;";

      s << "convert -scale 982x982 V002" << i << ".png A.png;"
        << "composite -gravity Center A.png frame_black_border_black_bg.png W002" << i << ".png;";

      s << "convert -scale 982x982 V003" << i << ".png A.png;"
        << "composite -gravity Center A.png frame_black_border_black_bg.png W003" << i << ".png;";

      s << "rm A.png;";

   }

   cerr << "s.str() == " << s.str() << endl;


   status = system(s.str().c_str());

   cerr << "status == " << status << endl;








/* ** (2) */

   return 0;

}  /* End of |main| definition  */


/* * (1) */


#if 0 

/* ** (2) */

      s << "convert -rotate 90 V000" << i << ".png V002" << i << ".png;"
        << "convert -rotate 90 V001" << i << ".png V003" << i << ".png;";

/* ** (2) */

   // status = reflect("violet_00", "V00");
   // status = combine("V0000", "V0010");


   for (int i = 1; i <= 3; ++i)
   {
      in_strm.str("");
      out_strm.str("");

      in_strm  << "V000" << i; 
      out_strm << "V001" << i; 

      status = combine(in_strm.str().c_str(), out_strm.str().c_str());
   }

/* ** (2) */
      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

/* ** (2) */


/* ** (2) */
#endif




/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


