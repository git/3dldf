/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>

using namespace std;

stringstream s;

#include "colordefs.hxx"
#include "process_files_functions.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int shift_val = 10;



vector<string> color_vector_1;

int
reflect(string filename);


int
combine(string in_filename, string out_filename);

int
combine(string in_filename, string out_filename)
{
   int status = 0;

   cerr << "Entering `combine'." << endl;

   stringstream s;

   s << "convert +append " << in_filename << ".png " << in_filename << ".png A.png;"
     << "convert -append A.png A.png " << out_filename << ".png";

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "status == " << status << endl;

   if (status != 0)
      exit(1);


   cerr << "Exiting `combine' successfully with return value 0." << endl;

   return 0;




}




int
reflect(string filename, int ctr)
{

   int status = 0;

   cerr << "Entering `reflect'." << endl;

   stringstream s;

   s << "rm -f " ;

   for (int i = ctr+1; i <= ctr+9; ++i)
      s << filename << "000" << i << ".png ";
  
   s << ";";

   s << "convert -flop " << filename << "000" << ctr << ".png " << filename << "000" << (ctr+1) << ".png;"
     << "convert +append " << filename << "000" << ctr << ".png " 
     << filename << "000" << (ctr+1) << ".png " << filename << "000" << (ctr+2) << ".png;"

     << "convert -flip " << filename << "000" << (ctr+2) << ".png " 
     << filename << "000" << (ctr+3) << ".png;"

     << "convert -append " << filename << "000" << (ctr+2) << ".png " 
     << filename << "000" << (ctr+3) << ".png " << filename << "000" << (ctr+4) << ".png;"

     << "convert -append " << filename << "000" << (ctr+3) << ".png " 
     << filename << "000" << (ctr+2) << ".png " << filename << "000" << (ctr+5) << ".png;"

     << "convert +append " << filename << "000" << (ctr+1) << ".png " 
     << filename << "000" << ctr << ".png " << filename << "000" << (ctr+6) << ".png;"

     << "convert -flip " << filename << "000" << (ctr+6) << ".png " 
     << filename << "000" << (ctr+7) << ".png;"

     << "convert -append " << filename << "000" << (ctr+6) << ".png " 
     << filename << "000" << (ctr+7) << ".png " << filename << "000" << (ctr+8) << ".png;"

     << "convert -append " << filename << "000" << (ctr+7) << ".png " 
     << filename << "000" << (ctr+6) << ".png " << filename << "000" << (ctr+9) << ".png;"

     << "rm " << filename << "0001.png " << filename << "0002.png "
     << filename << "0003.png " << filename << "0006.png "
     << filename << "0007.png;";


   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "status == " << status << endl;

   if (status != 0)
      exit(1);


   cerr << "Exiting `reflect' successfully with return value 0." << endl;

   return 0;

}


/* ** (2) Main */

// Frame size:  1726x982

int main(int argc, char *argv[])
{
/* ** (2) */


   int status = 0;

   stringstream s;

   int i = 0;
   int j = 0;
   int m = 0;

/* ** (2) */

#if 0 

   status = reflect("G", 0); 
   status = combine("G0004", "H0004");
   status = combine("G0005", "H0005");
   status = combine("G0008", "H0008");  
   status = combine("G0009", "H0009");          
#endif 

   for (i = 4; i <= 9; ++i)
   {
      if (i > 5 && i < 8)
         continue;

      s << "convert -scale 982x982 H000" << i << ".png A.png;"
        << "composite -gravity Center A.png frame_black_border_black_bg.png I000" << i << ".png;";


      status = system(s.str().c_str());

   }






/* ** (2) */

   return 0;

}  /* End of |main| definition  */



/* ** (2) */


#if 0 

#endif




/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


