/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

#if 0 

  Q0 with_draw_color red with_fill_color green;    % S

  Q1 with_draw_color cyan with_fill_color magenta; % T

  Q2 with_draw_color purple with_fill_color orange;   % A outer
  Q3 with_draw_color pink with_fill_color dark_green; % A inner

  Q4 with_draw_color teal_blue_rgb with_fill_color rose_madder_rgb; % R outer
  Q5 with_draw_color Periwinkle_rgb with_fill_color Melon_rgb;      % R inner
  
  Q6 with_draw_color Mahogany_rgb with_fill_color Apricot_rgb;      % D outer
  Q7 with_draw_color Salmon_rgb with_fill_color Goldenrod_rgb;      % D outer

  Q8 with_draw_color RawSienna_rgb with_fill_color Emerald_rgb;   % U

  Q9 with_draw_color ForestGreen_rgb with_fill_color Peach_rgb;   % S

  Q10 with_draw_color SkyBlue_rgb with_fill_color Dandelion_rgb;   % T

convert XXX.png -unique-colors -depth 8 txt:
# ImageMagick pixel enumeration: 6,1,255,srgb
0,0: (0,0,0)  #000000  black
1,0: (0,225,0)  #00E100  srgb(0,225,0)
2,0: (255,181,40)  #FFB528  srgb(255,181,40)
3,0: (0,0,255)  #0000FF  blue
4,0: (15,227,255)  #0FE3FF  srgb(15,227,255)
5,0: (255,255,255)  #FFFFFF  white


#endif 

string zeroes;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   for (int i = 1; i <= 1; i++)
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";


   s << "convert -fill black -opaque red -opaque " << green << " "
     << "-opaque cyan -opaque magenta "
     << "-opaque " << purple << " -opaque " << orange << " "
     << "-opaque " << pink << " "
     << "-opaque " << teal_blue << " -opaque " << rose_madder << " "
     << "-opaque " << periwinkle << " "
     << "-opaque " << mahogany << " -opaque " << apricot << " "
     << "-opaque " << salmon << " "
     << "-opaque " << raw_sienna << " -opaque " << emerald << " "
     << "-opaque " << forest_green << " -opaque " << peach << " "
     << "-opaque " << sky_blue << " -opaque " << dandelion << " "
     << "-fill white -opaque " << dark_green << " -opaque " << melon << " " 
     << "-opaque " << goldenrod << " "
     << "stardust_" << zeroes << i << ".png A.png;";

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "`status' == " << status << endl;

   if (status != 0)
   {
       exit(1);
   }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */


#if 0 


      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "composite pretty_girl_" << zeroes << i << ".png pretty_girl_"
        << zeroes_1 << j << ".png A" << zeroes << i << ".png";


      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
  }

#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


