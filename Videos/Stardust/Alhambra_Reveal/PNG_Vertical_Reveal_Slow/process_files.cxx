/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   int j = 0;
   int k = 147;
   for (int i = 0; i <= 147; ++i, ++j, --k) 
   {
      s.str("");

      if (i < 10)
         zeroes = "00";
      else if (i < 100)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      if (k < 10)
         zeroes_2 = "00";
      else if (k < 100)
         zeroes_2 = "0";
      else
         zeroes_2 = "";

#if 0 
      s << "composite A" << zeroes << i << ".png ./Backgrounds/presents_cr.png "
        << "B" << zeroes << i << ".png;";
#endif 

      s << "composite A" << zeroes_2 << k << ".png ./Backgrounds/the_end.png "
        << "C" << zeroes_1 << j << ".png;";


      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */




#if 0 

     s << "composite alhambra_207_1_" << zeroes << i << ".png ./Backgrounds/frame_black_green.png A.png;"
       << "convert -transparent " << orange << " A.png B.png;"
       << "composite B.png ./Backgrounds/Krapplack_Rose_wc.png C.png;"
       << "convert -transparent blue C.png D.png;"
       << "composite D.png ./Backgrounds/Pariserblau_2_coats_wc.png E.png;"
       << "convert -transparent " << teal_blue << " E.png F.png;"
       << "composite F.png ./Backgrounds/Dioxazine_Mauve_2_coats_wc.png G.png;"
       << "convert -transparent " << purple << " G.png H.png;"
       << "composite H.png ./Backgrounds/Indischgelb_2_coats_wc.png I.png;"
       << "convert -transparent cyan I.png J.png;"
       << "composite J.png ./Backgrounds/Saftgruen_2_wc.png K.png;"
       << "convert -transparent white K.png L.png;"
       << "composite L.png ./Backgrounds/Pale_gray_Ingres_Paper.png M.png;"
       << "composite ./Backgrounds/frame_black_trans.png M.png N.png;"
       << "convert -transparent " << green << " N.png A" << zeroes << i << ".png;";











#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


