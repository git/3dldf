#### Makefile
#### Created by Laurence D. Finston (LDF) Sa 12. Okt 14:33:07 CEST 2024

#### * Copyright and License.

#### This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
#### Copyright (C) 2024 The Free Software Foundation, Inc.

#### GNU 3DLDF is free software; you can redistribute it and/or modify 
#### it under the terms of the GNU General Public License as published by 
#### the Free Software Foundation; either version 3 of the License, or 
#### (at your option) any later version. 

#### GNU 3DLDF is distributed in the hope that it will be useful, 
#### but WITHOUT ANY WARRANTY; without even the implied warranty of 
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#### GNU General Public License for more details. 

#### You should have received a copy of the GNU General Public License 
#### along with GNU 3DLDF; if not, write to the Free Software 
#### Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

#### GNU 3DLDF is a GNU package.  
#### It is part of the GNU Project of the  
#### Free Software Foundation 
#### and is published under the GNU General Public License. 
#### See the website http://www.gnu.org 
#### for more information.   
#### GNU 3DLDF is available for downloading from 
#### http://www.gnu.org/software/3dldf/LDF.html. 

#### Please send bug reports to Laurence.Finston@@gmx.de
#### The mailing list help-3dldf@gnu.org is available for people to 
#### ask other users for help.  
#### The mailing list info-3dldf@gnu.org is for sending 
#### announcements to users. To subscribe to these mailing lists, send an 
#### email with "subscribe <email-address>" as the subject.  

#### The author can be contacted at: 

#### Laurence D. Finston 
#### c/o Free Software Foundation, Inc. 
#### 51 Franklin St, Fifth Floor 
#### Boston, MA  02110-1301  
#### USA

#### Laurence.Finston@gmx.de 


## * (1) Top.

.PHONY: mp dvi eps pdf png fmp fdvi feps fpdf fpng pf prog 

prog: process_files

pf: process_files
	./process_files

process_files: process_files.cxx colordefs.hxx
	g++ -o $@ $<

mp: alhambra_207_1.mp
png eps: alhambra_207_1_000.eps
dvi: alhambra_207_1.dvi
pdf: alhambra_207_1.pdf

alhambra_207_1.pdf: alhambra_207_1.dvi
	dvipdfmx alhambra_207_1.dvi

alhambra_207_1.ps: alhambra_207_1.dvi
	dvips -o alhambra_207_1.ps alhambra_207_1.dvi

alhambra_207_1.dvi: alhambra_207_1.tex alhambra_207_1_000.eps 
	tex alhambra_207_1.tex

alhambra_207_1.mp: 3dldf$(EXEEXT) alhambra_207_1.ldf sub_alhambra_207_1.ldf horizontal_reveal.ldf 
	3dldf$(EXEEXT) --no-database alhambra_207_1.ldf

alhambra_207_1_000.eps: alhambra_207_1.mp
	mpost -numbersystem="double" alhambra_207_1.mp

.PHONY: purge

purge:
	rm -f mp_input.* mp_output.* *.dvi *.log *.mf *.mpx mpxerr.*

