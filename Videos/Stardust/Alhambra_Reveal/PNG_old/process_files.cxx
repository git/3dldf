/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   int j = 0;
   for (int i = 1; i <= 4; ++i, ++j)
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

#if 0 
      s << "composite H" << zeroes << i << ".png frame_black_border_green.png R" << zeroes << i << ".png;"
        << "convert R" << zeroes << i << ".png S" << zeroes << i << ".png;";
#endif 

      s << "composite S" << zeroes << i << ".png P" << zeroes << i << ".png T" << zeroes << i << ".png;"
        << "convert -transparent " << green << " T" << zeroes << i << ".png U" << zeroes << i << ".png;";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */


#if 0 

 
    "convert -fill red -opaque white -opaque black -opaque cyan G" << zeroes << i << ".png "
        << "-opaque blue -opaque " << teal_blue << " -opaque " << orange 
        << " -opaque " << purple << " "
        << "R" << zeroes << i << ".png";



      s << "composite H" << zeroes << i << ".png frame_green_border_red.png A.png;"

        << "convert -transparent white A.png B.png;"
        << "composite B.png ./Backgrounds/Ruegener_Kreide_2_coats_nat.png C.png;"

        << "convert -transparent blue C.png D.png;"
        << "composite D.png ./Backgrounds/Heliotuerkis_gouache.png E.png;"

        << "convert -transparent cyan E.png F.png;"
        << "composite F.png ./Backgrounds/Kobaltblau_hell_gouache.png G.png;"

        << "convert -transparent " << purple << " G.png H.png;"
        << "composite H.png ./Backgrounds/Violett_plus_Zinkweiss.png I.png;"

        << "convert -transparent " << orange << " I.png J.png;"
        << "composite J.png ./Backgrounds/Indischgelb_gouache.png K.png;"

        << "convert -transparent " << teal_blue << " K.png L.png;"
        << "composite L.png ./Backgrounds/Zinnobergruen_gouache.png M.png;"

        << "convert -transparent red M.png N.png;"
        << "composite N.png the_end.png Q" << zeroes_1 << j << ".png;";





Gouache:

Heliotuerkis_gouache.png
Indischgelb_gouache.png
Kobaltblau_hell_gouache.png
Pale_gray_Ingres_Paper.png
Violett_plus_Zinkweiss.png
Watercolor
Zinnobergruen_gouache.png

Watercolor:

Tried using with gouache, but it was too strong:  Krapplack_Rose_wc.png

Ruegener_Kreide_2_coats_nat.png
Pariserblau_2_coats_wc.png
Preussischblau_1_coat_wc.png
Cobalt_violet_dark_1_coat.png
Indischgelb_2_coats_wc.png
Maigruen_2_coats_600dpi_wc.png
#endif 

#if 0 

      s << "convert -scale 1726x982 G" << zeroes << i << ".png H" << zeroes << i << ".png;";

      s << "convert -crop 2010x1142+" << (10*i) << "+0 A.png B.png;"
        << "convert -crop 2010x1142+" << (10*i) << "+0 -gravity East C.png D.png;"
        << "composite B.png D.png E.png;"
        << "convert -transparent " << green << " E.png F.png;"
        << "composite alhambra_207_1_000.png F.png G" << zeroes << i << ".png;";



#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


