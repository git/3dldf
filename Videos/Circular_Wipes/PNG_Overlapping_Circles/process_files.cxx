/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

#include "colordefs.hxx"

string str[3];

void create_masks(string str0, string str1, string str2);



// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

string zeroes;
string zeroes_1;
string zeroes_2;

stringstream s;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;


   int j = 0;
   for (int i = 224; i >= 0; --i, ++j)  // 0 225
   {

      s.str("");

      if (i < 10)
         zeroes = "00";
      else if (i < 100)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "00";
      else if (j < 100)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

      s << "cp B" << zeroes << i << ".png C" << zeroes_1 << j << ".png;";

      cerr << "s.str() == " << s.str() << endl;

      status = system(s.str().c_str());

      cerr << "`status' == " << status << endl;

      if (status != 0)
      {
          exit(1);
      }   
   
   }  /* |for|  */


  return 0;

}

#if 0 
      s << "composite combined_" << zeroes << i << ".png ./Backgrounds/Graphitgrau_1_coat_wc.png "
        << "A.png;" 
        << "convert -transparent red A.png B.png;"
        << "composite B.png ./Backgrounds/Kadmiumrot_mittel_2_coats_wc.png C.png;"
        << "convert -transparent blue C.png D.png;"
        << "composite D.png ./Backgrounds/Preussischblau_2_coats_wc.png E.png;"
        << "convert -transparent " << green << " E.png F.png;"
        << "composite F.png ./Backgrounds/Maigruen_1_coat_wc.png G.png;"
        << "convert -transparent cyan G.png H.png;"
        << "composite H.png ./Backgrounds/Cobalt_violet_dark_2_coats_wc.png I.png;"
        << "convert -transparent magenta I.png J.png;"
        << "composite J.png ./Backgrounds/Turners_Gelb_2_coats_wc.png K.png;"
        << "convert -transparent " << rose_madder << " K.png L.png;"
        << "composite L.png ./Backgrounds/Pariserblau_1_coat_wc.png M.png;"
        << "convert -transparent " << teal_blue << " M.png N.png;"
        << "composite N.png ./Backgrounds/Arches_140lb_wc_paper.png "
        << "B" << zeroes << i << ".png;";
#endif 

#if 0 
   for (int i = 3; i <= 678; i+=3, ++j)  
      s.str("");
      s << "circular_wipes_" << zeroes << i << ".png";
      str[0] = s.str();


      if ((i+1) < 10)
         zeroes_2 = "00";
      else if ((i+1) < 100)
         zeroes_2 = "0";
      else
         zeroes_2 = "";

      s.str("");
      s << "circular_wipes_" << zeroes_2 << (i+1) << ".png";
      str[1] = s.str();

      if ((i+2) < 10)
         zeroes_2 = "00";
      else if ((i+2) < 100)
         zeroes_2 = "0";
      else
         zeroes_2 = "";

      s.str("");
      s << "circular_wipes_" << zeroes_2 << (i+2) << ".png";
      str[2] = s.str();

      s.str("");

#if 1 // 0 
      create_masks(str[0], str[1], str[2]);
#endif 

      // Do not reset s.str(() here!  create_masks fills it.  LDF 2025.02.16. */

      // Combine

     /* Combine masks  */
 
     s << "composite mask_blue_green_isect.png mask_red_blue_green_isect.png A.png;"
       << "composite A.png mask_red_blue_isect.png B.png;"
       << "composite B.png mask_red_green_isect.png C.png;"
       << "composite C.png mask_blue_minus_red_and_green.png D.png;"
       << "composite D.png mask_green_minus_red_and_blue.png E.png;"
       << "composite E.png mask_red_minus_blue_and_green.png F.png;"
       << "mv F.png combined_" << zeroes_1 << j << ".png;";

#if 0 
     /* Convert colors to "color mixtures".  */

      s << "convert -fill " << yellow << " -opaque magenta combined.png A.png;"
        << "convert -fill magenta -opaque cyan A.png B.png;" 
        << "convert -fill cyan -opaque " << rose_madder << " B.png C.png;"
        << "convert -fill " << medium_dark_gray << " -opaque white C.png D.png;"
        << "convert -fill white -opaque " << teal_blue << " D.png E.png;"
        << "convert -transparent white circles_drawn.png F.png;"
        << "composite F.png E.png combined_mixture.png;";
#endif 

#endif 


/* * (1) create_masks  */

void 
create_masks(string str0, string str1, string str2)
{
      s.str("");

      s << "rm -f mask*.png;"
        << "convert -transparent cyan " << str[0] << " mask_red_circle.png;"
        << "convert -transparent red " << str[0] << " mask_red_circle_not.png;"
        << "convert -transparent magenta " << str[1] << " mask_blue_circle.png;"
        << "convert -transparent blue " << str[1] << " mask_blue_circle_not.png;"
        << "convert -transparent " << orange << " " << str[2] << " mask_green_circle.png;"
        << "convert -transparent " << green << " " << str[2] << " mask_green_circle_not.png;";

      // mask_red_minus_blue.png Top circle 1 (red/cyan)

      s << "composite mask_blue_circle.png mask_red_circle.png G.png;";
      s << "composite mask_red_circle_not.png G.png  I.png;";
      s  << "convert -transparent cyan I.png J.png;";
      s  << "convert -fill cyan -opaque blue J.png mask_red_minus_blue.png;";

      // mask_red_minus_green.png Top circle 2 (red/magenta)

      s << "composite mask_green_circle.png mask_red_circle.png G.png;";
      s << "composite mask_red_circle_not.png G.png  I.png;";
      s  << "convert -transparent cyan I.png J.png;";
      s  << "convert -fill magenta -opaque " << green << " J.png mask_red_minus_green.png;";

      // mask_blue_minus_red.png Lower left circle 1 (blue/orange)

      s << "composite mask_red_circle.png mask_blue_circle.png G.png;"
        << "composite mask_blue_circle_not.png G.png  I.png;"
        << "convert -transparent magenta I.png J.png;"
        << "convert -fill orange -opaque red J.png mask_blue_minus_red.png;";

      // mask_blue_minus_green.png Lower left circle 2 (blue/rose_madder)

      s << "composite mask_green_circle.png mask_blue_circle.png G.png;"
        << "composite mask_blue_circle_not.png G.png  I.png;"
        << "convert -transparent magenta I.png J.png;"
        << "convert -fill " << rose_madder << " -opaque " << green << " J.png mask_blue_minus_green.png;";

      // mask_green_minus_red.png Lower right circle 1 (green/violet)

      s << "composite mask_red_circle.png mask_green_circle.png G.png;"
        << "composite mask_green_circle_not.png G.png  I.png;"
        << "convert -transparent orange I.png J.png;"
        << "convert -fill " << violet << " -opaque red J.png mask_green_minus_red.png;";

      // mask_green_minus_blue.png Lower right circle 2 (green/periwinkle)

      s << "composite mask_blue_circle.png mask_green_circle.png G.png;"
        << "composite mask_green_circle_not.png G.png  I.png;"
        << "convert -transparent orange I.png J.png;"
        << "convert -fill " << periwinkle << " -opaque blue J.png mask_green_minus_blue.png;";

      // mask_red_minus_blue_and_green.png

      s << "convert -transparent red mask_red_minus_blue.png A.png;"
        << "convert -transparent red mask_red_minus_green.png B.png;"
        << "composite A.png B.png C.png;"
        << "composite C.png mask_red_circle.png D.png;"
        << "convert -transparent cyan -transparent magenta D.png mask_red_minus_blue_and_green.png;";

      s << "convert -transparent blue mask_blue_minus_red.png A.png;" 
        << "composite A.png mask_blue_minus_green.png B.png;"
        << "convert -transparent orange -transparent " << rose_madder << " B.png "
        << "mask_blue_minus_red_and_green.png;";

      s << "convert -transparent " << green << " mask_green_minus_red.png A.png;" 
        << "composite A.png mask_green_minus_blue.png B.png;"
        << "convert -transparent " << violet << " -transparent " << periwinkle << " B.png "
        << "mask_green_minus_red_and_blue.png;";

      /* isects  */

      /* blue-green isect  */

      s << "convert -transparent " << blue << " mask_blue_minus_green.png A.png;" 
        << "composite mask_red_circle.png A.png B.png;"
        << "convert -transparent red B.png " << "mask_blue_green_isect.png;";

      /* red-blue-green isect  */

      s << "convert -transparent " << blue << " -fill " << teal_blue << " "
        << "-opaque " << rose_madder << " mask_blue_minus_green.png A.png;" ;
      s << "composite mask_blue_green_isect.png A.png B.png;";
      s << "convert -transparent " << rose_madder << " B.png mask_red_blue_green_isect.png;";

      /* red-blue isect */

      s << "convert -fill blue -opaque red mask_red_minus_blue_and_green.png A.png;";
      s << "composite A.png mask_red_circle.png B.png;";
      s << "convert -transparent blue B.png C.png;";
      s << "composite mask_green_circle.png C.png D.png;"
        << "convert -transparent " << green << " D.png E.png;";
      s << "convert -fill cyan -opaque red E.png mask_red_blue_isect.png;";

      /* red-green isect  */

#if 0
      /* Repeat of code for previous isect.  */
      s << "convert -fill blue -opaque red mask_red_minus_blue_and_green.png A.png;";
      s << "composite A.png mask_red_circle.png B.png;";
      s << "convert -transparent blue B.png C.png;";
#endif 

      s << "composite mask_blue_circle.png C.png D.png;"
        << "convert -transparent blue D.png E.png;";
      s << "convert -fill magenta -opaque red E.png mask_red_green_isect.png;";

   return;

}  /* End of create_masks definition  */






/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


