/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 20. Feb 10:03:25 CET 2025 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>


using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

vector<string> color_vector_A;

string zeroes;
string zeroes_1;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

/* ** (2) */

   color_vector_A.push_back("red");
   color_vector_A.push_back("blue");
   color_vector_A.push_back(dark_green);
   color_vector_A.push_back(violet);
   color_vector_A.push_back(orange);
   color_vector_A.push_back("magenta");

/* ** (2) */

   for (int i = 11; i <= 479; i++) // 0 -- 479
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

   s << "convert -blur 0x12 the_end_blue_blur_" << zeroes << i << ".png A.png;"
     << "convert -blur 0x12 the_end_red_blur_" << zeroes << i << ".png B.png;";

   if (i <= 222)
      s << "composite A.png B.png C.png;"; 
   else    
      s << "composite B.png A.png C.png;";

   s << "composite C.png frame_black_black.png Blue_Red_Blur_Only_" << zeroes << i << ".png;";

   /* Outlines on blur  */

   s << "composite the_end_blue_" << zeroes << i << ".png A.png D.png;"
     << "composite the_end_red_" << zeroes << i << ".png B.png E.png;";

   if (i <= 222)
      s << "composite D.png E.png F.png;";
   else
      s << "composite E.png D.png F.png;";

   s << "composite F.png frame_black_black.png Blue_Red_Outline_and_Blur_" << zeroes << i << ".png";


#if 0 

   s << "convert -transparent white "
     << "-fill red "
     << "-opaque " << magenta << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_" << zeroes << i << ".png A.png;"
     << "convert -transparent white "
     << "-fill red "
     << "-opaque " << magenta << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_blur_" << zeroes << i << ".png B.png;"
     << "convert -blur 0x12 B.png C.png;"
     << "composite A.png C.png D.png;"
     << "composite D.png frame_black_black.png "
     << "./Red/Red_" << zeroes << i << ".png;";


   s << "convert -transparent white "
     << "-fill " << green << " "
     << "-opaque " << magenta << " "
     << "-opaque " << red << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_" << zeroes << i << ".png A.png;"
     << "convert -transparent white "
     << "-fill " << green << " "
     << "-opaque " << red << " "
     << "-opaque " << magenta << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_blur_" << zeroes << i << ".png B.png;"
     << "convert -blur 0x12 B.png C.png;"
     << "composite A.png C.png D.png;"
     << "composite D.png frame_black_black.png "
     << "./Green/Green_" << zeroes << i << ".png;";

   s << "convert -transparent white "
     << "-fill " << yellow << " "
     << "-opaque " << magenta << " "
     << "-opaque " << red << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_" << zeroes << i << ".png A.png;"
     << "convert -transparent white "
     << "-fill " << yellow << " "
     << "-opaque " << red << " "
     << "-opaque " << magenta << " "
     << "-opaque " << dark_green << " "
     << "-opaque " << violet << " "
     << "-opaque " << orange << " "
     << "-opaque " << blue << " "
     << "the_end_blur_" << zeroes << i << ".png B.png;"
     << "convert -blur 0x12 B.png C.png;"
     << "composite A.png C.png D.png;"
     << "composite D.png frame_black_black.png "
     << "./Yellow/Yellow_" << zeroes << i << ".png;";

#endif 



   
   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "`status' == " << status << endl;

   if (status != 0)
   {
       exit(1);
   }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */

#if 0 

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

   s << "convert -transparent white the_end_" << zeroes << i << ".png A.png;" 
     << "convert -transparent white the_end_blur_" << zeroes << i << ".png B.png;" 
     << "convert -blur 0x12 B.png C.png;"
     << "composite A.png C.png D.png;"
     << "composite D.png frame_black_black.png A" << zeroes << i << ".png;";

   s << "convert -fill black ";

   for (vector<string>::iterator iter = color_vector_A.begin();
        iter != color_vector_A.end();
        ++iter)
   {
         s << " -opaque " << *iter << " ";
   }

   s << "the_end_" << zeroes << i << ".png A" << zeroes << i << ".png;";

   s << "convert -blur 0x12 the_end_" << zeroes << i << ".png B" << zeroes << i << ".png;";

#endif 


/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


