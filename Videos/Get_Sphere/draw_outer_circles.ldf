%% draw_outer_circles.ldf
%% Created by Laurence D. Finston (LDF) So 8. Dez 15:50:05 CET 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Loops

%% ** (2) circle 3, magenta

q3 += ..;

p20 := (get_normal c3) shifted by p16;
p21 := get_point 0 c3;

for loop_ctr = 0 step 5 until 360:
  beginfig(fig_ctr);

%% ** (2)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;      
    fi;

    draw frame with_pen big_square_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% ** (2)

    
    current_picture := v3;

    % Inner circles
    if do_draw_inner_circles_A:
      draw c0 with_color red with_pen big_pen;
      draw c1 with_color green with_pen big_pen;
      draw c2 with_color cyan with_pen big_pen;
    fi; 
    
    temp_pt := p21 rotated_around (p16, p20) loop_ctr;

    q3 += temp_pt;

    draw q3 with_color magenta with_pen big_pen;

    shift current_picture (0, 0, 1);

  endfig with_focus f;
  fig_ctr += 1;
endfor;


q3 += cycle; % circle 3, magenta

%% ** (2) circle 4, orange 

q4 += ..;

p22 := (get_normal c4) shifted by p16;
p23 := get_point 0 c4;

for loop_ctr = 0 step 5 until 360:
  beginfig(fig_ctr);

%% ** (2)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;      
    fi;

    draw frame with_pen big_square_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% ** (2)

    current_picture := v3;

    % Inner circles
    if do_draw_inner_circles_A:
      draw c0 with_color red with_pen big_pen;
      draw c1 with_color green with_pen big_pen;
      draw c2 with_color cyan with_pen big_pen;
    fi; 

    draw q3 with_color magenta with_pen big_pen;
    
    temp_pt := p23 rotated_around (p16, p22) loop_ctr;

    q4 += temp_pt;

    draw q4 with_color orange with_pen big_pen;

    shift current_picture (0, 0, 1);

  endfig with_focus f;
  fig_ctr += 1;
endfor;

q4 += cycle; % circle 4, orange


%% ** (2) circle 5, dark_green 

q5 += ..;

p24 := (get_normal c5) shifted by p16;
p25 := get_point 0 c5;

for loop_ctr = 0 step 5 until 360:
  beginfig(fig_ctr);

%% ** (2)
    
    if do_black:
      fill frame with_color black;
    else:
      if do_png:
	fill frame with_color white;
      fi;      
    fi;

    draw frame with_pen big_square_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% ** (2)

    current_picture := v3;

    % Inner circles
    if do_draw_inner_circles_A:
      draw c0 with_color red with_pen big_pen;
      draw c1 with_color green with_pen big_pen;
      draw c2 with_color cyan with_pen big_pen;
    fi; 

    draw q3 with_color magenta with_pen big_pen;
    draw q4 with_color orange with_pen big_pen;
    
    temp_pt := p25 rotated_around (p16, p24) loop_ctr;

    q5 += temp_pt;

    draw q5 with_color dark_green with_pen big_pen;

    shift current_picture (0, 0, 1);

  endfig with_focus f;
  fig_ctr += 1;
endfor;

q5 += cycle; % circle 5, dark_green






%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

