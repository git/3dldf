%% titles.ldf
%% Created by Laurence D. Finston (LDF) Mi 14. Aug 18:35:52 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

% Dimensions:
% Green Triangle = a, a, b (a are the longer sides)
% Yellow Links = a
% Horizontal Distance between Ground Joints = 2b
  
input "plainldf.lmc";

medium_pen := pencircle scaled (.333mm, .333mm, .333mm);

point p[];

numeric frame_wd;
numeric frame_ht;

numeric outer_frame_wd;
numeric outer_frame_ht;

frame_wd := 600mm;
frame_ht := 337.5mm;

frame_wd += 8mm;
frame_ht += 8mm;

outer_frame_wd := frame_wd + 12cm;
outer_frame_ht := frame_ht + 12cm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));


rectangle outer_frame;

outer_frame := ((-.5outer_frame_wd, -.5outer_frame_ht), (.5outer_frame_wd, -.5outer_frame_ht),
          (.5outer_frame_wd, .5outer_frame_ht), (-.5outer_frame_wd, .5outer_frame_ht));



string s;

boolean do_labels;
boolean do_black;
boolean do_png;

do_labels := false; % true;  % 
do_png    := true; % false; % 
do_black  := true;  % false; %

color line_color;

if do_black:
  do_labels := false;
  line_color := white;
else:
    line_color := black;
fi;


if do_png:
  %do_labels := false;

  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%2c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";";
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%2c.eps\";";
fi;

verbatim_tex   "\font\smallrm=cmr10 scaled 6500"
             & "\font\Smallrm=cmr10 scaled 7000"
             & "\font\mediumrm=cmr10 scaled 10500"
             & "\font\largerm=cmr10 scaled 11000"
             & "\font\LargermA=cmr12 scaled 10500"
             & "\font\LargermB=cmr12 scaled 11000"
             & "\font\Largerm=cmr12 scaled 11500"
             & "\font\Titlefont=cmr12 scaled 15000"
             & "\font\hugerm=cmr12 scaled 23000";

medium_pen     := pencircle scaled (1.5mm, 1.5mm, 1.5mm);
big_pen        := pencircle scaled (2.5mm, 2.5mm, 2.5mm);
big_square_pen := pencircle scaled (2mm, 2mm, 2mm);

pickup medium_pen;

%% * (1)

fig_ctr := 0;

color frame_color;

frame_color := black; % blue; % 

color foreground_color;
color background_color;


if do_black:
  foreground_color := white;
  background_color := black;
else:
  foreground_color := black;
  background_color := white;
fi;

for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

for i = 0 upto 3:
  p[i+4] := mediate(get_point (i) frame, get_point ((i+1) mod 4) frame);
endfor;

p8 := get_center frame;

%% * (1)

%% * (1) Fig.

beginfig(fig_ctr);
  fill frame with_color background_color;
  %draw outer_frame with_pen big_square_pen with_color black;
  draw frame with_pen big_square_pen with_color black;

  s :=   "{\Largerm Cuboids and Circles Nr.~1}";
  label(s, p8 shifted (0, 2.5)) with_color foreground_color; % white; %

  s :=   "{\mediumrm GNU 3DLDF}";
  label(s, p8 shifted (0, -4)) with_color foreground_color; % white; %

  s :=   "{\largerm Author:  Laurence Finston}";
  label(s, p8 shifted (0, -9.5)) with_color foreground_color; % white; % 

  s :=   "{\smallrm Copyright (C) 2024, 2025 The Free Software Foundation, Inc.}";
  label(s, p4 shifted (0, 2)) with_color foreground_color; % white; % 

endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Fig. "THE END"

beginfig(fig_ctr);
  fill frame with_color background_color;

  %draw outer_frame with_pen big_square_pen with_color black;
  draw frame with_pen big_square_pen with_color black;
  
  s := "{\hugerm THE\hskip.5em END}";
  label(s, p8 shifted (0, 0)) with_color foreground_color; % white; % 
  
endfig with_projection parallel_x_y;
fig_ctr += 1;


%% * (1) Fig. Frame with black background.

beginfig(fig_ctr);
  fill frame with_color black;
  %draw outer_frame with_pen big_square_pen with_color black;
  draw frame with_pen big_square_pen with_color black;
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Fig. Frame with white background.

beginfig(fig_ctr);
  fill frame with_color white;
  %draw outer_frame with_pen big_square_pen with_color black;
  draw frame with_pen big_square_pen with_color black;
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1)

message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
if not do_png:
  %pause;
fi;

bye;




%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

