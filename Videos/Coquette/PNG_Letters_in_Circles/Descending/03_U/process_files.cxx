/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

#if 0 
/*
% Draw color letters

plum;             % 13
bittersweet;      % 14
cadet_blue;       % 15
emerald;          % 16
salmon;           % 17
melon;            % 18
burnt_orange;     % 19
apricot;          % 20

% Fill color letters

jungle_green;          % 21
sky_blue;              % 22
brick_red;             % 23
dandelion;             % 24
cerulean_dvips;        % 25
mahogany;              % 26
turquoise_dvips;       % 27
cornflower_blue;       % 28

% Draw color circles

pink;             % 29
lavender;         % 30
mulberry;         % 31
rubine_red;       % 32
violet_red_dvips; % 33
peach;            % 34
navy_blue;        % 35
forest_green;     % 36

% Fill color circles

wild_strawberry;  % 37
green_yellow;     % 38
yellow_orange;    % 39
red_orange;       % 40
lime_green;       % 41
orange_red_dvips; % 42
thistle;          % 43
royal_blue;       % 44



*/

#endif 



string zeroes;
string zeroes_1;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   int j = 0;

   //   (int i = 45; i <= 45; i++) 

   for (int i = 5; i <= 172; i++) // 5, 172
   {
#if 0 // 1
       if (i == 45)
         continue;
#endif 

      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";

   // U

   /* Fill color circles (4th color) */

   s << "convert -transparent " << red_orange << " U_mask_" << zeroes << i << ".png A.png;"
     << "composite A.png big_dots_mask.png B.png;"
     << "composite B.png ../Backgrounds/Neapelgelb_1_coat_wc.png C.png;"
     << "convert -transparent blue C.png D.png;"
     << "composite D.png ../Backgrounds/Pariserblau_uniform_1_coat_wc.png E.png;";

   /* Draw colors letter and circle (1st and 3rd colors) */

   s << "convert -transparent " << emerald << " -transparent " << rubine_red << " "

   /* Fill color letter (2nd color) */

     << "-transparent " << dandelion << " E.png F.png;"
     << "composite F.png ../Backgrounds/Lampenschwarz_uniform_2_coats_wc.png G.png;";

   s << "composite frame_black_trans.png G.png U_desc_on_white_" << zeroes << i << ".png;";

   s << "convert -transparent white G.png H.png;"
     << "composite frame_black_trans.png H.png U_desc_on_trans_" << zeroes << i << ".png;";

/* ** (2) */

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "`status' == " << status << endl;

   if (status != 0)
   {
       exit(1);
   }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */

#if 0 


    /* This works well.  LDF 2025.02.23.  */

    /* Color replace circles with letters, once on transparent background,
       once on white.

       The images can also be put on a color replaced background, 
       but it will make more sense to composite all the letters in circles
       first.
       LDF 2025.02.23.
    */


#endif 


/* * (1) */

#if 0 

   /* Place letters in circles, color replaced, onto background with Naples yellow and blue (wc) dots  */

   /* This is relatively fast, but not needed, or only for testing.  LDF 2025.02.23.  */

   s << "composite C_desc_on_trans_" << zeroes << i << ".png Blue_on_Naples_Yellow.png "
     << "C_desc_on_color_replaced_bg_" << zeroes << i << ".png;";
#endif 


/* * (1) Remove all but one circle with letter.  */

/* DO NOT ADD UNUSED CODE HERE!   LDF 2025.02.23.  */

#if 0

   // C

   if (0)
      s << "convert -transparent white -transparent " << plum << " -transparent " << jungle_green << " "
        << "-transparent " << pink << " -transparent " << wild_strawberry << " "
        << " coquette_0004.png A.png;";
   else
      s << "cp coquette_" << zeroes << i << ".png A.png;";

     


   // O


      s << "convert -transparent white -transparent " << bittersweet << " -transparent " << sky_blue << " "
        << "-transparent " << lavender << " -transparent " << green_yellow << " "
        << "A.png B.png;";

   // Q

     s << "convert -transparent white -transparent " << cadet_blue << " -transparent " << brick_red << " "
       << "-transparent " << mulberry << " -transparent " << yellow_orange << " "
       << " B.png C.png;";

   // U

      s << "convert -transparent white -transparent " << emerald << " -transparent " << dandelion << " "
        << "-transparent " << rubine_red << " -transparent " << red_orange << " "
        << " C.png D.png;";

   // E

      s << "convert -transparent white -transparent " << salmon << " -transparent " << cerulean_dvips << " "
        << "-transparent " << violet_red_dvips << " -transparent " << lime_green << " "
        << " D.png E.png;";


   // T

      s << "convert -transparent white -transparent " << melon << " -transparent " << mahogany << " "
        << "-transparent " << peach << " -transparent " << orange_red_dvips << " "
        << " E.png F.png;";

   // T

      s << "convert -transparent white -transparent " << burnt_orange << " -transparent " << turquoise_dvips << " "
        << "-transparent " << navy_blue << " -transparent " << thistle << " "
        << " F.png G.png;";

   // E

      s << "convert -transparent white -transparent " << apricot << " -transparent " << cornflower_blue << " "
        << "-transparent " << forest_green << " -transparent " << royal_blue << " "
        << " G.png H.png;";

   /* For incremental backwards, use (i > x), `zeroes_1' and `j' and decrement `j'  
      LDF 2025.02.22.  */

   s << "composite H.png frame_black_white_bg.png C_mask_" << zeroes << i << ".png;";


#endif 

/* * (1) */

/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


