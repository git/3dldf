%% left_to_right_bg.ldf
%% Created by Laurence D. Finston (LDF) Do 20. Feb 12:57:03 CET 2025

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)


%% * (1) Left to right, background

%% ** (2)

for i = 0 upto 123:
  beginfig(fig_ctr);

%% *** (3)

    draw frame with_pen big_square_pen;

    if not do_png:
      draw outer_frame with_pen big_square_pen;
    fi;

    C14 := B[5][0] shifted (-.5mag, 6mag);
    C15 := (xpart C14, ypart p0);

    current_picture += v0;
    
    if i > 0:
      fill C14 -- C15 -- C15 shifted (i*mag/6, 0) -- C14 shifted (i*mag/6, 0) -- cycle
	with_color magenta;
    fi; 
    
    if not do_png:
      draw (C14 -- C15) shifted (i*mag/6, 0) with_pen big_pen with_color red;
    fi;
    
    
    if do_labels:
      dotlabel.bot("$p_0$", p0);
      dotlabel.bot("$p_1$", p1);
      dotlabel.lrt("$p_2$", p2);
      dotlabel.llft("$p_3$", p3);

      dotlabel.top("$C_{14}$", C14);
      dotlabel.bot("$C_{15}$", C15);
    fi;
    

    
%% *** (3)    
    
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    if do_png:
      clip current_picture to frame;
    else:
      clip current_picture to outer_frame;
    fi; 

    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    if not do_png:
      draw outer_frame with_pen big_square_pen;
    fi;

    draw frame with_pen big_square_pen;

  endfig with_projection parallel_x_y;
  fig_ctr += 1;
endfor; 

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

