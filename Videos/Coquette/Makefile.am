#### Makefile.am
#### Created by Laurence D. Finston (LDF) Di 18. Feb 09:48:04 CET 2025

#### * Copyright and License.

#### This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
#### Copyright (C) 2025 The Free Software Foundation, Inc.

#### GNU 3DLDF is free software; you can redistribute it and/or modify 
#### it under the terms of the GNU General Public License as published by 
#### the Free Software Foundation; either version 3 of the License, or 
#### (at your option) any later version. 

#### GNU 3DLDF is distributed in the hope that it will be useful, 
#### but WITHOUT ANY WARRANTY; without even the implied warranty of 
#### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
#### GNU General Public License for more details. 

#### You should have received a copy of the GNU General Public License 
#### along with GNU 3DLDF; if not, write to the Free Software 
#### Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

#### GNU 3DLDF is a GNU package.  
#### It is part of the GNU Project of the  
#### Free Software Foundation 
#### and is published under the GNU General Public License. 
#### See the website http://www.gnu.org 
#### for more information.   
#### GNU 3DLDF is available for downloading from 
#### http://www.gnu.org/software/3dldf/LDF.html. 

#### Please send bug reports to Laurence.Finston@@gmx.de
#### The mailing list help-3dldf@gnu.org is available for people to 
#### ask other users for help.  
#### The mailing list info-3dldf@gnu.org is for sending 
#### announcements to users. To subscribe to these mailing lists, send an 
#### email with "subscribe <email-address>" as the subject.  

#### The author can be contacted at: 

#### Laurence D. Finston 
#### c/o Free Software Foundation, Inc. 
#### 51 Franklin St, Fifth Floor 
#### Boston, MA  02110-1301  
#### USA

#### Laurence.Finston@gmx.de 


## * (1) Top.

## * (1)

prog: process_files

pf: process_files
	./process_files

process_files: process_files.cxx colordefs.hxx
	g++ -o $@ $<

## * (1) 

.PHONY: mp dvi eps pdf png

mp: coquette.mp
png eps: coquette_0000.eps
dvi: coquette.dvi
pdf: coquette.pdf

# right_to_left_big_dots.ldf 

coquette.mp: coquette.ldf plainldf.lmc glyphmac.lmc sub_coquette.ldf  \
             glyphs_coquette_cmitt10.ldf glyphs_coquette_cmbx10.ldf \
             dot_matrices.ldf left_to_right_big_dots.ldf left_to_right_bg.ldf \
             top_down_dots.ldf top_down_bg.ldf panels.ldf letters_on_panels.ldf \
             sub_sphere.ldf
	./3dldf --no-database $<

coquette_0000.eps: coquette.mp 
	mpost -numbersystem="double" $< # >/dev/null

coquette.dvi: coquette.tex coquette_0000.eps
	tex $<


coquette.pdf: coquette.dvi
	dvipdfmx $<

#### ** (2) titles

# .PHONY: tmp teps tdvi tpdf tpng

tmp: titles.mp
tpng teps: titles_00.eps
tdvi: titles.dvi
tpdf: titles.pdf


titles.mp: titles.ldf plainldf.lmc
	./3dldf --no-database $<

titles_00.eps: titles.mp 
	mpost -numbersystem="double" $< # >/dev/null

titles.dvi: titles.tex titles_00.eps
	tex $<

titles.pdf: titles.dvi
	dvipdfmx $<


#### ** (2) the_end

# .PHONY: tmp teps tdvi tpdf tpng

# tmp: the_end.mp
# tpng teps: the_end_00.eps
# tdvi: the_end.dvi
# tpdf: the_end.pdf


the_end.mp: the_end.ldf plainldf.lmc
	./3dldf --no-database $<

the_end_00.eps: the_end.mp 
	mpost -numbersystem="double" $< # >/dev/null

the_end.dvi: the_end.tex the_end_00.eps
	tex $<

the_end.pdf: the_end.dvi
	dvipdfmx $<






#### ** (2) Letter chart

.PHONY: lmp leps ldvi lpdf lpng

lmp: letter_chart_cmitt10.mp

leps: letter_chart_cmitt10_0000.eps

ldvi: letter_chart_cmitt10.dvi

lpdf: letter_chart_cmitt10.pdf

letter_chart_cmitt10.mp: letter_chart_cmitt10.ldf plainldf.lmc glyphs_coquette_cmitt10.ldf \
                         glyphmac.lmc
	3dldf $<

letter_chart_cmitt10_0000.eps: letter_chart_cmitt10.mp
	mpost -numbersystem="double" $<

letter_chart_cmitt10.dvi: letter_chart_cmitt10.tex letter_chart_cmitt10_0000.eps
	tex $<

letter_chart_cmitt10.pdf: letter_chart_cmitt10.dvi
	dvipdfmx $<


.PHONY: purge

purge:
	rm -f mp_input.* mp_output.* *.dvi *.log *.mf *.mpx mpxerr.*

