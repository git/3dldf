/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 25. Jan 13:13:19 CET 2024 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */

#if 0 
/*
% Draw color letters

cv += Plum_rgb;             % 13
cv += Bittersweet_rgb;      % 14
cv += CadetBlue_rgb;        % 15
cv += Emerald_rgb;          % 16
cv += Salmon_rgb;           % 17
cv += Melon_rgb;            % 19
cv += BurntOrange_rgb;      % 15
cv += Apricot_rgb;          % 20

% Fill color letters

cv += JungleGreen_rgb;      % 21
cv += SkyBlue_rgb;	    % 22
cv += BrickRed_rgb;	    % 23
cv += Dandelion_rgb;	    % 24
cv += Cerulean_rgb;	    % 25
cv += Mahogany_rgb;	    % 26
cv += Turquoise_rgb;	    % 27
cv += CornflowerBlue_rgb;   % 28

% Draw color circles

cv += pink;                 % 29
cv += Lavender_rgb;         % 30
cv += Mulberry_rgb;         % 31
cv += RubineRed_rgb;        % 32
cv += VioletRed_rgb;        % 33
cv += Peach_rgb;            % 34
cv += NavyBlue_rgb;         % 35
cv += ForestGreen_rgb;      % 36

% Fill color circles

cv += WildStrawberry_rgb; % 37
cv += GreenYellow_rgb;    % 38
cv += YellowOrange_rgb;   % 39
cv += RedOrange_rgb;      % 40
cv += LimeGreen_rgb;      % 41
cv += OrangeRed_rgb;      % 42
cv += Thistle_rgb;        % 43
cv += RoyalBlue_rgb;      % 44

*/

#endif 



string zeroes;
string zeroes_1;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   int j = 0;
   for (int i = 1; i <= 1; i++)
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";

      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";


   s << "convert -fill black -opaque red -opaque " << green << " "
     << "-opaque cyan -opaque magenta "
     << "-opaque " << purple << " -opaque " << orange << " "
     << "-opaque " << pink << " "
     << "-opaque " << teal_blue << " -opaque " << rose_madder << " "
     << "-opaque " << periwinkle << " "
     << "-opaque " << mahogany << " -opaque " << apricot << " "
     << "-opaque " << salmon << " "
     << "-opaque " << raw_sienna << " -opaque " << emerald << " "
     << "-opaque " << forest_green << " -opaque " << peach << " "
     << "-opaque " << sky_blue << " -opaque " << dandelion << " "
     << "-fill white -opaque " << dark_green << " -opaque " << melon << " " 
     << "-opaque " << goldenrod << " "
     << "stardust_" << zeroes << i << ".png A.png;";

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "`status' == " << status << endl;

   if (status != 0)
   {
       exit(1);
   }   
   
   }  /* |for|  */


  return 0;

}

/* * (1) */


#if 0

#endif 


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


