/* process_files.cxx  */
/* Created by Laurence D. Finston (LDF) Do 20. Feb 10:03:25 CET 2025 */

/* * (1) Copyright and License.

This file is part of GNU 3DLDF, a package for three-dimensional drawing.  
Copyright (C) 2024 The Free Software Foundation, Inc.

GNU 3DLDF is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; either version 3 of the License, or 
(at your option) any later version.  

GNU 3DLDF is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details.  

You should have received a copy of the GNU General Public License 
along with GNU 3DLDF; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

GNU 3DLDF is a GNU package.  
It is part of the GNU Project of the  
Free Software Foundation 
and is published under the GNU General Public License. 
See the website http://www.gnu.org 
for more information.   
GNU 3DLDF is available for downloading from 
http://www.gnu.org/software/3dldf/LDF.html.

Please send bug reports to Laurence.Finston@gmx.de
The mailing list help-3dldf@gnu.org is available for people to 
ask other users for help.  
The mailing list info-3dldf@gnu.org is for sending 
announcements to users. To subscribe to these mailing lists, send an 
email with ``subscribe <email-address>'' as the subject.  

The author can be contacted at: 

Laurence D. Finston 
c/o Free Software Foundation, Inc. 
51 Franklin St, Fifth Floor 
Boston, MA  02110-1301  
USA

Laurence.Finston@gmx.de 
*/


#include <stdlib.h>
#include <cstdio>
#include <float.h>
#include <iomanip>
#include <ios>
#include <iostream>
#include <iterator>
#include <math.h>
#include <new>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

stringstream s;

#include "colordefs.hxx"

// (setq outline-regexp "/\\* [*\f]+")

/* * (1) */


string zeroes;
string zeroes_1;

int main(int argc, char *argv[])
{
/* ** (2) */

   int status = 0;

   stringstream s;

   int j = 0;
   for (int i = 0; i <= 0; i++)
   {
      s.str("");

      if (i < 10)
         zeroes = "000";
      else if (i < 100)
         zeroes = "00";
      else if (i < 1000)
         zeroes = "0";
      else
         zeroes = "";


      if (j < 10)
         zeroes_1 = "000";
      else if (j < 100)
         zeroes_1 = "00";
      else if (j < 1000)
         zeroes_1 = "0";
      else
         zeroes_1 = "";


   // 

   s << "convert -define png:color-type=6 -fill " << teal_blue << " -opaque " << pink << " "
     << "letters_outlines_pink_on_trans.png A.png;"
     << "convert -define png:color-type=6 -fill " << teal_blue << " -opaque " << pink << " "
     << "letters_blur_template_pink_on_trans.png B.png;"
     << "convert -define png:color-type=6 -blur 0x12 B.png C.png;"
     << "composite -define png:color-type=6 A.png C.png D.png;"
     << "composite -define png:color-type=6 D.png frame_black_black.png E.png;"
     << "composite -define png:color-type=6 E.png frame_black_trans.png "
     << "letters_blur_teal_blue_with_outlines.png;"
     << "composite -define png:color-type=6 C.png frame_black_black.png F.png;"   
     << "composite -define png:color-type=6 frame_black_trans.png F.png "
     << "letters_blur_teal_blue_no_outlines.png;";

   cerr << "s.str() == " << s.str() << endl;

   status = system(s.str().c_str());

   cerr << "`status' == " << status << endl;

   if (status != 0)
   {
       exit(1);
   }   
   
   }  /* |for|  */


  return 0;

}


/* * (1) */

#if 0 


   // Letters filled white, outlines white

   s << "convert -fill black -opaque " << orange << " -opaque " << violet << " "
     << "-opaque " << dark_green << " -opaque " << teal_blue
     << " -opaque " << rose_madder << " -opaque " << periwinkle
     << " -opaque " << goldenrod  << " -opaque " << maroon << " "
     << "-fill white -opaque " << cornflower_blue << " "
     << "coquette_0004.png A.png;";

   // Letters filled black, outlines white


   s << "convert -fill black -opaque white -opaque " << orange << " -opaque " << violet << " "
     << "-opaque " << dark_green << " -opaque " << teal_blue
     << " -opaque " << rose_madder << " -opaque " << periwinkle
     << " -opaque " << goldenrod  << " -opaque " << maroon << " "
     << "-fill white -opaque " << cornflower_blue << " "
     << "coquette_0004.png B.png;";


   // Letters filled black, outlines pink

   s << "convert -fill black -opaque white -opaque " << orange << " -opaque " << violet << " "
     << "-opaque " << dark_green << " -opaque " << teal_blue
     << " -opaque " << rose_madder << " -opaque " << periwinkle
     << " -opaque " << goldenrod  << " -opaque " << maroon << " "
     << "-fill " << pink << " -opaque " << cornflower_blue << " "
     << "coquette_0004.png C.png;";

   // RGB pink on Lampenschwarz

   s << "convert -transparent black C.png D.png;"
     << "composite D.png ./Backgrounds/Lampenschwarz_2_coats_wc.png E.png;";

#endif 

/* * (1) */


/* * Emacs-Lisp code for use in indirect buffers when using the          */
/*   GNU Emacs editor.  The local variable list is not evaluated when an */
/*   indirect buffer is visited, so it's necessary to evaluate the       */
/*   following s-expression in order to use the facilities normally      */
/*   accessed via the local variables list.                              */
/*   \initials{LDF 2004.02.12}.                                          */
/*   (progn (cweb-mode) (outline-minor-mode t))                          */

/* * Local variables for Emacs.*/
/* Local Variables: */
/* mode:CWEB */
/* eval:(display-time) */
/* eval:(read-abbrev-file) */
/* indent-tabs-mode:nil */
/* eval:(outline-minor-mode) */
/* fill-column:80 */
/* End: */


