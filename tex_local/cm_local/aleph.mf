%% aleph.mf
%% Created by Laurence D. Finston (LDF) Fri 05 Aug 2022 12:30:45 PM CEST

%% Copied from symbol.mf.  LDF 2022.08.05.

cmchar "Hebrew letter aleph";
beginchar(oct"100",11u#,asc_height#,0);
adjust_fit(0,0);
pickup fine.nib;

pos1(cap_stem,75);
pos2(cap_stem,90);
pos3(cap_stem,90);
pos4(cap_stem,75);

lft x1l=hround u-eps;
x2=2.5u=w-x3;
rt x4r=hround(w-u)+eps;
top y1r=h;
bot y4l=0;
z2=whatever[z1l,z4r];
z3=whatever[z1l,z4r];

filldraw z1r{4(x1l-x1r),y1l-y1r}...{down}z1l...z2l
 ---z3l...{down}z4l{4(x4r-x4l),y4r-y4l}...{up}z4r...z3r
 ---z2r...{up}cycle; % long diagonal

pos5(cap_stem,75); pos6(cap_stem,90); pos7(cap_stem,75);

lft x5l=hround(w-4u)-eps; x6=.5[x5,x7]; x7=x4;

y5=y1; 
bot y7l=x_height-o; 
z6=whatever[z5l,z7r];

filldraw z5r{4(x5l-x5r),y5l-y5r}...{down}z5l
 ...z6l{z7r-z5l}...{down}z7l{4(x7r-x7l),y7r-y7l}...{up}z7r
 ...z6r{z5l-z7l}...{up}cycle; % short diagonal

pos8(cap_hair,0); 
pos9(cap_hair,0); 
z8=z6; 
x9=x8-.75u; 
z9=whatever[z2,z3];

filldraw stroke z8e{down}..{down}z9e;  % right stem

pos10(cap_hair,-30); 
pos11(stem,0);
pos12(cap_curve,0); 
pos13(cap_curve,0); 
pos14(vair,90);

lft x11l=hround 1.5u; 
x10=x12=.4[x11,.5w]; 
z10=whatever[z2,z3];

lft x13l=hround u; 
z13l=z14l; 
y11=.5y10; 
y12=.2[y14r,y11]; 
bot y13=0;

filldraw stroke z10e{2(x11-x10),y11-y10}...z11e{down}..{down}z12e; % left stem

filldraw z12r{down}...z13r---z13l--z14r{right}...{up}z12l--cycle; % flourish

penlabels(1,2,3,4,5,6,7,8,9,10,11,12,13,14); endchar;
