/* astronmy.sql    */
/* Created by Laurence D. Finston (LDF) 2021.05.29.  */

/* * (1) Copyright notice  */

/* This file is part of GNU 3DLDF, a package for three-dimensional drawing.  */
/* Copyright (C) 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. */

/* GNU 3DLDF is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or  */
/* (at your option) any later version. */

/* GNU 3DLDF is distributed in the hope that it will be useful,  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  */
/* GNU General Public License for more details.  */

/* You should have received a copy of the GNU General Public License  */
/* along with GNU 3DLDF; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  */

/* GNU 3DLDF is a GNU package.   */
/* It is part of the GNU Project of the   */
/* Free Software Foundation  */
/* and is published under the GNU General Public License.  */
/* See the website http://www.gnu.org  */
/* for more information.    */
/* GNU 3DLDF is available for downloading from  */
/* http://www.gnu.org/software/3dldf/LDF.html.  */

/* Please send bug reports to Laurence.Finston@gmx.de */
/* The mailing list help-3dldf@gnu.org is available for people to  */
/* ask other users for help.   */
/* The mailing list info-3dldf@gnu.org is for sending  */
/* announcements to users. To subscribe to these mailing lists, send an  */
/* email with "subscribe <email-address>" as the subject.   */

/* The author can be contacted at:  */

/* Laurence D. Finston                   */
/* c/o Free Software Foundation, Inc.   */
/* 51 Franklin St, Fifth Floor           */
/* Boston, MA  02110-1301               */
/* USA                                   */

/* Laurence.Finston@gmx.de */


/* * (1)  */

use 3dldf;

/* * (1) Catalogues and catalogue numbers.  */

http://www.atlasoftheuniverse.com/stars.html

bs_hr_number:  BS:  Bright Star, HR:  Harvard Revised.

https://en.wikipedia.org/wiki/Bright_Star_Catalogue

/* ** (2) Create tables  */

/* *** (3) Stars  */

drop table Stars;

create table Stars
(
   id int not null unique,
   constellation_id int not null default 0,
   common_name varchar(32) not null default "",
   greek_name varchar(32) not null default "",
   latin_name varchar(32) not null default "",
   arabic_name varchar(32) not null default "",
   flamsteed_designation_number int not null default 0,
   bayer_designation_greek_letter varchar(16) not null default "",
   bayer_designation_greek_letter_tex varchar(16) not null default "",	
   bayer_designation_extension varchar(16) not null default "",	
   bs_hr_number int not null default 0,
   approx_rank_apparent_magnitude int not null default 0,
   apparent_magnitude float default null,
   apparent_magnitude_varies boolean not null default 0,
   absolute_magnitude float not null default 0,
   absolute_magnitude_varies boolean not null default 0,
   constellation_abbreviation char(3) not null default "",
   constellation_full_name varchar(32) not null default "",
   constellation_name_genitive varchar(64) not null default "",
   constellation_number int not null default 0,
   right_ascension_hours int not null default 0,
   right_ascension_minutes int not null default 0,
   right_ascension_seconds float not null default 0.0,
   right_ascension_decimal_hours float not null default 0.0,
   right_ascension_decimal_degrees float not null default 0.0,
   declination_degrees int not null default 0,
   declination_minutes int not null default 0,
   declination_seconds float not null default 0.0,
   declination_decimal_degrees float not null default 0.0,
   epoch int not null default 2000,
   is_binary int not null default 0,
   is_multiple int not null default 0,
   is_binary_component int not null default 0,
   is_eclipsing_binary int not null default 0,
   notes varchar(1024) not null default ""
);

alter table Stars add column epoch int not null default 2000 after declination_decimal_degrees;

insert into Stars (id, constellation_id, common_name) values (0, 0, "dummy");


-- Numbers are `approx_rank_apparent_magnitude'

1. Alpha Canis Majoris       Sirius            06, 45, -16.7  227.2  -8.9  A1V          -1.46   1.43  379.21 1.58     9
2  Alpha Carinae             Canopus
3, Alpha Centauri,           Rigil Kentaurus
4, Alpha Boötis,             Arcturus
5, Alpha Lyra,               Vega
6, Alpha Auriga,             Capella
7, Beta Orionis,             Rigel
8, Alpha Canis Minor,        Procyon
9, Alpha Eridani,            Achernar
10, Alpha Orionis,           Betelgeuse

11. Beta Centauri             Hadar             14 04 -60.4  311.8  +1.2  B1III         0.61  -5.42    6.21 0.56   530
12. Alpha Aquilae             Altair            19 51  +8.9   47.8  -9.0  A7V           0.77   2.21  194.44 0.94    17
13. Alpha Crucis              Acrux             12 27 -63.1  300.2  -0.4  B0.5IV+B1V    0.79  -4.17   10.17 0.67   320
14. Alpha Tauri               Aldebaran         04 36 +16.5  181.0 -20.2  K5III         0.86v -0.64v  50.09 0.95    65
15. Alpha Scorpii             Antares           16 29 -26.4  351.9 +15.1  M1Ib+B4V      0.95v -5.39v   5.40 1.68   600
16. Alpha Virginis            Spica             13 25 -11.2  316.1 +50.8  B1V+B2V       0.97  -3.56   12.44 0.86   260
17. Beta Geminorum            Pollux            07 45 +28.0  192.2 +23.3  K0III         1.14   1.07   96.74 0.87    34
18. Alpha Piscis Austrini     Fomalhaut         22 58 -29.6   20.6 -65.0  A3V           1.15   1.72  130.08 0.92    25
19. Alpha Cygni               Deneb             20 41 +45.3   84.3  +2.1  A2Ia          1.24  -8.74    1.01 0.57  3000
20. Beta Crucis               Mimosa            12 48 -59.7  302.5  +3.2  B0.5III       1.26  -3.91    9.25 0.61   350

/* ** (2) */

show columns from Stars;

select time_to_sec('1:45:20.02');
select time_to_sec('1:45:20');

select utc_time();

/* *** (3) Constellations  */

drop table Constellations;

create table Constellations
(
   id int not null default 0,
   rank_constellation int not null default 0,
   name varchar(64) not null default "",
   name_genitive varchar(64) not null default "",
   abbreviation varchar(16) not null default ""
);

insert into Constellations (id, name) values (0, "dummy");

show columns from Constellations;

/* *** (3) Stars_Links  */

drop table Stars_Links;

create table Stars_Links
(
   id int not null default 0,
   common_name varchar(32) not null default "",
   flamsteed_designation_number int not null default 0,
   bayer_designation_greek_letter varchar(16) not null default "",
   constellation_name_genitive varchar(64) not null default "",
   bs_hr_number int not null default 0,
   link varchar(128) not null default ""
);

/* **** (4) */

show columns from Stars_Links;

/* **** (4) Sirius  */

delete from Stars_Links where common_name = "Sirius";

replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
("Sirius", 9, "alpha", 2491, "Canis Majoris", "https://en.wikipedia.org/wiki/Sirius");


replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
("Sirius", 9, "alpha", 2491, "Canis Majoris", "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=*+alf+CMa");

replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
("Sirius", 9, "alpha", 2491, "Canis Majoris", "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=TYC+5949-2777-1");

replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
("Sirius", 9, "alpha", 2491, "Canis Majoris", "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=*+alf+CMa+B");

/* **** (4) Canopus  */

replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
(
"Canopus", 0, "alpha", 2326, "Carinae", "https://en.wikipedia.org/wiki/Canopus");


replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
(
"Canopus", 0, "alpha", 2326, "Carinae", "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=canopus");

/* **** (4) Select from Stars_Links  */

select * from Stars_Links order by bs_hr_number, link\G

select * from Stars_Links where bs_hr_number = (select bs_hr_number from Stars where common_name = "Sirius")\G

select * from Stars where common_name = "Canopus"\G

select * from Stars_Links where bs_hr_number = (select bs_hr_number from Stars where common_name = "Canopus")\G

/* *** (3) */

/* ** (2) Insert into Constellations  */

insert ignore into Constellations (rank_constellation, abbreviation, name) values (1, "Hya", "Hydra");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (02, "Vir", "Virgo");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (03, "UMa", "Ursa Major");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (04, "Cet", "Cetus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (05, "Her", "Hercules");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (06, "Eri", "Eridanus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (07, "Peg", "Pegasus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (08, "Dra", "Draco");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (09, "Cen", "Centaurus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (10, "Aqr", "Aquarius");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (11, "Oph", "Ophiuchus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (12, "Leo", "Leo");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (13, "Boo", "Boötes");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (14, "Psc", "Pisces");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (15, "Sgr", "Sagittarius");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (16, "Cyg", "Cygnus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (17, "Tau", "Taurus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (18, "Cam", "Camelopardalis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (19, "And", "Andromeda");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (20, "Pup", "Puppis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (21, "Aur", "Auriga");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (22, "Aql", "Aquila");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (23, "Ser", "Serpens");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (24, "Per", "Perseus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (25, "Cas", "Cassiopeia");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (26, "Ori", "Orion");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (27, "Cep", "Cepheus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (28, "Lyn", "Lynx");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (29, "Lib", "Libra");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (30, "Gem", "Gemini");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (31, "Cnc", "Cancer");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (32, "Vel", "Vela");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (33, "Sco", "Scorpius");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (34, "Car", "Carina");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (35, "Mon", "Monoceros");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (36, "Scl", "Sculptor");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (37, "Phe", "Phoenix");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (38, "CVn", "Canes");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (39, "Ari", "Aries");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (40, "Cap", "Capricornus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (41, "For", "Fornax");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (42, "Com", "Coma");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (43, "CMa", "Canis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (44, "Pav", "Pavo");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (45, "Gru", "Grus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (46, "Lup", "Lupus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (47, "Sex", "Sextans");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (48, "Tuc", "Tucana");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (49, "Ind", "Indus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (50, "Oct", "Octans");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (51, "Lep", "Lepus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (52, "Lyr", "Lyra");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (53, "Crt", "Crater");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (54, "Col", "Columba");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (55, "Vul", "Vulpecula");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (56, "UMi", "Ursa Minor");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (57, "Tel", "Telescopium");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (58, "Hor", "Horologium");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (59, "Pic", "Pictor");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (60, "PsA", "Piscis Austrinus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (61, "Hyi", "Hydrus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (62, "Ant", "Antlia");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (63, "Ara", "Ara");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (64, "LMi", "Leo Minor");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (65, "Pyx", "Pyxis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (66, "Mic", "Microscopium");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (67, "Aps", "Apus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (68, "Lac", "Lacerta");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (69, "Delete", "Delphinus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (70, "Crv", "Corvus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (71, "CMi", "Canis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (72, "Dor", "Dorado");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (73, "CrB", "Corona Borealis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (74, "Nor", "Norma");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (75, "Men", "Mensa");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (76, "Vol", "Volans");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (77, "Mus", "Musca");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (78, "Tri", "Triangulum");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (79, "Cha", "Chamaeleon");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (80, "CrA", "Corona Australis");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (81, "Cae", "Caelum");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (82, "Ret", "Reticulum");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (83, "TrA", "Triangulum");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (84, "Sct", "Scutum");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (85, "Cir", "Circinus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (86, "Sge", "Sagitta");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (87, "Equ", "Equuleus");

insert ignore into Constellations (rank_constellation, abbreviation, name) values (88, "Cru", "Crux");


/* ** (2) Replace into Stars  */

/* ** (2) Update Stars, set `constellation_full_name'.  */

/* *** (3) */

update Stars set constellation_full_name = "Andromeda" where constellation_abbreviation = "And";
update Stars set constellation_number = 19 where constellation_abbreviation = "And";

update Stars set constellation_full_name = "Antlia" where constellation_abbreviation = "Ant";
update Stars set constellation_number = 62 where constellation_abbreviation = "Ant";

update Stars set constellation_full_name = "Apus" where constellation_abbreviation = "Aps";
update Stars set constellation_number = 67 where constellation_abbreviation = "Aps";

update Stars set constellation_full_name = "Aquarius" where constellation_abbreviation = "Aqr";
update Stars set constellation_number = 10 where constellation_abbreviation = "Aqr";

update Stars set constellation_full_name = "Aquila" where constellation_abbreviation = "Aql";
update Stars set constellation_number = 22 where constellation_abbreviation = "Aql";

update Stars set constellation_full_name = "Ara" where constellation_abbreviation = "Ara";
update Stars set constellation_number = 63 where constellation_abbreviation = "Ara";

update Stars set constellation_full_name = "Aries" where constellation_abbreviation = "Ari";
update Stars set constellation_number = 39 where constellation_abbreviation = "Ari";

update Stars set constellation_full_name = "Auriga" where constellation_abbreviation = "Aur";
update Stars set constellation_number = 21 where constellation_abbreviation = "Aur";

update Stars set constellation_full_name = "Boötes" where constellation_abbreviation = "Boo";
update Stars set constellation_number = 13 where constellation_abbreviation = "Boo";

update Stars set constellation_full_name = "Caelum" where constellation_abbreviation = "Cae";
update Stars set constellation_number = 81 where constellation_abbreviation = "Cae";

update Stars set constellation_full_name = "Camelopardalis" where constellation_abbreviation = "Cam";
update Stars set constellation_number = 18 where constellation_abbreviation = "Cam";

update Stars set constellation_full_name = "Cancer" where constellation_abbreviation = "Cnc";
update Stars set constellation_number = 31 where constellation_abbreviation = "Cnc";

update Stars set constellation_full_name = "Canes_Venatici" where constellation_abbreviation = "CVn";
update Stars set constellation_number = 38 where constellation_abbreviation = "CVn";

update Stars set constellation_full_name = "Canis_Major" where constellation_abbreviation = "CMa";
update Stars set constellation_number = 43 where constellation_abbreviation = "CMa";

update Stars set constellation_full_name = "Canis_Minor" where constellation_abbreviation = "CMi";
update Stars set constellation_number = 71 where constellation_abbreviation = "CMi";

update Stars set constellation_full_name = "Capricornus" where constellation_abbreviation = "Cap";
update Stars set constellation_number = 40 where constellation_abbreviation = "Cap";

update Stars set constellation_full_name = "Carina" where constellation_abbreviation = "Car";
update Stars set constellation_number = 34 where constellation_abbreviation = "Car";

update Stars set constellation_full_name = "Cassiopeia" where constellation_abbreviation = "Cas";
update Stars set constellation_number = 25 where constellation_abbreviation = "Cas";

update Stars set constellation_full_name = "Centaurus" where constellation_abbreviation = "Cen";
update Stars set constellation_number = 09 where constellation_abbreviation = "Cen";

update Stars set constellation_full_name = "Cepheus" where constellation_abbreviation = "Cep";
update Stars set constellation_number = 27 where constellation_abbreviation = "Cep";

update Stars set constellation_full_name = "Cetus" where constellation_abbreviation = "Cet";
update Stars set constellation_number = 04 where constellation_abbreviation = "Cet";

update Stars set constellation_full_name = "Chamaeleon" where constellation_abbreviation = "Cha";
update Stars set constellation_number = 79 where constellation_abbreviation = "Cha";

update Stars set constellation_full_name = "Circinus" where constellation_abbreviation = "Cir";
update Stars set constellation_number = 85 where constellation_abbreviation = "Cir";

update Stars set constellation_full_name = "Columba" where constellation_abbreviation = "Col";
update Stars set constellation_number = 54 where constellation_abbreviation = "Col";

update Stars set constellation_full_name = "Coma_Berenices" where constellation_abbreviation = "Com";
update Stars set constellation_number = 42 where constellation_abbreviation = "Com";

update Stars set constellation_full_name = "Corona_Australis" where constellation_abbreviation = "CrA";
update Stars set constellation_number = 80 where constellation_abbreviation = "CrA";

update Stars set constellation_full_name = "Corona_Borealis" where constellation_abbreviation = "CrB";
update Stars set constellation_number = 73 where constellation_abbreviation = "CrB";

update Stars set constellation_full_name = "Corvus" where constellation_abbreviation = "Crv";
update Stars set constellation_number = 70 where constellation_abbreviation = "Crv";

update Stars set constellation_full_name = "Crater" where constellation_abbreviation = "Crt";
update Stars set constellation_number = 53 where constellation_abbreviation = "Crt";

update Stars set constellation_full_name = "Crux" where constellation_abbreviation = "Cru";
update Stars set constellation_number = 88 where constellation_abbreviation = "Cru";

update Stars set constellation_full_name = "Cygnus" where constellation_abbreviation = "Cyg";
update Stars set constellation_number = 16 where constellation_abbreviation = "Cyg";

update Stars set constellation_full_name = "Delphinus" where constellation_abbreviation = "Del";
update Stars set constellation_number = 69 where constellation_abbreviation = "Del";

update Stars set constellation_full_name = "Dorado" where constellation_abbreviation = "Dor";
update Stars set constellation_number = 72 where constellation_abbreviation = "Dor";

update Stars set constellation_full_name = "Draco" where constellation_abbreviation = "Dra";
update Stars set constellation_number = 08 where constellation_abbreviation = "Dra";

update Stars set constellation_full_name = "Equuleus" where constellation_abbreviation = "Equ";
update Stars set constellation_number = 87 where constellation_abbreviation = "Equ";

update Stars set constellation_full_name = "Eridanus" where constellation_abbreviation = "Eri";
update Stars set constellation_number = 06 where constellation_abbreviation = "Eri";

update Stars set constellation_full_name = "Fornax" where constellation_abbreviation = "For";
update Stars set constellation_number = 41 where constellation_abbreviation = "For";

update Stars set constellation_full_name = "Gemini" where constellation_abbreviation = "Gem";
update Stars set constellation_number = 30 where constellation_abbreviation = "Gem";

update Stars set constellation_full_name = "Grus" where constellation_abbreviation = "Gru";
update Stars set constellation_number = 45 where constellation_abbreviation = "Gru";

update Stars set constellation_full_name = "Hercules" where constellation_abbreviation = "Her";
update Stars set constellation_number = 05 where constellation_abbreviation = "Her";

update Stars set constellation_full_name = "Horologium" where constellation_abbreviation = "Hor";
update Stars set constellation_number = 58 where constellation_abbreviation = "Hor";

update Stars set constellation_full_name = "Hydra" where constellation_abbreviation = "Hya";
update Stars set constellation_number = 01 where constellation_abbreviation = "Hya";

update Stars set constellation_full_name = "Hydrus" where constellation_abbreviation = "Hyi";
update Stars set constellation_number = 61 where constellation_abbreviation = "Hyi";

update Stars set constellation_full_name = "Indus" where constellation_abbreviation = "Ind";
update Stars set constellation_number = 49 where constellation_abbreviation = "Ind";

update Stars set constellation_full_name = "Lacerta" where constellation_abbreviation = "Lac";
update Stars set constellation_number = 68 where constellation_abbreviation = "Lac";

update Stars set constellation_full_name = "Leo" where constellation_abbreviation = "Leo";
update Stars set constellation_number = 12 where constellation_abbreviation = "Leo";

update Stars set constellation_full_name = "Leo_Minor" where constellation_abbreviation = "LMi";
update Stars set constellation_number = 64 where constellation_abbreviation = "LMi";

update Stars set constellation_full_name = "Lepus" where constellation_abbreviation = "Lep";
update Stars set constellation_number = 51 where constellation_abbreviation = "Lep";

update Stars set constellation_full_name = "Libra" where constellation_abbreviation = "Lib";
update Stars set constellation_number = 29 where constellation_abbreviation = "Lib";

update Stars set constellation_full_name = "Lupus" where constellation_abbreviation = "Lup";
update Stars set constellation_number = 46 where constellation_abbreviation = "Lup";

update Stars set constellation_full_name = "Lynx" where constellation_abbreviation = "Lyn";
update Stars set constellation_number = 28 where constellation_abbreviation = "Lyn";

update Stars set constellation_full_name = "Lyra" where constellation_abbreviation = "Lyr";
update Stars set constellation_number = 52 where constellation_abbreviation = "Lyr";

update Stars set constellation_full_name = "Mensa" where constellation_abbreviation = "Men";
update Stars set constellation_number = 75 where constellation_abbreviation = "Men";

update Stars set constellation_full_name = "Microscopium" where constellation_abbreviation = "Mic";
update Stars set constellation_number = 66 where constellation_abbreviation = "Mic";

update Stars set constellation_full_name = "Monoceros" where constellation_abbreviation = "Mon";
update Stars set constellation_number = 35 where constellation_abbreviation = "Mon";

update Stars set constellation_full_name = "Musca" where constellation_abbreviation = "Mus";
update Stars set constellation_number = 77 where constellation_abbreviation = "Mus";

update Stars set constellation_full_name = "Norma" where constellation_abbreviation = "Nor";
update Stars set constellation_number = 74 where constellation_abbreviation = "Nor";

update Stars set constellation_full_name = "Octans" where constellation_abbreviation = "Oct";
update Stars set constellation_number = 50 where constellation_abbreviation = "Oct";

update Stars set constellation_full_name = "Ophiuchus" where constellation_abbreviation = "Oph";
update Stars set constellation_number = 11 where constellation_abbreviation = "Oph";

update Stars set constellation_full_name = "Orion" where constellation_abbreviation = "Ori";
update Stars set constellation_number = 26 where constellation_abbreviation = "Ori";

update Stars set constellation_full_name = "Pavo" where constellation_abbreviation = "Pav";
update Stars set constellation_number = 44 where constellation_abbreviation = "Pav";

update Stars set constellation_full_name = "Pegasus" where constellation_abbreviation = "Peg";
update Stars set constellation_number = 07 where constellation_abbreviation = "Peg";

update Stars set constellation_full_name = "Perseus" where constellation_abbreviation = "Per";
update Stars set constellation_number = 24 where constellation_abbreviation = "Per";

update Stars set constellation_full_name = "Phoenix" where constellation_abbreviation = "Phe";
update Stars set constellation_number = 37 where constellation_abbreviation = "Phe";

update Stars set constellation_full_name = "Pictor" where constellation_abbreviation = "Pic";
update Stars set constellation_number = 59 where constellation_abbreviation = "Pic";

update Stars set constellation_full_name = "Pisces" where constellation_abbreviation = "Psc";
update Stars set constellation_number = 14 where constellation_abbreviation = "Psc";

update Stars set constellation_full_name = "Piscis_Austrinus" where constellation_abbreviation = "PsA";
update Stars set constellation_number = 60 where constellation_abbreviation = "PsA";

update Stars set constellation_full_name = "Puppis" where constellation_abbreviation = "Pup";
update Stars set constellation_number = 20 where constellation_abbreviation = "Pup";

update Stars set constellation_full_name = "Pyxis" where constellation_abbreviation = "Pyx";
update Stars set constellation_number = 65 where constellation_abbreviation = "Pyx";

update Stars set constellation_full_name = "Reticulum" where constellation_abbreviation = "Ret";
update Stars set constellation_number = 82 where constellation_abbreviation = "Ret";

update Stars set constellation_full_name = "Sagitta" where constellation_abbreviation = "Sge";
update Stars set constellation_number = 86 where constellation_abbreviation = "Sge";

update Stars set constellation_full_name = "Sagittarius" where constellation_abbreviation = "Sgr";
update Stars set constellation_number = 15 where constellation_abbreviation = "Sgr";

update Stars set constellation_full_name = "Scorpius" where constellation_abbreviation = "Sco";
update Stars set constellation_number = 33 where constellation_abbreviation = "Sco";

update Stars set constellation_full_name = "Sculptor" where constellation_abbreviation = "Scl";
update Stars set constellation_number = 36 where constellation_abbreviation = "Scl";

update Stars set constellation_full_name = "Scutum" where constellation_abbreviation = "Sct";
update Stars set constellation_number = 84 where constellation_abbreviation = "Sct";

update Stars set constellation_full_name = "Serpens" where constellation_abbreviation = "Ser";
update Stars set constellation_number = 23 where constellation_abbreviation = "Ser";

update Stars set constellation_full_name = "Sextans" where constellation_abbreviation = "Sex";
update Stars set constellation_number = 47 where constellation_abbreviation = "Sex";

update Stars set constellation_full_name = "Taurus" where constellation_abbreviation = "Tau";
update Stars set constellation_number = 17 where constellation_abbreviation = "Tau";

update Stars set constellation_full_name = "Telescopium" where constellation_abbreviation = "Tel";
update Stars set constellation_number = 57 where constellation_abbreviation = "Tel";

update Stars set constellation_full_name = "Triangulum" where constellation_abbreviation = "Tri";
update Stars set constellation_number = 78 where constellation_abbreviation = "Tri";

update Stars set constellation_full_name = "Triangulum_Australe" where constellation_abbreviation = "TrA";
update Stars set constellation_number = 83 where constellation_abbreviation = "TrA";

update Stars set constellation_full_name = "Tucana" where constellation_abbreviation = "Tuc";
update Stars set constellation_number = 48 where constellation_abbreviation = "Tuc";

update Stars set constellation_full_name = "Ursa_Major" where constellation_abbreviation = "UMa";
update Stars set constellation_number = 03 where constellation_abbreviation = "UMa";

update Stars set constellation_full_name = "Ursa_Minor" where constellation_abbreviation = "UMi";
update Stars set constellation_number = 56 where constellation_abbreviation = "UMi";

update Stars set constellation_full_name = "Vela" where constellation_abbreviation = "Vel";
update Stars set constellation_number = 32 where constellation_abbreviation = "Vel";

update Stars set constellation_full_name = "Virgo" where constellation_abbreviation = "Vir";
update Stars set constellation_number = 02 where constellation_abbreviation = "Vir";

update Stars set constellation_full_name = "Volans" where constellation_abbreviation = "Vol";
update Stars set constellation_number = 76 where constellation_abbreviation = "Vol";

update Stars set constellation_full_name = "Vulpecula" where constellation_abbreviation = "Vul";
update Stars set constellation_number = 55 where constellation_abbreviation = "Vul";

update Stars set constellation_full_name = "Ursa Major" where constellation_abbreviation = "UMa";

update Stars set constellation_full_name = "Canes Venatici" where constellation_abbreviation = "CVn";

update Stars set constellation_full_name = "Coma Berenices" where constellation_abbreviation = "Com";

update Stars set constellation_full_name = "Canis Major" where constellation_abbreviation = "CMa";

update Stars set constellation_full_name = "Ursa Minor" where constellation_abbreviation = "UMi";

update Stars set constellation_full_name = "Piscis Austrinus" where constellation_abbreviation = "PsA";

update Stars set constellation_full_name = "Leo Minor" where constellation_abbreviation = "LMi";

update Stars set constellation_full_name = "Canis Minor" where constellation_abbreviation = "CMi";

update Stars set constellation_full_name = "Corona Borealis" where constellation_abbreviation = "CrB";

update Stars set constellation_full_name = "Corona Australis" where constellation_abbreviation = "CrA";

update Stars set constellation_full_name = "Triangulum Australe" where constellation_abbreviation = "TrA";

/* *** (3)  */

-- Numbers are `approx_rank_apparent_magnitude'
1. Alpha Canis Majoris           Sirius
2  Alpha Carinae                 Canopus
3, Alpha Centauri,               Rigil Kentaurus
4, Alpha Boötis,                 Arcturus
5, Alpha Lyra,                   Vega
6, Alpha Auriga,                 Capella
7, Beta Orionis,                 Rigel
8, Alpha Canis Minor,            Procyon
9, Alpha Eridani,                Achernar
10, Alpha Orionis,               Betelgeuse

11, Beta Centauri,                Hadar
12, Alpha Aquila,                Altair
13, Alpha Crucis,                 Acrux
14, Alpha Tauri,                  Aldebaran
15, Alpha Scorpii,                Antares
16, Alpha Virginis,               Spica
17, Beta Geminorum,               Pollux
18, Alpha Piscis Austrini,        Fomalhaut
19, Alpha Cygni,                  Deneb
20, Beta Crucis,                  Mimosa
21, Alpha Leonis,                 Regulus
22, Epsilon Canis Major,        Adhara
23, Alpha Geminorum,              Castor
24, Lambda Scorpii,               Shaula
25, Gamma Crucis,                 Gacrux
26, Gamma Orionis,                Bellatrix
27, Beta Tauri,                   Elnath
28, Beta Carina,                 Miaplacidus
29, Epsilon Orionis,              Alnilam
30, Alpha Gruis,                  Alnair
31, Zeta Orionis,                 Alnitak
32, Epsilon Ursa Major,        Alioth
33, Alpha Persei,                 Mirfak
34, Alpha Ursa Major,          Dubhe
35, Gamma Velorum,                Regor
36, Delta Canis Major,          Wezen
37, Epsilon Sagittarii,           Kaus Australis
38, Eta Ursa Major,            Alkaid
39, Theta Scorpii,                Sargas
40, Epsilon Carina,              Avior
41, Beta Auriga,                 Menkalinan
42, Alpha Trianguli Australis,    Atria
43, Gamma Geminorum,              Alhena
44, Alpha Pavonis,                Peacock
45, Delta Velorum,                Koo She
46, Beta Canis Major,           Mirzam
47, Alpha Hydra,                 Alphard
48, Alpha Ursa Minor,          Polaris
49, Gamma Leonis,                 Algieba
50, Alpha Arietis,                Hamal
51, Beta Ceti,                    Diphda
52, Sigma Sagittarii,             Nunki
53, Theta Centauri,               Menkent
54, Alpha Andromeda,             Alpheratz
55, Beta Andromeda,              Mirach
56, Kappa Orionis,                Saiph
57, Beta Ursa Minor,           Kochab
58, Beta Gruis,                   Al Dhanab
59, Alpha Ophiuchi,               Rasalhague
60, Beta Persei,                  Algol
61, Gamma Andromeda,             Almach
62, Beta Leonis,                  Denebola
63, Gamma Cassiopeia,            Cih
64, Gamma Centauri,               Muhlifain
65, Zeta Puppis,                  Naos
66, Iota Carina,                 Aspidiske
67, Alpha Corona Borealis,       Alphecca
68, Lambda Velorum,              Suhail
69, Zeta Ursa Major,             Mizar
70, Gamma Cygni,                 Sadr
71, Alpha Cassiopeia,            Schedar
72, Gamma Draconis,               Eltanin
73, Delta Orionis,                Mintaka
74, Beta Cassiopeia,             Caph
76, Delta Scorpii,                Dschubba
77, Epsilon Scorpii,              Wei
78, Alpha Lupi,                   Men
80, Beta Ursa Major,              Merak
81, Epsilon Boötis,               Izar
82, Epsilon Pegasi,               Enif
83, Kappa Scorpii,                Girtab
84, Alpha Phoenicis,              Ankaa
85, Gamma Ursa Major,             Phecda
86, Eta Ophiuchi,                 Sabik
87, Beta Pegasi,                  Scheat
88, Eta Canis Major,              Aludra
89, Alpha Cephei,                 Alderamin
90, Kappa Velorum,                Markeb
91, Epsilon Cygni,                Gienah
92, Alpha Pegasi,                 Markab
93, Alpha Ceti,                   Menkar
94, Zeta Ophiuchi,                Han
95, Zeta Centauri,                Al Nair
96, Delta Leonis,                 Zosma
97, Beta Scorpii,                 Graffias
98, Alpha Leporis,                Arneb
100, Gamma Corvi,                  Gienah Ghurab
101, Zeta Sagittarii,              Ascella
102, Beta Libra,                  Zubeneschamali
103, Alpha Serpentis,              Unukalhai
104, Beta Arietis,                 Sheratan
105, Alpha Libra,                 Zubenelgenubi
106, Alpha Columba,               Phact
108, Beta Corvi,                   Kraz
109, Delta Cassiopeia,            Ruchbah
110, Eta Boötis,                   Muphrid
111, Beta Lupi,                    Ke Kouan
112, Iota Auriga,                 Hassaleh
115, Upsilon Scorpii,              Lesath
117, Delta Sagittarii,             Kaus Meridionalis 
118, Gamma Aquila,                Tarazed
119, Delta Ophiuchi,               Yed Prior
120, Eta Draconis,                 Aldhibain
122, Gamma Virginis,               Porrima
123, Iota Orionis,                 Hatysa
125, Beta Ophiuchi,                Cebalrai
126, Beta Eridani,                 Kursa
127, Beta Herculis,                Kornephoros
129, Beta Draconis,                Rastaban
130, Alpha Canum Venaticorum,      Cor Caroli
132, Beta Leporis,                 Nihal
133, Zeta Herculis,                Rutilicus
136, Lambda Sagittarii,            Kaus Borealis
137, Gamma Pegasi,                 Algenib
138, Rho Puppis,                   Turais
142, Alpha Ara,                    Choo
143, Eta Tauri,                    Alcyone
144, Epsilon Virginis,             Vindemiatrix
145, Delta Capricorni,             Deneb Algedi
146, Alpha Hydri,                  Head of Hydrus
148, Mu Geminorum,                 Tejat
151, Theta Eridani,                Acamar
152, Pi Sagittarii,                Albaldah
153, Beta Canis Minor,             Gomeisa
156, Sigma Scorpii,                Alniyat
157, Beta Cygni,                   Albireo
158, Beta Aquarii,                 Sadalsuud
161, Eta Pegasi,                   Matar
163, Delta Corvi,                  Algorel
164, Alpha Aquarii,                Sadalmelik
165, Gamma Eridani,                Zaurak
166, Zeta Tauri,                   Alheka
167, Epsilon Leonis,               Ras Elased Australis 
168, Gamma^2 Sagittarii,           Alnasl
171, Zeta Aquila,                  Deneb el Okab
174, Gamma Ursa Minor,             Pherkad Major
178, Zeta Canis Major,             Phurad
180, Epsilon Corvi,                Minkar
181, Epsilon Auriga,               Almaaz
183, Gamma Boötis,                 Seginus
184, Beta Capricorni,              Dabih
185, Epsilon Geminorum,            Mebsuta
186, Mu Ursa Major,                Tania Australis
187, Delta Draconis,               Tais
192, Alpha Indi,                   Persian
193, Beta Columba,                 Wazn
194, Iota Ursa Major,              Talita
196, Delta Herculis,               Sarin
197, Kappa Centauri,               Ke Kwan
202, Theta Ursa Major,             Al Haud
203, Zeta Draconis,                Aldhibah
205, Eta Auriga,                   Hoedus II
207, Pi^3 Orionis,                 Tabit
212, Gamma Cephei,                 Errai
214, Epsilon Ophiuchi,             Yed Posterior
215, Eta Serpentis,                Alava
216, Beta Cephei,                  Alphirk
221, Sigma Libra,                  Brachium
222, Gamma Lyra,                   Sulaphat
226, Delta Aquarii,                Skat
229, Iota Draconis,                Edasich
233, Eta Geminorum,                Propus
234, Alpha Herculis,               Rasalgethi
237, Rho Persei,                   Gorgonea Tertia
238, Delta Ursa Major,             Megrez
243, Theta Leonis,                 Chort
244, Xi Puppis,                    Asmidiske
245, Epsilon Cassiopeia,           Segin
246, Eta Orionis,                  Algjebbah
247, Xi Geminorum,                 Alzirr
248, Omicron Ursa Major,           Muscida
251, Zeta Virginis,                Heze
253, Lambda Orionis,               Meissa
255, Delta Virginis,               Auva
263, Zeta Pegasi,                  Homam
264, Alpha Trianguli,              Mothallah
269, Zeta Leonis,                  Adhafera
270, Lambda Aquila,                Althalimain
271, Lambda Ursa Major,            Tania Borealis
272, Beta Lyra,                    Sheliak
273, Eta Cassiopeia,               Achird
274, Eta Ceti,                     Dheneb
277, Gamma Ceti,                   Kaffaljidhma
282, Nu Ursa Major,                Alula Borealis
283, Beta Bootis,                  Nekkar
287, Delta Geminorum,              Wasat
290, Mu Pegasi,                    Sadalbari
291, Delta Eridani,                Rana
292, Omicron Leonis,               Subra
293, Phi Velorum,                  Tseen Ke
295, Theta Pegasi,                 Baham
296, Epsilon Tauri,                Ain



/* Local Variables:                   */
/* eval:(outline-minor-mode t)        */
/* outline-regexp:"/\\* \\*+"         */
/* fill-column:80                     */
/* End:                               */
