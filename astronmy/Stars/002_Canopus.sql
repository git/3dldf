/* canopus.sql    */
/* Created by Laurence D. Finston (LDF) Do 8. Aug 20:04:33 CEST 2024 */

/* * (1) Copyright notice  */

/* This file is part of GNU 3DLDF, a package for three-dimensional drawing.  */
/* Copyright (C) 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. */

/* GNU 3DLDF is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or  */
/* (at your option) any later version. */

/* GNU 3DLDF is distributed in the hope that it will be useful,  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  */
/* GNU General Public License for more details.  */

/* You should have received a copy of the GNU General Public License  */
/* along with GNU 3DLDF; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  */

/* GNU 3DLDF is a GNU package.   */
/* It is part of the GNU Project of the   */
/* Free Software Foundation  */
/* and is published under the GNU General Public License.  */
/* See the website http://www.gnu.org  */
/* for more information.    */
/* GNU 3DLDF is available for downloading from  */
/* http://www.gnu.org/software/3dldf/LDF.html.  */

/* Please send bug reports to Laurence.Finston@gmx.de */
/* The mailing list help-3dldf@gnu.org is available for people to  */
/* ask other users for help.   */
/* The mailing list info-3dldf@gnu.org is for sending  */
/* announcements to users. To subscribe to these mailing lists, send an  */
/* email with "subscribe <email-address>" as the subject.   */

/* The author can be contacted at:  */

/* Laurence D. Finston                   */
/* c/o Free Software Foundation, Inc.   */
/* 51 Franklin St, Fifth Floor           */
/* Boston, MA  02110-1301               */
/* USA                                   */

/* Laurence.Finston@gmx.de */


/* * (1)  */

/* **** (4) Canopus  */
2. Alpha Carinae             Canopus           06 24 -52.7  261.2 -25.3  F0Ib         -0.73  -5.64   10.43 0.53   310

replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
(
"Canopus", 0, "alpha", 2326, "Carinae", "https://en.wikipedia.org/wiki/Canopus");


replace into Stars_Links (common_name, flamsteed_designation_number, bayer_designation_greek_letter,
                          bs_hr_number, constellation_name_genitive, link)
values
(
"Canopus", 0, "alpha", 2326, "Carinae", "http://simbad.u-strasbg.fr/simbad/sim-id?Ident=canopus");

select * from Stars where common_name = "Canopus"\G

select * from Stars_Links where bs_hr_number = (select bs_hr_number from Stars where common_name = "Canopus")\G

update Stars set common_name = "Canopus", approx_rank_apparent_magnitude = 2
where bayer_designation_greek_letter = "Alpha" and constellation_full_name = "Carina";




/* * (1)  */

/* Local Variables:                   */
/* eval:(outline-minor-mode t)        */
/* outline-regexp:"/\\* \\*+"         */
/* fill-column:80                     */
/* End:                               */

