/* astronmy.sql    */
/* Created by Laurence D. Finston (LDF) 2021.05.29.  */

/* * (1) Copyright notice  */

/* This file is part of GNU 3DLDF, a package for three-dimensional drawing.  */
/* Copyright (C) 2021, 2022, 2023, 2024 The Free Software Foundation, Inc. */

/* GNU 3DLDF is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or  */
/* (at your option) any later version. */

/* GNU 3DLDF is distributed in the hope that it will be useful,  */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of  */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  */
/* GNU General Public License for more details.  */

/* You should have received a copy of the GNU General Public License  */
/* along with GNU 3DLDF; if not, write to the Free Software  */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA  */

/* GNU 3DLDF is a GNU package.   */
/* It is part of the GNU Project of the   */
/* Free Software Foundation  */
/* and is published under the GNU General Public License.  */
/* See the website http://www.gnu.org  */
/* for more information.    */
/* GNU 3DLDF is available for downloading from  */
/* http://www.gnu.org/software/3dldf/LDF.html.  */

/* Please send bug reports to Laurence.Finston@gmx.de */
/* The mailing list help-3dldf@gnu.org is available for people to  */
/* ask other users for help.   */
/* The mailing list info-3dldf@gnu.org is for sending  */
/* announcements to users. To subscribe to these mailing lists, send an  */
/* email with "subscribe <email-address>" as the subject.   */

/* The author can be contacted at:  */

/* Laurence D. Finston                   */
/* c/o Free Software Foundation, Inc.   */
/* 51 Franklin St, Fifth Floor           */
/* Boston, MA  02110-1301               */
/* USA                                   */

/* Laurence.Finston@gmx.de */


/* * (1)  */

use 3dldf;

/* * (1) Catalogues and catalogue numbers.  */

http://www.atlasoftheuniverse.com/stars.html

bs_hr_number:  BS:  Bright Star, HR:  Harvard Revised.

https://en.wikipedia.org/wiki/Bright_Star_Catalogue

/* ** (2) Create tables  */

/* *** (3) Stars  */

drop table Stars;

create table Stars
(
   id int not null unique,
   constellation_id int not null default 0,
   common_name varchar(32) not null default "",
   greek_name varchar(32) not null default "",
   latin_name varchar(32) not null default "",
   arabic_name varchar(32) not null default "",
   flamsteed_designation_number int not null default 0,
   bayer_designation_greek_letter varchar(16) not null default "",
   bayer_designation_greek_letter_tex varchar(16) not null default "",	
   bayer_designation_extension varchar(16) not null default "",	
   bs_hr_number int not null default 0,
   approx_rank_apparent_magnitude int not null default 0,
   apparent_magnitude float default null,
   apparent_magnitude_varies boolean not null default 0,
   absolute_magnitude float not null default 0,
   absolute_magnitude_varies boolean not null default 0,
   constellation_abbreviation char(3) not null default "",
   constellation_full_name varchar(32) not null default "",
   constellation_name_genitive varchar(64) not null default "",
   constellation_number int not null default 0,
   right_ascension_hours int not null default 0,
   right_ascension_minutes int not null default 0,
   right_ascension_seconds float not null default 0.0,
   right_ascension_decimal_hours float not null default 0.0,
   right_ascension_decimal_degrees float not null default 0.0,
   declination_degrees int not null default 0,
   declination_minutes int not null default 0,
   declination_seconds float not null default 0.0,
   declination_decimal_degrees float not null default 0.0,
   epoch int not null default 2000,
   is_binary int not null default 0,
   is_multiple int not null default 0,
   is_binary_component int not null default 0,
   is_eclipsing_binary int not null default 0,
   notes varchar(1024) not null default ""
);

insert into Stars (id, constellation_id, common_name) values (0, 0, "dummy");


/* * (1) Stars */

/* ** (2) 001 Sirius */

replace into Stars (id,
                    constellation_id,
		    common_name,
                    bayer_designation_greek_letter,
                    right_ascension_hours,
   		    right_ascension_minutes,
   		    right_ascension_seconds,
   		    declination_degrees,
   		    declination_minutes,
   		    declination_seconds)
values
(1,
1,
"Sirius",
"alpha", 
6,
45,
08.917,
-16,
42,
58.02
);

/* ** (2) 002  Alpha Carinae Canopus */


/* ** (2) Select from Stars/Constellations  */

select S.id, S.common_name, S.constellation_id, S.bayer_designation_greek_letter,
C.name_genitive as "const. genitive", C.abbreviation  as "const. abbreviation",
C.name as "const. name",
S.right_ascension_hours,
S.right_ascension_minutes,
S.right_ascension_seconds,
S.declination_degrees,
S.declination_minutes,
S.declination_seconds
from Stars as S, Constellations as C
where S.id > 0 and S.constellation_id = C.id order by S.id;


/* ** (2) */

/* * (1) Constellations */


replace into Constellations (id, name, name_genitive, abbreviation)
values
(1, "Canis Major", "Canis Majoris", "CMa");


select * from Constellations where id > 0 order by id;



-- Numbers are `approx_rank_apparent_magnitude'

1. Alpha Canis Majoris       Sirius            
2  Alpha Carinae             Canopus
3, Alpha Centauri,           Rigil Kentaurus
4, Alpha Boötis,             Arcturus
5, Alpha Lyra,               Vega
6, Alpha Auriga,             Capella
7, Beta Orionis,             Rigel
8, Alpha Canis Minor,        Procyon
9, Alpha Eridani,            Achernar
10, Alpha Orionis,           Betelgeuse

/* Local Variables:                   */
/* eval:(outline-minor-mode t)        */
/* outline-regexp:"/\\* \\*+"         */
/* fill-column:80                     */
/* End:                               */
