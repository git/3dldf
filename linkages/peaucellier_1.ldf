%% peaucellier_1.ldf
%% Created by Laurence D. Finston (LDF) So 21. Jul 13:05:15 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

% Dimensions:
% Green Triangle = a, a, b (a are the longer sides)
% Yellow Links = a
% Horizontal Distance between Ground Joints = 2b
  
input "plainldf.lmc";

medium_pen := pencircle scaled (.333mm, .333mm, .333mm);

point p[];
path q[];
picture v[];
transform t[];
boolean B[];

numeric frame_wd;
numeric frame_ht;

numeric outer_frame_wd;
numeric outer_frame_ht;

frame_wd := 600mm;
frame_ht := 337.5mm;

frame_wd += 8mm;
frame_ht += 8mm;

outer_frame_wd := frame_wd + 10cm;
outer_frame_ht := frame_ht + 10cm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));

string s;

boolean do_labels;
boolean do_black;
boolean do_png;

do_labels := false; % true;  % 
do_png    := true; % false;  %
do_black  := true;  % false; %

color line_color;
color curr_color;

point temp_pt;

bool_point_vector bpv;

curr_color := black;

if do_black:
  do_labels := false;
  line_color := white;
else:
    line_color := black;
fi;


if do_png:
  %do_labels := false;

  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%4c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";";
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";";
fi;

verbatim_metapost "ahlength := 1cm;;dotlabeldiam:=6mm;";
verbatim_tex   "\magnification=7000\font\smallrm=cmr10 scaled 600";


pen Big_dot_pen;
Big_dot_pen := pencircle scaled (8mm, 8mm, 8mm);

Big_pen := pencircle scaled (6mm, 6mm, 6mm);
big_pen := pencircle scaled (3mm, 3mm, 3mm);
big_square_pen := pencircle scaled (2mm, 2mm, 2mm);

%% * (1)

fig_ctr := 0;

color frame_color;

frame_color := black; % blue; % 

circle c[];
numeric ang[]; % angles
numeric C[];   % circumferences
numeric L[];   % lengths (distances along circle)

%% Wikipedia article:  https://en.wikipedia.org/wiki/Watt%27s_linkage

%% * (1)

pickup big_pen;

%% * (1) Set up linkage.

beginfig(fig_ctr);

%% ** (2)

  message "fig_ctr:  " & decimal fig_ctr;

  % if do_black:
  %   fill frame with_color black;
  % else:
  %   fill frame with_color white;
  % fi;

  fill frame with_color white;
  
  
  draw frame with_pen big_square_pen with_color frame_color;

  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2)

  p0 := (-20, 0);
  p1 := (-10, 0);
  p2 := (10, 0);
  p3 := (20, 0);
  p4 := (0, 9);
  p5 := (0, -9);

  draw p5 -- p0;
  draw p5 -- p1;
  draw p5 -- p2;
  draw p5 -- p3;

  draw p4 -- p0;
  draw p4 -- p1;
  draw p4 -- p2;
  draw p4 -- p3;
  
  
  if do_labels:
    dotlabel.lft("$p_0$", p0);
    dotlabel.lft("$p_1$", p1);
    dotlabel.rt("$p_2$", p2);
    dotlabel.rt("$p_3$", p3);
    dotlabel.urt("$p_4$", p4);
    dotlabel.lrt("$p_5$", p5);
  fi;

  a := magnitude(p4 - p0);

  c0 := ((unit_circle scaled (a, 0, a)) rotated (90, 0)) shifted by p0;

  b := magnitude(p3 - p5);

  d := magnitude(p1 - p5);

  draw c0 with_color red;

  C0 := 2pi * a;

  
%% ** (2)

  scale current_picture (.6667, .6667, .6667);
  shift current_picture (2, 0);
  
  output current_picture with_projection parallel_x_y;
  clear current_picture;
  clip current_picture to frame;
  draw frame with_pen big_square_pen with_color frame_color;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop

for i = 0 step 2.5 until 360:
  beginfig(fig_ctr);

    message "fig_ctr:  " & decimal fig_ctr;

    if do_black:
      fill frame with_color black;
    else:
      fill frame with_color white;
    fi;
    
    draw frame with_pen big_square_pen with_color frame_color;

    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% ** (2)

    ang0 := i;
    L0 := i * C0 / 360;

    if not do_black:
      draw c0 with_color red;
    fi;
    

    p6 := p4 rotated_around (p0, p0 shifted (0, 0, 1)) -ang0;
    

    p7 := p5 rotated_around (p0, p0 shifted (0, 0, 1)) ang0;



    p8 := p0 rotated_around (p6, p7) 180;


    p9  := p2 shifted (xpart p8 - xpart p3, 0);
    p10 := p1 shifted (-(xpart p8 - xpart p3), 0);

    n := magnitude(p2 - p4);

    c1 := ((unit_circle scaled (n, 0, n)) rotated (90, 0)) shifted by p6;
    C1 := 2pi * n;

    if not do_black:
      draw c1 with_color green;
    fi;
    
    
    % ang1 := (L0 * 360) / C1;

    clear bpv;
    bpv := c1 intersection_points (p0 -- p8);

    p9      := INVALID_POINT;
    temp_pt := INVALID_POINT;

    if (size bpv) > 0:
      p9 := bpv0;
    fi;

    if (size bpv) > 1:
      temp_pt := bpv1;
    fi;
    

    if (is_valid p9) and (is_valid temp_pt):
      if (xpart p9) < (xpart temp_pt):
     	p10 := p9;
     	p9 := temp_pt;
      fi;
      if ((xpart p9) <= (xpart p8)) and ((xpart p9) >= (xpart p0)) and
	 ((xpart p10) <= (xpart p8)) and ((xpart p10) >= (xpart p0)) and

	  ((((ypart p9) <= (ypart p6)) and ((ypart p9) >= (ypart p7)) and
	      ((ypart p10) <= (ypart p6)) and ((ypart p10) >= (ypart p7)))
	    or
	    (((ypart p9) >= (ypart p6)) and ((ypart p9) <= (ypart p7)) and
	      ((ypart p10) >= (ypart p6)) and ((ypart p10) <= (ypart p7)))):
	  curr_color := line_color;
      else:
	curr_color := gray;
      fi;
    fi;
        

    % message "p9:";
    % show p9;

    % message "temp_pt:";
    % show temp_pt;
    % pause;
    
    
    draw p0 -- p6 -- p8 -- p7 -- cycle with_color line_color;

    if (is_valid p9) and (is_valid p10):
      draw p7 -- p9 -- p6 -- p10 -- cycle with_color Salmon_rgb;
    fi; 
    

    if do_labels:
      dotlabel.lft("$p_0$", p0);
      %dotlabel.ulft("$p_1$", p1);
      %dotlabel.bot("$p_2$", p2);
      %dotlabel.bot("$p_3$", p3);
      %dotlabel.urt("$p_4$", p4);
      %dotlabel.bot("$p_5$", p5);
      dotlabel.urt("$p_6$", p6);
      dotlabel.lrt("$p_7$", p7);
      dotlabel.rt("$p_8$", p8);
      if (is_valid p9) and (is_valid p10):
	dotlabel.rt("$p_9$", p9);
	dotlabel.lft("$p_{10}$", p10);
      fi;
      
    fi;

    if false:
      draw p5 -- p0 with_color red;
      draw p5 -- p1 with_color blue;
      draw p5 -- p2 with_color green;
      draw p5 -- p3 with_color magenta;
      

      draw p4 -- p1 with_color orange;
      draw p4 -- p2 with_color teal_blue_rgb;
      draw p4 -- p3 with_color rose_madder_rgb;
    fi;
    
  
  
%% ** (2)    

    scale current_picture (.6667, .6667, .6667);
    shift current_picture (2, 0);
    
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    clip current_picture to frame;

    if do_labels:
      s := "Angle $=" & decimal i & "^\circ$";
      label.urt(s, mediate(get_point 0 frame, get_point 1 frame, 2/3) shifted (0, .5));
    fi;


    draw frame with_pen big_square_pen with_color frame_color;
    
endfig with_projection parallel_x_y;
fig_ctr += 1;
    
endfor;


%% * (1)

message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
if not do_png:
  pause;
fi;

bye;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

