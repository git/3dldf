%% letter_chart_astrosym.ldf
%% Created by Laurence D. Finston (LDF) Fr 9. Aug 09:50:54 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)


input "plainldf.lmc";
input "glyphmac.lmc";

medium_pen := pencircle scaled (.333mm, .333mm, .333mm);

dot_pen := pencircle scaled (4mm, 4mm, 4mm);

pen small_dot_pen;
small_dot_pen := pencircle scaled (2mm, 2mm, 2mm);

numeric overshoot_val;

overshoot_val := .333;

verbatim_tex   "\magnification=4500\font\smallrm=cmr10 scaled 600";

verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";dotlabeldiam:=4mm;"
                  & "labeloffset := 6bp;";

verbatim_metapost "ahlength := .5cm;";

point p[];
path q[];
path Q[];
picture v[];
transform t[];

numeric frame_wd;
numeric frame_ht;

numeric outer_frame_wd;
numeric outer_frame_ht;

frame_wd := 770mm; % 600mm;
frame_ht := 510mm; % 337.5mm;

  
frame_wd += 8mm;
frame_ht += 8mm;

outer_frame_wd := frame_wd + 10cm;
outer_frame_ht := frame_ht + 10cm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));

rectangle outer_frame;

outer_frame := ((-.5outer_frame_wd, -.5outer_frame_ht), (.5outer_frame_wd, -.5outer_frame_ht),
          (.5outer_frame_wd, .5outer_frame_ht), (-.5outer_frame_wd, .5outer_frame_ht));


for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

p4 := get_center frame;

pickup medium_pen;

string s;

boolean do_labels;
do_labels := true; % false;

color_vector cv;

cv += red;
cv += blue;
cv += dark_green;
cv += violet;
cv += orange;
cv += magenta;
cv += teal_blue_rgb;
cv += rose_madder_rgb;

path_vector qv;

fig_ctr := 0;

input "glyphs_astrosym.ldf";

%% * (1)
  
message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
%pause;

bye;


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

