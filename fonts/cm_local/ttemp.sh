#! /bin/bash

#### ttemp.sh
#### Created by Laurence D. Finston (LDF) Mo 8. Jul 18:47:00 CEST 2024


git add -f ./cmti10/letter_chart_cmti10.dvi
git add -f ./cmitt10/letter_chart_cmitt10.dvi
git add -f ./cmss10/letter_chart_cmss10.dvi
git add -f ./cmssbx10/letter_chart_cmssbx10.dvi
git add -f ./cmtt10/letter_chart_cmtt10.dvi
git add -f ./cmr10/letter_chart_cmr10.dvi
git add -f ./cmdunh10/letter_chart_cmdunh10.dvi
git add -f ./cmsy10/letter_chart_cmsy10.dvi

git add -f ./cmti10/letter_chart_cmti10.pdf
git add -f ./cmitt10/letter_chart_cmitt10.pdf
git add -f ./cmss10/letter_chart_cmss10.pdf
git add -f ./cmssbx10/letter_chart_cmssbx10.pdf
git add -f ./cmtt10/letter_chart_cmtt10.pdf
git add -f ./cmr10/letter_chart_cmr10.pdf
git add -f ./cmdunh10/letter_chart_cmdunh10.pdf
git add -f ./cmsy10/letter_chart_cmsy10.pdf


exit 0

git add -f ./cmvtt10/cmvtt10.dvi
git add -f ./cmitt10/cmitt10.dvi
git add -f ./cmss10/cmss10.dvi
git add -f ./cmssi10/cmssi10.dvi
git add -f ./cmssi10/cmtex10/cmtex10.dvi
git add -f ./cmsl10/cmsl10.dvi
git add -f ./cmssbx10/cmssbx10.dvi
git add -f ./cmtt10/cmtt10.dvi
git add -f ./cmr10/cmr10.dvi
git add -f ./cmbxsl10/cmbxsl10.dvi
git add -f ./cmdunh10/cmdunh10.dvi
git add -f ./cmbx10/cmbx10.dvi
git add -f ./cmsy10/cmsy10.dvi
git add -f ./cminch/cminch.dvi
git add -f ./cmsltt10/cmsltt10.dvi

git add -f ./cmvtt10/cmvtt10.pdf
git add -f ./cmitt10/cmitt10.pdf
git add -f ./cmss10/cmss10.pdf
git add -f ./cmssi10/cmssi10.pdf
git add -f ./cmssi10/cmtex10/cmtex10.pdf
git add -f ./cmsl10/cmsl10.pdf
git add -f ./cmssbx10/cmssbx10.pdf
git add -f ./cmtt10/cmtt10.pdf
git add -f ./cmr10/cmr10.pdf
git add -f ./cmbxsl10/cmbxsl10.pdf
git add -f ./cmdunh10/cmdunh10.pdf
git add -f ./cmbx10/cmbx10.pdf
git add -f ./cmsy10/cmsy10.pdf
git add -f ./cminch/cminch.pdf
git add -f ./cmsltt10/cmsltt10.pdf

exit 0

