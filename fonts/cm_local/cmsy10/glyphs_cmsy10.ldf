%% glyphs_cmsy10.ldf
%% Created by Laurence D. Finston (LDF) Mi 22. Mai 05:42:35 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Card suits done.

numeric letter_chart_frame_wd;
numeric letter_chart_frame_ht;

letter_chart_frame_wd := 770mm; % 600mm;
letter_chart_frame_ht := 510mm; % 337.5mm;
  
letter_chart_frame_wd += 8mm;
letter_chart_frame_ht += 8mm;

rectangle letter_chart_frame;

letter_chart_frame := ((-.5letter_chart_frame_wd, -.5letter_chart_frame_ht),
                       (.5letter_chart_frame_wd, -.5letter_chart_frame_ht),
                       (.5letter_chart_frame_wd, .5letter_chart_frame_ht),
                       (-.5letter_chart_frame_wd, .5letter_chart_frame_ht));

for i = 0 upto 3:
  p[i] := get_point (i) letter_chart_frame;
endfor;

p4 := get_center letter_chart_frame;



%% * (1) Fig. 0.

pickup big_pen;

beginfig(fig_ctr);

%% ** (2)  
  
  draw frame with_pen big_pen;



  string font_name;
  font_name := "cmsy10";
  color font_color;
  font_color := Apricot_rgb;
  color label_color;
  label_color := blue;
  numeric_vector nv;

% 124--127:  Club, Diamond, Heart, Spade

%% ** (2)
  
  glyph club;

  % nv += 0;
  % nv += 22;
  
  letter (club) {font_name, font_color, 124, 1, 0, 5, nv, p3, 2, -16, false, % true, % 
          16, 0,
	  0, 0,
	  -4, -5.25,
	  0, 0,
	  0, 0,
	  0, 0,
    false, false, false};

%% ** (2)
  
  glyph diamond;

  letter (diamond) {font_name, font_color, 125, 2, 2, 10, nv, p6, 4, 0, false, % true, % 
          0, 4,
	  0, 0,
	  -2, -5.25,
	  0, 3,
	  0, 0,
	  0, 0,
    false, false, false};

%% ** (2)
  
  glyph heart;

  letter (heart) {font_name, font_color, 126, 2, 5, 15, nv, p11, 4, 0, false, % true, % 
          2, 6,
	  0, 0,
	  -2, -4.25,
	  0, 1.75,
	  0, 0,
	  0, 0,
    false, false, false};

%% ** (2)
  
  glyph spade;
  
  letter (spade) {font_name, font_color, 127, 1, 8, 20, nv, p16, 4, 0, false, % true, % 
          12, 0,
	  0, 0,
	  -2, -5.5,
	  0, 0,
	  0, 0,
	  0, 0,
    false, false, false};

  

  
%% ** (2)  
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1)

save p, q with_prefix "cmsy10" overwrite;

%% * (1)

  
endinput;


%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

