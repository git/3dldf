%% o_letter.ldf
%% Created by Laurence D. Finston (LDF) Fr 10. Mai 06:47:29 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

message "Entering o_letter.ldf";

beginfig(fig_ctr);

  message "Fig. " & decimal fig_ctr;
  
  draw frame with_pen big_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := v0;

%% ** (2)

  temp_pt0 := (xpart p65, 0, zpart p61);  
  
  q125 := (q25 scaled (.6667, 0, .6667) rotated (0, 0, 180))
    shifted by (mid_pt_vector1 - (temp_pt0 scaled (.6667, 0, .6667) rotated (0, 0, 180)));

  q126 := (q26 scaled (.6667, 0, .6667) rotated (0, 0, 180))
    shifted by (mid_pt_vector1 - (temp_pt0 scaled (.6667, 0, .6667) rotated (0, 0, 180)));

  
  rotate_around q125 (vv2, vv1) 180;
  rotate_around q126 (vv2, vv1) 180;

  q225 := q125;
  q226 := q126;
   
  rotate_around q125 (vv0, mid_pt_vector1) 180;
  rotate_around q126 (vv0, mid_pt_vector1) 180;

  %shift q125 by (mediate(vv2, vv1, .5) - mid_pt_vector1);
  %shift q126 by (mediate(vv2, vv1, .5) - mid_pt_vector1);

  shift q125 by (mediate(mid_pt_vector1, vv0, .0825) - mid_pt_vector1);
  shift q126 by (mediate(mid_pt_vector1, vv0, .0825) - mid_pt_vector1);
  
  % dh := get_dihedral_angle P0;

  % rotate_around q225 (vv1, vv2) 70.53;

  % q300 := q225 rotated_around (vv3, vv2) -dh;
  % rotate_around q300 (mid_pt_vector2, vv3) 180;

  % shift q300 by (mediate(vv2, vv0, .4) - mid_pt_vector2);
     
  % q400 := q300 rotated_around (vv3, vv0) -dh;
  % rotate_around q400 (mid_pt_vector0, vv3) 180;
  % %shift q400 by (mediate(vv0, vv1, .4) - mid_pt_vector0);

  filldraw q125 with_fill_color cv0;
  unfill q126;
  
  rotate current_picture (-90, 30);

  v1 := current_picture;

endfig with_focus f;
fig_ctr += 1;

%% * (1) Main loop for o_letter

if true: % false: % 
  for loop_ctr = 0 upto 72:
    beginfig(fig_ctr);

      message "Fig. " & decimal fig_ctr;
      
      draw frame with_pen big_pen;
      output current_picture with_projection parallel_x_y;
      clear current_picture;

      current_picture := v1;

      rotate current_picture (0, 5loop_ctr, 0);
      
      
    endfig with_focus f;
    fig_ctr += 1;
  endfor;
fi;

%% * (1)

message "Exiting o_letter.ldf";

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2024.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
