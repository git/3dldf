%% lay_out_0.ldf
%% Created by Laurence D. Finston (LDF) Sa 8. Jun 15:43:45 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

message "Entering lay_out_0.ldf";

do_labels     := true; % false;
do_help_lines := true; % false;

if false: % true: % 
  do_labels     := false; % true;
  do_help_lines := false; % true;
fi;

if do_png:
  do_labels     := false;
  do_help_lines := false;
fi;

if do_labels:
  background_color := white;
else:
  background_color := dark_green; % Salmon_rgb; % CornflowerBlue_rgb;
fi;


% clubs:     0--12
% diamonds: 13--25
% hearts:   26--38
% spades:   39--51


V100 := V17; % 5D
V101 := V42; % 4S
V102 := V32; % 7H
V103 := V2;  % 3C
V104 := V45; % 7S
V105 := V22; % 10D
V106 := V4;  % 5C
V107 := V25; % KD
V108 := V24; % QD
V109 := V3;
V110 := V28;
V111 := V31;
V112 := V27;
V113 := V30;
V114 := V0;
V115 := V15;
V116 := V11;
V117 := V46;
V118 := V33;
V119 := V16;
V120 := V29;
V121 := V35;
V122 := V26;
V123 := V13;
V124 := V36;
V125 := V12;
V126 := V21;
V127 := V48;
V128 := V34;
V129 := V20;
V130 := V44;
V131 := V39;
V132 := V40;
V133 := V19;
V134 := V50;
V135 := V37;
V136 := V6;
V137 := V49;
V138 := V51;
V139 := V41;
V140 := V10;
V141 := V38;
V142 := V43;
V143 := V7;
V144 := V14;
V145 := V47;
V146 := V18;
V147 := V23;
V148 := V8;
V149 := V9;
V150 := V1;
V151 := V5;

t1 := identity shifted (0, 10, 45);

point L[];
path M[];
M0 += ..;

point N[];

% Card at bottom center, back of card.

V300 := back_of_card_picture;
shift V300 (0, -35, 0);
r100 := r0;
shift r100 (0, -35, 0);

% First card in row, upper third.

V200 := V100;
shift V200 (-70, 15, 0);
r101 := r0;
shift r101 (-70, 15, 0);

numeric rectangle_point_ctr[];

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture += V300; % Comment back in for back of card.
  %current_picture += V200;

  for i = 0 upto 3:
    L[i] := get_point (i) r101;
  endfor;

  for i = 0 upto 3:
    n := (i+1) mod 4;
    L[4+i] := mediate(get_point (i) r101, get_point (n) r101);
  endfor;

  if do_labels:
    label.bot("$r_{100}$", mediate(get_point 0 r100, get_point 1 r100));
    dotlabel.lrt("$L_0$", L0);
    dotlabel.bot("$L_1$", L1);
    dotlabel.top("$L_2$", L2);
    dotlabel.urt("$L_3$", L3);
    dotlabel.bot("$L_4$", L4);
    dotlabel.llft("$L_5$", L5);
    dotlabel.top("$L_6$", L6);
    dotlabel.rt("$L_7$", L7);
  fi;


  if do_help_lines:
    draw r100 with_color green with_pen Big_pen;
    draw r101 with_color magenta with_pen Big_pen;
  fi;

%% ** (2)

  L8 := get_center r100;
  L9 := get_center r101;

  if do_help_lines:
    draw L8 -- L9 with_color dark_green with_pen Big_pen;
  fi;


  L10 := mediate(L8, L9);

  if do_labels:
    label.lft("$L_8\longrightarrow$", L8 shifted (-10, 0));
    label.top("$r_{101}$", L9);
    dotlabel.bot("$L_9$", L9);
    dotlabel.lft("$L_{10}$", L10);
  fi;


%% ** (2) Lay out the top row of rectangles.

  for i = 0 upto 5:
    r[100+i+2] := (r[100+i+1] shifted by (L1 - L0)) shifted (4.6667, 0);
    if do_help_lines:
      draw r[100+i+2] with_color magenta;
    fi;
    L[11+i] := get_center r[100+i+2];
    if do_labels:
      s := "$L_{" & decimal (11+i) & "}$";
      dotlabel.bot(s, L[11+i]);
      s := "$r_{" & decimal (100+i+2) & "}$";
      label.top(s, L[11+i]);
    fi;
  endfor;
    
%% ** (2) Draw lines from the center of r100 to the centers of the upper rectangles.

  point temp_pt;
  
  point_ctr := 17;
  for i = 11 upto 16:
    if do_help_lines:
      draw L8 -- L[i] with_color dark_green;
    fi;
    
    L[point_ctr] := mediate(L8, L[i]);
    if do_labels:
      s := "$L_{" & decimal point_ctr & "}$";
      dotlabel.lft(s, L[point_ctr]);
    fi;
    point_ctr += 1;
  endfor;

  ellipse e[];

%% ** (2) Rectangle 108 for ellipse for leftmost card (r101).
  
  L23 := L8 shifted (0, 0, 6);
  L24 := L8 shifted (0, 0, -6);
  L25 := L9 shifted (0, 0, -6);
  L26 := L9 shifted (0, 0, 6);

  set r108 (L23, L24, L25, L26);


  point normal[];
  point center[];

  center0 := get_center r108;
  normal0 := get_normal r108;
  shift normal0 by center0;

  t4 := align (center0 -- normal0) with_axis y_axis;
  
  mag := magnitude(L9 - L8);

  e0 := unit_ellipse scaled (.5mag, 0, 6);

  e0 *= inverse t4;

  if do_help_lines:
    draw r108 with_color blue;
    draw e0 with_color red;
  fi;

  m := (size e0) / 2;
  n := (size e0) - 1; % size e0 == 32.

  point_ctr := 27;
  rectangle_point_ctr1 := point_ctr;
  for i = m upto n:
    L[point_ctr] := get_point (i) e0;
    if do_labels:
      %s := "$L_{" & decimal (point_ctr) & "}$";
      %dotlabel.bot(s, L[point_ctr]) with_color red;
    fi;
    point_ctr += 1;
  endfor;
  
%% ** (2) Rectangle 109 for ellipse for second card (r102).

  L[point_ctr]   := L11 shifted (0, 0, -6);
  L[point_ctr+1] := L11 shifted (0, 0, 6);
  
  set r109 (L23, L24, L[point_ctr], L[point_ctr+1]);

  point_ctr += 2;


  if do_help_lines:
    draw r109 with_color blue;
  fi;

  center1 := get_center r109;
  normal1 := get_normal r109;
  shift normal1 by center1;

  t6 := align (center1 -- normal1) with_axis y_axis;
  
  mag := magnitude(L11 - L8);

  e1 := unit_ellipse scaled (.5mag, 0, 6);

  e1 *= inverse t6;

  if do_help_lines:
    draw e1 with_color red;
  fi;

  m := (size e1) / 2;
  n := (size e1) - 1; % size e1 == 32.

  rectangle_point_ctr2 := point_ctr;
  for i = m upto n:
    L[point_ctr] := get_point (i) e1;
    if do_labels:
      %s := "$L_{" & decimal (point_ctr) & "}$";
      %dotlabel.bot(s, L[point_ctr]) with_color red;
    fi;
    point_ctr += 1;
  endfor;

%% ** (2) Rectangle 110--114 for ellipses for third--7th card (r103--r107).


  for i = 3 upto 7:
    L[point_ctr]   := L[i+9] shifted (0, 0, -6);
    L[point_ctr+1] := L[i+9] shifted (0, 0, 6);
    
    set r[107+i] (L23, L24, L[point_ctr], L[point_ctr+1]);

    point_ctr += 2;
    
    if do_help_lines:
      draw r[107+i] with_color blue;
    fi;


    
    center[i-1] := get_center r[107+i];
    normal[i-1] := get_normal r[107+i];
    shift normal[i-1] by center[i-1];

    t[i+4] := align (center[i-1] -- normal[i-1]) with_axis y_axis;
    
    mag := magnitude(L[i+9] - L8);

    e[i-1] := unit_ellipse scaled (.5mag, 0, 6);

    if i >= 4:
      rotate e[i-1] (0, 0, 180);
    fi;
    

    e[i-1] *= inverse t[i+4];

    if do_help_lines:
      draw e[i-1] with_color red;
    fi;

      
    m := (size e[i-1]) / 2;
    n := (size e[i-1]) - 1; % size e[i-1] == 32.

    rectangle_point_ctr[i] := point_ctr;

    for j = m upto n:
      L[point_ctr] := get_point (j) e[i-1];
      if do_labels:
	%s := "$L_{" & decimal (point_ctr) & "}$";
	%dotlabel.bot(s, L[point_ctr]) with_color red;
      fi;
      point_ctr += 1;
    endfor;
  endfor;
 
  
%% ** (2)
  
  W0 := current_picture;
  
%% ** (2)

  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%message "rectangle_ctr: " & decimal rectangle_ctr;
%pause;

%endinput;

%% * (1) Loop 1

% Check what value to use for rectangle_ctr after I've done the
% perpendicular ones for the other cards.
% It will be much less than 300.
% LDF 2024.06.09.


rectangle_ctr := 300;
for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr1+loop_ctr];
  message "loop_ctr: " & decimal loop_ctr;
  message "Shifting r" & decimal rectangle_ctr & " to point L" & decimal (rectangle_point_ctr1+loop_ctr);

  
  if do_help_lines:
    draw r[rectangle_ctr];
  fi;

  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr1+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V100;
    rotate V301 (0, 180);
  fi;

  t5 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr1+loop_ctr];

  V301 *= t5;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;
%pause;

%% ** (2) Final position 1

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 2

V201 := V101;
shift V201 by L11;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  current_picture += V200;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr2+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr2+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V101;
    rotate V301 (0, 180);
  fi;

  t[i+4] := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr2+loop_ctr];

  V301 *= t[i+4];

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 2

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  current_picture += V201;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 3, 7H

t100 := identity rotated (0, 0, 180);
V202 := V102;
V202 *= t100;
shift V202 by L12;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr3+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr3+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V102;
    rotate V301 (0, 180);
  fi;

  t100 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr3+loop_ctr];

  V301 *= t100;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 3

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 4, 3C.

t100 := identity rotated (0, 0, 180);
V203 := V103;
V203 *= t100;
shift V203 by L13;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr4+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr4+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V103;
    rotate V301 (0, 180);
  fi;

  t100 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr4+loop_ctr];

  V301 *= t100;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 4

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 5, 7S

t100 := identity rotated (0, 0, 180);
V204 := V104;
V204 *= t100;
shift V204 by L14;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr5+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr5+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V104;
    rotate V301 (0, 180);
  fi;

  t100 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr5+loop_ctr];

  V301 *= t100;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 5

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  current_picture += V204;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 6, 10D

t100 := identity rotated (0, 0, 180);
V205 := V105;
V205 *= t100;
shift V205 by L15;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  current_picture += V204;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr6+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr6+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V105;
    rotate V301 (0, 180);
  fi;

  t100 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr6+loop_ctr];

  V301 *= t100;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 6, 5C

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  current_picture := W0;
  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  current_picture += V204;
  current_picture += V205;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop 7

t100 := identity rotated (0, 0, 180);
V206 := V106;
V206 *= t100;
shift V206 by L16;

for loop_ctr = 1 upto 15:
beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  %current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  current_picture += V204;
  current_picture += V205;
  
  r[rectangle_ctr] := (r0 rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr7+loop_ctr];

  if do_help_lines:
    draw r[rectangle_ctr];
  fi;
  
  if do_labels:
    s := "$r_{" & rectangle_ctr & "}$";
    label.top(s, L[rectangle_point_ctr7+loop_ctr]);
  fi;
  

  if loop_ctr < 8:
    V301 := back_of_card_picture;
  else:
    V301 := V106;
    rotate V301 (0, 180);
  fi;

  t100 := (identity rotated (-11.25loop_ctr, 0, 0)) shifted L[rectangle_point_ctr7+loop_ctr];

  V301 *= t100;

  current_picture += V301;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;
rectangle_ctr += 1;
endfor;

%% ** (2) Final position 7

beginfig(fig_ctr);

  fill frame with_color background_color; 
  draw frame with_pen big_square_pen;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  %current_picture := W0;

  current_picture += V200;
  current_picture += V201;
  current_picture += V202;
  current_picture += V203;
  current_picture += V204;
  current_picture += V205;
  current_picture += V206;
  
  current_picture *= t1;
  
  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen;
  
endfig with_projection parallel_x_y;
fig_ctr += 1;








%% ** (2)

%% * (1)

message "Exiting lay_out_0.ldf";

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
