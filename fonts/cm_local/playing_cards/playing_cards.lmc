%% playing_cards.lmc
%% Created by Laurence D. Finston (LDF) Fr 31. Mai 06:07:05 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)


%% * (1)

macro ace;
macro two;
macro three;
macro four;
macro five;
macro six;
macro seven;
macro eight;
macro nine;
macro ten;
macro jack;
macro queen;
macro king;

path  suit_path[];
point suit_point[];
color suit_color[];
color label_color[];

suit_path0   := q0;
suit_point0  := p9;
suit_color0  := black;
label_color0 := red;

suit_path1   := q2;
suit_point1  := p14;
suit_color1  := red;
label_color1 := black;

suit_path2   := q5;
suit_point2  := p19;
suit_color2  := red;
label_color2 := black;

suit_path3   := q8;
suit_point3  := p24;
suit_color3  := black;
label_color3 := red;

%% * (1) macro ace

% Adds 2 paths.

def ace {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'ace'.";
  message "Fig. " & decimal fig_ctr;
  
  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Ace

    message "suit_ctr:    " & decimal suit_ctr;
    message "path_index:  " & decimal path_index;

    D[path_index] := (A40 scaled (.25, .25)) shifted by (C9 - B94 scaled (.25, .25)); % A, cmr10
    D[path_index+1] := (A41 scaled (.25, .25)) shifted by (C9 - B94 scaled (.25, .25));

    fill D[path_index] with_color suit_color[suit_ctr];
    unfill D[path_index+1];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];
    unfill D[path_index+1] * T0;

%% ** (2) Small suit symbol, upper left.

    D[path_index+1] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25));

    fill D[path_index+1]      with_color suit_color[suit_ctr];
    fill D[path_index+1] * T0 with_color suit_color[suit_ctr];

%% ** (2) Medium-sized suit symbols at middle of card.

    if do_middle_figures:

      D[path_index+2] := (suit_path[suit_ctr] scaled (1, 1))
	shifted by (C4 - suit_point[suit_ctr] scaled (1, 1));
      
      fill D[path_index+2] with_color suit_color[suit_ctr];

    fi;

%% ** (2)    
    
    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
    %shift current_picture (0, 5, 10);
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'ace'.";

enddef;

%% * (1) macro two

% Adds three paths.

def two {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'two'.";
  message "Fig. " & decimal fig_ctr;
  
  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Two

    message "suit_ctr:    " & decimal suit_ctr;
    message "path_index:  " & decimal path_index;

    D[path_index] := (A129 scaled (.25, .25)) shifted by (C9 - B298 scaled (.25, .25)); % 2, cmr10

    fill D[path_index] with_color suit_color[suit_ctr];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];

%% ** (2) Small suit symbol, upper left.

    D[path_index+1] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25));

    fill D[path_index+1]      with_color suit_color[suit_ctr];
    fill D[path_index+1] * T0 with_color suit_color[suit_ctr];

%% ** (2) Medium-sized suit symbols at middle of card.

    if do_middle_figures:

      D[path_index+2] := (suit_path[suit_ctr] scaled (.5, .5))
	shifted by (C13 - suit_point[suit_ctr] scaled (.5, .5));
      
      fill D[path_index+2] with_color suit_color[suit_ctr];

      D[path_index+3] := (suit_path[suit_ctr] scaled (.5, .5))
	shifted by (C14 - suit_point[suit_ctr] scaled (.5, .5));

      rotate_around D[path_index+3] (C14, C15) 180;
      
      fill D[path_index+3] with_color suit_color[suit_ctr];

    fi;

%% ** (2)    
    
    draw r0 with_pen big_square_pen;    

    V[picture_index] := current_picture;
    
    %shift current_picture (0, 5, 10);
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'two'.";

enddef;

%% * (1) macro three

def three {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'three'.";
  message "Fig. " & decimal fig_ctr;

  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Three

    message "suit_ctr:    " & decimal suit_ctr;
    message "path_index:  " & decimal path_index;
    
    % Three
  
    D[path_index] := (A131 scaled (.25, .25)) shifted by (C9 - B303 scaled (.25, .25)); % 3, cmr10

    fill D[path_index] with_color suit_color[suit_ctr];
    fill D[path_index] * T0 with_color suit_color[suit_ctr];

    % Small suit symbols

    % if do_help_lines:
    %   draw tri0 with_color dark_gray;
    % fi;

    D[path_index+1] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25));

    fill D[path_index+1] with_color suit_color[suit_ctr];
    fill D[path_index+1] * T0 with_color suit_color[suit_ctr];

    if do_middle_figures:
      D[path_index+2] := ((suit_path[suit_ctr] scaled (.4, .4))
	  shifted by (C18 - suit_point[suit_ctr] scaled (.4, .4))) rotated_around (C18, C19) 180;
    
      D[path_index+3] := D[path_index+2] shifted by (C17 - C18);

      D[path_index+4] := (suit_path[suit_ctr] scaled (.4, .4))
	shifted by (C16 - suit_point[suit_ctr] scaled (.4, .4));

      fill D[path_index+2] with_color suit_color[suit_ctr];
      fill D[path_index+3] with_color suit_color[suit_ctr];
      fill D[path_index+4] with_color suit_color[suit_ctr];
    fi;

%% ** (2)    

    draw r0 with_pen big_square_pen;
    
    V[picture_index] := current_picture;

%% ** (2)    
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'three'.";

enddef;

%% * (1) macro four

def four {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'four'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2)    
    
    % Four
  
    D[path_index] := (A133 scaled (.25, .25)) shifted by (C9 - B308 scaled (.25, .25)); % 3, cmr10
    D[path_index+1] := (A134 scaled (.25, .25)) shifted by (C9 - B308 scaled (.25, .25)); % 3, cmr10	

    fill D[path_index] with_color suit_color[suit_ctr];
    unfill D[path_index+1];

    % Small suit symbol, corners
      
    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+2] with_color suit_color[suit_ctr];

    for i = 0 upto 2:
      D[path_index+3+i] := D[i+path_index] * T0;
    endfor;

    fill D[path_index+3]  with_color suit_color[suit_ctr];
    unfill D[path_index+4];
    fill D[path_index+5]  with_color suit_color[suit_ctr];	

%% ** (2) Middle figures

    if do_middle_figures:
      
      D[path_index+6] := (suit_path[suit_ctr] scaled (.333, .333))
	shifted by (C20 - suit_point[suit_ctr] scaled (.333, .333)); 

      point temp_pt;

      temp_pt := C20 shifted (0, 0, 1);

      rotate_around D[path_index+6] (C20, temp_pt) 180;
      fill D[path_index+6]  with_color suit_color[suit_ctr];

      D[path_index+7] := D[path_index+6] shifted by (C21 - C20);
      fill D[path_index+7]  with_color suit_color[suit_ctr];

      D[path_index+8] := (suit_path[suit_ctr] scaled (.333, .333))
	shifted by (C22 - suit_point[suit_ctr] scaled (.333, .333)); 
      fill D[path_index+8]  with_color suit_color[suit_ctr];

      D[path_index+9] := (suit_path[suit_ctr] scaled (.333, .333))
	shifted by (C23 - suit_point[suit_ctr] scaled (.333, .333)); 
      fill D[path_index+9]  with_color suit_color[suit_ctr];
    fi;

    
    if do_labels:
      dotlabel.bot("$C_{20}$", C20) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{21}$", C21) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{22}$", C22) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{23}$", C23) with_color label_color[suit_ctr];
    fi;
    
    

%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'four'.";

enddef;

%% * (1) macro five

def five {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'five'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2)    

    % Five
  
    D[path_index] := (A136 scaled (.25, .25)) shifted by (C9 - B313 scaled (.25, .25)); % 5, cmr10

    fill D[path_index] with_color suit_color[suit_ctr];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];

    % Small suit symbol, corners
      
    D[path_index+1] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25));

    fill D[path_index+1] with_color suit_color[suit_ctr];

    fill D[path_index+1] * T0 with_color suit_color[suit_ctr];

%% ** (2) Middle figures

    if do_help_lines:
      draw r2 with_color dark_gray;
    fi;

    if do_labels:
      dotlabel.bot("$C_{24}$", C24);
      dotlabel.bot("$C_{25}$", C25);
      dotlabel.top("$C_{26}$", C26);
      dotlabel.top("$C_{27}$", C27);
    fi;

    if do_middle_figures:

      D[path_index+2] := (suit_path[suit_ctr] scaled (.2875, .2875)) % Right-side up, center
	shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875));

      fill D[path_index+2] with_color suit_color[suit_ctr];
      
      D[path_index+3] := (((suit_path[suit_ctr] scaled (.2875, .2875)) % Upside-down
	    shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875)))
	  rotated_around (C4, C4 shifted (0, 0, 1)) 180)
	shifted by (C24 - C4);

      fill D[path_index+3] with_color suit_color[suit_ctr];
            
      D[path_index+4] := D[path_index+3] shifted by (C25 - C24); % Upside-down

      fill D[path_index+4] with_color suit_color[suit_ctr];
      
      D[path_index+5] := D[path_index+2] shifted by (C27 - C4); % Right-side up

      fill D[path_index+5] with_color suit_color[suit_ctr];

      D[path_index+6] := D[path_index+2] shifted by (C26 - C4); 

      fill D[path_index+6] with_color suit_color[suit_ctr];
      
    fi;
    
    

%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'five'.";

enddef;

%% * (1) macro six

def six {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'six'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
  
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2)    

    % Six
  
    D[path_index] := (A138 scaled (.25, .25)) shifted by (C9 - B318 scaled (.25, .25)); % 6, cmr10
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := (A139 scaled (.25, .25)) shifted by (C9 - B318 scaled (.25, .25)); % 6, cmr10
    unfill D[path_index+1];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];
    unfill D[path_index+1] * T0;

    % Small suit symbol, corners
      
    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25));

    fill D[path_index+2] with_color suit_color[suit_ctr];

    fill D[path_index+2] * T0 with_color suit_color[suit_ctr];

%% ** (2) Middle figures

    if do_help_lines:
      draw rp0 with_color dark_gray;
    fi;

    
    if do_middle_figures:

      D[path_index+3] := (suit_path[suit_ctr] scaled (.2875, .2875)) % Right-side up, center
	shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875));

      fill D[path_index+3] with_color suit_color[suit_ctr];

      D[path_index+4] := (D[path_index+3] rotated_around % Upside-down
	  (C4, C4 shifted (0, 0, 1)) 180)
	shifted by (C30 - C4);

      fill D[path_index+4] with_color suit_color[suit_ctr];


      D[path_index+5] := D[path_index+3] shifted by (C29 - C4); % Right-side up

      fill D[path_index+5] with_color suit_color[suit_ctr];

      D[path_index+6] := D[path_index+3] shifted by (C31 - C4);

      fill D[path_index+6] with_color suit_color[suit_ctr];

      D[path_index+7] := D[path_index+3] shifted by (C32 - C4);

      fill D[path_index+7] with_color suit_color[suit_ctr];

      D[path_index+8] := D[path_index+4] shifted by (C33 - C30); % Upside-down

      fill D[path_index+8] with_color suit_color[suit_ctr];

      
            
      if do_labels:
	dotlabel.top("$C_{4}$", C4) with_color label_color[suit_ctr];
	dotlabel.top("$C_{29}$", C29) with_color label_color[suit_ctr];
	dotlabel.lft("$C_{30}$", C30) with_color label_color[suit_ctr];
	dotlabel.bot("$C_{31}$", C31) with_color label_color[suit_ctr];
	dotlabel.bot("$C_{32}$", C32) with_color label_color[suit_ctr];
	dotlabel.rt("$C_{33}$", C33) with_color label_color[suit_ctr];
      fi;

    fi;

    
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'six'.";

enddef;

%% * (1) macro seven

def seven {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'seven'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

  % Seven
  
  D[path_index] := (A141 scaled (.25, .25)) shifted by (C9 - B323 scaled (.25, .25)); % 7, cmr10
  fill D[path_index] with_color suit_color[suit_ctr];

  D[path_index+1] := D[path_index] * T0;
  fill D[path_index+1] with_color suit_color[suit_ctr];

  if do_help_lines:
    draw rp1 with_color dark_gray;
  fi;

  % Small suit symbol, upper left, lower right.

  D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
    shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

  fill D[path_index+2] with_color suit_color[suit_ctr];

  D[path_index+3] := D[path_index+2] * T0;

  fill D[path_index+3]  with_color suit_color[suit_ctr];


  if do_labels:
    dotlabel.top("$C_{4}$", C4)   with_color label_color[suit_ctr];
    dotlabel.top("$C_{35}$", C35) with_color label_color[suit_ctr];
    dotlabel.lft("$C_{36}$", C36) with_color label_color[suit_ctr];
    dotlabel.bot("$C_{37}$", C37) with_color label_color[suit_ctr];
    dotlabel.bot("$C_{38}$", C38) with_color label_color[suit_ctr];
    dotlabel.rt("$C_{39}$", C39)  with_color label_color[suit_ctr];
    dotlabel.top("$C_{40}$", C40) with_color label_color[suit_ctr];
  fi;    

  if do_middle_figures:
    
    D[path_index+2] := (suit_path[suit_ctr] scaled (.2875, .2875))
      shifted by (C35 - suit_point[suit_ctr] scaled (.2875, .2875)); % Right-side-up

    point temp_pt;

    temp_pt := C35 shifted (0, 0, 1);

    D[path_index+3] := (D[path_index+2] rotated_around (C35, temp_pt) 180) % Upside-down
      shifted by (C36 - C35);


    D[path_index+4] := D[path_index+2] shifted by (C4 - C35); % Right-side up, center
    D[path_index+5] := D[path_index+2] shifted by (C40 - C35); % Right-side up
    D[path_index+6] := D[path_index+2] shifted by (C37 - C35);  
    D[path_index+7] := D[path_index+2] shifted by (C38 - C35);

    D[path_index+8] := D[path_index+3] shifted by (C39 - C36); % Upside-down 
    

    for i = 2 upto 8:
      fill D[path_index+i] with_color suit_color[suit_ctr];
    endfor;

  fi;
  
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'seven'.";

enddef;

%% * (1) macro eight

def eight {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'eight'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

  % Eight
  
    D[path_index] := (A143 scaled (.25, .25)) shifted by (C9 - B328 scaled (.25, .25)); % 8, cmr10
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := (A144 scaled (.25, .25)) shifted by (C9 - B328 scaled (.25, .25)); % 8, cmr10
    unfill D[path_index+1];

    D[path_index+2] := (A145 scaled (.25, .25)) shifted by (C9 - B328 scaled (.25, .25)); % 8, cmr10
    unfill D[path_index+2];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];
    unfill D[path_index+1] * T0;
    unfill D[path_index+2] * T0;

  % Small suit symbol, upper left, lower right.

    D[path_index+3] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+3] with_color suit_color[suit_ctr];
    fill D[path_index+3] * T0 with_color suit_color[suit_ctr];


  if do_middle_figures:

    if do_help_lines:
      draw rp2 with_color dark_gray;
    fi;

    if do_labels:
      dotlabel.top("$C_{41}$", C41) with_color label_color[suit_ctr];
      dotlabel.lft("$C_{42}$", C42) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{43}$", C43) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{44}$", C44) with_color label_color[suit_ctr];
      dotlabel.rt("$C_{45}$", C45) with_color label_color[suit_ctr];
      dotlabel.top("$C_{46}$", C46) with_color label_color[suit_ctr];
      dotlabel.top("$C_{47}$", C47) with_color label_color[suit_ctr];
    fi;
    
    % Right-side-up, center
    D[path_index+4] := (suit_path[suit_ctr] scaled (.2875, .2875))
      shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875)); 

    fill D[path_index+4] with_color suit_color[suit_ctr];

    D[path_index+5] := D[path_index+4] shifted by (C41 - C4); % Right-side up
    D[path_index+6] := D[path_index+4] shifted by (C43 - C4);
    D[path_index+7] := D[path_index+4] shifted by (C46 - C4);

    % Upside-down 
    D[path_index+8] := (D[path_index+4] rotated_around (C4, C4 shifted (0, 0, 1)) 180)
      shifted by (C42 - C4);

    D[path_index+9] := D[path_index+8] shifted by (C47 - C42);
    D[path_index+10] := D[path_index+8] shifted by (C44 - C42);
    D[path_index+11] := D[path_index+8] shifted by (C45 - C42);

    for i = 4 upto 11:
      fill D[path_index+i] with_color suit_color[suit_ctr];
    endfor;
    
  fi;
  
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'eight'.";

enddef;

%% * (1) macro nine

def nine {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'nine'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

  % Nine
  
    D[path_index] := (A147 scaled (.25, .25)) shifted by (C9 - B333 scaled (.25, .25)); % 8, cmr10
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := (A148 scaled (.25, .25)) shifted by (C9 - B333 scaled (.25, .25)); % 8, cmr10
    unfill D[path_index+1];

    fill D[path_index] * T0 with_color suit_color[suit_ctr];
    unfill D[path_index+1] * T0;

  % Small suit symbol, upper left, lower right.

    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+2] with_color suit_color[suit_ctr];
    fill D[path_index+2] * T0 with_color suit_color[suit_ctr];

  if do_middle_figures:	 

    if do_help_lines:
      draw rp3 with_color dark_gray;
    fi;

    D[path_index+3] := (suit_path[suit_ctr] scaled (.2875, .2875)) % Right-side up, center
      shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875));

    D[path_index+4] := D[path_index+3] shifted by (C48 - C4); % Right-side up
    D[path_index+5] := D[path_index+3] shifted by (C55 - C4);
    D[path_index+6] := D[path_index+3] shifted by (C50 - C4);
    D[path_index+7] := D[path_index+3] shifted by (C53 - C4);

    % Upside-down 
    
    D[path_index+8] := (D[path_index+3] rotated_around (C4, C4 shifted (0, 0, 1)) 180) 
      shifted by (C49 - C4);

    D[path_index+9]  := D[path_index+8] shifted by (C54 - C49); 
    D[path_index+10] := D[path_index+8] shifted by (C51 - C49);
    D[path_index+11] := D[path_index+8] shifted by (C52 - C49);

    for i = 3 upto 11:
      fill D[path_index+i] with_color suit_color[suit_ctr];
    endfor;

    if do_labels:
      dotlabel.top("$C_{48}$", C48) with_color label_color[suit_ctr];
      dotlabel.lft("$C_{49}$", C49) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{50}$", C50) with_color label_color[suit_ctr];
      dotlabel.bot("$C_{51}$", C51) with_color label_color[suit_ctr];
      dotlabel.rt("$C_{52}$",  C52) with_color label_color[suit_ctr];
      dotlabel.top("$C_{53}$", C53) with_color label_color[suit_ctr];
      dotlabel.top("$C_{54}$", C54) with_color label_color[suit_ctr];
      dotlabel.urt("$C_{55}$", C55) with_color label_color[suit_ctr];
    fi; 
  fi;
  
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'nine'.";

enddef;

%% * (1) macro ten

def ten {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'ten'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Ten
    
    D[path_index] := (A127 scaled (.25, .25)) shifted by (C9 - B293 scaled (.25, .25)); % 1, cmr10
    shift D[path_index] (-.75, 0);
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := (A124 scaled (.25, .25)) shifted by (C9 - B288 scaled (.25, .25)); % 0 (zero), cmr10
    shift D[path_index+1] (.75, 0);
    fill D[path_index+1] with_color suit_color[suit_ctr];

    D[path_index+2] := (A125 scaled (.25, .25)) shifted by (C9 - B288 scaled (.25, .25)); 
    shift D[path_index+2] (.75, 0);
    unfill D[path_index+2];

    D[path_index+3] := D[path_index] * T0; % 1
    fill D[path_index+3] with_color suit_color[suit_ctr];

    D[path_index+4] := D[path_index+1] * T0; % 0 (zero)
    fill D[path_index+4] with_color suit_color[suit_ctr];

    D[path_index+5] := D[path_index+2] * T0; % 0 (zero)
    unfill D[path_index+5];

    % Small suit symbol, upper left, lower right.

    D[path_index+16] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+16] with_color suit_color[suit_ctr];

    D[path_index+17] := D[path_index+16] * T0;

    fill D[path_index+17]  with_color suit_color[suit_ctr];
    
%% ** (2) Middle figures

    
    if do_middle_figures:
      
      if do_help_lines:
	draw rp4 with_color dark_gray;
      fi;
      
      D[path_index+6] := (suit_path[suit_ctr] scaled (.2875, .2875))
	shifted by (C4 - suit_point[suit_ctr] scaled (.2875, .2875)); % Right-side-up
      fill D[path_index+6] with_color suit_color[suit_ctr];

      D[path_index+7] := D[path_index+6] shifted by (C56 - C4);
      fill D[path_index+7] with_color suit_color[suit_ctr];
      
      D[path_index+8] := D[path_index+6] shifted by (C58 - C4);
      fill D[path_index+8] with_color suit_color[suit_ctr];

      D[path_index+9] := D[path_index+6] shifted by (C63 - C4);
      fill D[path_index+9] with_color suit_color[suit_ctr];
      
      D[path_index+10] := D[path_index+6] shifted by (C60 - C4);
      fill D[path_index+10] with_color suit_color[suit_ctr];

      D[path_index+11] := D[path_index+6] shifted by (C61 - C4);
      fill D[path_index+11] with_color suit_color[suit_ctr];


      D[path_index+12] := (D[path_index+6] rotated_around (C4, C4 shifted (0, 0, 1)) 180)
	shifted by (C57 - C4);
	
      fill D[path_index+12] with_color suit_color[suit_ctr];

      D[path_index+13] := D[path_index+12] shifted by (C64 - C57);
      fill D[path_index+13] with_color suit_color[suit_ctr];

      D[path_index+14] := D[path_index+12] shifted by (C59 - C57);
      fill D[path_index+14] with_color suit_color[suit_ctr];

      D[path_index+15] := D[path_index+12] shifted by (C62 - C57);
      fill D[path_index+15] with_color suit_color[suit_ctr];
	
      if do_labels:
	dotlabel.top("$C_{56}$", C56) with_color label_color[suit_ctr];
	dotlabel.ulft("$C_{57}$", C57) with_color label_color[suit_ctr];
	dotlabel.lft("$C_{58}$", C58) with_color label_color[suit_ctr];
	dotlabel.lft("$C_{59}$", C59) with_color label_color[suit_ctr];
	dotlabel.lft("$C_{60}$", C60) with_color label_color[suit_ctr];
	dotlabel.bot("$C_{61}$", C61) with_color label_color[suit_ctr];
	dotlabel.rt("$C_{62}$", C62) with_color label_color[suit_ctr];
	dotlabel.rt("$C_{63}$", C63) with_color label_color[suit_ctr];
	dotlabel.rt("$C_{64}$", C64) with_color label_color[suit_ctr];
      fi;
  fi;
    
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'ten'.";

enddef;

%% * (1) macro jack

def jack {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'jack'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Jack
    
    D[path_index] := (A92 scaled (.25, .25)) shifted by (C9 - B214 scaled (.25, .25)); % J, cmr10
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := D[path_index] * T0;
    fill D[path_index+1] with_color suit_color[suit_ctr];

    % Small suit symbol, upper left, lower right.

    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+2] with_color suit_color[suit_ctr];

    D[path_index+3] := D[path_index+2] * T0;

    fill D[path_index+3]  with_color suit_color[suit_ctr];
    
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'jack'.";

enddef;


%% * (1) macro queen

def queen {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'queen'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) Queen

    for i = 0 upto 2:
      D[path_index+i] := (A[101+i] scaled (.25, .25)) shifted by (C9 - B234 scaled (.25, .25)); % Q, cmr10
      shift D[path_index+i] (0, -.1875);
    endfor;

    fill D[path_index] with_color suit_color[suit_ctr];
    unfill D[path_index+1];
    unfill D[path_index+2];

    for i = 0 upto 2:
      D[path_index+3+i] := D[path_index+i] * T0;
    endfor;
    
    fill D[path_index+3] with_color suit_color[suit_ctr];
    unfill D[path_index+4];
    unfill D[path_index+5];
    
    % Small suit symbol, upper left, lower right.

    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+2] with_color suit_color[suit_ctr];

    D[path_index+3] := D[path_index+2] * T0;

    fill D[path_index+3]  with_color suit_color[suit_ctr];
    
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'queen'.";

enddef;



%% * (1) macro king

def king {numeric suit_ctr, numeric path_index, numeric picture_index} =

  message "Entering macro 'king'.";
  message "Fig. " & decimal fig_ctr;
  %pause;

  beginfig(fig_ctr);

%% ** (2)  
    
    draw frame with_pen big_pen;
    output current_picture with_projection parallel_x_y;
    clear current_picture;
    
    filldraw r0 with_fill_color white with_pen medium_square_pen;

%% ** (2) King
    
    D[path_index] := (A97 scaled (.25, .25)) shifted by (C9 - B224 scaled (.25, .25)); % K, cmr10
    fill D[path_index] with_color suit_color[suit_ctr];

    D[path_index+1] := D[path_index] * T0;
    fill D[path_index+1] with_color suit_color[suit_ctr];

    % Small suit symbol, upper left, lower right.

    D[path_index+2] := (suit_path[suit_ctr] scaled (.25, .25))
      shifted by (C10 - suit_point[suit_ctr] scaled (.25, .25)); 

    fill D[path_index+2] with_color suit_color[suit_ctr];

    D[path_index+3] := D[path_index+2] * T0;

    fill D[path_index+3]  with_color suit_color[suit_ctr];
    
%% ** (2)

    draw r0 with_pen big_square_pen;

    V[picture_index] := current_picture;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;
  
  message "Exiting macro 'king'.";

enddef;

%% * (1) deal_cards

macro deal_cards;

def deal_cards (sstart_picture, eend_picture) {point sstart, point eend, numeric iindex} =

  message "Entering deal_cards.";


  U131 := eend shifted (0, 0, -4);
  U132 := eend shifted (0, 0, 4);

  set r[200+iindex] (U129, U130, U131, U132);

  U133 := get_center r[200+iindex];

  normal[200+iindex] := get_normal r[200+iindex];
  shift normal[200+iindex] by U133;

  t1 := align (U133 -- normal[200+iindex]) with_axis y_axis;

  mag := magnitude(eend - U128);

  e[100+iindex] := unit_ellipse scaled (.5mag, 0, 4);

  e[100+iindex] *= inverse t1;

  if (xpart eend) >= (xpart U128):
    m := 0;
    n := (size e[100+iindex]) / 2;
  else:
    m := (size e[100+iindex]) / 2;
    n := (size e[100+iindex]) - 1;
  fi;


  for loop_ctr = 1 upto 7:
    beginfig(fig_ctr);

%% *** (3)
    
      fill frame with_color background_color;

      draw frame with_pen big_square_pen;

      if do_labels:
	dotlabel.bot("$p_0$", p0);
	dotlabel.bot("$p_1$", p1);
	dotlabel.top("$p_2$", p2);
	dotlabel.top("$p_3$", p3);
      fi;

      if do_labels:
	dotlabel.bot("$U_8$", U8);
	dotlabel.rt("$U_9$", U9);
	dotlabel.top("$U_{10}$", U10);
	dotlabel.lft("$U_{11}$", U11);
      fi;

      output current_picture with_projection parallel_x_y;
      clear current_picture;

      current_picture := W0;
      shift current_picture (0, 5, 10);
      output current_picture with_focus f;
      clear current_picture;

      if do_combine and (iindex > 1):
	for i = 1 upto (iindex - 1):
	  current_picture := W[i];
	  shift current_picture (0, 5, 10);
	  output current_picture with_focus f;
	  clear current_picture;
	endfor;
      fi;
      
      % if do_help_lines:
      % 	draw sstart -- eend with_color red;
      % 	draw r[200+iindex] with_color green;
      % 	draw e[100+iindex] with_color violet;
      % 	for i = m step 2 until n:
      % 	  drawdot get_point (i) e[100+iindex] with_pen pencircle scaled (4mm, 4mm, 4mm)
      %              with_color blue;
      % 	endfor;
      % fi;

%% *** (3)
    
      V[200+iindex] := eend_picture;
      V[200+iindex] *= t0;

      V[200+iindex] *= t2;
      
      t1 := (identity rotated_around (U7, U5) -22.5loop_ctr)
	shifted by (get_point (m) e[100+iindex] - sstart);

      Y3 := sstart_picture;
      Y3 *= t1;
      r[300+iindex] := r100 * t1;
      V[200+iindex] *= t1;

      if loop_ctr < 5:
	current_picture += Y3;
      else:
	current_picture += V[200+iindex];
      fi;
      
      % if do_help_lines:
      % 	draw r[300+iindex] with_color blue;
      % fi;

      r[300+iindex] := r100 rotated_around (U7, U5) -22.5loop_ctr;
      shift r[300+iindex] by (get_point (m) e[100+iindex] - sstart);

      % if do_help_lines:
      % 	draw r[300+iindex] with_color blue;
      % fi;

%% *** (3)

      shift current_picture (0, 5, 10);
      
      output current_picture with_focus f;
      clear current_picture;
      clip current_picture to frame;
      draw frame with_pen big_square_pen;
      
    endfig with_projection parallel_x_y;
    fig_ctr += 1;
    m += 2;    
  endfor;

%% ** (2) Final image for r[100+iindex].

  beginfig(fig_ctr);

%% *** (3)
    
    fill frame with_color background_color;

    draw frame with_pen big_square_pen;

    if do_labels:
      dotlabel.bot("$p_0$", p0);
      dotlabel.bot("$p_1$", p1);
      dotlabel.top("$p_2$", p2);
      dotlabel.top("$p_3$", p3);
    fi;

    if do_labels:
      dotlabel.bot("$U_8$", U8);
      dotlabel.rt("$U_9$", U9);
      dotlabel.top("$U_{10}$", U10);
      dotlabel.lft("$U_{11}$", U11);
    fi;

    output current_picture with_projection parallel_x_y;
    clear current_picture;

%% *** (3)
    
    current_picture := W0;
    shift current_picture (0, 5, 10);
    output current_picture with_focus f;
    clear current_picture;

    if do_combine and (iindex > 1):
      for i = 1 upto (iindex - 1):
	current_picture := W[i];
	shift current_picture (0, 5, 10);
	output current_picture with_focus f;
	clear current_picture;
      endfor;
    fi;
    
    % if do_help_lines:
    %   draw sstart -- eend with_color red;
    %   draw r[200+iindex] with_color green;
    %   draw e[100+iindex] with_color violet;
    %   for i = m step 2 until n:
    % 	drawdot get_point (i) e[100+iindex] with_pen pencircle scaled (4mm, 4mm, 4mm) with_color blue;
    %   endfor;
    % fi;

    % if do_help_lines:
    %   draw sstart -- eend with_color red;
    %   draw r[200+iindex] with_color green;
    %   draw e[100+iindex] with_color violet;
    %   for i = m step 2 until n:
    % 	drawdot get_point (i) e[100+iindex] with_pen pencircle scaled (4mm, 4mm, 4mm) with_color blue;
    %   endfor;
    %   draw r[100+iindex] with_color blue with_pen BIG_pen;
    % fi;

    V[200+iindex] := eend_picture;
    V[200+iindex] *= t0;
    
    shift V[200+iindex] by (eend - sstart);

    current_picture += V[200+iindex];

    
    W[iindex] := current_picture;

    shift current_picture (0, 5, 10);

    output current_picture with_focus f;
    clear current_picture;
    clip current_picture to frame;
    draw frame with_pen big_square_pen;
    
  endfig with_projection parallel_x_y;
  fig_ctr += 1;

  
  message "Exiting deal_cards.";
  
enddef;


%% * (1)


endinput;

% %% ** (2)

  
endfig with_projection parallel_x_y; % with_focus f;
fig_ctr += 1;



%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

