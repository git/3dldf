%% playing_cards.ldf
%% Created by Laurence D. Finston (LDF) Di 28. Mai 07:18:44 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

input "plainldf.lmc";

medium_pen := pencircle scaled (.333mm, .333mm, .333mm);


boolean b;
point p[];
path q[];
path Q[];
picture v[];
picture V[];
transform t[];

picture back_of_card_picture;

numeric frame_wd;
numeric frame_ht;

numeric outer_frame_wd;
numeric outer_frame_ht;

frame_wd := 600mm;
frame_ht := 337.5mm;

frame_wd += 8mm;
frame_ht += 8mm;

outer_frame_wd := frame_wd + 10cm;
outer_frame_ht := frame_ht + 10cm;

rectangle frame;

frame := ((-.5frame_wd, -.5frame_ht), (.5frame_wd, -.5frame_ht),
          (.5frame_wd, .5frame_ht), (-.5frame_wd, .5frame_ht));

rectangle outer_frame;

outer_frame := ((-.5outer_frame_wd, -.5outer_frame_ht), (.5outer_frame_wd, -.5outer_frame_ht),
          (.5outer_frame_wd, .5outer_frame_ht), (-.5outer_frame_wd, .5outer_frame_ht));


for i = 0 upto 3:
  p[i] := get_point (i) frame;
endfor;

p4 := get_center frame;

pickup medium_pen;

string s;

boolean do_labels;
boolean do_help_lines;
boolean do_back_of_card;
boolean do_fill;

do_fill := true; % false; % 

do_back_of_card := true; % false; % 

if false: % true: %
  do_labels     := false;
  do_help_lines := false;
else:
  do_labels     := true; % false;
  do_help_lines := true; % false;
fi;

boolean do_png;

do_png := false; % true; %

if do_png:
  do_labels := false;

  verbatim_metapost   "outputformat:=\"png\";outputtemplate := \"%j_%4c.png\";"
    & "outputformatoptions := \"format=rgba antialias=none\";";
else:
  verbatim_metapost "prologues := 3;outputtemplate := \"%j_%4c.eps\";";
fi;

verbatim_metapost "ahlength := .5cm;;dotlabeldiam:=4mm;";
verbatim_tex   "\magnification=4500\font\smallrm=cmr10 scaled 600";

boolean do_middle_figures;
do_middle_figures := true; % false; % 

color background_color;
background_color := white;

color_vector cv;

cv += red;
cv += blue;
cv += dark_green;
cv += violet;
cv += orange;
cv += magenta;
cv += teal_blue_rgb;
cv += rose_madder_rgb;

path_vector qv;

%% * (1)


rectangle r[];
point u[];
path F[];
circle c[];
ellipse e[];


scale_val := 7.5;
r0 := (unit_rectangle scaled (2.5scale_val, 0, 3.5scale_val)) rotated (90, 0);

numeric border_width;

border_width := 1.25;

pickup Big_pen;

pickup big_pen;

numeric scale_val;

picture W[];

fig_ctr := 0;

focus f;

set f with_position (0, 5, -10) with_direction (0, 5, 10) with_distance 20;

%% * (1) Back of card

if do_back_of_card:
  input "back_of_card.ldf";
fi;

% message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
% pause;
% bye;

%% * (1)

input "glyphmac.lmc";

do_labels := true; % false; % 

input "glyphs_cmr10.ldf";

path A[];
point B[];

% Between `Y' and `y' point indices were inadvertantly skipped.
% The conditional in the loop for assigning to points tests
% whether the point on the right-hand side is known.
%
% Path indices weren't skipped, but a conditional was added
% in the loop for assigning to path variables as well,
% just in case this occurs at a later date.
% LDF 2024.05.31.

for i = 0 upto 149:
  if known q[i]:
    A[i] := q[i];
  fi;
endfor;

for i = 0 upto 333:
  if known p[i]: 
    B[i] := p[i];
  fi;
endfor;

path G[];
point H[];

input "glyphs_cmss10.ldf";

for i = 0 upto 118:
  if known q[i]:
    G[i] := q[i];
  fi;
endfor;

for i = 6 upto 247:
  if known p[i]: 
    H[i] := p[i];
  fi;
endfor;

input "glyphs_cmsy10.ldf";

%% * (1)


%% * (1) Playing cards

do_labels := false; % true; % 

point C[];
path D[];

transform T[];


triangle tri[];
reg_polygon rp[];
numeric picture_ctr;

%% * (1) Input files for the individual suits.


% `ace_of_clubs.ldf' Must be input before any of the "suit" files
% because it contains declarations and assignments that are used in
% them.  The suit files do not depend on each other.
% LDF 2024.05.31.

input "set_up_cards.ldf"; 

input "playing_cards.lmc";

picture_ctr := 0;

input "clubs.ldf";
input "diamonds.ldf";
input "hearts.ldf";
input "spades.ldf";

%% ** (2) Input files for the individual suits without middle figures.

if true: % false: % 

picture_ctr := 100;
do_middle_figures := false;

input "clubs.ldf";
input "diamonds.ldf";
input "hearts.ldf";
input "spades.ldf";


  input "button_up.ldf";
fi;


%% * (1) Rotation.  Loops.

%% ** (2) About the x-axis

if false:
  for i = -90 step 15 until 270:
    beginfig(fig_ctr);

      if do_png:
	fill frame with_color white;
      fi;
      
      draw frame with_pen big_square_pen;
      
      output current_picture with_projection parallel_x_y;
      clear current_picture;

      r100 := r0;
      rotate r100 (i, 0, 0);
      shift r100 (0, 0, 20);

      b := r100 is_facing f;

      if false:    
	message "Fig. " & decimal fig_ctr;
	message "b:";
	show b;
      fi;

      if b:
	current_picture := V0;
      else:
	current_picture := W1;
      fi;

      rotate current_picture (i, 0, 0);
      shift current_picture (0, 0, 20);
    endfig with_focus f;
    fig_ctr += 1;
  endfor;
fi;

%% ** (2) About the y-axis

if false:
  for i = 0 step 15 until 360:
    beginfig(fig_ctr);

      if do_png:
	fill frame with_color white;
      fi;


      draw frame with_pen big_square_pen;
      output current_picture with_projection parallel_x_y;
      clear current_picture;

      r100 := r0;
      rotate r100 (0, i, 0);
      shift r100 (0, 0, 20);

      b := r100 is_facing f;

      if false:    
	message "Fig. " & decimal fig_ctr;
	message "b:";
	show b;
      fi;

      if b:
	current_picture := V0;
      else:
	current_picture := W1;
      fi;

      rotate current_picture (0, i, 0);
      shift current_picture (0, 5, 20);
    endfig with_focus f;
    fig_ctr += 1;
  endfor;
fi;

%% ** (2) About the x- and y-axes

if false: % true: % 
  for i = 0 step 5 until 720:
    beginfig(fig_ctr);

      if do_png:
	fill frame with_color white;
      fi;

      draw frame with_pen big_square_pen;
      output current_picture with_projection parallel_x_y;
      clear current_picture;

      t0 := (identity rotated (2i, i/2, 0)) shifted (0, 5, 20);
      
      r100 := r0;
      r100 *= t0;

      b := r100 is_facing f;

      if false:    
	message "Fig. " & decimal fig_ctr;
	message "b:";
	show b;
      fi;

      if b:
	current_picture := V0;
      else:
	current_picture := W1;
      fi;

      current_picture *= t0;

    endfig with_focus f;
    fig_ctr += 1;
  endfor;
fi;

%% * (1) Fan.

if false: % true: % 
  input "fan.ldf";
fi;

%% * (1) Lay out 0.

if false: % true: % 
  input "lay_out_0.ldf";
fi;

%% * (1) spread

if false: % true: % 
  input "spread.ldf";
fi;

%% * (1)

message "fig_ctr - 1:  " & decimal (fig_ctr - 1);
if not do_png:
  pause;
fi;

bye;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:

