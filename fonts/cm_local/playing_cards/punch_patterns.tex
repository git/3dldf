%% punch_patterns.tex
%% Created by Laurence D. Finston (LDF) Do 30. Mai 11:04:36 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Top

%% * (1)

\input eplain
\input epsf 
\nopagenumbers
\input colordvi

\enablehyperlinks[dvipdfm]
\hlopts{bwidth=0}

%% * (1)

%% ** (2) Page format

\special{papersize=297mm, 210mm} %% DIN A4 Landscape
\hsize=297mm
\vsize=210mm

\topskip=0pt
\parskip=0pt
\parindent=0pt
\baselineskip=0pt

\advance\voffset by -1in
\advance\hoffset by -1in

\topskip=0pt
\parskip=0pt
\parindent=0pt
\baselineskip=0pt

%% *** (3) 

\def\epsfsize#1#2{#1}

%% *** (3) 

\footline={\hfil \folio\hfil}

\font\largerm=cmr12 scaled \magstep3

\pageno=1

%% ** (2) Fig. 0.  Card, undivided.

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_00.eps}\hss}\vskip-1cm
\hbox to \hsize{\hskip4cm {\largerm Fig.~00}\hss}%
\vss}
\fi 

%% ** (2) Fig. 6.  Rectangular divisions.

\iffalse % \iftrue %

\headline={\hfil\lower1.25cm\hbox{{\bf Rectangular Divisions}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}


\vbox to \vsize{\vbox{}\vskip2.5cm
\hbox to \hsize{\hskip2cm\epsffile{punch_patterns_06.eps}\hss}\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~06}\hss}%
\vss}
\eject
\fi 

%% ** (2) Fig. 7.  Tabs for rectangular divisions.


\iffalse % \iftrue % 
\headline={\hfil\lower1.25cm\hbox{{\bf Tabs for Rectangular Divisions}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}

\vbox to \vsize{\vbox{}\vskip1.5cm
\hbox to \hsize{\hskip1cm\epsffile{punch_patterns_07.eps}\hss}%
%\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~07}\hss}%
\vss}
\eject
\fi 

%% ** (2) Fig. 8.  Rectangular divisions, version 2.


\iffalse % \iftrue % 

\headline={\hfil\lower1.25cm\hbox{{\bf Rectangular Divisions, Version 2}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}

\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_08.eps}\hss}\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~08}\hss}%
\vss}
\fi 

%% ** (2) Fig. 9.  Triangular divisions version 1.

\iffalse % \iftrue % 
\headline={\hfil\lower1.25cm\hbox{{\bf Triangular Divisions version 1}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}

\vbox to \vsize{\vbox{}\vskip2.5cm
\hbox to \hsize{\hskip2cm\epsffile{punch_patterns_09.eps}\hss}%
%\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~09}\hss}%
\vss}
\eject
\fi 

%% ** (2) Fig. 10.  Triangular divisions version 1, continued.

\iffalse % \iftrue % 
\headline={\hfil\lower1.25cm\hbox{{\bf Triangular Divisions version 1, cont.}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}

\vbox to \vsize{\vbox{}\vskip2.5cm
\hbox to \hsize{\hskip2cm\epsffile{punch_patterns_10.eps}\hss}%
%\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~10}\hss}%
\vss}
\eject
\fi 

%% ** (2) Fig. 11.  Triangular divisions version 1, combined.

\iftrue % \iffalse % 
\headline={\hfil\lower1.25cm\hbox{{\bf Triangular Divisions version 1, combined.}\hskip4em
Author: Laurence Finston\hskip4em
Copyright {\copyright} 2024, 2025 The Free Software Foundation\hskip4em
{\tt \timestamp}}\hskip2cm}

\vbox to \vsize{\vbox{}\vskip1.25cm
\hbox to \hsize{\hskip2cm\epsffile{punch_patterns_11.eps}\hss}%
%\vskip-1cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~11}\hss}%
\vss}
\eject
\fi 



\bye

%% ** (2) Fig. 1.  Single tab, short side.

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_01.eps}\hss}\vskip-1cm
\hbox to \hsize{\hskip4cm {\largerm Fig.~01}\hss}%
\vss}
\fi

%% ** (2) Fig. 2.  Single tab, long side.

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_02.eps}\hss}\vskip-1cm
\hbox to \hsize{\hskip4cm {\largerm Fig.~02}\hss}%
\vss}
\fi 

%% ** (2) Fig. 3.  Divisions, halves

\iftrue % \iffalse % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_03.eps}\hss}\vskip-1cm
\hbox to \hsize{\hskip4cm {\largerm Fig.~03}\hss}%
\vss}
\fi 







%% ** (2) Card, Divided diagonally.


\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_04.eps}\hss}\vskip-1cm
\hbox to \hsize{\hskip4cm {\largerm Fig.~04}\hss}%
\vss}
\fi 

\bye

%% ** (2) Hexagons

%% *** (3) Diam. 1.5cm

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vskip1cm
\hbox to \hsize{\hskip1cm\epsffile{punch_patterns_07.eps}\hss}%
%\vskip10cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~07}\hss}%
\vss}
\fi 


\bye

%% *** (3) Hexagons, diam. 1cm

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vskip1cm
\hbox to \hsize{\hskip1.5cm\epsffile{punch_patterns_06.eps}\hss}%
%\vskip10cm
%\hbox to \hsize{\hskip4cm {\largerm Fig.~06}\hss}%
\vss}
\fi 



%% ** (2) Tabs for card, divided diagonally.

\iffalse % \iftrue % 
\vbox to \vsize{\vbox{}\vskip1.125cm
\hbox to \hsize{\hskip1.25cm\epsffile{punch_patterns_05.eps}\hss}%\vskip3cm
%\hbox to \hsize{\hskip1.25cm {\largerm Fig.~05}\hss}%
\vss}
\fi 

\bye


%% ** (2) Card, half.

\iftrue % \iffalse % 
\vbox to \vsize{\vbox{}\vss
\hbox to \hsize{\hss\epsffile{punch_patterns_03.eps}\hss}%
\vss}
\fi 

%% ** (2) Tabs, short

\iffalse % \iftrue % 
\setbox0=\vbox{\hbox{\epsffile{punch_patterns_01.eps}\hskip.5cm
\epsffile{punch_patterns_01.eps}\hskip.5cm\epsffile{punch_patterns_01.eps}\hskip.5cm
\epsffile{punch_patterns_01.eps}}}

\hoffset=-1cm
\vbox to \vsize{\vskip1.5cm\copy0\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vss}
\fi

%% ** (2) Tabs, long

\iffalse % \iftrue % 
\setbox0=\vbox{\hbox{\epsffile{punch_patterns_02.eps}\hskip.5cm
\epsffile{punch_patterns_02.eps}\hskip.5cm\epsffile{punch_patterns_02.eps}}}

\hoffset=-1.333cm
\vbox to \vsize{\vskip1.125cm\copy0\vskip.5cm\copy0\vskip.5cm\copy0\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0
\vskip.5cm\copy0\vss}
\fi

\bye


%% ** (2) 

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not evaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (plain-tex-mode) (outline-minor-mode t) (setq outline-regexp "%% [*\f]+"))    

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:plain-TeX
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% abbrev-mode:t
%% outline-regexp:"%% [*\f]+"
%% auto-fill-function:nil
%% end:
