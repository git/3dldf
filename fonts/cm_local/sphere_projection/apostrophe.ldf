%% apostrophe.ldf
%% Created by Laurence D. Finston (LDF) Mo 17. Jun 18:55:57 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

%% * (1) "'" (apostrophe).  Set up sphere.

pickup big_pen;

beginfig(fig_ctr);

%% ** (2)
  
  draw frame with_pen big_square_pen with_color frame_color;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2) Q25:  Original path.  


  draw S0 with_color sphere_color;
  
  % if do_labels:
  %   dotlabel.lrt("$o$", origin);
  % fi;
  
  
  Q25 := q119 shifted by (origin - p252);

  shift Q25 (0, 0, -8);
  
  n := (size Q25) - 1;

  % for i = 0 upto n:
  %   dotlabel.top(decimal i, get_point (i) Q25) with_color red;
  % endfor;

  if do_originals:
    fill Q25 with_color cyan;
    draw Q25;
  fi; 


  clear qv;

  qv := resolve Q25 to 100;
  Q26 := qv0;
  shift Q26 (0, 0, -8);


  if do_originals:
    draw Q26 with_color green;
  fi;
  
  n := (size Q26) - 1;

  Q27 += ..;
  
  for i = 0 upto n:
    clear bpv;
    bpv := (origin -- get_point (i) Q26) intersection_points S0;
    temp_pt0 := bpv0;
    temp_pt1 := bpv1;
    if zpart temp_pt0 < zpart temp_pt1:
      Q27 += temp_pt0;
    else:
      Q27 += temp_pt1;
    fi;
    endfor;

    Q27 += cycle;

    filldraw Q27 with_pen Big_pen with_fill_color red;
    
    if false:
      for i = 0 step 45 until 315:
	draw Q27 rotated (0, i) with_pen Big_pen with_color red;
      endfor;
    fi;
    
      
%% ** (2)  
  
  v4 := current_picture;
    
  shift current_picture (0, 5, 20);

  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen with_color frame_color;

endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop "'" (apostrophe)

for loop_ctr = 1 upto 1:
beginfig(fig_ctr);

  draw frame with_pen big_square_pen with_color frame_color;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  v5 := v4;

  rotate v5 (0, 10loop_ctr);

  scale v5 (.2, .2, .2);
  
  rotate v5 (0, 10loop_ctr);
  rotate v5 (15, 0);

  shift v5 (-25, 18);
  
  current_picture += v5;
    
  shift current_picture (0, 5, 20);

  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen with_color frame_color;

endfig with_projection parallel_x_y;
fig_ctr += 1;
endfor;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
