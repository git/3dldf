%% m.ldf
%% Created by Laurence D. Finston (LDF) Mo 17. Jun 21:26:49 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1)

%% * (1) "m".  Set up sphere.

pickup big_pen;

do_labels := false; % true; % 
do_help_lines := false; % true; % 

if do_png:
  do_labels := false;
  do_help_lines := false;
fi; 

beginfig(fig_ctr);

%% ** (2)

  message "Fig. " & decimal fig_ctr;
  message "The letter m";
  
  draw frame with_pen big_square_pen with_color frame_color;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

%% ** (2)

  draw S0 with_color sphere_color;

  % if do_labels:
  %   for i = 0 upto (size c2) - 1:
  %     dotlabel.top(decimal i, get_point (i) c2) with_color blue;
  %   endfor;
  % fi;

  if do_labels:
    dotlabel.rt("0", get_point 0 c2) with_color blue;
  fi; 
  

%% ** (2) Q28:  Original path.  
  

  
  if do_labels:
    dotlabel.lrt("$o$", origin);
  fi;
  
  
  Q28 := q49 shifted by (origin - p115);

  shift Q28 (0, 0, -8);
  
  n := (size Q28) - 1;
  
  if do_labels:
    for i = 0 upto n:
      dotlabel.top(decimal i, get_point (i) Q28) with_color red;
    endfor;
  fi;
  

  if false:
    fill Q28 with_color cyan;
    draw Q28;
  fi;
  
  clear qv;

  qv := resolve Q28 (0, 2) to 300;
  Q29 := qv0;
  shift Q29 (0, 0, -8);

  if do_help_lines:
    drawarrow Q29 with_color green;
  fi;
  

  if do_labels:
    label.urt("$Q_{29}$", mediate(get_point 0 Q28, get_point 1 Q28) shifted (.5, .5)) with_color green;
  fi;
  
  
  clear qv;

  qv := resolve Q28 (2, 4) to 300;
  Q30 := qv0;
  shift Q30 (0, 0, -8);

  if do_help_lines:
    drawarrow Q30 with_color magenta;
  fi;
  

  if do_labels:
    label.top("$Q_{30}$", (get_point 3 Q28) shifted (0, 1)) with_color magenta;
  fi;
  

  clear qv;

  qv := resolve Q28 (4, 9) to 300;
  Q31 := qv0;
  shift Q31 (0, 0, -8);

  if do_help_lines:
    drawarrow Q31 with_color orange;
  fi;
  

  if do_labels:
    label.lft("$Q_{31}$", mediate(get_point 6 Q28, get_point 7 Q28)) with_color orange;
  fi;
  

  clear qv;

  qv := resolve Q28 (9, 11) to 300;
  Q32 := qv0;
  shift Q32 (0, 0, -8);

  if do_help_lines:
    drawarrow Q32 with_color teal_blue_rgb;
  fi;
  

  if do_labels:
    label.bot("$Q_{32}$", (get_point 10 Q28) shifted (0, -.25)) with_color teal_blue_rgb;
  fi;
  

  clear qv;

  qv := resolve Q28 (11, 14) to 300;
  Q33 := qv0;
  shift Q33 (0, 0, -8);

  if do_help_lines:
    drawarrow Q33 with_color rose_madder_rgb;
  fi;
  

  if do_labels:
    label.rt("$Q_{33}$", mediate(get_point 13 Q28, get_point 14 Q28)) with_color rose_madder_rgb;
  fi;
  

  clear qv;

  qv := resolve Q28 (14, 16) to 300;
  Q34 := qv0;
  shift Q34 (0, 0, -8);

  if do_help_lines:
    drawarrow Q34 with_color violet;
  fi;
  

  if do_labels:
    label.bot("$Q_{34}$", (get_point 15 Q28) shifted (0, -.5)) with_color violet;
  fi;
  

  clear qv;
  
  qv := resolve Q28 (16, 19) to 300;
  Q35 := qv0;
  shift Q35 (0, 0, -8);

  if do_help_lines:
    drawarrow Q35 with_color Goldenrod_rgb;
  fi;
  

  if do_labels:
    label.rt("$Q_{35}$", mediate(get_point 18 Q28, get_point 0 Q28)) with_color Goldenrod_rgb;
  fi;
  

  Q36 := Q29 -- Q30 -- Q31 -- Q32 -- Q33 -- Q34 -- Q35;

  Q36 += --;
  Q36 += cycle;

  if do_help_lines:
    drawarrow Q36 with_color Melon_rgb with_pen Big_pen;
  fi;
  

  if do_labels:
    label.top("$Q_{36}$", (get_point 1 Q28) shifted (0, 1))with_color Melon_rgb;
  fi;
  
  Q37 += ..;

  n := (size Q36) - 1;
  
  for i = 0 upto n:
    clear bpv;
    bpv := (origin -- get_point (i) Q36) intersection_points S0;
    temp_pt0 := bpv0;
    temp_pt1 := bpv1;
    if zpart temp_pt0 < zpart temp_pt1:
      Q37 += temp_pt0;
    else:
      Q37 += temp_pt1;
    fi;
    endfor;

    Q37 += cycle;

    filldraw Q37 with_pen Big_pen with_fill_color red; % "m"

    if do_labels:
      label.rt("$Q_{37}$", get_point 0 Q37) with_color red;
    fi;    
    
%% ** (2)  
  
  v6 := current_picture;
    
  shift current_picture (0, 5, 20);

  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen with_color frame_color;

endfig with_projection parallel_x_y;
fig_ctr += 1;

%% * (1) Loop "m"

for loop_ctr = 1 upto 1:
beginfig(fig_ctr);

  message "Fig. " & decimal fig_ctr;
  
  draw frame with_pen big_square_pen with_color frame_color;
  output current_picture with_projection parallel_x_y;
  clear current_picture;

  v7 := v6;

  rotate v7 (0, 10loop_ctr);

  scale v7 (.2, .2, .2);
  
  rotate v7 (0, 10loop_ctr);
  rotate v7 (15, 0);

  shift v7 (-17, 18);
  
  current_picture += v7;
    
  shift current_picture (0, 5, 20);

  output current_picture with_focus f;
  clear current_picture;

  clip current_picture to frame;
  draw frame with_pen big_square_pen with_color frame_color;

endfig with_projection parallel_x_y;
fig_ctr += 1;
endfor;

%% * (1)

endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
