#! /bin/bash


ln -s -t letter_charts/ `pwd`/Euler_local/letter_chart_eurm10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmti10/letter_chart_cmti10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmitt10/letter_chart_cmitt10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmss10/letter_chart_cmss10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmssbx10/letter_chart_cmssbx10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmtt10/letter_chart_cmtt10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmr10/letter_chart_cmr10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmdunh10/letter_chart_cmdunh10.pdf
ln -s -t letter_charts/ `pwd`/cm_local/cmsy10/letter_chart_cmsy10.pdf

exit 0
