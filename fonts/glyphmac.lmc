%% glyphmac.lmc
%% Created by Laurence D. Finston (LDF) So 19. Mai 12:07:53 CEST 2024

%% * (0) Copyright and License.

%% This file is part of GNU 3DLDF, a package for three-dimensional drawing. 
%% Copyright (C) 2024, 2025 The Free Software Foundation, Inc. 

%% GNU 3DLDF is free software; you can redistribute it and/or modify 
%% it under the terms of the GNU General Public License as published by 
%% the Free Software Foundation; either version 3 of the License, or 
%% (at your option) any later version. 

%% GNU 3DLDF is distributed in the hope that it will be useful, 
%% but WITHOUT ANY WARRANTY; without even the implied warranty of 
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
%% GNU General Public License for more details. 

%% You should have received a copy of the GNU General Public License 
%% along with GNU 3DLDF; if not, write to the Free Software 
%% Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 

%% GNU 3DLDF is a GNU package.  
%% It is part of the GNU Project of the  
%% Free Software Foundation 
%% and is published under the GNU General Public License. 
%% See the website http://www.gnu.org 
%% for more information.   
%% GNU 3DLDF is available for downloading from 
%% http://www.gnu.org/software/3dldf/LDF.html. 

%% ("@@" stands for a single at-sign in the following paragraph.) 

%% Please send bug reports to Laurence.Finston@@gmx.de 
%% The mailing list help-3dldf@@gnu.org is available for people to 
%% ask other users for help.  
%% The mailing list info-3dldf@@gnu.org is for sending 
%% announcements to users. To subscribe to these mailing lists, send an 
%% email with "subscribe <email-address>" as the subject.  

%% The author can be contacted at: 

%% Laurence D. Finston                  
%% c/o Free Software Foundation, Inc.  
%% 51 Franklin St, Fifth Floor          
%% Boston, MA  02110-1301              
%% USA                                 

%% Laurence.Finston@@gmx.de (@@ stands for a single "at" sign.)

%% * (1) Top

%% * (1) macro letter

%% !! PLEASE NOTE:  The algorithm implemented in this macro doesn't account for glyphs such as "apostrophe",
%% that don't go all the way down to the baseline.  In these cases, the surrounding box must be used for
%% placement and the box that fits the glyph tightly must be programmed by hand.
%%
%% However, it would probably be possible to account for this case.
%% LDF 2024.06.16.

macro letter;

def letter (G) {string ffont_name, color ffont_color, numeric glyph_ctr, numeric path_ctr, 
            numeric path_index, numeric point_index, numeric_vector suffixes_vector, 
            point ref_pt, numeric shift_val_x, numeric shift_val_y,
            boolean do_test, numeric xpart_point_ctr, numeric ypart_point_ctr,
            numeric center_shift_val_x, numeric center_shift_val_y, 
            numeric path_zero_shift_val_x, numeric path_zero_shift_val_y,
            numeric path_one_shift_val_x, numeric path_one_shift_val_y,
            numeric path_two_shift_val_x, numeric path_two_shift_val_y,
            numeric border_shift_val_x, numeric border_shift_val_y,
            boolean fill_path_one, boolean fill_path_two, boolean use_path_one} = 

    
  % message "glyph_ctr   		== " & decimal glyph_ctr;
  % message "path_ctr    		== " & decimal path_ctr;
  % message "path_index  		== " & decimal path_index;
  % message "point_index 		== " & decimal point_index;

  % message "size suffixes_vector == " & decimal size suffixes_vector;

  % n := (size suffixes_vector) - 1;

  % for i = 0 upto n:
  %   message "suffixes_vector" & decimal i & " == " & decimal suffixes_vector[i];
  % endfor;

  % message "ref_pt:";
  % show ref_pt;

  % message "shift_val_x == " & decimal shift_val_x;
  % message "shift_val_y == " & decimal shift_val_y;

  % message "do_test == ";
  % show do_test;

  % message "xpart_point_ctr == " & decimal xpart_point_ctr;
  % message "ypart_point_ctr == " & decimal ypart_point_ctr;

  % message "fill_path_one == ";
  % show fill_path_one;

  % message "fill_path_two == ";
  % show fill_path_two;

  % message "use_path_one == ";
  % show use_path_one;

  color llabel_color;

  if known label_color:
    llabel_color := label_color;
  else:	 
    llabel_color := red;
  fi;

  string A;

  G := get_glyph glyph_ctr from ffont_name;

  clear qv;

  qv := get_paths from G;

  scale qv (12, 12, 12);

  j := path_index;
  
  for i = 0 upto path_ctr:
    q[i+j] := qv[i];
  endfor;
  
  if do_test:
    if use_path_one:
      n := (size q[j+1]) - 1;
    else:
      n := (size q[j]) - 1;
    fi;

    %message "n == " & decimal n;
    
    for i = 0 upto n:
      s := "$\scriptstyle " & decimal i & "$";
      if use_path_one:
	dotlabel.top(s, get_point (i) q[j+1]) with_color llabel_color;
      else:
	dotlabel.top(s, get_point (i) q[j]) with_color llabel_color;
      fi;
    endfor;
  fi;

  if (size suffixes_vector) > 0:
    n := (size suffixes_vector - 1);
    for i = 0 upto n:
      message decimal suffixes_vector[i];
      m := suffixes_vector[i];
      show get_point (m) q[j];
    endfor;
    pause;
  fi;
  
  k := point_index;
  
  p[k] := ref_pt shifted (shift_val_x, shift_val_y);

  if not do_test:

    if use_path_one:
      p[k+1] := (xpart get_point (xpart_point_ctr) q[j+1], ypart get_point (ypart_point_ctr) q[j+1]);
    else:
      p[k+1] := (xpart get_point (xpart_point_ctr) q[j], ypart get_point (ypart_point_ctr) q[j]);
    fi;

    for i = path_ctr downto 0:
      shift q[i+j] by (p[k] - p[k+1]);
    endfor;

    p[k+1] := get_point 1 q[j+path_ctr];
    p[k+2] := get_point 2 q[j+path_ctr];
    p[k+3] := (xpart p[k], ypart p[k+2]);
    p[k+4] := mediate(p[k], p[k+2]);

    q[j+path_ctr] := p[k] -- p[k+1] -- p[k+2] -- p[k+3] -- cycle;
  fi;
  
  if path_ctr == 1:
    draw q[j+1] with_color dark_gray;
  fi;

  if path_ctr == 2:
    draw q[j+2] with_color dark_gray;
  fi;

  if path_ctr == 3:
    draw q[j+3] with_color dark_gray;
  fi;

  if path_ctr == 4:
    draw q[j+4] with_color dark_gray;
  fi;
  
  filldraw q[j] with_fill_color ffont_color;

  if path_ctr > 1:
    if fill_path_one:
      filldraw q[j+1] with_fill_color ffont_color;
    else:
      unfilldraw q[j+1];
    fi;
  fi;

  if path_ctr > 2:
    if fill_path_two:
      filldraw q[j+2] with_fill_color ffont_color;
    else:
      unfilldraw q[j+2];
    fi;
  fi;

  if path_ctr > 3:
    unfilldraw q[j+3];
  fi;

  
  if do_labels:

    if not do_test:

      if glyph_ctr == 73:
	A := "$p_{" & decimal k & "}$";
	label.bot(A, p[k] shifted (-1, 0));
	drawdot p[k] with_pen dot_pen;
	A := "$p_{" & decimal (k+1) & "}$";
	label.bot(A, p[k+1] shifted (1, 0));
	drawdot p[k+1] with_pen dot_pen;
	A := "$p_{" & decimal (k+2) & "}$";
	label.top(A, p[k+2] shifted (1, 0));
	drawdot p[k+2] with_pen dot_pen;
	A := "$p_{" & decimal (k+3) & "}$";
	label.top(A, p[k+3]) shifted (-1, 0);
	drawdot p[k+3] with_pen dot_pen;
	A := "$p_{" & decimal (k+4) & "}$";
	label.bot(A, p[k+4] shifted (center_shift_val_x, center_shift_val_y));
      else:
	A := "$p_{" & decimal k & "}$";
	dotlabel.bot(A, p[k]);
	A := "$p_{" & decimal (k+1) & "}$";
	dotlabel.bot(A, p[k+1]);
	A := "$p_{" & decimal (k+2) & "}$";
	dotlabel.top(A, p[k+2]);
	A := "$p_{" & decimal (k+3) & "}$";
	dotlabel.top(A, p[k+3]);
	A := "$p_{" & decimal (k+4) & "}$";
	label.bot(A, p[k+4] shifted (center_shift_val_x, center_shift_val_y));
      fi;


      A := "$q_{" & decimal j & "}$";
      label(A, p[k+4] shifted (path_zero_shift_val_x, path_zero_shift_val_y));

      if path_ctr >= 2:
	A := "$q_{" & decimal (j+1) & "}$";
	label(A, p[k+4] shifted (path_one_shift_val_x, path_one_shift_val_y));
      fi;

      if path_ctr >= 3:
	A := "$q_{" & decimal (j+2) & "}$";
	label(A, p[k+4] shifted (path_two_shift_val_x, path_two_shift_val_y));
      fi;
                  
      A := "$q_{" & decimal (j+path_ctr) & "}$";
      label.top(A, mediate(p[k+3], p[k+2]) shifted (0+border_shift_val_x, .125+border_shift_val_y))
	with_color dark_gray;

      drawdot p[k+4] with_pen dot_pen;
    fi;
  fi;

  
enddef;

%% * (1) macro letter_no_draw

%% !! PLEASE NOTE:  The algorithm implemented in this macro doesn't account for glyphs such as "apostrophe",
%% that don't go all the way down to the baseline.  In these cases, the surrounding box must be used for
%% placement and the box that fits the glyph tightly must be programmed by hand.
%%
%% However, it would probably be possible to account for this case.
%% LDF 2024.06.16.

macro letter_no_draw;

def letter_no_draw (G) {string ffont_name, color ffont_color, numeric glyph_ctr, numeric path_ctr, 
            		numeric path_index, numeric point_index, numeric_vector suffixes_vector, 
            		point ref_pt, numeric shift_val_x, numeric shift_val_y,
            		boolean do_test, numeric xpart_point_ctr, numeric ypart_point_ctr,
            		numeric center_shift_val_x, numeric center_shift_val_y, 
            		numeric path_zero_shift_val_x, numeric path_zero_shift_val_y,
            		numeric path_one_shift_val_x, numeric path_one_shift_val_y,
            		numeric path_two_shift_val_x, numeric path_two_shift_val_y,
            		numeric border_shift_val_x, numeric border_shift_val_y,
            		boolean fill_path_one, boolean fill_path_two, boolean use_path_one} = 

    
  % message "glyph_ctr   		== " & decimal glyph_ctr;
  % message "path_ctr    		== " & decimal path_ctr;
  % message "path_index  		== " & decimal path_index;
  % message "point_index 		== " & decimal point_index;

  % message "size suffixes_vector == " & decimal size suffixes_vector;

  % n := (size suffixes_vector) - 1;

  % for i = 0 upto n:
  %   message "suffixes_vector" & decimal i & " == " & decimal suffixes_vector[i];
  % endfor;

  % message "ref_pt:";
  % show ref_pt;

  % message "shift_val_x == " & decimal shift_val_x;
  % message "shift_val_y == " & decimal shift_val_y;

  % message "do_test == ";
  % show do_test;

  % message "xpart_point_ctr == " & decimal xpart_point_ctr;
  % message "ypart_point_ctr == " & decimal ypart_point_ctr;

  % message "fill_path_one == ";
  % show fill_path_one;

  % message "fill_path_two == ";
  % show fill_path_two;

  % message "use_path_one == ";
  % show use_path_one;

  color llabel_color;

  if known label_color:
    llabel_color := label_color;
  else:	 
    llabel_color := red;
  fi;

  string A;

  G := get_glyph glyph_ctr from ffont_name;

  clear qv;

  qv := get_paths from G;

  scale qv (12, 12, 12);

  j := path_index;
  
  for i = 0 upto path_ctr:
    q[i+j] := qv[i];
  endfor;
  
  if do_test:
    if use_path_one:
      n := (size q[j+1]) - 1;
    else:
      n := (size q[j]) - 1;
    fi;

    %message "n == " & decimal n;
    
  fi;

  if (size suffixes_vector) > 0:
    n := (size suffixes_vector - 1);
    for i = 0 upto n:
      message decimal suffixes_vector[i];
      m := suffixes_vector[i];
      show get_point (m) q[j];
    endfor;
    pause;
  fi;
  
  k := point_index;
  
  p[k] := ref_pt shifted (shift_val_x, shift_val_y);

  if not do_test:

    if use_path_one:
      p[k+1] := (xpart get_point (xpart_point_ctr) q[j+1], ypart get_point (ypart_point_ctr) q[j+1]);
    else:
      p[k+1] := (xpart get_point (xpart_point_ctr) q[j], ypart get_point (ypart_point_ctr) q[j]);
    fi;

    for i = path_ctr downto 0:
      shift q[i+j] by (p[k] - p[k+1]);
    endfor;

    p[k+1] := get_point 1 q[j+path_ctr];
    p[k+2] := get_point 2 q[j+path_ctr];
    p[k+3] := (xpart p[k], ypart p[k+2]);
    p[k+4] := mediate(p[k], p[k+2]);

    q[j+path_ctr] := p[k] -- p[k+1] -- p[k+2] -- p[k+3] -- cycle;
  fi;
  
enddef;




endinput;

%% * (1) Emacs-Lisp code for use in indirect buffers when using the          
%%       GNU Emacs editor.  The local variable list is not fvaluated when an 
%%       indirect buffer is visited, so it's necessary to evaluate the       
%%       following s-expression in order to use the facilities normally      
%%       accessed via the local variables list.                              
%%       LDF 2004.02.12.

%% (progn (metafont-mode) (outline-minor-mode t) (setq fill-column 80) (ignore '(  
%% )) (setq outline-regexp "%% [*\f]+"))

%% * (1) Local variables for Emacs.

%% Local Variables:
%% mode:Metapost
%% eval:(outline-minor-mode t)
%% eval:(read-abbrev-file abbrev-file-name)
%% outline-regexp:"%% [*\f]+"
%% End:
